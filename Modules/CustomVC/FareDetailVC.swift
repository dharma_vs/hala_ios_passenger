//
//  FareDetailVC.swift
//  HalaMobility
//
//  Created by ADMIN on 17/08/22.
//

import UIKit

class FareDetailVC: UIViewController {
    @IBOutlet weak var lblDisplay: AppLabel!
    @IBOutlet weak var imgVehicle: AppImageView!
    
    @IBOutlet weak var lblUnlock: AppLabel!
    @IBOutlet weak var lblUnlockans: AppLabel!
    
    @IBOutlet weak var lblBase: AppLabel!
    @IBOutlet weak var lblBasekans: AppLabel!
    
    @IBOutlet weak var lblMinute: AppLabel!
    @IBOutlet weak var lblMinuteans: AppLabel!
    
    @IBOutlet weak var lblPause: AppLabel!
    @IBOutlet weak var lblPauseans: AppLabel!
    
    @IBOutlet weak var lblGST: AppLabel!
    @IBOutlet weak var lblGSTans: AppLabel!
    @IBOutlet weak var btnOK: AppButton!
    var fetchdetail: Rental1?

    override func viewDidLoad() {
        super.viewDidLoad()
        lblUnlock.text = "Unlock Fare".localized
        lblBase.text = "Base Fare".localized
        lblMinute.text = "Minute Fare (0 Min)".localized
        lblPause.text = "Pause Fare (10 Mins)".localized
        lblGST.text = "GST".localized
        self.btnOK?.setTitle("OK".localized, for: .normal)
        lblDisplay.text = fetchdetail?.vehicleTypeInfo?.displayName ?? ""
        let image = baseImageURL + (fetchdetail?.vehicleTypeInfo?.image ?? "")!
        imgVehicle.hnk_setImage(from: image.urlFromString, placeholder: nil, success: { (image) in
            self.imgVehicle.image = image
            }, failure: nil)

        let farevalues = fetchdetail?.fareBreakup as? FareBreakupClass
        lblUnlockans.text =  Extension.updateCurrecyRate(fare: "\(String(farevalues?.unlockFare?.stringValue ?? "0"))").currencyValue
        lblBasekans.text =  Extension.updateCurrecyRate(fare: "\(String(farevalues?.baseFare?.stringValue ?? "0"))").currencyValue
        lblMinuteans.text =  Extension.updateCurrecyRate(fare: "\(String(farevalues?.minuteFare?.stringValue ?? "0"))").currencyValue
        lblPauseans.text =  Extension.updateCurrecyRate(fare: "\(String(farevalues?.pauseMins?.stringValue ?? "0"))").currencyValue
        lblGSTans.text =  Extension.updateCurrecyRate(fare: "\(String(farevalues?.unlockFare?.stringValue ?? "0"))").currencyValue

        let gestureRecognizer = UITapGestureRecognizer(target: self,action: #selector(dismiss))
        self.view.addGestureRecognizer(gestureRecognizer)
    }
    @objc func dismiss(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnclkOK() {
        self.dismiss(animated: true, completion: nil)
    }
}
