//
//  ConnectVC.swift
//  HalaMobility
//
//  Created by ADMIN on 21/07/22.
//

import UIKit

class ConnectVC: UIViewController {
    @IBOutlet weak var lblPlswait: AppLabel!
    @IBOutlet weak var imgScooter: AppImageView!
    
    @IBOutlet weak var lblScan: AppLabel!
    @IBOutlet weak var loading1: UIActivityIndicatorView!
    
    @IBOutlet weak var ConstViewHeight: NSLayoutConstraint!
    @IBOutlet weak var stackview: UIStackView!
    
    @IBOutlet weak var lblChecking: AppLabel!
    @IBOutlet weak var loading2: UIActivityIndicatorView!
    
    @IBOutlet weak var lbllock: AppLabel!
    @IBOutlet weak var loading3: UIActivityIndicatorView!
    
    @IBOutlet weak var lblenable: AppLabel!
    @IBOutlet weak var loading4: UIActivityIndicatorView!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!

    
    var rentalstatus:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        lblPlswait.text = "pleasewait".localized
        if rentalstatus == "start"{
            lblScan.text = "scantheconnection".localized
            lbllock.text = "checkingthescooter".localized
            loading1.startAnimating()
            loading2.startAnimating()
            view3.isHidden = true
            view4.isHidden = true
            ConstViewHeight.constant = 270
        }else{
            lbllock.text = "lockthescooter".localized
            lblenable.text = "enabledefense".localized
            loading3.startAnimating()
            loading4.startAnimating()
            view3.isHidden = false
            view4.isHidden = false
            ConstViewHeight.constant = 370
        }
        
        let gestureRecognizer = UITapGestureRecognizer(target: self,action: #selector(dismiss))
        self.view.addGestureRecognizer(gestureRecognizer)
    }
    @objc func dismiss(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
