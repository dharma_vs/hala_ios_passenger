//
//  FavoriteLocationViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit
import DropDown


class FavoriteLocationViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func addLocation(_ sender: AppButton) {
        self.showSearchViewController("".localized, type: "0", id: NSNotFound, isNeedToUpdate: false, searchCompletion: { [unowned self] (req) in
            let saveLocationVC = SaveLocationViewController.initFromStoryBoard(.Menu)
            saveLocationVC.locationInfo = req?.location
            saveLocationVC.didRequestToSaveLocation = { [unowned self] updatedLocation in
                if let location = updatedLocation {
                    self.addFavouriteLocation(location.locationReq, completion: { [unowned self] in
                        self.tableView?.reloadData()
                    })
                }
            }
            self.present(saveLocationVC, animated: true, completion: nil)
        }) { (location) in }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init("PROFILE_UPDATED"), object: nil, queue: .main) { [weak self](notification) in
            //self?.tableView?.reloadData()
        }

        // Do any additional setup after loading the view.
    }
    
}

extension FavoriteLocationViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 2 : (CacheManager.favouriteLocations.filter{
            let locationNames = [String.Home, String.Work]
            return !locationNames.contains($0.locationName ?? "")
        }).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: FavouriteAddressCell2 = tableView.dequeueCell(indexPath)
        if let tmpCell = cell as? FavouriteAddressCell2 {
            if indexPath.section == 0 {
                let homeLocation = CacheManager.favouriteLocations.filter{$0.type == 1}.first
                let workLocation = CacheManager.favouriteLocations.filter{$0.type == 2}.first
                if indexPath.row == 0 {
                    tmpCell.favLbl.text = homeLocation?.locationName ?? "Add Home"
                    tmpCell.addressLbl.text = homeLocation?.locationAddress
                    tmpCell.optionBtn.isHidden = true
                    tmpCell.iconImage.image = #imageLiteral(resourceName: "ic_fav_home")
                    tmpCell.location = homeLocation
                    tmpCell.accessoryType = .disclosureIndicator
                } else {
                    tmpCell.favLbl.text = workLocation?.locationName ?? "Add Work"
                    tmpCell.addressLbl.text = workLocation?.locationAddress
                    tmpCell.optionBtn.isHidden = true
                    tmpCell.iconImage.image = #imageLiteral(resourceName: "ic_fav_work")
                    tmpCell.location = workLocation
                    tmpCell.accessoryType = .disclosureIndicator
                }
                
            } else {
                let otherLocations = (CacheManager.favouriteLocations.filter{
                    let locationNames = [String.Home, String.Work]
                    return !locationNames.contains($0.locationName ?? "")
                })
                let location = otherLocations[indexPath.row]
                tmpCell.location = location
                tmpCell.favLbl?.text = location.locationName
                tmpCell.addressLbl?.text = location.locationAddress
                tmpCell.iconImage?.image = #imageLiteral(resourceName: "rate")
            }
            tmpCell.iconImage?.imageThemeColor = 8
            tmpCell.didRequestToEditLocation = { [unowned self] location in
                let saveLocationVC = SaveLocationViewController.initFromStoryBoard(.Menu)
                saveLocationVC.locationInfo = location
                saveLocationVC.didRequestToSaveLocation = { updatedLocation in
                    if let location = updatedLocation {
                        self.updateFavouriteLocation(location.locationReq, id: location.id ?? NSNotFound, completion: { [unowned self] in
                            self.tableView?.reloadData()
                        })
                    }
                }
                self.present(saveLocationVC, animated: true, completion: nil)
            }
            
            tmpCell.didRequestToDeleteLocation = { [unowned self] location in
                self.deleteFavLocation(location, completion: { [unowned self] in
                    self.tableView?.reloadData()
                })
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let name = indexPath.row == 0 ? String.Home : String.Work
            let type = indexPath.row == 0 ? "1" : "2"
            let homeLocation = CacheManager.favouriteLocations.filter{$0.type == 1}.first
            let workLocation = CacheManager.favouriteLocations.filter{$0.type == 2}.first
            let id = indexPath.row == 0 ? homeLocation?.id ?? NSNotFound : workLocation?.id ?? NSNotFound
            self.showSearchViewController(name, type: type, id: id, isNeedToUpdate: false, searchCompletion: { [unowned self] (req) in
                let saveLocationVC = SaveLocationViewController.initFromStoryBoard(.Menu)
                saveLocationVC.locationInfo = req?.location
                saveLocationVC.locationInfo?.id = id
                saveLocationVC.didRequestToSaveLocation = { [unowned self] updatedLocation in
                    if let location = updatedLocation {
                        self.updateFavouriteLocation(location.locationReq, id: id, completion: { [unowned self] in
                            self.tableView?.reloadData()
                        })
                    }
                }
                self.present(saveLocationVC, animated: true, completion: nil)
            }) { (location) in }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: FavoriteHeader = UIView.loadFromNib(withName: "FavoriteHeader") ?? FavoriteHeader()
        let headings = ["Favourites".localized, "Other Saved Places".localized]
        header.heading?.text = headings[section]
        return header
    }
}

class FavouriteAddressCell1: UITableViewCell {
    
    @IBOutlet weak var favLbl: AppLabel!
    @IBOutlet weak var addressLbl: AppLabel!
    
    @IBOutlet weak var bottomBorder: AppLabel!
    @IBOutlet weak var iconImage: AppImageView!
    
}

class FavouriteAddressCell2: FavouriteAddressCell1 {
    
    @IBOutlet weak var optionBtn: AppButton!
    
    @IBAction func showOption(_ sender: AppButton) {
        dropDown.show()
    }
    var location: FavoriteLocation?
    var didRequestToEditLocation: ((FavoriteLocation?)->Void)?
    var didRequestToDeleteLocation: ((FavoriteLocation?)->Void)?
    lazy var dropDown: DropDown = {
        let dropDwn = DropDown()
        dropDwn.anchorView = optionBtn
        dropDwn.direction = .any
        dropDwn.backgroundColor = .white
        dropDwn.width = 120
        dropDwn.dataSource = ["Edit".localized, "Delete".localized]
        dropDwn.textFont = UIFont(name: "Montserrat-Regular", size: 14) ?? UIFont.systemFont(ofSize: 14)
        
        dropDwn.selectionAction = { [weak self] (index: Int, item: String) in
            if index == 0 {
                self?.didRequestToEditLocation?(self?.location)
            } else {
                self?.didRequestToDeleteLocation?(self?.location)
            }
            dropDwn.hide()
        }
        return dropDwn
    }()
    
}

extension UIViewController {
    func addFavouriteLocation(_ req: FavoriteLocationReqInfo,  completion: @escaping (() -> Void)) {
         self.startActivityIndicatorInWindow()
         ServiceManager().addFavouriteLocation(req, completion: { [weak self] (inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
                let response = try inner()
                self?.view.makeToast(response?.message?.stringValue)
                completion()
            } catch (_) {
                
            }
        })
    }
    
    func updateFavouriteLocation(_ req: FavoriteLocationReqInfo, id: Int,  completion: @escaping (() -> Void)) {
        self.startActivityIndicatorInWindow()
        ServiceManager().updateFavouriteLocation(req, id: id, completion: { [weak self] (inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
                let response = try inner()
                self?.view.makeToast(response?.message?.stringValue)
                completion()
            } catch (_) {
                
            }
        })
    }
}
