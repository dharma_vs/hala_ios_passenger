//
//  SettingsVC.swift
//  TaxiPickup
//
//  Created by TAMILARASAN on 27/01/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

class FavouriteView: UIView {
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var rightBtn: UIButton!
    @IBOutlet weak var rightLbl: UILabel!
    @IBOutlet weak var contentBtn: UIButton!
    
    var rightBtnAction: (() -> Void)!
    var contentBtnAction: (() -> Void)!
    
    var viewModel: FavouriteViewViewModel! {
        didSet {
            viewModel.updateFavouriteView(self)
        }
    }
    
    @IBAction func rightBtnPressed() {
        rightBtnAction?()
    }
    
    @IBAction func contentBtnPressed() {
        contentBtnAction?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentLbl?.numberOfLines = 0
    }
}

class SettingsViewController: BaseViewController {

    @IBOutlet weak var topConstraint1: NSLayoutConstraint!
    @IBOutlet weak var topConstraint2: NSLayoutConstraint!
    @IBOutlet weak var locationContentView: UIView!
    @IBOutlet weak var bottomContentView: UIView!
    
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var accountTitleLbl: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    
    @IBOutlet weak var favTitleLbl: UILabel!
    @IBOutlet weak var homeFavouriteView: FavouriteView!
    @IBOutlet weak var workFavouriteView: FavouriteView!
    
    @IBOutlet weak var otherPlaceButton: UIButton!
    @IBOutlet weak var languageView: FavouriteView!
    
    @IBOutlet weak var speechSwitch: UISwitch!
    
    @IBOutlet weak var lblDocuments: UILabel!
    @IBOutlet weak var lbldocVerifiled: UILabel!

    @IBAction func speechSwitch(_ sender: UISwitch) {
        CacheManager.sharedInstance.setObject(sender.isOn, key: .SpeechEnabled)
    }
    @IBAction func currencyBtnAction(_ sender: UIButton) {
        let languageVC = ChangeLanguageViewController.initFromStoryBoard(.Base)
        languageVC.isLanguageBool = false
        self.navigationController?.pushViewController(languageVC, animated: true)
    }
    @IBAction func cardBtnAction(_ sender: UIButton) {
//        let cardListVC = CardListViewController.initFromStoryBoard(.Payment)
//        self.navigationController?.pushViewController(cardListVC, animated: true)
    }
    @IBAction func btnclkDocument() {
        let cardListVC = UserDocumentVC.initFromStoryBoard(.Settings)
        self.navigationController?.pushViewController(cardListVC, animated: true)
    }
    var currencySymbol = ""
    var viewModel: SettingsViewModel! {
        didSet {
            self.viewModel.updateSettingViewController(self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        homeFavouriteView.viewModel = FavouriteViewViewModel(nil)
        workFavouriteView.viewModel = FavouriteViewViewModel(nil)
        languageView.viewModel = FavouriteViewViewModel(nil)
        
        if let riderInfo = CacheManager.riderInfo {
            self.viewModel = SettingsViewModel.init(riderInfo)
        }
        getFavouriteLocations()
        
        self.speechSwitch.tintColor = ColorTheme(rawValue: 3)?.color
        self.speechSwitch.onTintColor = ColorTheme(rawValue: 8)?.color

        let state = (CacheManager.sharedInstance.getObjectForKey(.SpeechEnabled) as? Bool) ?? true
        self.speechSwitch.setOn( state, animated: true)

    }
    override func viewWillAppear(_ animated: Bool) {
        
//        self.currencyLbl.text = "\((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currency_symbol ?? "")"
//        if ((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currencies) != nil {
//            for item in (((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currencies)!){
//                           print(item)
//                if "\(CacheManager.sharedInstance.getObjectForKey(.updatedCurrencySymbol) ?? "")" == "\(item.currency_symbol ?? "")"{
//                    self.currencyLbl.text = "\(item.currency_code ?? "")-\(item.currency_symbol ?? "")"
//                }
//            }
//        }
        getFavouriteLocations()
        super.viewWillAppear(animated)
    }
    override func setup() {
        super.setup()
    }
    
    override func localize() {
        super.localize()
        
        titleLbl.text = "Settings".localized
        accountTitleLbl.text = "Account".localized
        favTitleLbl.text = "Favourites".localized
        homeFavouriteView.contentLbl.text = "Add Home".localized
        workFavouriteView.contentLbl.text = "Add Work".localized
        otherPlaceButton.setAttributedTitle("View other saved places".localized.underlined, for: .normal)
        languageView.contentLbl.text = "Language".localized
        lblDocuments.text = "Documents".localized
        lbldocVerifiled.text = "Not Submit".localized
       // self.currencyTitleLbl.text = "Currency".localized
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == String.GotoChangeLanguageFromProfile {
             let languageVC = segue.destination as? ChangeLanguageViewController
            languageVC!.isLanguageBool = true
            (segue.destination as! ChangeLanguageViewController).didLanguageChange = { [weak self] in
                self?.speak(text: Speech.languageUpdate.rawValue)
                self?.languageView.viewModel.setLanguage()
            }
        } else if segue.identifier == String.GotoProfileFromSettings {
            (segue.destination as! ProfileViewController).didProfileUpdated = { [weak self] in
                if let riderInfo = CacheManager.riderInfo {
                    self?.viewModel = SettingsViewModel.init(riderInfo)
                }
                (self?.slideMenuController()?.leftViewController as! MenuViewController).refreshViewModel()
            }
        }
    }
    
    @IBAction func profileBtnPressed() {
        let profileVC = ProfileViewController.initFromStoryBoard(.Profile)
        profileVC.didProfileUpdated = { [weak self] in
            if let riderInfo = CacheManager.riderInfo {

            }
        }
        self.navigationController?.pushViewController(profileVC, animated: true)
//        self.pushViewController(profileVC, type: ProfileViewController.self, animated: true)
//        performSegue(withIdentifier: String.GotoProfileFromSettings, sender: self)
    }
    
    @IBAction func viewOtherPlaces(_ sender: UIButton) {
        let favLocationsVC = FavoriteLocationViewController.initFromStoryBoard(.Settings)
        self.navigationController?.pushViewController(favLocationsVC, animated: true)
    }
    func getFavouriteLocations() {
        self.startActivityIndicatorInWindow()
        ServiceManager().getAllFavouriteLocations {[weak self] (inner) -> (Void) in
            do {
                let locations = try inner()
                locations?.forEach({ [weak self] (favLocation) in
                    switch favLocation.locationName {
                    case String.Home:
                        self?.homeFavouriteView.viewModel.location = favLocation
                    case String.Work:
                        self?.workFavouriteView.viewModel.location = favLocation
                    default:
                        break
                    }
                })
            } catch (_) {
                
            }
            self?.stopActivityIndicator()
        }
    }
}

extension CacheManager {
    static var currency: AppCurrency {
        return "\((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currency_symbol ?? "")" == "FC" ? .FC : .USD
    }
}

enum AppCurrency: String {
    case FC
    case USD
}

extension Double {
    var convertedToUSD: Double {
        let settings =  CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings
        let rate = settings?.currencies?.filter{$0.currency_code == "USD"}.first?.rate?.doubleValue ?? 0
        return (self * rate).rounded()
    }
}
