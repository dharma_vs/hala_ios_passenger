//
//  ProductPaymentViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 08/03/22.
//

import Foundation

class ProductPaymentViewModel: AppViewModel {
    weak var controller: ProductPaymentViewController!
    required init(controller: ProductPaymentViewController) {
        self.controller = controller
    }
}

extension ServiceManager {
    
    func jPayment(_ info: JPaymentReq, completion: @escaping (_ inner: () throws ->  GeneralResponse<JPaymentRes>?) -> (Void)) {
        
        self.request(endPoint: PaymentDataServiceEndPoint.jPayment, body: info.JSONRepresentation ) { result in
            
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: GeneralResponse<JPaymentRes>.self)
                let checkMode = CacheManager.settings?.payment_mode?.stringValue == "Test"
                if response?.status == (checkMode == true ? false : true) {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
