//
//  PaymentTypeViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import Foundation
import UIKit

class PaymentTypeViewModel: NSObject {
    
    weak var controller: ProductPaymentViewController!
    let apiService: APIServiceCallProtocol
    
    var modelObject:PaymentTypeModel?
    var paymentTypeCellList:[PaymentTypeCellModel]?
    var selectedSubTypeIndex = -1
    var selectedPaymentSubType:AnyObject?
    var getCurrentPaymentSubTypeList:[PaymentSubType]?
    
    init(cvc:ProductPaymentViewController) {
        controller = cvc
        modelObject = PaymentTypeModel()
        apiService = Network()
    }
    func getPaymentTypeList() {
        apiService.getPaymentTypeList() { [weak self] (success, response, error) in
            guard let self = self else { return }
            if success == ResponseCode.success {
                guard let response = response else {
                    
                    return
                }
                self.modelObject?.paymentTypeList = response as? PaymentType
                if let list = self.modelObject?.paymentTypeList?.list{
                    self.groupCellViewModels(typeList: list)
                    self.paymentTypeCellList?[self.selectedPaymentType!].isSelected = true
                }
                
                self.controller.collectionPaytype.reloadData()
                self.controller.collectionPaytype.selectItem(at: IndexPath(item: self.selectedPaymentType!, section: 0), animated: true, scrollPosition: .left)
                
            } else {
                
            }
        }
    }
    var paymentTypeCellCount:Int{
        return paymentTypeCellList?.count ?? 0
    }
    private func groupCellViewModels(typeList: [PaymentTypeList]) {
        let list = typeList.map { (model) in
            return PaymentTypeCellModel(listItem: model)
        }
        self.paymentTypeCellList = list
    }
    func paymentTypeCellViewModelForRow(indexPath: IndexPath) -> PaymentTypeCellModel? {
        guard let cellVM = paymentTypeCellList, cellVM.count > indexPath.row else { return nil }
        return cellVM[indexPath.row]
    }
    //MARK: Payment Type Selection
    var selectedPaymentType:Int?{
        didSet{
            selectedSubTypeIndex = -1
            controller.searchTextField.text = ""
            controller.lblNoCard.isHidden = true
            switch selectedPaymentType {
            case 0:
                //Hala Wallet
                //controller.cardHeaderView.isHidden = true
                //controller.searchView.isHidden = true
                controller.halaWalletView.isHidden = false
                self.getPaymentSubTypeList(withText: "")
                controller.paymentSubTypeTbl.reloadData()
                self.loadHalaWallet()
            case 1:
                //Card Selected
                //controller.cardHeaderView.isHidden = false
                //controller.searchView.isHidden = true
                controller.halaWalletView.isHidden = true
                self.getPaymentSubTypeList(withText: "")
                controller.lblNoCard.textColor = UIColor.colorStatusNoDriver
                controller.lblNoCard.text = "No Card Found"
                controller?.lblNoCard?.isHidden = (modelObject?.getCardList().count ?? 0) > 0
                if self.getPaymentSubTypeTableRows == 0{
                    controller.lblNoCard.isHidden = false
                }
                controller.paymentSubTypeTbl.reloadData()
            case 2:
                //Netbanking Selected
                //controller.cardHeaderView.isHidden = true
               // controller.searchView.isHidden = false
                controller.halaWalletView.isHidden = true
                controller.searchTextField.placeHolerString = "Search Bank"
                controller.savedCardsLbl.text = "SELECT BANK"
                self.getPaymentSubTypeList(withText: "")
                controller.paymentSubTypeTbl.reloadData()
            case 3:
                //Wallet Selected
                //controller.cardHeaderView.isHidden = true
                //controller.searchView.isHidden = false
                controller.halaWalletView.isHidden = true
                controller.searchTextField.placeHolerString = "Search Wallet"
                controller.savedCardsLbl.text = "SELECT WALLET"
                self.getPaymentSubTypeList(withText: "")
                controller.paymentSubTypeTbl.reloadData()
            case 4:
                //UPI Selected
                //controller.cardHeaderView.isHidden = true
                //controller.searchView.isHidden = false
                controller.halaWalletView.isHidden = true
                controller.searchTextField.placeHolerString = "Search UPI"
                controller.savedCardsLbl.text = "SELECT UPI"
                controller.lblNoCard.textColor = UIColor.colorStatusNoDriver
                controller.lblNoCard.text = "No UPI App found"
                self.getPaymentSubTypeList(withText: "")
                if self.getCurrentPaymentSubTypeList?.count ?? 0 == 0{
                    controller.lblNoCard.isHidden = false
                }
                controller.paymentSubTypeTbl.reloadData()
            default:
                break
            }
        }
    }
    func loadHalaWallet(){
        let walletAmount = CacheManager.userInfo?.walletAmount?.doubleValue ?? 0
        controller.halaWalletBalanceLbl.text = Extension.updateCurrecyRate(fare: "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)").currencyValue
        
        
        if Double(walletAmount) < controller.amountToPay{
            controller.lblNoCard.textColor = UIColor.colorStatusCancelled
            controller.lblNoCard.text = "Insufficient Balance"
            controller.lblNoCard.isHidden = false
        }else{
            controller.lblNoCard.isHidden = true
        }
        
        //"\(CacheManager.userInfo?.walletAmount ?? 0)".currencyValue
    }
    func canPayWithHalaWalet()->Bool{
        let walletAmount = CacheManager.userInfo?.walletAmount?.doubleValue ?? 0
        
        return Double(walletAmount) < controller.amountToPay ? false : true
    }
    
    func canPayWithOnline()->(Bool,String){
        if selectedSubTypeIndex == -1{
            var msgString = ""
            if selectedPaymentType == 1{
                msgString = "Please select the card"
            }
            else if selectedPaymentType == 2{
                msgString = "Please select the bank"
            }else if selectedPaymentType == 3{
                msgString = "Please select the wallet"
            }
            else if selectedPaymentType == 4{
                msgString = "Please select the UPI Bank"
            }
            return (false,msgString)
        }else{
            return (true,"")
        }
    }
    //MARK:Payment SubList
    func getPaymentSubTypeList(withText:String){
        if selectedPaymentType == 2{
            getCurrentPaymentSubTypeList = modelObject?.getBankList()
        }else if selectedPaymentType == 3{
            getCurrentPaymentSubTypeList = modelObject?.getWalletList()
        }else if selectedPaymentType == 4{
            getCurrentPaymentSubTypeList = modelObject?.getUPIList(controller: controller)
        }
        
        if withText != ""{
            let filtered = getCurrentPaymentSubTypeList?.filter({ (paymentSubtype) -> Bool in
                return (paymentSubtype.subTypeName.uppercased()).contains(withText.uppercased())
            })
            getCurrentPaymentSubTypeList = filtered
        }
    }
    var getPaymentSubTypeTableRows:Int{
        
        switch selectedPaymentType {
        case 0:
            return 0
        case 1:
            return modelObject?.getCardList().count ?? 0
        case 2:
            return self.getCurrentPaymentSubTypeList?.count ?? 0
        case 3:
            return self.getCurrentPaymentSubTypeList?.count ?? 0
        case 4:
            print("Nothing")
            /*controller?.showAlertWithMessage(controller.razorpay?.getSupportedUPIApps().joined(separator: "--") ?? "", phrase1: "Yes".localized, phrase2: "Cancel".localized, skipDismiss: false, action: UIAlertAction.Style.default, .cancel, completion: { [weak self] (style) in
                if style == .default {
                    print("User decided to buy")
                    
                }
            })*/
            return modelObject?.getUPIList(controller: controller).count ?? 0
        default:
            return 0
        }
    }
    func getPaymentSubTypeCellModel(row:Int)->PaymentSubType?{
        switch selectedPaymentType {
        
        case 2:
            return getCurrentPaymentSubTypeList?[row]
        case 3:
            return getCurrentPaymentSubTypeList?[row]
        case 4:
            return getCurrentPaymentSubTypeList?[row]
        default:
            return nil
        }
    }
    
    func getPaymentSubTypeCardCellModel(row:Int)->Card_info?{
        return modelObject?.getCardList()[row]
    }
    
}
