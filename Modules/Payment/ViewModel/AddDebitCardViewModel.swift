//
//  AddDebitCardViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 07/03/22.
//

import Foundation

class AddDebitCardViewModel: AppViewModel {
    weak var controller: AddDebitCardViewController!
    required init(controller: AddDebitCardViewController) {
        self.controller = controller
    }
    
    func addCard() {
        if (self.controller.cardHolderNameTxt.text?.replacingOccurrences(of: " ", with: ""))?.count == 0{
            self.controller.view.makeToast("Please enter name")
        }else if (self.controller.cardNumberTxt.text?.replacingOccurrences(of: " ", with: ""))?.count ?? 0 < 14{
            self.controller.view.makeToast("Please enter valid card number")
        }else if (self.controller.cvcTxt.text?.replacingOccurrences(of: " ", with: ""))?.count ?? 0 < 3{
            self.controller.view.makeToast("Please enter valid CVV")
        }else if (self.controller.expirationDateTxt.text?.replacingOccurrences(of: " ", with: ""))?.count ?? 0 == 0 /*|| expirationDateTxt.errorMessage != ""*/{
            self.controller.view.makeToast("Please enter valid expiry date")
        }else{
            self.controller.startActivityIndicator()
            //API Call here
            let token = UUID().uuidString
            let options = ["registration_id" : token,
                           "brand" :  "Visa",
                           "card_name" :  self.controller.cardHolderNameTxt.text ?? "",
                           "card_no" : self.controller.cardNumberTxt.text?.replacingOccurrences(of: " ", with: "") ?? "",
                           "card_expiry_date" : self.controller.expirationDateTxt.text?.replacingOccurrences(of: "/", with: "20") ?? "",
                           "cvv" : self.controller.cvcTxt.text ?? ""]
            ServiceManager().addCard(dic: options as NSDictionary) { [weak self] (inner) -> (Void) in
                do {
                    let response = try inner()
                    self?.controller.view.makeToast(response?.message?.stringValue)
                    let info = newCard()
                    info.card_no = self?.controller.cardNumberTxt.text?.replacingOccurrences(of: " ", with: "") ?? ""
                    info.card_name = self?.controller.cardHolderNameTxt.text ?? ""
                    info.expiry_date = self?.controller.expirationDateTxt.text?.replacingOccurrences(of: "/", with: "20") ?? ""
                    info.cvv = self?.controller.cvcTxt.text ?? ""
                    info.brand = self?.controller.selectedBrand
                    info.registration_id = token
                    self?.controller.delegate?.newCardAdded(info: info)
                    self?.controller.navigationController?.popViewController(animated: true)
                } catch {
                    self?.controller.view.makeToast((error as? CoreError)?.description)
                }
                self?.controller.stopActivityIndicator()
            }
        }
    }
}


extension ServiceManager {
    func addCard(dic: NSDictionary, completion: @escaping (_ inner: () throws ->  CommonRes?) -> Void) {
        self.request(endPoint: PaymentDataServiceEndPoint.cards, body: (dic as? Dictionary<String, Any>) ?? [String: Any](), pathSuffix: nil) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response  = data.getDecodedObject(from: CommonRes.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func addMoneyToWallet(_ amount: Double, transactionId: String = "", completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        self.request(endPoint: PaymentDataServiceEndPoint.addMoneyToWallet, body: ["amount": amount, "transaction_id": transactionId]) { [weak self] (result) in
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: CommonRes.self)
                self?.getProfile({ (inner) -> (Void) in  })
                if response?.status?.isSuccess == true {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message?.stringValue ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
