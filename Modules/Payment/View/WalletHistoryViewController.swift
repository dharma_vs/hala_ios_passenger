//
//  WalletHistoryViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit
import DropDown



enum WalletFilter: String {
    
    case DateWise = "Date Wise"
    
    case RangeWise = "Range Wise"
    
    static var all:[WalletFilter] {
        return [.DateWise, .RangeWise]
    }
}

class WalletHistoryViewController: BaseViewController {
    
    @IBOutlet weak var titleLbl: AppLabel!
   // @IBOutlet weak var dateWiseContentView: UIView!
  //  @IBOutlet weak var rangeWiseContentView: UIView!
    @IBOutlet weak var historyTblView: UITableView!
   // @IBOutlet weak var fromDateBtn: UIButton!
  //  @IBOutlet weak var toDateBtn: UIButton!
  //  @IBOutlet weak var selectedDateBtn: UIButton!
 //   @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var toLbl: AppLabel!
    @IBOutlet weak var noResultLbl: AppLabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var rechargeBtn: AppButton!
    @IBOutlet weak var walletAmountLbl: AppLabel!

    @IBOutlet weak var scrollView: UIView!
    
    var hasMoreData = true
    var isFromDashBoard = Bool()
    @IBOutlet weak var walletAmountStackView: UIStackView!

    @IBOutlet var headerViewHeightConstraint: NSLayoutConstraint!
   
    
    var selectedFilter: WalletFilter = .DateWise {
        didSet {
          
            self.historyData.removeAll()
            self.loadData()
        }
    }
    
    var selectedDate: Date! {
        didSet {
            self.hasMoreData = true
            self.historyData.removeAll()
//            self.loadData()
        }
    }
    
    var fromDate: Date!
    var toDate: Date!
    
    var paginator: Paginator<WalletHistoryInfo>!
    var historyData = [WalletHistoryInfo]() {
        didSet {
            self.historyTblView?.reloadData()
        }
    }
    
    func loadData(currentPage: Int = 0) {
        var params = WalletParams()
        params.start_date = "2019-12-01" //selectedFilter == .DateWise ? selectedDate.dateFromFormater("yyyy-MM-dd") : fromDate.dateFromFormater("yyyy-MM-dd")
        params.end_date = Date().dateFromFormater("yyyy-MM-dd")//selectedFilter == .DateWise ? selectedDate.dateFromFormater("yyyy-MM-dd") : Date().dateFromFormater("yyyy-MM-dd")
        params.currentPage = "\(currentPage)"
        params.pageSize = "\(pageSize)"
        
        self.activityIndicator.startAnimating()
        ServiceManager().walletHistory(params) {[weak self] (inner) -> (Void) in
            self?.activityIndicator.stopAnimating()
            do {
                self?.paginator = try inner()
                if let rideList = self?.paginator?.result {
                    self?.historyData.append(contentsOf: rideList)
                }
                self?.hasMoreData = !((self?.paginator?.result?.count ?? 0) < pageSize)
//                self?.walletAmountLbl.text = Extension.updateCurrecyRate(fare: "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)").currencyValue
                self?.historyTblView.isHidden = self?.historyData.count == 0
                self?.noResultLbl.isHidden = !(self?.historyData.count == 0)
            } catch {
                
            }
        }
    }
    @IBAction func rechargeBtnAction(_ sender: Any) {
        let walletVC = WalletAmountVC.initFromStoryBoard(.Payment)
        self.navigationController?.pushViewController(walletVC, animated: true)
    }
    @IBAction func selectDate(_ sender: Any) {
        let dateSelectionVC = Router.main.instantiateViewController(withIdentifier: "DateSelectionViewController")
        (dateSelectionVC as? DateSelectionViewController)?.didSelectDate = {[weak self] date in
            self?.selectedDate = date
           // self?.selectedDateBtn.setTitle(self?.selectedDate.dateFromFormater("yyyy-MM-dd"), for: .normal)
            self?.loadData()
            self?.dismiss(animated: true, completion: nil)
        }
        dateSelectionVC.modalPresentationStyle = .overCurrentContext
        self.present(dateSelectionVC, animated: true, completion: nil)
    }
    
    @IBAction func chooseFromDate(_ sender: Any) {
        let dateSelectionVC = Router.main.instantiateViewController(withIdentifier: "DateSelectionViewController")
        (dateSelectionVC as? DateSelectionViewController)?.didSelectDate = {[weak self] date in
            self?.fromDate = date
            self?.hasMoreData = true
            self?.historyData.removeAll()
            self?.loadData()
          //  self?.fromDateBtn.setTitle(self?.fromDate.dateFromFormater("yyyy-MM-dd"), for: .normal)
            self?.dismiss(animated: true, completion: nil)
        }
        dateSelectionVC.modalPresentationStyle = .overCurrentContext
        self.present(dateSelectionVC, animated: true, completion: nil)
    }
    
    @IBAction func chooseToDate(_ sender: Any) {
        let dateSelectionVC = Router.main.instantiateViewController(withIdentifier: "DateSelectionViewController")
        (dateSelectionVC as? DateSelectionViewController)?.didSelectDate = {[weak self] date in
            self?.toDate = date
            self?.hasMoreData = true
            self?.historyData.removeAll()
            self?.loadData()
          //  self?.toDateBtn.setTitle(self?.toDate.dateFromFormater("yyyy-MM-dd"), for: .normal)
            self?.dismiss(animated: true, completion: nil)
        }
        dateSelectionVC.modalPresentationStyle = .overCurrentContext
        self.present(dateSelectionVC, animated: true, completion: nil)
    }
    
    @IBAction func moveToNextDate(_ sender: Any) {
        self.selectedDate = self.selectedDate.addingTimeInterval(60*60*24)
        //self.selectedDateBtn.setTitle(self.selectedDate.dateFromFormater("yyyy-MM-dd"), for: .normal)
        self.loadData()
    }
    
    @IBAction func moveToPreviousDate(_ sender: Any) {
        self.selectedDate = self.selectedDate.addingTimeInterval(-60*60*24)
        //self.selectedDateBtn.setTitle(self.selectedDate.dateFromFormater("yyyy-MM-dd"), for: .normal)
    }
    
    @IBAction func showFliter(_ sender: Any) {
        let dropDown: DropDown = {
               let dropDwn = DropDown()
               dropDwn.anchorView = self.scrollView
               dropDwn.direction = .bottom
               dropDwn.backgroundColor = .white
               dropDwn.selectionBackgroundColor = .white
               dropDwn.width = 150
            
              
               dropDwn.textFont = UIFont(name: "Montserrat-SemiBold", size: 13) ?? UIFont.systemFont(ofSize: 13)
               
               dropDwn.selectionAction = { [unowned self] (index: Int, item: String) in
                   let filter = item == "Range Wise" ? WalletFilter.all[1] : WalletFilter.all[0]
                   self.selectedFilter = filter
                   dropDwn.hide()
               }
               return dropDwn
           }()
        dropDown.show()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let date = Date().dateFromFormater("yyyy-MM-dd")?.dateWithFormat("yyyy-MM-dd")
        self.selectedDate = date
        self.fromDate = Date().startOfMonth()
        self.toDate = Date().endOfMonth()
      
        self.selectedFilter = .DateWise
        // Do any additional setup after loading the view.
        self.rechargeBtn.isHidden = false

//        if CacheManager.driverCompleteInfo?.company_info != nil {
//            self.rechargeBtn.isHidden = (CacheManager.settings?.minimum_balance?.doubleValue ?? 0.0) <= Double(CacheManager.driverCompleteInfo?.wallet_amount ?? 0)
//        }
        if isFromDashBoard {
            self.walletAmountLbl.isHidden = true
            headerViewHeightConstraint.constant = 120
            walletAmountStackView.isHidden = true
        }
       
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
//        self.walletAmountLbl.text = Extension.updateCurrecyRate(fare: "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)").currencyValue
        
      //  self.walletAmountLbl.text = "$ \(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)"
        
//        self.historyData.removeAll()
//        self.loadData()
    }
    
}

extension WalletHistoryViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.historyData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletHistoryTableViewCell", for: indexPath) as! WalletHistoryTableViewCell
        cell.selectionStyle = .none

        let historyInfo = self.historyData[indexPath.row]
        cell.statusLbl.text = historyInfo.rideID == "0" ? "Recharged".localized : "On Ride".localized
        cell.walletImg.image = UIImage(named: historyInfo.rideID == "0" ? "recharge" : "Tripwallet")
        cell.walletImg.tintColor = historyInfo.rideID == "0" ? ColorTheme.colorPrimaryDark.color : UIColor.colorOffline
        cell.statusLbl.textColor = historyInfo.rideID == "0" ? ColorTheme.colorPrimaryDark.color : UIColor.colorOffline
        cell.numberLbl.text = historyInfo.rideID == "0" ? historyInfo.transactionID : historyInfo.bookingID
        
        cell.amountLbl.text = Extension.updateCurrecyRate(fare: "\(historyInfo.amount ?? 0.0)").currencyValue
       // cell.amountLbl.text = "\(historyInfo.amount ?? 0.0)".currencyValue
        cell.amountLbl.textColor = historyInfo.rideID == "0" ? ColorTheme.colorPrimaryDark.color : UIColor.colorOffline
        cell.dateLbl.text = historyInfo.paymentAt?.UTCToLocal1("yyyy-MM-dd HH:mm:ss", formatTo:"dd-MM-yyyy hh:mm a")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let historyInfo = self.historyData[indexPath.row]
        if historyInfo.rideID != "0" {
            
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            if let vc = storyBoard.instantiateViewController(withIdentifier: "TripDetailsViewController") as? TripDetailsViewController
            {
                vc.rideId = historyInfo.rideID ?? "0"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == historyTblView {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) && hasMoreData {
                self.loadData(currentPage: (self.paginator?.currentPage?.intValue() ?? 0) + 1)
            }
        }
    }
}

class WalletHistoryTableViewCell: UITableViewCell {
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var walletImg: UIImageView!

}

struct WalletHistoryInfo: JSONSerializable {
    let id: Int?
    let rideID, bookingID, type, transactionID: String?
    let paymentMode: String?
    let amount: Double?
    let paymentAt, countryShortName: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case rideID = "ride_id"
        case bookingID = "booking_id"
        case type
        case transactionID = "transaction_id"
        case paymentMode = "payment_mode"
        case amount
        case paymentAt = "payment_at"
        case countryShortName = "country_short_name"
    }
}

class WalletHistoryResponse: Paginator<WalletHistoryInfo> {
    
}

struct WalletParams: JSONSerializable {
    var start_date: String!
    var end_date: String!
    var pageSize: String!
    var currentPage: String!
}

extension ServiceManager {
    func walletHistory(_ params: WalletParams, completion: @escaping (_ inner: () throws ->  Paginator<WalletHistoryInfo>?) -> (Void)) {
        self.request(endPoint: MenuDataServiceEndpoint.walletHistory, parameters: nil, body: nil, pathSuffix: nil) { result in
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: GeneralResponse<WalletHistoryResponse>.self)
                if let data = response?.data, response?.status == true {
                    completion({ return data})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
