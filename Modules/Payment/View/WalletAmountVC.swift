//
//  WalletAmountVC.swift
//  FleetManager
//
//  Created by apple on 16/04/20.
//  Copyright © 2020 TAMILARASAN. All rights reserved.
//

import UIKit

class WalletAmountVC: BaseViewController,UITextFieldDelegate {

    @IBOutlet weak var walletBalanceLbl: UILabel!
    @IBOutlet weak var rechargeAmountLbl: UILabel!
    @IBOutlet weak var addMoneyLbl: UILabel!

    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var amount1Btn: UIButton!
    @IBOutlet weak var amount2Btn: UIButton!
    @IBOutlet weak var amount3Btn: UIButton!

    @IBOutlet weak var walletBGView: UIView!

    @IBOutlet var addWalletAmountTxt: PaddableTextField!
    var balAmount = ""
    var driverID = ""

    override func viewDidLoad() {
        
        super.viewDidLoad()
        walletBGView.layer.cornerRadius = 10
        walletBGView.layer.borderColor = UIColor.lightGray.cgColor
        walletBGView.layer.borderWidth = 0.6
        addWalletAmountTxt.delegate = self
        addWalletAmountTxt.keyboardType = .decimalPad

    }
    override func viewWillAppear(_ animated: Bool) {
        
        rechargeAmountLbl.text =   Extension.updateCurrecyRate(fare: "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)").currencyValue
            
            
         //   "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)".currencyValue
        addWalletAmountTxt.text = ""

        super.viewWillAppear(true)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location == 5 { return false }

        let numberOnly = NSCharacterSet.init(charactersIn: "-+0123456789.")
        let stringFromTextField = NSCharacterSet.init(charactersIn: string)
        let strValid = numberOnly.isSuperset(of: stringFromTextField as CharacterSet)
        
        guard let oldText = textField.text, let r = Range(range, in: oldText) else {
            return true
        }
        let updatedText = oldText.replacingCharacters(in: r, with: string)


        return strValid && (updatedText.filter{$0 == "."}.count <= 1)
     }

    @IBAction func btnclk_Submit(sender:UIButton) {
        if addWalletAmountTxt.text != ""{
            let paymentContactVC = Router.payment.instantiateViewController(withIdentifier: "PaymentContactViewController") as! PaymentContactViewController
            paymentContactVC.amountToPay = (Double(addWalletAmountTxt.text ?? "0") ?? 0)
            paymentContactVC.isFromWallet = true
            self.navigationController?.pushViewController(paymentContactVC, animated: true)        }
        //            let paymentVC = Router.main.instantiateViewController(withIdentifier: StoryboardIDs.ProductPaymentViewController) as! ProductPaymentViewController
//            paymentVC.amountToPay = Double(addWalletAmountTxt.text ?? "0") ?? 0
//            paymentVC.isFromWallet = true
//            self.navigationController?.pushViewController(paymentVC, animated: true)        }
    }
    @IBAction func walletAmountAction(_ sender: UIButton) {
        if sender.tag == 1 {
            addWalletAmountTxt.text = "10"
            
        }else   if sender.tag == 2 {
            addWalletAmountTxt.text = "50"
            
        }else {
            if sender.tag == 3 {
                addWalletAmountTxt.text = "100"
            }
        }
    }
  
    @IBAction func backbtnAction(sender:UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
   
}


class PaymentContactInfo: NSObject, JSONSerializable, NSCoding {
    var billingName: CustomType?
    var street: CustomType?
    var city: CustomType?
    var state: CustomType?
    var country: CustomType?
    var phone: CustomType?
    var email: CustomType?
    
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        billingName = aDecoder.decodeObject(forKey: "billingName") as? CustomType
        street = aDecoder.decodeObject(forKey: "street") as? CustomType
        city = aDecoder.decodeObject(forKey: "city") as? CustomType
        state = aDecoder.decodeObject(forKey: "state") as? CustomType
        country = aDecoder.decodeObject(forKey: "country") as? CustomType
        phone = aDecoder.decodeObject(forKey: "phone") as? CustomType
        email = aDecoder.decodeObject(forKey: "email") as? CustomType
    }
}

extension String {
    var custom: CustomType? {
        return CustomType(value: self as AnyObject)
    }
}
extension Int {
    var custom: CustomType? {
        return CustomType(value: self as AnyObject)
    }
}
extension Double {
    var custom: CustomType? {
        return CustomType(value: self as AnyObject)
    }
}
extension Float {
    var custom: CustomType? {
        return CustomType(value: self as AnyObject)
    }
}
