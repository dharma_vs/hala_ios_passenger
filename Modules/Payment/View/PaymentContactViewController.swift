//
//  PaymentContactViewController.swift
//  TaxiPickup
//
//  Created by Admin on 24/01/22.
//  Copyright © 2022 PPD Solutions. All rights reserved.
//

import UIKit

class PaymentContactViewController: BaseViewController {
    
    var amountToPay:Double = 0
    var isFromWallet = false
    
    /*
    var showAdd = "N"
    var useCCPromo = "N"
    var validateAdd = "N"
    var merchantId = "45990"
    var accessCode = "AVXI04JA21AY17IXYA"
    var encryptionKey = "11B8D36DC99A2A73243972F6024D6E02"
    var rsa_url = "https://dev.virtuesense.com/jeffery/web/payment_test/GetRSA.php"
    var redirect_url = "https://dev.virtuesense.com/jeffery/web/payment_test/ccavResponseHandler.php"
    var cancel_url = "https://dev.virtuesense.com/jeffery/web/payment_test/ccavResponseHandler.php"
    var promocode = ""
    
    var amountToPay: Double = 0
    
    var isFromWallet = false
    var isFromDashboard = false
    
    var paymentVC = CCAvenuePaymentController()
    
    @IBOutlet weak var billingName: InputView!
    @IBOutlet weak var street: InputView!
    @IBOutlet weak var city: InputView!
    @IBOutlet weak var state: InputView!
    @IBOutlet weak var country: InputView!
    @IBOutlet weak var phone: InputView! {
        didSet {
            self.phone?.inputTextField?.delegate = self
        }
    }
    @IBOutlet weak var email: InputView!
    
    @IBOutlet weak var jeffreyPaymentLbl: UILabel!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    
    var viewModel: PaymentContactViewModel = PaymentContactViewModel()

    var delegate:PaymentCompleteDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.totalAmountLbl?.text = self.amountToPay.intValue.stringValue.usdValue
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI() {
        billingName?.inputTextField?.placeholder = "Billing Name".localized
        street?.inputTextField?.placeholder = "Street".localized
        city?.inputTextField?.placeholder = "City".localized
        state?.inputTextField?.placeholder = "State".localized
        country?.inputTextField?.placeholder = "Country".localized
        phone?.inputTextField?.placeholder = "Phone".localized
        email?.inputTextField?.placeholder = "Email".localized
        
        let userInfo = CacheManager.userInfo
        let contactInfo = CacheManager.paymentContactInfo
        billingName?.inputTextField?.text = contactInfo?.billingName?.stringValue ?? userInfo?.name
        street?.inputTextField?.text = contactInfo?.street?.stringValue
        city?.inputTextField?.text = contactInfo?.city?.stringValue
        state?.inputTextField?.text = contactInfo?.state?.stringValue
        country?.inputTextField?.text = contactInfo?.country?.stringValue
        phone?.inputTextField?.text = contactInfo?.phone?.stringValue ?? userInfo?.phone ?? userInfo?.phoneNo
        email?.inputTextField?.text = contactInfo?.email?.stringValue ?? userInfo?.email
    }

    @IBAction func proceedWithPayment(_ sender: Any) {
        guard validateInputs() else {
            return
        }
        let contactInfo = PaymentContactInfo()
        contactInfo.billingName = billingName.inputTextField.text?.custom
        contactInfo.street = street.inputTextField.text?.custom
        contactInfo.city = city.inputTextField.text?.custom
        contactInfo.state = state.inputTextField.text?.custom
        contactInfo.country = country.inputTextField.text?.custom
        contactInfo.phone = phone.inputTextField.text?.custom
        contactInfo.email = email.inputTextField.text?.custom
        CacheManager.paymentContactInfo = contactInfo
        let user = CacheManager.userInfo
        let settings =  CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings
        let rate = settings?.currencies?.filter{$0.currency_code == "USD"}.first?.rate?.doubleValue ?? 0
        paymentVC = CCAvenuePaymentController(orderId:"\((arc4random() % 9999999) + 1)",
                                                               merchantId:merchantId ,
                                                               accessCode:accessCode,
                                                               custId:user?.id?.stringValue ?? "",
                                                               amount:self.amountToPay.stringValue,
                                                               currency:"AED",
                                                               rsaKeyUrl:rsa_url,
                                                               redirectUrl:redirect_url,
                                                               cancelUrl:cancel_url,
                                                               showAddress:"Y",
                                                               billingName:billingName.inputTextField.text,
                                                               billingAddress:street.inputTextField.text,
                                                               billingCity:city.inputTextField.text,
                                                               billingState:state.inputTextField.text,
                                                               billingCountry:country.inputTextField.text,
                                                               billingTel:phone.inputTextField.text,
                                                               billingEmail:email.inputTextField.text,
                                                               deliveryName:billingName.inputTextField.text,
                                                               deliveryAddress:street.inputTextField.text,
                                                               deliveryCity:city.inputTextField.text,
                                                               deliveryState:state.inputTextField.text,
                                                               deliveryCountry:country.inputTextField.text,
                                                               deliveryTel:phone.inputTextField.text,
                                                               promoCode:promocode,
                                                               merchant_param1:"Param 1",
                                                               merchant_param2:"Param 2",
                                                               merchant_param3:"Param 3",
                                                               merchant_param4:"Param 4",
                                                               merchant_param5:"Param 5",
                                                               useCCPromo:"Y")
        paymentVC.delegate = self;
        paymentVC.modalPresentationStyle = .fullScreen
        self.present(paymentVC, animated: true, completion: nil);
    }
    
    private func validateInputs() -> Bool {
        
        if billingName?.inputTextField?.text?.isEmpty == true || billingName?.inputTextField?.text?.isEmpty == true {
            self.view.makeToast("Please enter \((billingName?.inputTextField?.placeholder ?? "").localized())")
            return false
        }
        if street?.inputTextField?.text?.isEmpty == true || street?.inputTextField?.text?.isEmpty == true {
            self.view.makeToast("Please enter \((street?.inputTextField?.placeholder ?? "").localized())")
            return false
        }
        if city?.inputTextField?.text?.isEmpty == true || city?.inputTextField?.text?.isEmpty == true {
            self.view.makeToast("Please enter \((city?.inputTextField?.placeholder ?? "").localized())")
            return false
        }
        if state?.inputTextField?.text?.isEmpty == true || state?.inputTextField?.text?.isEmpty == true {
            self.view.makeToast("Please enter \((state?.inputTextField?.placeholder ?? "").localized())")
            return false
        }
        if country?.inputTextField?.text?.isEmpty == true || country?.inputTextField?.text?.isEmpty == true {
            self.view.makeToast("Please enter \((country?.inputTextField?.placeholder ?? "").localized())")
            return false
        }
        if phone?.inputTextField?.text?.isEmpty == true || phone?.inputTextField?.text?.isEmpty == true {
            self.view.makeToast("Please enter \((phone?.inputTextField?.placeholder ?? "").localized())")
            return false
        }
        if email?.inputTextField?.text?.isEmpty == true || email?.inputTextField?.text?.isEmpty == true {
            self.view.makeToast("Please enter \((email?.inputTextField?.placeholder ?? "").localized())")
            return false
        }
        
        if email?.inputTextField?.text?.isValidEmail() == false {
            self.view.makeToast("Please enter valid email address".localized)
            return false
        }
        
        return true
        
    }
 */
}

extension PaymentContactViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == phone.inputTextField {
//            let text = textField.text?.replacingCharacters(in: Range(range, in: textField.text!)!, with: string)
//            let isNumber = text?.isNumber == true
//            if isNumber {
//                return (text?.count ?? 0) <= 9
//            }
//        }
        return true
    }
}

class PaymentContactViewModel {
    func proceedWithPayment() {
        
    }
}

class InputView: UIView {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var inputTextField: AppTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.inputTextField?.textThemeColor = 19
        self.inputTextField?.placeHolderThemeColor = 23
    }
}

/*
extension PaymentContactViewController: CCAvenuePaymentControllerDelegate {
    func getResponse(_ responseDict_: NSMutableDictionary!) {
        let orderNum = "\(responseDict_["order_id"] ?? "")"
        let orderStatus = "\(responseDict_["order_status"] ?? "")"
        let bankReceiptNO = "\(responseDict_["bank_receipt_no"] ?? "")"
        let orderTrackId = "\(responseDict_["tracking_id"] ?? "")"
        if orderStatus == "Success" {
            let transactionId = bankReceiptNO+"_"+orderTrackId
            if isFromDashboard {
                delegate?.paymentCompletedNotification(token: transactionId)
            } else if isFromWallet {
                self.startActivityIndicatorInWindow()
                let settings =  CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings
                let rate = settings?.currencies?.filter{$0.currency_code == "USD"}.first?.rate?.doubleValue ?? 0
                ServiceManager().addMoneyToWallet(Int(Double(self.amountToPay)/rate) , transactionId: transactionId) { [weak self] (inner) -> (Void) in
                    do {
                        let response = try inner()
                        self?.view.makeToast(response?.message)
                        self?.showAlertWith("Payment Succeeded", title: "Success", completion: { [weak self] (style) -> (Void) in
                            self?.dismiss(animated: false) { [weak self] in
                                self?.navigationController?.popToRootViewController(animated: true)
                            }
                        })
                    } catch {
                        self?.view.makeToast((error as? CoreError)?.description)
                    }
                    self?.stopActivityIndicator()
                }
            } else {
                let msg = "Your order # \(orderNum) is \(orderStatus)\n\nPayment Reference Number : \(orderTrackId)\n\nYou may use this number for any future communications.";
                
                let alert = UIAlertController(title: "Payment Status", message: msg, preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "Done", style: UIAlertAction.Style.default, handler: nil));
                
                self.present(alert, animated: true, completion: nil);
            }
        } else {
            let status_message = "\(responseDict_["status_message"] ?? responseDict_["order_status"] ?? "")"
            self.showAlertWith(status_message)
        }
    }
}


extension CacheManager {
    static var paymentContactInfo: PaymentContactInfo? {
        get {
            return CacheManager.sharedInstance.getObjectForKey(.BillingInfo) as? PaymentContactInfo
        }
        set {
            CacheManager.sharedInstance.setObject(newValue, key: .BillingInfo)
            CacheManager.sharedInstance.saveCache()
        }
    }
}
*/
extension Double {
    var intValue: Int {
        return Int(self)
    }
}
