//
//  PaymentCardListCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import UIKit

class PaymentCardListCell: UITableViewCell {

    @IBOutlet weak var indicator: AppImageView!
    @IBOutlet weak var cardType: UIImageView!
    @IBOutlet weak var cardHolderName: AppLabel!
    @IBOutlet weak var cardNumber: AppLabel!
    @IBOutlet weak var cardExpiryDate: AppLabel!
    var cellViewModel:Card_info?{
        didSet{
            if let cardInfo = cellViewModel{
                cardHolderName.text = cardInfo.card_name
                cardNumber.text = "XXXX-XXXX-XXXX-\(cardInfo.card_no?.suffix(4) ?? "")"
                let expiry = cardInfo.expiry_date ?? ""
                let month = expiry.prefix(2)
                let year = expiry.suffix(4)
                cardExpiryDate.text = "Expires: \(month)/\(year)"
                //print(cardInfo.brand)
                let cardBrand = cardInfo.brand ?? ""
                switch cardBrand.uppercased() {
                case "VISA":
                    cardType.image = UIImage(named: "ic_type_visa")
                case "MASTERCARD","MASTER":
                    cardType.image = UIImage(named: "ic_type_master")
                case "MASTERO":
                    cardType.image = UIImage(named: "ic_type_mastero")
                case "DISCOVER":
                    cardType.image = UIImage(named: "ic_type_discover")
                default:
                    break
                }
            }
        }
    }
    var isCellSelected:Bool=false{
        didSet{
            if isCellSelected{
                indicator.image = UIImage(named: "ic_card_select")
            }else{
                indicator.image = UIImage(named: "ic_card_not_select")
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
