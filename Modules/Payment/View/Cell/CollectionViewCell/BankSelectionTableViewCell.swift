//
//  BankSelectionTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import UIKit

class BankSelectionTableViewCell: UITableViewCell {
    @IBOutlet weak var leftBorder: AppLabel!
    @IBOutlet weak var rightBorder: AppLabel!
    @IBOutlet weak var bottomBorder: AppLabel!
    @IBOutlet weak var topBorder: AppLabel!
    @IBOutlet weak var bankName: AppLabel!
    @IBOutlet weak var selectionIndicator: UIImageView!
    @IBOutlet weak var bankImg: UIImageView!
    @IBOutlet weak var bankImgWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bankImgTrailingConstraint: NSLayoutConstraint!
    
    var cellViewModel:PaymentSubType?{
        didSet{
            if let model = cellViewModel{
                bankName.text = model.subTypeName
                if model.subTypeImg != ""{
                    bankImg.isHidden = false
                    bankImg.image = UIImage(named: model.subTypeImg)
                    self.bankImgWidthConstraint.constant = 25
                    self.bankImgTrailingConstraint.constant = 10
                }else{
                    bankImg.isHidden = true
                    self.bankImgWidthConstraint.constant = 0
                    self.bankImgTrailingConstraint.constant = 0
                }
            }
        }
    }
    var isCellSelected:Bool?{
        didSet{
            if let status = isCellSelected{
                if status{
                    leftBorder.backgroundColor = UIColor.gStartColor
                    rightBorder.backgroundColor = UIColor.gStartColor
                    topBorder.backgroundColor = UIColor.gStartColor
                    bottomBorder.backgroundColor = UIColor.gStartColor
                    bottomBorder.isHidden = false
                    selectionIndicator.isHidden = false
                }else{
                    leftBorder.backgroundColor = UIColor.colorLightGray_2
                    rightBorder.backgroundColor = UIColor.colorLightGray_2
                    topBorder.backgroundColor = UIColor.colorLightGray_2
                    bottomBorder.backgroundColor = UIColor.colorLightGray_2
                    bottomBorder.isHidden = true
                    selectionIndicator.isHidden = true
                }
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
