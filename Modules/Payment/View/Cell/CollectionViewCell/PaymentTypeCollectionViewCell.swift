//
//  PaymentTypeCollectionViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import UIKit

class PaymentTypeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cellBgView: TpView!
    @IBOutlet weak var paymentTypeImg: AppImageView!
    @IBOutlet weak var paymentTypeName: AppLabel!
    @IBOutlet weak var selectIndicator: AppImageView!
    
    var cellViewModel:PaymentTypeCellModel?{
        didSet{
            if let paymentType = cellViewModel{
                paymentTypeImg.image = paymentType.getPaymentTypeImage?.withRenderingMode(.alwaysTemplate)
                paymentTypeName.text = paymentType.getPaymentTypeTitle
                if paymentType.isSelected{
                    cellBgView.backgroundColor = UIColor.gStartColor
                    selectIndicator.isHidden = false
                    self.isSelected = true
                    paymentTypeImg.tintColor = .white

                }else{
                    cellBgView.backgroundColor = UIColor.white
                    selectIndicator.isHidden = true
                    self.isSelected = false
                    paymentTypeImg.tintColor = UIColor.gStartColor
                }
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
class PaymentTypeCellModel{
    
    var list:PaymentTypeList
    var isSelected = false
    
    init(listItem:PaymentTypeList) {
        list = listItem
    }
    
    var getPaymentTypeImage:UIImage?{
        return UIImage(named:list.image ?? "")
    }
    var getPaymentTypeTitle:String{
        return list.title ?? ""
    }
}
