//
//  ProductPaymentViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import UIKit
//import Razorpay
import SafariServices
import WebKit

protocol PaymentCompleteDelegate {
    func paymentCompletedNotification(token:String)
}
enum PaymentOption: String, CaseIterable {
    case Halawallet = "Hala Wallet"
    case Card = "Card"
    case Netbanking = "Net Banking"
    case Wallet = "Wallet"
    case UPI = "UPI"
    
    var iconImg: UIImage? {
        switch self {
        case .Halawallet:
            return #imageLiteral(resourceName: "img_logo_circle")
        case .Card:
            return #imageLiteral(resourceName: "img_dash_km_travel")
        case .Netbanking:
            return #imageLiteral(resourceName: "ic_draw_ifsc_code")
        case .Wallet:
            return #imageLiteral(resourceName: "img_wallet_refund")
        case .UPI:
            return #imageLiteral(resourceName: "ic_share")
        }
    }
}
class ProductPaymentViewController: BaseViewController,WKNavigationDelegate {

    @IBOutlet weak var lblAmountToPay: AppLabel!
    @IBOutlet weak var collectionPaytype: UICollectionView!
    @IBOutlet weak var searchTextField: AppTextField!
    @IBOutlet weak var paymentSubTypeTbl: UITableView!
    @IBOutlet weak var lblNoCard: AppLabel!
    @IBOutlet weak var halaWalletView: UIView!
    @IBOutlet weak var halaWalletBalanceLbl: AppLabel!
    @IBOutlet weak var savedCardsLbl: AppLabel!
    
    @IBOutlet weak var lblselectamode: AppLabel!
    @IBOutlet weak var lblhalawalletbal: AppLabel!
    @IBOutlet weak var btnAddcard: AppButton!
    @IBOutlet weak var btnproceedpay: AppButton!

    var amountToPay:Double = 0
    
    var viewModel:PaymentTypeViewModel?
    
    //Payment Gateway
//    var razorpayObj : RazorpayCheckout? = nil
    
    let razorpayKey = "rzp_test_9RlSBPrtyO0tN0"
    var webView: WKWebView!
//    var razorpay: RazorpayCheckout!
    var isFromWallet = false
    var isFromDashboard = false
    var delegate:PaymentCompleteDelegate?
    
    //MARK:View Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setValuespayment()
        // Do any additional setup after loading the view.
        lblAmountToPay.text = Formatter.shared.addCurrencyValue(from: amountToPay).currencyValue
        collectionPaytype.register(UINib.init(nibName: Constant.PaymentType.kPaymentTypeCellID, bundle: Bundle.main), forCellWithReuseIdentifier: Constant.PaymentType.kPaymentTypeCellID)
        
        viewModel = PaymentTypeViewModel.init(cvc: self)
        viewModel?.selectedPaymentType = isFromWallet ? 1 : 0
        //searchTextField.placeHolerString = "Search Bank"
        self.webView = WKWebView(frame: self.view.frame)
//        self.razorpay = RazorpayCheckout.initWithKey(razorpayKey, andDelegate: self, withPaymentWebView: self.webView)
//        print(self.razorpay?.getSupportedUPIApps())
        
        searchTextField.addTarget(self, action: #selector(textChanged(txtField:)), for: .editingChanged)
        viewModel?.getPaymentTypeList()

    }
    func setValuespayment(){
        lblselectamode.text = "Select a mode to pay".localized.capitalized
        lblhalawalletbal.text = "Hala Mobility Wallet Balance".localized
        btnproceedpay.setTitle("Proceed Payment".localized, for: .normal)
        savedCardsLbl.text = "Saved Cards".localized
        btnAddcard.setTitle("Add Card".localized, for: .normal)
        lblNoCard.text = "No Application Found".localized
        self.title = "Payment Method"
        collectionPaytype.register(UINib(nibName: "PaymentTypeCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "PaymentTypeCollectionViewCell")

       /* "Select Bank" = "Select Bank";
        "Select Wallet" = "Select Wallet";
        "Select UPI" = "Select UPI";*/
    }
    deinit {
        print("Payment screen removed")
    }
    @IBAction func moveBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func proceedWithPayment(_ sender: Any) {
        switch viewModel?.selectedPaymentType ?? 0 {
        case 0:
            if (viewModel?.canPayWithHalaWalet())!{
                //Proceed with payment
                //viewModel?.updateTransactionStatusToServer(transactionId: "", paymentMethod: "3")
            }else{
                let alertVC = self.alertController
                alertVC?.message = "You don't have sufficient money in your wallet. Want to upgrade your wallet balance?".localized
                alertVC?.addAction(.default, buttonPhrase: "Ok".localized, completion: nil)
                self.present(alertVC!, animated: true, completion: nil)
            }
        case 1:
            if (viewModel?.canPayWithOnline().0)!{
                //Proceed with payment
                //Get CVV now
                self.showAlertWithMessage("Enter CVV", phrase1: "Submit".localized.doubleSpaced, phrase2: "".localized, skipDismiss: false,showInput: true, action: UIAlertAction.Style.default, completion: { [weak self] (style) in
                    if style == .default {
                        print("User decided to buy")
                        let cvv = CVV
                        print(CVV)
                        let cardInfo = self?.viewModel?.getPaymentSubTypeCardCellModel(row: self?.viewModel?.selectedSubTypeIndex ?? 0)
                        self?.getAmountFromGateway(amount: self?.amountToPay ?? 0, cvv: cvv, cardInfo: cardInfo)
                    }
                })
            }else{
                let alertVC = self.alertController
                alertVC?.message = (viewModel?.canPayWithOnline().1)
                alertVC?.addAction(.default, buttonPhrase: "Ok".localized, completion: nil)
                self.present(alertVC!, animated: true, completion: nil)
            }
        case 2,3,4:
            if (viewModel?.canPayWithOnline().0)!{
                //Proceed with payment
                self.getAmountFromGateway(amount: self.amountToPay, cvv: "")
            }else{
                let alertVC = self.alertController
                alertVC?.message = (viewModel?.canPayWithOnline().1)
                alertVC?.addAction(.default, buttonPhrase: "Ok".localized, completion: nil)
                self.present(alertVC!, animated: true, completion: nil)
            }
        default:
            break
        }
        
    }
    @IBAction func addNewCard(_ sender: Any) {
        let paymentVC = AddDebitCardViewController.initFromStoryBoard(.Payment)
        paymentVC.delegate = self
        self.navigationController?.pushViewController(paymentVC, animated: true)
    }
    
    
    func getAmountFromGateway(amount:Double,cvv:String, cardInfo: Card_info? = nil){
        self.startActivityIndicatorInWindow()
        let amountToPass = "\(Int(amount))"
        let month = String(cardInfo?.expiry_date?.prefix(2) ?? "")
        let year = String(cardInfo?.expiry_date?.suffix(2) ?? "")
        let req = JPaymentReq(order: self.randomId ?? "", transaction: self.randomId ?? "", amount: amount.stringValue, currency: "USD", description: "wallet_recharge", nameOnCard: cardInfo?.card_name, number: cardInfo?.card_no, month: month, year: year, securityCode: cardInfo?.cvv)
        ServiceManager().jPayment(req) {  [weak self] (inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
                let res = try inner()
                let checkMode = CacheManager.settings?.payment_mode?.stringValue == "Test"
                if res?.status == (checkMode == true ? false : true) {
                    
                if self?.isFromDashboard == true {
                    self?.delegate?.paymentCompletedNotification(token: (res?.data?.authorizationResponse?.transactionIdentifier?.stringValue ?? ""))
                }else{
                    if self?.isFromWallet == true {
                        self?.startActivityIndicatorInWindow()
                        let settings =  CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings
                        let rate = settings?.currencies?.filter{$0.currency_code == "USD"}.first?.rate?.doubleValue ?? 0
                        ServiceManager().addMoneyToWallet((Double(self?.amountToPay ?? 0)/rate) , transactionId: res?.data?.transaction?.id?.stringValue ?? "") { [weak self] (inner) -> (Void) in
                            do {
                                let response = try inner()
                                self?.view.makeToast(response?.message?.stringValue)
                                self?.showAlertWith("Payment Succeeded", title: "Success", completion: { [weak self] (style) -> (Void) in
                                    self?.dismiss(animated: false) { [weak self] in
                                        self?.navigationController?.popToRootViewController(animated: true)
                                    }
                                })
                            } catch {
                                self?.view.makeToast((error as? CoreError)?.description)
                            }
                            self?.stopActivityIndicator()
                        }
                    }
                    //navBar.isHidden = true
                    //navBar.isUserInteractionEnabled = false
                }
                } else {
                    self?.showAlertWith(res?.message ?? "")
                    return
                }
            } catch {
                self?.stopActivityIndicator()
            }
        }
//        let amountToPass = "\(Int(amount * 100))"
//            ServiceManager().generateOrderID(amount: amountToPass,key: razorpayKey,value: "ePM7mWGZ2lzjHFzLtOp1LWIv") { [weak self](inner) -> (Void) in
//                do{
//                    let token = try inner()
//                    self?.openRazorpayCheckout(orderId: token, amount: Int(amount * 100),cvv: cvv)
//                    self?.stopActivityIndicator()
//                } catch{
//                    self?.stopActivityIndicator()
//                }
//        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK:Gateway Webview
    private func openRazorpayCheckout(orderId:String,amount:Int,cvv:String = "") {
        // 1. Initialize razorpay object with provided key. Also depending on your requirement you can assign delegate to self. It can be one of the protocol from RazorpayPaymentCompletionProtocolWithData, RazorpayPaymentCompletionProtocol.
        let userEmail = (CacheManager.userInfo?.email ?? "") == "" ? "\(CacheManager.userInfo?.phoneNo ?? "")@yopmail.com" : (CacheManager.userInfo?.email ?? "")
        self.webView.isHidden = false
     self.webView.navigationDelegate = self
     self.view.addSubview(self.webView)
        var options = [String:Any]()
        
        if viewModel?.selectedPaymentType ?? 0 == 1{
            let cardInfo = viewModel?.selectedPaymentSubType as! Card_info
            let expiry = cardInfo.expiry_date ?? ""
            options = [
                    "amount": amount, // amount in currency subunits. Defaults to INR. 100 = 100 paise = INR 1. We support more than 92 currencies.
//            "currency": CacheManager.currencyShortName,//We support more that 92 international currencies.
            "email": userEmail,
            "contact": CacheManager.userInfo?.phoneNo ?? "",
                    "order_id": orderId,//From response of Step 3
                    "method": "card",
                    "card[name]": cardInfo.card_name ?? "",
                    "card[number]": cardInfo.card_no ?? "",
                    "card[expiry_month]": expiry.prefix(2),
                    "card[expiry_year]": expiry.suffix(2),
            "card[cvv]": cvv
                ]
        }else if viewModel?.selectedPaymentType ?? 0 == 2{
            let payInfo = viewModel?.selectedPaymentSubType as! PaymentSubType
            options = [
                    "amount": amount, // amount in currency subunits. Defaults to INR. 100 = 100 paise = INR 1. We support more than 92 currencies.
//            "currency": CacheManager.currencyShortName,//We support more that 92 international currencies.
            "email": userEmail,
            "contact": CacheManager.userInfo?.phoneNo ?? "",
                    "order_id": orderId,//From response of Step 3
                    "method": "netbanking",
                    "bank": payInfo.shortCode
                ]
        }else if viewModel?.selectedPaymentType ?? 0 == 3{
            let payInfo = viewModel?.selectedPaymentSubType as! PaymentSubType
            options = [
                    "amount": amount, // amount in currency subunits. Defaults to INR. 100 = 100 paise = INR 1. We support more than 92 currencies.
//            "currency": CacheManager.currencyShortName,//We support more that 92 international currencies.
            "email": userEmail,
            "contact": CacheManager.userInfo?.phoneNo ?? "",
                    "order_id": orderId,//From response of Step 3
                    "method": "wallet",
                    "wallet": payInfo.shortCode
                ]
        }else if viewModel?.selectedPaymentType ?? 0 == 4{
            let payInfo = viewModel?.selectedPaymentSubType as! PaymentSubType
            options = [
                    "amount": amount, // amount in currency subunits. Defaults to INR. 100 = 100 paise = INR 1. We support more than 92 currencies.
//            "currency": CacheManager.currencyShortName,//We support more that 92 international currencies.
            "email": userEmail,
            "contact": CacheManager.userInfo?.phoneNo ?? "",
                    "order_id": orderId,//From response of Step 3
                    "method": "upi",
                    "_[flow]": "intent",
                "upi_app_package_name": payInfo.shortCode,
                ]
        }
//        if razorpay == nil{
//            self.razorpay = RazorpayCheckout.initWithKey(razorpayKey, andDelegate: self, withPaymentWebView: self.webView)
//        }
//     razorpay.authorize(options)
        /*let options: [AnyHashable:Any] = [
            "prefill": [
             "contact": CacheManager.userInfo?.phone ?? "",
             "email": CacheManager.userInfo?.email ?? "",
             "method" : "upi"
            ],
            "image": merchantDetails.logo,
            "amount" : (addWalletAmountTxt.text!).intValue() * 100,
            "name": merchantDetails.name,
            "theme": [
                "color": "#0099d7"
            ]
        ]
        if let rzp = self.razorpayObj {
            rzp.open(options)
        } else {
            print("Unable to initialize")
        }*/
    }
//
//     public func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!){
//         if razorpay != nil{
//             self.razorpay.webView(webView, didCommit: navigation)
//         }
//     }
//
//     public func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError er: Error) {
//         if razorpay != nil{
//             self.razorpay.webView(webView, didFailProvisionalNavigation: navigation, withError: er)
//         }
//     }
//
//     public func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError er: Error){
//         if razorpay != nil{
//             self.razorpay.webView(webView, didFail: navigation, withError: er)
//         }
//     }
//
//     public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!){
//         if razorpay != nil{
//             self.razorpay.webView(webView, didFinish: navigation)
//         }
//     }
}
//MARK:Collection View
extension ProductPaymentViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4 //viewModel?.paymentTypeCellCount ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.PaymentType.kPaymentTypeCellID, for: indexPath) as! PaymentTypeCollectionViewCell
        //cell.cellViewModel = viewModel?.paymentTypeCellViewModelForRow(indexPath: indexPath)
        if isFromWallet && indexPath.row == 0{
            cell.isHidden = true
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected Item--->\(indexPath.row)")
        if let selectedCell = collectionView.cellForItem(at: indexPath) as? PaymentTypeCollectionViewCell{
            
            selectedCell.cellViewModel?.isSelected = true
            selectedCell.cellBgView.backgroundColor = UIColor.gStartColor
            selectedCell.paymentTypeImg.imageThemeColor = 5
            selectedCell.selectIndicator.isHidden = false
        }
        viewModel?.selectedPaymentType = indexPath.row
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print("UnSelected Item--->\(indexPath.row)")
        if let selectedCell = collectionView.cellForItem(at: indexPath) as? PaymentTypeCollectionViewCell{
            
            selectedCell.cellViewModel?.isSelected = false
            selectedCell.cellBgView.backgroundColor = UIColor.white
            selectedCell.paymentTypeImg.imageThemeColor = 8
            
            selectedCell.selectIndicator.isHidden = true
        }
    }
    
}
extension ProductPaymentViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return (isFromWallet && indexPath.row == 0) ?  CGSize(width: 0, height: 90) : CGSize(width: 80, height: 90)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return isFromWallet ? 5 : 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
    }
}
//MARK:Table View
extension ProductPaymentViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.getPaymentSubTypeTableRows ?? 0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return viewModel?.getPaymentSubTypeTableRows ?? 0 == 0 ? 10 : 15
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if viewModel?.selectedPaymentType ?? 0 == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.PaymentType.kPaymentCardListCellID) as! PaymentCardListCell
            
            cell.cellViewModel = viewModel?.getPaymentSubTypeCardCellModel(row: indexPath.row)
            cell.isCellSelected = viewModel?.selectedSubTypeIndex == indexPath.row
            cell.indicator.isHidden = false
            cell.cardType.isHidden = false
            cell.selectionStyle = .none
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: Constant.PaymentType.kPaymentBankCellID) as! BankSelectionTableViewCell
            
            cell.selectionStyle = .none
            cell.backgroundColor = UIColor.white
            cell.cellViewModel = viewModel?.getPaymentSubTypeCellModel(row: indexPath.row)
            cell.isCellSelected = viewModel?.selectedSubTypeIndex == indexPath.row
            if indexPath.row == ((viewModel?.getPaymentSubTypeTableRows ?? 0) - 1){
                cell.bottomBorder.isHidden = false
            }
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Bank Option Selected")
        if viewModel?.selectedPaymentType ?? 0 == 1{
            let cell = tableView.cellForRow(at: indexPath) as! PaymentCardListCell
            viewModel?.selectedSubTypeIndex = indexPath.row
            viewModel?.selectedPaymentSubType = cell.cellViewModel as AnyObject?
            paymentSubTypeTbl.reloadData()
        }
        else{
            let cell = tableView.cellForRow(at: indexPath) as! BankSelectionTableViewCell
            viewModel?.selectedSubTypeIndex = indexPath.row
            viewModel?.selectedPaymentSubType = cell.cellViewModel as AnyObject?
            paymentSubTypeTbl.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel?.selectedPaymentType ?? 0 == 1 ? 80 : 40
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.white
        return footerView
    }
}


// RazorpayPaymentCompletionProtocol - This will execute two methods 1.Error and 2. Success case. On payment failure you will get a code and description. In payment success you will get the payment id.
//extension ProductPaymentViewController : RazorpayPaymentCompletionProtocol {
//
//
//    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]){
//        let alertController = UIAlertController(title: "FAILURE", message: str, preferredStyle: UIAlertController.Style.alert)
//        let cancelAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
//        alertController.addAction(cancelAction)
//        //self.view = view that controller manages
//        self.view.sendSubviewToBack(self.webView)
//        self.webView.isHidden = true
//        //navBar.isHidden = true
//        //navBar.isUserInteractionEnabled = false
//        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
//        self.razorpay = nil
//    }
//
//    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]){
//        /*let alertController = UIAlertController(title: "SUCCESS", message: "Payment Id \(payment_id)", preferredStyle: UIAlertController.Style.alert)
//        let cancelAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil)
//        alertController.addAction(cancelAction)
//        //self.view = view that controller manages
//        self.view.sendSubviewToBack(self.webView)*/
//        if isFromDashboard{
//            delegate?.paymentCompletedNotification(token: payment_id)
//        }else{
//            if isFromWallet{
//                //self.presentAlert(withTitle: "Success", message: "Payment Succeeded")
//                self.startActivityIndicatorInWindow()
//                ServiceManager().addMoneyToWallet(Int(amountToPay) ?? 0) { (inner) -> (Void) in
//                    self.stopActivityIndicator()
//                    do {
//                        let response = try inner()
//                        self.view.makeToast(response?.message)
//                        Router.setDashboardViewControllerAsRoot()
//                    } catch {
//                        self.view.makeToast((error as? CoreError)?.description)
//                    }
//                }
//            }
//            //navBar.isHidden = true
//            //navBar.isUserInteractionEnabled = false
//            self.razorpay = nil
//        }
//
//    }
//
//    //MARK: Action functions
//    @IBAction func cancel(_ sender: Any){
//
//        let alertController = UIAlertController(title: "Cancel Transaction", message: "Are you sure you want to cancel the current transaciton ? You will be taken back to checkout page, where you can choose another payment option", preferredStyle: UIAlertController.Style.alert)
//
//        let cancelAction = UIAlertAction(title: "  Do Not Cancel  ", style: UIAlertAction.Style.cancel, handler: nil)
//        let okayAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { action in
//            self.razorpay.userCancelledPayment()
//            } )
//
//        alertController.addAction(cancelAction)
//        alertController.addAction(okayAction)
//
//        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
//    }
//}

extension ProductPaymentViewController {
    func presentAlert(withTitle title: String?, message : String?) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "Okay", style: .default)
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

extension ProductPaymentViewController : SFSafariViewControllerDelegate {
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
extension ProductPaymentViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return false
    }
    @objc func textChanged(txtField:UITextField){
        viewModel?.getPaymentSubTypeList(withText: txtField.text ?? "")
        paymentSubTypeTbl.reloadData()

    }
}
extension ProductPaymentViewController:AddDebitCardDelegate{
    func newCardAdded(info: Card_info) {
        CacheManager.userInfo?.card_info?.append(info)
        viewModel?.selectedPaymentSubType = info
//        self.getAmountFromGateway(amount: self.amountToPay , cvv: info.cvv ?? "", cardInfo: info)
        viewModel?.modelObject?.paymentSubTypeCardList?.append(info)
        self.lblNoCard?.isHidden = true
        self.paymentSubTypeTbl.reloadData()
    }
}

//Remaining payment office
//{
//    "image":"ic_type_net",
//    "title": "Net Banking",
//    "description": "If you are close to the scooter and you see your Hala bike, press this button in the app to make the honking sound and the lights to flash"
//}, {
//    "image":"ic_type_wallet",
//    "title": "Wallet",
//    "description": "Press the unlock button in the app to start the vehicle and press the helmet icon to access the trunk case."
//},
//{
//    "image":"ic_type_upi",
//    "title": "UPI",
//    "description": "Press the unlock button in the app to start the vehicle and press the helmet icon to access the trunk case."
//}

extension NSObject {
    var randomId: String? {
        return (CacheManager.userInfo?.id?.stringValue ?? "") + UUID().uuidString
    }
}

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)


struct JPaymentRes: JSONSerializable {
    let authorizationResponse: AuthorizationResponse?
    let gatewayEntryPoint, merchant: CustomType?
    let order: Order?
    let response: Responsee?
    let result: CustomType?
    let sourceOfFunds: SourceOfFunds?
    let timeOfLastUpdate, timeOfRecord: CustomType?
    let transaction: Transaction?
    let version: CustomType?
}

// MARK: - AuthorizationResponse
struct AuthorizationResponse: JSONSerializable {
    let cardSecurityCodeError, commercialCard, commercialCardIndicator, date: CustomType?
    let posData, posEntryMode, processingCode, responseCode: CustomType?
    let returnAci, stan, time, transactionIdentifier: CustomType?
    let validationCode: CustomType?
}

// MARK: - Order
struct Order: Codable {
    let amount: CustomType?
    let authenticationStatus: CustomType?
    let chargeback: Chargeback?
    let creationTime, currency, orderDescription, id: CustomType?
    let lastUpdatedTime: CustomType?
    let merchantAmount: CustomType?
    let merchantCategoryCode, merchantCurrency, status: CustomType?
    let totalAuthorizedAmount, totalCapturedAmount, totalRefundedAmount: CustomType?

    enum CodingKeys: String, CodingKey {
        case amount, authenticationStatus, chargeback, creationTime, currency
        case orderDescription = "description"
        case id, lastUpdatedTime, merchantAmount, merchantCategoryCode, merchantCurrency, status, totalAuthorizedAmount, totalCapturedAmount, totalRefundedAmount
    }
}

// MARK: - Chargeback
struct Chargeback: Codable {
    let amount: CustomType?
    let currency: CustomType?
}

// MARK: - Response
struct Responsee: Codable {
    let acquirerCode, acquirerMessage: CustomType?
    let cardSecurityCode: CardSecurityCode?
    let gatewayCode: CustomType?
}

// MARK: - CardSecurityCode
struct CardSecurityCode: Codable {
    let acquirerCode, gatewayCode: CustomType?
}

// MARK: - SourceOfFunds
struct SourceOfFunds: Codable {
    let provided: Provided?
    let type: CustomType?
}

// MARK: - Provided
struct Provided: Codable {
    let card: Card?
}

// MARK: - Card
struct Card: Codable {
    let brand: CustomType?
    let expiry: Expiry?
    let fundingMethod, nameOnCard, number, scheme: CustomType?
    let storedOnFile: CustomType?
}

// MARK: - Expiry
struct Expiry: Codable {
    let month, year: CustomType?
}

// MARK: - Transaction
struct Transaction: Codable {
    let acquirer: Acquirer?
    let amount: CustomType?
    let authenticationStatus, currency, id, receipt: CustomType?
    let source, stan, terminal, type: CustomType?
}

// MARK: - Acquirer
struct Acquirer: Codable {
    let batch: CustomType?
    let date, id, merchantID, settlementDate: CustomType?
    let timeZone, transactionID: CustomType?

    enum CodingKeys: String, CodingKey {
        case batch, date, id
        case merchantID = "merchantId"
        case settlementDate, timeZone
        case transactionID = "transactionId"
    }
}

extension String {
    var spaced: String {
        return " " + self + " "
    }
    
    var doubleSpaced: String {
        return "  " + self + "  "
    }
    
    var tripleSpaced: String {
        return "   " + self + "   "
    }
    
    var prefixSpaced: String {
        return "  " + self
    }
    
    var suffixSpaced: String {
        return "  " + self
    }
}
