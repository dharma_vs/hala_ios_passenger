//
//  AddDebitCardViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import UIKit
protocol AddDebitCardDelegate {
    func newCardAdded(info:Card_info)
}

class AddDebitCardViewController: BaseViewController {

    @IBOutlet weak var cardHolderNameTxt: AppTextField!
    @IBOutlet weak var cardNumberTxt: AppTextField!
    @IBOutlet weak var expirationDateTxt: AppTextField!
    @IBOutlet weak var cvcTxt: AppTextField!
    @IBOutlet weak var cardTypeImage: UIImageView!
    var delegate:AddDebitCardDelegate?
    var selectedBrand = ""
    
    var viewModel: AddDebitCardViewModel!
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.viewModel = AddDebitCardViewModel(controller: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func submitAction(_ sender: Any) {
        self.view.endEditing(true)
        self.viewModel.addCard()
    }
    @IBAction func moveBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddDebitCardViewController:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == cardHolderNameTxt{
            guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                return true
            }
            let updatedText = oldText.replacingCharacters(in: r, with: string)
            if updatedText.count < 56{
                return true
            }
            
            return false
        }else if textField == cvcTxt{
            guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                return true
            }
            let updatedText = oldText.replacingCharacters(in: r, with: string)
            if updatedText.count < 4{
                return true
            }
            
            return false
        }
        else if textField == cardNumberTxt {
            guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                return true
            }
            let updatedText = oldText.replacingCharacters(in: r, with: string)
            self.identifyCard(startCard: updatedText)
            
            return formatCardNumber(textField: textField, shouldChangeCharactersInRange: range, replacementString: string)
        }else if textField == expirationDateTxt{
            guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                return true
            }
            let updatedText = oldText.replacingCharacters(in: r, with: string)
            //self.identifyCard(startCard: updatedText)
            if string == "" {
                if updatedText.count == 2 {
                    textField.text = "\(updatedText.prefix(1))"
                    return false
                }
            } else if updatedText.count == 1 {
                if updatedText > "1" {
                    return false
                }
            } else if updatedText.count == 2 {
                if updatedText <= "12" { //Prevent user to not enter month more than 12
                    textField.text = "\(updatedText)/" //This will add "/" when user enters 2nd digit of month
                }
                return false
            } else if updatedText.count == 5 {
                self.expDateValidation(dateStr: updatedText)
            } else if updatedText.count > 5 {
                return false
            }
            
            let spli = updatedText.components(separatedBy: "/")
            if spli.count <= 1{
                return updatedText.count < 3
            }else{
                return (spli.first?.count ?? 0) < 3 && spli[1].count < 3
            }
            return true
        }
        return true
    }
    func expDateValidation(dateStr:String) {

        let currentYear = Calendar.current.component(.year, from: Date()) % 100   // This will give you current year (i.e. if 2019 then it will be 19)
        let currentMonth = Calendar.current.component(.month, from: Date()) // This will give you current month (i.e if June then it will be 6)

        let enteredYear = Int(dateStr.suffix(2)) ?? 0 // get last two digit from entered string as year
        let enteredMonth = Int(dateStr.prefix(2)) ?? 0 // get first two digit from entered string as month
        print(dateStr) // This is MM/YY Entered by user

        if enteredYear > currentYear {
            if (1 ... 12).contains(enteredMonth) {
                print("Entered Date Is Right")
                expirationDateTxt.errorMessage = ""
            } else {
                print("Entered Date Is Wrong")
                expirationDateTxt.errorMessage = "Enter valid date"
            }
        } else if currentYear == enteredYear {
            if enteredMonth >= currentMonth {
                if (1 ... 12).contains(enteredMonth) {
                   print("Entered Date Is Right")
                    expirationDateTxt.errorMessage = ""
                } else {
                   print("Entered Date Is Wrong")
                    expirationDateTxt.errorMessage = "Enter valid date"
                }
            } else {
                print("Entered Date Is Wrong")
                expirationDateTxt.errorMessage = "Enter valid date"
            }
        } else {
           print("Entered Date Is Wrong")
            expirationDateTxt.errorMessage = "Enter valid date"
        }

    }
    func identifyCard(startCard:String){
        if startCard.count >= 4{
        if startCard.prefix(1) == "4"{
            print("Visa")
            cardTypeImage.image = UIImage(named: "ic_type_visa_tiny")
            selectedBrand = "Visa"
        }else if ["51", "52", "53", "54", "55"].contains(startCard.prefix(2)){
            print ("master")
            cardTypeImage.image = UIImage(named: "ic_type_master_tiny")
            selectedBrand = "Master"
        }else if ["34", "37"].contains(startCard.prefix(2)){
            print ("master")
            cardTypeImage.image = UIImage(named: "ic_type_master_tiny")
            selectedBrand = "Master"
        }else if ["644", "645", "646", "647", "648",
                  "649"].contains(startCard.prefix(3)){
            print("Discover")
            selectedBrand = "Discover"
            cardTypeImage.image = UIImage(named: "ic_type_discover_tiny")
        }else if startCard.prefix(2) == "65" || startCard.prefix(4) == "6011"{
            print("Discover")
            selectedBrand = "Discover"
            cardTypeImage.image = UIImage(named: "ic_type_discover_tiny")
        }else if ["5010", "5011", "5012", "5013", "5014", "5015", "5016", "5017", "5018",
        "6012", "6013", "6014", "6015", "6016", "6017", "6018", "6019",
        "6760", "6761", "6762", "6763", "6764", "6765", "6766", "6768", "6769", "6060"].contains(startCard.prefix(4)){
            print("Mastero")
            selectedBrand = "Mastero"
            cardTypeImage.image = UIImage(named: "ic_type_mastero_tiny")
        }else if ["602", "603", "604", "605",
                  "677", "675", "674", "673", "672", "671", "670","502", "503", "504", "505", "506", "507", "508"].contains(startCard.prefix(3)){
            print("mastero")
            cardTypeImage.image = UIImage(named: "ic_type_mastero_tiny")
            selectedBrand = "Mastero"
        }
           
        }else{
            cardTypeImage.image = UIImage(named: "ic_card_unknown")
            selectedBrand = ""
        }
    }

    func formatCardNumber(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == cardNumberTxt {
            let replacementStringIsLegal = string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "0123456789").inverted) == nil

            if !replacementStringIsLegal {
                return false
            }

            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let components = newString.components(separatedBy: NSCharacterSet(charactersIn: "0123456789").inverted)
            let decimalString = components.joined(separator: "") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.character(at: 0) == (1 as unichar)

            if length == 0 || (length > 16 && !hasLeadingOne) || length > 19 {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int

                return (newLength > 16) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()

            if hasLeadingOne {
                formattedString.append("1 ")
                index += 1
            }
            if length - index > 4 {
                let prefix = decimalString.substring(with: NSRange(location: index, length: 4))
                formattedString.appendFormat("%@ ", prefix)
                index += 4
            }

            if length - index > 4 {
                let prefix = decimalString.substring(with: NSRange(location: index, length: 4))
                formattedString.appendFormat("%@ ", prefix)
                index += 4
            }
            if length - index > 4 {
                let prefix = decimalString.substring(with: NSRange(location: index, length: 4))
                formattedString.appendFormat("%@ ", prefix)
                index += 4
            }

            let remainder = decimalString.substring(from: index)
            formattedString.append(remainder)
            textField.text = formattedString as String
            return false
        } else {
            return true
        }
    }
}
extension UITextField {

public func setText(to newText: String, preservingCursor: Bool) {
    if preservingCursor {
        let cursorPosition = offset(from: beginningOfDocument, to: selectedTextRange!.start) + newText.count - (text?.count ?? 0)
        text = newText
        if let newPosition = self.position(from: beginningOfDocument, offset: cursorPosition) {
            selectedTextRange = textRange(from: newPosition, to: newPosition)
        }
    }
    else {
        text = newText
    }
}
}
