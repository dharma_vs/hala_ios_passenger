//
//  PaymentDataServiceEndPoint.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 07/03/22.
//

import Foundation

//case .cards:
//

enum PaymentDataServiceEndPoint: AddressableEndPoint {
    
    case cards, updateRidePaymentMethod, jPayment, addMoneyToWallet
    
    var endPointInfo: DataServiceEndpointInfo {
        switch self {
        
        case .addMoneyToWallet:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/rider/profile/wallet", method: .PUT, authType: .user)
        
        case .cards:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/cards", method: .POST, authType: .user)
            
        case .updateRidePaymentMethod:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/ride/update-payment-method", method: .PUT, authType: .user)
            
        case .jPayment:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/payment/jpayment", method: .POST, authType: .user)
        }
    }
}
        
