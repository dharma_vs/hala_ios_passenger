//
//  SubType.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import Foundation

struct PaymentSubType{
    var subTypeName:String
    var shortCode:String
    var subTypeImg:String
    init(code:String,title:String,img:String = "") {
        subTypeName = title
        shortCode = code
        subTypeImg = img
    }
}
