//
//  JPaymentReq.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import Foundation

struct JPaymentReq: JSONSerializable {
    var order: String?
    var transaction: String?
    var amount: String?
    var currency: String?
    var description: String?
    var nameOnCard: String?
    var number: String?
    var month: String?
    var year: String?
    var securityCode: String?
}
