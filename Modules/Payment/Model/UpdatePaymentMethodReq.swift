//
//  UpdatePaymentMethodReq.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 09/03/22.
//

import Foundation

struct UpdatePaymentMethodReq: JSONSerializable {
    var ride_id, driver_id, payment_method: String!
}
