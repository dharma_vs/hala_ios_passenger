//
//  PaymentTypeList.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 09/03/22.
//

import Foundation

struct PaymentTypeList:Decodable{
    
    var image: String?
    var title: String?
    var description: String?
}
