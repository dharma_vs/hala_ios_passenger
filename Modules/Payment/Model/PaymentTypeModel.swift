//
//  PaymentTypeModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 11/03/22.
//

import Foundation

class PaymentTypeModel:NSObject{
    
    var paymentTypeList:PaymentType?
    var paymentSubTypeBankList:[PaymentSubType]?
    var paymentSubTypeWalletList:[PaymentSubType]?
    var paymentSubTypeCardList:[Card_info]?
    var paymentSubTypeUPIList:[PaymentSubType]?
    
    func getBankList()->[PaymentSubType] {
        if let bankList = paymentSubTypeBankList{
            return bankList
        }else{
            paymentSubTypeBankList = [PaymentSubType]()
            paymentSubTypeBankList!.append(PaymentSubType(code :"ABHY", title : "Abhyudaya Co-operative Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"ABPB", title : "Aditya Birla Idea Payments Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"ACBX", title : "Adarsh Cooperative Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"ADBX", title : "The Ahmedabad District Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"AGVX", title : "Assam Gramin Vikash Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"AIRP", title : "Airtel Payments Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"ALLA", title : "Allahabad Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"AMCB", title : "Ahmedabad Mercantile Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"AMDN", title : "Ahmednagar Merchants Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"ANDB", title : "Andhra Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"APBL", title : "The Andhra Pradesh State Cooperative Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"APGB", title : "Andhra Pragathi Grameena Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"APGVB", title : "Andhra Pragathi Grameena Vikas Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"APMC", title : "A. P. Mahesh Co-operative Urban Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"ASBL", title : "Apna Sahakari Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"AUGX", title : "Allahabad UP Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"AUSF", title : "AU Small Finance Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"UTIB", title : "Axis Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"BACB", title : "Bassein Catholic Coop Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"BARA", title : "Baramati Sahakari Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"BARB_R", title : "Bank of Baroda"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"BBKM", title : "Bank of Bahrein and Kuwait"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"BDBL", title : "Bandhan Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"BGBX", title : "Dakshin Bihar Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"BGGX", title : "Baroda Gujarat Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"BHUX", title : "Bhilwara Urban Co-operative Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"BKDN", title : "Dena Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"BRDX", title : "Baroda Central Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"BRGX", title : "Baroda Rajasthan Kshetriya Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"BUGX", title : "Baroda Uttar Pradesh Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"BKID", title : "Bank of India"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MAHB", title : "Bank of Maharashtra"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"CBIN", title : "Central Bank of India"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"CGBX", title : "Chhattisgarh Rajya Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"CGGX", title : "Chaitanya Godavari Grameena Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"CITI", title : "Citibank Retail"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"CIUB", title : "City Union Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"CNRB", title : "Canara Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"COLX", title : "Costal Local Area Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"CORP", title : "Corporation Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"COSB", title : "The Cosmos Co-Op. Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"CSBK", title : "Catholic Syrian Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"DBSS", title : "DBS Digibank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"DCBL", title : "DCB Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"DEGX", title : "Dena Gujarat Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"DEUT", title : "Deutsche Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"DGBX", title : "Telangana Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"DLXB", title : "Dhanlaxmi Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"DNSB", title : "Dombivali Nagrik Sahakari Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"ESFB", title : "Equitas Small Finance Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"ESMF", title : "ESAF Small Finance Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"FDRL", title : "Federal Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"FINO", title : "Fino Payment Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"FINF", title : "Fincare Small Finance Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"GSCB", title : "The Gujarat State Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"HCBL", title : "The Hasti Co operative Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"HDFC", title : "HDFC Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"HGBX", title : "Sarva Haryana Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"HMBX", title : "Himachal Pradesh Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"HSBC", title : "HSBC"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"HUTX", title : "Hutatma Sahakari Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"IBKL", title : "IDBI Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"ICIC", title : "ICICI Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"IDFB", title : "IDFC First Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"IDIB", title : "Indian Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"INDB", title : "IndusInd Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"IOBA", title : "Indian Overseas Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"IPOS", title : "India Post Payment Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"JAKA", title : "Jammu and Kashmir Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"JIOP", title : "Jio Payments Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"JJSB", title : "Jalgaona Janata Sahkari Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"JSBP", title : "Janata Sahakari Bank (Pune)"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"JSFB", title : "Jana Small Finance Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"KAIJ", title : "Kallappanna Awade Ichalkaranji Janata Sahakari Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"KARB", title : "Karnataka Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"KCCB", title : "Kalupur Commercial Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"KDIX", title : "Shree Kadi Nagarik Sahakari Bank Ltd"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"KGRB", title : "Kaveri Grameena Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"KGSX", title : "Kashi Gomti Samyut Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"KJSB", title : "The Kalyan Janta Sahkari Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"KKBK", title : "Kotak Mahindra Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"KLGB", title : "Kerala Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"KVBL", title : "Karur Vysya Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"KVGB", title : "Karnataka vikas Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"LAVB_R", title : "Lakshmi Vilas Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"LDRX", title : "Langpi Dehangi Rural Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MAHG", title : "Maharashtra Grameen Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MBGX", title : "Madhya Bihar Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MCBL", title : "The Mahanagar Co Op. Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MDGX", title : "Rajasthan Marudhara Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MERX", title : "Meghalaya Rural Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MGRB", title : "Malwa Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MRBX", title : "Manipur Rural Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MRTX", title : "Maratha Cooprative Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MSBL", title : "Malad Sahakari Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MSCI", title : "Maharashtra state co opp Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MSLM", title : "Muslim Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MSNU", title : "Mehsana Urban Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MUBL", title : "Municipal Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"MZRX", title : "Mizoram Rural Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"NKGS", title : "NKGSB Co-op Bank Ltd"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"NSPB", title : "NSDL Payments Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"NTBL", title : "Nainital Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"NNSB", title : "Nutan Nagarik Sahakari Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"ORBC", title : "Oriental Bank of Commerce"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"PASX", title : "Paschim Banga Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"PSIB", title : "Punjab & Sind Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"PGBX", title : "Pragati Krishna Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"PJSB", title : "G P Parsik Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"PKGB", title : "Karnataka Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"PMCB", title : "Punjab & Maharashtra Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"PRTH", title : "Prathama Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"PTSX", title : "Patan Nagarik Sahakari Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"PUGX", title : "Punjab Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"PUNB", title : "Punjab National Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"PURX", title : "Purvanchal Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"PYTM", title : "Paytm Payments Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"RATN", title : "RBL Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"RNSB", title : "Rajkot Nagari Sahakari Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SACB", title : "Shri Arihant Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SAGX", title : "Saurashtra Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SBIN", title : "State Bank of India"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SCBL", title : "Standard Chartered Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SCOB", title : "Samruddhi Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SDCX", title : "Sindhudurg District Central Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SIBL", title : "South Indian Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SPCB", title : "Surat People Cooperative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SPSX", title : "Sandur Pattana Souharda Sahakari Bank Niyamitha"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SRCB", title : "Saraswat Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SRCX", title : "Shree Bharat Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SSDX", title : "Suco Souharda Sahakari Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SSKX", title : "Sadhana Sahakari Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SUBX", title : "Sarva UP Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SURY", title : "Suryoday Small Finance Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SUTB", title : "Sutex Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SUVX", title : "Suvarnayug Sahakari Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SVCB", title : "Shamrao Vithal Co-operative Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SVCX", title : "Sarvodaya Commercial Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"SYNB", title : "Syndicate Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"TBSB", title : "Thane Bharat Sahakari Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"TGBX", title : "Tripura Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"TBSB", title : "Thane Bharat Sahakari Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"TMBL", title : "Tamilnadu Mercantile Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"TNSC", title : "Tamilnadu State Apex Co operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"TSAB", title : "Telangana State Co Operative Apex Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"TUDX", title : "The Urban Co-Op. Bank Ltd. Dharangaon"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"TUMX", title : "The Udaipur Mahila Urban Co-op. Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"UBIN", title : "Union Bank of India"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"UCBA", title : "UCO Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"UJVN", title : "Ujjivan Small Finance Bank Limited"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"UMSX", title : "The Udaipur Mahila Samridhi Urban Co-op. Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"UTBI", title : "United Bank of India"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"UTGX", title : "Uttarakhand Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"UUCX", title : "Udaipur Urban Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"VARA", title : "The Varachha Co-Operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"VCOB", title : "The Vijay Co-Op. Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"VGBX", title : "Vananchal Gramin Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"VIJB", title : "Vijaya Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"VSBL", title : "The Vishweshwar Sahakari Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"VSCX", title : "Vikas Souharda Co-operative Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"VVSB", title : "Vasai Vikas Co-op Bank Ltd."));
            paymentSubTypeBankList!.append(PaymentSubType(code :"XJKG", title : "J & K Grameen Bank"));
            paymentSubTypeBankList!.append(PaymentSubType(code :"YESB", title : "Yes Bank"));

            return paymentSubTypeBankList!
        }
    }
    func getWalletList()->[PaymentSubType] {
        if let walletList = paymentSubTypeWalletList{
            return walletList
        }else{
            paymentSubTypeWalletList = [PaymentSubType]()
            
            paymentSubTypeWalletList!.append(PaymentSubType(code :"freecharge", title : "FreeCharge",img: "ic_free_charge"));
            paymentSubTypeWalletList!.append(PaymentSubType(code :"payzapp", title : "PayZapp",img: "ic_payz"));
            paymentSubTypeWalletList!.append(PaymentSubType(code :"airtelmoney", title : "Airtel Money",img: "ic_airtel"));
            paymentSubTypeWalletList!.append(PaymentSubType(code :"mobikwik", title : "MobiKwik",img: "ic_mobi"));
            paymentSubTypeWalletList!.append(PaymentSubType(code :"amazonpay", title : "Amazon Pay",img: "ic_amazon"));
            paymentSubTypeWalletList!.append(PaymentSubType(code :"paytm", title : "Paytm",img: "ic_paytm"));
            return paymentSubTypeWalletList!
        }
    }
    func getCardList()->[Card_info]{
        if let cardList = paymentSubTypeCardList{
            return cardList
        }else{
            paymentSubTypeCardList = CacheManager.userInfo?.card_info
            if paymentSubTypeCardList == nil{
                paymentSubTypeCardList = [Card_info]()
            }
            return paymentSubTypeCardList!
        }
    }
    func getUPIList(controller:ProductPaymentViewController)->[PaymentSubType]{
        if let upiList = paymentSubTypeUPIList{
            return upiList
        }else{
//            let list = controller.razorpay.getSupportedUPIApps()
//            print(list)
            return [PaymentSubType]()
        }
    }
}
struct PaymentType:Decodable{
    var list:[PaymentTypeList]?
}
struct PaymentTypeList:Decodable{
    
    var image: String?
    var title: String?
    var description: String?
}
struct PaymentToken:Decodable{
    var token:String?
    var status:String?
    
    enum CodingKeys:String,CodingKey {
        case token = "id"
        case status
    }
    init() {
        token = nil
        status = nil
    }
}
struct PaymentSubType{
    var subTypeName:String
    var shortCode:String
    var subTypeImg:String
    init(code:String,title:String,img:String = "") {
        subTypeName = title
        shortCode = code
        subTypeImg = img
    }
}
