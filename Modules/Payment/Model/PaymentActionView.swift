//
//  PaymentActionView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 10/03/22.
//

import UIKit

class PaymentActionView: UIView {
    
    var viewModel: PaymentActionViewModel! {
        didSet {
            self.viewModel.updatePaymentActionView(self)
        }
    }
    
    var completPayment: (()->Void)?
    var refreshView: (()->Void)?
    var payByCard: (()->Void)?
    
    func releaseCallBacks() {
        self.completPayment = nil
        self.payByCard = nil
    }

    @IBOutlet weak var addressContentView: RideAddressView!
    @IBOutlet weak var rideDistanceInfoView: RideDistanceInfoView!
    @IBOutlet weak var rideUserInfoView: UserInfoView!
    @IBOutlet weak var changePaymentToCash: UIButton!
    
    @IBOutlet weak var rideFareLbl: UILabel!
    @IBOutlet weak var collectCashAmontLbl: UILabel!
    @IBOutlet weak var walletDetutionLbl: UILabel!
    @IBOutlet weak var totalFareLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    
    @IBOutlet weak var walletDetutionValueLbl: UILabel!
    @IBOutlet weak var totalFareValueLbl: UILabel!

    
    @IBOutlet weak var paymentCompletedBtn: GradientButton!
    @IBOutlet weak var paybyCardBtn: UIButton!
    
    @IBOutlet weak var packageView: UIView!
    @IBOutlet weak var packageNameLbl: UILabel!
    @IBOutlet weak var packageLbl: UILabel!
    @IBOutlet weak var startReading: UILabel!
    @IBOutlet weak var startReadingLbl: UILabel!
    @IBOutlet weak var destinationChangeFareLbl: UILabel!
    
    weak var baseVC: UIViewController?
    
    @IBAction func showReading(_ sender: UIButton) {
        let profileInfo = ProfileInfo(info: "Start Reading".localized, value: "1 Photo".localized, isPhotoType: true, type: .vehicle_document, titles: ["Start Reading".localized], urlStrings: [/*"documents/"+(*/self.viewModel.detail.rental?.startReadingImage?.stringValue/*.components(separatedBy: "/").last*/ ?? ""])
        
        let imageVC = ProfileImagesViewController.initFromStoryBoard(.Main)
        imageVC.profileDocumentInfo = profileInfo
        imageVC.modalPresentationStyle = .overCurrentContext
        baseVC?.present(imageVC, animated: true, completion: nil)
    }

    @IBAction func completePayment(_ sender: Any) {
        self.completPayment?()
    }
    @IBAction func payByCardBtnAction(_ sender: Any) {
           self.payByCard?()
       }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.packageLbl?.text = "Package".localized
        self.startReading?.text = "Start Reading".localized
        self.changePaymentToCash?.setAttributedTitle("Change Payment to Cash".localized.underlined, for: UIControl.State())
        self.totalFareLbl?.text = "Total Fare".localized
        self.walletDetutionLbl?.text = "Wallet Deduction".localized
        self.destinationChangeFareLbl?.text = "Wallet Deduction".localized
    }
    
    @IBAction func changePaymentMethod(_ sender: UIButton) {
        ServiceManager().updateRidePaymentMethod(UpdatePaymentMethodReq(ride_id: self.viewModel.detail.id?.stringValue, driver_id: self.viewModel.detail.driver_info?.id?.stringValue, payment_method: "1")) { [weak self] (inner) -> (Void) in
            do {
                _ = try inner()
                self?.refreshView?()
//                DispatchQueue.main.async {
//                    self?.paymentCompletedBtn?.isHidden = false
//                    self?.changePaymentToCash?.isHidden = true
//                    self?.paybyCardBtn?.isHidden = true
//                }
//                self?.refreshList()
            } catch {
                
            }
        }
    }
    
    func refreshList() {
        ServiceManager().checkCurrentRideStatus {[weak self] (inner) -> (Void) in
            do {
                if let _ = try inner(), let weakSelf = self, var detail = self?.viewModel.detail {
                    detail.payment_mode = "1"
                    self?.viewModel = PaymentActionViewModel(rideDetail: detail)
                    self?.viewModel.updatePaymentActionView(weakSelf)
                }
            } catch { }
        }
    }
}

extension String {
    var underlined: NSAttributedString {
        return NSAttributedString(string: self, attributes: [
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue,
            NSAttributedString.Key.foregroundColor: UIColor.black
        ])
    }
}
