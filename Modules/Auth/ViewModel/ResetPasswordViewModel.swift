//
//  ResetPasswordViewModel.swift
//  Base
//
//  Created by Raju on 14/02/22.
//

import UIKit

    class ResetPasswordViewModel: AppViewModel {
        weak var controller: ResetPasswordViewController!
        required init(controller: ResetPasswordViewController) {
            self.controller = controller
        }
    }

extension ServiceManager {
    func resetPassword(_ password: String, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        let body = ["password": password] as Dictionary<String,Any>
        self.request(endPoint: AuthDataServiceEndpoint.resetPassword, body: body) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: CommonRes.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
