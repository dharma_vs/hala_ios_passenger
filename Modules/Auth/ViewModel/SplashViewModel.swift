//
//  SplashViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 05/03/22.
//

import Foundation
import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces

class SplashViewModel: AppViewModel {
    weak var controller: SplashViewController!
    required init(controller: SplashViewController) {
        self.controller = controller
    }
    
    var mapsHelper = GoogleMapsHelper()
    
    func checkAccessKey() {
        UserDefaults.accessKey = "virtu7620"
        if let accessKey = UserDefaults.accessKey {
            self.proceedWithKey(accessKey)
        } else {
            self.controller.getAccessKey()
        }
    }
    
    func proceedWithKey(_ key: String) {
        self.mapsHelper.getCurrentLocation {[weak self] (location) in
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: { [weak self] (placeMarks, error) in
                UserDefaults.country = "congo"
                ServiceManager().appSettings { [weak self](inner) -> (Void) in
                    do {
                        let appSettings = try inner()
                        CacheManager.settings = appSettings
//                        googleMapKey = appSettings?.api_key ?? googleMapKey
                        GMSServices.provideAPIKey(googleMapKey)
                        GMSPlacesClient.provideAPIKey(googleMapKey)
                        if (Double( CacheManager.settings?.ios_passenger_app_version ?? "0") ?? 0) > (Double(String.appVersion ?? "0") ?? 0) {
                            if CacheManager.settings?.ios_passenger_mandatory_update == "true"{
                                self?.controller?.showAlertWithMessage("New version of this application avaliable at app store. Please update now", phrase1: "Update", action: UIAlertAction.Style.default, completion: { [weak self] (style) in
                                    if style == .default {
                                        self?.controller?.updateApplication()
                                    }
                                })
                                
                            }else{
                                self?.controller?.showAlertWithMessage("New version of this application avaliable at app store. Please update now", phrase1: "Update", phrase2: "Later", action: UIAlertAction.Style.default, UIAlertAction.Style.cancel, completion: { [weak self] (style) in
                                    if style == .default {
                                        self?.controller?.updateApplication()
                                    }else{
                                        guard let _ = CacheManager.sharedInstance.getObjectForKey(.AuthToken),
                                              let _ = CacheManager.sharedInstance.getObjectForKey(.RefreshToken),
                                              let _ = CacheManager.riderInfo else {
                                                  Router.setLangageViewControllerAsRoot()
                                                  return
                                              }
                                        ServiceManager().getProfile({ [weak self](inner) -> (Void) in
                                            do {
                                                let _ = try inner()
                                                CacheManager.sharedInstance.setObject(true, key: .SpeechEnabled)
                                                Router.setHomeViewControllerAsRoot()
                                                
                                            } catch {
                                                Router.setLangageViewControllerAsRoot()
                                                print(error)
                                            }
                                        })
                                    }
                                })
                            }
                            return
                        }
                        guard let _ = CacheManager.sharedInstance.getObjectForKey(.AuthToken),
                              let _ = CacheManager.sharedInstance.getObjectForKey(.RefreshToken),
                              let _ = CacheManager.riderInfo else {
                                  Router.setLangageViewControllerAsRoot()
                                  return
                              }
                        ServiceManager().getProfile({ [weak self](inner) -> (Void) in
                            do {
                                let _ = try inner()
                                CacheManager.sharedInstance.setObject(true, key: .SpeechEnabled)
                                Router.setDashboardViewControllerAsRoot()
                            } catch {
                                Router.setLangageViewControllerAsRoot()
                                print(error)
                                //self?.view.makeToast(.m)
                            }
                        })
                    } catch {
                        print(error)
                    }
                }
            })
        }
        
    }
}

extension AppViewModel {
    var presentedViewController: UIViewController? {
        return (self.controller as? UIViewController)?.presentedViewController
    }
}
