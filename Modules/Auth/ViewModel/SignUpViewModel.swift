//
//  SignUpViewModel.swift
//  Base
//
//  Created by Raju on 14/02/22.
//

import UIKit
import Toast_Swift
  
class SignUpViewModel: AppViewModel {
    weak var controller: SignUpViewController!
    required init(controller: SignUpViewController) {
        self.controller = controller
    }
    
    func register() {
        if let register = controller?.registerObject() as? Register {
            self.controller?.startActivityIndicatorInWindow()
            ServiceManager().register(Token.self, register: register) { [weak self] (inner) in
                do {
                    let response = try inner()
                    if response?.status == true {
                        ServiceManager().getProfile({ [weak self] (inner) -> (Void) in
                            do {
                                let _ = try inner()
                                self?.controller?.view.makeToast(response?.message, duration: 2, position: .bottom, title: nil, image: nil, style: ToastStyle(), completion: { (success) in
                                    Router.setDashboardViewControllerAsRoot()
                                })
                            } catch {
                                self?.controller?.stopActivityIndicator()
//                                self.view.makeToast((error as? CoreError)?.description)
                            }
                        })
                    } else {
                        self?.controller?.stopActivityIndicator()
//                        self.view.makeToast(response?.message)
                    }
                } catch (let e) {
                    self?.controller?.stopActivityIndicator()
//                    self.view.makeToast("\(String.Failed.localized) \((e.localizedDescription))")
                }
            }
        }

    }
    
    func checkAccount() {
        if (controller?.registerObject() as? Register) != nil {
        let body = ["ccp":controller.countryCodeBtn?.titleLabel?.text ?? "" , "tel_no":controller.phoneTextFld.text ?? "", "email": controller.emailTextFld.text ?? "", "name": controller.nameTextFld?.text ?? ""] as AnyObject
        ServiceManager().checkAccount(body) { [weak self](inner) in
            do {
                let response = try inner()
                if response?.isSuccess == false {
                    self?.controller?.stopActivityIndicator()
                    self?.controller?.showAlertWith(response?.message?.stringValue ?? "")
                } else {
                    if self?.controller?.isPhoneVerified == true {
                        self?.register()
                    } else {
                        self?.sendOTP()
                    }
                }
            } catch (let e) {
                self?.controller?.stopActivityIndicator()
                //  self.view.makeToast("\(String.Failed.localized) \(e.localizedDescription)")
            }
        }
        }
    }
    
    func sendOTP() {
        let numberVerificationVC = NumberVerificationViewController.initFromStoryBoard(.Authenticate)
        numberVerificationVC.modalPresentationStyle = .overFullScreen
        numberVerificationVC.modalTransitionStyle = .crossDissolve
        numberVerificationVC.verificationSucceeds = { [weak self] verified in
            self?.controller?.dismiss(animated: false, completion: nil)
            if verified {
                let phone: String = self?.controller?.phoneTextFld.text ?? ""
                let code = self?.controller?.countryCodeBtn.titleLabel?.text ?? ""
                let body = ["tel_no": phone, "ccp": "+91"] as AnyObject
                ServiceManager().sendOTP(OTP.self, body: body, completion: { [weak self] (inner) in
                    do {
                        let response = try inner()
                        if response?.status == true, let otp = response?.data?.otp, (self?.controller?.navigationController?.topViewController is OTPViewController) == false {
                            self?.controller?.performSegue(withIdentifier: .GotoOTPFromSignupViewController, sender: otp)
                        }
                        self?.controller?.navigationController?.topViewController?.view?.makeToast(response?.message)
                    } catch (let e) {
                        //                self.view.makeToast("\(String.Failed.localized) \(e.localizedDescription)")
                    }
                    self?.controller?.stopActivityIndicator()
                })
            } else {
                
            }
        }
        self.controller?.present(numberVerificationVC, animated: true, completion: nil)
    }
}

extension ServiceManager {
    func register<T: Decodable>(_ type: T.Type, register: Register, completion: @escaping (_ inner: () throws ->  GeneralResponse<T>?) -> (Void)) {
        let body = register.JSONRepresentation as Dictionary<String,Any>
        
        self.request(endPoint: AuthDataServiceEndpoint.register, body: body) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: GeneralResponse<T>.self)
                if response?.status == true, let token: Token = response?.data as? Token {
                    CacheManager.sharedInstance.setObject(token.accessToken ?? "", key: .AuthToken)
                    CacheManager.sharedInstance.setObject(token.refreshToken ?? "", key: .RefreshToken)
                    CacheManager.sharedInstance.saveCache()
                }
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
