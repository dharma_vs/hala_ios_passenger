//
//  NumberVerificationViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 17/03/22.
//

import Foundation

class NumberVerificationViewModel: AppViewModel {
    weak var controller: NumberVerificationViewController!
    required init(controller: NumberVerificationViewController) {
        self.controller = controller
    }
    
    var timer: Timer?
    
    func startTimer() {
        if timer == nil {
            timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: { [weak self] tmr in
                self?.stopTimer()
            })
        }
    }
    
    func animateCallImage() {
        self.controller?.circleView?.animateCircleWithDuration(3, pollingTime: 3, isFill: true)
        self.startTimer()
    }
    
    func stopTimer() {
        self.timer?.invalidate()
        self.timer = nil
        self.controller?.verificationSucceeds?(true)
    }
}
