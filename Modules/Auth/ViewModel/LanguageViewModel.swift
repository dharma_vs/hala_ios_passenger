//
//  LanguageViewModel.swift
//  Base
//
//  Created by Raju on 13/02/22.
//

import UIKit

enum Language: String {
    case english = "English"
    case french = "French"
    
    static var all: [Language] {
        return [Language.english, .french]
    }
    
    static var allRawValues: [String] {
        return [Language.english.rawValue, Language.french.rawValue,
                ]
    }
    
    var shortName: String {
        switch self {
        case .english:
            return "en"
        case .french:
            return "fr"

        }
    }
    
    static func language(_ shortName: String) -> Language {
        switch shortName {
        case "en":
            return .english
        case "fr":
            return .french
 
        default:
            return .english
        }
    }
    
    var index: Int {
        switch self {
        case .english:
            return 0
        case .french:
            return 1
 
        }
    }
}

class LanguageViewModel: AppViewModel {
    weak var controller: LanguageViewController!
    required init(controller: LanguageViewController) {
        self.controller = controller
    }
}

