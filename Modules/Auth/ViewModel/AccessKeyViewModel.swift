//
//  AccessKeyViewModel.swift
//  Base
//
//  Created by Raju on 14/02/22.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces

class AccessKeyViewModel: AppViewModel {
    weak var controller: AccessKeyViewController!
    required init(controller: AccessKeyViewController) {
        self.controller = controller
    }
    
    var mapsHelper = GoogleMapsHelper()
    
    func proceedWithKey(_ key: String) {
        self.mapsHelper.getCurrentLocation {[weak self] (location) in
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: { [weak self] (placeMarks, error) in
                UserDefaults.country = placeMarks?.first?.countryy.country_full_name
                self?.controller?.startActivityIndicatorInWindow()
        ServiceManager().keyVerification (key){ [weak self] (newInner) -> (Void) in
            do {
                _ = try newInner()
                UserDefaults.accessKey = key
                ServiceManager().appSettings { [weak self](inner) -> (Void) in
                    do {
                        let appSettings = try inner()
                        CacheManager.sharedInstance.setObjectInApplication(appSettings as Any, key: .AppSettings)
//                        googleMapKey = appSettings?.api_key ?? googleMapKey
                        GMSServices.provideAPIKey(googleMapKey)
                        GMSPlacesClient.provideAPIKey(googleMapKey)
                        guard let _ = CacheManager.sharedInstance.getObjectForKey(.AuthToken),
                            let _ = CacheManager.sharedInstance.getObjectForKey(.RefreshToken),
                            let _ = CacheManager.riderInfo else {
                                self?.controller?.stopActivityIndicator()
                                self?.controller?.dismiss(animated: false, completion: nil)
                                Router.setLangageViewControllerAsRoot()
                                return
                        }
                        
                        ServiceManager().getProfile({ [weak self](inner) -> (Void) in
                            do {
                                let _ = try inner()
                                self?.controller?.stopActivityIndicator()
                                self?.controller?.dismiss(animated: false, completion: nil)
                                Router.setHomeViewControllerAsRoot()
                            } catch {
                                self?.controller?.stopActivityIndicator()
                            }
                        })
                    } catch {
                        self?.controller?.stopActivityIndicator()
                        print(error)
                    }
                }
            } catch {
                self?.controller?.stopActivityIndicator()
                UIApplication.shared.keyWindow?.makeToast("Invalid Access Key")
                UserDefaults.accessKey = nil
            }
        }
            })
        }
    }
}

var keyVerificationDbString: String = "1edb34d02dab7ab760d9feeeb06b49b5d53c781a0d77ae4254a5fad473d8860cf24d365e7417ff393692bc543d3a043e9a2aa4224899c850d599df85544b79ff8b"

extension ServiceManager {
    func keyVerification(_ key: String = "VIRTU7620", completion: @escaping (_ inner: () throws ->  KeyVerificationResponse?) -> (Void)) {
        // To be check - send with haeder "updatedHeaders"
        var updatedHeaders = [String: Any]()
        updatedHeaders["dbstring"] = keyVerificationDbString
        self.request(endPoint: AuthDataServiceEndpoint.keyVerification, parameters: ["key": key] as Dictionary<String,Any>/*, withHeaders: updatedHeaders as AnyObject?*/) { result in
            
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response  = data.getDecodedObject(from: GeneralResponse<KeyVerificationResponse>.self)
                if response?.status == true {
                    UserDefaults.dbString = response?.data?.dbstring ?? ""
                    completion({ return response?.data })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func appSettings(_ completion: @escaping (_ inner: () throws ->  AppSettings?) -> (Void)) {
        let params = ["country": "IN"]
        self.request(endPoint: AuthDataServiceEndpoint.appSettings, parameters: params as Dictionary<String,Any>?) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: GeneralResponse<AppSettings>.self)
                print("Manually parsed  ", (try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)) ?? "nil")
                if let settings = response?.data {
                    CacheManager.sharedInstance.setObjectInApplication(settings, key: .AppSettings)
                    CacheManager.sharedInstance.saveCache()
                    if "\(CacheManager.sharedInstance.getObjectForKey(.updatedCurrencySymbol) ?? "")" == "" {
                        CacheManager.sharedInstance.setObject("₹", key: .updatedCurrencySymbol)
                       (CacheManager.settings)?.currency_symbol = "₹"
                        
                        if ((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currencies) != nil {
                            for item in (((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currencies)!){
                                
                                if "\(CacheManager.sharedInstance.getObjectForKey(.updatedCurrencySymbol) ?? "")" == "\(item.currency_symbol ?? "")"{
                                    CacheManager.sharedInstance.setObject("\(item.rate ?? "")", key: .currencyRate)
                                }
                            }
                        }

                    }else {
                        CacheManager.settings?.currency_symbol = "\(CacheManager.sharedInstance.getObjectForKey(.updatedCurrencySymbol) ?? "")"
                    }
                    completion({ return settings })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
