//
//  LoginViewModel.swift
//  Base
//
//  Created by Raju on 06/02/22.
//

import Foundation
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
//To be check - move to notification caonstant

extension String {
    static let SESSION_EXPIRED = "SESSION_EXPIRED"
    static let PROFILE_UPDATED = "PROFILE_UPDATED"
}

class LoginViewModel: AppViewModel {
    weak var controller: LoginViewController!
    required init(controller: LoginViewController) {
        self.controller = controller
    }
    
    var isAlreadyRegistered: Bool = false
    
    func loginWithFaceBook() {
        let fbLoginManager = LoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(permissions: ["email", "public_profile"], from: self.controller) { [weak self] (result, error) in
            if (error == nil) {
                let fbLoginResult: LoginManagerLoginResult = result!
                if ((result?.grantedPermissions != nil) && (fbLoginResult.grantedPermissions.contains("email"))) {
                    //                    ["fields": "id, name, first_name, last_name, picture.type(large), email, gender, birthday, phone"]
                    GraphRequest(graphPath: "me").start { (connection, result, error) in
                        if error == nil {
                            self?.controller?.startActivityIndicatorInWindow()
                            let dict = result as! [String : AnyObject]
                            let body = ["ccp":"", "tel_no":dict["phone"] as? String, "email": (dict["email"] as! String), "name": (dict["name"] as! String)] as AnyObject
                            self?.checkAccount(body: body, isSocialAccount: true)
                        }
                    }
                }
            }
        }
    }
    
    func loginWithGoogle() {
        GIDSignIn.sharedInstance.signOut()
        let config = GIDConfiguration(clientID: "597213563268-k64ljik3ijgg7teo6bimvmnsag9pc98e.apps.googleusercontent.com")

            GIDSignIn.sharedInstance.signIn(withPresenting: self.controller){ user, error in
            if error == nil {
                self.controller?.startActivityIndicatorInWindow()
                let body = ["ccp":"", "tel_no":"", "email": (user?.user.profile?.email ?? ""), "name": (user?.user.profile?.name ?? "")] as AnyObject
                self.checkAccount(body: body, isSocialAccount: true)
                GIDSignIn.sharedInstance.signOut()
            }
        }
    }
    
    func checkAccount(body: AnyObject, isSocialAccount: Bool = false) {
        ServiceManager().checkAccount(body) { [weak self](inner) in
            do {
                let response = try inner()
                self?.isAlreadyRegistered = response?.isSuccess == false
                if isSocialAccount {
                    let email = (body as? [String: String])?["email"] ?? ""
                    let name = (body as? [String: String])?["name"] ?? ""
                    if response?.isSuccess == false {
                        // do login
                        let login = Login()
                        login.email = email
                        self?.doLogin(login)
                    } else {
                        // do Register
                        self?.controller?.stopActivityIndicator()
                        self?.controller?.performSegue(withIdentifier: .GotoRegisterFromMobileNumber, sender: [email, name].lazy.joined(separator: ","))
                    }
                } else {
                    if self?.isAlreadyRegistered == false {
                        self?.controller?.stopActivityIndicator()
                        self?.sendOTP()
                        //   self.performSegue(withIdentifier: "GotoSignUpMobile", sender: nil)
                        return
                    }
                    if CacheManager.settings?.enable_login_verification?.boolValue == false {
                        self?.doLogin(self?.controller?.loginData() ?? Login())
                    } else {
                        self?.sendOTP()
                    }
                }
            } catch (let e) {
                self?.controller?.stopActivityIndicator()
                //  self.view.makeToast("\(String.Failed.localized) \(e.localizedDescription)")
            }
        }
    }
    
    func doLogin(_ login: Login) {
        self.controller?.startActivityIndicatorInWindow()
        ServiceManager().login(Token.self, login: login) { [weak self] (inner) -> (Void) in
            do {
                let response = try inner()
                if response?.status == true {
                    ServiceManager().getProfile({ [weak self] (inner) -> (Void) in
                        self?.controller?.stopActivityIndicator()
                        do {
                            let _ = try inner()
                            Router.setDashboardViewControllerAsRoot()
                        } catch {
                            //                            self.view.makeToast((error as? CoreError)?.description)
                        }
                    })
                } else {
                    self?.controller?.stopActivityIndicator()
//                    self?.controller?.view?.makeToast(response?.message)
                    self?.controller?.showAlertWithMessage(response?.message ?? "", phrase1: "Ok".localized,  action: UIAlertAction.Style.default, completion: { (style) in
                        if style == .default {
                            ServiceManager().deleteAccount()
                        }
                    })
                }
            } catch (let e) {
                self?.controller?.stopActivityIndicator()
                //                self.view.makeToast("\(String.Failed.localized) \(e.localizedDescription)")
            }
        }
    }

    func sendOTP() {
        let numberVerificationVC = NumberVerificationViewController.initFromStoryBoard(.Authenticate)
        numberVerificationVC.modalPresentationStyle = .overFullScreen
        numberVerificationVC.modalTransitionStyle = .crossDissolve
        numberVerificationVC.verificationSucceeds = { [weak self] verified in
            self?.controller?.dismiss(animated: false, completion: nil)
            if verified {
                let phone: String = self?.controller?.numberTextFld.text ?? ""
                let code = self?.controller?.countryCodeBtn.titleLabel?.text ?? ""
                let body = ["tel_no": phone, "ccp": "+91"] as AnyObject
                ServiceManager().sendOTP(OTP.self, body: body, completion: { [weak self] (inner) in
                    do {
                        let response = try inner()
                        if response?.status == true, let otp = response?.data?.otp, (self?.controller?.navigationController?.topViewController is OTPViewController) == false {
                            self?.controller?.performSegue(withIdentifier: .GotoOTPFromMobileNumber, sender: otp)
                        }
                        self?.controller?.navigationController?.topViewController?.view?.makeToast(response?.message)
                    } catch (let e) {
                        //                self.view.makeToast("\(String.Failed.localized) \(e.localizedDescription)")
                    }
                    self?.controller?.stopActivityIndicator()
                })
            } else {
                
            }
        }
        self.controller?.present(numberVerificationVC, animated: true, completion: nil)
    }
}

extension ServiceManager {
    func deleteAccount() {
        let params = ["type" : "rider","device_id" : deviceId] as? AnyObject
        self.request(endPoint: AuthDataServiceEndpoint.deleteAccount, parameters: params as? Dictionary<String,Any>, body: nil, pathSuffix: nil) { result in
            print(result)
        }
    }
    func checkAccount(_ body: AnyObject, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        self.request(endPoint: AuthDataServiceEndpoint.checkAccount, parameters: nil, body: body as? Dictionary<String,Any>, pathSuffix: nil) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response  = data.getDecodedObject(from: CommonRes.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func login<T: Decodable>(_ type: T.Type, login: Login, completion: @escaping (_ inner: () throws ->  GeneralResponse<T>?) -> (Void)) {
        let body = login.JSONRepresentation as AnyObject
        self.request(endPoint: AuthDataServiceEndpoint.login, body: body as? Dictionary<String,Any>) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: GeneralResponse<T>.self)
                if response?.status == true, let token: Token = response?.data as? Token {
                    CacheManager.sharedInstance.setObject(token.accessToken ?? "", key: .AuthToken)
                    CacheManager.sharedInstance.setObject(token.refreshToken ?? "", key: .RefreshToken)
                    CacheManager.sharedInstance.saveCache()
                }
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func getProfile(_ completion: @escaping (_ inner: () throws ->  UserInfo) -> (Void)) {
        self.request(endPoint: AuthDataServiceEndpoint.getProfile) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: GeneralResponse<UserInfo>.self)
                print("Manually parsed  ", (try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)) ?? "nil")
                if response?.message?.contains("jwt expired") == true {
                    completion({ throw CoreError.init(code: 1000, description: "Session Expired, Please login again".localized, innerError: nil, informations: nil) })
                    NotificationCenter.default.post(name: NSNotification.Name.init(.SESSION_EXPIRED), object: nil)
                    return
                }
                if let userInfo = response?.data {
                    CacheManager.sharedInstance.setObject(userInfo, key: .UserInfo)
                    CacheManager.sharedInstance.saveCache()
                    NotificationCenter.default.post(name: NSNotification.Name.init(.PROFILE_UPDATED), object: nil)
                    completion({ return userInfo })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func sendOTP<T: Decodable>(_ type: T.Type, body: AnyObject, completion: @escaping (_ inner: () throws ->  GeneralResponse<T>?) -> (Void)) {
        self.requestContentType = .json

        self.request(endPoint: AuthDataServiceEndpoint.sendOTP, body: body as? Dictionary<String,Any>) { result in
            
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: GeneralResponse<T>.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}

