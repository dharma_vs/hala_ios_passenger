//
//  InfoViewModel.swift
//  Base
//
//  Created by Raju on 14/02/22.
//

import UIKit
    
    class InfoViewModel: AppViewModel {
        weak var controller: InfoViewController!
        required init(controller: InfoViewController) {
            self.controller = controller
        }
    }

extension ServiceManager {
    func info(_ index: Int, completion: @escaping (_ inner: () throws -> Info?) -> (Void)) {
        self.request(endPoint: AuthDataServiceEndpoint.info, pathSuffix: "/\(index)") { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: GeneralResponse<Info>.self)
                if let info = response?.data {
                    completion({ return info })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}

