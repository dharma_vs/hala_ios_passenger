//
//  ForgotPasswordViewModel.swift
//  Base
//
//  Created by Raju on 14/02/22.
//

import Foundation

//To be check - check info class and rename correctly and move
struct Info: JSONSerializable {
    let id: Int?
    let title: String?
    let content: String?
}
 
    class ForgotPasswordViewModel: AppViewModel {
        weak var controller: ForgotPasswordViewController!
        required init(controller: ForgotPasswordViewController) {
            self.controller = controller
        }
    }


extension ServiceManager {
    func forgotPassword(_ body: AnyObject, completion: @escaping (_ inner: () throws ->  GeneralResponse<Token>?) -> (Void)) {
        self.request(endPoint: AuthDataServiceEndpoint.forgotPassword, body: body as? Dictionary<String,Any>) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: GeneralResponse<Token>.self)
                if response?.status == true, let token: Token = response?.data {
                    CacheManager.sharedInstance.setObject(token.accessToken ?? "", key: .AuthToken)
                    CacheManager.sharedInstance.setObject(token.refreshToken ?? "", key: .RefreshToken)
                    CacheManager.sharedInstance.saveCache()
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
