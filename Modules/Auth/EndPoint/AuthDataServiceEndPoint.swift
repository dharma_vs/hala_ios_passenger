//
//  AuthDataServiceEndPoint.swift
//  HalaMobility
//
//  Created by Admin on 19/02/22.
//

import Foundation

enum AuthDataServiceEndpoint: AddressableEndPoint {
    
    case checkAccount, login, getProfile, sendOTP, info, resetPassword, forgotPassword, categoryList, changePassword
    case keyVerification, appSettings, register, otpCreation, deleteAccount
    
    var endPointInfo: DataServiceEndpointInfo {
        switch self {
        case .checkAccount:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/account/check-account", method: .POST, authType: .none)
        case .login:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/account/login", method: .POST, authType: .none)
        case .getProfile:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/profile", method: .GET, authType: .user)
        case .sendOTP:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/account/otp-verify", method: .POST, authType: .none)
        case .keyVerification:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentMasterPathSuffix + "/key-verification", method: .GET, authType: .none)
        case .appSettings:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/account/app-setting", method: .GET, authType: .none)
        case .info:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentMasterPathSuffix + "/info", method: .GET, authType: .none)
        case .resetPassword:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/profile/reset-password", method: .PUT, authType: .user)
        case .forgotPassword:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/account/forgot-password", method: .POST, authType: .none)
        case .categoryList:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/category/list", method: .GET, authType: .user)
        case .changePassword:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/rider/profile/change-password", method: .PUT, authType: .user)
        case .register:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/account/signup", method: .POST, authType: .none)
            
        case .otpCreation:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/account/signup", method: .POST, authType: .none)
            
        case .deleteAccount:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentMasterPathSuffix + "/delete-log", method: .GET, authType: .none)

        }
    }
}

extension DataService {
    
    func otpCreation(_ info: OtpCreationReq = OtpCreationReq(), completion: @escaping (_ inner: () throws ->  OtpCreationRes) -> Void) {
        self.request(endPoint: AuthDataServiceEndpoint.otpCreation, parameters: info.JSONRepresentation) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response = data.getDecodedObject(from: GeneralResponse<OtpCreationRes>.self)
                if let data = response?.data, response?.status == true {
                    completion({return data})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}

struct OtpCreationReq: JSONSerializable {
    var clientID: Int?
    var countryCodeName, deviceID: String?
    var hasTruecaller: Bool?
    var language, os, phoneNumber: String?
    var phonePermission: Bool?
    var simSerial: [String]?
    var sequence: Int?
    var version: String?
}

struct OtpCreationRes: JSONSerializable {
    var verificationToken: String?
    var status: Int?
    var message, method: String?
    var tokenTTL: Int?

    enum CodingKeys: String, CodingKey {
        case verificationToken, status, message, method
        case tokenTTL = "tokenTtl"
    }
}
