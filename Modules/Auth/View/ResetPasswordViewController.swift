//
//  ResetPasswordViewController.swift
//  TaxiPickup
//
//  Created by LKB-007 on 10/01/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

import Toast_Swift

class ResetPasswordViewController: BaseViewController {

    @IBOutlet weak var backBtn: AppButton!
    @IBOutlet weak var appLogoImg: AppImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var passwordTxtFld: UITextField!
    @IBOutlet weak var confirmPasswordTxtFld: UITextField!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var madeinindiaLbl: AppLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  setTpBackground()
    }
    
    override func localize() {
        titleLbl.text = "Reset Password".localized
        passwordTxtFld.placeholder = "New Password".localized
        confirmPasswordTxtFld.placeholder = "Confirm Password".localized
        continueBtn.setTitle("Continue".localized, for: .normal)
        self.madeinindiaLbl?.attributedText = "Made with ♥ in India".localized.heartAttributted
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func continueBtnPressed() {
        view.endEditing(true)
        if let password1 = passwordTxtFld.text, !password1.isEmpty, let password2 = confirmPasswordTxtFld.text, !password2.isEmpty, password1 == password2 {
            self.startActivityIndicatorInWindow()
            ServiceManager().resetPassword(password1) { [weak self] (inner) -> (Void) in
                self?.stopActivityIndicator()
                do {
                    let response = try inner()
                    self?.view.makeToast(response?.message?.stringValue, duration: 2, position: .bottom, title: nil, image: nil, style: ToastStyle(), completion: { [weak self] (success) in
                        if response?.isSuccess == true {
                            self?.popToMobileNumberViewController()
                        }
                    })
                } catch (let e) {
//                    self.view.makeToast("\(String.Failed.localized) \(e.localizedDescription)")
                }
            }
        } else {
            view.makeToast("Passwords are not same do login".localized)
        }
    }
    
    func popToMobileNumberViewController() {
        if let lvc = navigationController?.viewControllers.filter({ $0 is LoginViewController }).first {
            self.navigationController?.popToViewController(lvc, animated: true)
        }
    }
}
