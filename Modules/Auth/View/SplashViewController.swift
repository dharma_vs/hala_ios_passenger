//
//  SplashViewController.swift
//  Taxi
//
//  Created by SELLADURAI on 2/10/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

class SplashViewController: BaseViewController, UIDynamicAnimatorDelegate {
    
    func getAccessKey() {
        let accessKeyVC = (self.presentedViewController as? UINavigationController)?.rootViewController as? AccessKeyViewController ?? AccessKeyViewController.initFromStoryBoard(.Authenticate)
        
        accessKeyVC.didGetAccessKey = { [weak self] key in
            UserDefaults.accessKey = key
            self?.viewModel.proceedWithKey(key)
        }
        if self.presentedViewController == nil {
            self.present(BaseNavigationController.init(rootViewController: accessKeyVC), animated: true, completion: nil)
        }
    }
    
    var timer: Timer?
    @IBOutlet weak var versionLbl: UILabel!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var animationContentView: UIView!
    @IBOutlet weak var animationContentViewHeight: NSLayoutConstraint!

    @IBOutlet weak var madeWithLoveLbl: AppLabel!
    var animator: UIDynamicAnimator!
    
    var viewModel: SplashViewModel!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.viewModel = SplashViewModel(controller: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animationContentViewHeight?.constant = (UIScreen.main.bounds.size.height/2 + self.logoImage.bounds.size.height/2)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.speak(text: Speech.welcome.rawValue)
        viewModel.checkAccessKey()
    }
    
    func dynamicAnimatorDidPause(_ animator: UIDynamicAnimator) {
        animator.removeAllBehaviors()
        viewModel.checkAccessKey()
    }
    
    override func localize() {
        self.versionLbl?.text = String.version
        madeWithLoveLbl?.attributedText = "Made with ♥ in India".heartAttributted
    }
    
    func updateApplication(){
//        if let url = URL(string: "https://apps.apple.com/us/app/id1517865497")
//        {
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//            }
//            else {
//                if UIApplication.shared.canOpenURL(url as URL) {
//                    UIApplication.shared.openURL(url as URL)
//                }
//            }
//        }
    }
    
    
    func stopTimer() {
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.stopTimer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


extension UIViewController {

}

var isSpeeking = false

extension NSObject {
    func speak(text: String) {
        let state = CacheManager.sharedInstance.getObjectForKey(.SpeechEnabled) as? Bool
        if (state == nil || state == true) && !isSpeeking {
            isSpeeking = true
            let speechSynthesizer = AVSpeechSynthesizer()
            let speechUtterance = AVSpeechUtterance(string: text)
            
            speechUtterance.rate = AVSpeechUtteranceDefaultSpeechRate
            speechUtterance.pitchMultiplier = 1.0
            speechUtterance.volume = 1.0
            
            speechSynthesizer.speak(speechUtterance)
            
            _ = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: { (tmr) in
                isSpeeking = false
            })
        }
    }
}

import AVFoundation

enum Speech: String {
    case welcome = "Welcome to Hala"
    case rideCreated = "Your ride has been crated"
    case rideAccepted = "You got a driver"
    case rideArrived = "Your driver has been arrived to your location"
    case rideStarted = "Your ride has started. Enjoy your ride"
    case rideEnded = "You have reached your destination. Please make payment"
    case feedbackRequest = "Please give your feedback"
    case feedbackThanks = "Thanks for your valuable feedback"
    case rideCancelledDriver = "Sorry! your ride has cancelled by driver"
    case rideCancelledRider = "Your ride has been cancelled"
    case rideNoDriver = "Sorry! no driver available at this moment"
    case message = ""
    case selectLanguage = "Please select your language"
    case languageUpdate = "Your language updated"
    case updateDestination = "Your destination has been changed"
}

