//
//  ForgotPasswordViewController.swift
//  TaxiPickup
//
//  Created by SELLADURAI on 15/03/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

import Toast_Swift
import CountryPickerView
import PhoneNumberKit

class ForgotPasswordViewController: BaseViewController {

    @IBOutlet weak var appLogoImg: AppImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var countryCodeBtn: UIButton!
    @IBOutlet weak var countryCodeBtnWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var backBtn: AppButton!
    @IBOutlet weak var madeinindiaLbl: AppLabel!

    
    var didCompleteForgotPasswordProcess: ((String) -> Void)!
    
    let cpv = CountryPickerView()
    let pnk = PhoneNumberKit()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.setTpBackground()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
     }
    override func localize() {
        titleLbl.text = "Forget Password".localized
        subTitleLbl.text = "Enter register email address or phone number".localized
        textField.placeholder = "Email/Phone".localized
        continueBtn.setTitle("Continue".localized, for: .normal)
        self.madeinindiaLbl?.attributedText = "Made with ♥ in India".localized.heartAttributted
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func countryCodeBtnPressed() {
        self.view.endEditing(true)
        
        cpv.delegate = self
        cpv.dataSource = self
        cpv.showCountryCodeInView = true
        cpv.showPhoneCodeInView = true
        cpv.showCountriesList(from: self)
    }
    
    @IBAction func continueBtnPressed() {
        if let text = textField.text, !text.isEmpty, let code = countryCodeBtn.titleLabel?.text!, !code.isEmpty {
            view.endEditing(true)
            self.startActivityIndicatorInWindow()
            
            var body = [String: String]()
            if text.isValidEmail() {
                body["email"] = text
            } else {
                do {
                    let _ = try pnk.parse("\(code) \(text)")
                    body["tel_no"] = text
                    body["ccp"] = "+91"
                } catch (_) {
//                    self.view.makeToast(String.Enter_valid_phone_number_select_correct_country_code.localized)
                }
            }
            
            if body.count > 0 {
                ServiceManager().forgotPassword(body as AnyObject) { [weak self] (inner) -> (Void) in
                    self?.stopActivityIndicator()
                    do {
                        let response = try inner()
                        self?.view.makeToast(response?.message, duration: 2, position: .bottom, title: nil, image: nil, style: ToastStyle(), completion: { [weak self] (success) in
                            if response?.status == true, let otp = response!.data!.otp {
                                self?.didCompleteForgotPasswordProcess("\(otp)")
                                self?.navigationController?.popViewController(animated: false)
                            }
                        })
                    } catch(let e) {
                        self?.view.makeToast("\("Failed".localized) \(e.localizedDescription)")
                    }
                }
            }
            
        } else {
            view.makeToast(String.Enter_valid_email_address_or_phone_number)
        }
    }
}

extension ForgotPasswordViewController: CountryPickerViewDelegate, CountryPickerViewDataSource {
    
    //  MARK: - CountryPickerViewDelegate
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        countryCodeBtn.setTitle(country.phoneCode, for: .normal)
    }
    
    //  MARK: - CountryPickerViewDataSource
    
    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select a Country".localized
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.textField {
            let text = textField.text?.replacingCharacters(in: Range(range, in: textField.text!)!, with: string)
            let isNumber = text?.isNumber == true
            
            countryCodeBtn.isHidden = !isNumber
            countryCodeBtnWidthConstraint.constant = isNumber ? 45 : 0
            
            if isNumber {
                return (text?.count ?? 0) <= 10
            }
        }
        return true
    }
}
