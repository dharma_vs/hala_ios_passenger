//
//  NumberVerificationViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 22/02/22.
//

import UIKit

class NumberVerificationViewController: BaseViewController {
    
    @IBOutlet weak var applogoImg: AppImageView!
    @IBOutlet weak var verifyingLbl: AppLabel!
    @IBOutlet weak var truecallerverificationImg: AppImageView!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var callingverifynumberLbl: AppLabel!
    @IBOutlet weak var noneedanswerLbl: AppLabel!
    @IBOutlet weak var madeinindiaLbl: AppLabel!
    
    var verificationSucceeds: ((Bool)->Void)?
    
    var viewModel: NumberVerificationViewModel!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.viewModel = NumberVerificationViewModel(controller: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.animateCallImage()
    }
    
    override func localize() {
        self.verifyingLbl?.text = "Verifying You".localized
        self.callingverifynumberLbl?.text = "Calling to Verify Your Number".localized
        self.noneedanswerLbl?.text = "No Need to Answer Your Call".localized
        self.madeinindiaLbl?.attributedText = "Made with ♥ in India".localized.heartAttributted
    }
}
