//
//  AccessKeyViewController.swift
//  Taxi
//
//  Created by ShamlaTech on 7/30/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

extension StoryBoradID {
    static let AccessKeyViewController = "AccessKeyViewController"
}

class AccessKeyViewController: BaseViewController {
    
    @IBOutlet weak var appLogoImg: AppImageView!
    @IBOutlet weak var accessKeyTextView: TextFieldWithTitleView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var dontHaveKeyBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    
    var didGetAccessKey:((String)->Void)?
    
    var viewModel: AccessKeyViewModel!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.viewModel = AccessKeyViewModel(controller: self)
    }
    
    @IBAction func submitBtnAction(_ sender: UIButton) {
        guard  let accessKey = self.accessKeyTextView.textField.text, accessKey.isEmpty == false else {
            self.view.makeToast("Enter valid Access Key".localized)
            return
        }
        self.viewModel.proceedWithKey(accessKey)
    }
    
    @IBAction func dontHaveKey(_ sender: UIButton) {
        let accessKeySupportVC = AccessKeySupportViewController.initFromStoryBoard(.Main)
        self.navigationController?.pushViewController(accessKeySupportVC, animated: true)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

}


extension CLPlacemark {
    
    var countryy: Countryy {
        var country = Countryy()
        country.country_full_name = self.country
        country.country_short_name = self.isoCountryCode
        return country
    }
}

extension UserDefaults {
    static var country: String? {
        get {
            return UserDefaults.standard.value(forKey: .Country) as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: .Country)
            UserDefaults.standard.synchronize()
        }
    }
    
    static var dbString: String? {
        get {
            return UserDefaults.standard.string(forKey: .DB_STRING)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: .DB_STRING)
            UserDefaults.standard.synchronize()
        }
    }
}
