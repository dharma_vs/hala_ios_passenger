//
//  AccessKeySupportViewController.swift
//  Taxi
//
//  Created by ShamlaTech on 8/10/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

extension StoryBoradID {
    static let AccessKeySupportViewController = "AccessKeySupportViewController"
}

class AccessKeySupportViewController: BaseViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var mainTitleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var mailBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func localize() {
        super.localize()
        mainTitleLbl.text = "Welcome To\nMOS Driver".localized
        subTitleLbl.text = "Please contact admin\nfor your new access key".localized
    }
    
    @IBAction func callBtnPressed() {
        call(to: CacheManager.settings?.support_number)
    }
    
    @IBAction func mailBtnPressed() {
        let emailStr: String = CacheManager.settings?.support_email ?? ""
        email(to: [emailStr])
    }
}

extension CacheManager {
    
    static var riderInfo: UserInfo? {
        return CacheManager.sharedInstance.getObjectForKey(.UserInfo) as? UserInfo
    }
    
}
