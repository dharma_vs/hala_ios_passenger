//
//  SignUpViewController.swift
//  TaxiPickup
//
//  Created by LKB-007 on 10/01/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

import CountryPickerView
import PhoneNumberKit
import Toast_Swift
import TTTAttributedLabel

class SignUpViewController: BaseViewController {
    
    @IBOutlet weak var backBtn: AppButton!
    @IBOutlet weak var appLogoSmallImg: AppImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var nameTextFld: AppTextField!
    @IBOutlet weak var emailTextFld: AppTextField!
    @IBOutlet weak var referralTextFld: AppTextField!
    @IBOutlet weak var phoneTextFld: UITextField!
    @IBOutlet weak var countryCodeBtn: UIButton!
    @IBOutlet weak var termsConditionBtn: UIButton!
    @IBOutlet weak var termsConditionLbl: TTTAttributedLabel!
    @IBOutlet weak var continueBtn: AppImageView!
    @IBOutlet weak var madeinindiaLbl: AppLabel!

    
    var userDetail: (Bool?, String?, String?, String?)! // (isPhone, value, ccp, name)
        
    let cpv = CountryPickerView()
    let pnk = PhoneNumberKit()
    
    var viewModel: SignUpViewModel!
    var isPhoneVerified: Bool = false
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.viewModel = SignUpViewModel(controller: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if userDetail != nil {
            self.nameTextFld?.text = userDetail.3
            if userDetail.0 == true {
                phoneTextFld.appEnabled = false
                emailTextFld.appEnabled = true
                countryCodeBtn.setTitle(userDetail.2?.hasPrefix("+") == true ? userDetail.2 : ("+"+(userDetail.2 ?? "91")), for: .normal)
                phoneTextFld.text = userDetail.1
            } else {
                emailTextFld.appEnabled = false
                phoneTextFld.appEnabled = true
                emailTextFld.text = userDetail.1
            }
        }
        
        termsConditionTextLayout()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func setup() {
     //   setTpBackground()
    }
    
    override func localize() {
        super.localize()
        
        titleLbl.text = "Sign Up".localized
        nameTextFld.placeholder = "Name".localized
        emailTextFld.placeholder = "Email Address (Optional)".localized
        phoneTextFld.placeholder = "Phone Number".localized
        referralTextFld.placeholder = "Referral Code (Optional)".localized
        self.madeinindiaLbl?.attributedText = "Made with ♥ in India".localized.heartAttributted
    }

    func termsConditionTextLayout() {
        let agree = "I agree to our".localized
        let termsCondition = "terms and condition".localized
        
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(agree)\(termsCondition)")
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.colorAccent, range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 13, weight: .regular) as Any, range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 13, weight: .semibold) as Any, range: NSMakeRange(agree.count, termsCondition.count))
        
        termsConditionLbl.delegate = self
        termsConditionLbl.linkAttributes = [NSAttributedString.Key.foregroundColor: UIColor.colorAccent.cgColor, NSAttributedString.Key.underlineStyle: 1]
        termsConditionLbl.attributedText = attributeString
        termsConditionLbl.addLink(to: URL(string: "local://termsCondition")!, with: NSMakeRange(agree.count, termsCondition.count))
    }
    
    @IBAction func countryCodeBtnPressed() {
        self.view.endEditing(true)
        
//        cpv.delegate = self
//        cpv.dataSource = self
//        cpv.showCountryCodeInView = true
//        cpv.showPhoneCodeInView = true
//        cpv.showCountriesList(from: self)
    }
    
    @IBAction func termsConditionBtnPressed() {
        termsConditionBtn.isSelected = !termsConditionBtn.isSelected
    }
    
    @IBAction func continueClickAction() {
        self.view.endEditing(true)
        self.viewModel.checkAccount()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == .GotoOTPFromSignupViewController {
            (segue.destination as! OTPViewController).phoneDetail = (countryCodeBtn.titleLabel!.text!, phoneTextFld.text!, sender as! String)
            (segue.destination as? OTPViewController)?.isAlreadyRegisterd = false
            (segue.destination as? OTPViewController)?.isFromSignup = true
            (segue.destination as? OTPViewController)?.otpVerificationSucceeds = { phoneDetail in
                self.navigationController?.popToViewController(self, animated: false)
                self.viewModel?.register()
            }
        }
    }

    func registerObject() -> Any? {
        
        func showMessage(_ message: String) -> Any? {
            self.view.makeToast(message)
            return nil
        }
        
        let register = Register()
        if let name = nameTextFld.text, !name.isEmpty {
            register.name = name
        } else {
            return showMessage("Please enter the name".localized)
        }
        
        if let email = emailTextFld.text, !email.isEmpty, email.isValidEmail() {
            register.email = email
        } else {
            register.email = ""
            return showMessage("Please enter the valid email".localized)
        }
        
        if let phone = phoneTextFld.text, !phone.isEmpty,phone.count == 10, let code = countryCodeBtn.titleLabel?.text!, !code.isEmpty {
            do {
                let _ = try pnk.parse("\(code) \(phone)")
                register.phone = phone
                register.code = code
            } catch (_) {
//                return showMessage("Enter valid phone number select correct country code".localized)
                return showMessage("Enter valid phone number".localized)
            }
        } else {
//            return showMessage("Enter valid phone number select correct country code".localized)
            return showMessage("Enter valid phone number".localized)
        }
        
        if let referral = referralTextFld.text, !referral.isEmpty {
            register.referral = referral
        }
        
        if termsConditionBtn.isSelected == false {
            return showMessage("Please accept the terms and conditions".localized)
        }
        
        return register
    }
}

extension SignUpViewController: CountryPickerViewDelegate, CountryPickerViewDataSource {
    
    //  MARK: - CountryPickerViewDelegate
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        countryCodeBtn.setTitle(country.phoneCode, for: .normal)
    }
    
    //  MARK: - CountryPickerViewDataSource
    
    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select a Country".localized
    }
}

extension SignUpViewController: TTTAttributedLabelDelegate {
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        if url.scheme == "local", url.host == "termsCondition" {
            let infoVC = InfoViewController.initFromStoryBoard(.Authenticate)
            infoVC.title = "Terms and conditions".localized
            infoVC.index = 3
            infoVC.navigationType = 4
            self.navigationController?.pushViewController(infoVC, animated: true)
        }
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTextFld {
            let text = textField.text?.replacingCharacters(in: Range(range, in: textField.text!)!, with: string)
            let isNumber = text?.isNumber == true
            
            countryCodeBtn.isHidden = !isNumber
            
            if isNumber {
                return (text?.count ?? 0) <= 10
            }
        }
        return true
    }
}

internal extension CacheManager {
    static var termsURL: String {
        return (CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.terms_url ?? ""
    }
}

extension UITextField {
    var appEnabled: Bool {
        set {
            self.isUserInteractionEnabled = newValue
            (self.superview as? AppImageView)?.backgroundThemeColor = newValue ? 75 : 16
            (self.superview as? AppImageView)?.borderTheme = newValue ? 8 : 16
        }
        get {
            return isUserInteractionEnabled
        }
    }
}
