//
//  InfoViewController.swift
//  Taxi
//
//  Created by ShamlaTech on 5/28/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit


class InfoViewController: BaseViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var menuBtn: UIButton!

    var index: Int! = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        backBtn.isHidden = true
//        menuBtn.isHidden = true
//        self.title == "menu" ? (menuBtn.isHidden = false) : (backBtn.isHidden = false)
//        self.titleLbl?.text = title //== "menu" ? "Terms & Conditions".localized : title
        startActivityIndicatorInWindow()
        ServiceManager().info(index) { [weak self](inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
                let info = try inner()
                DispatchQueue.main.async { [weak self] in
                    if let content = info?.content {
                        self?.contentTextView.text = content.replacingOccurrences(of: "</span></span></span>", with: "</span>").htmlDecoded
                        self?.contentTextView.textColor = .black
                        self?.view.bringSubviewToFront(self!.contentTextView)
                    }
                }
            } catch {
                
            }
        }

        // Do any additional setup after loading the view.
    }
    
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
extension String {
    var htmlDecoded: String {
        let decoded = try? NSAttributedString(data: Data(utf8), options: [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ], documentAttributes: nil).string
        return decoded ?? self
    }
}
