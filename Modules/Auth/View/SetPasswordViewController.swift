//
//  SetPasswordViewController.swift
//  TaxiPickup
//
//  Created by Prabakaran on 09/01/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

import Toast_Swift

class SetPasswordViewController: BaseViewController {
    
    @IBOutlet weak var backBtn: AppButton!
    @IBOutlet weak var appLogoImg: AppImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var passwordTxtFld: AppTextField!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var madeinindiaLbl: AppLabel!

    
    var phoneDetail: (String, String)! // (ccp, phone)
    var didCompleteForgotPasswordProcess: ((String) -> Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  self.setTpBackground()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
     }
    override func localize() {
        titleLbl.text = "Password Login".localized
        subTitleLbl.text = "Enter your password to login".localized
        passwordTxtFld.placeholder = "Password".localized
        continueBtn.setTitle("Continue".localized, for: .normal)
        forgotPasswordBtn.setTitle("Forget Password".localized, for: .normal)
        self.madeinindiaLbl?.attributedText = "Made with ♥ in India".localized.heartAttributted
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == .GotoForgetPasswordFromSetPassword {
            (segue.destination as! ForgotPasswordViewController).didCompleteForgotPasswordProcess = { [weak self] (otp) in
                self?.didCompleteForgotPasswordProcess(otp)
                DispatchQueue.main.async { [weak self] in
                    self?.navigationController?.popViewController(animated: false)
                }
            }
        }
    }
    
    @IBAction func continueBtnPressed() {
        if let password = passwordTxtFld.text, password.isEmpty == false {
            view.endEditing(true)
            let login = Login()
            login.phone = phoneDetail.1
            login.code = phoneDetail.0
            login.password = password
            
            self.startActivityIndicatorInWindow()
            ServiceManager().login(Token.self, login: login) { [weak self] (inner) -> (Void) in
                do {
                    let response = try inner()
                    if response?.status == true {
                        ServiceManager().getProfile({ [weak self] (inner) -> (Void) in
                            do {
                                let _ = try inner()
                                self?.view.makeToast(response?.message, duration: 2, position: .bottom, title: nil, image: nil, style: ToastStyle(), completion: { [weak self] (success) in
                                    Router.setDashboardViewControllerAsRoot()
                                })
                            } catch {
                                self?.stopActivityIndicator()
//                                self.view.makeToast((error as? CoreError)?.description)
                            }
                        })
                    } else {
                        self?.stopActivityIndicator()
                        self?.showAlertWithMessage(response?.message ?? "", phrase1: "Ok".localized, action: UIAlertAction.Style.default, completion: { (style) in
                            if style == .default {
                                
                            }
                        })
                    }
                } catch (let e) {
                    self?.stopActivityIndicator()
//                    self.view.makeToast("\(String.Failed.localized) \(e.localizedDescription)")
                }
            }
        }
    }
    
    @IBAction func forgotPasswordBtnPressed() {
        performSegue(withIdentifier: .GotoForgetPasswordFromSetPassword, sender: nil)
    }
}

