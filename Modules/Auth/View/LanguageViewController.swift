//
//  LanguageViewController.swift
//  TaxiPickup
//
//  Created by Prabakaran on 09/01/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit
import DropDown

class LanguageViewController: BaseViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var languageBtn: UIButton!
    @IBOutlet weak var continueButton: UIButton!

    lazy var dropDown: DropDown = {
        let dropDwn = DropDown()
        dropDwn.anchorView = self.languageBtn
        dropDwn.direction = .any
        dropDwn.backgroundColor = .white
        dropDwn.width = self.languageBtn.bounds.size.width
        dropDwn.dataSource = Language.allRawValues
        dropDwn.textFont = UIFont(name: "Montserrat-Regular", size: 14) ?? UIFont.systemFont(ofSize: 14)
        
        dropDwn.selectionAction = { [weak self] (index: Int, item: String) in
            self?.languageBtn.setTitle(item, for: .normal)
            dropDwn.hide()
        }
        return dropDwn
    }()
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
         }
    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  self.setTpBackground()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.speak(text: Speech.selectLanguage.rawValue)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - private
    
    override func setup() {
        super.setup()
        dropDown.selectRow(0)
//        languageBtn.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: languageBtn.bounds.size.width * 0.6557377049, bottom: 5, right: 0)
        languageBtn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: (UIScreen.main.bounds.width * 0.55))
    }
    
    override func localize() {
        super.localize()
        titleLbl.text = "Select Language".localized
        languageBtn.setTitle(Language.english.rawValue.localized, for: .normal)
        continueButton.setTitle("Continue".localized, for: .normal)
    }
    
    // MARK: - Button click actions
    
    @IBAction func languageBtnPressed() {
        self.dropDown.width = self.languageBtn.bounds.size.width
        self.dropDown.show()
    }
    
    @IBAction func continueClickAction() {
        if let language = dropDown.selectedItem, let lang = Language(rawValue: language) {
            UserDefaults.selectedLanguage = lang.rawValue
            performSegue(withIdentifier: .GotoLoginFromLanguage, sender: self)
        }
    }
}

