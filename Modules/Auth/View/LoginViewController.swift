//
//  MobileNumberViewController.swift
//  Taxi
//
//  Created by SELLADURAI on 10/02/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

import CountryPickerView
import PhoneNumberKit
import Toast_Swift
import AuthenticationServices
import TrueSDK

extension UserDefaults {
    static var accessKey: String? {
        get {
            return UserDefaults.standard.value(forKey: "AccessKey") as? String
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "AccessKey")
            UserDefaults.standard.synchronize()
        }
    }
}

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var backBtn: AppButton!
    @IBOutlet weak var appLogoImg: AppImageView!
    
    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var countryCodeBtn: AppButton!
    @IBOutlet weak var countryCodeBtnWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!

    @IBOutlet weak var numberTextFld: AppTextField!
    @IBOutlet weak var continueBtn: AppButton!
    
    @IBOutlet weak var connectWithLbl: AppLabel!
    @IBOutlet weak var appleBtn: UIButton!
    @IBOutlet weak var fbButton: AppButton!
    @IBOutlet weak var gpButton: AppButton!
    @IBOutlet weak var companyUserBtn: AppButton!
    @IBOutlet weak var orView: UIView!
    @IBOutlet weak var fbView: UIView!
    @IBOutlet weak var madeWithLoveLbl: AppLabel!
    
    let cpv = CountryPickerView()
    let pnk = PhoneNumberKit()
    
    var shouldHideBackButton = false
    var isAlreadyRegistered = false
    
    var viewModel: LoginViewModel!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.viewModel = LoginViewModel(controller: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setup() {
        super.setup()
        UserDefaults.companyLogin = false
        countryCodeBtn.isHidden = false
        countryCodeBtnWidthConstraint.constant = 50
        if self.view.frame.size.height < 590 {
            self.logoTopConstraint?.constant = 15
        }
        backBtn.isHidden = true
        if #available(iOS 13.0, *) {
            self.appleBtn?.isHidden = false
        } else {
            self.appleBtn?.isHidden = true
        }
        
        #if DBUG
//        numberTextFld?.text = "9876543212"
        #endif
        
        TCTrueSDK.sharedManager().delegate = self
        TCTrueSDK.sharedManager().viewDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == .GotoOTPFromMobileNumber {
            (segue.destination as! OTPViewController).phoneDetail = (countryCodeBtn.titleLabel!.text!, numberTextFld.text!, sender as! String)
            (segue.destination as? OTPViewController)?.isAlreadyRegisterd = self.viewModel.isAlreadyRegistered
            (segue.destination as? OTPViewController)?.otpVerificationSucceeds = { phoneDetail in
                self.navigationController?.popToViewController(self, animated: false)
                self.viewModel?.doLogin(self.loginData())
            }
        } else if segue.identifier == .GotoRegisterFromMobileNumber {
            let email = (sender as? String)?.components(separatedBy: ",").first
            let name = (sender as? String)?.components(separatedBy: ",").last
            (segue.destination as! SignUpViewController).userDetail = (false, email, nil, name)
        } else if segue.identifier == "GotoSignUpMobile"  {
            (segue.destination as! SignUpViewController).userDetail = (true, numberTextFld.text!, countryCodeBtn.titleLabel!.text!, nil)
        }
    }
    
    override func localize() {
        super.localize()
        connectWithLbl.text = "Connect With".localized
        titleLbl.text = "Enter your mobile number \n to get started".localized
        numberTextFld.placeholder = "Phone Number".localized
        continueBtn.setTitle("Continue".localized, for: .normal)
        companyUserBtn.setTitle("Switch to company login".localized, for: .normal)
        countryCodeBtn.setTitle(String.DefaultCountryCode, for: .normal)
        madeWithLoveLbl?.attributedText = "Made with ♥ in India".heartAttributted
        
    }
    @IBAction func companyuserBtnAction(_ sender: UIButton) {
        if companyUserBtn.titleLabel?.text == "Switch to company login".localized {
            companyUserBtn.setTitle("Switch to individual login".localized, for: .normal)
            self.orView.isHidden = true
            self.fbView.isHidden = true
            self.connectWithLbl.isHidden = true
            UserDefaults.companyLogin = true
        }else{
            companyUserBtn.setTitle("Switch to company login".localized, for: .normal)
            self.orView.isHidden = false
            self.fbView.isHidden = false
            self.connectWithLbl.isHidden = false
            UserDefaults.companyLogin = false
        }
    }
    
    @IBAction func fbPtnPressed() {
        self.viewModel?.loginWithFaceBook()
    }
    
    @IBAction func gpBtnPressed() {
        self.viewModel?.loginWithGoogle()
    }
    
    @IBAction func appleBtnPressed() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func countryCodeBtnPressed() {
        //        numberTextFld.resignFirstResponder()
        //
        //        cpv.delegate = self
        //        cpv.dataSource = self
        //        cpv.showCountryCodeInView = true
        //        cpv.showPhoneCodeInView = true
        //        cpv.showCountriesList(from: self)
    }
    
    @IBAction func continueBtnPressed() {
        tapGesture()
        guard let phone: String = numberTextFld?.text, phone.isNumber else {
            return
        }
        //TCTrueSDK.sharedManager().requestVerification(forPhone: <#PHONE_NUMBER_STRING>,countryCode: <#DEFAULT_COUNTRY_CODE>)
        
       // TCTrueSDK.sharedManager().requestVerification(forPhone: "9944686811", countryCode: "+91")
        
        

        if let code = countryCodeBtn.titleLabel?.text!, !code.isEmpty, !phone.isEmpty,phone.count == 10 {
            self.startActivityIndicatorInWindow()
            do {
                //                let _ = try pnk.parse("\(code) \(phone)")]
                let body = ["email": nil, "tel_no": phone.encrypted, "ccp": "+91"] as AnyObject
                self.viewModel?.checkAccount(body: body)
            } catch (_) {
                self.stopActivityIndicator()
                //self.view.makeToast("Enter valid Phone Number / Email Address".localized)
            }
        } else {
            self.view.makeToast("Enter valid Phone Number".localized)
        }
    }
    
    func loginData() -> Login {
        let login = Login()
        login.phone = numberTextFld.text!
        login.code = countryCodeBtn.titleLabel?.text!
        return login
    }
    
    @IBAction func tapGesture() {
        view.endEditing(true)
    }
}

extension LoginViewController: CountryPickerViewDelegate, CountryPickerViewDataSource {
    
    //  MARK: - CountryPickerViewDelegate
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        countryCodeBtn.setTitle(country.phoneCode, for: .normal)
    }
    
    //  MARK: - CountryPickerViewDataSource
    
    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select a Country".localized
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == numberTextFld {
            let text = textField.text?.replacingCharacters(in: Range(range, in: textField.text!)!, with: string)
            let isNumber = text?.isNumber == true
            
            //            countryCodeBtn.isHidden = !isNumber
            //            countryCodeBtnWidthConstraint.constant = isNumber ? 50 : 0
            
            if isNumber {
                return (text?.count ?? 0) <= 10
            }
        }
        return true
    }
}

class OTP: NSObject, Decodable {
    var otp: String?
}

extension String {
    var heartAttributted: NSAttributedString {
        let mutableAttributted = NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13, weight: .regular)])
        mutableAttributted.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.systemPink, range: (self as NSString).range(of: "♥", options: .caseInsensitive))
        return mutableAttributted
    }
}

@available(iOS 13.0, *)
extension LoginViewController: ASAuthorizationControllerDelegate {
    /// - Tag: did_complete_authorization
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
            switch authorization.credential {
            case let appleIDCredential as ASAuthorizationAppleIDCredential:
                // Create an account in your system.
                let userIdentifier = appleIDCredential.user
                let fullName = appleIDCredential.fullName
                let email = appleIDCredential.email
    //            print(userIdentifier)
    //            print(fullName)
                print(email)
                if email?.isValidEmail() ?? false {
                    var userInfo = [String: Any]()
                    userInfo["email"] = email
                    userInfo["firstName"] = fullName?.givenName
                    userInfo["lastName"] = fullName?.familyName
                    //GlobalValues.sharedInstance.strAppleUsername = (fullName?.givenName ?? "") + " " + (fullName?.familyName ?? "")
                    let body = ["ccp":"", "tel_no":"", "email": (email ?? ""), "name": (fullName?.givenName ?? "")] as AnyObject
                    FirebaseHelper.shared.updateAppleInfo(info: AppleInfo(email: email, firstName: fullName?.givenName, lastName: fullName?.familyName), key: userIdentifier.replacingOccurrences(of: ".", with: "_"))
                    self.viewModel.checkAccount(body: body, isSocialAccount: true)
               } else {
                    FirebaseHelper.shared.getAppleInfo(key: userIdentifier.replacingOccurrences(of: ".", with: "_")) { [weak self] (info) in
                        let body = ["ccp":"", "tel_no":"", "email": (info?.email ?? ""), "name": (info?.firstName ?? "") + " " + (info?.lastName ?? "")] as AnyObject
                        //GlobalValues.sharedInstance.strAppleUsername = (info?.firstName ?? "") + " " + (info?.lastName ?? "")
                        self?.viewModel.checkAccount(body: body as AnyObject, isSocialAccount: true)
                    }
    //                self.showAlertWith("Invalid Email")
                }
                break
            case let passwordCredential as ASPasswordCredential:
                // Sign in using an existing iCloud Keychain credential.
                let username = passwordCredential.user
                let password = passwordCredential.password
                // For the purpose of this demo app, show the password credential as an alert.
                DispatchQueue.main.async {
                    self.showPasswordCredentialAlert(username: username, password: password)
                }
            default:
                break
            }
    }
    
//    private func saveUserInKeychain(_ userIdentifier: String) {
//        do {
//            try KeychainItem(service: "com.a2solution.SpareParts", account: "userIdentifier").saveItem(userIdentifier)
//        } catch {
//            print("Unable to save userIdentifier to keychain.")
//        }
//    }
    
    private func showResultViewController(userIdentifier: String, fullName: PersonNameComponents?, email: String?) {
//        guard let viewController = self.presentingViewController as? ResultViewController
//            else { return }
//
//        DispatchQueue.main.async {
//            viewController.userIdentifierLabel.text = userIdentifier
//            if let givenName = fullName?.givenName {
//                viewController.givenNameLabel.text = givenName
//            }
//            if let familyName = fullName?.familyName {
//                viewController.familyNameLabel.text = familyName
//            }
//            if let email = email {
//                viewController.emailLabel.text = email
//            }
//            self.dismiss(animated: true, completion: nil)
//        }
    }
    
    private func showPasswordCredentialAlert(username: String, password: String) {
        let message = "The app has received your selected credential from the keychain. \n\n Username: \(username)\n Password: \(password)"
        let alertController = UIAlertController(title: "Keychain Credential Received",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    /// - Tag: did_complete_error
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
    }
}

extension LoginViewController: TCTrueSDKDelegate, TCTrueSDKViewDelegate {
    
    func didReceive(_ profile: TCTrueProfile) {
        print("didReceive")
    }
    
    func didFailToReceiveTrueProfileWithError(_ error: TCError) {
        print("didFailToReceiveTrueProfileWithError : ", error.description)
    }
    
    func didReceive(_ profileResponse: TCTrueProfileResponse) {
        print("didReceive response")
    }
    
}
