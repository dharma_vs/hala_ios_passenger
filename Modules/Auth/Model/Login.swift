//
//  Login.swift
//  Taxi
//
//  Created by SELLADURAI on 20/02/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

class Login: NSObject, JSONSerializable, CustomReflectable {
    var email: String!
    var code: String!
    var phone: String!
    
    var password = ""
    var deviceId = device_Id
    var deviceType = "ios"
    
    var customMirror: Mirror {
        return Mirror(Login.self, children: ["email": email, "ccp": code, "tel_no": phone ?? "", "password": password, "device_id": deviceId, "device_type": deviceType], displayStyle: Mirror.DisplayStyle.class, ancestorRepresentation: .generated)
    }
}
