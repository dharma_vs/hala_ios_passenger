//
//  AppSettings.swift
//  TaxiPickup
//
//  Created by SELLADURAI on 3/14/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import Foundation

class AppSettings: NSObject, JSONSerializable, NSCoding {
    var currency: String?
    var currency_symbol: String?
    var currency_denote: String? = "symbol"
    var show_error: String?
    var ride_later_min_duration: String?
    var ride_later_minimum_start_hours: String?
    var static_map_base_url: String?
    var api_key: String?
    var source_marker: String?
    var destination_marker: String?
    var app_version: String?
    var mandatory_update: String?
    var sms_enable: String?
    var ride_otp_need: String?
    var country_picker_need: String?
    var email_mandatory: String?
    var currency_name: String?
    var thousand_separator: String?
    var decimal_separator: String?
    var default_country_code: String?
    var sos: String?
    var suggestion_email: String?
    var support_number: String?
    var support_email: String?
    var quick_money_1: String?
    var quick_money_2: String?
    var quick_money_3: String?
    var polling_time: CustomType?
    var normal_ride_maximum_distance: String?
    var ride_later_minute_duration: String?
    var ride_later_minimum_start_in_minute: String?
    var ride_rent_minimum_start_in_days: String?
    var ride_rent_minute_duration: String?
    var terms_url: String?
    var developer_panel, passenger_referral_message, passenger_referral_title: String?
    var app_enable: CustomType?
    
    var country: String?
    var enable_login_verification: CustomType?
    var minimum_balance: CustomType?
    var android_passenger_app_version: String?
    var android_passenger_mandatory_update: String?
    var android_driver_app_version: String?
    var android_driver_mandatory_update: String?
    var ios_passenger_app_version: String?
    var ios_passenger_mandatory_update: String?
    var ios_driver_app_version: String?
    var ios_driver_mandatory_update: String?
    var book_for_someone: String?
    var driver_referral_title: String?
    var driver_referral_message: String?
    var updated_at: String?
    let currencies : [Currencies]?
    var hub_location : String?
     var draw_polyline_distance: String?
    
    
    var fleet, company, rental, outstation, delivery: CustomType?
    var map: CustomType?
    var country_code: CustomType?
    var dial_code: CustomType?
    var currency_format: CustomType?
    var time_zone: CustomType?
    var wallet_enable: CustomType?
    var street_ride: CustomType?
    var rate_card: CustomType?
    var fixed_fare: CustomType?
    var payment_mode: CustomType?
    var toll_fare_enable: CustomType?
    var manufacture_year_start: CustomType?
    var can_fleet_approve_driver: CustomType?
    var can_fleet_approve_vehicle: CustomType?
    var allow_driver_add_car_model: CustomType?
    var invoice_prefix: CustomType?
    var crn_prefix: CustomType?
    var driver_prefix: CustomType?
    var pad_len: CustomType?
    var commission_type: CustomType?
    var default_commission: CustomType?
    var tax_label: CustomType?
    var tax_percentage: CustomType?
    var comm_tax_percentage: CustomType?
    var location_change_charge: CustomType?
    var android_api_key: CustomType?
    var ios_api_key: CustomType?
    var pubnub_publish_key: CustomType?
    var pubnub_subscribe_key: CustomType?
    var android_fleet_app_version: CustomType?
    var android_fleet_mandatory_update: CustomType?
    var android_partner_app_version: CustomType?
    var android_partner_mandatory_update: CustomType?
    var android_manager_app_version: CustomType?
    var android_manager_mandatory_update: CustomType?
    var ios_fleet_app_version: CustomType?
    var ios_fleet_mandatory_update: CustomType?
    var ios_partner_app_version: CustomType?
    var ios_partner_mandatory_update: CustomType?
    var ios_manager_app_version: CustomType?
    var ios_manager_mandatory_update: CustomType?
    var passenger_referral_amount: CustomType?
    var driver_referral_amount: CustomType?
    var questionnaire_feedback: CustomType?
    var card_enable: CustomType?
    var has_own_vehicle: CustomType?
    var driver_evaluation: CustomType?
    var doroob_map_token: CustomType?
    var doroob_auth_token: CustomType?
    var doroob_style_english: CustomType?
    var doroob_style_arabic: CustomType?
    var algolia_key: CustomType?
    var algolia_token: CustomType?
    var algolia_index: CustomType?
    var airport_latitude: CustomType?
    var airport_longitude: CustomType?
    var bridge_latitude: CustomType?
    var bridge_longitude: CustomType?
    
    var countryCode: String {
        return self.default_country_code?.hasPrefix("+") == true ? (self.default_country_code ?? "") : ("+" + (self.default_country_code ?? ""))
    }
    
    enum CodingKeys: String, CodingKey {
        case country
        case currency
        case sos
        case fleet, company, rental, outstation, delivery
        case map
        case currencies
        case country_code
        case dial_code
        case currency_name
        case currency_symbol
        case thousand_separator
        case decimal_separator
        case currency_format
        case time_zone
        case show_error
        case sms_enable
        case wallet_enable
        case enable_login_verification
        case ride_otp_need
        case country_picker_need
        case email_mandatory
        case app_enable
        case street_ride
        case rate_card
        case fixed_fare
        case payment_mode
        case default_country_code
        case toll_fare_enable
        case manufacture_year_start
        case can_fleet_approve_driver
        case can_fleet_approve_vehicle
        case allow_driver_add_car_model
        case invoice_prefix
        case crn_prefix
        case driver_prefix
        case pad_len
        case suggestion_email
        case support_number
        case support_email
        case quick_money_1
        case quick_money_2
        case quick_money_3
        case minimum_balance
        case polling_time
        case hub_location
        case normal_ride_maximum_distance
        case ride_later_minute_duration
        case ride_later_minimum_start_in_minute
        case ride_rent_minimum_start_in_days
        case ride_rent_minute_duration
        case commission_type
        case default_commission
        case tax_label
        case tax_percentage
        case comm_tax_percentage
        case location_change_charge
        case static_map_base_url
        case api_key
        case android_api_key
        case ios_api_key
        case source_marker
        case destination_marker
        case terms_url
        case pubnub_publish_key
        case pubnub_subscribe_key
        case android_passenger_app_version
        case android_passenger_mandatory_update
        case android_driver_app_version
        case android_driver_mandatory_update
        case android_fleet_app_version
        case android_fleet_mandatory_update
        case android_partner_app_version
        case android_partner_mandatory_update
        case android_manager_app_version
        case android_manager_mandatory_update
        case ios_passenger_app_version
        case ios_passenger_mandatory_update
        case ios_driver_app_version
        case ios_driver_mandatory_update
        case ios_fleet_app_version
        case ios_fleet_mandatory_update
        case ios_partner_app_version
        case ios_partner_mandatory_update
        case ios_manager_app_version
        case ios_manager_mandatory_update
        case developer_panel
        case book_for_someone
        case passenger_referral_title
        case passenger_referral_message
        case passenger_referral_amount
        case driver_referral_title
        case driver_referral_message
        case driver_referral_amount
        case updated_at
        case questionnaire_feedback
        case card_enable
        case has_own_vehicle
        case driver_evaluation
        case doroob_map_token
        case doroob_auth_token
        case doroob_style_english
        case doroob_style_arabic
        case algolia_key
        case algolia_token
        case algolia_index
        case airport_latitude
        case airport_longitude
        case bridge_latitude
        case bridge_longitude
        case app_version
        case mandatory_update
    }
    
    
    /*
    "android_driver_app_version" = "1.0";
    "android_driver_mandatory_update" = true;
    "android_passenger_app_version" = "1.0";
    "android_passenger_mandatory_update" = true;
    "api_key" = "AIzaSyDax8pT2ANI_jeoYbo58rxrok0BikSba8s";
    "app_enable" = true;
    "app_version" = "1.5";
    "book_for_someone" = true;
    country = India;
    "country_picker_need" = false;
    "crn_prefix" = CRN;
    currency = INR;
    "currency_name" = Rupees;
    "currency_symbol" = "\U20b9";
    "decimal_separator" = ".";
    "default_country_code" = "+60";
    "destination_marker" = "http://project.virtuesense.com/mobile_dev/Taxi/Images/ic_destination.png";
    "developer_panel" = false;
    "driver_prefix" = D;
    "driver_referral_message" = "Get 50 For referral";
    "driver_referral_title" = "Driver Referral";
    "email_mandatory" = false;
    "enable_login_verification" = false;
    "fixed_fare" = true;
    "invoice_prefix" = "INV_";
    "ios_driver_app_version" = "1.2";
    "ios_driver_mandatory_update" = true;
    "ios_passenger_app_version" = "1.2";
    "ios_passenger_mandatory_update" = false;
    "mandatory_update" = true;
    "minimum_balance" = 50;
    "normal_ride_maximum_distance" = 5000;
    "pad_len" = 6;
    "passenger_referral_message" = "Get 50 For referral";
    "passenger_referral_title" = "Passenger Referral";
    "polling_time" = 30;
    "quick_money_1" = 100;
    "quick_money_2" = 200;
    "quick_money_3" = 500;
    rental = true;
    "ride_later_minimum_start_in_minute" = 10;
    "ride_later_minute_duration" = 10;
    "ride_otp_need" = false;
    "ride_rent_minimum_start_in_days" = 2;
    "ride_rent_minute_duration" = 30;
    "show_error" = true;
    "sms_enable" = true;
    sos = 199;
    "source_marker" = "http://project.virtuesense.com/mobile_dev/Taxi/Images/ic_source.png";
    "static_map_base_url" = "https://maps.googleapis.com/maps/api/staticmap?size=600x400&path=color:0xA80D01FF|weight:3|enc:";
    "street_ride" = true;
    "suggestion_email" = "pickuptaxiasia@gmail.com";
    "support_email" = "pickuptaxiasia@gmail.com";
    "support_number" = "+60 12 565 0482";
    "terms_url" = "-";
    "thousand_separator" = ",";
    "updated_at" = "2019-07-27T12:55:49.000Z";
    "wallet_enable" = true;
    
    */
    
    
    
    
    
    
    required init?(coder aDecoder: NSCoder) {
        
        self.currency = aDecoder.decodeObject(forKey: "currency") as? String
        self.currency_symbol = aDecoder.decodeObject(forKey: "currency_symbol") as? String
        self.currency_denote = aDecoder.decodeObject(forKey: "currency_denote") as? String
        self.show_error = aDecoder.decodeObject(forKey: "show_error") as? String
        self.ride_later_min_duration = aDecoder.decodeObject(forKey: "ride_later_min_duration") as? String
        self.ride_later_minimum_start_hours = aDecoder.decodeObject(forKey: "ride_later_minimum_start_hours") as? String
        self.static_map_base_url = aDecoder.decodeObject(forKey: "static_map_base_url") as? String
        self.api_key = aDecoder.decodeObject(forKey: "api_key") as? String
        self.source_marker = aDecoder.decodeObject(forKey: "source_marker") as? String
        self.destination_marker = aDecoder.decodeObject(forKey: "destination_marker") as? String
        self.app_version = aDecoder.decodeObject(forKey: "app_version") as? String
        self.mandatory_update = aDecoder.decodeObject(forKey: "mandatory_update") as? String
        sms_enable = aDecoder.decodeObject(forKey: "sms_enable") as? String
        ride_otp_need = aDecoder.decodeObject(forKey: "ride_otp_need") as? String
        country_picker_need = aDecoder.decodeObject(forKey: "country_picker_need") as? String
        email_mandatory = aDecoder.decodeObject(forKey: "email_mandatory") as? String
        currency_name = aDecoder.decodeObject(forKey: "currency_name") as? String
        thousand_separator = aDecoder.decodeObject(forKey: "thousand_separator") as? String
        decimal_separator = aDecoder.decodeObject(forKey: "decimal_separator") as? String
        default_country_code = aDecoder.decodeObject(forKey: "default_country_code") as? String
        sos = aDecoder.decodeObject(forKey: "sos") as? String
        suggestion_email = aDecoder.decodeObject(forKey: "suggestion_email") as? String
        support_number = aDecoder.decodeObject(forKey: "support_number") as? String
        support_email = aDecoder.decodeObject(forKey: "support_email") as? String
        quick_money_1 = aDecoder.decodeObject(forKey: "quick_money_1") as? String
        quick_money_2 = aDecoder.decodeObject(forKey: "quick_money_2") as? String
        quick_money_3 = aDecoder.decodeObject(forKey: "quick_money_3") as? String
        polling_time = aDecoder.decodeObject(forKey: "polling_time") as? CustomType
        normal_ride_maximum_distance = aDecoder.decodeObject(forKey: "normal_ride_maximum_distance") as? String
        ride_later_minute_duration = aDecoder.decodeObject(forKey: "ride_later_minute_duration") as? String
        ride_later_minimum_start_in_minute = aDecoder.decodeObject(forKey: "ride_later_minimum_start_in_minute") as? String
        ride_rent_minimum_start_in_days = aDecoder.decodeObject(forKey: "ride_rent_minimum_start_in_days") as? String
        ride_rent_minute_duration = aDecoder.decodeObject(forKey: "ride_rent_minute_duration") as? String
        terms_url = aDecoder.decodeObject(forKey: "terms_url") as? String
        developer_panel = aDecoder.decodeObject(forKey: "developer_panel") as? String
        country = aDecoder.decodeObject(forKey: "country") as? String
        enable_login_verification = aDecoder.decodeObject(forKey: "enable_login_verification") as? CustomType
        minimum_balance = aDecoder.decodeObject(forKey: "minimum_balance") as? CustomType
        android_passenger_app_version = aDecoder.decodeObject(forKey: "android_passenger_app_version") as? String
        android_passenger_mandatory_update = aDecoder.decodeObject(forKey: "android_passenger_mandatory_update") as? String
        android_driver_app_version = aDecoder.decodeObject(forKey: "android_driver_app_version") as? String
        android_driver_mandatory_update = aDecoder.decodeObject(forKey: "android_driver_mandatory_update") as? String
        ios_passenger_app_version = aDecoder.decodeObject(forKey: "ios_passenger_app_version") as? String
        ios_passenger_mandatory_update = aDecoder.decodeObject(forKey: "ios_passenger_mandatory_update") as? String
        ios_driver_app_version = aDecoder.decodeObject(forKey: "ios_driver_app_version") as? String
        ios_driver_mandatory_update = aDecoder.decodeObject(forKey: "ios_driver_mandatory_update") as? String
        book_for_someone = aDecoder.decodeObject(forKey: "book_for_someone") as? String
        passenger_referral_title = aDecoder.decodeObject(forKey: "passenger_referral_title") as? String
        passenger_referral_message = aDecoder.decodeObject(forKey: "passenger_referral_message") as? String
        driver_referral_title = aDecoder.decodeObject(forKey: "driver_referral_title") as? String
        driver_referral_message = aDecoder.decodeObject(forKey: "driver_referral_message") as? String
        updated_at = aDecoder.decodeObject(forKey: "updated_at") as? String
        hub_location = aDecoder.decodeObject(forKey: "hub_location") as? String

        currencies = aDecoder.decodeObject(forKey: "currencies") as? [Currencies]
       draw_polyline_distance = aDecoder.decodeObject(forKey: "draw_polyline_distance") as? String
        
        delivery = aDecoder.decodeObject(forKey: "delivery") as? CustomType
        map = aDecoder.decodeObject(forKey: "map") as? CustomType
        country_code = aDecoder.decodeObject(forKey: "country_code") as? CustomType
        dial_code = aDecoder.decodeObject(forKey: "dial_code") as? CustomType
        currency_format = aDecoder.decodeObject(forKey: "currency_format") as? CustomType
        time_zone = aDecoder.decodeObject(forKey: "time_zone") as? CustomType
        wallet_enable = aDecoder.decodeObject(forKey: "wallet_enable") as? CustomType
        street_ride = aDecoder.decodeObject(forKey: "street_ride") as? CustomType
        rate_card = aDecoder.decodeObject(forKey: "rate_card") as? CustomType
        fixed_fare = aDecoder.decodeObject(forKey: "fixed_fare") as? CustomType
        payment_mode = aDecoder.decodeObject(forKey: "payment_mode") as? CustomType
        toll_fare_enable = aDecoder.decodeObject(forKey: "toll_fare_enable") as? CustomType
        manufacture_year_start = aDecoder.decodeObject(forKey: "manufacture_year_start") as? CustomType
        can_fleet_approve_driver = aDecoder.decodeObject(forKey: "can_fleet_approve_driver") as? CustomType
        can_fleet_approve_vehicle = aDecoder.decodeObject(forKey: "can_fleet_approve_vehicle") as? CustomType
        allow_driver_add_car_model = aDecoder.decodeObject(forKey: "allow_driver_add_car_model") as? CustomType
        invoice_prefix = aDecoder.decodeObject(forKey: "invoice_prefix") as? CustomType
        crn_prefix = aDecoder.decodeObject(forKey: "crn_prefix") as? CustomType
        driver_prefix = aDecoder.decodeObject(forKey: "driver_prefix") as? CustomType
        pad_len = aDecoder.decodeObject(forKey: "pad_len") as? CustomType
        commission_type = aDecoder.decodeObject(forKey: "commission_type") as? CustomType
        default_commission = aDecoder.decodeObject(forKey: "default_commission") as? CustomType
        tax_label = aDecoder.decodeObject(forKey: "tax_label") as? CustomType
        tax_percentage = aDecoder.decodeObject(forKey: "tax_percentage") as? CustomType
        comm_tax_percentage = aDecoder.decodeObject(forKey: "comm_tax_percentage") as? CustomType
        location_change_charge = aDecoder.decodeObject(forKey: "location_change_charge") as? CustomType
        android_api_key = aDecoder.decodeObject(forKey: "android_api_key") as? CustomType
        ios_api_key = aDecoder.decodeObject(forKey: "ios_api_key") as? CustomType
        pubnub_publish_key = aDecoder.decodeObject(forKey: "pubnub_publish_key") as? CustomType
        pubnub_subscribe_key = aDecoder.decodeObject(forKey: "pubnub_subscribe_key") as? CustomType
        android_fleet_app_version = aDecoder.decodeObject(forKey: "android_fleet_app_version") as? CustomType
        android_fleet_mandatory_update = aDecoder.decodeObject(forKey: "android_fleet_mandatory_update") as? CustomType
        android_partner_app_version = aDecoder.decodeObject(forKey: "android_partner_app_version") as? CustomType
        android_partner_mandatory_update = aDecoder.decodeObject(forKey: "android_partner_mandatory_update") as? CustomType
        android_manager_app_version = aDecoder.decodeObject(forKey: "android_manager_app_version") as? CustomType
        android_manager_mandatory_update = aDecoder.decodeObject(forKey: "android_manager_mandatory_update") as? CustomType
        ios_fleet_app_version = aDecoder.decodeObject(forKey: "ios_fleet_app_version") as? CustomType
        ios_fleet_mandatory_update = aDecoder.decodeObject(forKey: "ios_fleet_mandatory_update") as? CustomType
        ios_partner_app_version = aDecoder.decodeObject(forKey: "ios_partner_app_version") as? CustomType
        ios_partner_mandatory_update = aDecoder.decodeObject(forKey: "ios_partner_mandatory_update") as? CustomType
        ios_manager_app_version = aDecoder.decodeObject(forKey: "ios_manager_app_version") as? CustomType
        ios_manager_mandatory_update = aDecoder.decodeObject(forKey: "ios_manager_mandatory_update") as? CustomType
        passenger_referral_amount = aDecoder.decodeObject(forKey: "passenger_referral_amount") as? CustomType
        driver_referral_amount = aDecoder.decodeObject(forKey: "driver_referral_amount") as? CustomType
        questionnaire_feedback = aDecoder.decodeObject(forKey: "questionnaire_feedback") as? CustomType
        card_enable = aDecoder.decodeObject(forKey: "card_enable") as? CustomType
        has_own_vehicle = aDecoder.decodeObject(forKey: "has_own_vehicle") as? CustomType
        driver_evaluation = aDecoder.decodeObject(forKey: "driver_evaluation") as? CustomType
        doroob_map_token = aDecoder.decodeObject(forKey: "doroob_map_token") as? CustomType
        doroob_auth_token = aDecoder.decodeObject(forKey: "doroob_auth_token") as? CustomType
        doroob_style_english = aDecoder.decodeObject(forKey: "doroob_style_english") as? CustomType
        doroob_style_arabic = aDecoder.decodeObject(forKey: "doroob_style_arabic") as? CustomType
        algolia_key = aDecoder.decodeObject(forKey: "algolia_key") as? CustomType
        algolia_token = aDecoder.decodeObject(forKey: "algolia_token") as? CustomType
        algolia_index = aDecoder.decodeObject(forKey: "algolia_index") as? CustomType
        airport_latitude = aDecoder.decodeObject(forKey: "airport_latitude") as? CustomType
        airport_longitude = aDecoder.decodeObject(forKey: "airport_longitude") as? CustomType
        bridge_latitude = aDecoder.decodeObject(forKey: "bridge_latitude") as? CustomType
        bridge_longitude = aDecoder.decodeObject(forKey: "bridge_longitude") as? CustomType
    }
    
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            if label != "currencies" {
                 aCoder.encode(value, forKey: label!)
            }
           
        }
    }
}
struct Currencies : Codable {
    
    var currency_country : String?
    var currency_name : String?
    var currency_code : String?
    var currency_symbol : String?
    var thousand_separator : String?
    var decimal_separator : String?
    var currency_format : String?
    var time_zone : String?
    var rate : String?

    enum CodingKeys: String, CodingKey {

        case currency_country = "currency_country"
        case currency_name = "currency_name"
        case currency_code = "currency_code"
        case currency_symbol = "currency_symbol"
        case thousand_separator = "thousand_separator"
        case decimal_separator = "decimal_separator"
        case currency_format = "currency_format"
        case time_zone = "time_zone"
        case rate = "rate"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        currency_country = try values.decodeIfPresent(String.self, forKey: .currency_country)
        currency_name = try values.decodeIfPresent(String.self, forKey: .currency_name)
        currency_code = try values.decodeIfPresent(String.self, forKey: .currency_code)
        currency_symbol = try values.decodeIfPresent(String.self, forKey: .currency_symbol)
        thousand_separator = try values.decodeIfPresent(String.self, forKey: .thousand_separator)
        decimal_separator = try values.decodeIfPresent(String.self, forKey: .decimal_separator)
        currency_format = try values.decodeIfPresent(String.self, forKey: .currency_format)
        time_zone = try values.decodeIfPresent(String.self, forKey: .time_zone)
        rate = try values.decodeIfPresent(String.self, forKey: .rate)
    }

}


// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
struct Welcomee: Codable {
    var status: Bool?
    var message: String?
    var data: DataClasss?
}

// MARK: - DataClass
struct DataClasss: Codable {
    var country, countryCode, dialCode, currencyName: String?
    var currency, currencySymbol, thousandSeparator, decimalSeparator: String?
    var currencyFormat, timeZone, showError, smsEnable: String?
    var walletEnable, enableLoginVerification, rideOtpNeed, countryPickerNeed: String?
    var emailMandatory, appEnable, streetRide, fleet: String?
    var company, rental, outstation, delivery: String?
    var rateCard, fixedFare, paymentMode, defaultCountryCode: String?
    var sos, tollFareEnable, manufactureYearStart, canFleetApproveDriver: String?
    var canFleetApproveVehicle, allowDriverAddCarModel, invoicePrefix, crnPrefix: String?
    var driverPrefix: String?
    var padLen: Int?
    var suggestionEmail, supportNumber, supportEmail, quickMoney1: String?
    var quickMoney2, quickMoney3, minimumBalance: String?
    var pollingTime: Int?
    var hubLocation, normalRideMaximumDistance, rideLaterMinuteDuration, rideLaterMinimumStartInMinute: String?
    var rideRentMinimumStartInDays, rideRentMinuteDuration, commissionType, defaultCommission: String?
    var taxLabel, taxPercentage, commTaxPercentage, locationChangeCharge: String?
    var map: String?
    var staticMapBaseURL: String?
    var apiKey, androidAPIKey, iosAPIKey: String?
    var sourceMarker, destinationMarker: String?
    var termsURL, pubnubPublishKey, pubnubSubscribeKey, androidPassengerAppVersion: String?
    var androidPassengerMandatoryUpdate, androidDriverAppVersion, androidDriverMandatoryUpdate, androidFleetAppVersion: String?
    var androidFleetMandatoryUpdate, androidPartnerAppVersion, androidPartnerMandatoryUpdate, androidManagerAppVersion: String?
    var androidManagerMandatoryUpdate, iosPassengerAppVersion, iosPassengerMandatoryUpdate, iosDriverAppVersion: String?
    var iosDriverMandatoryUpdate, iosFleetAppVersion, iosFleetMandatoryUpdate, iosPartnerAppVersion: String?
    var iosPartnerMandatoryUpdate, iosManagerAppVersion, iosManagerMandatoryUpdate, developerPanel: String?
    var bookForSomeone, passengerReferralTitle, passengerReferralMessage: String?
    var passengerReferralAmount: Int?
    var driverReferralTitle, driverReferralMessage: String?
    var driverReferralAmount: Int?
    var updatedAt, questionnaireFeedback, cardEnable, hasOwnVehicle: String?
    var driverEvaluation, doroobMapToken, doroobAuthToken, doroobStyleEnglish: String?
    var doroobStyleArabic, algoliaKey, algoliaToken, algoliaIndex: String?
    var airportLatitude, airportLongitude, bridgeLatitude, bridgeLongitude: String?
    var appVersion, mandatoryUpdate: String?
    var currencies: [Currencies]?

    enum CodingKeys: String, CodingKey {
        case country
        case countryCode = "country_code"
        case dialCode = "dial_code"
        case currencyName = "currency_name"
        case currency
        case currencySymbol = "currency_symbol"
        case thousandSeparator = "thousand_separator"
        case decimalSeparator = "decimal_separator"
        case currencyFormat = "currency_format"
        case timeZone = "time_zone"
        case showError = "show_error"
        case smsEnable = "sms_enable"
        case walletEnable = "wallet_enable"
        case enableLoginVerification = "enable_login_verification"
        case rideOtpNeed = "ride_otp_need"
        case countryPickerNeed = "country_picker_need"
        case emailMandatory = "email_mandatory"
        case appEnable = "app_enable"
        case streetRide = "street_ride"
        case fleet, company, rental, outstation, delivery
        case rateCard = "rate_card"
        case fixedFare = "fixed_fare"
        case paymentMode = "payment_mode"
        case defaultCountryCode = "default_country_code"
        case sos
        case tollFareEnable = "toll_fare_enable"
        case manufactureYearStart = "manufacture_year_start"
        case canFleetApproveDriver = "can_fleet_approve_driver"
        case canFleetApproveVehicle = "can_fleet_approve_vehicle"
        case allowDriverAddCarModel = "allow_driver_add_car_model"
        case invoicePrefix = "invoice_prefix"
        case crnPrefix = "crn_prefix"
        case driverPrefix = "driver_prefix"
        case padLen = "pad_len"
        case suggestionEmail = "suggestion_email"
        case supportNumber = "support_number"
        case supportEmail = "support_email"
        case quickMoney1 = "quick_money_1"
        case quickMoney2 = "quick_money_2"
        case quickMoney3 = "quick_money_3"
        case minimumBalance = "minimum_balance"
        case pollingTime = "polling_time"
        case hubLocation = "hub_location"
        case normalRideMaximumDistance = "normal_ride_maximum_distance"
        case rideLaterMinuteDuration = "ride_later_minute_duration"
        case rideLaterMinimumStartInMinute = "ride_later_minimum_start_in_minute"
        case rideRentMinimumStartInDays = "ride_rent_minimum_start_in_days"
        case rideRentMinuteDuration = "ride_rent_minute_duration"
        case commissionType = "commission_type"
        case defaultCommission = "default_commission"
        case taxLabel = "tax_label"
        case taxPercentage = "tax_percentage"
        case commTaxPercentage = "comm_tax_percentage"
        case locationChangeCharge = "location_change_charge"
        case map
        case staticMapBaseURL = "static_map_base_url"
        case apiKey = "api_key"
        case androidAPIKey = "android_api_key"
        case iosAPIKey = "ios_api_key"
        case sourceMarker = "source_marker"
        case destinationMarker = "destination_marker"
        case termsURL = "terms_url"
        case pubnubPublishKey = "pubnub_publish_key"
        case pubnubSubscribeKey = "pubnub_subscribe_key"
        case androidPassengerAppVersion = "android_passenger_app_version"
        case androidPassengerMandatoryUpdate = "android_passenger_mandatory_update"
        case androidDriverAppVersion = "android_driver_app_version"
        case androidDriverMandatoryUpdate = "android_driver_mandatory_update"
        case androidFleetAppVersion = "android_fleet_app_version"
        case androidFleetMandatoryUpdate = "android_fleet_mandatory_update"
        case androidPartnerAppVersion = "android_partner_app_version"
        case androidPartnerMandatoryUpdate = "android_partner_mandatory_update"
        case androidManagerAppVersion = "android_manager_app_version"
        case androidManagerMandatoryUpdate = "android_manager_mandatory_update"
        case iosPassengerAppVersion = "ios_passenger_app_version"
        case iosPassengerMandatoryUpdate = "ios_passenger_mandatory_update"
        case iosDriverAppVersion = "ios_driver_app_version"
        case iosDriverMandatoryUpdate = "ios_driver_mandatory_update"
        case iosFleetAppVersion = "ios_fleet_app_version"
        case iosFleetMandatoryUpdate = "ios_fleet_mandatory_update"
        case iosPartnerAppVersion = "ios_partner_app_version"
        case iosPartnerMandatoryUpdate = "ios_partner_mandatory_update"
        case iosManagerAppVersion = "ios_manager_app_version"
        case iosManagerMandatoryUpdate = "ios_manager_mandatory_update"
        case developerPanel = "developer_panel"
        case bookForSomeone = "book_for_someone"
        case passengerReferralTitle = "passenger_referral_title"
        case passengerReferralMessage = "passenger_referral_message"
        case passengerReferralAmount = "passenger_referral_amount"
        case driverReferralTitle = "driver_referral_title"
        case driverReferralMessage = "driver_referral_message"
        case driverReferralAmount = "driver_referral_amount"
        case updatedAt = "updated_at"
        case questionnaireFeedback = "questionnaire_feedback"
        case cardEnable = "card_enable"
        case hasOwnVehicle = "has_own_vehicle"
        case driverEvaluation = "driver_evaluation"
        case doroobMapToken = "doroob_map_token"
        case doroobAuthToken = "doroob_auth_token"
        case doroobStyleEnglish = "doroob_style_english"
        case doroobStyleArabic = "doroob_style_arabic"
        case algoliaKey = "algolia_key"
        case algoliaToken = "algolia_token"
        case algoliaIndex = "algolia_index"
        case airportLatitude = "airport_latitude"
        case airportLongitude = "airport_longitude"
        case bridgeLatitude = "bridge_latitude"
        case bridgeLongitude = "bridge_longitude"
        case appVersion = "app_version"
        case mandatoryUpdate = "mandatory_update"
        case currencies
    }
}

let defaultCountryCode = "+91"


