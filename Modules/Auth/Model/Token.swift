//
//  Token.swift
//  Taxi
//
//  Created by SELLADURAI on 19/02/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import Foundation

class Token: NSObject, Decodable {
    var accessToken: String?
    var refreshToken: String?
    var otp: Int?
}
