//
//  KeyVerificationResponse.swift
//  Base
//
//  Created by Raju on 14/02/22.
//

import Foundation

struct KeyVerificationResponse: JSONSerializable {
    let validity: Int?
    let dbstring: String?
}
