//
//  Register.swift
//  Taxi
//
//  Created by SELLADURAI on 16/02/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

class Register: NSObject, JSONSerializable, CustomReflectable {
    var name: String!
    var email: String!
    var code: String!
    var phone: String!
    var referral: String!
    
    var country_short_name: String!
    var country_full_name: String!
    
    var password = ""
    var deviceId = device_Id
    var deviceType = "ios"
    
    var customMirror: Mirror {
        return Mirror(Register.self, children: ["fullname": name, "email": email, "ccp": (code.contains("+") == true ? code : "+"+code), "tel_no": phone, "referral": referral, "password": password, "device_id": deviceId, "device_type": deviceType,"country_short_name": self.country_short_name,
                                                "country_full_name": self.country_full_name,], displayStyle: Mirror.DisplayStyle.class, ancestorRepresentation: .generated)
    }
}
