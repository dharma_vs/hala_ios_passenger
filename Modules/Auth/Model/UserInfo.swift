//
//  UserInfo.swift
//  TaxiPickup
//
//  Created by SELLADURAI on 3/18/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import Foundation

extension String {
    var toFloat: Float? {
        return Float(self)
    }
}

class UserInfo: NSObject, JSONSerializable, NSCoding {
    var id: Int?
    var name, email, image, countryCode: String?
    var phoneNo, password, registerDate, icImage: String?
    var countryShortName: String?
    var rating: CustomType?
    var categoryID: Int?
    var myReferralCode, referralCode, isEmailVerify, isPhoneNoVerify, updated_at: String?
    var walletAmount: CustomType?
    var status: String?
    var favoriteLocation: [FavoriteLocation]?
    var icNumber: CustomType?
    var card_info: [Card_info]?
    var company_id : Int?
    
    var selfie: String?
    var id_card: String?
    var document_status: String?
    var total_spend: CustomType?
    var company_info: CompanyInfo?
    var licence_info: LicenceInfo?

    var digio_reference: String?
    var digo_template: CustomType?
    var total_km_travelled: CustomType?
    var total_trips: CustomType?

    enum CodingKeys: String, CodingKey {
        case id, name, email, image, updated_at
        case icImage = "ic_image"
        case countryCode = "country_code"
        case phoneNo = "phone_no"
        case password
        case registerDate = "register_date"
        case countryShortName = "country_short_name"
        case rating
        case categoryID = "category_id"
        case myReferralCode = "my_referral_code"
        case referralCode = "referral_code"
        case isEmailVerify = "IsEmailVerify"
        case isPhoneNoVerify = "IsPhoneNoVerify"
        case walletAmount = "wallet_amount"
        case status
        case favoriteLocation = "favorite_location"
        case icNumber = "ic_number"
        case card_info = "card_info"
        case company_id = "company_id"
        
        case selfie
        case id_card
        case document_status
        case total_spend
        case company_info
        case licence_info
        
        case digio_reference
        case digo_template
        case total_km_travelled
        case total_trips
        
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        icNumber = try? values.decode(CustomType.self, forKey: .icNumber)
        rating = try? values.decode(CustomType.self, forKey: .rating)
        id = try? values.decode(Int.self, forKey: .id)
        name = try? values.decode(String.self, forKey: .name)
        email = try? values.decode(String.self, forKey: .email)
        image = try? values.decode(String.self, forKey: .image)
        countryCode = try? values.decode(String.self, forKey: .countryCode)
        phoneNo = try? values.decode(String.self, forKey: .phoneNo)
        password = try? values.decode(String.self, forKey: .password)
        registerDate = try? values.decode(String.self, forKey: .registerDate)
        updated_at = try? values.decode(String.self, forKey: .updated_at)
        countryShortName = try? values.decode(String.self, forKey: .countryShortName)
        icImage = try? values.decode(String.self, forKey: .icImage)
        categoryID = try? values.decode(Int.self, forKey: .categoryID)
        myReferralCode = try? values.decode(String.self, forKey: .myReferralCode)
        referralCode = try? values.decode(String.self, forKey: .referralCode)
        isEmailVerify = try? values.decode(String.self, forKey: .isEmailVerify)
        isPhoneNoVerify = try? values.decode(String.self, forKey: .isPhoneNoVerify)
        walletAmount = try? values.decode(CustomType.self, forKey: .walletAmount)
        status = try? values.decode(String.self, forKey: .status)
        favoriteLocation = try? values.decode([FavoriteLocation].self, forKey: .favoriteLocation)
        card_info = try? values.decode([Card_info].self, forKey: .card_info)
        company_id = try? values.decode(Int.self, forKey: .company_id)
        selfie = try? values.decode(String.self, forKey: .selfie)
        id_card = try? values.decode(String.self, forKey: .id_card)
        document_status = try? values.decode(String.self, forKey: .document_status)
        total_spend = try? values.decode(CustomType.self, forKey: .total_spend)
        company_info = try? values.decode(CompanyInfo.self, forKey: .company_info)
        licence_info = try? values.decode(LicenceInfo.self, forKey: .licence_info)
        digio_reference = try? values.decode(String.self, forKey: .digio_reference)
        digo_template = try? values.decode(CustomType.self, forKey: .digo_template)
        total_km_travelled = try? values.decode(CustomType.self, forKey: .total_km_travelled)
        total_trips = try? values.decode(CustomType.self, forKey: .total_trips)

    }
    
    required init?(coder aDecoder: NSCoder) {
        icNumber = aDecoder.decodeObject(forKey: "icNumber") as? CustomType
        id = aDecoder.decodeObject(forKey: "id") as? Int
        name = aDecoder.decodeObject(forKey: "name") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        image = aDecoder.decodeObject(forKey: "image") as? String
        countryCode = aDecoder.decodeObject(forKey: "countryCode") as? String
        phoneNo = aDecoder.decodeObject(forKey: "phoneNo") as? String
        password = aDecoder.decodeObject(forKey: "password") as? String
        registerDate = aDecoder.decodeObject(forKey: "registerDate") as? String
        updated_at = aDecoder.decodeObject(forKey: "updated_at") as? String
        countryShortName = aDecoder.decodeObject(forKey: "countryShortName") as? String
        rating = aDecoder.decodeObject(forKey: "rating") as? CustomType
        icImage = aDecoder.decodeObject(forKey: "icImage") as? String
        categoryID = aDecoder.decodeObject(forKey: "categoryID") as? Int
        myReferralCode = aDecoder.decodeObject(forKey: "myReferralCode") as? String
        referralCode = aDecoder.decodeObject(forKey: "referralCode") as? String
        isEmailVerify = aDecoder.decodeObject(forKey: "isEmailVerify") as? String
        isPhoneNoVerify = aDecoder.decodeObject(forKey: "isPhoneNoVerify") as? String
        walletAmount = aDecoder.decodeObject(forKey: "walletAmount") as? CustomType
        status = aDecoder.decodeObject(forKey: "status") as? String
        favoriteLocation = aDecoder.decodeObject(forKey: "favoriteLocation") as? [FavoriteLocation]
        company_id = aDecoder.decodeObject(forKey: "company_id") as? Int
        selfie = aDecoder.decodeObject(forKey: "selfie") as? String
        id_card = aDecoder.decodeObject(forKey: "id_card") as? String
        document_status = aDecoder.decodeObject(forKey: "document_status") as? String
        total_spend = aDecoder.decodeObject(forKey: "total_spend") as? CustomType
        company_info = aDecoder.decodeObject(forKey: "company_info") as? CompanyInfo
        licence_info = aDecoder.decodeObject(forKey: "licence_info") as? LicenceInfo
        digio_reference = aDecoder.decodeObject(forKey: "digio_reference") as? String
        digo_template = aDecoder.decodeObject(forKey: "digo_template") as? CustomType
        total_km_travelled = aDecoder.decodeObject(forKey: "total_km_travelled") as? CustomType
        total_trips = aDecoder.decodeObject(forKey: "total_trips") as? CustomType
    }
    
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
}

class FavoriteLocation: NSObject, JSONSerializable, NSCoding {
    var id: Int?
    var locationName, locationAddress: String?
    var lat, lng: Double?
    var type: Int?
    
    override init() {
        super.init()
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case locationName = "location_name"
        case locationAddress = "location_address"
        case lat, lng, type
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        locationName = aDecoder.decodeObject(forKey: "locationName") as? String
        locationAddress = aDecoder.decodeObject(forKey: "locationAddress") as? String
        lat = aDecoder.decodeObject(forKey: "lat") as? Double
        lng = aDecoder.decodeObject(forKey: "lng") as? Double
        type = aDecoder.decodeObject(forKey: "type") as? Int
    }
    
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
    
    var locationReq: FavoriteLocationReqInfo {
        var req = FavoriteLocationReqInfo()
        req.name = self.locationName
        req.address = self.locationAddress
        req.lat = self.lat
        req.lng = self.lng
        req.type = "\(self.type ?? 0)"
        return req
    }
}

struct FavoriteLocationReqInfo: JSONSerializable {
    var name: String?
    var lat: Double?
    var lng: Double?
    var address: String?
    var type: String?
    
    var location: FavoriteLocation {
        let loc = FavoriteLocation()
        loc.locationName = self.name
        loc.locationAddress = self.address
        loc.lat = self.lat
        loc.lng = self.lng
        loc.type = self.type?.intValue()
        return loc
    }
}
class Card_info: NSObject, JSONSerializable, NSCoding {
    
    let id : Int?
    var brand : String?
    var card_name : String?
    var card_no : String?
    var expiry_date : String?
    var cvv : String?
    var registration_id : String?
    
    override init() {
        self.id = nil
        self.brand = nil
        self.card_name = nil
        self.card_no = nil
        self.expiry_date = nil
        self.cvv = nil
        self.registration_id = nil
    }
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.brand = aDecoder.decodeObject(forKey: "brand") as? String
        self.card_name = aDecoder.decodeObject(forKey: "card_name") as? String
        self.card_no = aDecoder.decodeObject(forKey: "card_no") as? String
        self.expiry_date = aDecoder.decodeObject(forKey: "expiry_date") as? String
        self.cvv = aDecoder.decodeObject(forKey: "cvv") as? String
        self.registration_id = aDecoder.decodeObject(forKey: "registration_id") as? String


    }
    
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
}

class newCard: Card_info{
    
    override init() {
        super.init()
    }
    required init(from decoder: Decoder) throws {
        fatalError("init(from:) has not been implemented")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class CompanyInfo: NSObject, JSONSerializable, NSCoding {
    func encode(with coder: NSCoder) {
        
    }
    
    required init?(coder: NSCoder) {
        
    }
    
//    var id: Int?
//    var registrationID, brand, cardName, cardNo: String?
//    var expiryDate, cvv: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case registrationID = "registration_id"
//        case brand
//        case cardName = "card_name"
//        case cardNo = "card_no"
//        case expiryDate = "expiry_date"
//        case cvv
//    }
}

// MARK: - LicenceInfo
class LicenceInfo: NSObject, JSONSerializable, NSCoding, CustomReflectable {
    
    override init() {
        super.init()
    }
    
    func encode(with coder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            coder.encode(value, forKey: label!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        licenceNumber = aDecoder.decodeObject(forKey: "licenceNumber") as? CustomType
        issuedOn = aDecoder.decodeObject(forKey: "issuedOn") as? CustomType
        expiryOn = aDecoder.decodeObject(forKey: "expiryOn") as? CustomType
        licenceFront = aDecoder.decodeObject(forKey: "licenceFront") as? CustomType
        licenceRear = aDecoder.decodeObject(forKey: "licenceRear") as? CustomType
        registerNumber = aDecoder.decodeObject(forKey: "registerNumber") as? CustomType
    }
    
    var licenceNumber, issuedOn, expiryOn, licenceFront: CustomType?
    var licenceRear, registerNumber: CustomType?

    enum CodingKeys: String, CodingKey {
        case licenceNumber = "licence_number"
        case issuedOn = "issued_on"
        case expiryOn = "expiry_on"
        case licenceFront = "licence_front"
        case licenceRear = "licence_rear"
        case registerNumber = "reg_no"
    }
    
    var customMirror: Mirror {
        return Mirror(LicenceDetail.self, children: ["licence_number": licenceNumber?.stringValue ?? "", "issued_on": issuedOn?.stringValue ?? "", "expiry_on": expiryOn?.stringValue ?? "", "licence_front": licenceFront?.stringValue ?? "", "licence_rear": licenceRear?.stringValue ?? "", "reg_no": registerNumber?.stringValue ?? ""], displayStyle: Mirror.DisplayStyle.class, ancestorRepresentation: .generated)
    }
}
