//
//  ChatDataServiceEndpoint.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 09/03/22.
//
import Foundation

enum ChatDataServiceEndpoint: AddressableEndPoint {
    
    case notifyPushNotificationMessage
    
    var endPointInfo: DataServiceEndpointInfo {
        switch self {
        case .notifyPushNotificationMessage:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/notifyPushNotificationMessage", method: .POST, authType: .user)
        }
    }
}
