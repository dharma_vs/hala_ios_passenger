//
//  SingleChatController.swift
//  User
//
//  Created by CSS on 05/03/18.
//  Copyright © 2018 Appoets. All rights reserved.
//

import UIKit
import Haneke

extension UIImageView {
    
    func makeRoundedCorner(){
        
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.bounds.width/2
        
    }
    
}

class SingleChatViewController: BaseViewController {
    var typingSkipOnce: Bool = false
    var typingTimer: Timer?
    @IBOutlet private var backBtn : UIButton!
    @IBOutlet private var userTyping : UILabel!
    @IBOutlet private var userName : UILabel!
    @IBOutlet private var tableView : UITableView!
    @IBOutlet private var bottomConstraint : NSLayoutConstraint!
    @IBOutlet private var textViewSingleChat : UITextView!
    
    @IBOutlet private weak var viewSend : UIView!
    
    @IBOutlet private weak var progressViewImage : UIProgressView!
    
    private var navigationTapgesture : UITapGestureRecognizer!
    private var imageButtonView : UIImageView? // used to modify profile image after changes
    
    private let senderCellTextId = "chatSender"
    private let recieverCellTextId = "chatReceiver"
    private let senderMediaId = "senderMedia"
    private let reciverMediaId = "reciverMedia"
    
    private var datasource = [ChatResponse]()   // Current Chat Data
    
    private var currentUser : (UserDetails)! // Current User Data
    
    private var chatType :  ChatType!   // Current Chat eg:-  single or group
    
    private var currentUserId = 0
    var rideDriverID = 0

    private var isSendShown = false {
        
        didSet {
            
            self.viewSend.isHidden = !isSendShown
            
        }
        
    }
    
    private var addedObservers = [UInt]() // Added Observers
    
    private var viewJustAppeared = true
    
    private var isConversationToneEnabled = false
    
    // Deinit
    
    deinit {  // Remove the current firebase observer
        
//        FirebaseHelper.shared.remove(observers: self.addedObservers)
        
    }
    
    let avPlayerHelper = AVPlayerHelper()
    
    let chatSenderNib = "ChatSender"
    let chatRecieverNib = "ChatReciever"
    let imageCellSenderNib = "ImageCellSender"
    let imageCellRecieverNib = "ImageCellReciever"
    
    
}


extension SingleChatViewController {
    
    var userId: Int? {
        return CacheManager.riderInfo?.id
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        self.initialLoads()
        self.userName?.text = self.currentUser.first_name
        self.userTyping?.isHidden = true
        self.userTyping?.text = (self.currentUser.first_name ?? "") + " was typing..."
        self.backBtn.setImage(UIImage(named: "img_app_back")?.withRenderingMode(.alwaysTemplate), for: .normal)
    }
    
    //MARK:- Set Current User Data
    
    func setUserInfo(_ userInfo: DriverInfo?) {
        let user = UserDetails.init(first_name: userInfo?.name, id: userInfo?.id, last_name: nil, rating: "\(userInfo?.rating?.floatValue ?? 0)", mobile: userInfo?.mobile, picture: userInfo?.image)
        self.currentUser = user
        self.chatType = .single
        self.currentUserId = currentUser.id ?? 0
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateUI()
        self.viewJustAppeared = true
        self.isConversationToneEnabled = true//UserDefaults.standard.bool(forKey: Keys.list.conversationTones)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        FirebaseHelper.shared.remove(observers: self.addedObservers)
        self.navigationController?.navigationBar.removeGestureRecognizer(self.navigationTapgesture)
        self.typingTimer?.invalidate()
        self.typingTimer = nil
        self.clearBadgeCount()
        
        
    }
    
    //MARK:- Make Tone
    
    private func makeTone(){
        
        if self.isConversationToneEnabled {
            
            DispatchQueue.main.async {
                self.avPlayerHelper.play(file: "mesageTone.wav", isLooped: false)
            }
        }
    }
    
    //MARK:- Update UI
    
    private func updateUI() {
        
        self.tableView.reloadRows(at: tableView.indexPathsForVisibleRows ?? [IndexPath(row: 0, section: 0)], with: .fade)
        self.navigationTapgesture = UITapGestureRecognizer(target: self, action: #selector(self.navigationBarTapped))
        self.navigationController?.navigationBar.addGestureRecognizer(self.navigationTapgesture)
        self.navigationItem.title = String.removeNil(currentUser.first_name)+" "+String.removeNil(currentUser.last_name)
        if let urlStr = currentUser.picture, let url = URL.init(string: urlStr) {
            self.imageButtonView?.hnk_setImage(from: url, placeholder: nil, success: { [weak self](image) in
                self?.imageButtonView?.image = image?.resizeImage(newWidth: 30)?.withRenderingMode(.alwaysOriginal)
            }, failure: nil)
        }
        
    }
    
    //MARK:- Initial Loads
    
    private func initialLoads(){
        
        if traitCollection.forceTouchCapability == .available {  // Set Peek and pop for Image Preview
            self.registerForPreviewing(with: self, sourceView: self.tableView)  // Setting image preview for tableview cells
        }
        if #available(iOS 11.0, *) {
            self.navigationItem.largeTitleDisplayMode = .never
        }
        let backButtonArrow = UIBarButtonItem(image: #imageLiteral(resourceName: "back") , style: .plain, target: self, action: #selector(self.backAction))
        let fixedSpace = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: self, action: nil)
        let imageButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_dummy_user").resizeImage(newWidth: 30)?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(self.navigationBarTapped))
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: 30, height: 30)))
        imageView.makeRoundedCorner()
        imageView.contentMode = .scaleAspectFill
        imageButton.customView = imageView
        imageButton.customView?.isUserInteractionEnabled = true
        self.imageButtonView = imageView
        if let urlStr = currentUser.picture, let url = URL.init(string: urlStr) {
            imageView.hnk_setImage(from: url, placeholder: nil, success: {[weak self] (image) in
                imageView.image = image?.resizeImage(newWidth: 30)?.withRenderingMode(.alwaysOriginal)
            }, failure: nil)
        }
        
        self.navigationItem.leftBarButtonItems = [backButtonArrow,fixedSpace]
        
        self.addKeyBoardObserver(with: bottomConstraint)
        
        self.textViewSingleChat.delegate = self
        self.tableView.register(UINib(nibName: chatSenderNib, bundle: .main), forCellReuseIdentifier: senderCellTextId)
        self.tableView.register(UINib(nibName: chatRecieverNib, bundle: .main), forCellReuseIdentifier: recieverCellTextId)
        
        self.viewSend.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.sendOnclick)))
        
        self.startObservers()
        
    }
    
    @IBAction private func backAction() {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction private func navigationBarTapped(){
        
        print("Navigstion bar tapped ")
        
    }
    
    // MARK:- Start Observers
    
    private func startObservers(){
        
        let chatPath = "users/" + firebaseUserID + fireBaseDB + "/Chats/" + rideId + "/Messages"//Common.getChatId(with: currentUser.id) else { return }
        
        let childObserver = FirebaseHelper.shared.observe(path : chatPath, with: .childAdded) {[weak self] (childValue) in
            
            if (self?.datasource.count ?? 0)>0, let child = childValue.first {
                
                if (child.response?.receive_on == "" || child.response?.receive_on == nil) && child.response?.receiver_id == "P_\(self?.userId ?? 0)" {
                    child.response?.receive_on = "\(self?.getUTCCurrentTimeMillis() ?? 0)"
                    FirebaseHelper.shared.update(chat: child.response!, key: child.key!, toUser: Int(child.response!.sender_id!.components(separatedBy: "_").last!) ?? 0)
                }
                
                DispatchQueue.main.async {
                    
                    self?.tableView.beginUpdates()
                    
                    if self?.datasource.filter({ $0.key == child.key }).count == 0 {
                        
                        self?.datasource.append(child)
                        self?.tableView.insertRows(at: [IndexPath(row: (self?.datasource.count ?? 0)-1, section: 0)], with: .fade)
                    }
                    
                    self?.tableView.endUpdates()
                    self?.tableView.scrollToRow(at: IndexPath(row: (self?.datasource.count ?? 0)-1, section: 0), at: .bottom, animated: false)
                    
                }
                self?.makeTone()
                
            } else {
                
                self?.datasource =  childValue
                if let child = childValue.first {
                    if (child.response?.receive_on == "" || child.response?.receive_on == nil) && child.response?.receiver_id == "P_\(self?.userId ?? 0)" {
                        child.response?.receive_on = "\(self?.getUTCCurrentTimeMillis() ?? 0)"
                        FirebaseHelper.shared.update(chat: child.response!, key: child.key!, toUser: Int(child.response!.sender_id!.components(separatedBy: "_").last!) ?? 0)
                    }
                }
                DispatchQueue.main.async {
                    self?.reload()
                }
            }
            
        }
        
        let modifedObserver = FirebaseHelper.shared.observe(path : chatPath, with: .childChanged) {[weak self] (childValue) in
            
            if let childNode = childValue.first, let index = self?.datasource.index(where: { (chat) -> Bool in
                chat.key == childNode.key
            })
            {
                
                self?.datasource[index] = childNode
                
                DispatchQueue.main.async {
                    self?.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
                }
                
            }
        }
        
        let typingChatPath = "users/" + firebaseUserID + fireBaseDB + "/Chats/" + rideId + "/Typing/P_\(self.currentUserId)"
        
        let typingObserver = FirebaseHelper.shared.observe(path : typingChatPath, with: .childChanged) { [weak self](childValue) in
            if self?.typingSkipOnce == true {
                self?.typingTimer?.invalidate()
                self?.typingTimer = nil
                self?.userTyping?.isHidden = false
                self?.typingTimer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: { [weak self] (timer) in
                    self?.userTyping?.isHidden = true
                })
            } else {
                self?.typingSkipOnce = true
            }
        }
        
        self.addedObservers.append(childObserver)
        self.addedObservers.append(modifedObserver)
        self.addedObservers.append(typingObserver)
        
    }
    
    @IBAction private func reload()
    {
        
        self.tableView.reloadData()
        if self.datasource.count>0{
            UIView.animate(withDuration: 0.5, animations: { [weak self] in // reload and scroll to last index
                self?.tableView.scrollToRow(at: IndexPath(row: (self?.datasource.count ?? 0)-1, section: 0), at: .top, animated: false)
            })
        }
        
    }
    
    @IBAction private func sendOnclick(){
        ServiceManager().notifyPushNotificationMessage(rideId: rideId, riderId: rideDriverID , message: self.textViewSingleChat.text) { [weak self] (innner) -> (Void) in
        }

        FirebaseHelper.shared.write(to: self.currentUserId, with: self.textViewSingleChat.text, type : self.chatType)
        self.textViewSingleChat.text = .Empty
        self.textViewDidEndEditing(self.textViewSingleChat)
        
    }
    
    //MARK:- Back Button Action
    
    @IBAction private func backButtonAction(){
        
        if navigationController == nil {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        super.touchesBegan(touches, with: event)
        
        if let touch = touches.first {
            let currentPoint = touch.location(in: self.view)
            print(currentPoint)
            if !self.textViewSingleChat.bounds.contains(currentPoint){
                self.textViewSingleChat.resignFirstResponder()
            }
        }
    }
    
    private func setEmptyView(){
        
        DispatchQueue.main.async {
            
            self.tableView.backgroundView = self.datasource.count == 0 ?  {
                
                let label = UILabel(frame: self.tableView.bounds)
                label.textAlignment = .center
                label.text = noChatHistory
                //Common.setFont(to: label, isTitle: true)
                //                setFont(TextField: nil, label: label, Button: nil, size: nil)
                return label
                
                }() : nil
            
        }
    }
    
}

let noChatHistory = "No Chat History Found"

//MARK:- TableView Datasource and delegate

extension SingleChatViewController : UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //        if indexPath.row % 2 == 0 {
        
        if let chat = datasource[indexPath.row].response, let tableCell = tableView.dequeueReusableCell(withIdentifier: getCellId(from: chat), for: indexPath) as? ChatCell {
            
            if chat.sender_id == "P_\(self.userId ?? 0)" {
                
                tableCell.setSender(values: datasource[indexPath.row])
                
            } else {
                
                tableCell.setRecieved(values: datasource[indexPath.row], chatType: self.chatType)
                
                
            }
            
            return tableCell
        }
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.datasource[indexPath.row].response?.type == Mime.text.rawValue {
            return UITableView.automaticDimension
        }
        
        return 400 * (568 / UIScreen.main.bounds.height)
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    private func getCellId(from entity : ChatEntity)->String {
        
        if entity.sender_id == "P_\(self.userId ?? 0)" {
            
            return entity.type == Mime.text.rawValue ? senderCellTextId : senderMediaId
            
        } else {
            
            return entity.type == Mime.text.rawValue ? recieverCellTextId : reciverMediaId
            
        }
        
    }
    
}

let typeSomething = "Type Something"

//MARK:- UITextViewDelegate

extension SingleChatViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == typeSomething {
            textView.text = .Empty
            textView.textColor = .black
        }
        self.reload()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == .Empty {
            textView.text = typeSomething
            textView.textColor = .lightGray
            isSendShown = false
        }
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        
        if !textView.text.isEmpty {
            isSendShown = true
        } else  {
            isSendShown = false
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.clearAppBadgeCount()
    }
    
}

//MARK:- UIViewControllerPreviewingDelegate

extension SingleChatViewController : UIViewControllerPreviewingDelegate {
    
    // peek
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController?{
        return nil
        
    }
    
    // pop
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController){
        
        self.present(viewControllerToCommit, animated: true, completion: nil)
        
    }
}

extension NSObject {
    func incrementAppBadgeCount() {
//        UIApplication.shared.applicationIconBadgeNumber += 1
    }
    
    func setAppBadgeCount(_ count: Int) {
//        UIApplication.shared.applicationIconBadgeNumber = count
    }
    
    func clearAppBadgeCount() {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
}
