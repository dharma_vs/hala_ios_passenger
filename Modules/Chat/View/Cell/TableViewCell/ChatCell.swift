//
//  ChatCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import UIKit

class ChatCell: UITableViewCell {
    
    var userId: Int? {
        return CacheManager.riderInfo?.id
    }

    @IBOutlet private var viewCell : AppLabel!
    
    @IBOutlet private var labelCell : AppLabel?
    
    @IBOutlet private var labelTime : UILabel!
    
    @IBOutlet private var imageViewStatus : UIImageView!
    
    @IBOutlet private var imageViewAttachment : UIImageView?
    
    @IBOutlet private var activityIndicator : UIActivityIndicatorView?
    
    @IBOutlet private var labelSenderName : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func setSender(values : ChatResponse) {
        
       self.set(values: values.response, isRecieved: false)

    }
    
    
    //MARK:- Set Sender Detail only for Group
    
    func setSenderDetail(detail : String?){
        
        self.labelSenderName?.text = detail
        
    }
    
    func setRecieved(values : ChatResponse, chatType : ChatType = .single){
       
        guard let entity = values.response , let key = values.key, let sender = entity.sender_id, let senderId = Int(sender.components(separatedBy: "_").last ?? "") else {
            return
        }
        
        if (entity.read_on == "" || entity.read_on == nil) && entity.receiver_id == "P_\(self.userId ?? 0)" {
            entity.read_on = "\(self.getUTCCurrentTimeMillis())"
            FirebaseHelper.shared.update(chat: entity, key: key, toUser: senderId)
        }
        
        self.set(values: values.response, isRecieved: true)
        self.imageViewStatus.isHidden = true // hiding message status for reciever
        
        if chatType == .group, let senderId = values.response?.sender_id {
            self.labelSenderName?.text = senderId
            
        }
        
    }
    
    private func set(values : ChatEntity?, isRecieved : Bool){
        
        if values?.type != Mime.text.rawValue {
            self.imageViewAttachment?.image = #imageLiteral(resourceName: "Favorite")  //
            
        } else {
            
            self.labelCell?.text = values?.message
            
        }
        
        self.labelCell?.textColor = isRecieved ? .black : UIColor.colorAccent
        self.viewCell.backgroundColor = isRecieved ? UIColor.gStartColor : .white
        if isRecieved {
            
            self.labelTime.text = values?.receive_on?.dateTimeString
            
//            self.labelTime.text = Formatter.shared.relativePast(for: Date(timeIntervalSince1970: TimeInterval(String.removeNil(values?.receive_on)) ?? 0))
        } else {
            
            self.labelTime.text = values?.sent_on?.dateTimeString
            
//            self.labelTime.text = Formatter.shared.relativePast(for: Date(timeIntervalSince1970: TimeInterval(String.removeNil(values?.sent_on)) ?? 0))
            self.imageViewStatus.image = values?.read_on?.isEmpty == false ? #imageLiteral(resourceName: "recieved") :  values?.receive_on?.isEmpty == false ? #imageLiteral(resourceName: "recieved") : #imageLiteral(resourceName: "sent")
            self.imageViewStatus.tintColor = values?.read_on?.isEmpty == false ? UIColor(netHex: 0x8F0A00) : .black
        }
        self.layoutIfNeeded()
 
    }
    
}

extension String {
    var dateTimeString: String? {
        let timeInterval = Int64(self) ?? 0
        let myNSDate = Date(timeIntervalSince1970: Double(timeInterval))
        return myNSDate.dateFromFormater("hh:mm a")
    }
}
