//
//  SingleChatViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 09/03/22.
//

import Foundation
    
    class SingleChatViewModel: AppViewModel {
        weak var controller: SingleChatViewController!
        required init(controller: SingleChatViewController) {
            self.controller = controller
        }
    }

extension ServiceManager {
    
    func notifyPushNotificationMessage(rideId:String,riderId:Int,message:String,  completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        var body = [String: Any]()
        body["message"] = message
        body["ride_id"] = rideId
        body["driver_id"] = riderId
        
        self.request(endPoint: ChatDataServiceEndpoint.notifyPushNotificationMessage, body: body) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: CommonRes.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
    
