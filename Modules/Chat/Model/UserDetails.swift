//
//  UserDetails.swift
//  HalaMobility
//
//  Created by Admin on 03/03/22.
//

import Foundation

struct UserDetails: JSONSerializable {
    //    var device_id : String?
    //    var device_token: String?
    //    var device_type: String?
    //    var email : String?
    var first_name: String?
    var id: Int?
    var last_name : String?
    var rating : String?
    var mobile: String?
    var picture: String?
    //    var stripe_cust_id: String?
    //    var wallet_balance: Int?
    //    var user_id: Int?
    //    var request_id: Int?
    //    var status : Int?
    
}
