//
//  MycouponCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import UIKit

class MycouponCell: UITableViewCell {
    @IBOutlet weak var lbl_offer: UILabel!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var lbl_Code: UILabel!
    @IBOutlet weak var btn_code: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
