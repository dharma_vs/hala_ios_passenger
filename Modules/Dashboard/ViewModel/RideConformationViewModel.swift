//
//  RideConformationViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
//

import Foundation
import UIKit

enum PaymentMethod: String {
    
    case cash = "Cash"
    
    case wallet = "Wallet"
    
    case card = "Card"
    
    static var allRawValues: [String] {
        return [PaymentMethod.cash.rawValue,PaymentMethod.wallet.rawValue,PaymentMethod.card.rawValue]
    }
}

class RideConfirmationViewModel: NSObject {
    
    let vehicleName: String
    
    let estimatedFare: String
    
    let availableDrivers: String
    
    let pricePerWaitTime: String
    
    let capacityCount: String
    
    let distance: String
    
    let travelTime: String
    
    let taxes: String
    
    let fareInfo: String
    
    var selectedPaymentMethod: PaymentMethod = .cash
    
    var choosedCoupon: Coupon?
    
    
    init(category: Category?) {
        print("category : ", category)
        self.vehicleName = category?.displayName ?? ""
        
        self.estimatedFare = "\(Extension.updateCurrecyRate(fare: "\(category?.rideEstimate?.estimatedFare ?? 0)"))".currencyValue
        
        self.availableDrivers = "\(category?.nearByCabs?.count ?? 1)"
        
        self.pricePerWaitTime = "\(Extension.updateCurrecyRate(fare: "\(category?.fareBreakup?.waitingCostPerMinute ?? 0.0)"))".currencyValue
        
        self.distance = "\(category?.distance ?? "")".distanceValue
        
        self.travelTime = "\(category?.rideEstimate?.travelTimeInMinutes ?? 0)".timeMinValue
        
        self.taxes = "\(Extension.updateCurrecyRate(fare: "\(category?.rideEstimate?.taxes?.totalTax ?? 0)"))".currencyValue
        
        self.fareInfo = category?.message ?? ""
        
        self.capacityCount = category?.capacity ?? "0"
        
    }
    
    func updateRideConfirmationView(_ view: RideConfirmationView) {
        print("updateRideConfirmationView ")
        view.rideDetailValue.text = "\(availableDrivers)"
        
        view.rideDetail.text = "\("Available Drivers".localized)"
        
        view.capcityLbl.text = capacityCount
        
        view.moreRideDetailView.distanceLbl.text = "Distance".localized
        
        view.moreRideDetailView.distanceValueLbl.text = distance
        
        view.moreRideDetailView.travelTimeLbl.text = "Travel time".localized
        
        view.moreRideDetailView.travelTimeValueLbl.text = travelTime
        
        view.moreRideDetailView.estimatedFareLbl.text = "Estimated Fare".localized
        
        view.moreRideDetailView.estimatedFareValueLbl.text = estimatedFare
        
        view.moreRideDetailView.taxesLbl.text = "Taxes".localized
        view.moreRideDetailView.taxesLbl.isHidden = true
        view.moreRideDetailView.taxesValueLbl.text = taxes
        view.moreRideDetailView.taxesValueLbl.isHidden = true

        let escapedStr = fareInfo.replacingOccurrences(of: "<br/>", with: "\n")
        view.moreRideDetailView.fareNoteLbl.text = escapedStr
        
        view.couponTextField.placeholder = "Apply Coupon".localized
        
    }
    
}
