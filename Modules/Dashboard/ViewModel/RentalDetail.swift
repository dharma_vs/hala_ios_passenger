//
//  RentalDetail.swift
//  HalaMobility
//
//  Created by ADMIN on 19/07/22.
//

import Foundation

enum RentalStatus: Int {
    
    case NoBooking = 0
    
    case Created = 1
    
    case BookingConfirmed = 2
    
    case Arrived = 3
    
    case UnlockRequest = 4
    
    case UnlockApproved = 5
    
    case Running = 6
    
    case EndRequest = 7
    
    case EndApproved = 8
    
    case Completed = 9
    
    case UnlockRequestRejected = 10
    
    case EndRequestRejected = 11
    
    case UserCancelled = 12
    
    case AdminCancelled = 13
    
    case Waiting = 14
    var status: String {
        get {
            switch self {
            case .NoBooking:
                return "NONE".localized
            case .Created:
                return "CREATED".localized
            case .BookingConfirmed:
                return "CONFIRMED".localized
            case .Arrived:
                return "NEW RIDE".localized
            case .UnlockRequest:
                return "NEW RIDE".localized
            case .UnlockApproved:
                return "NEW RIDE".localized
            case .Running:
                return "NEW RIDE".localized
            case .EndRequest:
                return "END RIDE".localized
            case .EndApproved:
                return "END RIDE".localized
            case .Completed:
                return "COMPLETED".localized
            case .UnlockRequestRejected:
                return "NEW RIDE".localized
            case .EndRequestRejected:
                return "NEW RIDE".localized
            case .UserCancelled:
                return "NEW RIDE".localized
            case .AdminCancelled:
                return "NEW RIDE".localized
            case .Waiting:
                return "PAUSED".localized
            }
        }
    }
    
    /*var status: String {
        get {
            switch self {
            case .NewRide:
                return "NEW RIDE".localized
            case .Accepted:
                return "ACCEPTED".localized
            case .Arrived:
                return "ARRIVED".localized
            case .Started:
                return "On Ride".localized.uppercased()
            case .Completed:
                return "COMPLETED".localized
            case .UserCancelled:
                return "CANCELLED".localized
            case .DriverCancelled:
                return "CANCELLED".localized
            case .SystemCancelled:
                return "CANCELLED".localized
            case .PaymentCompleted:
                return "COMPLETED".localized
            case .WaitingTime:
                return "WAITING".localized

            case .None:
                return "None".localized
            }
        }
    }
    
    var tripStatus: String {
        get {
            switch self {
            case .NewRide:
                return "SCHEDULED".localized
            case .Accepted:
                return "ACCEPTED".localized
            case .Arrived:
                return "ARRIVED".localized
            case .Started:
                return "On Ride".localized.uppercased()
            case .Completed:
                return "COMPLETED".localized
            case .UserCancelled:
                return "CANCELLED BY RIDER".localized
            case .DriverCancelled:
                return "CANCELLED BY DRIVER".localized
            case .SystemCancelled:
                return "NO DRIVER - CANCELLED".localized
            case .PaymentCompleted:
                return "COMPLETED".localized
            case .None:
                return "None".localized
            case .WaitingTime:
                return "WAITING".localized

            }
        }
    }
    
    var color: UIColor {
        get {
            switch self {
            case .NewRide:
                return .colorStatusSearching
            case .Accepted:
                return .colorStatusAccepted
            case .Arrived:
                return .colorStatusArrived
            case .Started:
                return .colorStatusRunning
            case .Completed, .PaymentCompleted:
                return .colorStatusCompleted
            case .UserCancelled, .DriverCancelled, .SystemCancelled:
                return .colorStatusCancelled
            case .None:
                return .colorStatusNoDriver
            case .WaitingTime:
                return .colorStatusRunning
            }
        }
    }*/
}
