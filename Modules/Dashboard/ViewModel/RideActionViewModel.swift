//
//  RideActionViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
//

import Foundation
import UIKit

class RideActionViewModel: NSObject {
    
    let driverImage: String?
    
    let driverName: String?
    
    let rideStatus: RideStatus?
    
    let totalRides: String?
    
    let vehicleModel: String?
    
    let vehicleNumber: String?
    
    let vehicleCategoryName: String?
    
    let driverRating: Float
    
    let sourceCoordinate: LocationCoordinate
    
    let otp: String?
    
    let detail: RideDetail?
    
    init(_ rideDetail: RideDetail?) {
        
        self.detail = rideDetail
        
        self.driverImage = rideDetail?.driver_info?.image
        
        self.driverName = rideDetail?.driver_info?.name
        
        self.totalRides =  "Total Rides".localized + ": " + "\(rideDetail?.driver_info?.total_ride ?? 0)"
        
        self.vehicleModel = rideDetail?.vehicle_info?.model
        
        self.vehicleNumber = rideDetail?.vehicle_info?.number
        
        self.vehicleCategoryName = rideDetail?.vehicle_type_info?.display_name
        
        self.driverRating  = rideDetail?.driver_info?.rating?.floatValue ?? 0
        
        self.rideStatus = RideStatus.init(rawValue: rideDetail?.booking_status ?? 0)
        
        self.sourceCoordinate = rideDetail?.estimated_address?.source?.locationCoordinate ?? LocationCoordinate.zero
        
        self.otp = "OTP".localized + ": " + (rideDetail?.otp ?? "")
        
    }
    
    func updateRideActionView( view: RideActionView) {
        
        view.packageView?.isHidden = self.detail?.rental == nil
        view.packageNameLbl?.text = self.detail?.rental?.packageName?.stringValue
        view.startReadingLbl?.text = self.detail?.rental?.startReading?.stringValue
        view.startReadingLbl?.superview?.isHidden = self.detail?.rental?.startReading?.value == nil
        
        view.otpLbl?.text = self.otp
        
        view.driverImage.loadTaxiImage(self.driverImage?.urlFromString)
        
        view.driverName.text = self.driverName
        
        view.statusInfoLbl.text = self.rideStatus?.status
        
        view.tripsCountLbl.text = self.totalRides
        
        view.vehicleNameLbl.text = self.vehicleModel
        
        view.vehicleTypeLbl.text = self.vehicleCategoryName
        
        view.vehicleNumberLbl.text = self.vehicleNumber
        
        view.driverRating.rating = self.driverRating
        
        view.actionBtnHeight.constant = (self.rideStatus == RideStatus.Accepted) ? 35.0 : 0.0
        
        if self.rideStatus == RideStatus.Accepted {
            GoogleMapsHelper().calculateDistanceDuration(sourceCoordinate, toLocation: defaultMapLocation) { [weak self] (distanceDuration) in
                DispatchQueue.main.async { [weak self] in
                    view.statusInfoLbl.text = self?.rideStatus == RideStatus.Accepted ? "Will reach you in \(distanceDuration?.duration?.text ?? "")" : self?.rideStatus?.status
                }
            }
        }
    }
    
}

class RideStatusViewModel: NSObject {
    
    let driverImage: String?
    
    let driverName: String?
    
    let rideStatus: RideStatus?
    
    init(_ rideDetail: RideDetail?) {
        
        self.driverImage = rideDetail?.driver_info?.image
        
        self.driverName = rideDetail?.driver_info?.name
        
        self.rideStatus = RideStatus.init(rawValue: rideDetail?.booking_status ?? 0)
    }
    
    func updateRideActionView( view: RideStatusView) {
        
        view.userImage.loadTaxiImage(self.driverImage?.urlFromString)
        
        view.userName.text = self.driverName
        
        view.rideStatus.text = self.rideStatus?.status.uppercased()
        
    }
}
