//
//  PaymentActionViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 08/03/22.
//

import Foundation

class PaymentActionViewModel {
    var souceAddress: String?
    var dropAddress: String?
    var fare: String?
    var distance: String?
    var travelTime: String?
    var rideStatus: RideStatus!
    var userName: String?
    var userImageUrl: URL!
    var vehicleName: String?
    var detail: RideDetail
    
    init(rideDetail: RideDetail) {
        
        self.detail = rideDetail
        
        self.rideStatus = RideStatus.init(rawValue: rideDetail.booking_status ?? 0)
        
        self.souceAddress = rideDetail.actual_address?.source?.address
        
        self.dropAddress = rideDetail.actual_address?.destination?.address
        
        self.fare =  Extension.updateCurrecyRate(fare: "\(String(rideDetail.trip_info?.payable_amount ?? 0))").currencyValue

        self.distance = rideDetail.trip_info?.distance?.distanceValue ?? ""
        
        self.travelTime = "\(rideDetail.trip_info?.trip_time ?? 0)".timeValue
        
        self.userName = rideDetail.driver_info?.name
        
        self.userImageUrl = rideDetail.driver_info?.image?.urlFromString
        
        self.vehicleName = rideDetail.vehicle_info?.model
        
    }
    
    func updatePaymentActionView(_ view: PaymentActionView) {
        
        view.packageView?.isHidden = self.detail.rental == nil
        view.packageNameLbl?.text = self.detail.rental?.packageName?.stringValue
        view.startReadingLbl?.text = self.detail.rental?.startReading?.stringValue
        
        view.addressContentView?.sourceAddressLbl?.text = self.souceAddress
        view.addressContentView?.destinationAddressLbl?.text = self.dropAddress
        view.rideDistanceInfoView?.distanceLbl?.text = "Distance".localized
        view.rideDistanceInfoView?.durationLbl?.text = "Duration".localized
        view.rideDistanceInfoView?.distanceValue?.text = self.distance
        view.rideDistanceInfoView?.durationValue?.text = self.travelTime
        view.rideUserInfoView?.userImage.hnk_setImage(from: self.userImageUrl, placeholder: nil, success: { (image) in
            view.rideUserInfoView?.userImage?.image = image
        }, failure: nil)
        view.rideUserInfoView?.userName?.text = self.userName
        view.rideUserInfoView?.vehicleName?.text = self.vehicleName
        view.rideFareLbl?.text = self.fare
        view.changePaymentToCash?.isHidden = !(self.detail.payment_mode == "2")
        view.totalFareValueLbl?.text = Extension.updateCurrecyRate(fare: "\(String(detail.trip_info?.amount ?? 0))").currencyValue
        view.walletDetutionValueLbl?.text = "-" + Extension.updateCurrecyRate(fare: "\(String(detail.payment_details?.wallet_payment ?? 0))").currencyValue
        view.walletDetutionValueLbl?.superview?.isHidden = (self.detail.payment_details?.wallet_payment ?? 0) <= 0
        view.totalFareValueLbl?.superview?.isHidden = (self.detail.trip_info?.amount ?? 0) <= (self.detail.trip_info?.payable_amount ?? 0)
        let settings = CacheManager.settings
        view.destinationChangeFareLbl?.text = self.detail.destination_change_time != "" ? "Destination Change Fare $XXX".localized.replacingOccurrences(of: "$XXX", with: Extension.updateCurrecyRate(fare: "\(String(settings?.location_change_charge?.stringValue ?? ""))").currencyValue) : nil
        view.destinationChangeFareLbl?.isHidden = self.detail.destination_change_time == ""
        view.discountLbl?.text = Extension.updateCurrecyRate(fare: "\(String(detail.trip_info?.discount?.doubleValue ?? 0))").currencyValue + " " + "Discount applied".localized
        view.discountLbl?.isHidden = (detail.trip_info?.discount?.doubleValue ?? 0) <= 0
        
    }
}

internal extension CacheManager {
    static var currencySymbol: String {
        return ((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currency_symbol ?? "Rs").uppercased()
    }
    
    static var currencyShortName: String {
        return ((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currency ?? "INR").uppercased()
    }
}


extension ServiceManager {
    
    func updateRidePaymentMethod(_ info: UpdatePaymentMethodReq, completion: @escaping (_ inner: () throws ->  CustomType?) -> (Void)) {
        
        self.request(endPoint: PaymentDataServiceEndPoint.updateRidePaymentMethod, parameters: info.JSONRepresentation as Dictionary<String,Any>) { result in
            
            switch result {
            case .success(let data):
                if let status = data.getDecodedObject(from: CustomType.self) {
                    completion({ return status })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
