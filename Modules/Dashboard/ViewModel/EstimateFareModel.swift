//
//  EstimateFareModel.swift
//  HalaMobility
//
//  Created by ADMIN on 18/07/22.
//

import Foundation

struct EstimateFareModel: Codable {
    let status: Bool
    let message: String
    let data: EstimateFare
}

// MARK: - DataClass
struct EstimateFare: Codable {
    let vehicleTypeID: Int?
    let name, image: String?
    let fareBreakupID, baseFare, monthFare, weekFare: Int?
    let dayFare, hourFare, minuteFare, baseTime: Int?
    let unlockFare, rideFare: Int?
    let taxAmount, roundOff: Double?
    let estimatedFare: Int?

    enum CodingKeys: String, CodingKey {
        case vehicleTypeID = "vehicle_type_id"
        case name, image
        case fareBreakupID = "fare_breakup_id"
        case baseFare = "base_fare"
        case monthFare = "month_fare"
        case weekFare = "week_fare"
        case dayFare = "day_fare"
        case hourFare = "hour_fare"
        case minuteFare = "minute_fare"
        case baseTime = "base_time"
        case unlockFare = "unlock_fare"
        case rideFare = "ride_fare"
        case taxAmount = "tax_amount"
        case roundOff = "round_off"
        case estimatedFare = "estimated_fare"
    }
}
