//
//  DashboardViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
import Foundation
import UIKit
import GoogleMaps

class DashboardViewModel: NSObject {
    
    weak var controller: DashboardViewController!
    
    var currentRide: RideDetail? {
        didSet {
//            self.controller?.dropViews?.forEach {
//                $0.isHidden = self.currentRide?.ride_type == "rental"
//            }
        }
    }
    

    var hasOngoingRide: Bool
    
    var rides: [RideDetail]
    
    init(_ rideList: RideList?) {
        
        self.rides = rideList?.rides ?? []
        
        self.currentRide = rideList?.rides?.first
        
        self.hasOngoingRide = (rideList != nil && (rideList?.rides?.count ?? 0) > 0)
        
    }
    
    func updateDestinationAddressView() {
        for v in self.controller?.dropViews ?? [] {
            v.isHidden = self.controller?.fetchedRide?.ride_type?.lowercased() == "rental" && !(self.controller.newRideBool ?? true)
        }
        self.controller.rideInfoViewHeightConstant?.constant = self.controller?.fetchedRide?.ride_type?.lowercased() == "rental" && !(self.controller?.newRideBool ?? true) ? 65 : 130
    }
    
    func updateDashboardViewController(_ dashboardVC: DashboardViewController) {
        
        self.controller = dashboardVC
        
        if self.controller.selectedRide  != ""{
            for item in self.rides {
                if item.rideId == self.controller.selectedRide {
                    self.currentRide =  item
                }
            }
        }else {
            self.currentRide = self.rides.first
            
        }
        
        rideId = "\(self.currentRide?.id ?? 0)"
        
        let rideStatus = RideStatus.init(rawValue: self.currentRide?.booking_status ?? 0)!
        
        if rideStatus == .NewRide {
            if (self.currentRide?.request_countdown ?? 0) < -5 {
                ServiceManager().updateRideStatus(self.currentRide?.rideId, status: .reject,sourceLocation: self.controller.sourceLocationDetail!, rideStatus: rideStatus.rawValue , completion: { [weak self](inner) -> (Void) in
                    do {
                        let status = try inner()
                        print(status)
                    } catch {}
                })
            }
        }
        
        
        if self.controller.currentRideStatus != rideStatus {
                        
            self.controller.previousRideStatus = self.controller.currentRideStatus
            
            if !self.controller.newRideBool{
                self.controller.currentRideStatus = rideStatus
            }
            // ?? "" != "" ? self.currentRide?.rideId : self.controller.currentRideId ?? ""
//            if !UserDefaults.companyLogin{
//                if (self.currentRide?.rideId == nil && rideStatus == .None){
//                    self.controller.removeChatObserver()
//
//                    self.controller.statusContentView.insertArrangedSubview(dashboardVC.serviceSearchView, at: 0)
//                    self.controller.addressContentView.isHidden = false
////                    self.controller.dropViews?.forEach {
////                        $0.isHidden = self.currentRide?.ride_type == "rental"
////                    }
//                    updateDestinationAddressView()
//
//
//                    self.controller.newRidebtn.isHidden = true
//                    return
//                }
//            }
            
            ServiceManager().fetchRideDetails(UserDefaults.companyLogin ? self.controller.currentRideId : self.currentRide?.rideId, reason: "") { [weak self] (inner) -> (Void) in
                
                self?.controller.stopActivityIndicator()
                do {
                    let rideList = try inner()
                    self?.controller.fetchedRide = rideList?.rides?.first
                    
                    UserDefaults.setFetchRideStaus(true)
                    if UserDefaults.companyLogin{
                        self?.controller.currentRideStatus = RideStatus.init(rawValue: rideList?.rides?.first?.booking_status ?? 0)!

                    }
                    self?.updateCurrentRideUI(rideList: rideList ?? RideList(), dashboardVC: dashboardVC)
                    
                } catch {
                    UserDefaults.setFetchRideStaus(false)
                }
            }
            
            
            
        }
        
        if self.controller.newRideBool{
            self.hasOngoingRide = false
        }
        
        getCategories(nil)
        
    }
    func updateCurrentRideUI(rideList:RideList,dashboardVC: DashboardViewController) {
        UserDefaults.setFetchRideStaus(true)
        self.controller.newRidebtn.isHidden = false
        
        self.currentRide = (rideList.rides?.first)
        if  rides.count == 1 {
            self.controller.currentRideList.removeAll()
            if rideList.rides?.count ?? 0 > 0 {
                if !UserDefaults.companyLogin{
                    self.controller.currentRideList.append((rideList.rides?.first)!)
                }
                
            }
        }
        dashboardVC.currentRideCardDetails = (rideList.rides?.first)?.card_info
        func loadViewWithStatus(_ rideStatus: RideStatus?) {
            
            guard rideStatus != nil else {
                return
            }
            
            for arrangedSubView in dashboardVC.statusContentView.arrangedSubviews {
                arrangedSubView.removeFromSuperview()
            }
            
            self.controller.searchAnimationView?.stopAnimating()
            
            self.controller.addressContentView.isHidden = true
//            self.controller.dropViews?.forEach {
//                $0.isHidden = self.currentRide?.ride_type == "rental"
//            }
            updateDestinationAddressView()

            
            self.controller.sourceLocation.isUserInteractionEnabled = true
            self.controller.dropLocation.isUserInteractionEnabled = true
            self.controller.favBtn.isHidden = true
            
            self.controller.rideActionView.driverImage?.loadTaxiImage(self.currentRide?.driver_info?.image?.urlFromString)
            
            self.controller.rideActionView.driverName.text = self.currentRide?.driver_info?.name
            self.controller.rideActionView.statusInfoLbl.text = rideStatus == .Started ? "ON RIDE".localized.uppercased() : rideStatus?.status.uppercased()
                    
            self.controller.removeChatObserver()
            
            self.controller.sosBtn.isHidden = true
            
            self.controller.observeRideChat()
            
            switch rideStatus! {
                
            case .None:
                
                self.controller.removeChatObserver()
                if !UserDefaults.companyLogin {
                    self.controller.statusContentView.insertArrangedSubview(dashboardVC.serviceSearchView, at: 0)
                    
                }
                self.controller.addressContentView.isHidden = UserDefaults.companyLogin ? true : false
//                self.controller.dropViews?.forEach {
//                    $0.isHidden = self.currentRide?.ride_type == "rental"
//                }
                updateDestinationAddressView()


                self.controller.newRidebtn.isHidden = true
                
            case .NewRide:
                
                
                
                self.speak(text: Speech.rideCreated.rawValue)
                
                self.controller.heightConstraint.constant = UIScreen.main.bounds.height
               
                self.controller.insertView(self.controller.rideSearchingView)
                
                self.controller.searchAnimationView?.startAnimating()
                
            case .Accepted, .Arrived, .Started:
                
                switch rideStatus! {
                case .Accepted: self.speak(text: Speech.rideCreated.rawValue)
                    
                case .Arrived: self.speak(text: Speech.rideArrived.rawValue)
                    
                case .Started: self.speak(text: Speech.rideStarted.rawValue)
                    
                default:
                    break
                }
                
                if rideStatus == .Accepted || rideStatus == .Arrived || rideStatus == .Started {
                    
                    self.controller.addressContentView.isHidden = UserDefaults.companyLogin ? true : false
//                    self.controller.dropViews?.forEach {
//                        $0.isHidden = self.currentRide?.ride_type == "rental"
//                    }
                    updateDestinationAddressView()

                    self.controller.sourceLocation.isUserInteractionEnabled = false
                    //self.controller.dropLocation.isUserInteractionEnabled = false
                    
                    self.controller.sourceLocation.text = self.currentRide?.estimated_address?.source?.address
                    self.controller.dropLocation.text = self.currentRide?.estimated_address?.destination?.address
                    if !UserDefaults.companyLogin{
                        if self.currentRide != nil {
                            self.controller?.mapsHelper.getPlaceAddress(from: (self.currentRide?.estimated_address?.destination?.locationCoordinate)!, on: { [weak self] (locationDetail) in
                                self?.controller?.destinatioLocationDetail = locationDetail
                            })
                        }

                    }
                    
                    self.controller.favBtn.isHidden = true
                    
                    self.controller.sosBtn.isHidden = UserDefaults.companyLogin ? true : !(rideStatus == .Started)
                }
                
                self.controller.sourceLocationMarker.position = self.currentRide?.estimated_address?.source?.locationCoordinate ?? LocationCoordinate.zero
                
                self.controller.destinationLocationMarker.position = self.currentRide?.estimated_address?.destination?.locationCoordinate ?? LocationCoordinate.zero
                let failCases = Set([RideStatus.NewRide, .Started])
                self.controller.sourceLocationMarker.map = failCases.contains(rideStatus!) == false ? self.controller.mapView : nil
                
                self.controller.destinationLocationMarker.map = rideStatus != .NewRide ? self.controller.mapView : nil
                
                if UserDefaults.companyLogin{
                    self.controller.addCompanyRideDetialsView(rideDetails: self.currentRide!)
                    
                 }else{
                    self.controller.statusContentView.insertArrangedSubview(self.controller.rideActionView, at: 0)
                
                    let driverPath = "users/" + firebaseUserID + fireBaseDriverLocationDB + "/\(self.currentRide?.driver_id ?? 0)"
                
                    _ = FirebaseHelper.shared.observeDriverLocation(path: driverPath, with: .childChanged)
                }
                self.controller.rideActionView.viewModel = RideActionViewModel.init(self.controller?.fetchedRide ?? self.currentRide)
            case .WaitingTime :
                if #available(iOS 11.0, *) {
                    let window = UIApplication.shared.keyWindow
                    let topPadding = window?.safeAreaInsets.top
                    let bottomPadding = window?.safeAreaInsets.bottom
                    self.controller.waitingTimeViewheightConstraint.constant = UIScreen.main.bounds.height - ((74 + topPadding!) + (bottomPadding!))
                    
                }else{
                    
                    self.controller.waitingTimeViewheightConstraint.constant = UIScreen.main.bounds.height - 54
                    
                }
                self.controller.insertView(self.controller.waitingTimeView)
                self.controller.calculateRideWaitingTime()
                
            case .Completed:
                
                self.speak(text: Speech.rideEnded.rawValue)
                
                self.controller.paymentActionView.paybyCardBtn.isHidden = true
                
                if self.currentRide?.payment_mode == "2" || (self.currentRide?.payment_mode == "3" && (self.currentRide?.trip_info?.payable_amount ?? 0) > 0) {
                    
                    self.controller.paymentActionView.paybyCardBtn.isHidden = false
                    
                    
                    self.controller.paymentActionView.collectCashAmontLbl.attributedText = NSAttributedString(string: "Change Payment to Cash".localized, attributes:
                        [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue])

                }else {
                    self.controller.paymentActionView.collectCashAmontLbl.attributedText = NSAttributedString(string: "Pay amount by cash".localized, attributes:
                    nil)
                }
                self.controller.insertView(self.controller.paymentActionView)
                
                self.controller.paymentActionView.paymentCompletedBtn.isHidden = self.currentRide?.payment_mode?.lowercased() != "cash"
                
                self.controller.paymentActionView.viewModel = PaymentActionViewModel.init(rideDetail: self.controller?.fetchedRide ?? self.currentRide!)
                
            case .PaymentCompleted:
                
                if self.currentRide?.payment_mode?.lowercased() != "cash" {
                    
                //    self.controller.insertView(dashboardVC.ratingActionView)
                    self.controller.moveToPassengerFeedBackScreen()
                    self.controller.rideCheckHelper?.stopListening()
                    self.controller.removeChatObserver()
                    self.controller.rideCheckHelper = nil
                    self.controller.stopAnimationTimer()

                    self.speak(text: Speech.feedbackRequest.rawValue)
                    
                    self.controller.ratingActionView.viewModel = RatingActionViewModel.init(rideDetail: self.currentRide!)
                    
                } else {
                    
                    self.controller.insertView(self.controller.paymentActionView)
                    
                    self.controller.paymentActionView.paymentCompletedBtn.isHidden = self.currentRide?.payment_mode?.lowercased() != "cash"
                    
                    self.controller.paymentActionView.viewModel = PaymentActionViewModel.init(rideDetail: self.currentRide!)
                }
                if self.currentRide?.payment_mode == "3"{
                     self.controller.updateProfile()
                }
                
            case .UserCancelled, .DriverCancelled , .SystemCancelled:
                
                self.controller.addressContentView.isHidden = UserDefaults.companyLogin ? true : false
//                self.controller.dropViews?.forEach {
//                    $0.isHidden = self.currentRide?.ride_type == "rental"
//                }
                updateDestinationAddressView()

                if rideStatus == .UserCancelled {
                    self.speak(text: Speech.rideCancelledRider.rawValue)
                } else if rideStatus == .DriverCancelled {
                    self.speak(text: Speech.rideCancelledDriver.rawValue)
                }
                
                func updateCancelView() {
                    if let rideid = self.currentRide?.rideId {
                        ServiceManager().updateCancelView(rideid) { [weak self](inner) -> (Void) in
                            do {
                                _ = try inner()
                                DispatchQueue.main.async { [weak self] in
                                    self?.currentRide?.cancellation_rider_read = "1"
                                    self?.controller?.rideCheckHelper?.stopListening()
                                    self?.controller?.removeChatObserver()
                                    self?.controller?.rideCheckHelper = nil
                                    self?.controller?.stopAnimationTimer()
                                    self?.controller?.releaseCallBacks()
                                    Router.setDashboardViewControllerAsRoot()
                                }
                            } catch {
                            }
                        }
                    }
                }
                
                if rideStatus! == .SystemCancelled {
//                    if !self.controller.isCalledHubLocation {
//
//                        self.controller.heightConstraint.constant = UIScreen.main.bounds.height
//
//                        self.controller.insertView(self.controller.rideSearchingView)
//
//                        self.controller.searchAnimationView?.startAnimating()
//
//                        let hubLat = "\(((CacheManager.settings?.hub_location?.components(separatedBy: ",")as! [String])[0]))"
//
//                        let hubLong = "\(((CacheManager.settings?.hub_location?.components(separatedBy: ",")as! [String])[1]))"
//
//                        ServiceManager().updateRideStatusHubLocation(self.currentRide?.rideId, status: .retry,lat: hubLat,long:hubLong, rideStatus: 1,category_id: self.currentRide?.category_id ?? "", completion: { [weak self](inner) -> (Void) in
//                            do {
//                                let status = try inner()
//                                OperationQueue.main.addOperation { [weak self] in
//                                    self?.controller?.view?.makeToast(status?.message)
//                                    self?.controller?.currentRideStatus = nil
//                                    self?.controller?.rideCheckHelper?.resumeListening()
//                                }
//                            } catch {
//                                self?.controller?.rideCheckHelper?.resumeListening()
//                                print(error)
//                            }
//
//                        })
//                        self.controller?.isCalledHubLocation = true
//                        return
//                    }
                    self.controller?.rideCheckHelper?.stopListening()
                    self.controller?.rideCheckHelper = nil
                    self.speak(text: Speech.rideNoDriver.rawValue)
                    
                    if self.controller?.presentingViewController == nil {
                        self.controller?.showAlertWithMessage("UNAVAILABLE DRIVERS".localized, phrase1: "Yes".localized, phrase2: "No".localized, action: UIAlertAction.Style.default, .cancel) { [weak self](style) in
                            if style == .cancel {
                                self?.controller?.rideCheckHelper?.resumeListening()
                                updateCancelView()
                            } else {
                                
                                let hubLat = "\(((CacheManager.settings?.hub_location?.components(separatedBy: ",")as! [String])[0]))"
                                
                                let hubLong = "\(((CacheManager.settings?.hub_location?.components(separatedBy: ",")as! [String])[1]))"
                                
                                ServiceManager().updateRideStatusHubLocation(self?.currentRide?.rideId, status: .retry,lat: hubLat,long: hubLong, rideStatus: 1,category_id: self?.currentRide?.category_id ?? "", completion: { [weak self](inner) -> (Void) in
                                    do {
                                        let status = try inner()
                                        OperationQueue.main.addOperation { [weak self] in
                                            self?.controller?.view?.makeToast(status?.message?.stringValue)
                                        }
                                        UIApplication.shared.windows.first?.rootViewController?.homeViewController?.currentRideStatus = nil
                                        UIApplication.shared.windows.first?.rootViewController?.homeViewController?.checkRideList()
                                    } catch {
                                        UIApplication.shared.windows.first?.rootViewController?.homeViewController?.currentRideStatus = nil
                                        UIApplication.shared.windows.first?.rootViewController?.homeViewController?.checkRideList()
                                        print(error)
                                    }
                                })
                            }
                        }
                    }
                    
                    return
                }
                
                if self.currentRide?.cancellation_rider_read == "0" {
                    
                    self.controller.showAlertWithMessage(self.currentRide?.cancellation_reason ?? "", action: .default) { (style) in
                        updateCancelView()
                    }
                    
                }
                
            }
            if !self.controller.newRideBool{
                self.controller?.drawPolyline(isUpdatePolyLine: false)
            }
            
            
        }
//        DispatchQueue.main.async { [weak self] in
//            if let status = self?.controller.currentRideStatus {
                loadViewWithStatus(self.controller.currentRideStatus)
//            }
//        }
        
        if self.currentRide != nil {
            self.controller.updateDriverMarker()
        }
        
    }
    func getCategories(_ completion: (()->Void)? = nil) {
        
        if !self.hasOngoingRide  {
            guard self.controller.sourceLocationDetail?.coordinate != LocationCoordinate.zero && self.controller.sourceLocationDetail != nil else {
                return
            }
            var info = CategoryReq()
            info.city = GlobalValues.sharedInstance.cityName//self.controller.sourceLocationDetail?.cities.city
            info.city2 = self.controller.sourceLocationDetail?.cities.city2
            info.s_latitude = "\(self.controller.sourceLocationDetail?.coordinate.latitude ?? 0.0)"
            info.s_longitude = "\(self.controller.sourceLocationDetail?.coordinate.longitude ?? 0.0)"
            info.d_latitude = "\(self.controller.destinatioLocationDetail?.coordinate.latitude ?? (self.controller.sourceLocationDetail?.coordinate.latitude ?? 0.0))"
            info.d_longitude = "\(self.controller.destinatioLocationDetail?.coordinate.longitude ?? (self.controller.sourceLocationDetail?.coordinate.longitude ?? 0.0))"
            
            func handleCategoryError(_ error: Error) {
                if let errorMsg = (error as? CoreError)?.description {
                    if self.controller != nil {
                        if !self.controller.statusContentView.arrangedSubviews.contains(self.controller.serviceUnavailableView) {
                            for arrangedSubView in self.controller.statusContentView.arrangedSubviews {
                                arrangedSubView.removeFromSuperview()
                            }
                            self.controller.statusContentView.insertArrangedSubview(self.controller.serviceUnavailableView, at: 0)
                            DispatchQueue.main.async { [weak self] in
                                self?.controller.serviceUnavailableView.appologyLbl.text = errorMsg
                            }
                        }
                    }

                }
            }
            
            if self.controller.destinatioLocationDetail == nil {
                info.distance = 0.0
                info.time = 0
                info.weight = 0
                info.rate_card_type = "admin"
                info.category = 1
                ServiceManager().categoryList(info, completion: { [weak self](inner) -> (Void) in
                    do {
                        let categories = try inner()
                        self?.controller?.categories.removeAll()
                        if self?.controller?.categories != nil {
                            self?.controller.categories = categories
                        }
                    } catch {
                        handleCategoryError(error)
                    }
                })
            } else {
                
//                let firsLocation = CLLocation(latitude:self.controller.sourceLocationDetail!.coordinate.latitude , longitude:self.controller.sourceLocationDetail!.coordinate.longitude)
//                let secondLocation = CLLocation(latitude: self.controller.destinatioLocationDetail!.coordinate.latitude, longitude: self.controller.destinatioLocationDetail!.coordinate.longitude)
//
//                let distance = firsLocation.distance(from: secondLocation) / 1000
//                print("distance ---> \(distance)")
//
//                var timeZ = Double(distance) / Double(40)
//                timeZ = timeZ * 3600
//                let minutes = floor(timeZ / 60)
//                timeZ -= minutes * 60
//                let seconds = floor(timeZ)
//                let minuteSeconString = String(format: "%.f.%.f",Float(minutes),Float(seconds))
//                var eta = lroundf(Float(minuteSeconString)!)
//                if eta <= 0
//                {
//                    eta = 1
//                }
//
//                info.time = eta
//                info.distance = distance
//                ServiceManager().categoryList(info, completion: { [weak self](inner) -> (Void) in
//                    do {
//                        let categories = try inner()
//                        self?.controller?.categories.removeAll()
//                        if self?.controller?.categories != nil {
//                            self?.controller?.categories = categories
//                        }
//                    } catch {
//                        handleCategoryError(error)
//                    }
//                })
                if UserDefaults.pickupdropChangesStatus{
                    UserDefaults.pickupdropChangesStatus = false
                    self.controller?.mapsHelper.calculateDistanceDuration(self.controller.sourceLocationDetail!.coordinate, toLocation: self.controller.destinatioLocationDetail!.coordinate) { [weak self](durationDistance) in
                        info.time = durationDistance?.time
                        info.distance = durationDistance?.km
                        self?.controller.totalDistanceTravelling = durationDistance?.km
                        self?.controller.totalTimeTravelling = durationDistance?.time
                        self?.controller?.distanceDuration = durationDistance
                        info.weight = 0
                        info.rate_card_type = "admin"
                        info.category = 1
                        ServiceManager().categoryList(info, completion: { [weak self](inner) -> (Void) in
                            do {
                                let categories = try inner()
                                self?.controller?.categories.removeAll()
                                if self?.controller?.categories != nil {
                                    self?.controller?.categories = categories
                                }
                            } catch {
                                handleCategoryError(error)
                            }
                        })
                    }
                }else{
                    info.time = self.controller.totalTimeTravelling
                    info.distance = self.controller.totalDistanceTravelling
                   // self?.controller?.distanceDuration = durationDistance
                    info.weight = 0
                    info.rate_card_type = "admin"
                    info.category = 1
                    ServiceManager().categoryList(info, completion: { [weak self](inner) -> (Void) in
                        do {
                            let categories = try inner()
                            self?.controller?.categories.removeAll()
                            if self?.controller?.categories != nil {
                                self?.controller?.categories = categories
                            }
                        } catch {
                            handleCategoryError(error)
                        }
                    })
                }

            }
        }
    }
    
}

extension DashboardViewController {
    func observeRideChat() {
        self.clearAppBadgeCount()
        let chatPath = "users/" + firebaseUserID + fireBaseDB + "/Chats/" + rideId + "/Messages"
        
        let childObserver = FirebaseHelper.shared.observe(path : chatPath, with: .childAdded) {[weak self] (childValue) in
            
//            if let child = childValue.first {
            _ = FirebaseHelper.shared.observe(path: chatPath, with: EventType.value) { [weak self] res in
                let count = res.filter{($0.response?.read_on?.isEmpty == true || $0.response?.read_on == nil) && $0.response?.receiver_id?.lowercased() == "".myChatId.lowercased() }.count
                if count > 0 {
                    
                    if self?.presentedViewController is SingleChatViewController == false {
                     //   self?.speak(text: Speech.message.rawValue)
                        if !(UserDefaults.firstTimeLanuchStatus ?? false){
                                                UserDefaults.firstTimeLanuchStatus = true
//                            self?.setAppBadgeCount(0)
                            UserDefaults.chatCount = count
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CHAT_COUNT"), object: nil)


                             }else{
                                   UserDefaults.firstTimeLanuchStatus = false
                             }

                             if count > 0 {
                                 self?.rideActionView.chatBtn.badgeValue = "\(count)"
                                self?.waitingTimeChatBtn.badgeValue = "\(count)"

                             }
                        //self?.rideActionView.chatBtn.badgeValue = "\(UIApplication.shared.applicationIconBadgeNumber)"
                    }
                }
            }

//            }
        }

        self.chatObserver = childObserver
    }
    
    func removeChatObserver() {
        if self.chatObserver != nil {
            FirebaseHelper.shared.remove(observers: [self.chatObserver])
        }
      
    }
    
}

extension UIViewController {
    
    func clearBadgeCount() {
        self.clearAppBadgeCount()
        self.homeViewController?.rideActionView.chatBtn.badgeValue = ""
        self.homeViewController?.waitingTimeChatBtn.badgeValue = ""

    }
    
    
    
}

class SSBadgeButton : AppButton {
    
    var badgeValue : String! = "" {
        didSet {
            self.layoutSubviews()
        }
    }
    
    override init(frame :CGRect)  {
        // Initialize the UIView
        super.init(frame : frame)
        
        self.awakeFromNib()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.awakeFromNib()
    }
    
    
    override func awakeFromNib()
    {
        self.drawBadgeLayer()
    }
    
    var badgeLayer :CAShapeLayer!
    func drawBadgeLayer() {
        
        if self.badgeLayer != nil {
            self.badgeLayer.removeFromSuperlayer()
        }
        
        // Omit layer if text is nil
        if self.badgeValue == nil || self.badgeValue.count == 0 {
            return
        }
        
        //! Initial label text layer
        let labelText = CATextLayer()
        labelText.contentsScale = UIScreen.main.scale
        labelText.string = self.badgeValue.uppercased()
        labelText.fontSize = 9.0
        labelText.font = UIFont.systemFont(ofSize: 9)
        labelText.alignmentMode = .center
        labelText.foregroundColor = UIColor.white.cgColor
        let labelString = self.badgeValue.uppercased() as String
        let labelFont = UIFont.systemFont(ofSize: 9) as UIFont
        let attributes = [NSAttributedString.Key.font : labelFont]
        let w = self.frame.size.width
        let h = CGFloat(10.0)  // fixed height
        let labelWidth = min(w * 0.8, 10.0)    // Starting point
        let rect = labelString.boundingRect(with: CGSize(width: labelWidth, height: h), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: attributes, context: nil)
        let textWidth = round(rect.width * UIScreen.main.scale)
        labelText.frame = CGRect(x: 0, y: 0, width: textWidth, height: h)
        
        //! Initialize outline, set frame and color
        let shapeLayer = CAShapeLayer()
        shapeLayer.contentsScale = UIScreen.main.scale
        let frame : CGRect = labelText.frame
        let cornerRadius = CGFloat(5.0)
        let borderInset = CGFloat(-1.0)
        let aPath = UIBezierPath(roundedRect: frame.insetBy(dx: borderInset, dy: borderInset), cornerRadius: cornerRadius)
        
        shapeLayer.path = aPath.cgPath
        shapeLayer.fillColor = UIColor.red.cgColor
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.lineWidth = 0.5
        
        shapeLayer.insertSublayer(labelText, at: 0)
        
        shapeLayer.frame = shapeLayer.frame.offsetBy(dx: w*0.5, dy: 0.0)
        
        self.layer.insertSublayer(shapeLayer, at: 999)
        self.layer.masksToBounds = false
        self.badgeLayer = shapeLayer
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.drawBadgeLayer()
        self.setNeedsDisplay()
    }
    
}

extension String {
    var myChatId: String {
        return "P_\(CacheManager.userInfo?.id ?? 0)"
    }
}

extension UserDefaults {
    static var chatCount: Int {
        get {
            return UserDefaults.standard.integer(forKey: "CHAT_COUNT")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "CHAT_COUNT")
            UserDefaults.standard.synchronize()
        }
    }
}


extension ServiceManager {
    
    func callSOS(_ info: callSOSReqInfo, completion: @escaping (_ inner: () throws ->  CommonRes?) -> Void) {
        
        self.request(endPoint: MainDataServiceEndpoint.callSOS, parameters: info.JSONRepresentation as Dictionary<String,Any>) { result in
            
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: CommonRes.self)
                if let status = response, (status.status != nil) {
                    completion({return response})
                } else {
                    completion({ return  nil})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func updateRideStatus(_ rideId: String?, status: RideState = .retry,sourceLocation:LocationDetail,rideStatus:Int,  completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        var body = [String: Any]()
        body["status"] = status.rawValue
        body["rejected_count"] = 0
        body["search_latitude"] = sourceLocation.coordinate.latitude
        body["search_longitude"] = sourceLocation.coordinate.longitude
        body["prev_status"] = rideStatus
        
        self.request(endPoint: MainDataServiceEndpoint.updateRideStatus, body: body, pathSuffix: (rideId ?? "")) { result in
            
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: CommonRes.self)
                if let status = response {
                    completion({ return status })
                } else {
                    completion({ return nil })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func fetchRideDetails(_ rideId: String?, reason: String, completion: @escaping (_ inner: () throws ->  RideList?) -> (Void)) {
        
            let rideId = "\(rideId ?? "0")"
            guard rideId != "0" else {
                return
            }
            self.request(endPoint: MainDataServiceEndpoint.fetchRideDetails, pathSuffix: (rideId)) { result in
            
                switch result {
                case .success(let data):
                    let rideList = data.getDecodedObject(from: GeneralResponse<RideList>.self)
                    completion({ return rideList?.data})
                case .failure(let error):
                    completion({ throw error })
                }
            }
        }
    
    func updateCancelView(_ rideId: String, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        self.request(endPoint: MainDataServiceEndpoint.updateCancelView, pathSuffix: rideId) { result in
            
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: CommonRes.self)
                if let status = response, (status.status != nil) {
                    completion({ return response})
                } else {
                    completion({ return response})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
        func updateRideStatusHubLocation(_ rideId: String?, status: RideState, lat: String, long: String, rideStatus: Int, category_id: String, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        var body = [String: Any]()
        body["status"] = status.rawValue
        body["rejected_count"] = 0
        body["search_latitude"] = lat
        body["search_longitude"] = long
        body["prev_status"] = 8
        body["category_id"] = category_id
        body["category"] = category_id
        body["vehicle_type"] = category_id
        
        self.request(endPoint: MainDataServiceEndpoint.updateRideStatusHubLocation, body: body, pathSuffix: (rideId ?? "")) { result in
            
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: CommonRes.self)
                if let status = response {
                    completion({ return status })
                } else {
                    completion({ return nil })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func categoryList(_ info: CategoryReq, completion: @escaping (_ inner: () throws ->  [Category]) -> (Void)) {
        
        self.request(endPoint: MainDataServiceEndpoint.categoryList, parameters: info.JSONRepresentation as   Dictionary<String,Any>) { result in
            
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: GeneralResponse<[Category]>.self)
                if let categoryList = response?.data, response?.status == true {
                    completion({ return categoryList })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func updateFCMToken(_ completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        let body = [
            "device_id": device_Id,
            "fcmToken": HalaMobility.FCMToken,
            "device_type" : "ios"
        ]
        
        self.request(endPoint: MainDataServiceEndpoint.updateFCMToken, body: body) { result in
            
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: CommonRes.self)
                if let status = response, (status.status != nil) {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message?.stringValue ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func updateCashPayment(info: NSDictionary, id : Int, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        self.request(endPoint: MainDataServiceEndpoint.updateCashPayment, body: info as? Dictionary<String, Any>, pathSuffix: id as AnyObject as? String) { result in
            
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: CommonRes.self)
                if ((response?.status) != nil) {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message?.stringValue ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func cancelRide(createRide:Bool,_ ride: RideDetail?, reason: String,sourceLocation:LocationCoordinate, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        let rideId = "\(ride?.id ?? 0)"
        let driver_id = "\(ride?.driver_id ?? 0)"
        var body = [String: Any]()

        if createRide{
            
            body["prev_status"] = ride?.booking_status
            body["cancel_reason"] = 0
            body["driver_id"] = "\(ride?.current_driver_id ?? 0)"
            
        }else{
            body["cancel_reason"] = reason
            body["driver_id"] = driver_id
            body["prev_status"] = ride?.booking_status
            body["rejected_count"] = 0
            body["search_latitude"] = sourceLocation.latitude
            body["search_longitude"] = sourceLocation.longitude
            
        }
        
        self.request(endPoint: MainDataServiceEndpoint.cancelRide, body: body,pathSuffix: (rideId)) { result in
            
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: CommonRes.self)
                if let status = response, (status.status != nil) {
                    completion({ return response})
                } else {
                    completion({ return response})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func updateRideFeedback(_ ride: RideDetail?, feedBack: FeedbackInfo, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        let rideId = "\(ride?.id ?? 0)"
//        let driver_id = "\(ride?.driver_id ?? 0)"
        var feedback = feedBack
        feedback.driver_id = "\(ride?.driver_id ?? 0)"
        
        self.request(endPoint: MainDataServiceEndpoint.updateRideFeedback, body: feedback.JSONRepresentation, pathSuffix: (rideId)) { result in
            
            switch result {
            case .success(let data):
                if let response = data.getDecodedObject(from: CommonRes.self) {
                    completion({ return response})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func updateDestinationAddress(_ request: RequestConfirmRide?, reason: String, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        let rideId = "\(request?.rider_id ?? "0")"
        
        self.request(endPoint: MainDataServiceEndpoint.updateDestinationAddress, body: request?.JSONRepresentation, pathSuffix: (rideId)) { result in
            
            switch result {
            case .success(let data):
                let rideList = data.getDecodedObject(from: CommonRes.self)
                completion({ return rideList })
            case .failure(let error):
                completion({ throw error })
            }
        }     
    }
    
    func cardPayment(info: NSDictionary, id : Int, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        self.request(endPoint: MainDataServiceEndpoint.cardPayment, body: info as? Dictionary<String, Any>, pathSuffix: id as AnyObject as? String) { result in
            
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: CommonRes.self)
                if ((response?.status) != nil) {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message?.stringValue ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}

extension UIViewController {
    
    func updateProfile() {
        ServiceManager().getProfile({[weak self] (inner) -> (Void) in
            do {
                let _ = try inner()
            } catch {
//                self?.view.makeToast((error as? CoreError)?.description)
            }
        })
    }
}
