//
//  ApplyCouponViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 07/03/22.
//

import Foundation

class ApplyCouponViewModel: AppViewModel {
    weak var controller: ApplyCouponViewController!
    required init(controller: ApplyCouponViewController) {
        self.controller = controller
    }
}

extension ServiceManager {
    
    func verifyCoupon(_ params: VerifyCouponParams, completion: @escaping (_ inner: () throws ->  GeneralResponse<[Coupon]>?) -> Void) {
        
        self.request(endPoint: MainDataServiceEndpoint.verifyCoupon, parameters: params.JSONRepresentation as Dictionary<String,Any>) { result in
            
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: GeneralResponse<[Coupon]>.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func couponList(_ completion: @escaping (_ inner: () throws ->  [Coupon]?) -> (Void)) {
        
        self.request(endPoint: MainDataServiceEndpoint.couponList) { result in
            
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: [Coupon].self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
