//
//  RatingActionViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
//

import Foundation

class RatingActionViewModel: NSObject {
    
    let souceAddress: String?
    let dropAddress: String?
    
    let fare: String?
    let distance: String?
    let travelTime: String?
    
    let rideStatus: RideStatus!
    
    let userName: String?
    let userImageUrl: URL!
    
    let discount: String?
    
    var isFoundDiscount: Bool = false
    
    let paymentMode: String?
    
    let vehicleModelName: String?
    
    let detail: RideDetail
    
    init(rideDetail: RideDetail) {
        
        self.detail = rideDetail
        
        self.rideStatus = RideStatus.init(rawValue: rideDetail.booking_status ?? 0)
        
        self.souceAddress = rideDetail.actual_address?.source?.address
        
        self.dropAddress = rideDetail.actual_address?.destination?.address
        
        self.fare = Extension.updateCurrecyRate(fare: "\(String(rideDetail.trip_info?.amount ?? 0))").currencyValue

        self.distance = rideDetail.trip_info?.distance?.distanceValue ?? ""
        
        self.travelTime = "\(rideDetail.trip_info?.trip_time ?? 0)".timeValue
        
        self.userName = rideDetail.driver_info?.name
        
        self.userImageUrl = rideDetail.driver_info?.image?.urlFromString
        
        self.discount = Extension.updateCurrecyRate(fare: "\(rideDetail.trip_info?.discount?.doubleValue ?? 0)").currencyValue.appendString("Discount Applied".localized)
       // self.discount = "\(rideDetail.trip_info?.discount ?? 0)".currencyValue.appendString("Discount Applied".localized)
        
        self.isFoundDiscount = (((rideDetail.trip_info?.discount?.doubleValue) ?? 0) > 0) == true
        
        self.paymentMode = rideDetail.payment_details?.paidBy
        
        self.vehicleModelName = rideDetail.vehicle_info?.model
        
    }
    
    func updatePaymentActionView(_ view: RatingActionView) {
        
        view.packageView?.isHidden = self.detail.rental == nil
        view.packageNameLbl?.text = self.detail.rental?.packageName?.stringValue
        view.startReadingLbl?.text = self.detail.rental?.startReading?.stringValue
        
        view.vehicleModelName?.text = self.vehicleModelName
        view.addressContentView?.sourceAddressLbl?.text = self.souceAddress
        view.addressContentView?.destinationAddressLbl?.text = self.dropAddress
        view.rideDistanceInfoView?.distanceValue?.text = self.distance
        view.rideDistanceInfoView?.durationValue?.text = self.travelTime
        view.rideUserInfoView?.userImage.hnk_setImage(from: self.userImageUrl, placeholder: nil, success: { (image) in
            view.rideUserInfoView?.userImage?.image = image
        }, failure: nil)
        view.rideUserInfoView?.userName?.text = self.userName
        view.rideFareLbl?.text = self.fare
        view.discountAppliedLbl?.text = self.discount
        view.discountAppliedLbl.isHidden = !self.isFoundDiscount
        view.cashCollectedLbl?.text = self.paymentMode
    }
    
}

internal extension String {
    
    func appendString(_ string: String) -> String {
        return self + " " + string
    }
}
