//
//  UnlockVC.swift
//  HalaMobility
//
//  Created by ADMIN on 17/06/22.
//

import UIKit
import GoogleMaps
import FAPaginationLayout

class UnlockVC: BaseViewController , LocationServiceDelegate, GMSMapViewDelegate, MapMarkerDelegate, pauseContainerdelegate {
    func didTapInfoButton() {
        self.openGoogleMap(modConfirmRental?.pickupStation?.latitude?.stringValue ?? "0.0", daddr: modConfirmRental?.pickupStation?.longitude?.stringValue ?? "0.0",name: (modConfirmRental?.pickupStation?.name)!)
    }
    
    @IBOutlet weak var googleunlock: GMSMapView!
    var isCurrentLocation : Bool = false
    var zoomLevel: Float = 16.0
    let mapsHelper = GoogleMapsHelper()
    @IBOutlet weak var lblvehiclename: AppLabel!
    @IBOutlet weak var lbllowbattery: AppLabel!
    @IBOutlet weak var btnVehicleno: AppButton!
    @IBOutlet weak var btnpromocode: AppButton!
    @IBOutlet weak var btnunlock: AppButton!
    @IBOutlet weak var lblAddress: AppLabel!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var imgVehicle: AppImageView!
    @IBOutlet weak var collVehicle: UICollectionView!
    @IBOutlet weak var viewVehiclelist: VehicleListView!
    @IBOutlet weak var statusContentView: UIStackView!
    @IBOutlet weak var constViewHeight: NSLayoutConstraint!
    var intUnlockbtn:Int = 0
    
    //timerview
    @IBOutlet weak var viewTimer: UIView!
    @IBOutlet weak var viewbottomcurve: UIView!
    @IBOutlet weak var lblDaysans: UILabel!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblHoursans: UILabel!
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var lblMinusans: UILabel!
    @IBOutlet weak var lblMinus: UILabel!
    @IBOutlet weak var lblSecans: UILabel!
    @IBOutlet weak var lblSec: UILabel!
    var runningmapTimer: Timer?
    
    //buttonsview
    @IBOutlet weak var stackbuttons: UIStackView!
    @IBOutlet weak var lblConnect: UILabel!
    @IBOutlet weak var lblUnlock: UILabel!
    @IBOutlet weak var lblLock: UILabel!
    @IBOutlet weak var lblFind: UILabel!
    @IBOutlet weak var lblPause: UILabel!
    @IBOutlet weak var lblSwipe: UILabel!
    @IBOutlet weak var lblNear: UILabel!
    @IBOutlet weak var lbldirection: UILabel!
    
    var vehicle: Vehiclelist?
    var vehicletypeinfo: VehicleDetail?
    var locationdetail: LocationDetail?
    var modConfirmRental: Rental1?
    var rentalCheckHelper: RentalCheckHelper?
    var isNeedtimer :Bool = false
    //nearmy parking view
    @IBOutlet weak var viewnearmybarking: UIView!
    @IBOutlet weak var lblnearmeAddress: AppLabel!
    @IBOutlet weak var lblnearmeVehicleno: AppLabel!
    @IBOutlet weak var imgnearmeVehicle: AppImageView!
    private var infoWindow = MapMarkerWindow()
    fileprivate var locationMarker : GMSMarker? = GMSMarker()

    //pay button show screen
    @IBOutlet weak var containerPay: UIView!
    
    //Feedback COntainer
    @IBOutlet weak var containerFeedback: UIView!
    
    //Pause show screen
    @IBOutlet weak var containerPause: UIView!
    var pauseTimer: Timer?
    
    var objPay : PayContainer?
    var objPause : PauseContainer?
    var objFeedback : FeedbackContainer?
    var currentRentalStatus: RentalStatus? = nil {
        willSet {
            print("test")
        }
    }
    var CurveHeight:CGFloat = 350.0
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // let rideStatus = modConfirmRental?.bookingStatus?.intValue
        //self.currentRentalStatus = RentalStatus.init(rawValue: rideStatus ?? 0)!
       //google map integration
        googleunlock.settings.myLocationButton = false
        googleunlock.isMyLocationEnabled = false
        googleunlock.delegate = self
        self.setCurrentLocationToCameraPosition()
        self.setvalues()
        collVehicle.register(UINib(nibName: "VehicleCell", bundle: Bundle.main), forCellWithReuseIdentifier: "VehicleCell")
        collVehicle.contentInset = UIEdgeInsets.init(top: 0, left: 35, bottom: 0, right: 35)
        (collVehicle.collectionViewLayout as! FAPaginationLayout).scrollDirection = .horizontal
        (collVehicle.collectionViewLayout as! FAPaginationLayout).minimumLineSpacing = 20.0
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.removepopup(sender:)))
        self.statusContentView.addGestureRecognizer(gesture)
       

    }
    override func viewWillAppear(_ animated: Bool) {
        if GlobalValues.sharedInstance.currentRentalStatus != 0 && GlobalValues.sharedInstance.currentRentalStatus != 1 {
            self.checkRentalList()
        }
    }
    func setvalues(){
        if let name = vehicle?.displayName{
            self.lblvehiclename.text = name
        }else{
            self.lblvehiclename.text = vehicletypeinfo?.displayName
        }
        let vehiclno = vehicle?.vehicleNumber ?? "" == "" ? vehicle?.number : vehicle?.vehicleNumber
        btnVehicleno.setTitle(" " + vehiclno! + " ", for: .normal)
        //let image = baseImageURL + (vehicle?.vehiclePhotoFront ?? "")!
        let image = baseImageURL + (modConfirmRental?.vehicleTypeInfo?.image ?? "")!
        imgVehicle.hnk_setImage(from: image.urlFromString, placeholder: nil, success: { (image) in
            self.imgVehicle.image = image
            }, failure: nil)
        let battery:Double = (vehicle!.batteryVoltage.calculateBatterpercentage(list: vehicle))/100
        if battery < GlobalValues.sharedInstance.minimumBattery{
            let battery = Int(vehicle!.batteryVoltage)
            lbllowbattery.text = "Batterytoolow".localized + " " + ("\(battery)%")
            lbllowbattery.isHidden = false
        }
        self.updateCurveUI(intUI:GlobalValues.sharedInstance.currentRentalStatus)
        //self.updateCurveUI(intUI: currentRentalStatus == .NoBooking ? 0 : currentRentalStatus?.rawValue as! Int)

    }
    func setCurrentLocationToCameraPosition() {
        mapsHelper.getCurrentLocation { [weak self] (location) in
            DispatchQueue.main.async { [weak self] in
                let camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 17.0)
                self?.googleunlock?.animate(to: camera)
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        LocationManager.shared.delegate = self
    }
    
    @objc func removepopup(sender : UITapGestureRecognizer) {
        statusContentView.isHidden = true
        for arrangedSubView in self.statusContentView.arrangedSubviews {
            arrangedSubView.removeFromSuperview()
        }
    }
    
    func tracingLocation(userLocation: CLLocation) {
         currentLocation = userLocation
         if isCurrentLocation == false{
             let userLocationMarker = GMSMarker(position: currentLocation!.coordinate)
             userLocationMarker.icon = UIImage(named: "currentlocationmarker")
             userLocationMarker.map = googleunlock
         }
         let location: CLLocation = userLocation
         isCurrentLocation = true
         let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: zoomLevel)
         if googleunlock.isHidden {
             googleunlock.isHidden = false
             googleunlock.camera = camera
         }
     }
     func tracingLocationDidFailWithError(error: NSError) {
     }
     var currentLocation: CLLocation? {
         didSet {
         }
     }
    func insertView(_ view: UIView) {
        if !UserDefaults.companyLogin {
            if !self.statusContentView.arrangedSubviews.contains(view) {
                for arrangedSubView in self.statusContentView.arrangedSubviews {
                    arrangedSubView.removeFromSuperview()
                }
                self.statusContentView.insertArrangedSubview(view, at: 0)
            }
        }
    }
    //MARK: MAP VIEW MARKER
    func loadNiB() -> MapMarkerWindow {
        let infoWindow = MapMarkerWindow.instanceFromNib() as! MapMarkerWindow
        return infoWindow
    }
    func addmarker(latitude: Double, longitude: Double, title: String, vehiclecount: Int = 0) {
        let marker = GMSMarker()
        let markerImage = UIImage(named: "nearparking")!.withRenderingMode(.alwaysOriginal)
        //creating a marker view
        let markerView = UIImageView(image: markerImage)
        markerView.frame = CGRect(x: 0.0, y: markerView.center.y, width: 49, height: 49)
        marker.zIndex = 1
        //changing the tint color of the image
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        marker.iconView = markerView
        marker.map = googleunlock
        let nearestStation = CLLocation(latitude: latitude , longitude: longitude)
        self.googleunlock.drawPolygon(from: nearestStation.coordinate, to: GlobalValues.sharedInstance.currentLocation.coordinate, strokeWidth: 1.0, mode: "direction", onCompletion: { (inner) -> (Void) in
            let line =  inner()
            line.map = self.googleunlock
        })
}
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if marker.zIndex == 1 {
        locationMarker = marker
        infoWindow.removeFromSuperview()
        infoWindow = loadNiB()
        guard let location = locationMarker?.position else {
            print("locationMarker is nil")
            return false
        }
        // Pass the spot data to the info window, and set its delegate to self
        infoWindow.delegate = self
        // Configure UI properties of info window
        infoWindow.lblAddress.text = modConfirmRental?.pickupStation?.name
        infoWindow.lblETA.text = "ETA : 1 Min"
        // Offset the info window to be directly above the tapped marker
        infoWindow.center = mapView.projection.point(for: location)
        infoWindow.center.y = infoWindow.center.y - 82
        self.view.addSubview(infoWindow)
        }else{
            marker.title = "youarehere".localized
            marker.snippet = title
        }
        return false
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (locationMarker != nil){
            guard let location = locationMarker?.position else {
                print("locationMarker is nil")
                return
            }
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.center.y = infoWindow.center.y - 82
        }
    }
        
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        infoWindow.removeFromSuperview()
    }
    
    //MARK: Button Click
    @IBAction func btnclkNearmebottomArrow(){
        infoWindow.removeFromSuperview()
        viewnearmybarking.isHidden = true
        viewbottomcurve.isHidden = false
        constViewHeight.constant = CurveHeight
    }
    @IBAction func btnclks(_ sender : UIButton){
        switch sender.tag{
        case 0: //vehicle no
            if vehicle != nil{
            self.statusContentView.isHidden = false
            self.insertView((self.viewVehiclelist!))
            self.collVehicle.reloadData()
            }
            break
        case 1: //destination
                let searchVC = SearchLocationViewController.initFromStoryBoard(.Main)
                searchVC.addressType = .rideserve
                searchVC.didSelectLocation = { [weak self] locationDetail in
                    DispatchQueue.main.async { [weak self] in
                        print(locationDetail?.address ?? "")
                        self?.lblAddress.textColor = .black
                        self?.lblAddress.text = "\(locationDetail?.address ?? "")"
                        self?.locationdetail = locationDetail
                        self?.CurveHeight =  self!.CurveHeight + (self?.lblAddress.frame.size.height)!
                        self?.constViewHeight.constant =  self!.CurveHeight
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
                self.navigationController?.pushViewController(searchVC, animated: true)
            break
        case 2: //promo code
            let applyCouponVC = ApplyCouponViewController.initFromStoryBoard(.Dashboard)
            applyCouponVC.applyCouponCode = { [weak self] couponCode, coupon in
                //self?.selectedCoupon = coupon
                //self?.rideConfirmationView?.couponTextField?.text = couponCode
                self?.navigationController?.popViewController(animated: true)
            }
            self.navigationController?.pushViewController(applyCouponVC, animated: true)

            break
        case 3: //unlock
            if intUnlockbtn == -1{// cancel
                let cancelVC = CancelReasonViewController.initFromStoryBoard(.Main)
               // cancelVC.rideDetail = self.modConfirmRental
                cancelVC.didRequestToCancelRide = { [weak self] (rideDetail, reason, sourceLocation) in
                    self?.showAlertWithMessage("Cancel confirmation".localized, phrase1: "Yes".localized, phrase2: "No".localized, action: UIAlertAction.Style.default, .cancel) { [weak self](style) in
                        self?.dismiss(animated: false, completion: nil)
                        if style == .default {
                            self?.cancelRide(rideDetail: rideDetail, reason: reason, sourceLocation: sourceLocation)
                        }
                    }
                }
                cancelVC.modalPresentationStyle = .overCurrentContext
                self.present(cancelVC, animated: true, completion: nil)
            }else if intUnlockbtn == 0 { //before unlock
            if lblAddress.text == "Enter destination" || lblAddress.text == "" {
                self.view.makeToast("Please enter the destination".localized)
            }else if CacheManager.userInfo?.digio_reference == nil{
                self.view.makeToast("ProceedKYCVerification".localized)
            }else if lbllowbattery.isHidden == true{
                self.updateCurveUI(intUI:GlobalValues.sharedInstance.currentRentalStatus)
            }else{
                //check Zone
                self.checkZone(type: ZoneType)
            }
            }else if intUnlockbtn == 1{ // end retnal
                self.updateRentalStataus()
            }else if intUnlockbtn == 2{ // end retnal
                self.showAlertWithMessage("Areyousurewanttorental".localized, phrase1: "Yes".localized, phrase2: "No".localized, action: UIAlertAction.Style.default, UIAlertAction.Style.cancel, completion: { (style) in
                    if style == .default {
                        self.updaterental((self.modConfirmRental?.id!.stringValue)!)
                        self.endRide()
                    }
                })
            }
            break
        default:
            break
        }
    }
    
    @IBAction func btnclkCurrvebuttons(_ sender : UIButton){
        switch sender.tag{
        case 1 : //Connect
            let connect = ConnectVC.initFromStoryBoard(.Custom)
            connect.rentalstatus = "start"
            connect.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            //popOverVC.delegate = self
            //popOverVC.Delegate = self
            self.updateCurveUI(intUI: 3)
            self.present(connect, animated: true)
            
//            let headerView = Bundle.main.loadNibNamed("ConnectionView", owner:
//            self, options: nil)?.first as? ConnectionView
//            headerView?.frame = self.view.frame
//            self.view.addSubview(headerView!)
            break
        case 2 : //Unlock
            break
        case 3 : //Lock
            break
        case 4 : //Find
            break
        case 5 : //Pause
            self.showAlertWithMessage("Are you sure want to start the waiting?".localized, phrase1: "Yes".localized, phrase2: "No".localized, action: UIAlertAction.Style.default, UIAlertAction.Style.cancel, completion: { (style) in
                if style == .default {
                    self.endRide()
                }
            })
            break
        case 6 : //Swipe
            let qrcode = QRreaderVC.initFromStoryBoard(.Dashboard)
            self.navigationController?.pushViewController(qrcode, animated: true)
            break
        case 7 : //Near
            viewbottomcurve.isHidden = true
            constViewHeight.constant = 0
            viewnearmybarking.isHidden = false
            if lblnearmeAddress.text == "" {
                if let name = vehicle?.displayName{
                    self.lblnearmeAddress.text = name
                }else{
                    self.lblnearmeAddress.text = vehicletypeinfo?.displayName
                }
                let vehiclno = vehicle?.vehicleNumber ?? "" == "" ? vehicle?.number : vehicle?.vehicleNumber
                lblnearmeVehicleno.text = vehiclno
                let image = baseImageURL + (vehicle?.vehiclePhotoFront ?? "")!
                imgnearmeVehicle.hnk_setImage(from: image.urlFromString, placeholder: nil, success: { (image) in
                    self.imgnearmeVehicle.image = image
                    }, failure: nil)
            }
            self.addmarker(latitude: (modConfirmRental?.pickupStation?.latitude) ?? 0.0, longitude: (modConfirmRental?.pickupStation?.longitude) ?? 0.0, title: (modConfirmRental?.pickupStation?.name)!)
            
            break
        case 8 : //Direction
           // self.openGoogleMap((self?.vehiclelist?[index].deviceLatitude)!, daddr: (self?.vehiclelist?[index].deviceLongitude)!,name: self?.vehiclelist?[index].displayName ?? "")
            let latitude:String = String((modConfirmRental?.pickupStation?.latitude)!)
            let longitude:String = String((modConfirmRental?.pickupStation?.longitude)!)
            self.openGoogleMap(latitude, daddr: longitude,name: "")

            break
        default:
            break
        }
    }
    func clickResumeRide(){
        self.updatePausetimerAPI()
    }
    //MARK: RENTAL STATUS
    func updateRentalStataus(){
        switch currentRentalStatus{
        case .Running :
            self.updateCurveUI(intUI:GlobalValues.sharedInstance.currentRentalStatus)
            break
        case .none:
            break
        case .some(.NoBooking):
//            GlobalValues.sharedInstance.currentRentalStatus = 0
//            self.homeViewController?.rideCheckHelper?.stopListening()
//            self.homeViewController?.mapView.delegate = nil
//            self.homeViewController?.rideCheckHelper = nil
//            self.homeViewController?.removeChatObserver()
//            self.homeViewController?.stopAnimationTimer()
//            self.homeViewController?.releaseCallBacks()
//            Router.setDashboardViewControllerAsRoot()
            break
        case .some(.Created):
            if GlobalValues.sharedInstance.GVtimer != 0{
                self.infoWindow = loadNiB()
                self.updateCurveUI(intUI: 1)
            }
            break
        case .some(.BookingConfirmed):
            break
        case .some(.Arrived):
            break
        case .some(.UnlockRequest):
            break
        case .some(.UnlockApproved):
            break
        case .some(.EndRequest):
            self.updateCurveUI(intUI: GlobalValues.sharedInstance.currentRentalStatus)
            break
        case .some(.EndApproved):
            self.updateCurveUI(intUI: self.currentRentalStatus!.rawValue)
            /*
             "startdate" = "Start Date";
             "enddate" = "End Date";
             "totalfare" = "Total Fare";
             "payableamount" = "Payable Amount";
             */
            break
        case .some(.Completed):
            self.updateCurveUI(intUI:GlobalValues.sharedInstance.currentRentalStatus)
            break
        case .some(.UnlockRequestRejected):
            break
        case .some(.EndRequestRejected):
            break
        case .some(.UserCancelled):
            break
        case .some(.AdminCancelled):
            break
        case .some(.Waiting):
            break
        }
    }
    func endRide(){
        let connect = ConnectVC.initFromStoryBoard(.Custom)
        connect.rentalstatus = "end"
        connect.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(connect, animated: true)
    }
    //MARK: API
    func checkZone(type:String){  
        let params = ["type": type,"latitude":"\(locationdetail?.coordinate.latitude ?? 0.0)","longitude":"\(locationdetail?.coordinate.longitude ?? 0.0)"] as [String : Any]
        ServiceManager().checkZoneAPI((params as Dictionary<String,Any>?)!, completion: { [weak self] (inner) -> (Void) in
            do {
                let status = try inner()
                if !(status?.isSuccess ?? false) {
                    self?.view.makeToast("Sorry, we don't serve in this location".localized)
                }else{
                    self?.showAlertWithMessage("Are you sure to proceed ride?".localized, phrase1: "Yes".localized, phrase2: "No".localized, action: UIAlertAction.Style.default, UIAlertAction.Style.cancel, completion: { (style) in
                        if style == .default {
                            self?.checkEstimatefareAPI()
                        }
                    })
                }
            } catch {
            }
        })
    }
    func checkEstimatefareAPI(){
        let selectedDateTime: String = Date().dateFromFormater("yyyy-MM-dd HH:mm:ss") ?? ""
        let currentdateUTC = selectedDateTime.localToUTC(incomingFormat: "yyyy-MM-dd HH:mm:ss", outGoingFormat: "yyyy-MM-dd HH:mm:ss")
        //http://dev.virtuesense.com:6364/rental/rental-estimation/2/1?station_id=4&city=Hyderabad&category_id=3&pickup_date_time=2022-05-10%2004%3A30%3A00&drop_date_time=2022-05-10%2004%3A30%3A00&promo_code_id=0
        let params = ["station_id": StationID,"city":GlobalValues.sharedInstance.cityName,"category_id":CategoryID,"pickup_date_time":currentdateUTC,"drop_date_time":currentdateUTC,"promo_code_id":""] as [String : Any]
        ServiceManager().checkEstimation((params as Dictionary<String,Any>?)!, completion: { [weak self] (fare) -> (Void) in
            do {
                let response = try fare()
                let farebreakup = "\(response?.data.fareBreakupID ?? 0), \((response?.data.minuteFare) ?? 0) , \((response?.data.hourFare) ?? 0) , \((response?.data.dayFare) ?? 0) , \((response?.data.weekFare) ?? 0) , \((response?.data.monthFare) ?? 0)"
               // let farebreakup = response?.fareBreakupID ?? 0 + "," + (response?.minuteFare)! ?? 0 + "," + (response?.hourFare)! ?? 0 + "," + (response?.dayFare)! ?? 0 + "," + (response?.weekFare)! ?? 0 + "," + (response?.monthFare)! ?? 0
                var confirmrentparam = confirmRentalReq()
                confirmrentparam.rental_type = RideType
                confirmrentparam.pickup_station_id = "\(StationID)"
                confirmrentparam.pickup_station_name = self?.vehicle?.displayName
                confirmrentparam.city = GlobalValues.sharedInstance.cityName
                confirmrentparam.country_short_name = GlobalValues.sharedInstance.countryName
                confirmrentparam.category_id = "\(CategoryID)"
                confirmrentparam.vehicle_id = self?.vehicle?.vehicleID
                confirmrentparam.fare_breakup = farebreakup
                confirmrentparam.unlock_fee = "\(response?.data.unlockFare ?? 0)"
                confirmrentparam.base_fee = "\(response?.data.baseFare ?? 0)"
                confirmrentparam.base_time = "\(response?.data.baseTime ?? 0)"
                confirmrentparam.minute_fare = "\(response?.data.minuteFare ?? 0)"
                confirmrentparam.hour_fare = "\(response?.data.hourFare ?? 0)"
                confirmrentparam.day_fare = "\(response?.data.dayFare ?? 0)"
                confirmrentparam.week_fare = "\(response?.data.weekFare ?? 0)"
                confirmrentparam.month_fare = "\(response?.data.monthFare ?? 0)"
                confirmrentparam.net_fare = "\(response?.data.rideFare ?? 0)"
                confirmrentparam.pickup_date_time = "\(currentdateUTC)"
                confirmrentparam.drop_date_time = "\(currentdateUTC)"
                confirmrentparam.estimated_fare = "\(response?.data.estimatedFare ?? 0)"
                confirmrentparam.promo_code_id = "0"
                confirmrentparam.subscribe_id = "0"
                self?.confirmRental(confirmrentparam)
            } catch {
            }
        })
    }
    
    func confirmRental(_ info: confirmRentalReq)  {
        ServiceManager().confirmRentalAPI(info, completion: { [weak self] (inner) -> (Void) in
            do {
                self?.modConfirmRental = try inner()
                if self?.modConfirmRental != nil {
                    GlobalValues.sharedInstance.currentRentalStatus = (self?.modConfirmRental?.bookingStatus?.intValue)!
                    GlobalValues.sharedInstance.previousRentalStatus = (self?.modConfirmRental?.bookingStatus?.intValue)!
                    self?.updaterental((self?.modConfirmRental?.id!.stringValue)!)
                }else{
                    self?.view.makeToast("error")
                }
            } catch {
            }
        })
    }
    
    func updaterental(_ BookingID: String)  {
        var param:[String:Any]? = nil
        if GlobalValues.sharedInstance.currentRentalStatus == 1 { //start
            param = ["action":"START","odometer_reading":modConfirmRental?.vehicleInfo?.odometer ?? "","status":"6","prev_status":"\(GlobalValues.sharedInstance.previousRentalStatus)"]
        } else if GlobalValues.sharedInstance.currentRentalStatus == 6 { //end
            //param = ["drop_station_id":"4","action":"END","pause_mins":"1","unlock_fare_percentage":0,"ride_fare_percentage":0,"odometer_reading":modConfirmRental?.vehicleInfo?.odometer ?? "","status":"\(GlobalValues.sharedInstance.currentRentalStatus)","prev_status":"\(GlobalValues.sharedInstance.previousRentalStatus)"]
            param = ["drop_station_id":"4","action":"END","pause_mins":"1","unlock_fare_percentage":0,"ride_fare_percentage":0,"odometer_reading":modConfirmRental?.vehicleInfo?.odometer ?? "","status":"7","prev_status":"6"]

        }
        ServiceManager().updateriderequestAPI(BookingID,params:param!, completion: { [weak self] (inner) -> (Void) in
            do {
                let status = try inner()
                if (status?.isSuccess ?? false) {
                    self?.checkRentalList()
                }else{
                    self?.view.makeToast(status?.message?.stringValue)
                }
            } catch {
            }
        })
    }
    func checkRentalList() {
        if rentalCheckHelper == nil {
            self.rentalCheckHelper = RentalCheckHelper()
        }
        self.rentalCheckHelper?.startListening(on: { [weak self] (error, rentallist) in
            guard error == nil else {
                return
            }
            self?.modConfirmRental = rentallist
            if GlobalValues.sharedInstance.currentRentalStatus != self?.modConfirmRental?.bookingStatus?.intValue{
                self?.isNeedtimer = true
                GlobalValues.sharedInstance.currentRentalStatus = self?.modConfirmRental?.bookingStatus?.intValue ?? 0
                self?.currentRentalStatus = RentalStatus.init(rawValue: self?.modConfirmRental?.bookingStatus?.intValue ?? 0)!
                self?.updateRentalStataus()
            }
        })
    }
    func updatePausetimerAPI()  {
        let BookingID:String = (modConfirmRental?.id!.stringValue)!
        let param:[String:Any]? = ["rider_id":modConfirmRental?.riderID?.stringValue ?? "0","pause_id":modConfirmRental?.ridePauseTimes?.last?.id?.stringValue ?? "","prev_status":"\(GlobalValues.sharedInstance.currentRentalStatus)","status":"\(GlobalValues.sharedInstance.previousRentalStatus)"]
        ServiceManager().updateridepauserequestAPI(BookingID,params:param!, completion: { [weak self] (inner) -> (Void) in
            do {
                let status = try inner()
                if (status?.isSuccess ?? false) {
                    self?.checkRentalList()
                }else{
                    self?.view.makeToast(status?.message?.stringValue)
                }
            } catch {
            }
        })
    }
    //MARK: TimerView UI Updates
     func showTimerView() {
         let startDate = modConfirmRental?.datetime?.toDateTime()
         let  endDate = self.modConfirmRental?.startTime?.toDateTime()
         GlobalValues.sharedInstance.GVtimer = Date.differencebetweendates(recent: startDate! as Date, previous: endDate! as Date)
         viewTimer.isHidden =  false
         lblDays.text = "days".localized
         lblHours.text = "hours".localized
         lblMinus.text = "mins".localized
         lblSec.text = "sec".localized
         lblConnect.text = "Connect".localized
         lblUnlock.text = "Unlock".localized
         lblLock.text = "Lock".localized
         lblFind.text = "Find".localized
         lblPause.text = "Pauseride".localized
         lblSwipe.text = "SwipeVehicle".localized
         lblNear.text = "Nearparking".localized
         lbldirection.text = "Direction".localized
         btnunlock.setTitle("Endrental".localized, for: .normal)
         btnunlock.backgroundThemeColor = 78
//         let startDate = _list.datetime!.toDateTime()
//         let  endDate = _list.startTime!.toDateTime()
//         GlobalValues.sharedInstance.GVtimer = Date.differencebetweendates(recent: startDate as Date, previous: endDate as Date)
         self.runningmapTimers()
         runningmapTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runningmapTimers), userInfo: nil, repeats: true)
         if GlobalValues.sharedInstance.currentRentalStatus == 14 { // pasuse timer
             pauseTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(pausetimer), userInfo: nil, repeats: true)
         }

    }
    @objc func runningmapTimers() {
        if isNeedtimer{ GlobalValues.sharedInstance.GVtimer = GlobalValues.sharedInstance.GVtimer! + 1 }
        let timerValues = GlobalValues.sharedInstance.GVtimer?.secondsToTime()
        print(timerValues!)
        let values = timerValues?.components(separatedBy: ":")
        lblDaysans.text = values?[0] ?? "0"
        lblHoursans.text = values?[1] ?? "00"
        lblMinusans.text = values?[2] ?? "00"
        lblSecans.text = values?[3] ?? "00"
    }
    @objc func pausetimer(){
        objPause?.timerupdate()
    }
    func updateCurveUI(intUI:Int = 0){
        switch intUI{
        case 0:
            break
        case 1: // cancel button
            NotificationCenter.default.addObserver(forName: Notification.Name.init("CHECK_RIDE_UPDATES"), object: nil, queue: .main) {[weak self] (note) in
                let dashBoardVC = MapVC.initFromStoryBoard(.Dashboard)
                self?.pushViewController(dashBoardVC, type: MapVC.self)
            }
            viewAddress.isHidden = true
            btnpromocode.isHidden = true
            constViewHeight.constant = 300
            intUnlockbtn = -1
            btnunlock.setTitle("Cancel".localized, for: .normal)
            break
        case 6: //RUNNING
            intUnlockbtn = 1
            self.stackbuttons.isHidden = false
            CurveHeight = CurveHeight + 120
            constViewHeight.constant =  CurveHeight
            viewAddress.isHidden = true
            btnpromocode.isHidden = true
            self.showTimerView()
            break
        case 3: //
            intUnlockbtn = 2
            btnunlock.setTitle("Endrental".localized, for: .normal)
            btnunlock.backgroundThemeColor = 37
            break
        case 8,9,14: // END, COMPLETED,PAUSE
            if intUI != 14 {
            viewTimer.isHidden =  true
            runningmapTimer?.invalidate()
            runningmapTimer = nil
            GlobalValues.sharedInstance.GVtimer = 0
            lbllowbattery.isHidden = true
            btnVehicleno.backgroundThemeColor = 75
            btnVehicleno.textThemeColor = 3
            btnVehicleno.isUserInteractionEnabled = false
            stackbuttons.isHidden = true
            containerPay.isHidden = false
            }
            self.stackbuttons.isHidden = false
            viewAddress.isHidden = true
            btnpromocode.isHidden = true
            switch intUI{
            case 8:
                constViewHeight.constant = 450
                objPay?.fetchdetail = modConfirmRental
                objPay?.setupValues()
                break
            case 9://Feedback UI
                self.stackbuttons.isHidden = true
                constViewHeight.constant = 550
                containerFeedback.isHidden = false
                objFeedback?.fetchdetail = modConfirmRental
                objFeedback?.setupValues()
                break
            case 14:
                GlobalValues.sharedInstance.previousRentalStatus = 8
                isNeedtimer = true
                self.showTimerView()
                containerPause.isHidden = false
                constViewHeight.constant = 450
                objPause?.fetchdetail = modConfirmRental
                objPause?.delegate = self
                objPause?.setupValues()
                break
                
            default:
                break
            }
            break
        default:
            break
        }
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PayContainer"{
            if let vc = segue.destination as? PayContainer {
                objPay = vc
            }
        }else if segue.identifier == "PauseContainer"{
            if let vc = segue.destination as? PauseContainer {
                objPause = vc
            }
        }else if segue.identifier == "FeedbackContainer"{
            if let vc = segue.destination as? FeedbackContainer {
                objFeedback = vc
            }
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        runningmapTimer?.invalidate()
        runningmapTimer = nil
        pauseTimer?.invalidate()
        pauseTimer = nil
        self.rentalCheckHelper?.stopListening()
    }
}
@available(iOS 14.0, *)
extension UnlockVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x > 0 && scrollView.superview?.superview?.superview?.superview as? VehicleCell != nil{
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2
        let cell:VehicleCell = (scrollView.superview?.superview?.superview?.superview as? VehicleCell)!
        cell.PageControl.currentPage = Int(offSet + horizontalCenter) / Int(width)
        }
        //PageControl.currentPage = Int(offSet + horizontalCenter) / Int(width)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: VehicleCell = collectionView.dequeueCell(indexPath)
        cell.loadData(values: vehicle!, det: vehicletypeinfo)
        cell.btnDirection.tag = indexPath.row
        cell.scrollImage.delegate = self
        cell.didRequestToNavigatetoBrowser = { [weak self] option in
            
            self?.openGoogleMap((self?.vehicle?.deviceLatitude)!, daddr: (self?.vehicle?.deviceLongitude)!,name: self?.vehicle?.displayName ?? "")
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellSize = CGSize()
        cellSize = collectionView.bounds.size
        cellSize.width -= collectionView.contentInset.left
        cellSize.width -= collectionView.contentInset.right
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}
