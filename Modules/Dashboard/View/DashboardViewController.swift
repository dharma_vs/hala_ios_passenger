//
//  DashboardViewController.swift
//  TaxiPickup
//
//  Created by LKB-007 on 13/01/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit
import GoogleMaps
import DropDown
import FAPaginationLayout
import NVActivityIndicatorView
import ContactsUI
import SafariServices
import Toast_Swift



enum RideCreationState: Int {
    
    case none = 0
    
    case createRide = 1
    
    case confirmRide = 2
    
}



class DashboardViewController: BaseViewController ,SFSafariViewControllerDelegate, LocationServiceDelegate, UITextFieldDelegate {
    
    //Dashboard
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var comapnyHeaderView: UIView!
    @IBOutlet weak var newRidebtn: UIButton!
    @IBOutlet weak var closeRideBtn: UIButton!
    @IBOutlet weak var companyRideDropLbl: UILabel!
    @IBOutlet weak var addressContentView: UIView!
    @IBOutlet var pickUpTitleLbl: UILabel!
    @IBOutlet var pickUpTitleLblHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sourceLocation: UITextField!
    @IBOutlet weak var fromImage: UIImageView!
    @IBOutlet weak var dropLocation: UITextField!
    @IBOutlet weak var toImage: UIImageView!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet var dropTitleLbl: UILabel!
    @IBOutlet var dropTitleLblHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var theme2PickUpDropImageView: UIView!
    @IBOutlet var dropViews: [UIView]!
    @IBOutlet weak var statusContentView: UIStackView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!

    //CurrentLocation
    @IBOutlet weak var sosBtn: UIButton!
    @IBOutlet weak var rideCountBtn: UIButton!
    
    //Ride List View
    @IBOutlet weak var rideListView: RideListView!
    
    //Ride Destination ChangesView
    @IBOutlet weak var rideDestinationChangesView: RideDestinationChangesView!
    @IBOutlet weak var comapnyRidePickUpLbl: UILabel!
    @IBOutlet weak var comapnyRideDriverNameLbl: UILabel!
    @IBOutlet weak var comapnyRideVechileTypeLbl: UILabel!
    @IBOutlet weak var comapnyRideDriverImage: UIImageView!
    
    //Company Ride Details
    @IBOutlet weak var comapnyRideDetailsView: UIView!
    
    //Status View
    @IBOutlet weak var statusView: RideStatusView!
    @IBOutlet weak var ridemenu: UICollectionView!
    @IBOutlet var rideInfoViewHeightConstant: NSLayoutConstraint!

    //Create Ride View
    @IBOutlet weak var createRideView: CreateRideView!
    @IBOutlet var createRideViewHeightConstant: NSLayoutConstraint!
    
    //Ride Action View
    @IBOutlet weak var rideActionView: RideActionView!
    
    //Ride Confirmation View
    @IBOutlet weak var rideConfirmationView: RideConfirmationView!
    
    //Service Search View
    @IBOutlet weak var serviceSearchView: ServiceSearchView!
    
    //Service Unavailable View
    @IBOutlet weak var serviceUnavailableView: ServiceUnavailableView!
    
    //Payment Action View
    @IBOutlet weak var paymentActionView: PaymentActionView!
    
    //Rating Action View
    @IBOutlet weak var ratingActionView: RatingActionView!
    
    //Ride Search View
    @IBOutlet weak var rideSearchingView: UIView!
    @IBOutlet weak var searchAnimationView: NVActivityIndicatorView!
    @IBOutlet weak var creatingRideTitleLbl: UILabel!
    
    //Waiting Time View
    @IBOutlet weak var waitingTimeView: UIView!
    @IBOutlet weak var waitingTimeChatBtn: SSBadgeButton!
    @IBOutlet weak var waitingTimeValue: UILabel!
    @IBOutlet weak var waitingTimeViewheightConstraint: NSLayoutConstraint!
    
    
    
    var sourceLocationDetail: LocationDetail? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                if self?.currentLocation?.coordinate == self?.sourceLocationDetail?.coordinate {
                    self?.sourceLocation?.text = "Current Location".localized
                } else {
                    self?.sourceLocation?.text = self?.sourceLocationDetail?.address
                }
                self?.updateMapForRideCreation()
            }
        }
    }
    
    func setCurrentAddressToPickupLocation(_ location: CLLocation) {
        self.mapsHelper.getPlaceAddress(from: location.coordinate, on: { [weak self] (locationDetail) in
            self?.sourceLocationDetail = locationDetail
        })
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let searchVC = SearchLocationViewController.initFromStoryBoard(.Main)
        searchVC.addressType = textField == sourceLocation ? .source : .destination
        searchVC.didSelectLocation = { [weak self] locationDetail in
            UserDefaults.pickupdropChangesStatus = true
            DispatchQueue.main.async { [weak self] in
                if textField == self?.sourceLocation {
                    if self?.destinatioLocationDetail?.coordinate != locationDetail?.coordinate {
                        self?.sourceLocationDetail = locationDetail
                    } else {
                        self?.view.makeToast("locationNotEqual".localized)
                    }
                } else {
                    if (self?.viewModel.currentRide != nil && !self!.newRideBool){
                        self?.startActivityIndicator()
                        let info = CategoryReq()
                        info.city = GlobalValues.sharedInstance.cityName//self?.sourceLocationDetail?.cities.city
                        info.city2 = self?.sourceLocationDetail?.cities.city2
                        info.s_latitude = "\(self?.sourceLocationDetail?.coordinate.latitude ?? 0.0)"
                        info.s_longitude = "\(self?.sourceLocationDetail?.coordinate.longitude ?? 0.0)"
                        info.d_latitude = "\(locationDetail?.coordinate.latitude ?? 0.0)"
                        info.d_longitude = "\(locationDetail?.coordinate.longitude ?? 0.0)"
                        info.vehicle_type = String(self?.viewModel.currentRide?.vehicle_type_id ?? 0)
                        self?.UpdatedDestinationLocation = locationDetail!
                        self?.mapsHelper.calculateDistanceDuration(self?.sourceLocationDetail!.coordinate ?? self?.currentRideList.first?.estimated_address?.source?.locationCoordinate ?? .zero, toLocation: (locationDetail?.coordinate)!) { [weak self](durationDistance) in
                            info.time = durationDistance?.time
                            info.distance = durationDistance?.km
                            info.weight = 0
                            info.rate_card_type = "admin"
                            info.category = 1
                            self?.distanceDuration = durationDistance
                            self?.UpdatedDestinationDistanceDuration = durationDistance!
                            ServiceManager().categoryList(info, completion: { [weak self](inner) -> (Void) in
                                do {
                                    self?.stopActivityIndicator()
                                    let categoriesItems = try inner()
                                    print(categoriesItems)
                                    self?.view.addSubview(self!.rideDestinationChangesView)
                                    self?.rideDestinationChangesView.frame = CGRect(x: (self?.view.frame.origin.x)!, y: (self?.view.frame.origin.y)!, width: (self?.view.frame.width)!, height: (self?.view.frame.height)!)
                                    self?.rideDestinationChangesView.isHidden = false
                                    self?.rideDestinationChangesView.destinationAddressLbl.text = locationDetail?.address ?? ""
                                    
                                    for item in categoriesItems{
                                        if item.id == self?.viewModel.currentRide?.vehicle_type_id ?? 0 {
                                            self?.rideDestinationChangesView.destinationEstimateFareLbl.text =
                                            Extension.updateCurrecyRate(fare: "\(String(item.rideEstimate?.estimatedFare ?? 0))").currencyValue
                                            self?.UpdatedDestinationCategory = item
                                            self?.selectedCategory = item
                                            return
                                        }
                                    }
                                } catch {
                                }
                            })
                        }
                    }else {
                        if self?.sourceLocationDetail?.coordinate != locationDetail?.coordinate {
                            self?.destinatioLocationDetail = locationDetail
                            self?.mapsHelper.calculateDistanceDuration((self?.sourceLocationDetail!.coordinate)!, toLocation: locationDetail?.coordinate ?? LocationCoordinate.zero) { [weak self](durationDistance) in
                                //                                info.time = durationDistance?.time
                                //                                info.distance = durationDistance?.km
                                self?.distanceDuration = durationDistance
                                self?.UpdatedDestinationDistanceDuration = durationDistance!
                            }
                        } else {
                            self?.view.makeToast("locationNotEqual".localized)
                        }
                    }
                }
                self?.pop(true)
            }
        }
        self.navigationController?.pushViewController(searchVC, animated: true)
        return false
    }
    
    
    
    
    
    
    var destinatioLocationDetail: LocationDetail?  {
        didSet {
            DispatchQueue.main.async { [weak self] in
                if self?.currentLocation?.coordinate == self?.destinatioLocationDetail?.coordinate {
                    self?.dropLocation?.text = "Current Location".localized
                } else {
                    self?.dropLocation?.text = self?.destinatioLocationDetail?.address
                    self?.favBtn.isHidden = false
                }
                if (CacheManager.favouriteLocations.filter{$0.coordinate == self?.destinatioLocationDetail?.coordinate ?? LocationCoordinate.zero}.first == nil) {
                    self?.favBtn.isHidden = false
                } else {
                    self?.favBtn.isHidden = true
                }
                self?.updateMapForRideCreation()
            }
        }
    }
    
    @IBAction func newRideBtnAction(_ sender: Any) {
        self.currentRideStatus = .None
        self.mapView?.clear()
        self.sourceLocationMarker.map = nil
        self.destinationLocationMarker.map = nil
        self.driverLocationMarker.map = nil
        self.sourceLocationMarker.map = self.mapView
        self.sourceLocationMarker.position = self.sourceLocationDetail?.coordinate ?? LocationCoordinate.zero
        if let coordinate = self.sourceLocationDetail?.coordinate {
            let camera = GMSCameraPosition.camera(withLatitude: (coordinate.latitude), longitude: (coordinate.longitude), zoom: self.zoomLevel)
            self.mapView?.animate(to: camera)
        }
        self.insertView(self.createRideView)
        newRideBool = true
        self.dropLocation.text = ""
        self.closeRideBtn.isHidden = false
        self.newRidebtn.isHidden = true
        self.viewModel?.updateDestinationAddressView()
        self.rideCheckHelper?.timerAction()
    }
    
    
    
    
    
    
    
    var viewModel: DashboardViewModel! {
        didSet {
            if mapView != nil {
                self.viewModel.updateDashboardViewController(self)
            }
        }
    }
    
    var currentLocation: CLLocation? {
        didSet {
            //            if let coordinate = self.currentLocation?.coordinate {
            //                let camera = GMSCameraPosition.camera(withLatitude: (coordinate.latitude), longitude: (coordinate.longitude), zoom: self.zoomLevel)
            //                self.mapView?.animate(to: camera)
            //            }
        }
    }
    
    lazy var sourceLocationMarker : GMSMarker = { [unowned self] in
        let marker = GMSMarker()
        let imageView = AppImageView(frame: CGRect(origin: .zero, size: CGSize(width: 30, height: 30)))
        imageView.contentMode =  .scaleAspectFit
        //        imageView.image = #imageLiteral(resourceName: "CurrentLocation")
        //        imageView.imageThemeColor = 3
        imageView.loadTaxiImage(CacheManager.settings?.source_marker?.urlFromString)
        marker.iconView = imageView
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.map = self.mapView
        marker.tracksViewChanges = false
        return marker
    }()
    
    lazy var destinationLocationMarker : GMSMarker = { [unowned self] in
        let marker = GMSMarker()
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: 40, height: 40)))
        imageView.contentMode =  .scaleAspectFit
        //        imageView.image = #imageLiteral(resourceName: "ic_destination")
        imageView.loadTaxiImage(CacheManager.settings?.destination_marker?.urlFromString)
        marker.iconView = imageView
        //        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.map = self.mapView
        marker.tracksViewChanges = false
        return marker
    }()
    
    lazy var driverLocationMarker : GMSMarker = {
        let marker = GMSMarker()
        marker.iconView = self.driverIconImge
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.rotation = 90.0
        marker.map = self.mapView
        marker.tracksViewChanges = false
        return marker
    }()
    
    //    @IBAction func moveCurrentLocation(_ sender: Any) {
    //
    //        updateThemeLayout()
    //        if self.sourceLocationDetail != nil && self.destinatioLocationDetail != nil {
    //            let sourceCoordinate = self.sourceLocationDetail?.coordinate ?? LocationCoordinate.zero
    //            let destCoordinate = self.destinatioLocationDetail?.coordinate ?? LocationCoordinate.zero
    //            self.focusMapToCoordinates(sourceCoordinate, dest: self.currentRideStatus == .Accepted ? self.driverLocationMarker.position : destCoordinate )
    //        } else if self.sourceLocationDetail != nil && self.destinatioLocationDetail == nil {
    //            self.focusMapToCoordinates(self.sourceLocationDetail?.coordinate ?? LocationCoordinate.zero, dest: self.currentRideStatus == .None || self.currentRideStatus == .Completed || self.currentRideStatus == .PaymentCompleted ? self.sourceLocationDetail?.coordinate ?? LocationCoordinate.zero : self.driverLocationMarker.position)
    //        } else {
    //            self.currentLocation = CLLocation(latitude: defaultMapLocation.latitude, longitude: defaultMapLocation.longitude)
    //            if let coordinate = self.currentLocation?.coordinate {
    //                let camera = GMSCameraPosition.camera(withLatitude: (coordinate.latitude), longitude: (coordinate.longitude), zoom: self.zoomLevel)
    //                self.mapView?.animate(to: camera)
    //            }
    //        }
    //    }
    
    @IBAction func moveCurrentLocation(_ sender: Any) {
        let cabLocation = LocationCoordinate(latitude: self.driverLatitude.doubleValue ?? 0, longitude: self.driverLongitude.doubleValue ?? 0)
        if let source = self.fetchedRide?.estimated_address?.source?.locationCoordinate, let destination = self.fetchedRide?.estimated_address?.destination?.locationCoordinate, (cabLocation.latitude != 0 && cabLocation.longitude != 0) {
            if !isCabLocationIsShowing {
                let camera = GMSCameraPosition.camera(withLatitude: (cabLocation.latitude), longitude: (cabLocation.longitude), zoom: 15.0)
                self.mapView?.animate(to: camera)
                isCabLocationIsShowing = !isCabLocationIsShowing
                return
            } else {
                isCabLocationIsShowing = !isCabLocationIsShowing
            }
            let sourceCoordinate = source
            let destCoordinate = destination
            self.focusMapToCoordinates(self.currentRideStatus == .Arrived || self.currentRideStatus == .Accepted || self.currentRideStatus == .Started ? cabLocation : sourceCoordinate, dest: self.currentRideStatus == .Accepted ? sourceCoordinate : destCoordinate )
        } else {
            self.setCurrentLocationToCameraPosition()
        }
        // updateThemeLayout()
        //        if self.sourceLocationDetail != nil && self.destinatioLocationDetail != nil {
        //            let sourceCoordinate = self.sourceLocationDetail?.coordinate ?? LocationCoordinate.zero
        //            let destCoordinate = self.destinatioLocationDetail?.coordinate ?? LocationCoordinate.zero
        //            self.focusMapToCoordinates(sourceCoordinate, dest: self.currentRideStatus == .Accepted ? self.driverLocationMarker.position : destCoordinate )
        //        } else if self.sourceLocationDetail != nil && self.destinatioLocationDetail == nil {
        //            self.focusMapToCoordinates(self.sourceLocationDetail?.coordinate ?? LocationCoordinate.zero, dest: self.currentRideStatus == .None || self.currentRideStatus == .Completed || self.currentRideStatus == .PaymentCompleted ? self.sourceLocationDetail?.coordinate ?? LocationCoordinate.zero : self.driverLocationMarker.position)
        //        } else {
        //            self.currentLocation = CLLocation(latitude: defaultMapLocation.latitude, longitude: defaultMapLocation.longitude)
        //            if let coordinate = self.currentLocation?.coordinate {
        //                let camera = GMSCameraPosition.camera(withLatitude: (coordinate.latitude), longitude: (coordinate.longitude), zoom: self.zoomLevel)
        //                self.mapView?.animate(to: camera)
        //            }
        //        }
    }
    
    
    
    func addNearbyCabs() {
        self.nearByCabMarkers.forEach{$0.map = nil}
        self.nearByCabMarkers.removeAll()
        if let category = self.selectedCategory, let nearbyCabs = category.nearByCabs {
            for cab in nearbyCabs {
                let marker = GMSMarker()
                let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: 30, height: 30)))
                imageView.contentMode =  .scaleAspectFit
                imageView.loadTaxiImage(category.cabMarker?.urlFromString)
                //                imageView.image = #imageLiteral(resourceName: "Car2").resizeImage(newWidth: 30)
                marker.iconView = imageView
                marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                marker.position = cab.coordinate
                marker.rotation = 90.0
                marker.map = self.mapView
                marker.tracksViewChanges = false
                self.nearByCabMarkers.append(marker)
            }
        }
    }
    
    func drawPolyline(isUpdatePolyLine:Bool) {
        calculateWaitingTime()
        if self.currentRideStatus == .None && self.sourceLocationDetail != nil && self.destinatioLocationDetail != nil {
            let sourceCoordinate = self.sourceLocationDetail?.coordinate ?? LocationCoordinate.zero
            let destCoordinate = self.destinatioLocationDetail?.coordinate ?? LocationCoordinate.zero
            DispatchQueue.main.async {
                self.mapView?.clear()
                self.sourceLocationMarker.map = self.mapView
                self.destinationLocationMarker.map = self.mapView
                self.sourceLocationMarker.position = sourceCoordinate
                self.destinationLocationMarker.position = destCoordinate
            }
            self.mapView.drawPolygon(from: sourceCoordinate, to: destCoordinate) { [weak self] (inner) -> (Void) in
                let polyLine = inner()
                self?.polylineInfo = (polyLine, destCoordinate)
                DispatchQueue.main.async {
                    self?.polylineInfo?.0?.map = self?.mapView
                }
            }
            return
        }
        let rideStatuses = Set([RideStatus.Accepted, .Arrived, .Started])
        let polySources = Set([RideStatus.Accepted])
        if let currentRideStatus = self.currentRideStatus {
            if rideStatuses.contains(currentRideStatus) {
                if let sourceCoordinate = self.fetchedRide?.estimated_address?.source?.locationCoordinate,
                   let destinationCoordinate = self.fetchedRide?.estimated_address?.destination?.locationCoordinate {
                    self.mapView?.clear()
                    self.sourceLocationMarker.map = self.mapView
                    self.destinationLocationMarker.map = self.mapView
                    self.sourceLocationMarker.position = sourceCoordinate
                    self.destinationLocationMarker.position = destinationCoordinate
                    self.driverLocationMarker.position = self.fetchedRide?.driver_info?.locationCoordinate ?? sourceCoordinate
                    DispatchQueue.main.async { [weak self] in
                        self?.driverLocationMarker.rotation = [-90, -45.0, 0.0, 45.0, 90].randomElement() ?? 0.0
                    }
                    self.driverLocationMarker.map = self.mapView
                    let driverLocation = CLLocationCoordinate2D(latitude: self.driverLatitude.doubleValue ?? 0, longitude: self.driverLongitude.doubleValue ?? 0)
                    self.mapView.drawPolygon(from: driverLocation, to: polySources.contains(currentRideStatus) ? sourceCoordinate : destinationCoordinate) { [weak self] (inner) -> (Void) in
                        let polyLine = inner()
                        self?.polylineInfo = (polyLine, driverLocation)
                        DispatchQueue.main.async {
                            self?.polylineInfo?.0?.map = self?.mapView
                        }
                    }
                    self.sourceLocationMarker.map = polySources.contains(currentRideStatus) ? self.mapView : nil
                    
                    self.destinationLocationMarker.map = polySources.contains(currentRideStatus) == false ? self.mapView : nil
                }
            } else if self.sourceLocationDetail != nil && self.destinatioLocationDetail == nil {
                self.mapView?.clear()
                self.sourceLocationMarker.map = nil
                self.destinationLocationMarker.map = nil
                self.driverLocationMarker.map = nil
                self.sourceLocationMarker.map = self.mapView
                self.sourceLocationMarker.position = self.sourceLocationDetail?.coordinate ?? LocationCoordinate.zero
                if let coordinate = self.sourceLocationDetail?.coordinate {
                    let camera = GMSCameraPosition.camera(withLatitude: (coordinate.latitude), longitude: (coordinate.longitude), zoom: self.zoomLevel)
                    self.mapView?.animate(to: camera)
                }
            } else {
                self.mapView?.clear()
                self.sourceLocationMarker.map = nil
                self.destinationLocationMarker.map = nil
                self.driverLocationMarker.map = nil
            }
        }
        if isUpdatePolyLine{
            if  let destinationCoordinate = destinatioLocationDetail?.coordinate {
                self.mapView?.clear()
                self.sourceLocationMarker.map = self.mapView
                self.destinationLocationMarker.map = self.mapView
                self.sourceLocationMarker.position = localMarkerCoordinate
                self.destinationLocationMarker.position = destinationCoordinate
                self.driverLocationMarker.position = localMarkerCoordinate
                DispatchQueue.main.async { [weak self] in
                    self?.driverLocationMarker.rotation = [-90, -45.0, 0.0, 45.0, 90].randomElement() ?? 0.0
                }
                self.driverLocationMarker.map = self.mapView
                self.mapView.drawPolygon(from: localMarkerCoordinate , to: destinationCoordinate) { [weak self] (inner) -> (Void) in
                    let polyLine = inner()
                    self?.polylineInfo = (polyLine, self?.localMarkerCoordinate)
                    DispatchQueue.main.async {
                        self?.polylineInfo?.0?.map = self?.mapView
                    }
                }
                self.sourceLocationMarker.map = polySources.contains(self.currentRideStatus!) ? self.mapView : nil
                self.destinationLocationMarker.map = polySources.contains(self.currentRideStatus!) == false ? self.mapView : nil
            }
        }
    }
    
    func updateDriverMarker() {
        let rideStatuses = Set([RideStatus.Accepted, .Arrived, .Started])
        let polySources = Set([RideStatus.Accepted])
        if let currentRideStatus = self.currentRideStatus {
            if rideStatuses.contains(currentRideStatus) {
                if self.driverLocationMarker.position != self.viewModel?.currentRide?.driver_info?.locationCoordinate {
                    if let sourceCoordinate = self.viewModel?.currentRide?.estimated_address?.source?.locationCoordinate,
                       let destinationCoordinate = self.viewModel?.currentRide?.estimated_address?.destination?.locationCoordinate {
                        self.mapView.drawPolygon(from:self.viewModel?.currentRide?.driver_info?.locationCoordinate ?? LocationCoordinate.zero, to: polySources.contains(currentRideStatus) ? sourceCoordinate : destinationCoordinate, animated: false) {[weak self] (inner) -> (Void) in
                            let polyLine = inner()
                            DispatchQueue.main.async { [weak self] in
                                self?.polylineInfo?.0?.map = nil
                                self?.polylineInfo = (polyLine, self?.viewModel?.currentRide?.driver_info?.locationCoordinate)
                                self?.polylineInfo?.0?.map = self?.mapView
                            }
                        }
                    }
                }
                DispatchQueue.main.async { [weak self] in
                    self?.driverLocationMarker.map = self?.mapView
                    self?.driverLocationMarker.rotation = [-90, -45.0, 0.0, 45.0, 90].randomElement() ?? 0.0
                    self?.driverLocationMarker.position = self?.viewModel?.currentRide?.driver_info?.locationCoordinate ?? LocationCoordinate.zero
                    self?.driverIconImge.hnk_setImage(from: self?.viewModel?.currentRide?.vehicle_type_info?.cab_marker?.urlFromString, placeholder: nil, success: { (image) in
                        //                        if self?.placeHolderImage == nil {
                        //                            self?.placeHolderImage = image
                        //                        }
                        DispatchQueue.main.async { [weak self] in
                            //                            self?.driverIconImge.image = image
                            self?.driverLocationMarker.iconView = nil
                            self?.driverLocationMarker.icon = image?.resizeImage(newWidth: 20)
                            
                            //                            self?.driverLocationMarker.iconView = self?.driverIconImge
                        }
                    }, failure: nil)
                }
            } else {
                self.destinationLocationMarker.map = nil
            }
        }
    }
    
    func setCurrentLocationToCameraPosition() {
        mapsHelper.getCurrentLocation { [weak self] (location) in
            DispatchQueue.main.async { [weak self] in
                let camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 15.0)
                self?.mapView?.animate(to: camera)
            }
            if self?.sourceLocationDetail == nil {
                self?.setCurrentAddressToPickupLocation(location)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("DashboardView-->")
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: String.LogoutNotification), object: nil, queue: nil) { [weak self] (note) in
            self?.showAlertWithMessage("Are you sure want to logout?".localized, phrase1: "Yes".localized, phrase2: "No".localized, action: UIAlertAction.Style.default, UIAlertAction.Style.cancel, completion: { (style) in
                if style == .default {
                    self?.logout()
                }
            })
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.driverMarkerUpdate(notification:)), name: Notification.Name("locationUpdate"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleEvent), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "CHAT_COUNT"), object: nil, queue: nil) { [weak self] notification in
            let newValue = UserDefaults.chatCount ?? 0
            DispatchQueue.main.async { [weak self] in
                self?.rideActionView.chatBtn.badgeValue = newValue > 0 ? newValue.stringValue : ""
                self?.waitingTimeChatBtn.badgeValue = newValue > 0 ? newValue.stringValue : ""
            }
        }
        
        NotificationCenter.default.addObserver(forName: Notification.Name.init("CHECK_RIDE_UPDATES"), object: nil, queue: .main) {[weak self] (note) in
            self?.navigationController?.popToRootViewController(animated: false)
            self?.rideCheckHelper?.timerAction()
        }
        
        UserDefaults.secondTheme = false
        self.setTpBackground()
        if HalaMobility.FCMToken != ""{
        ServiceManager().updateFCMToken { (inner) -> (Void) in }
        }
        
        guard let collectionView = ridemenu else { fatalError() }
        
        guard let collectionView1 = rideConfirmationView.ridemenu else { fatalError() }
        
        collectionView.register(UINib(nibName: "RideCell", bundle: Bundle.main), forCellWithReuseIdentifier: "RideCell")
        collectionView1.register(UINib(nibName: "RideCell", bundle: Bundle.main), forCellWithReuseIdentifier: "RideCell")
        collectionView.register(UINib(nibName: "RideCellTheme1", bundle: Bundle.main), forCellWithReuseIdentifier: "RideCellTheme1")
        collectionView1.register(UINib(nibName: "RideCellTheme1", bundle: Bundle.main), forCellWithReuseIdentifier: "RideCellTheme1")
        
        self.viewModel = DashboardViewModel(RideList())
        
        collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 35, bottom: 0, right: 35)
        
        (collectionView.collectionViewLayout as! FAPaginationLayout).scrollDirection = .horizontal
        
        collectionView1.contentInset = UIEdgeInsets.init(top: 0, left: 40, bottom: 0, right: 40)
        
        (collectionView1.collectionViewLayout as! FAPaginationLayout).scrollDirection = .horizontal
        (collectionView1.collectionViewLayout as! FAPaginationLayout).minimumLineSpacing = 20.0
        (collectionView.collectionViewLayout as! FAPaginationLayout).minimumLineSpacing = 20.0
        
        mapView.settings.myLocationButton = false
        //        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        
        self.checkRideList()
        
        mapsHelper.getCurrentLocation { [weak self](location) in
            if self?.currentRideStatus == .None {
                
                self?.isCabLocationIsShowing = true
                
                if !(self?.iscalledGoogleAddress ?? false) {
                    self?.iscalledGoogleAddress = true
                    self?.mapsHelper.getPlaceAddress(from: location.coordinate, on: { [weak self] (locationDetail) in
                        self?.sourceLocationDetail = locationDetail
                        self?.iscalledGoogleAddress = false
                    })
                }
            }
        }
        self.selectedPaymentMethod = .cash
        
        observeRideActions()
        
        self.rideActionView?.baseVC = self
        self.ratingActionView?.baseVC = self
        self.paymentActionView?.baseVC = self
        
        searchAnimationView.type = .ballScaleRipple
        //UIApplication.shared.statusBarView?.backgroundColor = .clear
        self.rideConfirmationView.contactInformationLbl?.titleLabel?.numberOfLines = 2
        self.rideConfirmationView.contactInformationLbl?.titleLabel?.lineBreakMode = .byClipping
        self.rideConfirmationView.contactInformationLbl.setTitle("Personal".localized, for: .normal)
        self.rideActionView.waitingTimeTitleLbl.text = "Waiting Timer".localized
        self.rideActionView.waitingTimeLbl.backgroundColor = .colorAppBackground
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = UIStatusBarStyle.darkContent
        } else {
            UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
        }
        
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(DashboardViewController.tapFunction))
        self.rideConfirmationView?.walletBalanceLbl.isUserInteractionEnabled = true
        self.paymentActionView.collectCashAmontLbl.isUserInteractionEnabled = true
        //            self.rideConfirmationView?.walletBalanceLbl.addGestureRecognizer(tap)
        //            self.paymentActionView.collectCashAmontLbl.addGestureRecognizer(tap)
        
        self.selectedCardID = "0"
        
        self.selectedRide = ""
        self.rideConfirmationView.closeBookForOtherBtn.isHidden = true
        if UserDefaults.companyLogin{
            self.addressContentView.isHidden = true
            
        }else{
            self.comapnyHeaderView.isHidden = true
        }
        moveCurrentLocation(UIButton())
    }
    
    @objc func driverMarkerUpdate(notification: Notification) {
        if ((notification as NSNotification).value(forKey: "userInfo")as! NSDictionary).object(forKey: "latitude") != nil {
            driverLatitude = "\(((notification as NSNotification).value(forKey: "userInfo")as! NSDictionary).object(forKey: "latitude")!)"
        }else if  ((notification as NSNotification).value(forKey: "userInfo")as! NSDictionary).object(forKey: "longitude") != nil{
            driverLongitude = "\(((notification as NSNotification).value(forKey: "userInfo")as! NSDictionary).object(forKey: "longitude")!)"
            
            let lat = Double(driverLatitude)
            let lon = Double(driverLongitude)
            
            let driverCoordinates = CLLocationCoordinate2D(latitude:lat!
                                                           , longitude:lon!)
            
            if self.currentRideStatus?.tripStatus == "ON RIDE" {
                
                let firsLocation = CLLocation(latitude:driverCoordinates.latitude , longitude:driverCoordinates.longitude)
                let secondLocation = CLLocation(latitude: localMarkerCoordinate.latitude, longitude: localMarkerCoordinate.longitude)
                
                if localMarkerCoordinate == LocationCoordinate.zero {
                    self.localMarkerCoordinate = driverCoordinates
                }else{
                    
                    let distance = firsLocation.distance(from: secondLocation) / 1000
                    print("\(distance)")
                    let settingDistance = CacheManager.settings?.draw_polyline_distance ?? "200"
                    
                    let polyLineDistance = Double("\(settingDistance)")
                    
                    if distance >= (polyLineDistance! / 1000) {
                        self.localMarkerCoordinate = driverCoordinates
                        self.drawPolyline(isUpdatePolyLine: true)
                        
                    }
                }
                //  let destCoordinate = self.destinatioLocationDetail?.coordinate ??
            }
            
            if self.currentRideStatus == .Accepted {
                self.drawPolyline(isUpdatePolyLine: false)
            }
            DispatchQueue.main.async { [weak self] in
                self?.driverLocationMarker.map = self?.mapView
                self?.driverIconImge.loadTaxiImage(self?.fetchedRide?.vehicle_type_info?.cab_marker?.urlFromString)
                self?.driverLocationMarker.position = driverCoordinates
            }
        }
        print(driverLongitude)
        print(driverLatitude)
    }
    
    func tracingLocation(userLocation: CLLocation) {
        let location: CLLocation = userLocation
        isCurrentLocation = true
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: zoomLevel)
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            //            mapView.animate(to: camera)
        }
        currentLocation = userLocation
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        return false
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
    }
        
    func focusMapToCoordinates(_ source: CLLocationCoordinate2D, dest: CLLocationCoordinate2D) {
        let bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: source, coordinate: dest)
        self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: self.addressContentView.frame.maxY))
    }
    
    func updateThemeLayout() {
        let loactionDetailsStackView = self.view.viewWithTag(101) as! UIStackView
        let picUpVIew = self.view.viewWithTag(102) as! TpView
        let dropView = self.view.viewWithTag(103) as! TpView
        let rideLaterView =  self.createRideView.viewWithTag(300)!
        
        if !UserDefaults.secondTheme {
            loactionDetailsStackView.spacing = 0
            //            picUpVIew.cornerRadius = 16
            //            dropView.cornerRadius = 16
            rideInfoViewHeightConstant.constant = 80
            self.pickUpTitleLblHeightConstraint?.constant = 0
            self.dropTitleLblHeightContraint?.constant = 0
            let separatorLbl = self.view.viewWithTag(106) as? UILabel
            separatorLbl?.isHidden = true
            self.createRideViewHeightConstant.constant = 170
            self.createRideView.backgroundColor = .clear
            self.createRideView.titleTheme2Lbl.isHidden = true
            self.createRideView.rideLaterBtnTheme2.isHidden = true
            self.createRideView.createRideTralingConstraint.constant = 35
            rideLaterView.isHidden = false
            self.fromImage.isHidden = true
            self.toImage.isHidden = true
            self.theme2PickUpDropImageView.isHidden = false
            
            pickUpTitleLbl.isHidden = false
            dropTitleLbl.isHidden = false
            
            self.rideConfirmationView.rideDetailLblHeightConstaint.constant = 90
            self.rideConfirmationView.bgCollectionView.isHidden = false
           // self.rideConfirmationView.paymentStackView.axis = .horizontal
            //self.rideConfirmationView.cashWalletStackview.alignment = .trailing
           // self.rideConfirmationView.PaymentBookorOthersStackView.axis = .vertical
          //  self.rideConfirmationView.bookForOtherStackView.axis = .vertical
           // self.rideConfirmationView.bookforOthersPhoneNumberStackView.axis = .horizontal
        }else {
            self.createRideViewHeightConstant.constant = 230
            self.createRideView.backgroundColor = .white
            self.createRideView.titleTheme2Lbl.text = "SUGGESTED RIDES".localized
            self.createRideView.titleTheme2Lbl.isHidden = false
            loactionDetailsStackView.spacing = 0
            //            picUpVIew.cornerRadius = 0
            //            dropView.cornerRadius = 0
            rideInfoViewHeightConstant.constant = 130
            rideLaterView.isHidden = true
            self.createRideView.createRideTralingConstraint.constant = 95
            self.rideConfirmationView.rideDetailLblHeightConstaint.constant = 10
            self.rideConfirmationView.bgCollectionView.isHidden = true
            self.createRideView.rideNowBtn.setImage(nil, for: .normal)
            self.createRideView.rideNowBtn.setTitle("Book Now".localized, for: .normal)
            self.createRideView.rideLaterBtnTheme2.isHidden = false
            self.createRideView.rideLaterBtnTheme2.layer.borderColor = UIColor.gStartColor.cgColor
            self.createRideView.rideLaterBtnTheme2.layer.borderWidth = 1
            self.createRideView.rideLaterBtnTheme2.layer.cornerRadius = 8
            self.fromImage.isHidden = true
            self.toImage.isHidden = true
            self.theme2PickUpDropImageView.isHidden = false
            
            pickUpTitleLbl.isHidden = false
            dropTitleLbl.isHidden = false
            //            self.pickUpTitleLblHeightConstraint.constant = 16
            //            self.dropTitleLblHeightContraint.constant = 16
      //      self.rideConfirmationView.paymentStackView.axis = .vertical
        //    self.rideConfirmationView.cashWalletStackview.alignment = .leading
        //    self.rideConfirmationView.PaymentBookorOthersStackView.axis = .horizontal
        //    self.rideConfirmationView.bookForOtherStackView.axis = .horizontal
         //   self.rideConfirmationView.bookforOthersPhoneNumberStackView.axis = .vertical
        }
        self.rideConfirmationView.rideLaterView.selectedDateTime = Date().dateFromFormater("yyyy-MM-dd HH:mm:ss") ?? ""
        self.rideConfirmationView.rideLaterView.dateLbl.text = Date().dateFromFormater("EEEE MMM dd yyyy") ?? ""
        let time =  Date().dateFromFormater("HH:mm:ss") ?? ""
        self.rideConfirmationView.rideLaterView.pickupTimeLbl.text = "Pick Up Time".localized + " - " + time
        
        self.ridemenu.reloadData()
        self.view.layoutIfNeeded()
    }
    
    
    
    
    
    
    func insertView(_ view: UIView) {
        if !UserDefaults.companyLogin {
            if !self.statusContentView.arrangedSubviews.contains(view) {
                for arrangedSubView in self.statusContentView.arrangedSubviews {
                    arrangedSubView.removeFromSuperview()
                }
                self.statusContentView.insertArrangedSubview(view, at: 0)
            }
        }
    }
    
    var categories: [Category] = [] {
        didSet {
            if self.categories.count > 0 {
                if self.rideCreationState == .confirmRide {
                    self.insertView(self.rideConfirmationView)
                    if !UserDefaults.secondTheme {
                        self.rideConfirmationView?.ridemenu?.reloadData()
                    }
                    
                    if let selectedCategory = self.selectedCategory {
                        self.selectedCategory = self.categories.filter{$0.id == selectedCategory.id}.first
                    } else {
                        self.selectedCategory = self.categories.first
                    }
                    if !cardPayment {
                        self.rideConfirmationView?.walletBalanceLbl?.text = "Wallet Balance".localized + ": " +
                        Extension.updateCurrecyRate(fare: "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)").currencyValue
                        
                        // "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)".currencyValue
                    }
                } else {
                    self.insertView(self.createRideView)
                    
                    if let selectedCategory = self.selectedCategory {
                        self.selectedCategory = self.categories.filter{$0.id == selectedCategory.id}.first
                    } else {
                        self.selectedCategory = self.categories.first
                    }
                }
            }
            DispatchQueue.main.async { [weak self] in
                self?.ridemenu?.reloadData()
                self?.rideConfirmationView?.ridemenu?.reloadData()
            }
        }
    }
    
    @IBAction func ShowSlidemenu() {
        
        if self.menuBtn.image(for: UIControl.State.normal) == #imageLiteral(resourceName: "img_menu_more").withRenderingMode(.alwaysTemplate){
            self.menuBtn.setImage(#imageLiteral(resourceName: "img_menu_more").withRenderingMode(.alwaysTemplate), for: UIControl.State())
            self.insertView(self.createRideView)
        } else {
            self.slideMenuController()?.openLeft()
        }
        
    }
    
    func observeRideActions() {
        
        self.createRideView.rideNow = { [weak self] in
            guard self?.checkDestination() == true else {
                return
            }
            if Double(self?.selectedCategory?.rideEstimate?.estimatedFare ?? 0) == 0 || Double(self?.selectedCategory?.rideEstimate?.estimatedFare ?? 0) == 0.0 {
                self?.view.makeToast("Please wait calculating fare".localized)
                return
            }
            self?.bookforOthersPhoneNumber = ""
            self?.bookforOthersName = ""
            
            self?.insertView((self?.rideConfirmationView)!)
            self?.rideConfirmationView?.isForRideLater = false
            self?.rideConfirmationView.viewModel = RideConfirmationViewModel(category: self?.selectedCategory)
            self?.rideCreationState = .confirmRide
            if !UserDefaults.secondTheme {
                let index = self?.categories.firstIndex(of: self?.selectedCategory ?? Category()) ?? 0
                self?.rideConfirmationView?.ridemenu?.scrollToItem(at: IndexPath(item: index, section: 0), at: UICollectionView.ScrollPosition.left, animated: false)
            }
            if let selectedCategory = self?.selectedCategory {
                self?.updateCreateRideActionVisibilityWithCategory(selectedCategory)
            }
            DispatchQueue.main.async {
                self?.rideConfirmationView?.walletBalanceLbl?.text = "Wallet Balance".localized + ": " + Extension.updateCurrecyRate(fare: "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)").currencyValue
                // "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)".currencyValue
                if !UserDefaults.secondTheme {
                    self?.rideConfirmationView?.ridemenu?.reloadData()
                    self?.rideConfirmationView?.ridemenu.isHidden = false
                }else {
                    self?.rideConfirmationView?.ridemenu.isHidden = true
                }
            }
        }
        self.createRideView.rideLater = { [weak self] in
            guard self?.checkDestination() == true else {
                return
            }
            self?.insertView((self?.rideConfirmationView)!)
            self?.rideConfirmationView?.isForRideLater = true
            self?.rideConfirmationView.viewModel = RideConfirmationViewModel(category: self?.selectedCategory)
            self?.rideCreationState = .confirmRide
            if UserDefaults.secondTheme {
                let index = self?.categories.firstIndex(of: self?.selectedCategory ?? Category()) ?? 0
                self?.rideConfirmationView?.ridemenu?.scrollToItem(at: IndexPath(item: index, section: 0), at: UICollectionView.ScrollPosition.left, animated: false)
            }
            if let selectedCategory = self?.selectedCategory {
                self?.updateCreateRideActionVisibilityWithCategory(selectedCategory)
            }
            DispatchQueue.main.async {
                self?.rideConfirmationView?.walletBalanceLbl?.text = "Wallet Balance".localized + ": " +
                Extension.updateCurrecyRate(fare: "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)").currencyValue
                
                // "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)".currencyValue
                if !UserDefaults.secondTheme {
                    self?.rideConfirmationView?.ridemenu?.reloadData()
                    self?.rideConfirmationView?.ridemenu.isHidden = false
                }else{
                    self?.rideConfirmationView?.ridemenu.isHidden = true
                }
            }
        }
        self.rideActionView.didAction = { [weak self] in
            self?.showAlertWithMessage("Cancel confirmation".localized, phrase1: "Yes".localized, phrase2: "No".localized, action: UIAlertAction.Style.default, .cancel) { [weak self](style) in
                self?.dismiss(animated: false, completion: nil)
                if style == .default {
                    let cancelVC = CancelReasonViewController.initFromStoryBoard(.Main)
                    (cancelVC).rideDetail = self?.viewModel.currentRide
                    (cancelVC).locatioDetails = self?.sourceLocationDetail
                    (cancelVC).didCancelRide = { [weak self] in
                        self?.rideCreationState = .createRide
                        self?.viewModel = DashboardViewModel(RideList())
                        self?.rideCheckHelper?.timerAction()
                        self?.restoreDashboard()
                    }
                    cancelVC.modalPresentationStyle = .overCurrentContext
                    self?.present(cancelVC, animated: true, completion: nil)
                }
            }
            //
            //            self.startActivityIndicatorInWindow()
            //            ServiceManager().cancelRide(self.viewModel.currentRide, reason: "", completion: { (inner) -> (Void) in
            //                self.stopActivityIndicator()
            //                do {
            //                    let response = try inner()
            //                    self.view.makeToast(response?.message)
            //                } catch {
            //                    self.view.makeToast((error as? CoreError)?.description)
            //                }
            //            })
        }
        self.rideActionView.didCallAction = { [weak self] in
            self?.call(to:  "+91" + (self?.currentRideList.first?.driver_info?.mobile ?? ""))
            
        }
        self.rideActionView.didMessageAction = { [weak self] in
            let singleChatVC = SingleChatViewController.initFromStoryBoard(.Chat)
            self?.rideActionView.chatBtn.badgeValue = ""
            self?.waitingTimeChatBtn.badgeValue = ""
            self?.present(singleChatVC, animated: true, completion: nil)
        }
        self.rideActionView.minimizeStatus = { [weak self] in
            self?.insertView((self?.statusView)!)
            self?.statusView.viewModel = RideStatusViewModel.init( self?.fetchedRide ?? self?.viewModel.currentRide)
        }
        self.statusView.hideStaus = { [weak self] in
            self?.insertView((self?.rideActionView)!)
        }
        self.rideConfirmationView.didRequestToConfirmRide = { [weak self] isRideLater in
            if self?.selectedPaymentMethod == .wallet{
                if Double(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0) < Double(self?.selectedCategory?.rideEstimate?.estimatedFare ?? 0){
                    self?.showAlertWithMessage("you don't have suficient money in your wallet. Want to upgrade your wallet balance?".localized, phrase1: "Yes".localized, phrase2: "Cancel".localized, skipDismiss: false, action: UIAlertAction.Style.default, .cancel, completion: { [weak self] (style) in
                        if style == .default {
                            let walletVC = WalletAmountVC.initFromStoryBoard(.Payment)
                            self?.navigationController?.pushViewController(walletVC, animated: true)
                        }
                    })
                }else{
                    self?.showAlertWithMessage("Are you sure you want to confirm Ride?".localized, phrase1: "Yes".localized, phrase2: "No".localized, skipDismiss: false, action: UIAlertAction.Style.default, .cancel, completion: { [weak self] (style) in
                        if style == .default {
                            confirmRide()
                        }
                    })
                }
            }else {
                self?.showAlertWithMessage("Are you sure you want to confirm Ride?".localized, phrase1: "Yes".localized, phrase2: "No".localized, skipDismiss: false, action: UIAlertAction.Style.default, .cancel, completion: { [weak self] (style) in
                    if style == .default {
                        confirmRide()
                    }
                })
            }
            
            func confirmRide() {
                let confirmInfo = RequestConfirmRide()
                confirmInfo.category = self?.selectedCategory?.serviceType ?? ""
                confirmInfo.vehicle_type = self?.selectedCategory?.id ?? 0
                confirmInfo.map_root = self?.distanceDuration?.root?.points ?? ""
                confirmInfo.city = GlobalValues.sharedInstance.cityName//self?.sourceLocationDetail?.cities.city ?? ""
                confirmInfo.s_address = self?.sourceLocationDetail?.address ?? ""
                confirmInfo.d_address = self?.destinatioLocationDetail?.address ?? ""
                confirmInfo.s_latitude = self?.sourceLocationDetail?.coordinate.latitude ?? 0.0
                confirmInfo.s_longitude = self?.sourceLocationDetail?.coordinate.longitude ?? 0.0
                confirmInfo.d_latitude = self?.destinatioLocationDetail?.coordinate.latitude ?? 0.0
                confirmInfo.d_longitude = self?.destinatioLocationDetail?.coordinate.longitude ?? 0.0
                confirmInfo.estimated_distance = "\(self?.distanceDuration?.km ?? 0.0)"
                confirmInfo.estimated_duration = "\(self?.distanceDuration?.time ?? 0)"
                confirmInfo.estimated_fare = Double(self?.selectedCategory?.rideEstimate?.estimatedFare ?? 0)
                confirmInfo.promo_code_id = self?.selectedCoupon?.id?.stringValue ?? ""
                //            confirmInfo.ride_id = "0"
                confirmInfo.payment_method = self?.selectedPaymentMethod == .cash ? 1 : self?.selectedPaymentMethod == .wallet ? 3 : 2
                confirmInfo.ride_type = isRideLater ? "Later" : "Now"
                //
                // self.bookingDate = rideDetail.booking_time?.UTCToLocal("yyyy-MM-dd HH:mm:ss", formatTo: "dd\nMMM\nyyyy") ?? ""
                confirmInfo.ride_datetime = isRideLater ? self?.rideConfirmationView.rideLaterView.selectedDateTime.localToUTC(incomingFormat: "yyyy-MM-dd HH:mm:ss", outGoingFormat: "yyyy-MM-dd HH:mm:ss") ?? "" : Date().dateFromFormater("yyyy-MM-dd HH:mm:ss") ?? ""
                confirmInfo.country_full_name = self?.sourceLocationDetail?.country?.country_full_name ?? ""
                confirmInfo.country_short_name = self?.sourceLocationDetail?.country?.country_short_name ?? ""
                confirmInfo.trip_info = "\(self?.selectedCategory?.fareBreakup?.id ?? 0)" + "," + "\(self?.selectedCategory?.fareBreakup?.type ?? "")"
                confirmInfo.rider_id = "\(CacheManager.userInfo?.id ?? 0)"
                confirmInfo.driver_id = "0"
                confirmInfo.category_id = RideCategory.Ride.rawValue
                confirmInfo.card_id = self?.selectedCardID ?? "0"
                confirmInfo.booking_for_name = self?.bookforOthersName
                confirmInfo.booking_for_phone = self?.bookforOthersPhoneNumber
                confirmInfo.rejected_count = 0
                confirmInfo.search_latitude = self?.sourceLocationDetail?.coordinate.latitude ?? 0.0
                confirmInfo.search_longitude = self?.sourceLocationDetail?.coordinate.longitude ?? 0.0
                self?.startActivityIndicatorInWindow()
                ServiceManager().confirmRide(confirmInfo, completion: { [weak self] (inner) -> (Void) in
                    self?.rideCheckHelper?.timerAction()
                    self?.stopActivityIndicator()
                    do {
                        let response = try inner()
                      //  self?.view.makeToast(response?.message?.stringValue)
                        self?.selectedRide = ""
                        if isRideLater {
                            let alertVC = self?.alertController
                            alertVC?.message = "Your ride has been created, you will get the driver details before 15 min from booking time".localized
                            alertVC?.addAction(.default, buttonPhrase: "Ok".localized, completion: { [weak self] (style) in
                                DispatchQueue.main.async { [weak self] in
                                    self?.dismiss(animated: true, completion: nil)
                                    self?.rideCheckHelper?.stopListening()
                                    self?.removeChatObserver()
                                    self?.rideCheckHelper = nil
                                    self?.stopAnimationTimer()
                                    self?.releaseCallBacks()
                                    Router.setDashboardViewControllerAsRoot()
                                }
                            })
                            self?.present(alertVC!, animated: true, completion: nil)
                        } else {
                            self?.rideConfirmationView?.didRequestToCancel?()
                        }
                    } catch {
                        self?.view.makeToast((error as? CoreError)?.description)
                    }
                    self?.closeRideBtn.isHidden = true
                    self?.newRideBool = false
                })
            }
        }
        self.rideConfirmationView.didRequestToShowCouponList = { [weak self] in
            let applyCouponVC = ApplyCouponViewController.initFromStoryBoard(.Dashboard)
            applyCouponVC.applyCouponCode = { [weak self] couponCode, coupon in
                self?.selectedCoupon = coupon
                self?.rideConfirmationView?.couponTextField?.text = couponCode
                self?.navigationController?.popViewController(animated: true)
            }
            self?.navigationController?.pushViewController(applyCouponVC, animated: true)
        }
        self.rideConfirmationView.didRequestToCancel = {[weak self] in
            self?.rideCreationState = .createRide
            self?.insertView(self?.createRideView ?? UIView())
            //            self.ridemenu?.currentItemIndex = self.selectedCategoryIndex
            if let selectedCategory = self?.selectedCategory {
                self?.updateCreateRideActionVisibilityWithCategory(selectedCategory)
            }
        }
        self.rideConfirmationView.showContactList = {[weak self] in
            OperationQueue.main.addOperation { [weak self] in
                let contactPicker = CNContactPickerViewController()
                contactPicker.delegate = self
                self?.present(contactPicker, animated: true, completion: nil)
            }
        }
        self.rideConfirmationView.clearContactInformation = {[weak self] in
            self?.rideConfirmationView.contactInformationLbl?.titleLabel?.numberOfLines = 2
            self?.rideConfirmationView.contactInformationLbl?.titleLabel?.lineBreakMode = .byClipping
            self?.rideConfirmationView.contactInformationLbl.setTitle("Personal".localized, for: .normal)
            self?.bookforOthersPhoneNumber = ""
            self?.bookforOthersName = ""
            self?.rideConfirmationView.closeBookForOtherBtn.isHidden = true
        }
        self.rideConfirmationView.moreRideDetailView.showFareInfo = { [weak self] in
            let vehicleInfoVC = VehicleInfoViewController.initFromStoryBoard(.Main)
            vehicleInfoVC.category = self?.selectedCategory
            vehicleInfoVC.modalPresentationStyle = .overCurrentContext
            self?.present(vehicleInfoVC, animated: true, completion: nil)
        }
        self.rideConfirmationView.rideLaterView.chooseDateTime = { [weak self] in
            let calendarVC = CalendarViewController.initFromStoryBoard(.Base)
            calendarVC.didSelectDateTime = { [weak self] (date, time) in
                self?.rideConfirmationView.rideLaterView.selectedDateTime = date?.dateFromFormater("yyyy-MM-dd HH:mm:ss") ?? ""
                self?.rideConfirmationView.rideLaterView.dateLbl.text = date?.dateFromFormater("EEEE MMM dd yyyy") ?? ""
                self?.rideConfirmationView.rideLaterView.pickupTimeLbl.text = "Pick Up Time".localized + " - " + (time ?? "")
                self?.dismiss(animated: true, completion: nil)
            }
            calendarVC.modalPresentationStyle = .overCurrentContext
            self?.present(calendarVC, animated: true, completion: nil)
        }
        self.rideConfirmationView.toggleRideInfo = { [weak self] in
        }
        self.rideConfirmationView.changePaymentType = { [weak self] in
            self?.dropDown.show()
        }
        self.paymentActionView.refreshView = { [weak self] in
            self?.currentRideStatus = .None
        }
        self.paymentActionView.completPayment = { [weak self] in
            self?.insertView((self?.ratingActionView)!)
            self?.speak(text: Speech.feedbackRequest.rawValue)
            self?.ratingActionView.viewModel = RatingActionViewModel.init(rideDetail: (self?.fetchedRide ?? self?.viewModel.currentRide)!)
        }
        self.paymentActionView.payByCard = { [weak self] in
            self?.getServerRegisterId()
            self?.openRazorpayCheckout()
        }
        self.ratingActionView.onClickSubmitRating = { [weak self] in
            var feedBackInfo = FeedbackInfo()
            feedBackInfo.rating = self?.ratingActionView.rateUserView.rating
            feedBackInfo.feedback = self?.ratingActionView.reviewTextView.text
            feedBackInfo.driver_id = "\((self?.fetchedRide ?? self?.viewModel.currentRide)?.driver_info?.id ?? 0)"
            self?.startActivityIndicatorInWindow()
            ServiceManager().updateRideFeedback(self?.fetchedRide ?? self?.viewModel.currentRide, feedBack: feedBackInfo, completion: { [weak self] (inner) -> (Void) in
                self?.stopActivityIndicator()
                do {
                    let _ = try inner()
                    self?.speak(text: Speech.feedbackThanks.rawValue)
                    self?.rideCreationState = .createRide
                    self?.viewModel = DashboardViewModel(RideList())
                    self?.restoreDashboard()
                } catch {
                    //                    self?.view.makeToast((error as? CoreError)?.description)
                }
            })
        }
        self.rideDestinationChangesView.toggleDestinationRideInfo = { [weak self] in
            self?.startActivityIndicator()
            let confirmInfo = RequestConfirmRide()
            confirmInfo.rider_id = "\(self?.viewModel.currentRide?.id ?? 0)"
            confirmInfo.vehicle_type = self?.UpdatedDestinationCategory.id ?? 0
            confirmInfo.map_root = self?.UpdatedDestinationDistanceDuration?.root?.points ?? ""
            confirmInfo.city = GlobalValues.sharedInstance.cityName//self?.sourceLocationDetail?.cities.city ?? ""
            confirmInfo.s_address = self?.sourceLocationDetail?.address ?? ""
            confirmInfo.d_address = self?.UpdatedDestinationLocation?.address ?? ""
            confirmInfo.s_latitude = self?.sourceLocationDetail?.coordinate.latitude ?? 0.0
            confirmInfo.s_longitude = self?.sourceLocationDetail?.coordinate.longitude ?? 0.0
            confirmInfo.s_name = ""
            confirmInfo.d_name = ""
            confirmInfo.d_latitude = self?.UpdatedDestinationLocation?.coordinate.latitude ?? 0.0
            confirmInfo.d_longitude = self?.UpdatedDestinationLocation?.coordinate.longitude ?? 0.0
            confirmInfo.estimated_distance = "\(self?.UpdatedDestinationDistanceDuration?.km ?? 0.0)"
            confirmInfo.estimated_duration = "\(self?.UpdatedDestinationDistanceDuration?.time ?? 0)"
            confirmInfo.estimated_fare = Double(self?.UpdatedDestinationCategory.rideEstimate?.estimatedFare ?? 0)
            confirmInfo.driver_id = "\((self?.fetchedRide ?? self?.viewModel.currentRide)?.driver_id ?? 0)"
            confirmInfo.category_id = RideCategory.Ride.rawValue
            ServiceManager().updateDestinationAddress(confirmInfo, reason: "", completion: { [weak self] (inner) -> (Void) in
                do {
                    _ = try inner()
                    self?.rideDestinationChangesView.isHidden = true
                    //   self?.destinatioLocationDetail = self?.UpdatedDestinationLocation
                    self?.currentRideStatus = .None
                    self?.viewModel.updateDashboardViewController(self!)
                    
                    self?.speak(text: Speech.updateDestination.rawValue)
                } catch {
                }
                self?.stopActivityIndicator()
            })
        }
        self.rideListView.addNewCard = { [weak self] in
            let paymentVC = AddDebitCardViewController.initFromStoryBoard(.Payment)
            paymentVC.delegate = self
            self?.navigationController?.pushViewController(paymentVC, animated: true)
        }
    }
    
    func updateCreateRideActionVisibilityWithCategory(_ category: Category?) {
        let isAbleToMakeRide = (category?.nearByCabs?.count ?? 0) > 0
        self.createRideView.rideLaterBtn.isUserInteractionEnabled = true
        self.rideConfirmationView.confirmRideBtn.isUserInteractionEnabled = true
        
        (self.createRideView?.rideNowBtn.superview as? GradientView)?.enableView(isAbleToMakeRide)
        
        (self.createRideView?.rideLaterBtn.superview as? GradientView)?.enableRideLaterView(true)
        
        (self.rideConfirmationView.confirmRideBtn.superview as? GradientView)?.enableView(isAbleToMakeRide)
        self.createRideView?.rideNowBtn?.enableBtn(isAbleToMakeRide)
        self.createRideView?.rideLaterBtn?.enableRideLaterBtn(true)
        self.rideConfirmationView.confirmRideBtn?.enableBtn(isAbleToMakeRide)
    }
    
    func calculateWaitingTime() {
        let dateString = "\(self.viewModel?.currentRide?.datetime ?? "")"
        if dateString == ""{
            return
        }
        if self.currentRideStatus == .Arrived {
            waitingTimeSeconds = 0
            self.rideActionView.waitingTimeStackView.isHidden = false
            self.rideActionView.waitingTimeHeightContraint.constant = 30
            self.rideActionView.waitingTimeTopContraint.constant = 30
            //  let endDate = "\(self.currentRideList.first?.datetime ?? "")".toDateTime()
            // let endDate = "\(self.currentRideList.first?.datetime ?? "")".toDateTime()
            // let startDate = "\(self.currentRideList.first?.arrival_time ?? "")".toDateTime()
            let currentVal = "\(self.currentRideList.first?.arrival_time ?? "")".UTCToLocal("yyyy-MM-dd HH:mm:ss", formatTo: "yyyy-MM-dd HH:mm:ss")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            let arrivalDate = dateFormatter.date(from: currentVal)
            let dateComponents = Calendar.current.dateComponents([Calendar.Component.second], from: ((arrivalDate ?? NSDate() as Date) as Date), to: NSDate() as Date)
            waitingTimeSeconds = dateComponents.second!
            let time = timeString(time: TimeInterval(waitingTimeSeconds))
            self.rideActionView.waitingTimeLbl.text = time
            if waitingTimer != nil {
                waitingTimer?.invalidate()
            }
            waitingTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runWaitingTimer), userInfo: nil, repeats: true)
        }else{
            self.rideActionView.waitingTimeStackView.isHidden = true
            self.rideActionView.waitingTimeHeightContraint.constant = 30
            self.rideActionView.waitingTimeTopContraint.constant = 30
            
        }
        if self.currentRideStatus == .Started {
            self.rideActionView.otpLbl.isHidden = true
        }
    }
    
    @IBAction func chatWaitingBtnAction(_ sender: Any) {
        let singleChatVC = SingleChatViewController.initFromStoryBoard(.Chat)
        singleChatVC.setUserInfo(self.fetchedRide?.driver_info)
        singleChatVC.rideDriverID = self.fetchedRide?.driver_id ?? 0
        
        self.rideActionView.chatBtn.badgeValue = ""
        self.waitingTimeChatBtn.badgeValue = ""
        
        self.present(singleChatVC, animated: true, completion: nil)
    }
    
    func calculateRideWaitingTime() {
        let dateString = "\(self.currentRideList.first?.ride_waiting?.last?.start_date ?? "")"
        if dateString == ""{
            return
        }
        if self.currentRideStatus == .WaitingTime {
            waitingTimeSeconds = 0
            let currentVal = "\(self.currentRideList.first?.ride_waiting?.last?.start_date ?? "")".UTCToLocal("yyyy-MM-dd HH:mm:ss", formatTo: "yyyy-MM-dd HH:mm:ss")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            let arrivalDate = dateFormatter.date(from: currentVal)
            let dateComponents = Calendar.current.dateComponents([Calendar.Component.second], from: ((arrivalDate ?? NSDate() as Date) as Date), to: NSDate() as Date)
            waitingTimeSeconds = dateComponents.second!
            let time = timeString(time: TimeInterval(waitingTimeSeconds))
            self.rideActionView.waitingTimeLbl.text = time
            if waitingTimer != nil {
                waitingTimer?.invalidate()
            }
            waitingTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTripWaitingTimer), userInfo: nil, repeats: true)
        }
        self.newRidebtn.isHidden = true
    }
    
    @objc func runWaitingTimer() {
        waitingTimeSeconds = waitingTimeSeconds + 1
        let time = timeString(time: TimeInterval(waitingTimeSeconds))
        self.rideActionView.waitingTimeLbl.text = time
    }
    
    
    
    
    
    
    var selectedPaymentMethod: PaymentMethod = .cash {
        didSet {
            self.rideConfirmationView?.walletBalanceLbl?.superview?.isHidden = selectedPaymentMethod == .cash || selectedPaymentMethod == .card
        }
    }
    
    var selectedCategory: Category? {
        didSet {
            self.selectedCategory?.distance = "\(self.distanceDuration?.km ?? 0.0)"
            self.selectedCategory?.rideEstimate?.travelTimeInMinutes = self.distanceDuration?.time
            updateCreateRideActionVisibilityWithCategory(self.selectedCategory)
            self.rideConfirmationView?.viewModel = RideConfirmationViewModel(category: self.selectedCategory)
            self.addNearbyCabs()
        }
    }
    
    lazy var dropDown: DropDown = { [weak self] in
        let dropDwn = DropDown()
        dropDwn.anchorView = self?.rideConfirmationView.paymentMethodIconImage
        dropDwn.direction = .any
        dropDwn.backgroundColor = .white
        dropDwn.selectionBackgroundColor = .white
        dropDwn.width = 80.0
        dropDwn.cellHeight = 35.0
        //        dropDwn.cornerRadius = 5.0
        dropDwn.SetBorder(width: 1, color: UIColor.colorDarkGray)
        dropDwn.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
        
        dropDwn.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDropDownCell else { return }
            //            cell.iconImg.image = UIImage(named: item.capitalized)?.withRenderingMode(.alwaysTemplate)
            //            cell.iconImg.tintColor = AppColors.colorAccent
        }
        
        dropDwn.dataSource = PaymentMethod.allRawValues
        dropDwn.textFont = UIFont(name: "Montserrat-SemiBold", size: 14) ?? UIFont.systemFont(ofSize: 14)
        
        dropDwn.selectionAction = { [weak self] (index: Int, item: String) in
            
            self?.selectedPaymentMethod = PaymentMethod(rawValue: item)!
            
            self?.selectedCardID = "0"
            
            if self?.selectedPaymentMethod == PaymentMethod.card{
                //Disabled Default Card selection flow
                //                self?.showCardList()
                self?.cardPayment = true
                self?.proceedWithCardPayment()
            }else {
                self?.cardPayment = false
                self?.rideConfirmationView?.walletBalanceLbl?.text = "Wallet Balance".localized + ": " +
                Extension.updateCurrecyRate(fare: "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)").currencyValue
                
                //  "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)".currencyValue
                
            }
            self?.rideConfirmationView?.paymentMethodValueLbl?.text = item
            dropDwn.hide()
        }
        return dropDwn
    }()
    
    @IBAction func showCardList() {
        self.cardPayment = true
        self.rideListView.frame = CGRect(x: 0, y: 0, width: (self.view.frame.width), height: (self.view.frame.height))
        self.view.addSubview(self.rideListView)
        self.view.bringSubviewToFront(self.rideListView)
        self.rideListView.rideListTableView.register(UINib(nibName: "CardListCell", bundle: Bundle.main), forCellReuseIdentifier: "CardListCell")
        self.rideListView.rideListTableView.tableFooterView = UIView()
        self.rideListView.rideListTableView.reloadData()
        self.rideListView.addCardBtn.isHidden = false
        self.rideListView.titleLbl.text = "Select Card"
        
        let str = self.selectedCard?.card_no
        let last4 = String(str?.suffix(4) ?? "")
        
        self.rideConfirmationView?.walletBalanceLbl?.text = self.selectedCard != nil ? String(format: "XXXX-XXXX-XXXX-%@ (\(self.selectedCard?.card_name ?? ""))", last4 ) : "Choose Card"
    }
    
    func proceedWithCardPayment() {
        self.selectedCard = nil
        self.rideConfirmationView?.walletBalanceLbl?.text = nil
        self.selectedCardID = "0"
    }
    
    
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        self.rideConfirmationView.contactInformationLbl?.titleLabel?.numberOfLines = 2
        self.rideConfirmationView.contactInformationLbl?.titleLabel?.lineBreakMode = .byClipping
        self.rideConfirmationView.contactInformationLbl.setTitle("\(contacts.first?.info.0 ?? "")(\(contacts.first?.phoneNumbers.first?.value.stringValue ?? ""))", for: .normal)
        bookforOthersPhoneNumber = contacts.first?.info.1 ?? ""
        bookforOthersName = contacts.first?.info.0 ?? ""
        self.rideConfirmationView.closeBookForOtherBtn.isHidden = false
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if cardPayment{
            let item = CacheManager.riderInfo?.card_info?[indexPath.row]
            self.selectedCard = item
            let str = item?.card_no
            let last4 = String(str?.suffix(4) ?? "")
            
            self.rideConfirmationView?.walletBalanceLbl?.text = String(format: "XXXX-XXXX-XXXX-%@ (\(item?.card_name ?? ""))", last4 )
            self.selectedCardID = "\(item?.id ?? 0)"
        }else{
            self.currentRideStatus = RideStatus.init(rawValue: self.currentRideList[indexPath.section].booking_status ?? 0)!
            var rideList = RideList()
            rideList.rides? = [self.currentRideList[indexPath.section]]
            self.selectedRide = self.currentRideList[indexPath.section].rideId
            self.viewModel.updateCurrentRideUI(rideList: rideList, dashboardVC: self)
        }
        self.rideListView.removeFromSuperview()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if !UserDefaults.secondTheme || collectionView == rideConfirmationView.ridemenu{
            let cell: RideCell = collectionView.dequeueCell(indexPath)
            let category = self.categories[indexPath.item]
            
            cell.titleLabel.text = category.displayName
            
            if ((category.nearByCabs?.count ?? 0) > 0) {
                if category.eta?.isEmpty == true {
                    cell.descLabel?.text = CacheManager.sharedInstance.getObjectForKey(.ETA) as? String
                    if let sourceCoordinate = self.sourceLocationDetail?.coordinate, let currentLocation = self.currentLocation {
                        self.mapsHelper.calculateDistanceDuration(sourceCoordinate, toLocation: currentLocation.coordinate) { [weak self] (distanceDuration) in
                            DispatchQueue.main.async { [weak self] in
                                CacheManager.sharedInstance.setObject(distanceDuration?.duration?.text?.eta, key: .ETA)
                                CacheManager.sharedInstance.saveCache()
                                cell.descLabel?.text = distanceDuration?.duration?.text?.eta }
                        }
                    }
                } else {
                    cell.descLabel?.text = category.eta?.eta
                }
                if (category.rideEstimate?.estimatedFare ?? 0) > 0 {
                    cell.rideFareLbl?.text = (Extension.updateCurrecyRate(fare: "\(category.rideEstimate?.estimatedFare ?? 0)")).estimatedFare
                } else {
                    cell.rideFareLbl?.text = ""
                }
            } else {
                cell.descLabel?.text = "Drivers Not Available".localized
                cell.rideFareLbl?.text = nil
            }
            cell.showCarInfo = { [weak self] in
                let vehicleInfoVC = VehicleInfoViewController.initFromStoryBoard(.Main)
                vehicleInfoVC.category = self?.selectedCategory
                vehicleInfoVC.modalPresentationStyle = .overCurrentContext
                self?.present(vehicleInfoVC, animated: true, completion: nil)
            }
            cell.carImage?.loadTaxiImage(category.image?.urlFromString)
            cell.titleLabel?.font = UIFont.myBoldSystemFont(ofSize: 14)
            cell.descLabel?.font = UIFont.mySystemFont(ofSize: 10)
            cell.rideFareLbl?.font = UIFont.mySystemFont(ofSize: 10)
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RideCellTheme1", for: indexPath) as! RideCellTheme1
            let category = self.categories[indexPath.item]
            
            cell.titleLabel.text = category.displayName
            
            if ((category.nearByCabs?.count ?? 0) > 0) {
                if category.eta?.isEmpty == true {
                    cell.descLabel?.text = CacheManager.sharedInstance.getObjectForKey(.ETA) as? String != nil ? "\((CacheManager.sharedInstance.getObjectForKey(.ETA) as? String)?.components(separatedBy: ": ")[1]as! String)" : nil
                    //CacheManager.sharedInstance.getObjectForKey(.ETA) as? String
                    if let sourceCoordinate = self.sourceLocationDetail?.coordinate, let currentLocation = self.currentLocation {
                        let firsLocation = CLLocation(latitude:category.nearByCabs?.first?.coordinate.latitude ?? 0, longitude:category.nearByCabs?.first?.coordinate.longitude ?? 0)
                        let secondLocation = CLLocation(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
                        let distance = firsLocation.distance(from: secondLocation) / 1000
                        print("\(distance)")
                        var timeZ = Double(distance) / Double(40)
                        timeZ = timeZ * 3600
                        let minutes = floor(timeZ / 60)
                        timeZ -= minutes * 60
                        let seconds = floor(timeZ)
                        let minuteSeconString = String(format: "%.f.%.f",Float(minutes),Float(seconds))
                        var eta = lroundf(Float(minuteSeconString)!)
                        if eta <= 0
                        {
                            eta = 1
                        }
                        cell.descLabel?.text = "\(eta)" + " Mins"
                        CacheManager.sharedInstance.setObject("ETA: " + "\(eta)" + " Mins", key: .ETA)
                        CacheManager.sharedInstance.saveCache()
                        //                        GoogleMapsHelper().calculateDistanceDuration(sourceCoordinate, toLocation: currentLocation.coordinate) { [weak self] (distanceDuration) in
                        //                            DispatchQueue.main.async { [weak self] in
                        //                                CacheManager.sharedInstance.setObject(distanceDuration?.duration?.text?.eta, key: .ETA)
                        //                                CacheManager.sharedInstance.saveCache()
                        //                                cell.descLabel?.text = distanceDuration?.duration?.text }
                        //                        }
                    }
                } else {
                    cell.descLabel?.text = category.eta
                }
                if (category.rideEstimate?.estimatedFare ?? 0) > 0 {
                    cell.rideFareLbl?.text = (Extension.updateCurrecyRate(fare: "\(category.rideEstimate?.estimatedFare ?? 0)")).currencySymbolValue
                } else {
                    cell.rideFareLbl?.text = ""
                }
                cell.etaImage.isHidden = false
            } else {
                cell.descLabel?.text = "Not Available".localized
                let ridefare = "\(category.rideEstimate?.estimatedFare ?? 0)"
                if ridefare != "0"{
                    cell.rideFareLbl?.text = (Extension.updateCurrecyRate(fare: "\(category.rideEstimate?.estimatedFare ?? 0)")).currencySymbolValue
                }else {
                    cell.rideFareLbl?.text = nil
                }
                cell.etaImage.isHidden = true
            }
            cell.showCarInfo = { [weak self] in
                let vehicleInfoVC = VehicleInfoViewController.initFromStoryBoard(.Main)
                vehicleInfoVC.category = self?.selectedCategory
                vehicleInfoVC.modalPresentationStyle = .overCurrentContext
                self?.present(vehicleInfoVC, animated: true, completion: nil)
            }
            cell.bgView.layer.cornerRadius = 5
            if selectedModel == indexPath.row {
                cell.carImage?.loadTaxiImage(category.selectedImage?.urlFromString)
                cell.bgView.layer.borderColor = UIColor.gStartColor.cgColor
                cell.bgView.backgroundColor = UIColor.colorAppBackground
                cell.bgView.layer.borderWidth = 2
                cell.bgView.layer.masksToBounds = true
                cell.etaImage.tintColor = .darkGray
            }else {
                cell.bgView.backgroundColor = .groupTableViewBackground
                cell.bgView.layer.borderColor = UIColor.lightGray.cgColor
                cell.bgView.layer.borderWidth = 1
                cell.carImage?.loadTaxiImage(category.image?.urlFromString)
                cell.etaImage.tintColor = .lightGray
            }
            cell.titleLabel?.font = UIFont.myBoldSystemFont(ofSize: 14)
            cell.descLabel?.font = UIFont.mySystemFont(ofSize: 12)
            cell.rideFareLbl?.font = UIFont.mySystemFont(ofSize: 14)
            return cell
        }
    }
    
    override func localize() {
        self.titleLbl?.text = "".localized
        self.rideConfirmationView?.confirmRideBtn?.setTitle("Confirm Ride".localized, for: UIControl.State())
        self.pickUpTitleLbl?.text = "PICK UP AT".localized
        self.dropTitleLbl.text = "WHERE TO?".localized
        self.creatingRideTitleLbl.text = "Getting you a ride please stay with us".localized
        self.serviceSearchView.searchingInfoLbl.text = "Searching Service. Please wait...".localized
    }
        
    @IBAction func rideCountBtnAction(_ sender: Any) {
        self.startActivityIndicator()
        self.currentRideList.removeAll()
        for item in 0..<self.viewModel.rides.count {
            ServiceManager().fetchRideDetails(self.viewModel.rides[item].rideId, reason: "") { [weak self] (inner) -> (Void) in
                self?.stopActivityIndicator()
                do {
                    let rideList = try inner()
                    guard rideList?.rides?.first != nil else { return }
                    self?.currentRideList.append((rideList?.rides?.first)!)
                    
                    if self?.viewModel.rides.count == self?.currentRideList.count{
                        self?.rideListView.frame = CGRect(x: 0, y: 0, width: self?.view.frame.width ?? 0, height: self?.view.frame.height ?? 0)
                        self?.view.addSubview(self?.rideListView ?? UIView())
                        self?.view.bringSubviewToFront(self?.rideListView ?? UIView())
                        self?.rideListView.rideListTableView.register(UINib(nibName: "RideListCell", bundle: Bundle.main), forCellReuseIdentifier: "RideListCell")
                        self?.rideListView.rideListTableView.tableFooterView = UIView()
                        self?.rideListView.rideListTableView.reloadData()
                        self?.rideListView.addCardBtn.isHidden = true
                        self?.stopActivityIndicator()
                    }
                }catch{
                    self?.view.makeToast("Try again")
                    self?.stopActivityIndicator()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView,
                   viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0,
                                              y: 0,
                                              width: self.rideListView.rideListTableView.frame.width,
                                              height: cardPayment ? 0 :30))
        headerView.backgroundColor = .white
        let rideId = UILabel(frame: CGRect(x: 0,
                                           y: 0,
                                           width: self.rideListView.rideListTableView.frame.width,
                                           height: cardPayment ? 0 :30))
        rideId.text = self.currentRideList[section].booking_id?.stringValue
        rideId.textColor = .black
        rideId.font = UIFont(name: "Montserrat-Bold", size: 15) ?? UIFont.systemFont(ofSize: 15)
        headerView.addSubview(rideId)
        return headerView
    }
    
    func newCardAdded(info: Card_info) {
        CacheManager.userInfo?.card_info?.append(info)
        self.rideListView?.rideListTableView?.reloadData()
        self.updateProfile()
    }
    
    
    
    
    
    
    
    @IBAction func closeNewRide(_ sender: Any) {
        newRideBool = false
        if currentRideList.isEmpty {
            self.startActivityIndicator()
            ServiceManager().fetchRideDetails(self.viewModel.rides[0].rideId, reason: "") { (inner) -> (Void) in
                
                self.stopActivityIndicator()
                do {
                    let rideList = try inner()
                    self.viewModel = DashboardViewModel(rideList)
                }catch{
                    self.view.makeToast("Try again")
                    
                }
            }
        }else {
            var rideList = RideList()
            rideList.rides? = [self.currentRideList[0]]
            self.viewModel.updateCurrentRideUI(rideList: rideList, dashboardVC: self)
        }
        self.closeRideBtn.isHidden = true
        self.newRidebtn.isHidden = false
        self.viewModel?.updateDestinationAddressView()
        self.rideCheckHelper?.timerAction()
    }
    
    
    
    
    
    
    func checkRideList() {
        if rideCheckHelper == nil {
            self.rideCheckHelper = RideCheckHelper()
        }
        self.rideCheckHelper?.startListening(on: { [weak self] (error, rideList) in
            guard error == nil else {
                return
            }
            let model = DashboardViewModel(rideList)
            model.controller = self
            self?.viewModel = model
            //    NotificationCenter.default.post(name: Notification.Name("currentridefeedback"), object: nil, userInfo: ["ridelist":rideList!])
            self?.rideCountBtn.isHidden = ((rideList?.rides?.count) ?? 0) <= 1 ? true : false
            self?.rideCountBtn.setTitle("\(((rideList?.rides?.count) ?? 0).stringValue)", for: .normal)
        })
    }
    
    
    
    
    
    
    
    
    
    func addCompanyRideDetialsView(rideDetails:RideDetail) {
        self.comapnyRideDetailsView.removeFromSuperview()
        self.comapnyRideDetailsView.frame = CGRect(x: 0, y: self.view.frame.height - 220, width: self.view.frame.width, height: 200)
        self.view.addSubview(self.comapnyRideDetailsView)
        self.comapnyRidePickUpLbl.text = rideDetails.estimated_address?.source?.address
        self.companyRideDropLbl.text = rideDetails.estimated_address?.destination?.address
        self.comapnyRideDriverNameLbl.text = rideDetails.driver_info?.name
        self.comapnyRideVechileTypeLbl.text = rideDetails.vehicle_type_info?.display_name ?? ""
        let imageURL: URL!
        if let urlStr = rideDetails.driver_info?.image, let url = URL.init(string: baseImageURL + urlStr) {
            imageURL = url
        } else {
            imageURL = nil
        }
        self.comapnyRideDriverImage.loadTaxiImage(imageURL)
    }
    
    
    
    
    
    
    @objc func runTripWaitingTimer() {
        waitingTimeSeconds = waitingTimeSeconds + 1
        let time = timeString(time: TimeInterval(waitingTimeSeconds))
        self.waitingTimeValue.text = time
    }
    
    
    
    
    
    
    
    @IBAction func addToFavoriteLocation(_ sender: Any) {
        if let destinationLocationDetail = self.destinatioLocationDetail {
            var favInfo = FavoriteLocationReqInfo()
            favInfo.address = destinationLocationDetail.address
            favInfo.name = destinationLocationDetail.cities.city
            favInfo.type = "3"
            favInfo.lat = destinationLocationDetail.coordinate.latitude
            favInfo.lng = destinationLocationDetail.coordinate.longitude
            self.startActivityIndicatorInWindow()
            ServiceManager().addFavouriteLocation(favInfo) { [weak self] (inner) -> (Void) in
                self?.stopActivityIndicator()
                do {
                    let response = try inner()
                    self?.view.makeToast(response?.message?.stringValue)
                    self?.favBtn.isHidden = true
                    self?.updateProfile()
                } catch {
                    //                    self?.view.makeToast((error as? CoreError)?.description)
                }
            }
        }
    }
    
    
    
    
    
    
    
    @IBAction func handleLeftSwipe(_ gesture: UISwipeGestureRecognizer) {
        if selectedCategoryIndex < self.categories.count {
            var contentOffset = ridemenu.contentOffset
            contentOffset.x += UIScreen.main.bounds.size.width * 1.5
            var rect = CGRect.zero
            rect.origin = contentOffset
            ridemenu.isScrollEnabled = true
            ridemenu.scrollRectToVisible(rect, animated: true)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if !UserDefaults.secondTheme && self.categories.count > 0{
            var visibleRect = CGRect()
            visibleRect.origin = scrollView.contentOffset
            visibleRect.size = scrollView.bounds.size
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            guard let indexPath = ridemenu.indexPathForItem(at: visiblePoint) else { return }
            self.selectedIndexPath = indexPath
            self.selectedCategoryIndex = indexPath.item
            self.selectedCategory = self.categories[indexPath.item]
        }
    }
    
    
    
    
    
    var currentRideList = [RideDetail]()
    
    var selectedRide = String()
    
    var newRideBool = Bool()
    
    var cardPayment = Bool()
    
    var selectedCardID = "0"
    
    var checkoutId = ""
    
    var currentRideId : String?
    
    var safariVC: SFSafariViewController?
    
    var currentRideCardDetails: Card_info?
    
    var bookforOthersPhoneNumber = ""
    
    var bookforOthersName = ""
    
    var isCalledHubLocation = Bool()
    
    var driverLatitude = ""
    var driverLongitude = ""
    var localMarkerCoordinate = LocationCoordinate()
    
    var totalDistanceTravelling : Double?
    var totalTimeTravelling : Int?
    
    var waitingTimer: Timer?
    var markerAnimationTimer: Timer?
    
    var selectedCoupon: Coupon?
    
    var selectedCategoryIndex: Int = 0
    
    var distanceDuration: DistanceDuration?
    
    var selectedModel = 0
    var waitingTimeSeconds = Int()
    
    var selectedCard: Card_info?
    
    var chatObserver: UInt!
    
    var rideCreationState: RideCreationState = .none {
        didSet {
            
        }
    }
    
    var UpdatedDestinationLocation : LocationDetail?{
        didSet {}
    }
    var UpdatedDestinationDistanceDuration : DistanceDuration?{
        didSet {}
    }
    var UpdatedDestinationCategory = Category()
    
    var zoomLevel: Float = 16.0
    var isMarkerTapped: Bool = false
    var isCurrentLocation : Bool = false
    
    var rideCheckHelper: RideCheckHelper?
    
    var fetchedRide: RideDetail?
    
    var currentRideStatus: RideStatus? = nil {
        willSet {
            print("test")
        }
    }
    
    var previousRideStatus: RideStatus? = nil
    
    var selectedIndexPath: IndexPath?
    
    var iscalledGoogleAddress = Bool()
    
    var nearByCabMarkers = [GMSMarker]()
    
    var polylineInfo: (GMSPolyline?, LocationCoordinate?)? = nil {
        didSet {
            print("Test")
        }
        willSet {
            print("Test Will")
        }
    }
    
    var isCabLocationIsShowing = false
    
    var placeHolderImage: UIImage?
    
    let mapsHelper = GoogleMapsHelper()
    
    
    
    
    
    lazy var driverIconImge: UIImageView = {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: 30, height: 30)))
        imageView.contentMode =  .scaleAspectFit
        imageView.loadTaxiImage(self.viewModel?.currentRide?.vehicle_type_info?.cab_marker?.urlFromString)
        return imageView
    }()
    
    
    
    
    
    deinit {
        print("\(self.description) deinited")
    }
    
    
    
    
    
    @IBAction func sosCall(_ sender: Any) {
        
        var info = callSOSReqInfo()
        info.ride_id = "\(self.viewModel.currentRide?.id ?? 0)"
        info.lat = "\(self.currentLocation?.coordinate.latitude ?? 0.0)"
        info.lng = "\(self.currentLocation?.coordinate.longitude ?? 0.0)"
        self.startActivityIndicatorInWindow()
        ServiceManager().callSOS(info) { (inner) in
            self.stopActivityIndicator()
            do {
                let _ = try inner()
                self.view.makeToast("SOS Sent".localized)
                self.call(to: CacheManager.settings?.sos)
            } catch {
                print(error)
            }
        }
    }
    
    @IBAction func handleRightSwipe(_ gesture: UISwipeGestureRecognizer) {
        if selectedCategoryIndex > 1 {
            var contentOffset = ridemenu.contentOffset
            contentOffset.x -= UIScreen.main.bounds.size.width * 1.5
            ridemenu.contentOffset = contentOffset
        }
    }
    
    @IBAction func cancelRideBtnAction(_ sender: Any) {
        self.showAlertWithMessage("Are you sure want cancel ride?".localized, phrase1: "Yes".localized, phrase2: "No".localized, action: UIAlertAction.Style.default, .cancel) { [weak self](style) in
            self?.dismiss(animated: false, completion: nil)
            if style == .default {
                self?.startActivityIndicatorInWindow()
                //   cancel_reason=0&driver_id=2621&prev_status=1
                ServiceManager().cancelRide(createRide: false, self?.viewModel.currentRide, reason: "",sourceLocation: (self?.sourceLocationDetail?.coordinate ?? .zero) ) {[weak self] (inner) -> (Void) in
                    self?.stopActivityIndicator()
                    do {
                        let _ = try inner()
                        self?.rideCreationState = .createRide
                        self?.viewModel = DashboardViewModel(RideList())
                        self?.restoreDashboard()
                    } catch {
                        self?.view.makeToast((error as? CoreError)?.description)
                    }
                }
            }
        }
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func callWaitingBtnAction(_ sender: Any) {
        self.call(to:  "+91" + (self.currentRideList.first?.driver_info?.mobile ?? ""))
    }
    
    @IBAction func callDriverBtnAction(_ sender: Any) {
        self.call(to:  "+91" + (self.viewModel.currentRide?.driver_info?.mobile ?? ""))
    }
    
    
    
    
    
    func releaseCallBacks() {
        self.createRideView?.releaseCallBacks()
        self.statusView?.releaseCallBacks()
        self.rideActionView?.releaseCallBacks()
        self.rideConfirmationView?.releaseCallBacks()
        //        serviceSearchView?.releaseCallBacks()
        //        serviceUnavailableView?.releaseCallBacks()
        self.paymentActionView?.releaseCallBacks()
        self.ratingActionView?.releaseCallBacks()
        //        searchAnimationView?.releaseCallBacks()
        self.rideDestinationChangesView?.releaseCallBacks()
        self.rideListView?.releaseCallBacks()
        self.waitingTimer?.invalidate()
        self.waitingTimer = nil
        self.markerAnimationTimer?.invalidate()
        self.markerAnimationTimer = nil
        //        waitingTimeChatBtn?.releaseCallBacks()
    }
    
    func updateMapForRideCreation() {
        drawPolyline(isUpdatePolyLine: false)
    }
    
    func addRideMenuSwipeGestures() -> (UISwipeGestureRecognizer, UISwipeGestureRecognizer) {
        let leftSwipe = UISwipeGestureRecognizer()
        leftSwipe.direction = .left
        leftSwipe.addTarget(self, action: #selector(handleLeftSwipe(_:)))
        let rightSwipe = UISwipeGestureRecognizer()
        rightSwipe.direction = .right
        rightSwipe.addTarget(self, action: #selector(handleRightSwipe(_:)))
        return (leftSwipe, rightSwipe)
    }
    
    func updateCurrencyDetails(){
        if  "\(CacheManager.sharedInstance.getObjectForKey(.updatedCurrencySymbol) ?? "")"  == ""{
            CacheManager.sharedInstance.setObject("$", key: .updatedCurrencySymbol)
            (CacheManager.settings)?.currency_symbol = "$"
        }
        if ((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currencies) != nil {
            for item in (((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currencies)!){
                
                if "\(CacheManager.sharedInstance.getObjectForKey(.updatedCurrencySymbol) ?? "")" == "\(item.currency_symbol ?? "")"{
                    CacheManager.sharedInstance.setObject("\(item.rate ?? "")", key: .currencyRate)
                }
            }
        }
    }
    
    func timeString(time: TimeInterval) -> String {
        let hour = Int(time) / 3600
        let minute = Int(time) / 60 % 60
        let second = Int(time) % 60
        return String(format: "%02i :%02i :%02i", hour, minute, second)
    }
    
    func animateCurrentLocationMarker() {
        markerAnimationTimer?.invalidate()
        markerAnimationTimer = nil
        markerAnimationTimer = Timer.scheduledTimer(withTimeInterval: requestCheckInterval, repeats: true, block: { [weak self] (timer) in
            DispatchQueue.main.async { [weak self] in
                self?.nearByCabMarkers.forEach{$0.rotation = [-90, -45.0, 0.0, 45.0, 90].randomElement() ?? 0.0}
            }
        })
    }
    
    func stopAnimationTimer() {
        markerAnimationTimer?.invalidate()
        markerAnimationTimer = nil
    }
    
    func moveToPassengerFeedBackScreen() {
        let feedBackVC = Router.main.instantiateViewController(withIdentifier: "RideFeedBackViewController") as! RideFeedBackViewController
        feedBackVC.currentRide =  self.fetchedRide ?? (UserDefaults.companyLogin ? self.viewModel.currentRide : self.currentRideList.first)
        feedBackVC.currentRideId = self.fetchedRide?.id ?? self.currentRideList.first?.rideId.intValue()
        feedBackVC.updateRide = { [weak self] in
            self?.rideCreationState = .createRide
            self?.viewModel = DashboardViewModel(RideList())
            self?.restoreDashboard()
        }
        if (self.navigationController?.topViewController is RideFeedBackViewController) == false {
            self.navigationController?.pushViewController(feedBackVC, animated: true) }
    }
    func getServerRegisterId() {
        let item = self.currentRideCardDetails
        if item?.registration_id == ""{
            self.view.makeToast("Invalid Card Details")
            self.stopActivityIndicator()
            return
        }
    }
    
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        LocationManager.shared.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        animateCurrentLocationMarker()
        calculateWaitingTime()
        updateThemeLayout()
        updateCurrencyDetails()
        self.rideCheckHelper?.timerAction()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //   waitingTimer?.invalidate()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        stopAnimationTimer()
    }
    
    override func currentLocationBtnPressed(_ sender: Any) {
        self.moveCurrentLocation(UIButton())
    }
    
    
    
    
    
    @objc func handleEvent() {
        // handle event
        calculateWaitingTime()
        calculateRideWaitingTime()
    }
    
    
    
    
    
    private func openRazorpayCheckout() {
        //        let paymentContactVC = Router.payment.instantiateViewController(withIdentifier: "PaymentContactViewController") as! PaymentContactViewController
        //                let tripAmount = Extension.updateCurrecyRate(fare: "\(self.currentRideList.first?.trip_info?.payable_amount ?? 0)")
        //        paymentContactVC.amountToPay = CacheManager.currency == .FC ? (Double(String(format: "%0.2f",tripAmount.doubleValue ?? 0 )) ?? 0).convertedToUSD : Double(String(format: "%0.2f",tripAmount.doubleValue ?? 0 )) ?? 0
        ////        paymentContactVC.isFromWallet = true
        //        paymentContactVC.isFromDashboard = true
        //        paymentContactVC.delegate = self
        //        self.navigationController?.pushViewController(paymentContactVC, animated: true)
        //          let paymentVC = Router.main.instantiateViewController(withIdentifier: StoryboardIDs.ProductPaymentViewController) as! ProductPaymentViewController
        //        let tripAmount = Extension.updateCurrecyRate(fare: "\(self.currentRideList.first?.trip_info?.amount ?? 0)")
        //        paymentVC.amountToPay = Double(String(format: "%0.2f",tripAmount.doubleValue ?? 0 )) ?? 0
        //          paymentVC.isFromWallet = true
        //          paymentVC.isFromDashboard = true
        //          paymentVC.delegate = self
        //          self.navigationController?.pushViewController(paymentVC, animated: true)
    }
}





extension DashboardViewController: CNContactPickerDelegate, GMSMapViewDelegate {
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print(picker)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        //self.bookForContact = contact
        print(contact)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        print(contactProperty)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelectContactProperties contactProperties: [CNContactProperty]) {
        
    }
    
    func checkDestination() -> Bool {
        guard self.destinatioLocationDetail != nil else {
            self.view.makeToast("Please choose drop location".localized)
            return false
        }
        return true
    }
    
    func tracingLocationDidFailWithError(error: NSError) {
        
    }
}





extension LocationCoordinate {
    static var zero: LocationCoordinate {
        return LocationCoordinate(latitude: 0.0, longitude: 0.0)
    }
}





extension String {
    var estimatedFare: String? {
        return "Estimated fare".localized + ": \(self.currencyValue)"
    }
    
    var eta: String? {
        return "ETA".localized + ": \(self)"
    }
}





extension UIViewController {
    func pop(_ animated: Bool = true) {
        self.navigationController?.popToViewController(self, animated: animated)
    }
    
    func restoreDashboard() {
        self.homeViewController?.rideCheckHelper?.stopListening()
        self.homeViewController?.mapView.delegate = nil
        self.homeViewController?.rideCheckHelper = nil
        self.homeViewController?.removeChatObserver()
        self.homeViewController?.stopAnimationTimer()
        self.homeViewController?.releaseCallBacks()
        Router.setDashboardViewControllerAsRoot()
    }
}





extension UIView {
    class func fromNib<T: RideCell>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    class func RideCellTheme1<T: RideCell>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}





extension DashboardViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cardPayment{
            return CacheManager.riderInfo?.card_info?.count ?? 0
        }
        return self.currentRideList.count > 0 ? 1 : 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if cardPayment{
            return 1
        }
        return self.currentRideList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if cardPayment{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CardListCell", for: indexPath) as! CardListCell
            cell.backgroundColor = .clear
            let item = CacheManager.riderInfo?.card_info?[indexPath.row]
            cell.lblName.text = item?.card_name
            let str = item?.card_no
            let last4 = String(str?.suffix(4) ?? "")
            
            cell.lblNo.text = String(format: "XXXX-XXXX-XXXX-%@", last4 )
            cell.btnDelete.tag = indexPath.row
            cell.lblDate.text = String(format: "Expires : %@", (item?.expiry_date)!)
            cell.lblBrand.text = String(format: "Brand : %@", (item?.brand)!)
            if "\(item?.brand?.lowercased() ?? "")" == "mastercard"{
                cell.imgCard.image = UIImage(named: "master")
            }else{
                cell.imgCard.image = UIImage(named: "\(item?.brand?.lowercased() ?? "")")
            }
            cell.selectionStyle = .none
            cell.btnDelete.isHidden = true
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "RideListCell", for: indexPath) as! RideListCell
        
        if self.currentRideList.count > indexPath.section {
            cell.pickUpAddressLbl.text = self.currentRideList[indexPath.section].estimated_address?.source?.address
            cell.dropAddressLbl.text = self.currentRideList[indexPath.section].estimated_address?.destination?.address
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat {
        return cardPayment ? 0 : 30
    }
    
    
}

extension DashboardViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categories.count
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if UserDefaults.secondTheme{
            if selectedModel == indexPath.row {
                let vehicleInfoVC = VehicleInfoViewController.initFromStoryBoard(.Main)
                vehicleInfoVC.modalPresentationStyle = .overCurrentContext
                self.present(vehicleInfoVC, animated: true, completion: nil)
            }else {
                selectedModel = indexPath.row
                collectionView.reloadData()
            }
            self.selectedIndexPath = indexPath
            self.selectedCategoryIndex = indexPath.item
            self.selectedCategory = self.categories[indexPath.item]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellSize = CGSize()
        if !UserDefaults.secondTheme{
            cellSize = collectionView.bounds.size
            cellSize.width -= collectionView.contentInset.left
            cellSize.width -= collectionView.contentInset.right
        }else {
            cellSize = collectionView.bounds.size
            cellSize.width = 100
        }
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

//Trip Payment
extension DashboardViewController:PaymentCompleteDelegate, AddDebitCardDelegate {
    func paymentCompletedNotification(token: String) {
        let params: NSDictionary = ["driver_id":self.fetchedRide?.driver_id ?? 0,"country_short_name":self.fetchedRide?.country_short_name ?? "","amount":self.fetchedRide?.trip_info?.payable_amount ?? 0,"commission":"0","transaction_id":token,"prev_status":self.currentRideStatus?.rawValue ?? 0]
        
        ServiceManager().cardPayment(info : params ,id: self.fetchedRide?.id ?? 0) { [weak self] (inner) -> (Void) in
            do {
                let result = try inner()
                self?.view.makeToast(result?.message?.stringValue)
                
                self?.currentRideStatus = .None
                if let weakSelf = self {
                    self?.viewModel.updateDashboardViewController(weakSelf)
                    self?.restoreDashboard()
                }
                DispatchQueue.main.asyncAfter(deadline: .now()+0.5) { [weak self] in
                    self?.presentAlert(withTitle: "Success", message: "Payment Succeeded")
                }
            }catch{
                self?.stopActivityIndicator()
                self?.view.makeToast("Try again")
            }
        }
        //        Router.setDashboardViewControllerAsRoot()
    }
}

extension DashboardViewController {
    func presentAlert(withTitle title: String?, message : String?) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "Okay", style: .default)
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

extension GradientView {
    func enableView(_ state: Bool) {
        self.startColorTheme = state ? 35 : 73
        self.endColorTheme = state ? 35 : 73
        self.borderWidth = state ? 1 : 0
        self.borderColor = state ? ColorTheme(rawValue: 1)?.color : .clear
    }
    
    func enableRideLaterView(_ state: Bool) {
        self.startColorTheme = 75//state ? 35 : 73
        self.endColorTheme = 75 //state ? 35 : 73
        self.borderWidth = state ? 1 : 0
        self.borderColor = state ? ColorTheme(rawValue: 1)?.color : .clear
    }
}

extension AppButton {
    func enableBtn(_ state: Bool) {
        self.textThemeColor = state ? 75 : 75
        self.imageThemeColor = state ? 75 : 75
    }
    
    func enableRideLaterBtn(_ state: Bool) {
        self.textThemeColor = 94//state ? 75 : 75
        self.imageThemeColor = 94//state ? 75 : 75
    }
}
