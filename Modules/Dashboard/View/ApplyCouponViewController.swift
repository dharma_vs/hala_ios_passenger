//
//  ApplyCouponViewController.swift
//  TaxiPickup
//
//  Created by SELLADURAI on 3/25/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

class ApplyCouponViewController: BaseViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var applyBtn: UIButton!
    
    @IBOutlet weak var couponTxtField: UITextField!
    
    @IBOutlet weak var couponTableView: UITableView!
    
    var applyCouponCode: ((String?, Coupon?)-> Void)?
    @IBAction func applyCoupon(_ sender: Any) {
        self.view.endEditing(true)
        guard self.couponTxtField.text?.isEmpty == false else {
            self.view.makeToast("Enter valid coupon to apply".localized)
            return
        }
        var params = VerifyCouponParams()
        params.type_ride = "ride"
        params.code = self.couponTxtField.text ?? ""
        self.startActivityIndicatorInWindow()
        ServiceManager().verifyCoupon(params) {[weak self] (inner) -> Void in
            do {
                let response = try inner()
                if response?.status == true {
                    self?.applyCouponCode?(self?.couponTxtField?.text, response?.data?.first)
                } else {
                     UIApplication.shared.keyWindow?.makeToast(response?.message)
                }
            } catch {
                print(error)
            }
            self?.stopActivityIndicator()
        }
    }
    
    var coupons: [Coupon] = [] {
        didSet {
            self.couponTableView?.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        couponTxtField.placeholder = "Enter Coupon Code".localized
        self.startActivityIndicatorInWindow()
        ServiceManager().couponList { [weak self] (inner) -> (Void) in
            do {
                let coupons = try inner()
                if coupons != nil {
                    self?.coupons = coupons!
                }
            } catch {
//                self.view.makeToast((error as? CoreError)?.description)
            }
            self?.stopActivityIndicator()
        }
        
        // Do any additional setup after loading the view.
    }

}

extension ApplyCouponViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.coupons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MycouponCell", for: indexPath) as! MycouponCell
        let coupon = self.coupons[indexPath.row]
        cell.lbl_offer.text = "\(coupon.offer ?? 0) %"
        cell.lbl_Code.text = "\(coupon.code ?? "")"
        let attributedText = NSMutableAttributedString.init(string: coupon.message ?? "", attributes: [NSAttributedString.Key.foregroundColor: UIColor(hex: 0x9A2315)])
        attributedText.append(NSAttributedString.init(string: "\nValid till \(coupon.expiry?.UTCToLocal("yyyy-MM-dd", formatTo:"dd-MM-yyyy") ?? "")", attributes: [NSAttributedString.Key.foregroundColor : UIColor.black ]))
        cell.lbl_time.attributedText = attributedText
        cell.lbl_time.numberOfLines = 0
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let coupon = self.coupons[indexPath.row]
        var params = VerifyCouponParams()
        params.type_ride = "ride"
        params.code = coupon.code ?? ""
        self.startActivityIndicatorInWindow()
        ServiceManager().verifyCoupon(params) { [weak self](inner) -> Void in
            do {
                let res = try inner()
                self?.applyCouponCode?(coupon.code, res?.data?.first)
            } catch {
                print(error)
            }
            self?.stopActivityIndicator()
        }
    }
}

extension ApplyCouponViewController: UITextFieldDelegate {
    
    
}
