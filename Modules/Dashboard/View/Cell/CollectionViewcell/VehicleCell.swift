//
//  VehicleCell.swift
//  HalaMobility
//
//  Created by ADMIN on 04/06/22.
//

import UIKit

class VehicleCell: UICollectionViewCell {
    @IBOutlet weak var btnBooknow: AppButton!
    @IBOutlet weak var lblTitle: AppLabel!
    @IBOutlet weak var lblvehicleno: AppLabel!
    @IBOutlet weak var lblminrech: AppLabel!
    @IBOutlet weak var lblminrechans: AppLabel!
    @IBOutlet weak var lblunlockfee: AppLabel!
    @IBOutlet weak var lblunlockfeeans: AppLabel!
    @IBOutlet weak var lblminutefare: AppLabel!
    @IBOutlet weak var lblminutefareans: AppLabel!
    @IBOutlet weak var lblpausefee: AppLabel!
    @IBOutlet weak var lblpausefeeans: AppLabel!
    @IBOutlet weak var lbltravelkm: AppLabel!
    @IBOutlet weak var btnDirection: AppButton!
    @IBOutlet weak var viewCircle: Circular!
    @IBOutlet weak var PageControl: UIPageControl!
    @IBOutlet weak var scrollImage: UIScrollView!
    @IBOutlet weak var stack1: UIStackView!
    @IBOutlet weak var stack2: UIStackView!
    @IBOutlet weak var stack3: UIStackView!
    @IBOutlet weak var stack4: UIStackView!
    @IBOutlet weak var viewCirclebg: AppImageView!
    
    var didRequestToNavigatetoBrowser: ((Int)->Void)?
    var didrequestbooknow: ((Int)->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        btnDirection.setTitle("", for: .normal)
        self.lblminrech.text = "Min To Reach".localized
        self.lblunlockfee.text = "Unlock Fee".localized
        self.lblminutefare.text = "Minute Fare".localized
        self.lblpausefee.text = "Pause Fare".localized
        PageControl.numberOfPages = 4
        PageControl.pageIndicatorTintColor = UIColor.lightGray
        PageControl.currentPageIndicatorTintColor = .primary
    }
    func loadData(values: Vehiclelist, det:VehicleDetail?) {
        if let name = values.displayName{
            self.lblTitle.text = name
        }else{
            self.lblTitle.text = det?.displayName
        }
        let vehiclno = values.vehicleNumber ?? "" == "" ? values.number : values.vehicleNumber
        lblvehicleno.text = vehiclno
        let firsLocation = GoogleMapsHelper().convertCllocation(values.deviceLatitude, Longitude: values.deviceLongitude)
        let distanceKM = GoogleMapsHelper().etaFrom(GlobalValues.sharedInstance.currentLocation.coordinate, drop: firsLocation.coordinate)
        //self.lblTitle.text = values.displayName
        //self.lblvehicleno.text = values.vehicleNumber
        //self.lblminutefareans.text = values.vehicleNumber
        //self.lblunlockfeeans.text = values.displayName
        //self.lblpausefeeans.text = values.vehicleNumber
        self.lbltravelkm.addLeading(image:UIImage(named: "ic_information_16x16")!, text: "You can travel upto 0 Km")
        
        if values.displayName != nil{
            stack1.isHidden = false
            stack2.isHidden = false
            stack3.isHidden = false
            stack4.isHidden = false
            lblminrechans.text = distanceKM
            var valuse  = det?.fareBreakup?.unlockFee
            self.lblunlockfeeans.text = "\(Extension.updateCurrecyRate(fare: "\(valuse ?? 0)"))".currencyValue
            valuse  = det?.fareBreakup?.minute
            self.lblminutefareans.text = "\(Extension.updateCurrecyRate(fare: "\(valuse ?? 0)"))".currencyValue
            let temppause = det?.fareBreakup?.pauseCharges
            self.lblpausefeeans.text = "\(Extension.updateCurrecyRate(fare: "\(temppause ?? 0.0)"))".currencyValue + "Per Min".localized
            
        }
        let battery:Double = (values.batteryVoltage.calculateBatterpercentage(list: values))/100
        viewCircle.setProgress(to: battery , withAnimation: true)
        if battery == 0{
            viewCirclebg.borderTheme = 40
            viewCirclebg.backgroundThemeColor = 190
        }
        
        //self.viewCircle?.animateCircleWithDuration(3, pollingTime: 1)
        for index in 0..<4 {
            var image = String()
            let imageViewObject = UIImageView()
            imageViewObject.frame = CGRect(x: scrollImage.frame.size.width * CGFloat(index), y: 0, width: scrollImage.frame.size.width, height: scrollImage.frame.size.height)
            imageViewObject.contentMode = .scaleAspectFit
            self.scrollImage.addSubview(imageViewObject)
            self.scrollImage.sendSubviewToBack(imageViewObject)
            switch index {
            case 0:
                image = values.vehiclePhotoFront ?? ""
                break
            case 1:
                image = values.vehiclePhotoRear ?? ""
                break
            case 2:
                image = values.vehicleDocument ?? ""
                break
            case 3:
                image = values.vehicleInsurance ?? ""
                break
            default:
                break
            }
            image = baseImageURL + image
            imageViewObject.hnk_setImage(from: image.urlFromString, placeholder: nil, success: { (image) in
                imageViewObject.image = image
            }, failure: nil)
            
        }
        
        scrollImage.contentSize = CGSize(width:scrollImage.frame.size.width * 4,height: scrollImage.frame.size.height)
        //PageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: .valueChanged)
    }
    @IBAction func btnclkOpenBrowser(sender:UIButton){
        self.didRequestToNavigatetoBrowser?(sender.tag)
    }
    @IBAction func btnclkBooknow(sender:UIButton){
        self.didrequestbooknow?(sender.tag)
    }
}
