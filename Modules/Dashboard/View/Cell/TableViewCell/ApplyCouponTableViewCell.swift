//
//  ApplyCouponTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/04/22.
//

import Foundation
import UIKit

class ApplyCouponTableViewCell: UITableViewCell {
    
    @IBOutlet weak var couponImg: AppImageView!
    @IBOutlet weak var couponTxtField: AppTextField!

}
