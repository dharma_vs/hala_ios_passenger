//
//  QRreaderVC.swift
//  HalaMobility
//
//  Created by ADMIN on 26/05/22.
//

import UIKit
import AVFoundation
struct QRData {
    var codeString: String?
}

class QRreaderVC: BaseViewController {
    @IBOutlet weak var btnmanual: AppButton!
    @IBOutlet weak var lbltitle: AppLabel!
    @IBOutlet weak var lblsub1: AppLabel!
    @IBOutlet weak var lblsub2: AppLabel!
    @IBOutlet weak var viewImportant: UIView!
    var viewAlert = TextboxAlertView()

    
    
    
    @IBOutlet weak var scannerView: QRScannerView! {
        didSet {
            scannerView.delegate = self
        }
    }
//    @IBOutlet weak var scanButton: UIButton! {
//        didSet {
//            scanButton.setTitle("STOP", for: .normal)
//        }
//    }
    
    var qrData: QRData? = nil {
        didSet {
            if qrData != nil {
                self.navigationController?.popViewController(animated: true)
                //self.performSegue(withIdentifier: "detailSeuge", sender: self)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.assignText()
//        self.viewAlert.clickTextboxAlertViewSubmit = {[weak self] txtvalues in
//            print(txtvalues!)
//        }
    }
    func assignText(){
        lbltitle.text = "importantinfo".localized.uppercased()
        lblsub1.text = "scanhint1".localized
        lblsub2.text = "scanhint2".localized
        self.btnmanual?.setTitle("orType".localized, for: .normal)
        lbltitle.font = UIFont.mySystemFont(ofSize: 14)
        lblsub1.font = UIFont.myLightFont(ofSize: 10)
        lblsub2.font = UIFont.myLightFont(ofSize: 10)
        btnmanual.titleLabel?.font =  UIFont.mySystemFont(ofSize: 14)

        
    }
    @IBAction func btnclkpopup() {
        let headerView = Bundle.main.loadNibNamed("TextboxAlertView", owner:
        self, options: nil)?.first as? TextboxAlertView
        headerView?.frame = self.view.frame
        self.view.addSubview(headerView!)
    }
    @IBAction func light(_ sender: UIButton) {
        sender.isSelected = sender.isSelected == true ? false : true
        self.toggleTorch(on: sender.isSelected == true ? true : false)
    }
    func toggleTorch(on: Bool) {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { print("Torch isn't available"); return }

        do {
            try device.lockForConfiguration()
            device.torchMode = on ? .on : .off
            // Optional thing you may want when the torch it's on, is to manipulate the level of the torch
            if on { try device.setTorchModeOn(level: AVCaptureDevice.maxAvailableTorchLevel.magnitude) }
            device.unlockForConfiguration()
        } catch {
            print("Torch can't be used")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if !scannerView.isRunning {
            scannerView.startScanning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !scannerView.isRunning {
            scannerView.stopScanning()
        }
    }

//    @IBAction func scanButtonAction(_ sender: UIButton) {
//        scannerView.isRunning ? scannerView.stopScanning() : scannerView.startScanning()
//        let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
//        sender.setTitle(buttonTitle, for: .normal)
//    }
}


extension QRreaderVC: QRScannerViewDelegate {
    func qrScanningDidStop() {
//        let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
//        scanButton.setTitle(buttonTitle, for: .normal)
    }
    
    func qrScanningDidFail() {
        //presentAlert(withTitle: "Error", message: "Scanning Failed. Please try again")
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        self.qrData = QRData(codeString: str)
    }
    
    
    
}


extension QRreaderVC {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(self.qrData?.codeString ?? "")
        /*if segue.identifier == "detailSeuge", let viewController = segue.destination as? DetailViewController {
            viewController.qrData = self.qrData
        }*/
    }
}
