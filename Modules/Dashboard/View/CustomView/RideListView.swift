//
//  RideListView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
//

import UIKit

class RideListView: UIView {
    
    var addNewCard: (()->Void)?
    
    func releaseCallBacks() {
        self.addNewCard = nil
    }

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var rideListTableView: UITableView!
    @IBOutlet weak var addCardBtn: UIButton!
    
    @IBAction func addCardBtnAction(_ sender: Any) {
        self.addNewCard?()

    }
    
}
