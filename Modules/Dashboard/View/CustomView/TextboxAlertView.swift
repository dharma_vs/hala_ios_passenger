//
//  TextboxAlertView.swift
//  HalaMobility
//
//  Created by ADMIN on 27/05/22.
//

import UIKit

class TextboxAlertView: UIView {

    @IBOutlet weak var lblTitle: AppLabel!
    @IBOutlet weak var txtField: AppTextField!
    @IBOutlet weak var btnCancel: AppButton!
    @IBOutlet weak var btnSubmit: AppButton!
    var clickTextboxAlertViewSubmit: ((String?)-> Void)?

    @IBAction func btnclkSubmitCancel(_ sender: UIButton) {
        if sender.tag == 0 { // cancel
            removeFromSuperview()
        }else{
            if txtField.text == ""{
                self.makeToast("pleaseenterthevehicle".localized)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtField.delegate = self
        lblTitle.text = "entervehicleno".localized
        self.btnCancel?.setTitle("Cancel".localized, for: .normal)
        self.btnSubmit?.setTitle("Submit".localized, for: .normal)
        lblTitle.font = UIFont.myLightFont(ofSize: 18)
        txtField.font = UIFont.myLightFont(ofSize: 15)
        btnCancel.titleLabel?.font =  UIFont.mySystemFont(ofSize: 12)
        btnSubmit.titleLabel?.font =  UIFont.mySystemFont(ofSize: 12)
        
    }
}
extension TextboxAlertView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.clickTextboxAlertViewSubmit?(txtField.text!)
        textField.resignFirstResponder()
    }
}
