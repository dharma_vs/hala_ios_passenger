//
//  RideNowView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.

import Foundation
import UIKit

class RideConfirmationView: UIView {
    
    var viewModel:RideConfirmationViewModel! {
        didSet {
            self.viewModel.updateRideConfirmationView(self)
        }
    }
    
    var isForRideLater: Bool = false {
        didSet {
            self.rideLaterView?.isHidden = !self.isForRideLater
        }
    }
    
    @IBOutlet weak var ridemenu: UICollectionView!
    
    @IBOutlet weak var rideDetail: AppLabel!
    
    @IBOutlet weak var rideDetailValue: AppLabel!
    
    @IBOutlet weak var capcityLbl: AppLabel!
    
    @IBOutlet weak var showHideDetailsBtn: AppButton!
    
    @IBOutlet weak var bgCollectionView: UIView!

    @IBOutlet var rideDetailLblHeightConstaint: NSLayoutConstraint!
    
    
    
    
    @IBAction func toggleRideDetails(_ sender: AppButton) {
        self.moreRideDetailView.isHidden = !self.moreRideDetailView.isHidden
        let title = self.moreRideDetailView.isHidden ? "Click here to show ride details...".localized : "Click here to hide ride details...".localized
        sender.setTitle(title, for: UIControl.State())
        toggleRideInfo?()
    }
    
    @IBAction func changePaymentMethod(_ sender: AppButton) {
        changePaymentType?()
    }
    
    @IBOutlet weak var paymentStackView: UIStackView!

    @IBOutlet weak var cashWalletStackview: UIStackView!

    @IBOutlet weak var bookForOtherStackView: UIStackView!

    @IBOutlet weak var PaymentBookorOthersStackView: UIStackView!

    @IBOutlet weak var bookforOthersPhoneNumberStackView: UIStackView!
    
    @IBOutlet weak var closeBookForOtherBtn: AppButton!
    
    @IBOutlet weak var paymentMethodLbl: AppLabel!
    
    @IBOutlet weak var paymentMethodValueLbl: AppLabel!
    
    @IBOutlet weak var paymentMethodIconImage: AppImageView!
    
    @IBOutlet weak var walletBalanceLbl: AppLabel!
    
    @IBOutlet weak var contactInformationLbl: AppButton!
    
    @IBOutlet weak var bookforOthersLbl: AppLabel!
            
    @IBOutlet weak var couponTextField: AppTextField!
    
    @IBOutlet weak var confirmRideBtn: AppButton!
    
    @IBOutlet weak var cancelBtn: AppButton!
    
    @IBOutlet weak var rideLaterView: RideLaterView!
    
    @IBOutlet weak var moreRideDetailView: MoreRideDetailView!
    
    @IBAction func confirmRide(_ sender: AppButton) {
        self.didRequestToConfirmRide?(isForRideLater)
    }
    @IBAction func picKContact(_ sender: AppButton) {
        self.showContactList?()
    }
    
    @IBAction func cancel(_ sender: AppButton) {
        self.didRequestToCancel?()
    }
    @IBAction func clearContactBtn(_ sender: AppButton) {
        self.clearContactInformation?()
    }
    
    func releaseCallBacks() {
        self.toggleRideInfo = nil
        self.changePaymentType = nil
        self.showContactList = nil
        self.clearContactInformation = nil
        self.didRequestToConfirmRide = nil
        self.didRequestToCancel = nil
        self.didRequestToShowCouponList = nil
        self.rideLaterView?.releaseCallBacks()
        self.moreRideDetailView?.releaseCallBacks()
    }
    
    var toggleRideInfo: (()->Void)?
    var changePaymentType: (()->Void)?
    var showContactList: (()-> Void)?
    var clearContactInformation: (()-> Void)?
    var didRequestToConfirmRide: ((Bool)-> Void)?
    var didRequestToCancel: (()-> Void)?
    var didRequestToShowCouponList: (()-> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.moreRideDetailView.isHidden = true
        paymentMethodLbl.text = "Payment Method".localized
        bookforOthersLbl.text = "Book for".localized
        
    }
    
}

extension RideConfirmationView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.didRequestToShowCouponList?()
        textField.resignFirstResponder()
    }
}
