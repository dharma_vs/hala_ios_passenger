//
//  RatingActionView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
//

import UIKit

class RatingActionView: UIView {
    
    var viewModel: RatingActionViewModel! {
        didSet {
            self.viewModel?.updatePaymentActionView(self)
        }
    }
    
    var onClickSubmitRating: (()->Void)?
    
    func releaseCallBacks() {
        self.onClickSubmitRating = nil
    }
    
    @IBOutlet weak var addressContentView: RideAddressView!
    @IBOutlet weak var rideDistanceInfoView: RideDistanceInfoView!
    @IBOutlet weak var rideUserInfoView: UserInfoView!
    
    @IBOutlet weak var rideFareLbl: UILabel!
    @IBOutlet weak var vehicleModelName: UILabel!
    @IBOutlet weak var discountAppliedLbl: UILabel!
    @IBOutlet weak var cashCollectedLbl: UILabel!
    @IBOutlet weak var howWasYourTripLbl: UILabel!
    
    @IBOutlet weak var rateUserView: FloatRatingView!
    @IBOutlet weak var reviewTextViewPlaceHolderLbl: UILabel!
    
    @IBOutlet weak var reviewTextView: UITextView!
    
    @IBOutlet weak var submitBtn: AppButton!
    
    @IBAction func submitRating(_ sender: Any) {
        self.onClickSubmitRating?()
    }
    
    @IBOutlet weak var packageView: UIView!
    @IBOutlet weak var packageNameLbl: UILabel!
    @IBOutlet weak var packageLbl: UILabel!
    @IBOutlet weak var startReading: UILabel!
    @IBOutlet weak var startReadingLbl: UILabel!
    
    weak var baseVC: UIViewController?
    
    @IBAction func showReading(_ sender: UIButton) {
        let profileInfo = ProfileInfo(info: "Start Reading".localized, value: "1 Photo".localized, isPhotoType: true, type: .vehicle_document, titles: ["Start Reading".localized], urlStrings: [/*"documents/"+(*/self.viewModel.detail.rental?.startReadingImage?.stringValue/*.components(separatedBy: "/").last*/ ?? ""])
        
        let imageVC = ProfileImagesViewController.initFromStoryBoard(.Main)
        imageVC.profileDocumentInfo = profileInfo
        imageVC.modalPresentationStyle = .overCurrentContext
        baseVC?.present(imageVC, animated: true, completion: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        reviewTextView.delegate = self
        rateUserView.editable = true
        
        self.rideDistanceInfoView.distanceLbl.text = "Distance".localized
        self.rideDistanceInfoView.durationLbl.text = "Duration".localized
        self.packageLbl?.text = "Package".localized
        self.startReading?.text = "Start Reading".localized
        
    }
}

extension RatingActionView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let string = textView.text?.replacingCharacters(in: Range(range, in: textView.text!)!, with: text) {
            reviewTextViewPlaceHolderLbl.isHidden = string.count > 0
        }
        return true
    }
}
