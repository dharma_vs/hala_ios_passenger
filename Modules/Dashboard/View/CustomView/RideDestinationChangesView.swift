//
//  RideDestinationChangesView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
//

import Foundation
import UIKit

class RideDestinationChangesView: UIView {

    var toggleDestinationRideInfo: (()->Void)?
    
    func releaseCallBacks() {
        self.toggleDestinationRideInfo = nil
    }

    @IBOutlet weak var destinationTitleLbl: UILabel!
    @IBOutlet weak var destinationAddressLbl: UILabel!
    @IBOutlet weak var destinationEstimateFareLbl: UILabel!
    @IBOutlet weak var destinationEstimateFareTitleLbl: UILabel!
    @IBOutlet weak var destinationAlertTitleLbl: UILabel!
    @IBOutlet weak var destinationAlertOkBtn: UIButton!
    @IBOutlet weak var destinationAlertCancelBtn: UIButton!

     @IBAction func toggleDestinationDetails(_ sender: UIButton) {
           toggleDestinationRideInfo?()
       }
    @IBAction func cancelDestinationDetails(_ sender: UIButton) {
             self.isHidden = true
        destinationAddressLbl.text = ""
        destinationEstimateFareLbl.text = ""
    }
    
    @IBOutlet weak var destinationChangeChargeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let appSettings = CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings
        self.destinationAlertOkBtn.setTitle("Yes", for: .normal)
        self.destinationAlertCancelBtn.setTitle("No", for: .normal)
        self.destinationTitleLbl.text = "Update Destination".localized
        self.destinationAlertTitleLbl.text = "Are you sure want to change your destination?".localized
        self.destinationEstimateFareTitleLbl.text = "Estimate Fare".localized
        self.destinationChangeChargeLbl?.text = "Destination charges fare $XXX will be add at end of the ride".localized.replacingOccurrences(of: "$XXX", with: Extension.updateCurrecyRate(fare: "\(String(appSettings?.location_change_charge?.intValue ?? 0))").currencyValue)
    }
       
          
}
