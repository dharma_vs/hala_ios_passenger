//
//  ServiceUnvailableView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
//

import UIKit

class ServiceUnavailableView: UIStackView {

    @IBOutlet weak var carIcon: UIImageView!
    @IBOutlet weak var serviceUnavailableLbl: UILabel!
    @IBOutlet weak var appologyLbl: UILabel!

}
