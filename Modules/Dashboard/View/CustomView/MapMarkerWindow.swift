//
//  MapMarkerWindow.swift
//  HalaMobility
//
//  Created by ADMIN on 26/07/22.
//

import UIKit
protocol MapMarkerDelegate: AnyObject {
    func didTapInfoButton()
}

class MapMarkerWindow: UIView {

    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblETA: UILabel!
    
    weak var delegate: MapMarkerDelegate?
    var spotData: NSDictionary?
    
    @IBAction func didTapInfoButton(_ sender: UIButton) {
        delegate?.didTapInfoButton()
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "MapMarkerWindow", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }
}
