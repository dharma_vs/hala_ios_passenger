//
//  CreateRideView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.

import UIKit

class CreateRideView: UIView {

    @IBOutlet weak var rideNowBtn: AppButton!
    @IBOutlet weak var rideLaterBtn: AppButton!
    @IBOutlet weak var rideLaterBtnTheme2: UIButton!
    @IBOutlet weak var titleTheme2Lbl: UILabel!

    @IBOutlet var createRideTralingConstraint: NSLayoutConstraint!
    var rideNow: (()->Void)?
    var rideLater: (()->Void)?
    
    func releaseCallBacks() {
        self.rideNow = nil
        self.rideLater = nil
    }
    
    @IBAction func rideNowBtnAction(_ sender: UIButton) {
        rideNow?()
    }
    
    @IBAction func rideLaterBtnAction(_ sender: UIButton) {
        rideLater?()
    }

}
