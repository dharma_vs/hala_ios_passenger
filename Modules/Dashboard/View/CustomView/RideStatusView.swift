//
//  RideStatusView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.

import UIKit
import Foundation

class RideStatusView: UIStackView {
    
    var viewModel: RideStatusViewModel! {
        didSet {
            self.viewModel.updateRideActionView(view: self)
        }
    }
    
    func releaseCallBacks() {
        self.hideStaus = nil
    }
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var rideStatus: UILabel!
        
    var hideStaus: (()-> Void)?
    
    @IBAction func hideStausView(_ sender: Any) {
        self.hideStaus?()
    }
}
