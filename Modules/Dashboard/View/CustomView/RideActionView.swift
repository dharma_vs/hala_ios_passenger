//
//  RideActionView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.

import Foundation
import UIKit

class RideActionView: UIStackView {
    
    var viewModel: RideActionViewModel! {
        didSet {
            self.viewModel.updateRideActionView(view: self)
        }
    }
    
    @IBOutlet weak var driverImage: UIImageView!
    
    @IBOutlet weak var driverName: UILabel!
    
    @IBOutlet weak var driverRating: FloatRatingView!
    
    @IBOutlet weak var tripsCountLbl: UILabel!
    
    @IBOutlet weak var statusInfoLbl: UILabel!
    
    @IBOutlet weak var vehicleNameLbl: UILabel!
    
    @IBOutlet weak var vehicleNumberLbl: UILabel!
    
    @IBOutlet weak var vehicleTypeLbl: UILabel!
    
    @IBOutlet weak var chatBtn: SSBadgeButton!
    
    @IBOutlet weak var rideActionBtn: UIButton!
    
    @IBOutlet weak var actionBtnHeight: NSLayoutConstraint!
    
    @IBOutlet weak var waitingTimeHeightContraint: NSLayoutConstraint!
    
    @IBOutlet weak var waitingTimeTopContraint: NSLayoutConstraint!

    @IBOutlet weak var otpLbl: UILabel!
    
    @IBOutlet weak var waitingTimeTitleLbl: UILabel!
    
    @IBOutlet weak var waitingTimeLbl: UILabel!
    
    @IBOutlet weak var waitingTimeStackView: UIStackView!

    @IBOutlet weak var otpView: UIView!
    
    @IBOutlet weak var packageView: UIView!
    @IBOutlet weak var packageNameLbl: UILabel!
    @IBOutlet weak var packageLbl: UILabel!
    @IBOutlet weak var startReading: UILabel!
    @IBOutlet weak var startReadingLbl: UILabel!
    
    weak var baseVC: UIViewController?
    
    @IBAction func showReading(_ sender: UIButton) {
        let profileInfo = ProfileInfo(info: "Start Reading".localized, value: "1 Photo".localized, isPhotoType: true, type: .vehicle_document, titles: ["Start Reading".localized], urlStrings: [/*"documents/"+(*/self.viewModel.detail?.rental?.startReadingImage?.stringValue/*.components(separatedBy: "/").last*/ ?? ""])
        
        let imageVC = ProfileImagesViewController.initFromStoryBoard(.Main)
        imageVC.profileDocumentInfo = profileInfo
        imageVC.modalPresentationStyle = .overCurrentContext
        baseVC?.present(imageVC, animated: true, completion: nil)
    }
    
    func releaseCallBacks() {
        self.didAction = nil
        self.didCallAction = nil
        self.didMessageAction = nil
        self.minimizeStatus = nil
    }
    
    var didAction: (()->Void)?
    var didCallAction: (()->Void)?
    var didMessageAction: (()->Void)?
    var minimizeStatus: (()->Void)?
    
    @IBAction func rideAction(_ sender: UIButton) {
        self.didAction?()
    }
    
    @IBAction func callBtnAction(_ sender: UIButton) {
        self.didCallAction?()
    }
    
    @IBAction func messageBtnAction(_ sender: UIButton) {
        self.didMessageAction?()
    }
    
    @IBAction func minimizeBtnAction(_ sender: UIButton) {
        self.minimizeStatus?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.driverRating.tintColor = TpColor().ratingColor
        self.driverRating.selectedStarTint = TpColor().ratingColor
        self.driverRating.unselectedStarTint = TpColor().ratingColor
        self.packageLbl?.text = "Package".localized
        self.startReading?.text = "Start Reading".localized
    }
    
}
