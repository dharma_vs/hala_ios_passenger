//
//  MapVC.swift
//  HalaMobility
//
//  Created by ADMIN on 07/05/22.
//

import UIKit
import GoogleMaps
import FAPaginationLayout

class MapVC: BaseViewController , LocationServiceDelegate, GMSMapViewDelegate {
    func tracingLocation(userLocation: CLLocation) {
        currentLocation = userLocation
        if isCurrentLocation == false{
            let userLocationMarker = GMSMarker(position: currentLocation!.coordinate)
            userLocationMarker.icon = UIImage(named: "currentlocationmarker")
            userLocationMarker.map = googleMapview
        }
        let location: CLLocation = userLocation
        isCurrentLocation = true
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: zoomLevel)
        if googleMapview.isHidden {
            googleMapview.isHidden = false
            googleMapview.camera = camera
        }
    }
    func tracingLocationDidFailWithError(error: NSError) {
    }
    var currentLocation: CLLocation? {
        didSet {
        }
    }
    @IBOutlet weak var googleMapview: GMSMapView!
    var markers = [GMSMarker]()

    var isCurrentLocation : Bool = false
    var isVehicle : Bool = false
    var zoomLevel: Float = 16.0
    let mapsHelper = GoogleMapsHelper()
    var vehiclelist: [Vehiclelist]? 
    @IBOutlet weak var lblscan: AppLabel!
    @IBOutlet weak var lblor: AppLabel!
    @IBOutlet weak var lblclickthe: AppLabel!
    var stationlist: [Station]?
    var vehicleDet : [VehicleDetail]?
    @IBOutlet weak var statusContentView: UIStackView!

    var getStation: [Station]?
    
    //vehicle list
    @IBOutlet weak var viewVehiclelist: VehicleListView!
    @IBOutlet weak var collVehicle: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        print(getStation ?? "")
        googleMapview.settings.myLocationButton = false
        googleMapview.isMyLocationEnabled = false
        googleMapview.delegate = self
        self.setCurrentLocationToCameraPosition()
        self.assignText()
        var arrdistance =  [Int]()
        for index in 0..<(stationlist?.count ?? 0) {
            self.addmarker(latitude: stationlist?[index].latitude ?? 0.0, longitude: stationlist?[index].longitude ?? 0.0, title: stationlist?[index].name ?? "",vehiclecount: stationlist?[index].vehicle_count ?? 0)
            //distance between current location to station
            let markerLocation = CLLocation(latitude: stationlist?[index].latitude ?? 0.0, longitude: stationlist?[index].longitude ?? 0.0)
            GoogleMapsHelper().calculateDistanceDuration(markerLocation.coordinate, toLocation: GlobalValues.sharedInstance.currentLocation.coordinate) { [weak self] (distanceDuration) in
                DispatchQueue.main.async { [weak self] in
                    arrdistance.append(Int(distanceDuration?.distance?.value ?? 0))
                    print(distanceDuration?.distance?.value ?? 0)
                
                if (self?.stationlist!.count)! - 1 == index{
                    //nearest station
                    let sorted = arrdistance.enumerated().sorted(by: {$0.element > $1.element})
                    let justIndices = sorted.map{$0.offset}
                    let nearestLocation = CLLocation(latitude: self?.stationlist?[justIndices.last ?? 0].latitude ?? 0.0, longitude: self?.stationlist?[justIndices.last ?? 0].longitude ?? 0.0)
                    self?.googleMapview.drawPolygon(from: nearestLocation.coordinate, to: GlobalValues.sharedInstance.currentLocation.coordinate, strokeWidth: 1.0, mode: "walking", onCompletion: { (polyline) -> (Void) in
                        let polyLine = polyline()
                        DispatchQueue.main.async { [weak self] in
                            polyLine.map = self?.googleMapview
                            polyLine.strokeWidth = 1
                            polyLine.strokeColor = UIColor.colorAccent
                        }
                    })
                }
            }
            }
        }


        collVehicle.register(UINib(nibName: "VehicleCell", bundle: Bundle.main), forCellWithReuseIdentifier: "VehicleCell")
        collVehicle.contentInset = UIEdgeInsets.init(top: 0, left: 35, bottom: 0, right: 35)
        (collVehicle.collectionViewLayout as! FAPaginationLayout).scrollDirection = .horizontal
        (collVehicle.collectionViewLayout as! FAPaginationLayout).minimumLineSpacing = 20.0
        //viewContent.addSubview(viewVehiclelist)
        self.insertView(self.viewVehiclelist)
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.removepopup(sender:)))
        self.statusContentView.addGestureRecognizer(gesture)
    }
    func insertView(_ view: UIView) {
        if !UserDefaults.companyLogin {
            if !self.statusContentView.arrangedSubviews.contains(view) {
                for arrangedSubView in self.statusContentView.arrangedSubviews {
                    arrangedSubView.removeFromSuperview()
                }
                self.statusContentView.insertArrangedSubview(view, at: 0)
            }
        }
    }
    func assignText(){
        lblscan.text = "Scanto".localized
        lblor.text = "Or".localized
        lblclickthe.addLeading(image:UIImage(named: "ic_information_16x16")!, text: "Clicktheparking".localized)
    }
    override func viewDidAppear(_ animated: Bool) {
        LocationManager.shared.delegate = self
    }
    
    func setCurrentLocationToCameraPosition() {
        mapsHelper.getCurrentLocation { [weak self] (location) in
            DispatchQueue.main.async { [weak self] in
                let camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 17.0)
                self?.googleMapview?.animate(to: camera)
            }
        }
    }
    override func currentLocationBtnPressed(_ sender: Any) {
        self.setCurrentLocationToCameraPosition()
    }
//    @IBAction func moveCurrentLocation(_ sender: Any) {
//        self.setCurrentLocationToCameraPosition()
//    }
    @IBAction func btnclkScan() {
        let walletVC = QRreaderVC.initFromStoryBoard(.Dashboard)
        self.navigationController?.pushViewController(walletVC, animated: true)
    }
    func addmarker(latitude: Double, longitude: Double, title: String, vehiclecount: Int = 0) {
        
        let label = UILabel(frame: CGRect(x: 35, y: 5, width: 30, height: 30))
        label.textAlignment = .center
        label.backgroundColor = .white
        label.textColor = .black
        label.font = UIFont.myBoldSystemFont(ofSize: 15)
        label.layer.cornerRadius = 15
        label.layer.masksToBounds = true
        label.text = "\(vehiclecount)"
        
        let marker = GMSMarker()
        let markerImage = UIImage(named: "img_marker_parking_slot_group")!.withRenderingMode(.alwaysOriginal)
        //creating a marker view
        let markerView = UIImageView(image: markerImage)
        markerView.frame = CGRect(x: 0.0, y: markerView.center.y, width: 68, height: 49)
        //changing the tint color of the image
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        marker.iconView = markerView
        marker.iconView!.addSubview(label)
        marker.title = "Parking-slot".localized
        marker.snippet = title
        markers.append(marker)
        marker.map = googleMapview
        
}
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        // do something
        marker.title = "Parking-slot".localized
        marker.snippet = title
        print("click marker")
        if let index = markers.firstIndex(of: marker) {
            StationID = (stationlist?[index].id)!
            CategoryID = vehiclelist?[index].categoryID ?? 0
            self.vehiclelistAPI()
        }
        isVehicle = false
        return true
    }
    @objc func removepopup(sender : UITapGestureRecognizer) {
        statusContentView.isHidden = true
        for arrangedSubView in self.statusContentView.arrangedSubviews {
            arrangedSubView.removeFromSuperview()
        }
    }
    func vehiclelistAPI(){
        ServiceManager().vehiclelistapiRequest( completion: { [weak self] (inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
              let response = try inner()
                self?.vehiclelist = response?.data
                if self?.vehiclelist !=  nil && self?.vehiclelist?.count != 0{
                    self?.statusContentView.isHidden = false
                    self?.insertView((self?.viewVehiclelist!)!)
                    self?.collVehicle.reloadData()
                }else{
                    self?.showAlertWithMessage("UNAVAILABLE VEHICLE".localized, phrase1: "Ok".localized, action: UIAlertAction.Style.default, completion: { (style) in
                        if style == .default {
                            
                        }
                    })
                }
            }catch{}
        })
    }
}
@available(iOS 14.0, *)
extension MapVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x > 0 && scrollView.superview?.superview?.superview?.superview as? VehicleCell != nil{
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2
        let cell:VehicleCell = (scrollView.superview?.superview?.superview?.superview as? VehicleCell)!
        cell.PageControl.currentPage = Int(offSet + horizontalCenter) / Int(width)
        }
        //PageControl.currentPage = Int(offSet + horizontalCenter) / Int(width)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return isVehicle ? 1 : self.vehiclelist?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: VehicleCell = collectionView.dequeueCell(indexPath)
        cell.loadData(values: (vehiclelist?[indexPath.row])!, det: (vehicleDet?.first)!)
        cell.btnDirection.tag = indexPath.row
        cell.btnBooknow.tag = indexPath.row
        cell.scrollImage.delegate = self
        cell.didRequestToNavigatetoBrowser = { [weak self] option in
            let index = option
            self?.openGoogleMap((self?.vehiclelist?[index].deviceLatitude)!, daddr: (self?.vehiclelist?[index].deviceLongitude)!,name: self?.vehiclelist?[index].displayName ?? "")
            //self?.mapsHelper.openTrackerInBrowser(toLat: (self?.vehiclelist?[index].deviceLatitude)!, toLong: (self?.vehiclelist?[index].deviceLongitude)!)
        }
        cell.didrequestbooknow = { [weak self] intIndex in
            if CacheManager.userInfo?.digio_reference == ""{
                self?.showAlertWithMessage("PleaseProceedKYCVerification".localized, phrase1: "Ok".localized, phrase2: "Cancel".localized, action: UIAlertAction.Style.default, UIAlertAction.Style.cancel, completion: { (style) in
                    if style == .default {
                        let objprofile = KYCVC.initFromStoryBoard(.Profile)
                        self?.navigationController?.pushViewController(objprofile, animated: true)
                    }
                })
            }else  {
                let tempvehicledet = (self?.vehiclelist![intIndex])!
                let battery:Double = (tempvehicledet.batteryVoltage.calculateBatterpercentage(list: tempvehicledet))/100
                let vehicleLocation = CLLocation(latitude: tempvehicledet.deviceLatitude?.doubleValue ?? 0.0, longitude: tempvehicledet.deviceLongitude?.doubleValue ?? 0.0)
                let humanLocation = CLLocation(latitude: GlobalValues.sharedInstance.currentLocation.coordinate.latitude, longitude: GlobalValues.sharedInstance.currentLocation.coordinate.longitude)

                //let vehicleLocation = CLLocationCoordinate2D(latitude: tempvehicledet.deviceLatitude.doubleValue ?? 0, longitude: tempvehicledet.deviceLongitude.doubleValue ?? 0)
                //let humanLocation = CLLocationCoordinate2D(latitude: GlobalValues.sharedInstance.currentLocation.coordinate.latitude , longitude: GlobalValues.sharedInstance.currentLocation.coordinate.longitude)
                let distanceInMeters = vehicleLocation.distance(from: humanLocation) // result is in meters
                /*if GlobalValues.sharedInstance.minimumBattery > battery { //temp**
                    self?.view?.makeToast("LowBattery".localized)

                }else*/ if GlobalValues.sharedInstance.unlockDistance > distanceInMeters {
                    self?.view?.makeToast("Miniumdistance".localized)
                }
                else{
                    let unlockvc = UnlockVC.initFromStoryBoard(.Dashboard)
                    unlockvc.vehicle = tempvehicledet
                    self?.navigationController?.pushViewController(unlockvc, animated: true)
                }
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellSize = CGSize()
        cellSize = collectionView.bounds.size
        cellSize.width -= collectionView.contentInset.left
        cellSize.width -= collectionView.contentInset.right
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
extension Double{
    func calculateBatterpercentage(list: Vehiclelist? = nil, list1:VehicleInfo1? = nil)-> Double{
        var battery : Double = 0
        let maxVolt : Double = list == nil ? list1?.batteryMaxVoltage ?? 0.0 : list?.batteryMaxVoltage ?? 0.0
        let minVolt : Double = list == nil ? list1?.batteryMinVoltage ?? 0.0 : list?.batteryMinVoltage ?? 0.0
        let batteryVolt : Double = list == nil ? list1?.batteryVoltage ?? 0.0 : list?.batteryVoltage ?? 0.0
        if (maxVolt == 0 && minVolt == 0 && batteryVolt == 0){
            battery = 0
        }else if (batteryVolt < minVolt){
            battery = 0
        }else if (batteryVolt == maxVolt || batteryVolt > maxVolt) {
            battery = 100
        }
        else{
            battery = (((100 / (maxVolt - minVolt)) * (batteryVolt - minVolt)))
        }
        return battery
        //lblBattery.addLeading(image:UIImage(named: "img_draw_battery")!, text: String(format: "%.0f", battery) + "%"
//)
    }
}
extension Date {
    static func differencebetweendates(recent: Date, previous: Date) -> (Int?) {
//        let day = Calendar.current.dateComponents([.day], from: previous, to: recent).day
//        let hour = Calendar.current.dateComponents([.hour], from: previous, to: recent).hour
//        let minute = Calendar.current.dateComponents([.minute], from: previous, to: recent).minute
        let second = Calendar.current.dateComponents([.second], from: previous, to: recent).second
        //return ( day: day, hour: hour, minute: minute, second: second)
        return ( second)
    }

}
extension Int {
    func secondsToTime(isDay:Bool = false) -> String {
        let (d,h,m,s) = (self / 86400, (self / 3600) % 24, (self % 3600) / 60, (self % 3600) % 60)
        var d_string = d < 10 ? "0\(d)" : "\(d)"
        let h_string = h < 10 ? "0\(h)" : "\(h)"
        let m_string =  m < 10 ? "0\(m)" : "\(m)"
        let s_string =  s < 10 ? "0\(s)" : "\(s)"
        if isDay{
            //            if d_string != "00"{
            //                d_string = d < 10 ? "0\(d)" : "\(d)" + "\(d > 1 ? "\(d) Day" : "\(d) Days")"
            //            }
            return "\(d_string) Days \(h_string) Hours \(m_string) Minutes"
        }
        return "\(d_string):\(h_string):\(m_string):\(s_string)"
    }
}
