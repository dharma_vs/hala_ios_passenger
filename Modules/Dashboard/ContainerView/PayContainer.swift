//
//  PayContainer.swift
//  HalaMobility
//
//  Created by ADMIN on 03/08/22.
//

import UIKit

class PayContainer: UIViewController {
    @IBOutlet weak var lblStartdate: AppLabel!
    @IBOutlet weak var lblStartdateans: AppLabel!
    @IBOutlet weak var lblEndtdate: AppLabel!
    @IBOutlet weak var lblEnddateans: AppLabel!
    @IBOutlet weak var lblTotal: AppLabel!
    @IBOutlet weak var lblTotalans: AppLabel!
    @IBOutlet weak var lblPrice: AppLabel!
    @IBOutlet weak var lblpayable: AppLabel!
    @IBOutlet weak var btnPay: AppButton!
    var fetchdetail: Rental1?
    override func viewDidLoad() {
        super.viewDidLoad()
        print("***  PayContainer  ***")
    }
    func setupValues(){
        lblStartdate.text = "startdate".localized
        lblEndtdate.text = "enddate".localized
        lblStartdateans.text = fetchdetail?.startEstimatedDateTime?.stringValue.UTCToLocal("yyyy-MM-dd HH:mm:ss", formatTo: "yyyy-MM-dd HH:mm a")
        lblEnddateans.text = fetchdetail?.endEstimatedDateTime?.stringValue.UTCToLocal("yyyy-MM-dd HH:mm:ss", formatTo: "yyyy-MM-dd HH:mm a")
        let farevalues = fetchdetail?.fareBreakup as? FareBreakupClass
        lblPrice.text = Extension.updateCurrecyRate(fare: "\(String(farevalues?.totalFare?.stringValue ?? "0"))").currencyValue
        switch GlobalValues.sharedInstance.currentRentalStatus{
        case 8 ://END
            self.btnPay?.setTitle("pay".localized, for: .normal)
            lblTotal.text = "totalfare".localized
            lblpayable.text = "payableamount".localized
            lblTotalans.text = lblPrice.text
            break
        case 9 ://FEEDBACK
            self.btnPay?.setTitle("Submit".localized, for: .normal)
            lblpayable.text = "finalfare".localized
            lblTotal.isHidden = true
            lblTotalans.isHidden = true
            break
        default:
            break
        }
    }
    
    @IBAction func btnclkPay() {
        if GlobalValues.sharedInstance.currentRentalStatus == 8 { // payment
            let paymentVC = ProductPaymentViewController.initFromStoryBoard(.Payment)
            paymentVC.amountToPay =  100.0
            self.navigationController?.pushViewController(paymentVC, animated: true)
        }else if GlobalValues.sharedInstance.currentRentalStatus == 9 { // Feedback
        }
    }
    @IBAction func btnclkInfo() {
        let fareobj = FareDetailVC.initFromStoryBoard(.Custom)
        fareobj.fetchdetail = fetchdetail
        fareobj.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(fareobj, animated: true)
    }
}
