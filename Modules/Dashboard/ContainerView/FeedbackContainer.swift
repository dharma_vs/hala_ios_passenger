//
//  FeedbackContainer.swift
//  HalaMobility
//
//  Created by ADMIN on 17/08/22.
//

import UIKit

class FeedbackContainer: UIViewController {
    @IBOutlet weak var lblStartdate: AppLabel!
    @IBOutlet weak var lblStartdateans: AppLabel!
    @IBOutlet weak var lblEndtdate: AppLabel!
    @IBOutlet weak var lblEnddateans: AppLabel!
    @IBOutlet weak var lblPrice: AppLabel!
    @IBOutlet weak var lblpayable: AppLabel!
    @IBOutlet weak var generalRating: FloatRatingView!
     @IBOutlet weak var txtviewFeedback: UITextView!
    @IBOutlet weak var btnPay: AppButton!
    var placeholderLabel : UILabel!
    var fetchdetail: Rental1?
    override func viewDidLoad() {
        super.viewDidLoad()
        generalRating.editable = true
        placeholderLabel = UILabel()
        placeholderLabel.text = "Enter your feedback".localized
        placeholderLabel.font = UIFont.systemFont(ofSize: 12)
        placeholderLabel.sizeToFit()
        txtviewFeedback.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (txtviewFeedback.font?.pointSize)! / 2)
        placeholderLabel.textColor = .lightGray
        placeholderLabel.isHidden = !txtviewFeedback.text.isEmpty
        print("***  FeedbackContainer  ***")
    }
    func setupValues(){
        lblStartdate.text = "startdate".localized
        lblEndtdate.text = "enddate".localized
        lblStartdateans.text = fetchdetail?.startEstimatedDateTime?.stringValue.UTCToLocal("yyyy-MM-dd HH:mm:ss", formatTo: "yyyy-MM-dd HH:mm a")
        lblEnddateans.text = fetchdetail?.endEstimatedDateTime?.stringValue.UTCToLocal("yyyy-MM-dd HH:mm:ss", formatTo: "yyyy-MM-dd HH:mm a")
        let farevalues = fetchdetail?.fareBreakup as? FareBreakupClass
        lblPrice.text = Extension.updateCurrecyRate(fare: "\(String(farevalues?.totalFare?.stringValue ?? "0"))").currencyValue
        self.btnPay?.setTitle("Submit".localized, for: .normal)
        lblpayable.text = "finalfare".localized
    }
    
    @IBAction func btnclkPay() {
        if generalRating.rating == 0{
            self.view.makeToast("Please give your rating".localized)
        }else{
            
        }
    }
    @IBAction func btnclkInfo() {
        let fareobj = FareDetailVC.initFromStoryBoard(.Custom)
        fareobj.fetchdetail = fetchdetail
        fareobj.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(fareobj, animated: true)
    }
}
extension FeedbackContainer : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
}
