//
//  PauseContainer.swift
//  HalaMobility
//
//  Created by ADMIN on 06/08/22.
//

import UIKit
protocol  pauseContainerdelegate:AnyObject {
    func clickResumeRide()
}
class PauseContainer: UIViewController {
    @IBOutlet weak var lblWaiting: AppLabel!
    @IBOutlet weak var lblTimer: AppLabel!
    @IBOutlet weak var btnResume: AppButton!
    var pausetimer:Int? = 0
    var fetchdetail: Rental1?
    weak var delegate: pauseContainerdelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func setupValues(){
        lblWaiting.text = "Waitingtimer".localized
        self.btnResume?.setTitle("resumeride".localized, for: .normal)
        //let endDate = fetchdetail?.ridePauseTimes?.last?.startDate?.stringValue.toDateTime()
        let startDate: String = Date().dateFromFormater("yyyy-MM-dd HH:mm:ss") ?? ""
        let endDate = fetchdetail?.ridePauseTimes?.last?.startDate?.stringValue.UTCToLocal1("yyyy-MM-dd HH:mm:ss", formatTo: "yyyy-MM-dd HH:mm:ss") ?? ""
        //let  endDate = fetchdetail?.ridePauseTimes?.last?.endDate?.stringValue.toDateTime()
        pausetimer = Date.differencebetweendates(recent: startDate.toDateTime() as Date, previous: endDate.toDateTime() as Date)
    }
    func timerupdate(){
        pausetimer = pausetimer! + 1
        let pausetimerValues = pausetimer?.secondsToTime()
        lblTimer.text = pausetimerValues
    }
    
    @IBAction func btnclkResumeRide() {
        if GlobalValues.sharedInstance.currentRentalStatus == 14 { // resumeride
            pausetimer = 0
            delegate?.clickResumeRide()
        }
    }
}
