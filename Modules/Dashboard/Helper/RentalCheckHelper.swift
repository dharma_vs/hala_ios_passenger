//
//  RentalCheckHelper.swift
//  HalaMobility
//
//  Created by ADMIN on 01/08/22.
//

import Foundation

class RentalCheckHelper {
    
    private weak var timer : Timer?
    
    private var completion: ((CustomError?,Rental1?)->Void)?
    
    func startListening(on completion : @escaping ((CustomError?,Rental1?)->Void)) {
        self.completion = completion
        if self.timer == nil {
            self.timerAction()
            self.timer = Timer.scheduledTimer(timeInterval: GlobalValues.sharedInstance.requestCheckInterval, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        }
    }
    
    @objc func timerAction(){
        ServiceManager().fetchapiRequest{ [weak self] (inner) -> (Void) in
            do {
                let rentalList = try inner()
                self?.completion?(nil,rentalList)
            } catch {
                self?.completion?(CustomError.init(description: error.localizedDescription, code: 1001), nil)
            }
        }
    }
    
    func stopListening() {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    func resumeListening() {
        if self.timer?.isValid == false || self.timer == nil {
            self.timer?.invalidate()
            self.timer = nil
            self.timerAction()
            self.timer = Timer.scheduledTimer(timeInterval: requestCheckInterval, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        }
    }
    
    deinit {
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
}
