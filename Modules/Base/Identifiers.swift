//
//  Identifiers.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import Foundation

extension String {
    static let GotoLoginFromLanguage = "GotoLoginFromLanguage"
    static let GotoOTPFromMobileNumber = "GotoOTPFromMobileNumber"
    static let GotoSignUpFromOTP = "GotoSignUpFromOTP"
    static let GotoSetPasswordFromOTP = "GotoSetPasswordFromOTP"
    static let GotoForgetPasswordFromSetPassword = "GotoForgetPasswordFromSetPassword"
    static let GotoResetPasswordFromOTP = "GotoResetPasswordFromOTP"
    static let GotoRegisterFromMobileNumber = "GotoRegisterFromMobileNumber"
    static let GotoChangeLanguageFromProfile = "GotoChangeLanguageFromProfile"
    static let GotoProfileFromSettings = "GotoProfileFromSettings"
    static let GoToChangeEmailFromProfile = "GoToChangeEmailFromProfile"
    static let GotoPasswordFromProfile = "GotoPasswordFromProfile"
    static let GoUpdateNumberSegue = "GoUpdateNumberSegue"
    static let GoUpdateNameSegue = "GoUpdateNameSegue"
    static let GotoOTPFromSignupViewController = "GotoOTPFromSignupViewController"
    static let GotoKYCSegue = "GotoKYCSegue"

}
