//
//  BaseDataServiceEndPoint.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/03/22.
//

import Foundation

enum BaseDataServiceEndPoint: AddressableEndPoint {
    
    case lastUpdated, datetime

    var endPointInfo: DataServiceEndpointInfo {
        switch self {
        case .lastUpdated:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort , path: componentMasterPathSuffix+"/last-updated", method: .GET, authType: .none)
        case .datetime:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort , path: componentMasterPathSuffix+"/datetime", method: .GET, authType: .none)
        }
    }
}

extension ServiceManager {
    func ļastUpdated(_ info: LastUpdatedReq = LastUpdatedReq(), completion: @escaping (_ inner: () throws ->  LastUpdate) -> Void) {
        self.request(endPoint: BaseDataServiceEndPoint.lastUpdated, parameters: info.JSONRepresentation) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response = data.getDecodedObject(from: GeneralResponse<LastUpdate>.self)
                if let data = response?.data, response?.status == true {
                    completion({return data})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func datetime(_ info: DateTimeReq = DateTimeReq(), completion: @escaping (_ inner: () throws ->  DateTime) -> Void) {
        self.request(endPoint: BaseDataServiceEndPoint.datetime, parameters: info.JSONRepresentation) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response = data.getDecodedObject(from: GeneralResponse<DateTime>.self)
                if let data = response?.data, response?.status == true {
                    completion({return data})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}

struct LastUpdatedReq: JSONSerializable {
    var type: String! = "Customer"
}

struct LastUpdate: JSONSerializable {
    var settingUpdate, profileUpdate, countryUpdate, cancelReasonUpdate: CustomType?
    var issueUpdate: CustomType?

    enum CodingKeys: String, CodingKey {
        case settingUpdate = "setting_update"
        case profileUpdate = "profile_update"
        case countryUpdate = "country_update"
        case cancelReasonUpdate = "cancel_reason_update"
        case issueUpdate = "issue_update"
    }
}

struct DateTimeReq: JSONSerializable {
    
}

struct DateTime: JSONSerializable {
    var dateTime: CustomType?
    var workingSlot: [CustomType]?
    
    enum CodingKeys: String, CodingKey {
        case dateTime = "datetime"
        case workingSlot = "working_slot"
    }
}
