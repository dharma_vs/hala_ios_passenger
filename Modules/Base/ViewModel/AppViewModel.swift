//
//  AppViewModel.swift
//  Base
//
//  Created by Admin on 17/01/22.
//

import Foundation
import UIKit

protocol AppViewModel {
    var controller: myType! { get set }
    init(controller: myType)
    associatedtype myType
}
