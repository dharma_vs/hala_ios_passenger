//
//  Constant.swift
//  HalaMobility
//
//  Created by Admin on 21/02/22.
//

import Foundation

var CVV = ""
extension String {
    
    static let DefaultCountryCode = "+91"
    static let Enter_Phone_Number_Email_Address = "Enter Phone Number / Email Address"
    
    // Register
    static let Enter_valid_phone_number_select_correct_country_code = "Enter valid phone number & select correct country code"
    static let Please_enter_valid_phone_number = "Please enter valid phone number"
    
    // Forgot Password
    static let Enter_valid_email_address_or_phone_number = "Enter valid email address or phone number"
    
    // Menu
    static let Home = "Home"
    static let Notifications = "Notifications"
    static let YourTrips = "Your Trips"
    static let BookRide = "Book Ride"
    static let RentARide = "Rent a Ride"
    static let MyWallet = "My Wallet"
    static let Feedback = "Feedback"
    static let About = "About"
    static let InviteFriends = "Invite friends"
    static let Logout = "Logout"
    
    // Address Search
    static let selectLocation = "Select Location"
    
    // Invite Friends
    static let when_your_friend_signs_up_with_your_referral_code_you_both_get_3_coupons = "when your friend signs up with your referral code, you both get 3 coupons"

    static let Work = "Work"
    static let Payment_Method = "Payment Method"
    
    //  Edit Profile
    static let Enter_valid_email_address = "Enter valid email address"
}
import UIKit
extension String {
    static let LogoutNotification = "LogoutNotification"
    static let systemVersion = UIDevice.current.systemVersion
    static let bundleIdentifier =  Bundle.main.bundleIdentifier
    static let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
}

struct Constant {
    struct BaseComponents {
        static let kEmptyString = ""
        static let kNumZero = 0
        static let kNumHoundred = 100
    }
    struct Introduction {
        static let kIntroductionCellID = "IntroductionCell"
    }
    struct BuyProduct{
        static let kProductDetailCellID = "ProductDetailCollectionViewCell"
    }
    struct PaymentType{
        static let kPaymentTypeCellID = "PaymentTypeCollectionViewCell"
        static let kPaymentBankCellID = "BankSelectionTableViewCell"
        static let kPaymentCardListCellID = "PaymentCardListCell"
    }
}

class Extension: NSObject {
    class func updateCurrecyRate(fare : String) -> String {
        if CacheManager.sharedInstance.getObjectForKey(.currencyRate) == nil{
             return fare
        }
        if "\(CacheManager.sharedInstance.getObjectForKey(.currencyRate)as! String)" == "" {
            return fare
        }
        let rateValue = "\((Double("\(CacheManager.sharedInstance.getObjectForKey(.currencyRate) ?? 0)")) ?? 0)"
        
        if rateValue == "1.0" {
            return fare
        }
        return "\((String(format:"%.2f", ((Double(fare) ?? 0) * (Double(rateValue) ?? 0)))))"
    }
}
