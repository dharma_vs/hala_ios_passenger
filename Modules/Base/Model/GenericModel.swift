//
//  GenericModel.swift
//  Base
//
//  Created by Admin on 17/01/22.
//

import Foundation

class GeneralResponseSingle<T: Decodable>: Decodable {
    var status: StatusInfo?
    var message, detail: String?
    var data : T?
    var isLoading: Bool? = false

    init() {
        self.data = nil
        self.message = nil
        self.detail = nil
        self.status = nil
        self.isLoading = false
    }
}

class GeneralResponseArray<T: Decodable>: Decodable {
    var status: StatusInfo?
    var message, detail: String?
    var data : [T]?
    var isLoading: Bool? = false
    var page, pages: CustomType?

    init() {
        self.data = []
        self.message = nil
        self.detail = nil
        self.status = nil
        self.isLoading = false
        self.page = nil
        self.pages = nil
    }
}

var device_Id: String  {
    if let deviceId = CacheManager.sharedInstance.getObjectInApplicationForKey(.DeviceId) as? String {
        return deviceId
    } else {
        let deviceId = UIDevice.current.identifierForVendor!.uuidString
        CacheManager.sharedInstance.setObjectInApplication(deviceId, key: .DeviceId)
        return deviceId
    }
}
let deviceType = "ios"
import UIKit
