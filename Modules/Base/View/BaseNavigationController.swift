//
//  BaseNavigationController.swift
//  HalaMobility
//
//  Created by Admin on 25/03/22.
//

import UIKit

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        let height = CGFloat(72)
//        navigationBar.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: height)
    }

}


extension UINavigationBar {

    open override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 55)
    }

}
