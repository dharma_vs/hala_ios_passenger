//
//  CalendarViewController.swift
//  TaxiPickup
//
//  Created by SELLADURAI on 3/24/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

class CalendarViewController: BaseViewController {
    
    var monthDetail: MonthDetail = Date.monthDetailFrom(2) {
        didSet {
            var days = [Int]()
            var previousMonthIndices = Set([Int]())
            let previousMonthLastDay = self.monthDetail.previousMonthLastDay!
            if self.monthDetail.weekDay! > 1 {
                for index in -(monthDetail.weekDay! - 1)...(-1) {
                    days.append(previousMonthLastDay+index)
                    previousMonthIndices.insert(days.count-1)
                }
                self.previousMonthIndices = previousMonthIndices
            }
            for index in 1...self.monthDetail.currentMonthDaysCount! {
                days.append(index)
            }
            self.monthDays = days
        }
    }
    var year: Int = Date().currentYear() {
        didSet {
            self.monthDetail = Date.monthDetailFrom(self.month, year: self.year)
        }
    }
    var month: Int = Date().currentMonth() {
        didSet {
            self.monthDetail = Date.monthDetailFrom(self.month, year: self.year)
            self.monthYearLbl.text = Date.dateFrom(month: self.month, day: 1, year: self.year)?.dateFromFormater("MMM yyyy")?.uppercased()
        }
    }
    var day: Int = Date().currentDay() {
        didSet {
            self.weekDayMonthDayYearLbl.text = "Date".localized + ": " + (Date.dateFrom(month: self.month, day: self.day, year: self.year)?.dateFromFormater("EEE, MMM dd yyyy") ?? "")
        }
    }
    var minute: Int = Date().currentMinutes() {
        didSet {
            self.pickupTimeLbl?.text = "Pick Up Time".localized + " - " + (Date.dateFrom(month: self.month, day: self.day, year: self.year, minute: self.minute)?.dateFromFormater("hh:mm a") ?? "")
            self.sliderTimeLbl?.text = (Date.dateFrom(month: self.month, day: self.day, year: self.year, minute: self.minute)?.dateFromFormater("hh:mm a") ?? "")
            self.sliderInfoView?.isHidden = false
            let image = self.sliderInfoView?.asImage()
            self.timeSlider.setThumbImage(image, for: UIControl.State())
            self.timeSlider.setThumbImage(image, for: UIControl.State.highlighted)
            self.sliderInfoView?.isHidden = true
        }
    }
    var monthDays: [Int] = [] {
        didSet {
            self.monthCollectionView?.reloadData()
        }
    }
    
    var previousMonthIndices = Set([Int]())
    
    @IBOutlet weak var monthCollectionView: UICollectionView!
    @IBOutlet weak var monthYearLbl: UILabel!
    @IBOutlet weak var weekDayMonthDayYearLbl: UILabel!
    
    @IBOutlet weak var pickupTimeLbl: UILabel!
    
    @IBOutlet weak var sliderTimeLbl: UILabel!
    
    @IBOutlet weak var errorLbl: UILabel!
    
    @IBOutlet weak var errorView: UIView!
    
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet weak var sliderTimeCenterConstraint: NSLayoutConstraint!
    
    @IBAction func loadPreviousMonth(_ sender: Any) {
        if self.month > 1 {
            self.month -= 1
        } else {
            self.year -= 1
            self.month = 12
        }
    }
    
    @IBAction func loadNextMonth(_ sender: Any) {
        if self.month < 12 {
            self.month += 1
        } else {
            self.year += 1
            self.month = 1
        }
    }
    
    @IBOutlet weak var weekDaySunLbl: UILabel!
    @IBOutlet weak var weekDayMonLbl: UILabel!
    @IBOutlet weak var weekDayTueLbl: UILabel!
    @IBOutlet weak var weekDayWedLbl: UILabel!
    @IBOutlet weak var weekDayThuLbl: UILabel!
    @IBOutlet weak var weekDayFriLbl: UILabel!
    @IBOutlet weak var weekDaySatLbl: UILabel!
    
    @IBOutlet weak var sliderInfoView: UIView!
    
    @IBOutlet weak var timeSlider: UISlider!
    
    @IBAction func timeSliderValueChanged(_ sender: UISlider) {
        let value = Int(sender.value)
        self.minute = value * 10
    }
    
    var didSelectDateTime: ((Date?, String?)->Void)?
    
    var selectedDate: Date?
    
    var selectedTime: String?
    
    var isForRentalRide = false
    
    @IBAction func confirmSelection(_ sender: Any) {
        let currentMinutes = Date().currentMinutes()
        let min = Date().currentHour() * 60
        let minDuration = isForRentalRide ? CacheManager.settings?.ride_rent_minute_duration: CacheManager.settings?.ride_later_minimum_start_in_minute
        let rideLaterMininumDuration = (Int(minDuration ?? "10") ?? 10)
        guard let expectedDateTime = Date.dateFrom(month: Date().currentMonth(), day: Date().currentDay(), year: Date().currentYear(), minute: min + currentMinutes + rideLaterMininumDuration), let selectedDateTime = Date.dateFrom(month: self.month, day: self.day, year: self.year, minute: self.minute), expectedDateTime <= selectedDateTime  else {
            self.errorLbl.text = "* Please Select Valid Date Time".localized
            return
        }
        self.selectedTime = selectedDateTime.dateFromFormater("hh:mm a")
        self.didSelectDateTime?(selectedDateTime, self.selectedTime)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.errorLbl.text = nil
        timeSlider.maximumValue = 144
        self.year = Date().currentYear()
        self.month = Date().currentMonth()
        self.day = Date().currentDay()
        let currentMinutes = Date().currentMinutes()
        let min = Date().currentHour() * 60
        let rideLaterMininumDuration = (Int(CacheManager.settings?.ride_later_minimum_start_in_minute ?? "10") ?? 10)
        self.minute = min + (currentMinutes - (currentMinutes%rideLaterMininumDuration) + rideLaterMininumDuration)
        self.timeSlider.value = Float(self.minute/10)
        self.selectedDate = Date.dateFrom(month: self.month, day: self.day, year: self.year)
        self.monthYearLbl.text = selectedDate?.dateFromFormater("MMM yyyy")
        let image = self.sliderInfoView.asImage()
        self.timeSlider.setThumbImage(image, for: UIControl.State())
        self.monthCollectionView.reloadData()
        self.confirmBtn?.setTitle("Confirm".localized, for: UIControl.State())
        self.cancelBtn?.setTitle("Cancel".localized, for: UIControl.State())
        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let currentMinutes = Date().currentMinutes()
        let min = Date().currentHour() * 60
        let minDuration = isForRentalRide ? CacheManager.settings?.ride_rent_minute_duration: CacheManager.settings?.ride_later_minimum_start_in_minute
        let rideLaterMininumDuration = (Int(minDuration ?? "10") ?? 10)
        self.minute = min + (currentMinutes - (currentMinutes%rideLaterMininumDuration) + rideLaterMininumDuration)
    }

}

extension CalendarViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.monthDays.count > 35 ? 42 : 35
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MonthCollectionViewCell", for: indexPath) as! MonthCollectionViewCell
        
        var weekStartIndexes = Set([7,14,21,28,35])
        if indexPath.item == 0 && self.monthDays.first == 1{
            weekStartIndexes.insert(0)
        }
        let value = indexPath.item < self.monthDays.count ? self.monthDays[indexPath.item] : 0
        let date = indexPath.item < self.monthDays.count ? "\(self.monthDays[indexPath.item])" : nil
        
        let thisDate = Date.dateFrom(month: self.month, day: value, year: self.year)
        
        let currentDate = Date.dateFrom(month: Date().currentMonth(), day: Date().currentDay(), year: Date().currentYear())
        
        if thisDate! >= currentDate! {
            cell.dateLbl.textColor = weekStartIndexes.contains(indexPath.item) && !previousMonthIndices.contains(indexPath.item) ? UIColor.init(netHex: 0xFFCC00) : previousMonthIndices.contains(indexPath.item) ? .lightGray : .darkGray
        } else {
            cell.dateLbl.textColor = .lightGray
        }
        
        cell.selectionBackground.isHidden = true
        
        cell.currentDateArrow?.isHidden = true
        
        if thisDate == self.selectedDate && !previousMonthIndices.contains(indexPath.item) && self.monthDays.count > indexPath.item {
            cell.selectionBackground.isHidden = false
            cell.dateLbl.textColor = .white
        } else {
            if thisDate! == currentDate! && self.monthDays.count > indexPath.item {
                cell.currentDateArrow?.isHidden = false
                cell.currentDateArrow?.image = #imageLiteral(resourceName: "dwn_arrow").withRenderingMode(.alwaysTemplate)
                cell.currentDateArrow?.tintColor = UIColor.init(hex: 0x017302)
            }
        }
        
        cell.dateLbl.text = date
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = collectionView.bounds.size
        size.width = size.width/7
        size.height = size.height/(self.monthDays.count > 35 ? 6 : 5)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? MonthCollectionViewCell {
            if cell.dateLbl.textColor != .lightGray {
                guard self.monthDays.count > indexPath.item else {
                    return
                }
                let day = self.monthDays[indexPath.item]
                self.selectedDate = Date.dateFrom(month: self.month, day: day, year: self.year)
                self.day = day
                collectionView.reloadData()
            }
        }
    }
}

class MonthCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var selectionBackground: UIView!
    @IBOutlet weak var dateLbl: AppLabel!
    @IBOutlet weak var currentDateArrow: AppImageView!
}

extension UIView {
    
    func asImage() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
}
