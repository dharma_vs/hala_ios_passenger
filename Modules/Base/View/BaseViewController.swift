//
//  BaseViewController.swift
//  Base
//
//  Created by Admin on 02/01/22.
//

import UIKit
import NVActivityIndicatorView
import Toast_Swift
import MessageUI
import DropDown
import AVKit
import MobileCoreServices
import CryptoSwift
import SlideMenuControllerSwift
import GoogleSignIn
import FBSDKLoginKit

class BaseViewController: UIViewController {
        
    var selectedImage: ((UIImage)-> Void)?
    
    override open func indicatorStyle() -> IndicatorStyle {
        return CustomIndicatorStyle()
    }
    
    override open func activityIndicatorView() -> UIView {
        return self.newActivityIndicator
    }
    
    lazy var newActivityIndicator: NVActivityIndicatorView = {
        let indicator = NVActivityIndicatorView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: 40.0, height: 40.0)))
        indicator.type = .ballClipRotate//.ballClipRotateMultiple
        indicator.color = .primaryDark
        return indicator
    }()
    
    override open func startActivityIndicator() {
        super.startActivityIndicator()
        self.newActivityIndicator.startAnimating()
    }
    
    override open func startActivityIndicatorInWindow() {
        super.startActivityIndicatorInWindow()
        self.newActivityIndicator.startAnimating()
    }
    
    var statusBarColor: UIColor {
        return .colorPrimaryDark
    }
    
    @IBInspectable var isNavigationBarHidden: Bool = false
    
    @IBInspectable var navigationType: Int = 0
    
    @IBInspectable var navBarColor: UIColor = .clear
    
    @IBInspectable var navBarTheme: Int = ColorTheme.colorPrimary.rawValue
    
    @IBInspectable var backArrowImage: UIImage = #imageLiteral(resourceName: "img_app_back").withRenderingMode(.alwaysTemplate)
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    open override var prefersStatusBarHidden: Bool {
        return false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        localize()
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init(.SESSION_EXPIRED), object: nil, queue: .main) { [weak self](notification) in
            self?.logout()
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.init(.AppLanguageChangedNotification), object: nil, queue: OperationQueue.main) { [weak self] (notification) in
            self?.localize()
        }
        
        navigationSetup()
    }
    
    func updateStatusBarColor() {
        if #available(iOS 13.0, *) {
            
            let statusBar1 =  UIView()
            statusBar1.frame = (UIApplication.keyWindow?.windowScene?.statusBarManager?.statusBarFrame) ?? CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: UIScreen.main.bounds.width, height: 20))
            statusBar1.backgroundColor = statusBarColor
            
            UIApplication.keyWindow?.addSubview(statusBar1)
            
        } else {
            
            let statusBar1: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            statusBar1.backgroundColor = statusBarColor
        }
    }
    
    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup() {
        self.updateStatusBarColor()
        
        self.navigationController?.isNavigationBarHidden = self.isNavigationBarHidden
        
        self.localize()
        
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
        
        dch_checkDeallocation()
    }
    
    @objc func localize() {
        
    }
    
    @IBAction func openMenu(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func backPressed() {
        if (self.navigationController?.viewControllers.count ?? 0) > 1 {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    deinit {
        print("\(self.description) deinited")
    }

}

extension UIViewController {
    
    func pushViewController(_ viewController: UIViewController, type: AnyClass, animated: Bool = false) {
        if let vc = (self.rootNavigationController?.viewControllers.filter{$0 .isKind(of: type)}.first) {
            self.rootNavigationController?.popToViewController(vc, animated: animated)
        } else {
            self.rootNavigationController?.popToRootViewController(animated: false)
            self.rootNavigationController?.pushViewController(viewController, animated: false)
        }
    }
    
    static var rootNavigation: UINavigationController? {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let slideMenuVC = appDelegate?.window?.rootViewController as? SlideMenuController
        return slideMenuVC?.mainViewController as? UINavigationController
    }
        
    var rootNavigationController: UINavigationController? {
        return slideMenuController()?.mainViewController as? UINavigationController
    }
    
    weak var homeViewController: DashboardViewController? {
        return (rootNavigationController?.viewControllers.filter{$0 is DashboardViewController}.first as? DashboardViewController)
    }
    
    var alertController: AlertController! {
        let alertVC = AlertController.initFromStoryBoard(.Base)
        alertVC.modalPresentationStyle = .overCurrentContext
        return alertVC
    }
    
    func showAlertWithMessage(_ message: String, phrase1: String = "Ok".localized, phrase2: String = "Cancel".localized, skipDismiss: Bool = false,showInput: Bool = false, action: UIAlertAction.Style..., completion: ((UIAlertAction.Style) -> Void)?) {
        let alertVC = self.alertController
        alertVC?.message = message
        alertVC?.skipDismiss = skipDismiss
        alertVC?.canShowTextField = showInput
        if action.contains(.default) {
            alertVC?.addAction(.default, buttonPhrase: phrase1, completion: completion)
        }
        if action.contains(.cancel) {
            alertVC?.addAction(.cancel, buttonPhrase: phrase2, completion: completion)
        }
        rootViewController?.present(alertVC!, animated: true, completion: nil)
    }
    
    func email(to mail: [String]) {
        if MFMailComposeViewController.canSendMail() {
            let mcvc = MFMailComposeViewController()
            mcvc.mailComposeDelegate = self as! BaseViewController
            mcvc.setToRecipients(mail)
            present(mcvc, animated: true)
        }
    }
    
    func openURL(_ urlStr: String) {
        if let url = URL(string: urlStr), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            self.view.makeToast("Not able to open the link".localized)
        }
    }
}

extension BaseViewController: MFMailComposeViewControllerDelegate {
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension UIViewController {
    func findChild<T>() -> T? {
        for child in children {
            if child is T {
                return child as? T
            } else if child.children.count > 0 {
                return child.findChild()
            }
        }
        return self.children.filter{$0 is T}.first as? T
    }
}

extension String {
    func width(_ font: UIFont = .systemFont(ofSize: 15)) -> CGSize? {
        let lbl = UILabel()
        lbl.font = font
        if let font = lbl.font {
            let fontAttributes = [NSAttributedString.Key.font: font]
            lbl.numberOfLines = 1
            return (self as NSString?)?.size(withAttributes: fontAttributes)
        }
        return nil
    }
}


extension UIViewController
{
    class func initFromStoryBoard(_ name: String = .Main) -> Self
    {
        return instantiateFromStoryboardHelper(name)
    }

    fileprivate class func instantiateFromStoryboardHelper<T>(_ name: String) -> T
    {
        let storyboard = UIStoryboard(name: name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! T
        return controller
    }
}

enum NavigationType: Int, CaseIterable {
    case None = 0
    case Menu
    case MenuWithVIPAndCustomer
    case Back
    case BackWithTitle
    case MenuWithTitle
    case MenuWithBunkCurrentLocation
    case BackWithBunkCurrentLocation
}

extension Int {
    var navigationType: NavigationType {
        switch self {
        case 0:
            return .None
        case 1:
            return .Menu
        case 2:
            return .MenuWithVIPAndCustomer
        case 3:
            return .Back
        case 4:
            return .BackWithTitle
        case 5:
            return .MenuWithTitle
        case 6:
            return .MenuWithBunkCurrentLocation
        case 7:
            return .BackWithBunkCurrentLocation
        default:
            return .None
        }
    }
}

//Navigation Setup
extension BaseViewController {
    func navigationSetup() {
        self.navigationController?.isNavigationBarHidden = self.isNavigationBarHidden
        self.navigationController?.setNavigationBarHidden(self.isNavigationBarHidden, animated: false)
        self.navigationController?.navigationBar.barTintColor = navBarColor
        self.navigationController?.navigationBar.backgroundColor = navBarColor
        self.navigationController?.navigationBar.setBackgroundImage(navBarColor.image, for: .default)
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = navBarColor
        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationItem.hidesBackButton = true
        switch navigationType.navigationType {

        case .Menu:
            self.navigationItem.leftBarButtonItems = [{
                let btn = UIBarButtonItem(customView: {
                    let btn = UIButton()
                    btn.setImage(#imageLiteral(resourceName: "img_menu").withRenderingMode(.alwaysTemplate), for: .normal)
                    btn.setTitleColor(.white, for: .normal)
                    btn.titleLabel?.font  = UIFont.systemFont(ofSize: 15, weight: .bold)
                    btn.addTarget(self, action: #selector(menuPressed(_:)), for: .touchUpInside)
                    btn.tintColor = .black
                    return btn
                }())
                btn.tintColor = .black
                return btn
            }(),
            ]
            self.navigationItem.rightBarButtonItems = []
        case .MenuWithVIPAndCustomer:
            self.navigationItem.leftBarButtonItems = [{
                let btn = UIBarButtonItem(customView: {
                    let btn = UIButton()
                    btn.setImage(#imageLiteral(resourceName: "img_menu").withRenderingMode(.alwaysTemplate), for: .normal)
                    btn.setTitleColor(.white, for: .normal)
                    btn.titleLabel?.font  = UIFont.systemFont(ofSize: 15, weight: .bold)
                    btn.addTarget(self, action: #selector(menuPressed(_:)), for: .touchUpInside)
                    btn.tintColor = .black
                    return btn
                }())
                btn.tintColor = .black
                return btn
            }(),
            ]
            self.navigationItem.rightBarButtonItems = [
                {
                    let btn = UIBarButtonItem(customView: {
                        let btn = UIButton(frame: CGRect(origin: .zero, size: CGSize(width: 40, height: 40)))
                        btn.setImage(#imageLiteral(resourceName: "img_dash_support").resizeImage(newWidth: 32)?.withRenderingMode(.alwaysTemplate), for: .normal)
                        btn.setTitleColor(.white, for: .normal)
                        btn.titleLabel?.font  = UIFont.systemFont(ofSize: 15, weight: .bold)
                        btn.addTarget(self, action: #selector(customerSupportPressed(_:)), for: .touchUpInside)
                        btn.cornerRadius = 5
                        btn.tintColor = .white
                        btn.backgroundColor = UIColor.colorAccentLight
                        return btn
                    }())
                    btn.tintColor = .white
                    return btn
                }(),
                {
                let btn = UIBarButtonItem(customView: {
                    let btn = UIButton()
                    btn.setImage(#imageLiteral(resourceName: "img_dash_membership").withRenderingMode(.alwaysOriginal), for: .normal)
                    btn.setTitleColor(.white, for: .normal)
                    btn.titleLabel?.font  = UIFont.systemFont(ofSize: 15, weight: .bold)
                    btn.addTarget(self, action: #selector(VIPPressed(_:)), for: .touchUpInside)
                    return btn
                }())
                btn.tintColor = .black
                return btn
            }()
            ]
        case .MenuWithBunkCurrentLocation:
            self.navigationItem.leftBarButtonItems = [{
                let btn = UIBarButtonItem(customView: {
                    let btn = UIButton()
                    btn.setImage(#imageLiteral(resourceName: "img_menu").withRenderingMode(.alwaysTemplate), for: .normal)
                    btn.setTitleColor(.white, for: .normal)
                    btn.titleLabel?.font  = UIFont.systemFont(ofSize: 15, weight: .bold)
                    btn.addTarget(self, action: #selector(menuPressed(_:)), for: .touchUpInside)
                    btn.tintColor = .black
                    return btn
                }())
                btn.tintColor = .black
                return btn
            }(),
            ]
            self.navigationItem.rightBarButtonItems = [
                {
                    let btn = UIBarButtonItem(customView: {
                        let btn = AppButton(frame: CGRect(origin: .zero, size: CGSize(width: 35, height: 35)))
                        btn.setImage(#imageLiteral(resourceName: "img_gps").resizeImage(newWidth: 18)?.withRenderingMode(.alwaysTemplate), for: .normal)
                        btn.titleLabel?.font  = UIFont.systemFont(ofSize: 15, weight: .bold)
                        btn.addTarget(self, action: #selector(currentLocationBtnPressed(_:)), for: .touchUpInside)
                        btn.tintColor = .black
                        btn.backgroundColor = .white
                        btn.isRounded = true
                        btn.borderWidth = 1
                        btn.borderColor = .black
                        return btn
                    }())
                    btn.tintColor = .black
                    return btn
                }(),
                {
                    let btn = UIBarButtonItem(customView: {
                        let btn = AppButton(frame: CGRect(origin: .zero, size: CGSize(width: 35, height: 35)))
                        btn.setImage(#imageLiteral(resourceName: "img_charging_point").resizeImage(newWidth: 18)?.withRenderingMode(.alwaysTemplate), for: .normal)
                        btn.setTitleColor(.white, for: .normal)
                        btn.titleLabel?.font  = UIFont.systemFont(ofSize: 15, weight: .bold)
                        btn.addTarget(self, action: #selector(bunkBtnPressed(_:)), for: .touchUpInside)
                        btn.isRounded = true
                        btn.tintColor = .black
                        btn.backgroundColor = UIColor.colorBtnYellow
                        return btn
                    }())
                    btn.tintColor = .white
                    return btn
                }()
            ]
        case .BackWithBunkCurrentLocation:
            self.navigationItem.leftBarButtonItems = [{
                let btn = UIBarButtonItem(customView: {
                    let btn = UIButton()
                    btn.setImage(backArrowImage.withRenderingMode(.alwaysTemplate), for: .normal)
                    btn.setTitleColor(.white, for: .normal)
                    btn.titleLabel?.font  = UIFont.systemFont(ofSize: 15, weight: .bold)
                    btn.addTarget(self, action: #selector(backPressed), for: .touchUpInside)
                    btn.tintColor = .black
                    return btn
                }())
                return btn
            }(),
            ]
            self.navigationItem.rightBarButtonItems = [
                {
                    let btn = UIBarButtonItem(customView: {
                        let btn = AppButton(frame: CGRect(origin: .zero, size: CGSize(width: 35, height: 35)))
                        btn.setImage(#imageLiteral(resourceName: "img_gps").resizeImage(newWidth: 18)?.withRenderingMode(.alwaysTemplate), for: .normal)
                        btn.titleLabel?.font  = UIFont.systemFont(ofSize: 15, weight: .bold)
                        btn.addTarget(self, action: #selector(currentLocationBtnPressed(_:)), for: .touchUpInside)
                        btn.tintColor = .black
                        btn.backgroundColor = .white
                        btn.isRounded = true
                        btn.borderWidth = 1
                        btn.borderColor = .black
                        return btn
                    }())
                    btn.tintColor = .black
                    return btn
                }(),
                {
                    let btn = UIBarButtonItem(customView: {
                        let btn = AppButton(frame: CGRect(origin: .zero, size: CGSize(width: 35, height: 35)))
                        btn.setImage(#imageLiteral(resourceName: "img_charging_point").resizeImage(newWidth: 18)?.withRenderingMode(.alwaysTemplate), for: .normal)
                        btn.setTitleColor(.white, for: .normal)
                        btn.titleLabel?.font  = UIFont.systemFont(ofSize: 15, weight: .bold)
                        btn.addTarget(self, action: #selector(bunkBtnPressed(_:)), for: .touchUpInside)
                        btn.isRounded = true
                        btn.tintColor = .black
                        btn.backgroundColor = UIColor.colorBtnYellow
                        return btn
                    }())
                    btn.tintColor = .white
                    return btn
                }()
            ]
        case .MenuWithTitle:
            self.navigationItem.leftBarButtonItems = [{
                let btn = UIBarButtonItem(customView: {
                    let btn = UIButton()
                    btn.setImage(#imageLiteral(resourceName: "img_menu").withRenderingMode(.alwaysTemplate), for: .normal)
                    btn.setTitleColor(.white, for: .normal)
                    btn.titleLabel?.font  = UIFont.systemFont(ofSize: 15, weight: .bold)
                    btn.addTarget(self, action: #selector(menuPressed(_:)), for: .touchUpInside)
                    btn.tintColor = .black
                    return btn
                }())
                btn.tintColor = .black
                return btn
            }(),
            ]
            self.navigationItem.titleView = {
                let titleLbl = UILabel()
                titleLbl.font = UIFont.systemFont(ofSize: 15, weight: .bold)
                titleLbl.text = title?.localized
                return titleLbl
            }()
//            self.navigationItem.title = nil
        case .Back:
            self.navigationItem.leftBarButtonItems = [{
                let btn = UIBarButtonItem(customView: {
                    let btn = UIButton()
                    btn.setImage(backArrowImage.withRenderingMode(.alwaysTemplate), for: .normal)
                    btn.setTitleColor(.white, for: .normal)
                    btn.titleLabel?.font  = UIFont.systemFont(ofSize: 15, weight: .bold)
                    btn.addTarget(self, action: #selector(backPressed), for: .touchUpInside)
                    btn.tintColor = .black
                    return btn
                }())
                return btn
            }(),
            ]
            self.navigationItem.rightBarButtonItems = []
            self.navigationItem.titleView = nil
            self.navigationItem.title = nil
        case .BackWithTitle:
            self.navigationItem.leftBarButtonItems = [{
                let btn = UIBarButtonItem(customView: {
                    let btn = UIButton()
                    btn.setImage(backArrowImage, for: .normal)
//                    btn.setTitle("  " + (title ?? ""), for: .normal)
//                    btn.setTitleColor(.white, for: .normal)
//                    btn.titleLabel?.font  = UIFont.systemFont(ofSize: 15, weight: .bold)
                    btn.addTarget(self, action: #selector(backPressed), for: .touchUpInside)
                    btn.tintColor = .black
                    return btn
                }())
                return btn
            }(),
            ]
            self.navigationItem.titleView = {
                let titleLbl = UILabel()
                titleLbl.font = UIFont.systemFont(ofSize: 15, weight: .bold)
                titleLbl.text = title?.localized
                return titleLbl
            }()
            self.navigationItem.title = nil
        default:
            self.navigationItem.leftBarButtonItems = []
            self.navigationItem.rightBarButtonItems = []
        }
    }
}

extension UIViewController {
    @IBAction func menuPressed(_ sender: Any) {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func VIPPressed(_ sender: Any) {
        let dashBoardVC = VIPVC.initFromStoryBoard(.Main)
        self.pushViewController(dashBoardVC, type: VIPVC.self)
    }
    
    @IBAction func customerSupportPressed(_ sender: Any) {
        self.view?.makeToast("Under development")
    }
    
    @IBAction func bunkBtnPressed(_ sender: Any) {
        
    }
    
    @IBAction func currentLocationBtnPressed(_ sender: Any) {
    }
    
    func logout() {
        
        self.homeViewController?.rideCheckHelper?.stopListening()
        self.homeViewController?.mapView.delegate = nil
        self.homeViewController?.rideCheckHelper = nil
        self.homeViewController?.removeChatObserver()
        self.homeViewController?.stopAnimationTimer()
        
        self.startActivityIndicatorInWindow()
        ServiceManager().logout({ [weak self](inner) -> (Void) in
            
            self?.stopActivityIndicator()
            GIDSignIn.sharedInstance.signOut()
            LoginManager().logOut()
            
            CacheManager.sharedInstance.clearUserData()
            CacheManager.sharedInstance.saveCache()
            
            Router.setLoginViewControllerAsRoot()
        })
        UserDefaults.companyLogin = false
    }
}
