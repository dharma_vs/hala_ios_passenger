//
//  LoadMoreTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit

public class LoadMoreTableViewCell: UITableViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override public func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        self.activityIndicator.startAnimating()
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
