//
//  AlertController.swift
//  HalaMobility
//
//  Created by Admin on 20/02/22.
//

import UIKit

class AlertController: BaseViewController ,UITextFieldDelegate {
    
    @IBOutlet weak var messageLbl: UILabel!
    
    @IBOutlet weak var defaultBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var inputField: AppTextField!
    
    @IBAction func defaultBtnAction(_ sender: Any) {
        if !inputField.isHidden{
            if (inputField.text?.count ?? 0) > 2{
                CVV = inputField.text ?? ""
                self.completion?(.default)
                if !skipDismiss {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }else{
            self.completion?(.default)
            if !skipDismiss {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        self.completion?(.cancel)
        if !skipDismiss {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    var completion:((UIAlertAction.Style) -> Void)?
    var message: String?
    var defaultBtnTitle: String?
    var cancelBtnTitle: String?
    var skipDismiss: Bool = false
    var canShowTextField = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.defaultBtn?.isHidden = true
        self.cancelBtn?.isHidden = true
        self.messageLbl?.text = message
        self.defaultBtn?.setTitle(defaultBtnTitle, for: .normal)
        self.cancelBtn?.setTitle(cancelBtnTitle, for: .normal)
        if defaultBtnTitle != nil {
            self.defaultBtn?.isHidden = false
        }
        if cancelBtnTitle != nil {
            self.cancelBtn?.isHidden = false
        }
        self.inputField?.isHidden = !canShowTextField
        // Do any additional setup after loading the view.
    }
    
    func addAction(_ action: UIAlertAction.Style, buttonPhrase: String, completion: ((UIAlertAction.Style) -> Void)?) {
        self.completion = completion
        if action == .default {
            self.defaultBtnTitle = buttonPhrase
        } else {
            self.cancelBtnTitle = buttonPhrase
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        let maxLength = 4
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
}
