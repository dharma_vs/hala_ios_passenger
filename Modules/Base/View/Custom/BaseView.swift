//
//  BaseView.swift
//  HalaMobility
//
//  Created by Admin on 20/02/22.
//

import UIKit


class BaseView: UIView {
    var view: UIView!
    
    func loadViewFromNib() -> UIView {
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: Bundle.main)
        if let tempView = nib.instantiate(withOwner: self, options: nil).first as? UIView {
            tempView.frame = self.bounds
            view = tempView
        } else {
            view = UIView()
        }
        return view
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        super.addSubview(view)
        
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func setup() {
        
    }
}

