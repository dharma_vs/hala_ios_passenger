//
//  TpBackgroundView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 11/03/22.
//

import Foundation
import UIKit

class TpBackgroundView: AppImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Setup view from .xib file
        self.setup(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Setup view from .xib file
        self.setup(frame: self.bounds)
    }
    
    func setup(frame: CGRect){
        let name = String(describing: type(of: self))
        let views = Bundle.main.loadNibNamed(name, owner: self, options: nil)
        let contentView = views?[0] as! UIView
        contentView.frame = frame
        self.addSubview(contentView)
    }
}
