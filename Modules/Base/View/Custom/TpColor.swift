//
//  TpColor.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 10/03/22.
//

import Foundation
import UIKit

class TpColor: UIColor {
    
    var buttonColor: UIColor {
        return UIColor().hexColor("A80D01")
    }
    
    //  Title
    var darkBlue : UIColor {
        return UIColor().hexColor("A80D01")
    }
    
    //  Content
    var blue : UIColor {
        return UIColor().hexColor("012CDF")
    }
    
    //  Place holder
    var lightblue : UIColor {
        return UIColor().hexColor("A4B1BB")
    }
    
    //  Button
    var pink : UIColor {
        return UIColor().hexColor("E36FA6")
    }
    
    //  Button bg value
    var red : UIColor {
        return UIColor().hexColor("A80D01")
    }
//    910700
    //  Positive value
    var green : UIColor {
        return UIColor().hexColor("3FD587")
    }
    
    //  White
    var white : UIColor {
        return UIColor().hexColor("FFFFFF")
    }
    
    //  Background color
    var bgcolor : UIColor {
        return UIColor().hexColor("F4F4F4")
    }
    var merun : UIColor {
        return UIColor().hexColor("8F0A00")
    }
    
    var appColor: UIColor {
        return UIColor().hexColor("910700")
    }
    
    var colorStatusSearching: UIColor {
      return UIColor.init(hex: 0x1763A6)
    }
    var ratingColor: UIColor { return UIColor.init(hex: 0x2EB52E) }
    var colorStatusAccepted: UIColor { return UIColor.init(hex: 0x3FD449) }
    var colorStatusArrived: UIColor { return UIColor.init(hex: 0xF25430) }
    var colorStatusRunning: UIColor { return UIColor.init(hex: 0x237529) }
    var colorStatusEnd: UIColor { return UIColor.init(hex: 0x114A7D) }
    var colorStatusCompleted: UIColor { return UIColor.init(hex: 0x194b1c) }
    var colorStatusCancelled: UIColor { return UIColor.init(hex: 0xF22600) }
    var colorStatusNoDriver: UIColor { return UIColor.init(hex: 0xF22657) }
    var colorTextDefault: UIColor { return UIColor.init(hex: 0x0b0b0b) }
    var colorTextDate: UIColor { return UIColor.init(hex: 0xa1a1a1) }

    var colorWalletRechargeHeader: UIColor { return UIColor.init(hex: 0x2ba80d01) }
    var colorWalletRideHeader: UIColor { return UIColor.init(hex: 0x2ba80d01) }
    var colorWalletWithDrawHeader: UIColor { return UIColor.init(hex: 0x2ba80d01) }
    
    var colorWalletRecharge: UIColor { return UIColor.init(hex: 0x0b9e00) }
    var colorWalletRide: UIColor { return UIColor.init(hex: 0xa80d01) }
    var colorWalletWithDraw: UIColor { return UIColor.init(hex: 0xa80d01) }
    
}
