//
//  GradientView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 23/02/22.
//

import UIKit

//@IBDesignable
class GradientView: UIView {
    
    @IBInspectable var startColorTheme:   Int = -1 { didSet { updateColors() }}
    @IBInspectable var endColorTheme:     Int = -1 { didSet { updateColors() }}

    
//    @IBInspectable var startColor:   UIColor = .gStartColor { didSet { updateColors() }}
//    @IBInspectable var endColor:     UIColor = .gEndColor { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    @IBInspectable var isReverseTheme:    Bool =  false { didSet { updatePoints() }}
    
    override class var layerClass: AnyClass { return CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        let startColor = self.startColorTheme != -1 ? ColorTheme(rawValue: self.startColorTheme)?.color ?? .gStartColor : .gStartColor
        let endColor = self.endColorTheme != -1 ? ColorTheme(rawValue: self.endColorTheme)?.color ?? .gEndColor: .gEndColor
        gradientLayer.colors = isReverseTheme ? [startColor.cgColor, startColor.cgColor] : [startColor.cgColor, endColor.cgColor]
    }
    
    var gradient: CAGradientLayer {
        return self.gradientLayer
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updatePoints()
        updateLocations()
        updateColors()
        
        self.cornerRadius = isRounded ? bounds.height/2 : cornerRadius
    }
}
