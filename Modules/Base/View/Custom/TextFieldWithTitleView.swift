//
//  TextFieldWithTitleView.swift
//  Taxi
//
//  Created by SELLADURAI on 27/02/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit
import TTTAttributedLabel

@objc protocol TextFieldWithTitleViewDelegate {
    func textFieldWithTitleRequestToShowTableView(_ view: TextFieldWithTitleView)
    func textFieldWithTitleContentBtnPressed(_ view: TextFieldWithTitleView)
}

//@IBDesignable
class TextFieldWithTitleView: BaseView {
    @IBOutlet weak var titleLbl: TTTAttributedLabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var leftActionBtn: UIButton!
    @IBOutlet weak var contentBtn: UIButton!
    
    @IBOutlet weak var borderLbl: UILabel!
    
    @IBInspectable var borderActiveColor: UIColor?
    @IBInspectable var borderInActiveColor: UIColor? {
        didSet {
            if isNeedBorderUpdate {
                self.borderLbl?.backgroundColor = borderInActiveColor
            }
        }
    }
    
    override func setup() {
        if isNeedBorderUpdate {
            self.borderLbl?.backgroundColor = borderInActiveColor
        }
        if let color = self.titleColor {
            self.titleColor = color
        }
    }
    
    @IBInspectable var isNeedBorderUpdate: Bool = false
    
    var didTapOnLeftAction: (()->Void)?
    
    @IBAction func leftAction(_ sender: Any) {
        self.didTapOnLeftAction?()
    }
    
    @IBOutlet weak var delegate: TextFieldWithTitleViewDelegate!
    
    @IBInspectable var title: String? {
        get {
            return (titleLbl.text as! String)
        }
        set {
            titleLbl.text = newValue?.localized
        }
    }
    
    @IBInspectable
    var titleColor: UIColor? {
        get {
            return titleLbl?.textColor
        }
        set {
            if let color = newValue {
                titleLbl?.textColor = color
            } else {
                titleLbl?.textColor = .black
            }
        }
    }
    
    @IBInspectable var textFieldText: String? {
        get {
            return textField.text ?? ""
        }
        set {
            textField.text = newValue?.localized
        }
    }
    
    @IBInspectable var textFieldPlaceholder: String? {
        get {
            return textField.placeholder
        }
        set {
            textField.placeholder = newValue?.localized
        }
    }
    
    @IBInspectable
    var textFieldColor: UIColor? {
        get {
            return textField.textColor
        }
        set {
            if let color = newValue {
                textField.textColor = color
            } else {
                textField.textColor = .black
            }
        }
    }
    
    @IBInspectable
    var isDropDown: Bool = false {
        didSet {
//            arrowImageView.isHidden = !isDropDown
        }
    }
    
    @IBInspectable var isDatePicker: Bool = false
    
    var maxDate: Date?
    var minDate: Date?
    var date: Date?
    
    @IBInspectable
    var isContentBtnEnabled: Bool = false {
        didSet {
            contentBtn.isHidden = !isContentBtnEnabled
        }
    }
    
    @IBAction func contentBtnPressed() {
        delegate?.textFieldWithTitleContentBtnPressed(self)
    }
    
    @IBInspectable var textThemeColor: Int = -1 {
        didSet {
            setup1()
        }
    }
    
    @IBInspectable var placeHolderThemeColor: Int = -1 {
        didSet {
            setup1()
        }
    }
    
    @IBInspectable var titleThemeColor: Int = -1 {
        didSet {
            setup1()
        }
    }
    
    @IBInspectable var borderThemeColor: Int = -1 {
        didSet {
            setup1()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup1()
    }
    
    func setup1() {
        if self.textThemeColor != -1 {
            //self.textField.textColor = ColorTheme.init(rawValue: self.textThemeColor)?.color ?? UIColor.black
            self.textField.textColor =  UIColor.black

        }
        if self.borderThemeColor != -1 {
            if isNeedBorderUpdate {
                self.borderActiveColor = ColorTheme.init(rawValue: self.titleThemeColor)?.color ?? UIColor.black
            } else {
                self.borderLbl?.backgroundColor = ColorTheme.init(rawValue: self.titleThemeColor)?.color ?? UIColor.black
            }
        }
        if self.titleThemeColor != -1 {
            self.titleLbl?.attributedText = NSAttributedString(string: (self.titleLbl?.text ?? "") as! String, attributes: [NSAttributedString.Key.foregroundColor : ColorTheme.init(rawValue: self.titleThemeColor)?.color ?? UIColor.black])
        }
        
        if self.placeHolderThemeColor != -1 {
            self.textField.attributedPlaceholder = NSAttributedString(string: self.textField.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : ColorTheme.init(rawValue: self.placeHolderThemeColor)?.color ?? UIColor.lightGray])
        }
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup1()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setup1()
    }
}

extension TextFieldWithTitleView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if isDropDown {
            delegate?.textFieldWithTitleRequestToShowTableView(self)
        }
        return !isDropDown
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if isNeedBorderUpdate {
            self.borderLbl.backgroundColor = self.borderActiveColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if isNeedBorderUpdate {
            self.borderLbl.backgroundColor = self.borderInActiveColor
        }
    }
}
