//
//  DateSelectionViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit


class DateSelectionViewController: BaseViewController {
    
    @IBOutlet weak var selectedDateLbl: AppLabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var cancelBtn: AppButton!
    @IBOutlet weak var okBtn: AppButton!
    
    var selectedDate: Date! {
        didSet {
            self.selectedDateLbl?.text = selectedDate.dateFromFormater("MMM dd, yyyy")
        }
    }
    
    var didSelectDate:((Date)->Void)?
    
    @IBAction func dateValueChanged(_ picker: UIDatePicker) {
        selectedDate = picker.date
    }
    
    @IBAction func confirmSelection(_ sender: Any) {
        self.didSelectDate?(selectedDate)
    }
    
    override func localize() {
        self.cancelBtn.setTitle("Cancel".localized, for: .normal)
        self.okBtn.setTitle("Ok".localized, for: .normal)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        selectedDate = Date()
    }
}

