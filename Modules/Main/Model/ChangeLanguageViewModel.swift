//
//  ChangeLanguageViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 09/03/22.
//

import Foundation

class ChangeLanguageViewModel: AppViewModel {
    weak var controller: ChangeLanguageViewController!
    required init(controller: ChangeLanguageViewController) {
        self.controller = controller
    }
}


public let CurrentApplicationLanguage = "CurrentApplicationLanguage"
public let AppLanguageChangedNotification = "AppLanguageChanged"
public let LCLCurrentLanguageKey = "LCLCurrentLanguageKey"
public let LCLDefaultLanguage = "en"
public let LCLLanguageChangeNotification = "LCLLanguageChangeNotification"


extension ServiceManager {
    
    public static func saveAppLanguage(_ lang: String) {
        UserDefaults.standard.set(lang, forKey: CurrentApplicationLanguage)
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppLanguageChangedNotification), object: nil)
    }
    
    public static func getAppLanguage() -> String {
        if let lang = UserDefaults.standard.object(forKey: CurrentApplicationLanguage) as? String {
            return lang
        } else {
            return "en"
        }
    }
    
    public static func setCurrentLanguage(_ language: String) {
        let selectedLanguage = availableLanguages().contains(language) ? language : defaultLanguage()
        if (selectedLanguage != currentLanguage()){
            UserDefaults.standard.set(selectedLanguage, forKey: LCLCurrentLanguageKey)
            UserDefaults.standard.synchronize()
            NotificationCenter.default.post(name: Notification.Name(rawValue: LCLLanguageChangeNotification), object: nil)
        }
    }
    
    public static func availableLanguages(_ excludeBase: Bool = false) -> [String] {
        var availableLanguages = Bundle.main.localizations
        // If excludeBase = true, don't include "Base" in available languages
        if let indexOfBase = availableLanguages.firstIndex(of: "Base") , excludeBase == true {
            availableLanguages.remove(at: indexOfBase)
        }
        return availableLanguages
    }
    
    public static func defaultLanguage() -> String {
        var defaultLanguage: String = String()
        guard let preferredLanguage = Bundle.main.preferredLocalizations.first else {
            return LCLDefaultLanguage
        }
        let availableLanguages: [String] = self.availableLanguages()
        if (availableLanguages.contains(preferredLanguage)) {
            defaultLanguage = preferredLanguage
        }
        else {
            defaultLanguage = LCLDefaultLanguage
        }
        return defaultLanguage
    }
    
    public static func currentLanguage() -> String {
        if let currentLanguage = UserDefaults.standard.object(forKey: LCLCurrentLanguageKey) as? String {
            return currentLanguage
        }
        return defaultLanguage()
    }
}
