//
//  RideDetail.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.

import Foundation
import JSONJoy

enum RideStatus: Int {
    
    case NewRide = 1
    
    case Accepted = 2
    
    case Arrived = 3
    
    case Started = 4
    
    case Completed = 5
    
    case UserCancelled = 6
    
    case DriverCancelled = 7
    
    case SystemCancelled = 8
    
    case PaymentCompleted = 9
    
    case None = 0
    
    case WaitingTime = 11
    
    var status: String {
        get {
            switch self {
            case .NewRide:
                return "NEW RIDE".localized
            case .Accepted:
                return "ACCEPTED".localized
            case .Arrived:
                return "ARRIVED".localized
            case .Started:
                return "On Ride".localized.uppercased()
            case .Completed:
                return "COMPLETED".localized
            case .UserCancelled:
                return "CANCELLED".localized
            case .DriverCancelled:
                return "CANCELLED".localized
            case .SystemCancelled:
                return "CANCELLED".localized
            case .PaymentCompleted:
                return "COMPLETED".localized
            case .WaitingTime:
                return "WAITING".localized

            case .None:
                return "None".localized
            }
        }
    }
    
    var tripStatus: String {
        get {
            switch self {
            case .NewRide:
                return "SCHEDULED".localized
            case .Accepted:
                return "ACCEPTED".localized
            case .Arrived:
                return "ARRIVED".localized
            case .Started:
                return "On Ride".localized.uppercased()
            case .Completed:
                return "COMPLETED".localized
            case .UserCancelled:
                return "CANCELLED BY RIDER".localized
            case .DriverCancelled:
                return "CANCELLED BY DRIVER".localized
            case .SystemCancelled:
                return "NO DRIVER - CANCELLED".localized
            case .PaymentCompleted:
                return "COMPLETED".localized
            case .None:
                return "None".localized
            case .WaitingTime:
                return "WAITING".localized

            }
        }
    }
    
    var color: UIColor {
        get {
            switch self {
            case .NewRide:
                return .colorStatusSearching
            case .Accepted:
                return .colorStatusAccepted
            case .Arrived:
                return .colorStatusArrived
            case .Started:
                return .colorStatusRunning
            case .Completed, .PaymentCompleted:
                return .colorStatusCompleted
            case .UserCancelled, .DriverCancelled, .SystemCancelled:
                return .colorStatusCancelled
            case .None:
                return .colorStatusNoDriver
            case .WaitingTime:
                return .colorStatusRunning
            }
        }
    }
}

class JoyResponse: JSONSerializable {
    
    var status: Bool?
    var message: String?
    var data : RideList?
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case data
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try? values.decode(Bool.self, forKey: .status)
        message = try? values.decode(String.self, forKey: .message)
        data = try? values.decode(RideList.self, forKey: .data)
    }
    
}

struct Rental: JSONSerializable {
    var packageID: CustomType?
    var packageName: CustomType?
    var startReading: CustomType?
    var startReadingImage: CustomType?
    var endReading: CustomType?
    var endReadingImage: CustomType?
    
    enum CodingKeys: String, CodingKey {
        case packageID = "package_id"
        case packageName = "package_name"
        case startReading = "start_reading"
        case startReadingImage = "start_reading_image"
        case endReading = "end_reading"
        case endReadingImage = "end_reading_image"
    }
}

struct RideList: JSONSerializable {
    
    var rides: [RideDetail]?
    
    enum CodingKeys: String, CodingKey {
        case rides
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        rides = try? values.decode([RideDetail].self, forKey: .rides)
    }
    
    init() {
        self.rides = []
    }
    
}

typealias EndRideInfo = (distance : Int, duration: String, mapRoot :String)

struct RideDetail: JSONSerializable {
    var rental: Rental?

    var rideId: String {
        return "\(self.id ?? 0)"
    }
    let id: Int?
    let booking_id: CustomType?
    let invoice_no: String?
    let rider_id, driver_id, current_driver_id, request_countdown, company_id, partner_id: Int?
    let overall_request_countdown: Int?
    let category_id: String?
    let vehicle_type_id, booking_status: Int?
    let country_short_name: String?
    let otp, ride_type, created_time, request_time: String?
    let booking_time, accepted_time, arrival_time, pickup_time, datetime: String?
    let drop_time, driver_rating, driver_feedback, rider_rating: String?
    let rider_feedback, canceled_by, cancellation_reason: String?
    let cancellation_charge: Int?
    var cancellation_driver_read: String?
    var cancellation_rider_read: String?
    var payment_mode, offer_applied: String?
    let city_name: String?
    let estimated_address: EstimatedAddress?
    let actual_address: EstimatedAddress?
    let offer: Offer?
    var estimated_distance, destination_change_time: String?
    let estimated_duration: String?
    let estimated_fare: Int?
    let trip_info: TripInfo?
    let fare_breakup_id: String?
    let fare_breakup_type: String?
    let fare_breakup: FareBreakupp?
    let payment_details:PaymentDetails?
    let vehicle_type_info: VehicleTypeInfo?
    let user_info: UserInfoo?
    let driver_info: DriverInfo?
    let vehicle_info: VehicleInfo?
    var card_info: Card_info?
    var ride_waiting: [Ride_waiting]?
    var ride_later_enabled: CustomType?
    let batteryMaxVoltage, batteryMinVoltage, batteryVoltage, p_rider_rating, p_driver_rating, toll_charge: CustomType?
    let ridingDistance, ridingDuration: Int?
    let bookingForName, bookingForPhone: String?
    let registrationID: CustomType?
    let pickupSignature, deliverySignature, ridePickupDatetime, rideReturnDatetime: String?
    let route, vendorName: String?
    let vendorImage: CustomType?
    let comment, processStatus, customerComments: String?
    let delivery: Delivery?
    //let rideWaiting: [JSONAny]

    var estimated_mapInfo: MapData {
        var mapData = MapData()
        mapData.root = self.estimated_address?.map_root
        mapData.source = self.estimated_address?.source
        mapData.destination = self.estimated_address?.destination
        mapData.distance = Double(self.estimated_distance ?? "0")
        return mapData
    }
    
    var actual_mapInfo: MapData {
        var mapData = MapData()
        mapData.root = self.actual_address?.map_root
        mapData.source = self.actual_address?.source
        mapData.destination = self.actual_address?.destination
        mapData.distance = Double(self.trip_info?.distance ?? "0")
        return mapData
    }
    
    enum CodingKeys: String, CodingKey {
        case fare_breakup_type
        case fare_breakup_id
        case id
        case booking_id
        case invoice_no
        case country_short_name
        case rider_id, driver_id, current_driver_id, request_countdown, company_id, partner_id
        case overall_request_countdown
        case category_id
        case vehicle_type_id, booking_status
        case otp, ride_type, created_time, request_time
        case booking_time, accepted_time, arrival_time, pickup_time, datetime
        case drop_time, driver_rating, driver_feedback, rider_rating
        case rider_feedback, canceled_by, cancellation_reason
        case cancellation_charge
        case cancellation_driver_read
        case cancellation_rider_read, payment_mode, offer_applied
        case city_name
        case estimated_address
        case actual_address
        case offer
        case estimated_distance
        case estimated_duration
        case estimated_fare
        case trip_info
        case fare_breakup
        case payment_details
        case vehicle_type_info
        case user_info
        case driver_info
        case vehicle_info
        case rental
        case card_info
        case ride_waiting, ride_later_enabled, destination_change_time
        case batteryMaxVoltage, batteryMinVoltage, batteryVoltage, p_rider_rating, p_driver_rating, toll_charge
        case ridingDistance = "riding_distance"
        case ridingDuration = "riding_duration"
        case bookingForName = "booking_for_name"
        case bookingForPhone = "booking_for_phone"
        case registrationID = "registration_id"
        case pickupSignature = "pickup_signature"
        case deliverySignature = "delivery_signature"
        case ridePickupDatetime = "ride_pickup_datetime"
        case rideReturnDatetime = "ride_return_datetime"
        case route
        case vendorName = "vendor_name"
        case vendorImage = "vendor_image"
        case comment
        case processStatus = "process_status"
        case customerComments = "customer_comments"
        case delivery
       // case rideWaiting = "ride_waiting"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        destination_change_time = try? values.decode(String.self, forKey: .destination_change_time)
        fare_breakup_type = try? values.decode(String.self, forKey: .fare_breakup_type)
        ride_later_enabled = try? values.decode(CustomType.self, forKey: .ride_later_enabled)
        fare_breakup_id = try? values.decode(String.self, forKey: .fare_breakup_id)
        id = try? values.decode(Int.self, forKey: .id)
        country_short_name = try? values.decode(String.self, forKey: .country_short_name)
        booking_id = try? values.decode(CustomType.self, forKey: .booking_id)
        invoice_no = try? values.decode(String.self, forKey: .invoice_no)
        rider_id = try? values.decode(Int.self, forKey: .rider_id)
        driver_id = try? values.decode(Int.self, forKey: .driver_id)
        current_driver_id  = try? values.decode(Int.self, forKey: .current_driver_id)
        request_countdown = try? values.decode(Int.self, forKey: .request_countdown)
        overall_request_countdown = try? values.decode(Int.self, forKey: .overall_request_countdown)
        category_id = try? values.decode(String.self, forKey: .category_id)
        vehicle_type_id = try? values.decode(Int.self, forKey: .vehicle_type_id)
        booking_status = try? values.decode(Int.self, forKey: .booking_status)
        otp = try? values.decode(String.self, forKey: .otp)
        ride_type = try? values.decode(String.self, forKey: .ride_type)
        created_time = try? values.decode(String.self, forKey: .created_time)
        request_time = try? values.decode(String.self, forKey: .request_time)
        booking_time = try? values.decode(String.self, forKey: .booking_time)
        accepted_time = try? values.decode(String.self, forKey: .accepted_time)
        datetime = try? values.decode(String.self, forKey: .datetime)
        arrival_time = try? values.decode(String.self, forKey: .arrival_time)
        pickup_time = try? values.decode(String.self, forKey: .pickup_time)
        drop_time = try? values.decode(String.self, forKey: .drop_time)
        driver_rating = try? values.decode(String.self, forKey: .driver_rating)
        driver_feedback = try? values.decode(String.self, forKey: .driver_feedback)
        rider_rating = try? values.decode(String.self, forKey: .rider_rating)
        rider_feedback = try? values.decode(String.self, forKey: .rider_feedback)
        canceled_by = try? values.decode(String.self, forKey: .canceled_by)
        cancellation_reason = try? values.decode(String.self, forKey: .cancellation_reason)
        cancellation_charge = try? values.decode(Int.self, forKey: .cancellation_charge)
        cancellation_driver_read = try? values.decode(String.self, forKey: .cancellation_driver_read)
        cancellation_rider_read = try? values.decode(String.self, forKey: .cancellation_rider_read)
        payment_mode = try? values.decode(String.self, forKey: .payment_mode)
        offer_applied = try? values.decode(String.self, forKey: .offer_applied)
        city_name = try? values.decode(String.self, forKey: .city_name)
        estimated_address = try? values.decode(EstimatedAddress.self, forKey: .estimated_address)
        actual_address  = try? values.decode(EstimatedAddress.self, forKey: .actual_address)
        offer = try? values.decode(Offer.self, forKey: .offer)
        estimated_distance = try? values.decode(String.self, forKey: .estimated_distance)
        company_id = try? values.decode(Int.self, forKey: .company_id)
        partner_id = try? values.decode(Int.self, forKey: .partner_id)
        batteryMaxVoltage = try? values.decode(CustomType.self, forKey: .batteryMaxVoltage)
        batteryMinVoltage = try? values.decode(CustomType.self, forKey: .batteryMinVoltage)
        batteryVoltage = try? values.decode(CustomType.self, forKey: .batteryVoltage)
        p_rider_rating = try? values.decode(CustomType.self, forKey: .p_rider_rating)
        p_driver_rating = try? values.decode(CustomType.self, forKey: .p_driver_rating)
        toll_charge = try? values.decode(CustomType.self, forKey: .toll_charge)
        
        ridingDistance = try! values.decode(Int.self, forKey: .ridingDistance)
        ridingDuration = try? values.decode(Int.self, forKey: .ridingDuration)
        bookingForName = try? values.decode(String.self, forKey: .bookingForName)
        bookingForPhone = try? values.decode(String.self, forKey: .bookingForPhone)
        pickupSignature = try? values.decode(String.self, forKey: .pickupSignature)
        deliverySignature = try? values.decode(String.self, forKey: .deliverySignature)
        ridePickupDatetime = try? values.decode(String.self, forKey: .ridePickupDatetime)
        rideReturnDatetime = try? values.decode(String.self, forKey: .rideReturnDatetime)
        route = try? values.decode(String.self, forKey: .route)
        registrationID = try? values.decode(CustomType.self, forKey: .registrationID)
        vendorName = try? values.decode(String.self, forKey: .vendorName)
        comment = try? values.decode(String.self, forKey: .comment)
        processStatus = try? values.decode(String.self, forKey: .processStatus)
        customerComments = try? values.decode(String.self, forKey: .customerComments)
        vendorImage = try? values.decode(CustomType.self, forKey: .vendorImage)
        if estimated_distance == nil {
            if let doubleDist = try? values.decode(Double.self, forKey: .estimated_distance) {
                estimated_distance = "\(doubleDist)"
            }
        }
        
        if estimated_distance == nil {
            if let intDist = try? values.decode(Int.self, forKey: .estimated_distance) {
                estimated_distance = "\(intDist)"
            }
        }
        estimated_duration = try? values.decode(String.self, forKey: .estimated_duration)
        estimated_fare = try? values.decode(Int.self, forKey: .estimated_fare)
        trip_info = try? values.decode(TripInfo.self, forKey: .trip_info)
        fare_breakup = try? values.decode(FareBreakupp.self, forKey: .fare_breakup)
        payment_details = try? values.decode(PaymentDetails.self, forKey: .payment_details)
        vehicle_type_info = try? values.decode(VehicleTypeInfo.self, forKey: .vehicle_type_info)
        user_info = try? values.decode(UserInfoo.self, forKey: .user_info)
        driver_info = try? values.decode(DriverInfo.self, forKey: .driver_info)
        vehicle_info = try? values.decode(VehicleInfo.self, forKey: .vehicle_info)
        rental = try? values.decode(Rental.self, forKey: .rental)
        card_info = try? values.decode(Card_info.self, forKey: .card_info)
        ride_waiting = try? values.decode([Ride_waiting].self, forKey: .ride_waiting)
        delivery = try? values.decode(Delivery.self, forKey: .delivery)
    }
}

struct Offer: JSONSerializable {
    let id: Int?
    let offer_type: String?
    let code: String?
    let offer: String?
    let message: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case offer_type
        case code
        case offer
        case message
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try? values.decode(Int.self, forKey: .id)
        offer_type = try? values.decode(String.self, forKey: .offer_type)
        code = try? values.decode(String.self, forKey: .code)
        offer = try? values.decode(String.self, forKey: .offer)
        message = try? values.decode(String.self, forKey: .message)
    }
}
// MARK: - Delivery
struct Delivery: Codable {
    let pickupContactName, pickupContactNumber, pickupSignature, deliveryContactName: String
    let deliveryContactNumber, deliverySignature, itemInformation, descriptionNote: String
    let weight: Int

    enum CodingKeys: String, CodingKey {
        case pickupContactName = "pickup_contact_name"
        case pickupContactNumber = "pickup_contact_number"
        case pickupSignature = "pickup_signature"
        case deliveryContactName = "delivery_contact_name"
        case deliveryContactNumber = "delivery_contact_number"
        case deliverySignature = "delivery_signature"
        case itemInformation = "item_information"
        case descriptionNote = "description_note"
        case weight
    }
}
struct EstimatedAddress: JSONSerializable {
    let source, destination: Destination?
    let map_root: String?
    
    enum CodingKeys: String, CodingKey {
        case source
        case destination
        case map_root
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        source = try? values.decode(Destination.self, forKey: .source)
        destination = try? values.decode(Destination.self, forKey: .destination)
        map_root = try? values.decode(String.self, forKey: .map_root)
    }
    
    var isEmpty: Bool {
        return source == nil || destination == nil
    }
}

struct Destination: JSONSerializable {
    let name, address: String?
    var latitude, longitude: Double?
    var locationCoordinate: LocationCoordinate {
        return LocationCoordinate(latitude: self.latitude ?? 0.0, longitude: self.longitude ?? 0.0)
    }
    enum CodingKeys: String, CodingKey {
        case name, address
        case latitude, longitude
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try? values.decode(String.self, forKey: .name)
        address = try? values.decode(String.self, forKey: .address)
        let lat: CustomType? = try? values.decode(CustomType.self, forKey: .latitude)
        latitude = lat?.doubleValue
        let lng: CustomType? = try? values.decode(CustomType.self, forKey: .longitude)
        longitude = lng?.doubleValue
    }
    var isEmpty: Bool {
        return address == nil || address?.isEmpty == true
    }
}

struct UserInfoo: JSONSerializable {
    let id: Int?
    let name, email, gender, mobile: String?
    let ccp, image, device_type, rating: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name, email, gender, mobile
        case ccp, image, device_type, rating
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try? values.decode(Int.self, forKey: .id)
        name = try? values.decode(String.self, forKey: .name)
        email = try? values.decode(String.self, forKey: .email)
        gender = try? values.decode(String.self, forKey: .gender)
        mobile = try? values.decode(String.self, forKey: .mobile)
        ccp = try? values.decode(String.self, forKey: .ccp)
        image = try? values.decode(String.self, forKey: .image)
        device_type = try? values.decode(String.self, forKey: .device_type)
        rating = try? values.decode(String.self, forKey: .rating)
    }
}

struct VehicleTypeInfo: JSONSerializable {
    let display_name, capacity, image, cab_marker: String?
    
    enum CodingKeys: String, CodingKey {
        case display_name, capacity, image, cab_marker
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        display_name = try? values.decode(String.self, forKey: .display_name)
        capacity = try? values.decode(String.self, forKey: .capacity)
        image = try? values.decode(String.self, forKey: .image)
        cab_marker = try? values.decode(String.self, forKey: .cab_marker)
    }
}

struct TripInfo: JSONSerializable {
    let amount: Int?
    let payable_amount: Int?
    var distance: String?
    let trip_time: Int?
    let wait_time: Int?
    let discount: CustomType?
    let advance: String?
    let mode_of_advance: String?
    
    enum CodingKeys: String, CodingKey {
        case amount
        case payable_amount
        case distance
        case trip_time
        case wait_time
        case discount
        case advance
        case mode_of_advance
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        amount = try? values.decode(Int.self, forKey: .amount)
        payable_amount = try? values.decode(Int.self, forKey: .payable_amount)
        distance = try? values.decode(String.self, forKey: .distance)
        if distance == nil {
            if let dist = try? values.decode(Int.self, forKey: .distance) {
                distance = "\(dist)"
            }
        }
        if distance == nil {
            if let dist = try? values.decode(Double.self, forKey: .distance) {
                distance = "\(dist)"
            }
        }
        trip_time = try? values.decode(Int.self, forKey: .trip_time)
        wait_time = try? values.decode(Int.self, forKey: .wait_time)
        discount = try? values.decode(CustomType.self, forKey: .discount)
        advance = try? values.decode(String.self, forKey: .advance)
        mode_of_advance = try? values.decode(String.self, forKey: .mode_of_advance)
    }
}

struct FareBreakupp: JSONSerializable {
    var ride_fare: CustomType?
    var advance_booking_fee: CustomType?
    var taxes: CustomType?
    var surge_fare: CustomType?
    var round_off: CustomType?
    var total_bill: CustomType?
    var cancellation_charge: CustomType?
    var destination_change_fare: CustomType?
    var waiting_charges: CustomType?
    var peak_time_charge:CustomType?
    var baseFare, batta: CustomType?
    var distanceFare, rideTimeCharge, destinationChangeFare: CustomType?
    var nightTimeCharge: CustomType?
    var extraKMFare, extraMinFare: CustomType?
    var tollFare: CustomType?
    var taxLabel, taxPercentage: CustomType?
    var taxAmount, roundOff, totalBill: CustomType?
    var commPercentage: CustomType?
    var commission: CustomType?
    var commTaxPercentage: CustomType?
    var commTaxAmount: CustomType?
    
    enum CodingKeys: String, CodingKey {
        case ride_fare
        case advance_booking_fee
        case taxes
        case surge_fare
        case round_off
        case total_bill
        case cancellation_charge
        case destination_change_fare
        case waiting_charges
        case peak_time_charge
        case baseFare = "base_fare"
        case batta
        case distanceFare = "distance_fare"
        case rideTimeCharge = "ride_time_charge"
        case nightTimeCharge = "night_time_charge"
        case extraKMFare = "extra_km_fare"
        case extraMinFare = "extra_min_fare"
        case tollFare = "toll_fare"
        case taxLabel = "tax_label"
        case taxPercentage = "tax_percentage"
        case taxAmount = "tax_amount"
        case commPercentage = "comm_percentage"
        case commission
        case commTaxPercentage = "comm_tax_percentage"
        case commTaxAmount = "comm_tax_amount"
    }
    
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        ride_fare = try? values.decode(Int.self, forKey: .ride_fare)
//        advance_booking_fee = try? values.decode(Int.self, forKey: .ride_fare)
//        taxes = try? values.decode(String.self, forKey: .taxes)
//        surge_fare = try? values.decode(Double.self, forKey: .surge_fare)
//        round_off = try? values.decode(Double.self, forKey: .round_off)
//        total_bill = try? values.decode(Int.self, forKey: .total_bill)
//        cancellation_charge = try? values.decode(Int.self, forKey: .cancellation_charge)
//        destination_change_fare = try? values.decode(Int.self, forKey: .destination_change_fare)
//        waiting_charges = try? values.decode(Int.self, forKey: .waiting_charges)
//        peak_time_charge = try? values.decode(Double.self, forKey: .peak_time_charge)
//
//    }
}

struct PaymentDetails: JSONSerializable {
    let pay_by: String?
    let cash_payment: Int?
    let card_payment: Int?
    let wallet_payment: Int?
    
    var paidBy: String {
        let payBy = self.pay_by ?? "1"
        switch payBy {
        case "1":
            return "Cash Collected".localized
        case "2":
            return "Paid by Card".localized
        case "3":
            return "Paid by Wallet".localized
        default:
            return ""
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case pay_by
        case cash_payment
        case card_payment
        case wallet_payment
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pay_by = try? values.decode(String.self, forKey: .pay_by)
        cash_payment = try? values.decode(Int.self, forKey: .cash_payment)
        card_payment = try? values.decode(Int.self, forKey: .card_payment)
        wallet_payment = try? values.decode(Int.self, forKey: .wallet_payment)
    }
}

struct DriverInfo: JSONSerializable {
    var id: Int?
    var name: String?
    var ccp: String?
    var phone: String?
    var email: String?
    var mobile: String?
    var image: String?
    var device_type: String?
    var rating: CustomType?
    var lat: Double?
    var lng: Double?
    var total_ride: Int?
    
    var locationCoordinate: LocationCoordinate {
        return LocationCoordinate.init(latitude: self.lat ?? 0.0, longitude: self.lng ?? 0.0)
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case ccp
        case phone
        case email
        case mobile
        case image
        case device_type
        case rating
        case lat
        case lng
        case total_ride
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try? values.decode(Int.self, forKey: .id)
        name = try? values.decode(String.self, forKey: .name)
        ccp = try? values.decode(String.self, forKey: .ccp)
        phone = try? values.decode(String.self, forKey: .phone)
        email = try? values.decode(String.self, forKey: .email)
        mobile = try? values.decode(String.self, forKey: .mobile)
        image = try? values.decode(String.self, forKey: .image)
        device_type = try? values.decode(String.self, forKey: .device_type)
        rating = try? values.decode(CustomType.self, forKey: .rating)
        lat = try? values.decode(Double.self, forKey: .lat)
        lng = try? values.decode(Double.self, forKey: .lng)
        total_ride = try? values.decode(Int.self, forKey: .total_ride)
    }
}

struct VehicleInfo: JSONSerializable {
    var make: String?
    var number: String?
    var model: String?
    var color: String?
    var features: String?
    var battery_max_voltage: Int?
    var battery_min_voltage: Int?
    var battery_voltage: Int?
    enum CodingKeys: String, CodingKey {
        case make
        case number
        case model
        case color
        case features
        case battery_max_voltage
        case battery_min_voltage
        case battery_voltage
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        make = try? values.decode(String.self, forKey: .make)
        number = try? values.decode(String.self, forKey: .number)
        model = try? values.decode(String.self, forKey: .model)
        color = try? values.decode(String.self, forKey: .color)
        features = try? values.decode(String.self, forKey: .features)
        battery_max_voltage = try? values.decode(Int.self, forKey: .battery_max_voltage)
        battery_min_voltage = try? values.decode(Int.self, forKey: .battery_min_voltage)
        battery_voltage = try? values.decode(Int.self, forKey: .battery_voltage)
    }
    
}

struct MapData : JSONSerializable {
    var root: String!
    var source: Destination!
    var destination: Destination!
    var distance: Double!
    
    
    var mapURL: String {
//        return "https://maps.googleapis.com/maps/api/staticmap?size=600x400&path=color:0xA80D01FF|weight:3|enc:" + self.root + "&zoom=\(self.zoomLevel)" + "&markers=icon:http://project.virtuesense.com/mobile_dev/Taxi/Images/ic_source.png|\(self.source?.latitude ?? 0.0),\(self.source?.longitude ?? 0.0)&markers=icon:http://project.virtuesense.com/mobile_dev/Taxi/Images/ic_destination.png|\(self.destination?.latitude ?? 0.0),\(self.destination?.longitude ?? 0.0)&key=\(googleMapKey)"
        
        var str = "https://maps.googleapis.com/maps/api/staticmap?size=600x400&path=color:0xA80D01FF|weight:3|enc:" + self.root + "&zoom=\(self.zoomLevel)"
        
        let sourceLat = (self.source.latitude ?? 0.0)
        let destLat = (self.destination.latitude ?? 0.0)
        if sourceLat != 0.0 {
            str += "&markers=icon:\(CacheManager.settings?.source_marker ?? "")|\(self.source.latitude ?? 0.0),\(self.source.longitude ?? 0.0)"
        }
        if destLat != 0.0 {
            str += "&markers=icon:\(CacheManager.settings?.destination_marker ?? "")|\(self.destination.latitude ?? 0.0),\(self.destination.longitude ?? 0.0)"
        }
        
        return str + "&key=\(googleMapKey)"
    }
    
    var zoomLevel: Double {
        let EQUATOR_LENGTH: Double = 40075004.0
        let latitudanalAdjustment: Double = cos(Double.pi * (self.source.latitude ?? 0.0) / 180)
        let arg: Double = (EQUATOR_LENGTH * Double(UIScreen.main.bounds.width) * latitudanalAdjustment) / ((self.distance * 1000) * 256);
        return log(arg) / log(2)
    }
}
class Ride_waiting : NSObject, JSONSerializable, NSCoding {
    
    let id : Int?
    let start_date : String?
    let end_date : String?
    let minute : Int?
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.start_date = aDecoder.decodeObject(forKey: "start_date") as? String
        self.end_date = aDecoder.decodeObject(forKey: "end_date") as? String
        self.minute = aDecoder.decodeObject(forKey: "minute") as? Int
       
    }
   func encode(with aCoder: NSCoder) {
          for case let (label, value) in Mirror(reflecting: self).children {
              aCoder.encode(value, forKey: label!)
          }
      }
}
