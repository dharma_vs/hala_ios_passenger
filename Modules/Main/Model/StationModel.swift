//
//  StationModel.swift
//  HalaMobility
//
//  Created by ADMIN on 20/05/22.
//

import Foundation
struct Stationres: JSONSerializable {
    var status: Bool?
    var message: String?
    var data: [Station]?
}

// MARK: - Datum
struct Station: Codable {
    var id: Int?
    var name, address, image: String?
    var marker: String?
    var city_name: String?
    var latitude, longitude, distance: Double?
    var capacity, vehicle_count: Int?

//    enum CodingKeys: String, CodingKey {
//        case id, name, address
//        //case cityName = "city_name"
//        case image, marker, latitude, longitude, distance, capacity
//        //case vehicleCount = "vehicle_count"
//    }
}
