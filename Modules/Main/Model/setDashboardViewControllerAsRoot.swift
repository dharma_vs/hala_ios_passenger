//
//  setDashboardViewControllerAsRoot.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 09/03/22.
//

import Foundation

static func setDashboardViewControllerAsRoot() {
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    if appDelegate?.window == nil {
        appDelegate?.window = UIWindow.init(frame: UIScreen.main.bounds)
    }
    if UserDefaults.companyLogin{
        let tripsVC = Router.main.instantiateViewController(withIdentifier: StoryboardIDs.TripsViewController) as! TripsViewController
        let navigationViewController = UINavigationController.init(rootViewController: tripsVC)
             navigationViewController.isNavigationBarHidden = true
             let menuViewController = Router.main.instantiateViewController(withIdentifier: StoryboardIDs.MenuViewController)
             
             SlideMenuOptions.hideStatusBar = false
             SlideMenuOptions.contentViewScale = 1.0
             SlideMenuOptions.panFromBezel = false
             
             let slideMenuViewController = SlideMenuController.init(mainViewController: navigationViewController, leftMenuViewController: menuViewController)
             
             appDelegate?.window?.rootViewController = slideMenuViewController
    }else{
        let dashboardVC = Router.main.instantiateViewController(withIdentifier: StoryboardIDs.DashboardViewController) as! DashboardViewController
        let navigationViewController = UINavigationController.init(rootViewController: dashboardVC)
             navigationViewController.isNavigationBarHidden = true
             let menuViewController = Router.main.instantiateViewController(withIdentifier: StoryboardIDs.MenuViewController)
             
             SlideMenuOptions.hideStatusBar = false
             SlideMenuOptions.contentViewScale = 1.0
             SlideMenuOptions.panFromBezel = false
             
             let slideMenuViewController = SlideMenuController.init(mainViewController: navigationViewController, leftMenuViewController: menuViewController)
             
             appDelegate?.window?.rootViewController = slideMenuViewController
        
//            DataService().checkCurrentRideStatus { (inner) -> (Void) in
//                let rideList = (try? inner()) ?? nil
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                    dashboardVC.viewModel = DashboardViewModel(rideList)
//                }
//            }
    }
 
}

public struct SlideMenuOptions {
    public static var leftViewWidth: CGFloat = 270.0
    public static var leftBezelWidth: CGFloat? = 16.0
    public static var contentViewScale: CGFloat = 0.96
    public static var contentViewOpacity: CGFloat = 0.5
    public static var contentViewDrag: Bool = false
    public static var shadowOpacity: CGFloat = 0.0
    public static var shadowRadius: CGFloat = 0.0
    public static var shadowOffset: CGSize = CGSize(width: 0,height: 0)
    public static var panFromBezel: Bool = true
    public static var animationDuration: CGFloat = 0.4
    public static var animationOptions: UIView.AnimationOptions = []
    public static var rightViewWidth: CGFloat = 270.0
    public static var rightBezelWidth: CGFloat? = 16.0
    public static var rightPanFromBezel: Bool = true
    public static var hideStatusBar: Bool = true
    public static var pointOfNoReturnWidth: CGFloat = 44.0
    public static var simultaneousGestureRecognizers: Bool = true
    public static var opacityViewBackgroundColor: UIColor = UIColor.black
    public static var panGesturesEnabled: Bool = true
    public static var tapGesturesEnabled: Bool = true
}
