//
//  VerifyCouponParams.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import Foundation

struct VerifyCouponParams: JSONSerializable {
    var type_ride: String!
    var code: String!
}
