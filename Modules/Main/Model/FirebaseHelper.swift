//
//  FirebaseHelper.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import Foundation
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth

//let fireBaseURL = "https://moveonsense.firebaseio.com/"
//let fireBaseDB = "Taxi/Android_Dev_1"
//let fireBaseDB = "Taxi/Demo"
//let fireBaseDB = "Taxi/Dev"
//let fireBaseStorageURL = "gs://moveonsense.appspot.com"
//let fireBaseUserName = "admin@admin.com"
//let fireBasePassword = "Admin@123"


let fireBaseURL = "https://jeffery-2020.firebaseio.com/"
let fireBaseDB = "/Taxi/Dev"
let fireBaseFleetLocationDB = "/Taxi/Dev"
let fireBaseDriverLocationDB = "/Taxi/Dev/drivers"

let fireBaseStorageURL = "gs://taxiliveafrica.appspot.com"
let fireBaseUserName = "admin@jeffery.com"
let fireBasePassword = "jeffery2020"
var firebaseUserID = "i1IqGMzxQ6WzxGsciYXNktLA3Eg1"

var rideId: String = ""

typealias UploadTask = StorageUploadTask
typealias SnapShot = DataSnapshot
typealias EventType = DataEventType

class FirebaseHelper: NSObject {
    
    private var ref: DatabaseReference?
    
    private var storage : Storage?
    
    static var shared = FirebaseHelper()
    
    // Write Text Message
    
    func write(to userId : Int, with text : String, type chatType : ChatType = .single){
        
        self.storeData(to: userId, with: text, mime: .text, type: chatType)
        
    }
    
    // Upload from data
    
    func write(to userId : Int, with data : Data, mime type : Mime, type chatType : ChatType = .single, completion : @escaping (Bool)->())->UploadTask{
        
        let metadata = self.initializeStorage(with: type)
        
        return self.upload(data: data,forUser : userId, mime: type, type : chatType, metadata: metadata, completion: { (url) in
            
            completion(url != nil)
            
            guard url != nil else {
                return
            }
            
            self.storeData(to: userId, with: url, mime: type, type: chatType)
            
        })
        
    }
    
    // Upload from Filepath
    
    func write(to userId : Int, file url : URL, mime type : Mime, type chatType : ChatType = .single , completion : @escaping (Bool)->())->UploadTask{
        
        let metadata = self.initializeStorage(with: type)
        
        return self.upload(file: url,forUser : userId, mime: type, type : chatType,metadata: metadata, completion: { (url) in
            
            completion(url != nil)
            
            guard url != nil else {
                return
            }
            
            self.storeData(to: userId, with: url, mime: type, type: chatType)
            
        })
        
        
    }
    
    // Update Message in Specific Path
    
    func update(chat: ChatEntity, key : String, toUser user : Int, type chatType : ChatType = .single){
        
        let chatPath = "users/" + firebaseUserID + fireBaseDB + "/Chats/" + rideId + "/Messages" //Common.getChatId(with: user) ?? .Empty
        
        self.update(chat: chat, key: key, inRoom: chatPath)
        
    }
    
}


//MARK:- Helper Functions


extension FirebaseHelper {
    
    // Initializing DB
    
    private func initializeDB(){
        
        if ref == nil {
            let db = Database.database(url: fireBaseURL)
            db.isPersistenceEnabled = true
            self.ref = db.reference()
        }
        
        let auth = Auth.auth()
        auth.signIn(withEmail: fireBaseUserName, password: fireBasePassword) { (result, error) in
            if result != nil {
                firebaseUserID = (result?.user.uid)!
            }
        }
    }
    
    // Initializing Storage
    
    private func initializeStorage(with type : Mime)->StorageMetadata{
        
        if self.storage == nil {
            self.storage = Storage.storage(url: fireBaseStorageURL)
        }
        
        
        let metadata = StorageMetadata()
        metadata.contentType = type.contentType
        
        return metadata
    }
    
    // Update Values in specific path
    
    private func update(chat : ChatEntity, key : String, inRoom room : String){
        if chat.receive_on?.contains("2018") == true {
            print("Stop")
        }
        self.ref?.child(room).child(key).updateChildValues(chat.JSONRepresentation)
        
    }
    
    // Common Function to Store Data
    
    private func storeData(to userId : Int, with string : String?, mime type : Mime, type chatType : ChatType){
        
        let chat = ChatEntity()
        chat.ride_id = rideId
        chat.read_on = ""
        chat.receive_on = ""
        //        chat.status = MsgStatus.sent.rawValue
        chat.receiver_id = "D_\(userId)"
        chat.sender_id = "P_\((CacheManager.riderInfo?.id) ?? 0)"
        //        chat.number = String.removeNil(User.main.mobile)
        let sec = self.getUTCCurrentTimeMillis()
        chat.sent_on = "\(sec)"
        chat.message = string
        let messageId = "\(sec)_P_\(userId)"
        chat.message_id = messageId
        chat.type = "1"
        
        //        if type == .text {
        //
        //            chat.text = string
        //
        //        } else {
        //
        //            chat.url = string
        //
        //        }
        
        //        if chatType == .group {
        //            chat.groupId = userId
        //        }
        
        self.initializeDB()
        let chatPath = "users/" + firebaseUserID + fireBaseDB + "/Chats/" + rideId + "/Messages/" + messageId //Common.getChatId(with: userId) ?? .Empty
        self.ref?.child(chatPath).setValue(chat.JSONRepresentation)
        
    }
    
    //MARK:- Upload Data to Storage Bucket
    
    private func upload(data : Data,forUser user : Int, mime : Mime, type chatType : ChatType, metadata : StorageMetadata, completion : @escaping (_  downloadUrl : String?) -> ())->UploadTask{
        
        let chatPath = self.getChatId(with: user) ?? .Empty//chatType == .group ? getGroupChat(with: user) : getRoom(forUser: user)
        let ref = self.storage?.reference(withPath: chatPath).child(ProcessInfo().globallyUniqueString+mime.ext)
        let uploadTask = ref?.putData(data, metadata: metadata, completion: { (metaData, error) in
            
            if error != nil ||  metaData == nil {
                
                print(" Error in uploading  ", error!.localizedDescription)
                
            } else {
                //
                //                if let image = UIImage(data: data) {  // Store the uploaded image in Cache
                //                    ref?.downloadURL(completion: { (url, error) in
                //                        completion(url?.absoluteString)
                //                        if let urlObject = url?.absoluteString {
                //                            Cache.shared.setObject(image, forKey: urlObject as AnyObject)
                //                        }
                //                    })
                //
                //                }
            }
        })
        
        return uploadTask!
        
    }
    
    func  getChatId(with userId : Int?) -> String? {
        
        guard let provider = (CacheManager.sharedInstance.getObjectInApplicationForKey(.UserId) as? Int), let userId = userId else { return nil }
        
        return userId <= provider ? "u\(userId)_p\(provider)" : "p\(provider)_u\(userId)"
        
    }
    
    //MARK:- Upload File to Storage Bucket
    
    private func upload(file url : URL,forUser user : Int, mime : Mime, type chatType : ChatType, metadata : StorageMetadata, completion : @escaping (_  downloadUrl : String?) -> ())->UploadTask{
        
        let chatPath = self.getChatId(with: user) ?? .Empty//chatType == .group ? getGroupChat(with: user) : getRoom(forUser: user)
        let ref = self.storage?.reference(withPath: chatPath).child(ProcessInfo().globallyUniqueString+mime.ext)
        let uploadTask = ref?.putFile(from: url, metadata: metadata, completion: { (metaData, error) in
            
            if error != nil || metaData == nil {
                
                print(" Error in uploading  ", error!.localizedDescription)
                
                
            } else {
                
                ref?.downloadURL(completion: { (url, error) in
                    completion(url?.absoluteString)
                })
            }
            
            
        })
        
        return uploadTask!
    }
    
}


//MARK:- Observers

extension FirebaseHelper {
    
    // Driver Location Observer
    
    func observeDriverLocation(path : String, with : EventType){
        
        self.initializeDB()
        
        self.ref!.child(path).getData { (error, snapshot) in
            let values = (snapshot.valueInExportFormat() as? [String:Any])
            if let lat = values?["latitude"] as? Double {
                NotificationCenter.default.post(name: Notification.Name("locationUpdate"), object: nil, userInfo: ["latitude":"\(lat)"])

            }
            if let lng = values?["longitude"] as? Double {
                NotificationCenter.default.post(name: Notification.Name("locationUpdate"), object: nil, userInfo: ["longitude":"\(lng)"])

            }
        }
        
        self.ref!.child(path).observe(with, with: { (snapShot) in
            print(snapShot)
            if snapShot.key == "latitude"{
                NotificationCenter.default.post(name: Notification.Name("locationUpdate"), object: nil, userInfo: ["latitude":snapShot.value ?? ""])

            }else if snapShot.key == "longitude"{
                NotificationCenter.default.post(name: Notification.Name("locationUpdate"), object: nil, userInfo: ["longitude":snapShot.value ?? ""])

            }
        })
        
    }
    // Observe if any value changes
    
    func observe(path : String, with : EventType, value : @escaping ([ChatResponse])->())->UInt {
        
        self.initializeDB()
        
        return self.ref!.child(path).observe(with, with: { (snapShot) in
            
            value(self.getModal(from: snapShot))
            
        })
        
        
        
    }
    
    // Remove Firebase Observers
    func remove(observers : [UInt]){
        
        self.initializeDB()
        
        for observer in observers {
            
            self.ref?.removeObserver(withHandle: observer)
            
        }
        
    }
    
    func removeObserversWithPath(_ path: String, observers: [UInt]) {
        self.initializeDB()
        for observer in observers {
            self.ref?.child(path).queryOrdered(byChild: "receive_on").removeObserver(withHandle: observer)
        }
    }
    
    func removeAllObservers() {
        self.ref?.removeAllObservers()
    }
    
    // Observe Last message
    
    func observeLastMessage(path : String, with : EventType, value : @escaping ([ChatResponse])->())->UInt {
        
        self.initializeDB()
        
        return self.ref!.child(path).queryLimited(toLast: 1).observe(with, with: { (snapShot) in
            
            value(self.getModal(from: snapShot))
            
        })
        
    }
    
    // Get Values From SnapShot
    
    private func getModal(from snapShot : SnapShot)->[ChatResponse]{
        
        var chatArray = [ChatResponse]()
        var response : ChatResponse?
        var chat : ChatEntity?
        
        if let snaps = snapShot.valueInExportFormat() as? [String : NSDictionary] {
            
            for snap in snaps {
                
                self.getChatEntity(with: &response, chat: &chat, snap: snap)
                chatArray.append(response!)
                
            }
            
        } else if let snaps = snapShot.value as? NSDictionary {
            
            self.getChatEntity(with: &response, chat: &chat, snap: (key: snapShot.key , value: snaps))
            chatArray.append(response!)
        }
        
        
        return chatArray.sorted(by: { (obj1, obj2) -> Bool in
            return (Int64(String.removeNil(obj1.response?.receive_on)) ?? 0) < (Int64(String.removeNil(obj2.response?.receive_on)) ?? 0)
        })
    }
    
    private func getChatEntity( with response : inout ChatResponse?, chat : inout ChatEntity?,snap : (key : String, value : NSDictionary)){
        
        response = ChatResponse()
        chat = ChatEntity()
        
        response?.key = snap.key
        
        chat?.read_on = snap.value.value(forKey: FirebaseConstants.main.read_on) as? String
        chat?.receiver_id = snap.value.value(forKey: FirebaseConstants.main.receiver_id) as? String
        chat?.sender_id = snap.value.value(forKey: FirebaseConstants.main.sender_id) as? String
        chat?.receive_on = snap.value.value(forKey: FirebaseConstants.main.receive_on) as? String
        chat?.sent_on = snap.value.value(forKey: FirebaseConstants.main.sent_on) as? String
        chat?.type = snap.value.value(forKey: FirebaseConstants.main.type) as? String
        chat?.message_id = snap.value.value(forKey: FirebaseConstants.main.message_id) as? String
        chat?.message = snap.value.value(forKey: FirebaseConstants.main.message) as? String
        chat?.ride_id = snap.value.value(forKey: FirebaseConstants.main.ride_id) as? String
        //        chat?.status = snap.value.value(forKey: FirebaseConstants.main.status) as? String
        
        response?.response = chat
        
    }
    
}

extension NSObject {
    func getUTCCurrentTimeMillis() -> Int64 {
        return Int64(Date().timeIntervalSince1970)
    }
}
