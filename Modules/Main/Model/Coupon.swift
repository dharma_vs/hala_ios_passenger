//
//  Coupon.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 09/03/22.
//

import Foundation

struct Coupon: JSONSerializable {
    let id: Int?
    let message: String?
    let code: String?
    let offer_type: String?
    let offer: Int?
    let min_ride_amount: Int?
    let expiry: String?
}
