//
//  Category.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
//

import Foundation

class CategoryReq: JSONSerializable {
    var city: String?
    var city2: String?
    var s_latitude: String?
    var s_longitude: String?
    var d_latitude: String?
    var d_longitude: String?
    var distance: Double?
    var time: Int?
    var vehicle_type: String?
    var category: Int?
    var rate_card_type: String?
    var weight: Int?
    
    init() {
        city = nil
        city2 = nil
        s_latitude = nil
        s_longitude = nil
        d_latitude = nil
        d_longitude = nil
        distance = nil
        time = nil
        vehicle_type = nil
        category = nil
        weight = nil
        rate_card_type = nil
    }
}

struct Category: JSONSerializable {
    let id: Int?
    let displayName, cityName, eta: String?
    var distance: String?
    let rideLaterEnabled, serviceType, capacity, image: String?, selectedImage: String?
    let cabMarker, message: String?
    let fareBreakup: FareBreakup?
    var rideEstimate: RideEstimate?
    var nearByCabs: [NearByCab]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case displayName = "display_name"
        case cityName = "city_name"
        case eta, distance
        case rideLaterEnabled = "ride_later_enabled"
        case serviceType = "service_type"
        case capacity, image
        case selectedImage = "selected_image"
        case cabMarker = "cab_marker"
        case message
        case fareBreakup = "fare_breakup"
        case rideEstimate = "ride_estimate"
        case nearByCabs = "near_by_cabs"
    }
    
    init() {
        id = nil
        displayName = nil
        cityName = nil
        eta = nil
        distance = nil
        rideLaterEnabled = nil
        serviceType = nil
        capacity = nil
        image = nil
        selectedImage = nil
        cabMarker = nil
        message = nil
        fareBreakup = nil
        rideEstimate = nil
        nearByCabs = []
    }
}



extension Category: Equatable {
    static func == (lhs: Category, rhs: Category) -> Bool {
        return (lhs.nearByCabs?.count ?? 0) == (rhs.nearByCabs?.count ?? 0)
    }
    
    static func hasChanges(_ lhs: [Category], rhs: [Category]) -> Bool {
        return !(lhs == rhs)
    }
}


struct FareBreakup: JSONSerializable {
    let id: Int?
    let type: String?
    let minimumDistance, minimumTime, baseFare, minimumFare: Double?
    let costPerDistance: Double?
    let waitingCostPerMinute, rideCostPerMinute: Double?
    let freeWaitingTime: Int?
    let waitingPricePerMin: Double?
    let nightChargesType: String?
    let nightCharges: Double?
    let peakChargesType: String?
    let peakCharges: Double?
    let surcharge: Surcharge?
    
    enum CodingKeys: String, CodingKey {
        case id
        case type
        case minimumDistance = "minimum_distance"
        case minimumTime = "minimum_time"
        case baseFare = "base_fare"
        case minimumFare = "minimum_fare"
        case costPerDistance = "cost_per_distance"
        case waitingCostPerMinute = "waiting_cost_per_minute"
        case rideCostPerMinute = "ride_cost_per_minute"
        case freeWaitingTime = "free_waiting_time"
        case waitingPricePerMin = "waiting_price_per_min"
        case nightChargesType = "night_charges_type"
        case nightCharges = "night_charges"
        case peakChargesType = "peak_charges_type"
        case peakCharges = "peak_charges"
        case surcharge
    }
}

struct Surcharge: JSONSerializable {
    let message, multiply: String?
}

struct NearByCab: JSONSerializable {
    var driverID: Int?
    var driverName: String?
    var lat, lng: Double?
    var deviceID, token, lastUpdated: String?
    
    var coordinate: LocationCoordinate {
        return LocationCoordinate(latitude: self.lat ?? 0.0, longitude: self.lng ?? 0.0)
    }
    
    enum CodingKeys: String, CodingKey {
        case driverID = "driver_id"
        case driverName = "driver_name"
        case lat, lng
        case deviceID = "device_id"
        case token, lastUpdated
    }
}

struct RideEstimate: JSONSerializable {
    var distance: String?
    var travelTimeInMinutes: Int?
    let estimatedFare, surge: Int?
    let taxes: Taxes?
    
    enum CodingKeys: String, CodingKey {
        case distance
        case travelTimeInMinutes = "travel_time_in_minutes"
        case estimatedFare = "estimated_fare"
        case surge, taxes
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if let distance = try? values.decode(Int.self, forKey: .distance) {
            self.distance = "\(distance)"
        } else if let distance = try? values.decode(String.self, forKey: .distance) {
            self.distance = distance
        }
        self.travelTimeInMinutes = try? values.decode(Int.self, forKey: .travelTimeInMinutes)
        self.estimatedFare = try? values.decode(Int.self, forKey: .estimatedFare)
        self.surge = try? values.decode(Int.self, forKey: .surge)
        self.taxes = try? values.decode(Taxes.self, forKey: .taxes)
    }
}

struct Taxes: JSONSerializable {
    let totalTax: Int?
    
    enum CodingKeys: String, CodingKey {
        case totalTax = "total_tax"
    }
}
