//
//  RideState.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 09/03/22.
//

import Foundation
var RideType:String = "2" // click ride 2 or rental = 1
var ZoneType:String = "1" //Type 1- Hala-Ride/Rental Type 2 - Taxi/Delivery
var StationID:Int = 0
var CategoryID:Int = 0
enum RideState: String {
    
    case accept = "ACCEPT"
    
    case reject = "REJECT"
    
    case arrived = "ARRIVED"
    
    case payment_completed = "PAYMENT_COMPLETED"
    
    case retry = "RETRY"
    
}
