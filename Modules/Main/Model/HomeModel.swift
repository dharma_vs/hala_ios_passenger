//
//  HomeModel.swift
//  HalaMobility
//
//  Created by ADMIN on 19/05/22.
//

import Foundation
// MARK: - Welcome
//struct FetchResponse: JSONSerializable {
//    var status: Bool?
//    var message: String?
//    var data: DataClass?
//}

// MARK: - DataClass
struct RentalRes: JSONSerializable {
    var rental: [Rental1]?
}

// MARK: - Rental
struct Rental1: JSONSerializable {
    var id: CustomType?
    var bookingID, rentalType: CustomType?
    var riderID, vehicleTypeID, subscribeID, bookingStatus: CustomType?
    var cityName, countryShortName, createdTime, bookedTime: CustomType?
    var arrivedTime, unlockReqestTime, unlockApprovalTime: CustomType?
    var endRequestTime, endApprovalTime, startEstimatedDateTime, endEstimatedDateTime: CustomType?
    var estimatedFare: CustomType?
    var startTime, datetime:String?
    var riderRating, riderAnswer, riderFeedback, cancellationDate: CustomType?
    var canceledBy: CustomType?
    var cancellationReason: CustomType?
    var cancellationRiderRead: CustomType?
    var km: CustomType?
    var unlockRequestRejectMessage, rideEndRequestRejectMessage: CustomType?
    var promoCodeID, taxAmount, commission, commTaxAmount: CustomType?
    var tripInfo: CustomType?
    var batteryMaxVoltage: Double?
    var batteryMinVoltage, batteryVoltage: CustomType?
    var fleetNo, vehicleDeviceID, availablity: CustomType?
    var customerQuery, managementSolution: CustomType?
    var odometer, runningStatus: CustomType?
    var pickupStation: Station?
    //var pickupStation: [Station]?
    var dropStation: CustomType?
    var paymentDetails: PaymentDetails1?
    var vehicleTypeInfo: VehicleDetail?
    //var vehicleTypeInfo: VehicleDetail?
    let fareBreakup: FareBreakupUnion?
    var userInfo: UserInfo1?
    var vehicleInfo: Vehiclelist?
    //var vehicleInfo: Vehiclelist?
    //var swapDetails: [SwapDetail]?
    var ridePauseTimes: [RidePauseTime]?
    var subscriptionInfo: [SubscriptionInfo]?

    enum CodingKeys: String, CodingKey {
        case id
        case bookingID = "booking_id"
        case rentalType = "rental_type"
        case riderID = "rider_id"
        case vehicleTypeID = "vehicle_type_id"
        case subscribeID = "subscribe_id"
        case bookingStatus = "booking_status"
        case cityName = "city_name"
        case countryShortName = "country_short_name"
        case createdTime = "created_time"
        case bookedTime = "booked_time"
        case arrivedTime = "arrived_time"
        case unlockReqestTime = "unlock_reqest_time"
        case unlockApprovalTime = "unlock_approval_time"
        case startTime = "start_time"
        case endRequestTime = "end_request_time"
        case endApprovalTime = "end_approval_time"
        case startEstimatedDateTime = "start_estimated_date_time"
        case endEstimatedDateTime = "end_estimated_date_time"
        case estimatedFare = "estimated_fare"
        case riderRating = "rider_rating"
        case riderAnswer = "rider_answer"
        case riderFeedback = "rider_feedback"
        case cancellationDate = "cancellation_date"
        case canceledBy = "canceled_by"
        case cancellationReason = "cancellation_reason"
        case cancellationRiderRead = "cancellation_rider_read"
        case km
        case unlockRequestRejectMessage = "unlock_request_reject_message"
        case rideEndRequestRejectMessage = "ride_end_request_reject_message"
        case promoCodeID = "promo_code_id"
        case taxAmount = "tax_amount"
        case commission
        case commTaxAmount = "comm_tax_amount"
        case tripInfo = "trip_info"
        case batteryMaxVoltage = "battery_max_voltage"
        case batteryMinVoltage = "battery_min_voltage"
        case batteryVoltage = "battery_voltage"
        case fleetNo = "fleet_no"
        case vehicleDeviceID = "vehicle_device_id"
        case availablity
        case customerQuery = "customer_query"
        case managementSolution = "management_solution"
        case odometer
        case runningStatus = "running_status"
        case pickupStation = "pickup_station"
        case dropStation = "drop_station"
        case datetime
        case fareBreakup = "fare_breakup"
        case paymentDetails = "payment_details"
        case vehicleTypeInfo = "vehicle_type_info"
        case userInfo = "user_info"
        case vehicleInfo = "vehicle_info"
       // case swapDetails = "swap_details"
        case ridePauseTimes = "ride_pause_times"
        case subscriptionInfo = "subscription_info"
    }
}
enum FareBreakupUnion: Codable {
    case fareBreakupClass(FareBreakupClass)
    case string(String)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        if let x = try? container.decode(FareBreakupClass.self) {
            self = .fareBreakupClass(x)
            return
        }
        throw DecodingError.typeMismatch(FareBreakupUnion.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for FareBreakupUnion"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .fareBreakupClass(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}
// MARK: - PaymentDetails
struct PaymentDetails1: JSONSerializable {
    var payBy: CustomType?
    var cashPayment, cardPayment, walletPayment: CustomType?

    enum CodingKeys: String, CodingKey {
        case payBy = "pay_by"
        case cashPayment = "cash_payment"
        case cardPayment = "card_payment"
        case walletPayment = "wallet_payment"
    }
}

// MARK: - PickupStation
struct PickupStation: JSONSerializable {
    var id: CustomType?
    var name, address, image, marker: CustomType?
    var latitude, longitude: CustomType?
}

// MARK: - RidePauseTime
struct RidePauseTime: JSONSerializable {
    var id: CustomType?
    var startDate, endDate: CustomType?
    var minute: CustomType?

    enum CodingKeys: String, CodingKey {
        case id
        case startDate = "start_date"
        case endDate = "end_date"
        case minute
    }
}

// MARK: - SubscriptionInfo
struct SubscriptionInfo: JSONSerializable {
    var id, subscriptionID: CustomType?
    var rsubscriptionID: CustomType?
    var subscriptionURL: CustomType?
    var packageDetails: CustomType?
    var totalAmount: CustomType?
    var totalTax: CustomType?
    var subscribeAt, expiryAt, monthStartAt, monthEndAt: CustomType?
    var amount: CustomType?
    var tax: CustomType?
    var transactionID, paymentAt, status: CustomType?

    enum CodingKeys: String, CodingKey {
        case id
        case subscriptionID = "subscription_id"
        case rsubscriptionID = "rsubscription_id"
        case subscriptionURL = "subscription_url"
        case packageDetails = "package_details"
        case totalAmount = "total_amount"
        case totalTax = "total_tax"
        case subscribeAt = "subscribe_at"
        case expiryAt = "expiry_at"
        case monthStartAt = "month_start_at"
        case monthEndAt = "month_end_at"
        case amount, tax
        case transactionID = "transaction_id"
        case paymentAt = "payment_at"
        case status
    }
}

// MARK: - SwapDetail
struct SwapDetail: JSONSerializable {
    var id, rentalID, oldVehicleID, newVehicleID: CustomType?
    var reason, swappedAt, oldVehicleNumber, newVehicleNumber: CustomType?
    var oldFleetNo, newFleetNo: CustomType?

    enum CodingKeys: String, CodingKey {
        case id
        case rentalID = "rental_id"
        case oldVehicleID = "old_vehicle_id"
        case newVehicleID = "new_vehicle_id"
        case reason
        case swappedAt = "swapped_at"
        case oldVehicleNumber = "old_vehicle_number"
        case newVehicleNumber = "new_vehicle_number"
        case oldFleetNo = "old_fleet_no"
        case newFleetNo = "new_fleet_no"
    }
}

// MARK: - UserInfo
struct UserInfo1: JSONSerializable {
    var id: CustomType?
    var name, email, gender, mobile: CustomType?
    var ccp, image, deviceType, rating: CustomType?

    enum CodingKeys: String, CodingKey {
        case id, name, email, gender, mobile, ccp, image
        case deviceType = "device_type"
        case rating
    }
}
// MARK: - FareBreakup
struct FareBreakupClass: JSONSerializable {
    let fareBreakupID: String?
    let unlockFare, baseFare, pauseCharges, pauseMins: CustomType?
    let minuteFare, hourFare, dayFare, weekFare: CustomType?
    let monthFare: CustomType?
    let noMinute, noHour, noDay, noWeek: String?
    let noMonth: String?
    let rideFare: CustomType?
    let roundOff: CustomType?
    let taxLabel, taxPercentage: String?
    let taxAmount: CustomType?
    let totalFare, cancellationCharge: CustomType?

    enum CodingKeys: String, CodingKey {
        case fareBreakupID = "fare_breakup_id"
        case unlockFare = "unlock_fare"
        case baseFare = "base_fare"
        case pauseCharges = "pause_charges"
        case pauseMins = "pause_mins"
        case minuteFare = "minute_fare"
        case hourFare = "hour_fare"
        case dayFare = "day_fare"
        case weekFare = "week_fare"
        case monthFare = "month_fare"
        case noMinute = "no_minute"
        case noHour = "no_hour"
        case noDay = "no_day"
        case noWeek = "no_week"
        case noMonth = "no_month"
        case rideFare = "ride_fare"
        case roundOff = "round_off"
        case taxLabel = "tax_label"
        case taxPercentage = "tax_percentage"
        case taxAmount = "tax_amount"
        case totalFare = "total_fare"
        case cancellationCharge = "cancellation_charge"
    }
}

// MARK: - VehicleInfo
struct VehicleInfo1: JSONSerializable {
    var id: CustomType?
    var make, number, fleetNo, model: String?
    var color, year, deviceID, deviceKey: CustomType?
    var deviceLatitude, deviceLongitude: CustomType?
    var batteryMaxVoltage: Double?
    var batteryMinVoltage, batteryVoltage: Double?
    var odometer, availablity, runningStatus, features: CustomType?
    var vehicleDocument, vehicleInsurance, vehiclePhotoFront, vehiclePhotoRear: CustomType?

    enum CodingKeys: String, CodingKey {
        case id, make, number
        case fleetNo = "fleet_no"
        case model, color, year
        case deviceID = "device_id"
        case deviceKey = "device_key"
        case deviceLatitude = "device_latitude"
        case deviceLongitude = "device_longitude"
        case batteryMaxVoltage = "battery_max_voltage"
        case batteryMinVoltage = "battery_min_voltage"
        case batteryVoltage = "battery_voltage"
        case odometer, availablity
        case runningStatus = "running_status"
        case features
        case vehicleDocument = "vehicle_document"
        case vehicleInsurance = "vehicle_insurance"
        case vehiclePhotoFront = "vehicle_photo_front"
        case vehiclePhotoRear = "vehicle_photo_rear"
    }
}

/* MARK: - VehicleTypeInfo
struct VehicleTypeInfo1: JSONSerializable {
    var id: CustomType?
    var displayName,image: String?
    var  capacity, cabMarker: CustomType?

    enum CodingKeys: String, CodingKey {
        case id
        case displayName = "display_name"
        case capacity, image
        case cabMarker = "cab_marker"
    }
}
struct FetchResponse: JSONSerializable {
    let status: CustomType?
    let message: String?
    let data: DataClass1
}

// MARK: - DataClass
struct DataClass1: JSONSerializable {
    let rental: [Rental1]
}

// MARK: - Rental
struct Rental1: JSONSerializable {
    var id: CustomType?
    var bookingID, rentalType: CustomType?
    var riderID, vehicleTypeID, subscribeID, bookingStatus: CustomType?
    let cityName, countryShortName, createdTime, bookedTime: String?
    let arrivedTime, unlockReqestTime, unlockApprovalTime, startTime: CustomType?
    let endRequestTime, endApprovalTime, startEstimatedDateTime, endEstimatedDateTime: CustomType?
    let estimatedFare: CustomType?
    let riderRating, riderAnswer, riderFeedback, cancellationDate: CustomType?
    let canceledBy: CustomType?
    let cancellationReason: CustomType?
    let cancellationRiderRead: CustomType?
    let km: CustomType?
    let unlockRequestRejectMessage, rideEndRequestRejectMessage: CustomType?
    let promoCodeID, taxAmount, commission, commTaxAmount: CustomType?
    let tripInfo: CustomType?
    let batteryMaxVoltage: CustomType?
    let batteryMinVoltage, batteryVoltage: CustomType?
    let fleetNo, vehicleDeviceID, availablity: CustomType?
    let customerQuery, managementSolution: CustomType?
    let odometer, runningStatus: CustomType?
    let pickupStation: PickupStation?
    let dropStation, datetime, fareBreakup: CustomType?
    let paymentDetails: PaymentDetails1? 
    let vehicleTypeInfo: VehicleTypeInfo1?
    let userInfo: UserInfo1?
    let vehicleInfo: VehicleInfo1?
    let swapDetails: [SwapDetail]?
    let ridePauseTimes: [RidePauseTime]?
    let subscriptionInfo: [SubscriptionInfo]?

    enum CodingKeys: String, CodingKey {
        case id
        case bookingID = "booking_id"
        case rentalType = "rental_type"
        case riderID = "rider_id"
        case vehicleTypeID = "vehicle_type_id"
        case subscribeID = "subscribe_id"
        case bookingStatus = "booking_status"
        case cityName = "city_name"
        case countryShortName = "country_short_name"
        case createdTime = "created_time"
        case bookedTime = "booked_time"
        case arrivedTime = "arrived_time"
        case unlockReqestTime = "unlock_reqest_time"
        case unlockApprovalTime = "unlock_approval_time"
        case startTime = "start_time"
        case endRequestTime = "end_request_time"
        case endApprovalTime = "end_approval_time"
        case startEstimatedDateTime = "start_estimated_date_time"
        case endEstimatedDateTime = "end_estimated_date_time"
        case estimatedFare = "estimated_fare"
        case riderRating = "rider_rating"
        case riderAnswer = "rider_answer"
        case riderFeedback = "rider_feedback"
        case cancellationDate = "cancellation_date"
        case canceledBy = "canceled_by"
        case cancellationReason = "cancellation_reason"
        case cancellationRiderRead = "cancellation_rider_read"
        case km
        case unlockRequestRejectMessage = "unlock_request_reject_message"
        case rideEndRequestRejectMessage = "ride_end_request_reject_message"
        case promoCodeID = "promo_code_id"
        case taxAmount = "tax_amount"
        case commission
        case commTaxAmount = "comm_tax_amount"
        case tripInfo = "trip_info"
        case batteryMaxVoltage = "battery_max_voltage"
        case batteryMinVoltage = "battery_min_voltage"
        case batteryVoltage = "battery_voltage"
        case fleetNo = "fleet_no"
        case vehicleDeviceID = "vehicle_device_id"
        case availablity
        case customerQuery = "customer_query"
        case managementSolution = "management_solution"
        case odometer
        case runningStatus = "running_status"
        case pickupStation = "pickup_station"
        case dropStation = "drop_station"
        case datetime
        case fareBreakup = "fare_breakup"
        case paymentDetails = "payment_details"
        case vehicleTypeInfo = "vehicle_type_info"
        case userInfo = "user_info"
        case vehicleInfo = "vehicle_info"
        case swapDetails = "swap_details"
        case ridePauseTimes = "ride_pause_times"
        case subscriptionInfo = "subscription_info"
    }
}

// MARK: - PaymentDetails
struct PaymentDetails1: JSONSerializable {
    let payBy: String
    let cashPayment, cardPayment, walletPayment: Int

    enum CodingKeys: String, CodingKey {
        case payBy = "pay_by"
        case cashPayment = "cash_payment"
        case cardPayment = "card_payment"
        case walletPayment = "wallet_payment"
    }
}

// MARK: - PickupStation
struct PickupStation: JSONSerializable {
    let id: Int?
    let name, address, image, marker: String?
    let latitude, longitude: Double?
}

// MARK: - RidePauseTime
struct RidePauseTime: JSONSerializable {
    let id: Int?
    let startDate, endDate: String?
    let minute: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case startDate = "start_date"
        case endDate = "end_date"
        case minute
    }
}

// MARK: - SubscriptionInfo
struct SubscriptionInfo: JSONSerializable {
    let id, subscriptionID: Int
    let rsubscriptionID: String
    let subscriptionURL: String
    let packageDetails: String
    let totalAmount: Int
    let totalTax: Double
    let subscribeAt, expiryAt, monthStartAt, monthEndAt: String
    let amount: Int
    let tax: Double
    let transactionID, paymentAt, status: String

    enum CodingKeys: String, CodingKey {
        case id
        case subscriptionID = "subscription_id"
        case rsubscriptionID = "rsubscription_id"
        case subscriptionURL = "subscription_url"
        case packageDetails = "package_details"
        case totalAmount = "total_amount"
        case totalTax = "total_tax"
        case subscribeAt = "subscribe_at"
        case expiryAt = "expiry_at"
        case monthStartAt = "month_start_at"
        case monthEndAt = "month_end_at"
        case amount, tax
        case transactionID = "transaction_id"
        case paymentAt = "payment_at"
        case status
    }
}

// MARK: - SwapDetail
struct SwapDetail: JSONSerializable {
    let id, rentalID, oldVehicleID, newVehicleID: Int
    let reason, swappedAt, oldVehicleNumber, newVehicleNumber: String
    let oldFleetNo, newFleetNo: String

    enum CodingKeys: String, CodingKey {
        case id
        case rentalID = "rental_id"
        case oldVehicleID = "old_vehicle_id"
        case newVehicleID = "new_vehicle_id"
        case reason
        case swappedAt = "swapped_at"
        case oldVehicleNumber = "old_vehicle_number"
        case newVehicleNumber = "new_vehicle_number"
        case oldFleetNo = "old_fleet_no"
        case newFleetNo = "new_fleet_no"
    }
}

// MARK: - UserInfo
struct UserInfo1: JSONSerializable {
    let id: Int
    let name, email, gender, mobile: String
    let ccp, image, deviceType, rating: String

    enum CodingKeys: String, CodingKey {
        case id, name, email, gender, mobile, ccp, image
        case deviceType = "device_type"
        case rating
    }
}

// MARK: - VehicleInfo
struct VehicleInfo1: JSONSerializable {
    let id: Int
    let make, number, fleetNo, model: String
    let color, year, deviceID, deviceKey: String
    let deviceLatitude, deviceLongitude: String
    let batteryMaxVoltage: Double
    let batteryMinVoltage, batteryVoltage: Int
    let odometer, availablity, runningStatus, features: String
    let vehicleDocument, vehicleInsurance, vehiclePhotoFront, vehiclePhotoRear: String

    enum CodingKeys: String, CodingKey {
        case id, make, number
        case fleetNo = "fleet_no"
        case model, color, year
        case deviceID = "device_id"
        case deviceKey = "device_key"
        case deviceLatitude = "device_latitude"
        case deviceLongitude = "device_longitude"
        case batteryMaxVoltage = "battery_max_voltage"
        case batteryMinVoltage = "battery_min_voltage"
        case batteryVoltage = "battery_voltage"
        case odometer, availablity
        case runningStatus = "running_status"
        case features
        case vehicleDocument = "vehicle_document"
        case vehicleInsurance = "vehicle_insurance"
        case vehiclePhotoFront = "vehicle_photo_front"
        case vehiclePhotoRear = "vehicle_photo_rear"
    }
}

// MARK: - VehicleTypeInfo
struct VehicleTypeInfo1: JSONSerializable {
    let id: Int
    let displayName, capacity, image, cabMarker: String

    enum CodingKeys: String, CodingKey {
        case id
        case displayName = "display_name"
        case capacity, image
        case cabMarker = "cab_marker"
    }
}
*/
