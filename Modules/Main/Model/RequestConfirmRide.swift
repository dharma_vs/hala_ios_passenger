//
//  RequestConfirmRide.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
//

import Foundation

enum RideCategory: Int {
    case  Ride = 1, Rental = 2, StreetPickup = 3, Share = 4, Scheduled = 5, Delivery = 6
    
    static var all: [RideCategory] {
        return [.Ride, .Rental, .StreetPickup, .Share]
    }
    
    var stringValue: String {
        switch self {
        case .Ride:
            return "Normal Ride".localized
        case .Scheduled:
            return "Schedule Ride".localized
        case .Rental:
            return "Rental Ride".localized
        case .StreetPickup:
            return "Street Pickup".localized
        case .Share:
            return "Share".localized
        case .Delivery:
            return "Delivery".localized
        }
    }
    
    static func rideCategoriesFrom(_ iDs: String?) -> [RideCategory] {
        var rideCategories = [RideCategory]()
        for idStr in (iDs?.components(separatedBy: ",") ?? []) {
            if let id = Int(idStr), let category = RideCategory.init(rawValue: id) {
                rideCategories.append(category)
            }
        }
        return rideCategories
    }
}

class RequestConfirmRide: JSONSerializable {
    var category: String
    var vehicle_type: Int
    var s_name: String = ""
    var s_address: String
    var s_latitude: Double
    var s_longitude: Double
    var d_name: String = ""
    var d_address: String
    var d_latitude: Double
    var d_longitude: Double
    var category_id: Int
    var package_id: String?
    var package_name: String?
    var map_root: String
    var city: String
    var payment_method,rejected_count: Int //1) CASH, 2) CARD & 3) WALLET
    var promo_code_id: String
    var ride_type: String //Now or Later
    var ride_datetime: String//2019-03-06 12:07:00
    var estimated_distance: String
    var estimated_duration: String
    var estimated_fare: Double
    var country_short_name: String
    var country_full_name: String
    var trip_info: String
    var rider_id, driver_id: String
    var card_id :String
    var booking_for_name, booking_for_phone: String?
    var search_latitude: Double
    var search_longitude: Double
    var ride_pickup_datetime, ride_return_datetime: String?
    init() {
        rider_id = ""
        driver_id = ""
        package_id = ""
        package_name = ""
        category_id = 0
        booking_for_name = ""
        booking_for_phone = ""
        vehicle_type = 0
        s_name = ""
        s_address = ""
        s_latitude = 0.0
        s_longitude = 0.0
        d_name = ""
        d_address = ""
        d_latitude = 0.0
        d_longitude = 0.0
        city = ""
        map_root = ""
        payment_method = 0 //1) C
        promo_code_id = ""
        ride_type = "" //Now or
        ride_datetime = ""//201
        estimated_distance = ""
        estimated_duration = ""
        estimated_fare = 0
        country_short_name = ""
        country_full_name = ""
        trip_info = ""//fare_br
        category = ""
        card_id = ""
        rejected_count = 0
        search_latitude = 0.0
        search_longitude = 0.0
        ride_pickup_datetime = ""
        ride_return_datetime = ""
    }
}

struct RentalListReq: JSONSerializable {
    var city, city2, s_latitude, s_longitude: String?
}

struct RentalDetailReq: JSONSerializable {
    var city, package_id, s_latitude, s_longitude: String?
}

struct RentalCategory: JSONSerializable {
    var rentalID, carType: Int?
    var displayName, cityName, eta, distance: String?
    var rideLaterEnabled, capacity, image, cabMarker, selectedImage: String?
    var baseFare, costPerDistance: CustomType?
    var rideCostPerMinute: Double?
    var message: String?
    var nearByCabs: [NearByCab]?
    var package_id: CustomType?
    var package_name: CustomType?
    var waiting_price_per_min: CustomType?
    var minimum_time: CustomType?
    var minimum_distance: CustomType?
    
    enum CodingKeys: String, CodingKey {
        case rentalID = "rental_id"
        case carType = "car_type"
        case displayName = "display_name"
        case cityName = "city_name"
        case eta, distance
        case rideLaterEnabled = "ride_later_enabled"
        case capacity, image
        case cabMarker = "cab_marker"
        case baseFare = "base_fare"
        case costPerDistance = "cost_per_distance"
        case rideCostPerMinute = "ride_cost_per_minute"
        case message
        case nearByCabs = "near_by_cabs"
        case selectedImage = "selectedImage"
        case package_id
        case package_name
        case waiting_price_per_min
        case minimum_time
        case minimum_distance
    }
}

struct confirmRentalReq: JSONSerializable {
    var rental_type, pickup_station_id, pickup_station_name, city, country_short_name, category_id, fare_breakup, unlock_fee, base_fee, base_time, minute_fare, hour_fare, day_fare, week_fare, month_fare, net_fare, pickup_date_time, drop_date_time, estimated_fare, promo_code_id, subscribe_id: String?
    var vehicle_id:Int?
}
