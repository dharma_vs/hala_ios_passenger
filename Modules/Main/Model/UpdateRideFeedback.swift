//
//  UpdateRideModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 05/03/22.
//

import Foundation

func updateRideFeedback(_ ride: RideDetail?, feedBack: FeedbackInfo, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
    let rideId = "\(ride?.id ?? 0)"
    let driver_id = "\(ride?.driver_id ?? 0)"
    var feedback = feedBack
    feedback.driver_id = "\(ride?.driver_id ?? 0)"
    self.requestEndPoint(DataServiceEndpoint.updateFeedback, withParameters: nil, withBody: feedback.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: (rideId /* + "-" + driver_id*/) as AnyObject?) { (inner, header) in
        do {
            let data = try inner()
            if let status = data?.getDecodedObject(from: GeneralResponse<CommonResponse>.self)?.data {
                completion({ return status })
            }
        } catch {
            completion({throw error})
        }
    }
}
