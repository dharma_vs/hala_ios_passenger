//
//  VehicledetailModel.swift
//  HalaMobility
//
//  Created by ADMIN on 08/06/22.
//

import Foundation

struct VehicledetRes: Codable {
    let status: Bool
    let message: String
    let data: [VehicleDetail]
}

// MARK: - Datum
struct VehicleDetail: Codable {
    let id: Int
    let displayName,  image: String
    let cabMarker: String
    let sort, min_deposit, max_deposit, no_vechicles: Int?
    let city_name, selected_image: String?
    let message: String?
    let fareBreakup: VehilceFareBreakup?

    enum CodingKeys: String, CodingKey {
        case id
        case city_name
        case selected_image
        case displayName = "display_name"
        case image
        case cabMarker = "cab_marker"
        case sort
        case min_deposit
        case max_deposit
        //case minDeposit = "min_deposit"
        //case maxDeposit = "max_deposit"
        case no_vechicles = "no_vechicles"
        case message
        case fareBreakup = "fare_breakup"
    }
}

// MARK: - FareBreakup
struct VehilceFareBreakup: Codable {
    let fareBreakupID, unlockFee, baseFee, baseTime: Int
    let pauseCharges: Double
    let minute, hour, day, week: Int
    let month: Int
    let subscriptionPackageIDS: String

    enum CodingKeys: String, CodingKey {
        case fareBreakupID = "fare_breakup_id"
        case unlockFee = "unlock_fee"
        case baseFee = "base_fee"
        case baseTime = "base_time"
        case pauseCharges = "pause_charges"
        case minute, hour, day, week, month
        case subscriptionPackageIDS = "subscription_package_ids"
    }
}
