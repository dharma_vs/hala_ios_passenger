//
//  CallSOSReqInfo.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import Foundation

struct callSOSReqInfo: JSONSerializable {
    var ride_id: String!
    var lat: String!
    var lng: String!
}
