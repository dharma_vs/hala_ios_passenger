//
//  VehiclelistModel.swift
//  HalaMobility
//
//  Created by ADMIN on 24/05/22.
//

import Foundation
// MARK: - Welcome
struct VehiclelistRes: Codable {
    let status: Bool
    let message: String
    let data: [Vehiclelist]
}

// MARK: - Datum
struct Vehiclelist: JSONSerializable {
    let vehicleID,id: Int?
    let device_id,vehicleDeviceID, device_key, features, make, model, number, vehicleNumber, brand : String?
    let color, year,fleetNo: String
    let stationType, status, chasisNumber, stationCity: String?
    let displayName, image, cabMarker, driverCarModel: String?
    let vehicleDocument, vehicleInsurance, vehiclePhotoFront, vehiclePhotoRear: String?
    let deviceLatitude, deviceLongitude: String?
    let categoryID, stationID: Int?
    let availablity: String
    let batteryMaxVoltage, batteryMinVoltage, batteryVoltage: Double
    let odometer: String
    let mileage: Int
    let runningStatus: String

    enum CodingKeys: String, CodingKey {
        case device_key,features, make, model, number
        case vehicleID = "vehicle_id"
        case id = "id"
        case vehicleDeviceID = "vehicle_device_id"
        case device_id = "device_id"
        case vehicleNumber = "vehicle_number"
        case fleetNo = "fleet_no"
        case brand, color, year
        case chasisNumber = "chasis_number"
        case stationCity = "station_city"
        case displayName = "display_name"
        case image
        case cabMarker = "cab_marker"
        case driverCarModel = "driver_car_model"
        case vehicleDocument = "vehicle_document"
        case vehicleInsurance = "vehicle_insurance"
        case vehiclePhotoFront = "vehicle_photo_front"
        case vehiclePhotoRear = "vehicle_photo_rear"
        case deviceLatitude = "device_latitude"
        case deviceLongitude = "device_longitude"
        case categoryID = "category_id"
        case stationID = "station_id"
        case stationType = "station_type"
        case status, availablity
        case batteryMaxVoltage = "battery_max_voltage"
        case batteryMinVoltage = "battery_min_voltage"
        case batteryVoltage = "battery_voltage"
        case odometer, mileage
        case runningStatus = "running_status"
    }
    
}
