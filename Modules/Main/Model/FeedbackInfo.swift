//
//  FeedbackInfo.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
//

import Foundation

struct FeedbackInfo: JSONSerializable {
    var rating : Float!
    var feedback: String!
    var driver_id: String!
    var answer: String!
}
