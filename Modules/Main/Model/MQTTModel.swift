//
//  MQTTModel.swift
//  HalaMobility
//
//  Created by ADMIN on 27/07/22.
//

import Foundation

// MARK: - Welcome
struct MqttRes: Codable {
    let status: Bool
    let message: String
    let data: [MQTT]
}

// MARK: - Datum
struct MQTT: Codable {
    let fid: String
    let engineIgnitionStatus: Bool
    let positionSpeed: Int
    let batteryVoltage, positionLatitude, positionLongitude: Double

    enum CodingKeys: String, CodingKey {
        case fid
        case engineIgnitionStatus = "engine.ignition.status"
        case positionSpeed = "position.speed"
        case batteryVoltage = "battery.voltage"
        case positionLatitude = "position.latitude"
        case positionLongitude = "position.longitude"
    }
}
