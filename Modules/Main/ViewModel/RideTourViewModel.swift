//
//  RideTourViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 07/03/22.
//

import Foundation

class RideTourViewModel: AppViewModel {
    weak var controller: RideTourViewController!
    required init(controller: RideTourViewController) {
        self.controller = controller
    }
}

extension ServiceManager{
    
    func confirmRide(_ info: RequestConfirmRide, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        self.request(endPoint: MainDataServiceEndpoint.confirmRide, body: info.JSONRepresentation as Dictionary<String,Any>) { result in
            
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: CommonRes.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func rentalList(_ info: RentalListReq, completion: @escaping (_ inner: () throws ->  [Package]) -> (Void)) {
        
        self.request(endPoint: MainDataServiceEndpoint.rentalList, parameters: info.JSONRepresentation as Dictionary<String,Any>) { result in
            
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: GeneralResponse<[Package]>.self)
                if let rentalList = response?.data, response?.status == true {
                    completion({ return rentalList })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }

    func rentalDetails(_ info: RentalDetailReq, completion: @escaping (_ inner: () throws ->  [RentalCategory]) -> (Void)) {
        
        self.request(endPoint: MainDataServiceEndpoint.rentalDetails, parameters: info.JSONRepresentation as Dictionary<String,Any>) { result in
            
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: GeneralResponse<[RentalCategory]>.self)
                if let rentalList = response?.data, response?.status == true {
                    completion({ return rentalList })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}


