//
//  RideCheckHelperViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 09/03/22.
//

import Foundation


extension ServiceManager {
    
    func checkCurrentRideStatus(_ completion: @escaping (_ inner: () throws ->  RideList?) -> (Void)) {
        
        self.request(endPoint: MainDataServiceEndpoint.checkCurrentRideStatus) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: RideList.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
