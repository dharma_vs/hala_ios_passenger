//
//  PaymentActionViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
//

import Foundation

class PaymentActionViewModel: NSObject {
    
    let souceAddress: String?
    let dropAddress: String?
    
    let fare: String?
    let distance: String?
    let travelTime: String?
    
    let rideStatus: RideStatus!
    
    let userName: String?
    let userImageUrl: URL!
    
    let vehicleName: String?
    
    var detail: RideDetail
    
    init(rideDetail: RideDetail) {
        
        self.detail = rideDetail
        
        self.rideStatus = RideStatus.init(rawValue: rideDetail.booking_status ?? 0)
        
        self.souceAddress = rideDetail.actual_address?.source?.address
        
        self.dropAddress = rideDetail.actual_address?.destination?.address
        
        self.fare =  Extension.updateCurrecyRate(fare: "\(String(rideDetail.trip_info?.payable_amount ?? 0))").currencyValue

        self.distance = rideDetail.trip_info?.distance?.distanceValue ?? ""
        
        self.travelTime = "\(rideDetail.trip_info?.trip_time ?? 0)".timeValue
        
        self.userName = rideDetail.driver_info?.name
        
        self.userImageUrl = rideDetail.driver_info?.image?.urlFromString
        
        self.vehicleName = rideDetail.vehicle_info?.model
        
    }
    
    func updatePaymentActionView(_ view: PaymentActionView) {
        
        view.packageView?.isHidden = self.detail.rental == nil
        view.packageNameLbl?.text = self.detail.rental?.packageName?.stringValue
        view.startReadingLbl?.text = self.detail.rental?.startReading?.stringValue
        
        view.addressContentView?.sourceAddressLbl?.text = self.souceAddress
        view.addressContentView?.destinationAddressLbl?.text = self.dropAddress
        view.rideDistanceInfoView?.distanceLbl?.text = "Distance".localized
        view.rideDistanceInfoView?.durationLbl?.text = "Duration".localized
        view.rideDistanceInfoView?.distanceValue?.text = self.distance
        view.rideDistanceInfoView?.durationValue?.text = self.travelTime
        view.rideUserInfoView?.userImage.hnk_setImage(from: self.userImageUrl, placeholder: nil, success: { (image) in
            view.rideUserInfoView?.userImage?.image = image
        }, failure: nil)
        view.rideUserInfoView?.userName?.text = self.userName
        view.rideUserInfoView?.vehicleName?.text = self.vehicleName
        view.rideFareLbl?.text = self.fare
        view.changePaymentToCash?.isHidden = !(self.detail.payment_mode == "2")
        view.totalFareValueLbl?.text = Extension.updateCurrecyRate(fare: "\(String(detail.trip_info?.amount ?? 0))").currencyValue
        view.walletDetutionValueLbl?.text = "-" + Extension.updateCurrecyRate(fare: "\(String(detail.payment_details?.wallet_payment ?? 0))").currencyValue
        view.walletDetutionValueLbl?.superview?.isHidden = (self.detail.payment_details?.wallet_payment ?? 0) <= 0
        view.totalFareValueLbl?.superview?.isHidden = (self.detail.trip_info?.amount ?? 0) <= (self.detail.trip_info?.payable_amount ?? 0)
        let settings = CacheManager.settings
        view.destinationChangeFareLbl?.text = self.detail.destination_change_time != "" ? "Destination Change Fare $XXX".localized.replacingOccurrences(of: "$XXX", with: Extension.updateCurrecyRate(fare: "\(String(settings?.location_change_charge?.stringValue ?? ""))").currencyValue) : nil
        view.destinationChangeFareLbl?.isHidden = self.detail.destination_change_time == ""
        view.discountLbl?.text = Extension.updateCurrecyRate(fare: "\(String(detail.trip_info?.discount?.doubleValue ?? 0))").currencyValue + " " + "Discount applied".localized
        view.discountLbl?.isHidden = (detail.trip_info?.discount?.doubleValue ?? 0) <= 0
        
    }
}

internal extension String {
    var currencyValue: String { return (((CacheManager.settings?.currency_denote?.lowercased() ?? "symbol") == "symbol") ? CacheManager.currencySymbol : CacheManager.currencyShortName) + " " + self }
    
    var usdValue: String { return "USD" + " " + self }
    
    var currencySymbolValue: String { return CacheManager.currencySymbol + " " + self }
    
    var distanceValue: String { return self + " " + "km".localized }
    
    var timeMinValue: String { return self + " " + (self == "1" ? "Min" : "Min") }
    
    var timeHourValue: String { return self + " " + (self == "1" ? "Hr" : "Hrs") }
    
    var timeValue: String {
        let mins = Int(self) ?? 0
        let hrs = mins/60
        let day = hrs/24
        var timeStr = ""
        if day > 0 {
            timeStr += "\(day) " + (day > 1 ? "Days" : "Day")
        }
        if hrs%24 > 0 {
            timeStr += " \(hrs%24) " + ((hrs%24) > 1 ? "Hours" : "Hour")
        }
        if mins%60 > 0 {
            timeStr += " \(mins%60) " + ((mins%60) > 1 ? "Mins" : "Min")
        }
        if timeStr == ""{
            timeStr = "0 Min"
        }

        return timeStr
    }
}

internal extension CacheManager {
    static var currencySymbol: String {
        return ((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currency_symbol ?? "Rs").uppercased()
    }
    
    static var currencyShortName: String {
        return ((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currency ?? "INR").uppercased()
    }
}
