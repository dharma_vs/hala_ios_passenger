//
//  updateCancelView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 09/03/22.
//

import Foundation

extension ServiceManager {
    
    func updateCancelView() {
        if let rideid = self.currentRide?.rideId {
            DataService().updateCancelView(rideid) { [weak self](inner) -> (Void) in
                do {
                    _ = try inner()
                    DispatchQueue.main.async { [weak self] in
                        self?.currentRide?.cancellation_rider_read = "1"
                        self?.controller?.rideCheckHelper?.stopListening()
                        self?.controller?.removeChatObserver()
                        self?.controller?.rideCheckHelper = nil
                        self?.controller?.stopAnimationTimer()
                        self?.controller?.releaseCallBacks()
                        Router.setDashboardViewControllerAsRoot()
                    }
                } catch {
                }
            }
        }
    }
}
