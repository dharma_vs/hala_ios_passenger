//
//  HomeViewModel.swift
//  HalaMobility
//
//  Created by Hala on 14/05/22.
//

import Foundation

extension ServiceManager {
    func requestBringHala(_ completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        //lat=3.15&lng=101.62
        if let currentLocation = LocationManager.shared.currentLocation {
        let body = [
            "lat": currentLocation.coordinate.latitude,
            "lng": currentLocation.coordinate.longitude
        ]
        
        self.request(endPoint: MainDataServiceEndpoint.bringHere, parameters: body) { result in
            
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: CommonRes.self)
                if let status = response, (status.status != nil) {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message?.stringValue ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
        }
    }
    func fetchapiRequest(completion: @escaping (_ inner: () throws ->  Rental1?) -> (Void)) {
        self.request(endPoint: MainDataServiceEndpoint.rentalFetch, pathSuffix: RideType
        ) { result in
            switch result {
            case .success(let data):
                print("\(data) fetchapiRequest.")
                let response  = data.getDecodedObject(from: GeneralResponse<RentalRes>.self)
                if response?.status == true {
                    completion({ return response?.data?.rental?.first})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func categorystationapiRequest(_ params: [String:Any], completion: @escaping (_ inner: () throws ->  Stationres?) -> (Void)) {
        self.request(endPoint: MainDataServiceEndpoint.station, parameters: params) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: Stationres.self)
                if response?.status == true {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func vehiclelistapiRequest(completion: @escaping (_ inner: () throws ->  VehiclelistRes?) -> (Void)) {
        self.request(endPoint: MainDataServiceEndpoint.vehiclelistbystation, pathSuffix: "\(StationID)"
        ) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: VehiclelistRes.self)
                if response?.status == true {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    // Vehicle details fare details
    func categoryapiRequest(completion: @escaping (_ inner: () throws ->  VehicledetRes?) -> (Void)) {
        var body = [String: Any]()
        body["station_id"] = StationID
        body["city"] = GlobalValues.sharedInstance.cityName
        body["city2"] = GlobalValues.sharedInstance.cityName
        self.request(endPoint: MainDataServiceEndpoint.categorydetails,parameters: body, pathSuffix: RideType
        ) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: VehicledetRes.self)
                if response?.status == true {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    func mqttapiRequest(fleetno:String, completion: @escaping (_ inner: () throws ->  MqttRes?) -> (Void)) {
        self.request(endPoint: MainDataServiceEndpoint.mqtt,parameters: nil, pathSuffix: fleetno
        ) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: MqttRes.self)
                if response?.status == true {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
