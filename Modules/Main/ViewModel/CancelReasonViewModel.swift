//
//  CancelReasonViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 08/03/22.
//

import Foundation

class CancelReasonViewModel: AppViewModel {
    weak var controller: CancelReasonViewController!
    required init(controller: CancelReasonViewController) {
        self.controller = controller
    }
}

extension ServiceManager {
    
    func cancelReason(_ completion: @escaping (_ inner: () throws ->  [CancelReason]) -> (Void)) {
        
        self.request(endPoint: MainDataServiceEndpoint.cancelReason, parameters: ["type":"Customer"] as Dictionary<String,Any>) { result in
            
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: GeneralResponse<[CancelReason]>.self)
                if let reasons = response?.data {
                    completion({ return reasons })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
