//
//  RideTourViewController.swift
//  TaxiPickup
//
//  Created by Prabakaran on 20/01/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit
import FAPaginationLayout
import DropDown

enum RentalRideCreationState: Int {
    case addressSelected = 1,
    packageLoaded,
    packageSelected,
    categoryLoaded
}

struct Package: JSONSerializable {
    let id: Int?
    let packageName, cityName: String?
    let travelHrs, travelDistance: Int?
    let distanceUnit, welcomeDescription: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case packageName = "package_name"
        case cityName = "city_name"
        case travelHrs = "travel_hrs"
        case travelDistance = "travel_distance"
        case distanceUnit = "distance_unit"
        case welcomeDescription = "description"
    }
}

import ContactsUI

extension RideTourViewController: CNContactPickerDelegate {
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        self.bookForContact = contact
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        self.bookForContact = contacts.first
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelectContactProperties contactProperties: [CNContactProperty]) {
        
    }
}

extension CNContact {
    var primartyNumber: String {
        return self.phoneNumbers.first?.value.stringValue ?? ""
    }
    
    var info: (String, String) {
        return (self.givenName, self.primartyNumber)
    }
}

class RideTourViewController: BaseViewController,UIScrollViewDelegate {
    
    @IBOutlet weak var basicPackageLbl: AppLabel!
    @IBOutlet weak var basicPackageValueLbl: AppLabel!
    @IBOutlet weak var extraPerKMLbl: AppLabel!
    @IBOutlet weak var extraPerKMValueLbl: AppLabel!
    @IBOutlet weak var extraPerMinLbl: AppLabel!
    @IBOutlet weak var extraPerMinValueLbl: AppLabel!
    
    @IBOutlet weak var paymentMethodIconImage: AppImageView!
    @IBOutlet weak var paymentMethodValueLbl: AppLabel!
    @IBOutlet weak var paymentMethodLbl: AppLabel!
    @IBOutlet weak var bookedForLbl: AppLabel!
    @IBOutlet weak var bookedForValue: AppLabel!
    @IBOutlet weak var walletBalance: AppLabel!
    
    @IBOutlet weak var paymentStackView: UIStackView!

    @IBOutlet weak var cashWalletStackview: UIStackView!

    @IBOutlet weak var bookForOtherStackView: UIStackView!

    @IBOutlet weak var PaymentBookorOthersStackView: UIStackView!

    @IBOutlet weak var bookforOthersPhoneNumberStackView: UIStackView!
    
    @IBOutlet weak var closeBookForOtherBtn: AppButton!
        
    @IBOutlet weak var walletBalanceLbl: AppLabel!
    
    @IBOutlet weak var contactInformationLbl: AppButton!
    
    @IBOutlet weak var rideListView: RideListView!
        
    @IBAction func chooseContact(_ sender: AppButton) {
        self.requestContactAccess { [weak self](status) in
            if status {
                OperationQueue.main.addOperation { [weak self] in
                    let contactPicker = CNContactPickerViewController()
                    contactPicker.delegate = self
                    //        contactPicker.predicateForEnablingContact = NSPredicate(format: "emailAddresses.@count > 0")
                    self?.present(contactPicker, animated: true, completion: nil)
                }
            } else {
                
            }
        }
    }
    
    var cardPayment = Bool()
    
    var selectedCardID = "0"
    
    var selectedCard: Card_info?
    
    @IBOutlet weak var showCardlistBtn: AppButton!
    
    @IBAction func showCardList() {
        self.cardPayment = true
        self.rideListView.frame = CGRect(x: 0, y: 0, width: (self.view.frame.width), height: (self.view.frame.height))
        self.view.addSubview(self.rideListView)
        self.view.bringSubviewToFront(self.rideListView)
        self.rideListView.rideListTableView.register(UINib(nibName: "CardListCell", bundle: Bundle.main), forCellReuseIdentifier: "CardListCell")
        self.rideListView.rideListTableView.tableFooterView = UIView()
        self.rideListView.rideListTableView.reloadData()
        self.rideListView.addCardBtn.isHidden = false
        self.rideListView.titleLbl.text = "Select Card"
        
        let str = self.selectedCard?.card_no
        let last4 = String(str?.suffix(4) ?? "")

        self.walletBalanceLbl?.text = self.selectedCard != nil ? String(format: "XXXX-XXXX-XXXX-%@ (\(self.selectedCard?.card_name ?? ""))", last4 ) : "Choose Card"
        
        self.rideListView.addNewCard = { [weak self] in
            let paymentVC = AddDebitCardViewController.initFromStoryBoard(.Payment)
            paymentVC.delegate = self
            self?.navigationController?.pushViewController(paymentVC, animated: true)
        }
    }
    
    var bookforOthersPhoneNumber = ""
    
    var bookforOthersName = ""
    
    @IBAction func clearContactInformation(_ sender: AppButton) {
        self.contactInformationLbl?.titleLabel?.numberOfLines = 2
        self.contactInformationLbl?.titleLabel?.lineBreakMode = .byClipping
        self.contactInformationLbl.setTitle("Personal".localized, for: .normal)
        self.bookforOthersPhoneNumber = ""
        self.bookforOthersName = ""
        self.closeBookForOtherBtn.isHidden = true
    }
    
    var changePaymentType: (()->Void)?
    
    var pickupLocationDetail: LocationDetail? {
        didSet {
            OperationQueue.main.addOperation { [weak self] in
                self?.addressLbl?.text = self?.pickupLocationDetail?.address
                self?.rentalCreationState = .addressSelected
            }
        }
    }
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionViewheightConstraint: NSLayoutConstraint!

    @IBOutlet weak var addOnsLbl: AppLabel!
    @IBOutlet weak var pickupLocationLbl: AppLabel!
    @IBOutlet weak var addressLbl: AppLabel!
    @IBOutlet weak var packageLbl: AppLabel!
    @IBOutlet weak var selectedPackageLbl: AppLabel!
    @IBOutlet weak var packageTableView: UITableView!
    @IBOutlet weak var vehicleTypeLbl: AppLabel!
    @IBOutlet weak var fareInfoLbl: AppLabel!
    @IBOutlet weak var timeLineView: RideLaterView!
    @IBOutlet weak var couponTextField: AppTextField!
    
    @IBOutlet weak var packageView: UIView!
    @IBOutlet weak var packageContentView: UIView!
    @IBOutlet weak var packageSelectionBtn: AppButton!
    @IBOutlet weak var rideTourScrollView: UIScrollView!

    @IBOutlet weak var selectionFallthroughView: UIView!
    
    var selectedCoupon: Coupon?
    
    @IBOutlet weak var confirmBtn: AppButton!
    
    var selectedModel = 0
    
    @IBOutlet weak var removeContactBtn: AppButton!
    
    var bookForContact: CNContact? {
        didSet {
            
            self.contactInformationLbl?.titleLabel?.numberOfLines = 2
            self.contactInformationLbl?.titleLabel?.lineBreakMode = .byClipping
            self.contactInformationLbl.setTitle(self.bookForContact == nil ? "Personal".localized : "\(bookForContact?.info.0 ?? "")(\(bookForContact?.phoneNumbers.first?.value.stringValue ?? ""))", for: .normal)
            
            bookforOthersPhoneNumber = bookForContact?.info.1 ?? ""
            bookforOthersName = bookForContact?.info.0 ?? ""

            
//            self.closeBookForOtherBtn?.isHidden = self.bookForContact == nil
//            let value = (self.bookForContact?.info.0 ?? "") + ", " + (self.bookForContact?.info.1 ?? "")
//            self.contactInformationLbl.setTitle(self.bookForContact == nil ? "Personal".localized : value, for: UIControl.State())
        }
    }
    
    @IBAction func confirmRide(_ sender: UIButton) {
        
        if self.selectedPaymentMethod == .card {
            self.selectedCardID = "0"
//            if self.selectedCard == nil {
//                self.view.makeToast("Please select or add card to proceed")
//                return
//            }
        }
        
        if (CacheManager.userInfo?.walletAmount?.doubleValue ?? 0) < (self.selectedCategory?.baseFare?.doubleValue ?? 0){
            self.showAlertWithMessage("you don't have suficient money in your wallet. Want to upgrade your wallet balance?".localized, phrase1: "Yes".localized, phrase2: "Cancel".localized, skipDismiss: false, action: UIAlertAction.Style.default, .cancel, completion: { [weak self] (style) in
                if style == .default {
                    let walletVC = Router.main.instantiateViewController(withIdentifier: "WalletAmountVC")
                    self?.navigationController?.pushViewController(walletVC, animated: true)
                }
            })
            return
        }
        
        self.showAlertWithMessage("Are you sure you want to confirm Ride?".localized, phrase1: "Yes".localized, phrase2: "No".localized, skipDismiss: false, action: UIAlertAction.Style.default, .cancel, completion: { [weak self] (style) in
            if style == .default {
                confirmRide()
            }
        })
        
        func confirmRide() {
            
           
            
            /*
             rider_id=8&driver_id=0&city=Coimbatore&s_name=&s_address=Gandhipuram,
             Coimbatore,
             Tamil Nadu,
             India&s_latitude=11.020983&s_longitude=76.9663344&d_name=&d_address=&d_latitude=&d_longitude=&payment_method=1&promo_code_id=&ride_datetime=2019-08-27 17:40:00&vehicle_type=4.0&category_id=2&trip_info=1.0,
             rental&ride_type=Rental&package_id=1.0&package_name=1 hrs - 10 km&estimated_distance=10.0&estimated_duration=1.0&estimated_fare=250.0&country_short_name=IN&country_full_name=India&map_root=&booking_for_name=&booking_for_phone=
             */
            
            let confirmInfo = RequestConfirmRide()
//            confirmInfo.category = self.selectedCategory?.serviceType ?? ""
            confirmInfo.vehicle_type = self.selectedCategory?.carType ?? 0
            confirmInfo.map_root = ""
           // confirmInfo.city = self.pickupLocationDetail?.cities.city ?? ""
            confirmInfo.city = "All Cities"
            confirmInfo.s_address = self.pickupLocationDetail?.address ?? ""
            confirmInfo.d_address = ""
            confirmInfo.s_latitude = self.pickupLocationDetail?.coordinate.latitude ?? 0.0
            confirmInfo.s_longitude = self.pickupLocationDetail?.coordinate.longitude ?? 0.0
//            confirmInfo.d_latitude = 0.0
//            confirmInfo.d_longitude = 0.0
            confirmInfo.estimated_distance = "\(self.selectedPackage?.travelDistance ?? 0)"
            confirmInfo.estimated_duration = "\(self.selectedPackage?.travelHrs ?? 0)"
            confirmInfo.estimated_fare = self.selectedCategory?.baseFare?.doubleValue ?? 0
            confirmInfo.promo_code_id = self.selectedCoupon?.id?.stringValue ?? ""
            confirmInfo.package_id = "\(self.selectedPackage?.id ?? 0)"
            confirmInfo.package_name = self.selectedPackage?.packageName ?? ""
            //            confirmInfo.ride_id = "0"
            confirmInfo.payment_method = self.selectedPaymentMethod == .cash ? 1 : self.selectedPaymentMethod == .wallet ? 3 : 2
            confirmInfo.ride_type = "Rental"
            confirmInfo.ride_datetime = self.timeLineView.selectedDateTime
           // confirmInfo.country_full_name = self.pickupLocationDetail?.country?.country_full_name ?? ""
           // confirmInfo.country_short_name = self.pickupLocationDetail?.country?.country_short_name ?? ""
            confirmInfo.country_full_name = "India"
            confirmInfo.country_short_name = "IN"
            confirmInfo.trip_info = "\(Float(self.selectedCategory?.rentalID ?? 0)),rental"
            confirmInfo.rider_id = "\(CacheManager.userInfo?.id ?? 0)"
            confirmInfo.driver_id = "0"
            confirmInfo.category_id = RideCategory.Rental.rawValue
            confirmInfo.s_name = ""
            confirmInfo.d_name = ""
            confirmInfo.search_latitude = self.pickupLocationDetail?.coordinate.latitude ?? 0.0
            confirmInfo.search_longitude = self.pickupLocationDetail?.coordinate.longitude ?? 0.0
            confirmInfo.booking_for_name = self.bookforOthersName
            confirmInfo.booking_for_phone = self.bookforOthersPhoneNumber
            confirmInfo.card_id = self.selectedCard?.id?.stringValue ?? "0"

//            rider_id=8&driver_id=0&partner_id=0&city=All Cities&s_name=&s_address=Unnamed Road, Narasingapuram, Tamil Nadu 636108, India&s_latitude=11.6051949&s_longitude=78.5705341&d_name=&d_address=&d_latitude=&d_longitude=&search_latitude=11.6051949&search_longitude=78.5705341&payment_method=2&card_id=3&promo_code_id=&ride_datetime=2021-09-13 14:20:00&vehicle_type=4&category_id=2&trip_info=1,rental&ride_type=Rental&vehicle_info=&package_id=1&package_name=1 hrs - 10 km&estimated_distance=10.0&estimated_duration=1&estimated_fare=100.0&country_short_name=CG&country_full_name=Congo&map_root=&booking_for_name=Abdul&booking_for_phone=+917845533782&route=&ride_pickup_datetime=&ride_return_datetime=
            
            self.startActivityIndicatorInWindow()
            ServiceManager().confirmRide(confirmInfo, completion: { [weak self] (inner) -> (Void) in
                self?.stopActivityIndicator()
                do {
                    let response = try inner()
                    self?.view.makeToast(response?.message?.stringValue)
                    let alertVC = self?.alertController
                    alertVC?.message = "Your ride has been created, you will get the driver details before 15 min from booking time".localized
                    alertVC?.addAction(.default, buttonPhrase: "Ok".localized, completion: { [weak self] (style) in
                        DispatchQueue.main.async { [weak self] in
                            self?.dismiss(animated: true, completion: nil)
//                                Router.setDashboardViewControllerAsRoot()
                                NotificationCenter.default.post(Notification.init(name: Notification.Name.init("CHECK_RIDE_UPDATES")))
                        }
                    })
                    self?.present(alertVC!, animated: true, completion: nil)
                } catch {
                    self?.view.makeToast((error as? CoreError)?.description)
                }
            })
        }
    }
    
    var viewModel: RideTourViewModel!

    @IBAction func removeBookFor(_ sender: UIButton) {
        self.bookForContact = nil
    }
    
    @IBAction func dismissCardSelection(_ sender: UIButton) {
        self.rideListView?.removeFromSuperview()
    }
    
    @IBAction func changePaymentMethod(_ sender: UIButton) {
        self.dropDown.anchorView = self.paymentMethodValueLbl
        self.dropDown.width = self.paymentMethodValueLbl.superview?.bounds.width ?? .zero
        self.dropDown.show()
    }
    
    @IBAction func choosePackage(_ sender: UIButton) {
        self.rentalCreationState = .packageLoaded
    }
    
    @IBAction func changePickupAddress(_ sender: UIButton) {
        let searchVC = SearchLocationViewController.initFromStoryBoard(.Main)
        searchVC.addressType = .source
        searchVC.didSelectLocation = { [weak self] locationDetail in
            DispatchQueue.main.async { [weak self] in
                self?.pickupLocationDetail = locationDetail
                self?.navigationController?.popViewController(animated: true)
            }
        }
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    var categories: [RentalCategory] = [] {
        didSet {
            self.collectionView?.reloadData()
        }
    }
    
    var selectedCategory: RentalCategory? {
        didSet{
            self.fareInfoLbl?.text = self.selectedCategory?.message
            
            self.basicPackageValueLbl?.text = Extension.updateCurrecyRate(fare: "\(self.selectedCategory?.baseFare?.doubleValue ?? 0)").currencyValue
            self.extraPerKMValueLbl?.text = Extension.updateCurrecyRate(fare: "\(self.selectedCategory?.costPerDistance?.doubleValue ?? 0)").currencyValue
            self.extraPerMinValueLbl?.text = Extension.updateCurrecyRate(fare: "\(self.selectedCategory?.rideCostPerMinute ?? 0)").currencyValue

        }
    }
    
    var selectedPackage: Package? {
        didSet{
            self.selectedPackageLbl?.text = self.selectedPackage?.packageName
            self.rentalCreationState = .packageSelected
            self.packageTableView?.reloadData()
        }
    }
    
    var packages: [Package] = [] {
        didSet {
            self.heightConstraint?.constant = CGFloat(40 * self.packages.count)
        }
    }
    
    var rentalCreationState: RentalRideCreationState = RentalRideCreationState.addressSelected {
        didSet {
            self.packageContentView?.isHidden = true
            self.packageView?.isHidden = true
            self.packageSelectionBtn?.isHidden = true
            self.packageTableView?.isHidden = true
            self.selectionFallthroughView?.isHidden = true
            switch self.rentalCreationState {
            case .addressSelected:
                self.loadPackages()
            case .packageLoaded:
                self.packageTableView?.isHidden = false
            case .packageSelected:
                self.packageView?.isHidden = false
                self.packageSelectionBtn?.isHidden = false
                self.loadCategories()
            case .categoryLoaded:
                self.packageView?.isHidden = false
                self.packageSelectionBtn?.isHidden = false
                self.selectionFallthroughView?.isHidden = false
            }
        }
    }
    
    func loadPackages() {
        var req = RentalListReq()
        req.city = pickupLocationDetail?.cities.city
        req.city2 = pickupLocationDetail?.cities.city2
        req.s_latitude = pickupLocationDetail?.coordinate.latitude.stringValue
        req.s_longitude = pickupLocationDetail?.coordinate.latitude.stringValue
        self.startActivityIndicatorInWindow()
        ServiceManager().rentalList(req) { [weak self] (inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
                let packages = try inner()
                self?.packages = packages
                self?.rentalCreationState = .packageLoaded
            } catch {}
        }
    }
    
    func loadCategories() {
        var req = RentalDetailReq()
        req.city = pickupLocationDetail?.cities.city
        req.package_id = self.selectedPackage?.id?.stringValue
        req.s_latitude = pickupLocationDetail?.coordinate.latitude.stringValue
        req.s_longitude = pickupLocationDetail?.coordinate.longitude.stringValue
        self.startActivityIndicatorInWindow()
        ServiceManager().rentalDetails(req) { [weak self] (inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
                let categories = try inner()
                self?.categories = categories
                self?.selectedCategory = self?.categories.first
                self?.rentalCreationState = .categoryLoaded
            } catch {}
        }
    }
    
    lazy var dropDown: DropDown = { [weak self] in
        let dropDwn = DropDown()
        dropDwn.anchorView = self?.paymentMethodValueLbl
        dropDwn.direction = .any
        dropDwn.backgroundColor = .white
        dropDwn.selectionBackgroundColor = .white
        dropDwn.width = 80.0
        dropDwn.cellHeight = 35.0
        dropDwn.layer.cornerRadius = 5.0
        dropDwn.SetBorder(width: 1, color: .colorTextLight)
        dropDwn.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
        
        dropDwn.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDropDownCell else { return }
            cell.iconImg.image = UIImage(named: item.capitalized)?.withRenderingMode(.alwaysTemplate)
            cell.iconImg.tintColor = .colorAccent
        }
        
        dropDwn.dataSource = PaymentMethod.allRawValues
        dropDwn.textFont = UIFont(name: "Montserrat-SemiBold", size: 14) ?? UIFont.systemFont(ofSize: 14)
        
        dropDwn.selectionAction = { [weak self] (index: Int, item: String) in
            
            self?.selectedPaymentMethod = item.lowercased() == "cash" ? .cash : item.lowercased() == "card" ? .card : .wallet
            
            self?.paymentMethodValueLbl?.text = item
            dropDwn.hide()
        }
        return dropDwn
        }()
    
    var selectedPaymentMethod: PaymentMethod = .cash {
        didSet {
            self.walletBalanceLbl?.superview?.isHidden = self.selectedPaymentMethod == .cash || self.selectedPaymentMethod == .card ? true : false
            
            self.showCardlistBtn?.isHidden = true//self.selectedPaymentMethod != .card
            
            if selectedPaymentMethod == .card {
                let item = self.selectedCard
                let str = item?.card_no
                let last4 = String(str?.suffix(4) ?? "")
                self.walletBalanceLbl?.text = nil//self.selectedCard != nil ? String(format: "XXXX-XXXX-XXXX-%@ (\(self.selectedCard?.card_name ?? ""))", last4 ) : "Choose Card"
                self.walletBalance?.text = nil//self.selectedCard != nil ? String(format: "XXXX-XXXX-XXXX-%@ (\(self.selectedCard?.card_name ?? ""))", last4 ) : "Choose Card"
            } else {
                self.walletBalanceLbl?.text = "Wallet Balance".localized + ": " +
                    Extension.updateCurrecyRate(fare: "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)").currencyValue
                    
                   // "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)".currencyValue
                self.walletBalance?.text = "Wallet Balance".localized + ": " +
                    Extension.updateCurrecyRate(fare: "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)").currencyValue
                
                //"\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)".currencyValue
            }
            
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        rideTourScrollView.delegate = self
        rideTourScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: 750)

        let mapsHelper = GoogleMapsHelper()
        mapsHelper.getCurrentLocation { [weak self](location) in
            mapsHelper.getPlaceAddress(from: location.coordinate, on: { [weak self] (locationDetail) in
                self?.pickupLocationDetail = locationDetail
            })
        }
        
        collectionView.register(UINib(nibName: "RentRideCell", bundle: Bundle.main), forCellWithReuseIdentifier: "RentRideCell")
        collectionView.register(UINib(nibName: "RideCellTheme1", bundle: Bundle.main), forCellWithReuseIdentifier: "RideCellTheme1")

        collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 25, bottom: 0, right: 25)
        
        (collectionView.collectionViewLayout as! FAPaginationLayout).scrollDirection = .horizontal
        
        (collectionView.collectionViewLayout as! FAPaginationLayout).minimumLineSpacing = 15.0
        
        self.timeLineView?.chooseDateTime = { [weak self] in
            let calendarVC = CalendarViewController.initFromStoryBoard(.Base)
            calendarVC.isForRentalRide = true
            calendarVC.didSelectDateTime = { [weak self] (date, time) in
                self?.timeLineView.selectedDateTime = date?.utcDateFromFormater("yyyy-MM-dd HH:mm:ss") ?? ""
                self?.timeLineView.dateLbl.text = date?.dateFromFormater("EEEE MMM dd yyyy") ?? ""
                self?.timeLineView.pickupTimeLbl.text = "Pick Up Time".localized + " - " + (time ?? "")
                self?.dismiss(animated: true, completion: nil)
            }
            calendarVC.modalPresentationStyle = .overCurrentContext
            self?.present(calendarVC, animated: true, completion: nil)
        }
        
        self.bookForContact = nil
        
        let minDuration = CacheManager.settings?.ride_rent_minute_duration
        let rideRentMininumDuration = (Int(minDuration ?? "10") ?? 10)
        let date = Date().addingTimeInterval(TimeInterval(rideRentMininumDuration * 60))
        let time = date.dateFromFormater("hh:mm a")
        self.timeLineView.selectedDateTime = date.utcDateFromFormater("yyyy-MM-dd HH:mm:ss") ?? ""
        self.timeLineView.dateLbl.text = date.dateFromFormater("EEEE MMM dd yyyy") ?? ""
        self.timeLineView.pickupTimeLbl.text = "Pick Up Time".localized + " - " + (time ?? "")
        self.selectedPaymentMethod = .cash
        
        clearContactInformation(AppButton())
        
        // Do any additional setup after loading the view.
    }
    
    override func localize() {
        self.addOnsLbl?.text = "Addons".localized
        self.pickupLocationLbl?.text = "Pickup Location".localized
        self.packageLbl?.text = "Package".localized
        self.vehicleTypeLbl?.text = "Vehicle Type".localized
        self.couponTextField?.placeholder = "Apply coupon".localized
        self.paymentMethodLbl?.text = "Payment Method".localized
        self.bookedForLbl?.text = "Booked for".localized
        self.confirmBtn?.setTitle("Confirm".localized, for: UIControl.State())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func ShowSlidemenu()
    {
        self.slideMenuController()?.openLeft()
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrolling")
    }
}

extension RideTourViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let applyCouponVC = ApplyCouponViewController.initFromStoryBoard(.Main)
        applyCouponVC.applyCouponCode = { [weak self] couponCode, coupon in
            self?.selectedCoupon = coupon
            self?.couponTextField?.text = couponCode
            self?.navigationController?.popViewController(animated: true)
        }
        self.navigationController?.pushViewController(applyCouponVC, animated: true)
        return false
    }
}

extension RideTourViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cardPayment && tableView == rideListView.rideListTableView {
            return CacheManager.riderInfo?.card_info?.count ?? 0
        }
        return self.packages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if cardPayment && tableView == rideListView.rideListTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CardListCell", for: indexPath) as! CardListCell
            
            cell.backgroundColor = .clear
            let item = CacheManager.riderInfo?.card_info?[indexPath.row]
            cell.lblName.text = item?.card_name
            let str = item?.card_no
            let last4 = String(str?.suffix(4) ?? "")
            
            cell.lblNo.text = String(format: "XXXX-XXXX-XXXX-%@", last4 )
            cell.btnDelete.tag = indexPath.row
            
            cell.lblDate.text = String(format: "Expires : %@", (item?.expiry_date)!)
            cell.lblBrand.text = String(format: "Brand : %@", (item?.brand)!)
            if "\(item?.brand?.lowercased() ?? "")" == "mastercard"{
                cell.imgCard.image = UIImage(named: "master")
            }else{
                cell.imgCard.image = UIImage(named: "\(item?.brand?.lowercased() ?? "")")
            }
            cell.selectionStyle = .none
            cell.btnDelete.isHidden = true
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "PackageCell", for: indexPath) as! PackageCell
        cell.packageName?.text = packages[indexPath.row].packageName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if cardPayment && tableView == rideListView.rideListTableView {
            let item = CacheManager.riderInfo?.card_info?[indexPath.row]
            self.selectedCard = item
            let str = item?.card_no
            let last4 = String(str?.suffix(4) ?? "")
            
            self.walletBalanceLbl?.text = String(format: "XXXX-XXXX-XXXX-%@ (\(item?.card_name ?? ""))", last4 )
            
            self.selectedCardID = "\(item?.id ?? 0)"
            self.rideListView?.removeFromSuperview()
        }
        self.selectedPackage = packages[indexPath.row]
    }
}

extension RideTourViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if UserDefaults.secondTheme {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RideCellTheme1", for: indexPath) as! RideCellTheme1
            
            let category = self.categories[indexPath.item]
            
            cell.titleLabel.text = category.displayName
            cell.etaImage.isHidden = true
            
            if ((category.nearByCabs?.count ?? 0) > 0) {
                
                if category.eta?.isEmpty == true {
                    cell.descLabel?.text = CacheManager.sharedInstance.getObjectForKey(.ETA) as? String
                    if let sourceCoordinate = self.pickupLocationDetail?.coordinate, let currentLocationCoordinate = category.nearByCabs?.first?.coordinate {
                        GoogleMapsHelper().calculateDistanceDuration(sourceCoordinate, toLocation: currentLocationCoordinate) { [weak self] (distanceDuration) in
                            DispatchQueue.main.async { [weak self] in
                                CacheManager.sharedInstance.setObject(distanceDuration?.duration?.text?.eta ?? "", key: .ETA)
                                CacheManager.sharedInstance.saveCache()
                                cell.descLabel?.text = distanceDuration?.duration?.text?.eta }
                        }
                    } else {
                        cell.descLabel?.text = category.eta?.eta ?? "Not Available".localized
                    }
                } else {
                    cell.descLabel?.text = category.eta?.eta ?? "Not Available".localized
                }
                /*
                 if (category.rideEstimate?.estimatedFare ?? 0) > 0 {
                 cell.rideFareLbl?.text = "\(category.rideEstimate?.estimatedFare ?? 0)".estimatedFare
                 } else { */
                cell.rideFareLbl?.text = Extension.updateCurrecyRate(fare: "\(category.baseFare?.doubleValue ?? 0)").currencyValue
                //            }
            } else {
                cell.descLabel?.text = "Not Available".localized
                cell.rideFareLbl?.text = Extension.updateCurrecyRate(fare: "\(category.baseFare?.doubleValue ?? 0)").currencyValue
            }

            
//            cell.showCarInfo = { [weak self] in
//                let vehicleInfoVC = Router.main.instantiateViewController(withIdentifier: StoryboardIDs.VehicleInfoViewController)
//                (vehicleInfoVC as? VehicleInfoViewController)?.category = self?.selectedCategory
//                vehicleInfoVC.modalPresentationStyle = .overCurrentContext
//                self?.present(vehicleInfoVC, animated: true, completion: nil)
//            }
            cell.bgView.layer.cornerRadius = 5
            if selectedModel == indexPath.row {
                cell.carImage?.loadTaxiImage((category.selectedImage ?? category.image)?.urlFromString)
                cell.bgView.layer.borderColor = UIColor.gStartColor.cgColor
                cell.bgView.backgroundColor = .colorAppBackground
                cell.bgView.layer.borderWidth = 2
                cell.bgView.layer.masksToBounds = true
            }else {
                cell.bgView.backgroundColor = .groupTableViewBackground
                cell.bgView.layer.borderColor = UIColor.lightGray.cgColor
                cell.bgView.layer.borderWidth = 1
                cell.carImage?.loadTaxiImage(category.image?.urlFromString)
            }
            cell.titleLabel?.font = UIFont.myBoldSystemFont(ofSize: 14)
            cell.descLabel?.font = UIFont.mySystemFont(ofSize: 12)
            cell.rideFareLbl?.font = UIFont.mySystemFont(ofSize: 14)
            return cell
            
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RentRideCell", for: indexPath) as! RentRideCell
                 
                 let category = self.categories[indexPath.item]
                 
                 cell.titleLabel.text = category.displayName
                 
                 if ((category.nearByCabs?.count ?? 0) > 0) {
                     
                     if category.eta?.isEmpty == true {
                         cell.descLabel?.text = CacheManager.sharedInstance.getObjectForKey(.ETA) as? String
                         if let sourceCoordinate = self.pickupLocationDetail?.coordinate, let currentLocationCoordinate = category.nearByCabs?.first?.coordinate {
                             GoogleMapsHelper().calculateDistanceDuration(sourceCoordinate, toLocation: currentLocationCoordinate) { [weak self] (distanceDuration) in
                                 DispatchQueue.main.async { [weak self] in
                                    CacheManager.sharedInstance.setObject(distanceDuration?.duration?.text?.eta ?? "", key: .ETA)
                                     CacheManager.sharedInstance.saveCache()
                                     cell.descLabel?.text = distanceDuration?.duration?.text?.eta }
                             }
                         } else {
                             cell.descLabel?.text = category.eta?.eta ?? "Drivers Not Available".localized
                         }
                     } else {
                         cell.descLabel?.text = category.eta?.eta ?? "Drivers Not Available".localized
                     }
                     /*
                      if (category.rideEstimate?.estimatedFare ?? 0) > 0 {
                      cell.rideFareLbl?.text = "\(category.rideEstimate?.estimatedFare ?? 0)".estimatedFare
                      } else { */
                    cell.rideFareLbl?.text = Extension.updateCurrecyRate(fare: "\(category.baseFare?.doubleValue ?? 0)").currencyValue
                     //            }
                 } else {
                     cell.descLabel?.text = "Drivers Not Available".localized
                    cell.rideFareLbl?.text = Extension.updateCurrecyRate(fare: "\(category.baseFare?.doubleValue ?? 0)").currencyValue
                 }
                 
                 cell.carImage?.loadTaxiImage(category.image?.urlFromString)
                 
                 cell.titleLabel?.font = UIFont.myBoldSystemFont(ofSize: 15)
                 cell.descLabel?.font = UIFont.mySystemFont(ofSize: 11)
                 cell.rideFareLbl?.font = UIFont.mySystemFont(ofSize: 13)
                 return cell
        }
     
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellSize = CGSize()
        
        if !UserDefaults.secondTheme{
            cellSize = collectionView.bounds.size
            cellSize.width -= collectionView.contentInset.left
            cellSize.width -= collectionView.contentInset.right
        }else {
            cellSize = collectionView.bounds.size
            cellSize.width = 100
        }
        return cellSize
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if UserDefaults.secondTheme{
          if selectedModel == indexPath.row {
             
            }else {
                selectedModel = indexPath.row
            }
        }
        self.selectedCategory = self.categories[indexPath.item]
        collectionView.reloadData()

    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        visibleRect.origin = scrollView.contentOffset
        visibleRect.size = scrollView.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = collectionView.indexPathForItem(at: visiblePoint) else { return }
        self.selectedCategory = self.categories[indexPath.item]
    }
}

extension Int {
    var stringValue: String {
        return "\(self)"
    }
    
    var currencyValue: String { return CacheManager.currencyShortName + " " + "\(self)" }
}

class PackageCell: UITableViewCell {
    @IBOutlet weak var packageName: UILabel!
}
import Contacts
extension UIViewController {
    func requestContactAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            self.alertContactsAccessNeeded()
        case .restricted, .notDetermined:
            CNContactStore().requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                } else {
                    self.alertContactsAccessNeeded()
                }
            }
        }
    }
    
    func alertContactsAccessNeeded() {
        let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
        let alert = UIAlertController(
            title: "Need Contacts Access",
            message: "Location access is required to get contact's information for booking ride for them.",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
            [unowned self] (alert) -> Void in
            UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Allow Location Access", style: .default, handler: { (alert) -> Void in
            UIApplication.shared.open(settingsAppURL, options: [UIApplication.OpenExternalURLOptionsKey : Any](), completionHandler: nil)
        }))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}

extension RideTourViewController: AddDebitCardDelegate {
    func newCardAdded(info: Card_info) {
        CacheManager.userInfo?.card_info?.append(info)
        self.rideListView?.rideListTableView?.reloadData()
        self.updateProfile()
    }
}

