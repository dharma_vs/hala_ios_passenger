//
//  DeliveryInformationViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 02/04/22.
//

import UIKit

class DeliveryInformationViewController: BaseViewController {

    @IBOutlet weak var pickUpLbl: AppLabel!
    @IBOutlet weak var pickUpTxtField: AppTextField!
    @IBOutlet weak var pickUpMobNoTxtField: AppTextField!
    @IBOutlet weak var pickUpaddressBtn: AppButton!
    @IBOutlet weak var deliveryLbl: AppLabel!
    @IBOutlet weak var deliveryTxtField: AppTextField!
    @IBOutlet weak var deliveryMobNoTxtField: AppTextField!
    @IBOutlet weak var deliveryaddressBtn: AppButton!
    @IBOutlet weak var submitBtn: AppButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func submitBtn(_ sender: AppButton) {
        
    }
}
