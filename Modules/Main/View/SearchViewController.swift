//
//  SearchVC.swift
//  TaxiPickup
//
//  Created by TAMILARASAN on 26/02/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit
import GooglePlaces

enum AddressType: String {
    case source = "Source Address"
    case destination = "Destination Address"
    case favourite = "Add Favourite Location"
    case rideserve = "Hala-Ride Serve"

}

class SearchLocationViewController: BaseViewController,GMSAutocompleteViewControllerDelegate,CLLocationManagerDelegate {
    
    deinit {
        print("Search VC deinits")
    }

    @IBOutlet weak var favouritesLbl: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var btn_chooselocation: UIButton!
    @IBOutlet weak var btn_chooseonmap: UIButton!
    @IBOutlet weak var btn_choosecurrentloc: UIButton!
    @IBOutlet weak var txtfld_chooseloc: UITextField!
    
    var addressType: AddressType = .source
    
    var didSelectLocation:((LocationDetail?)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        txtfld_chooseloc.addTarget(self, action: #selector(btnclk_Chooselocation), for: UIControl.Event.touchUpInside)
    }
    
    override func localize()
    {
        txtfld_chooseloc.placeholder = "chooseLocation".localized
        lbl_Title.text = addressType == .source ? "source".localized : addressType == .rideserve ? "HalaRideServe".localized : addressType == .destination ? "destination".localized : "Add Favourite Location".localized
//        btn_chooselocation .setTitle("chooseCurrentLocation".localized, for:.normal)
        btn_chooseonmap .setTitle("chooseLocationOnMap".localized, for:.normal)
        btn_choosecurrentloc .setTitle("chooseCurrentLocation".localized, for:.normal)
        favouritesLbl.text = "favourites".localized
    }
    
    @IBAction func btnclk_back() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnclk_Chooselocation() {
        let autoCompleteController = GMSAutocompleteViewController()
        autoCompleteController.delegate = self
        
        let filter = GMSAutocompleteFilter()
        autoCompleteController.autocompleteFilter = filter
        
//        self.locationManager.startUpdatingLocation()
        self.present(autoCompleteController, animated: true, completion: nil)
    }
    
    @IBAction func btnclk_Chooseonmap(_ sender: Any) {
        let vc = MapViewController.initFromStoryBoard(.Dashboard)
        vc.didSelectLocation = { [weak self] locationDetail in
            self?.didSelectLocation?(locationDetail)
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnclk_Choosecurrentloc() {
        let mapsHelper = GoogleMapsHelper()
        mapsHelper.getCurrentLocation { [weak self] (location) in
            mapsHelper.getPlaceAddress(from: location.coordinate, on: { [weak self] (locationDetail) in
                self?.didSelectLocation?(locationDetail)
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.locationManager.stopUpdatingLocation()
//        self.locationManager.delegate = nil
//        self.locationManager = nil
    }
    
    // MARK: GOOGLE AUTO COMPLETE DELEGATE
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let string = place.name! + ","
        
        txtfld_chooseloc.text=string + place.formattedAddress!

        GoogleMapsHelper().getPlaceAddress(from: place.coordinate) { [weak self] (locationDetail) in
            let updatedLocationDetail = (string + place.formattedAddress!, locationDetail.cities, place.coordinate, locationDetail.country)
            self?.didSelectLocation?(updatedLocationDetail)
        }
        
        self.dismiss(animated: false, completion: nil) // dismiss after place selected
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("ERROR AUTO COMPLETE \(error)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension SearchLocationViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CacheManager.favouriteLocations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteAddressCell", for: indexPath) as! FavouriteAddressCell
        let location = CacheManager.favouriteLocations[indexPath.row]
        cell.favLbl.text = location.locationName
        cell.addressLbl.text = location.locationAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let location = CacheManager.favouriteLocations[indexPath.row]
        GoogleMapsHelper().getPlaceAddress(from: location.coordinate) { [weak self] (locationDetail) in
            let updatedLocationDetail = (location.locationAddress!, locationDetail.cities, location.coordinate, locationDetail.country)
            self?.didSelectLocation?(updatedLocationDetail)
        }
//        self.didSelectLocation?(nil)
    }
}

extension FavoriteLocation {
    var coordinate: LocationCoordinate {
        return CLLocationCoordinate2D(latitude: self.lat ?? 0.0, longitude: self.lng ?? 0.0)
    }
}

class FavouriteAddressCell: UITableViewCell {
    
    @IBOutlet weak var favLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    
}

extension CacheManager {
    static var favouriteLocations: [FavoriteLocation] {
        return CacheManager.userInfo?.favoriteLocation ?? []
    }
    
    static var userInfo: UserInfo? {
        return CacheManager.sharedInstance.getObjectForKey(.UserInfo) as? UserInfo
    }
}

extension SearchLocationViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtfld_chooseloc {
            self.btnclk_Chooselocation()
            textField.resignFirstResponder()
        }
    }
}
