//
//  RideLaterView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
//

import Foundation
import UIKit

class RideLaterView: UIView {
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLineLbl: UILabel!
    @IBOutlet weak var pickupTimeLbl: UILabel!
    
    var selectedDateTime: String = Date().utcDateFromFormater("yyyy-MM-dd HH:mm:ss") ?? ""
    
    var chooseDateTime:(()->Void)?
    
    func releaseCallBacks() {
        self.chooseDateTime = nil
    }
    
    @IBAction func chooseDateTimeBtnAction(_ sender: Any) {
        self.chooseDateTime?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.timeLineLbl?.text = "Timeline".localized
    }
}
