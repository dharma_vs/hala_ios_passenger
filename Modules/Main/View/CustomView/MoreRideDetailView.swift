//
//  MoreRideDetailView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import Foundation
import UIKit

class MoreRideDetailView: UIStackView {
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var travelTimeLbl: UILabel!
    @IBOutlet weak var estimatedFareLbl: UILabel!
    @IBOutlet weak var taxesLbl: UILabel!
    
    @IBOutlet weak var distanceValueLbl: UILabel!
    @IBOutlet weak var travelTimeValueLbl: UILabel!
    @IBOutlet weak var estimatedFareValueLbl: UILabel!
    @IBOutlet weak var taxesValueLbl: UILabel!
    
    @IBOutlet weak var fareNoteLbl: UILabel!
    
    var showFareInfo:(()->Void)?
    
    func releaseCallBacks() {
        self.showFareInfo = nil
    }
    
    @IBAction func showFareInfoBtnAction(_ sender: Any) {
        self.showFareInfo?()
    }
}
