//
//  RideAddressView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import UIKit

class RideAddressView: UIStackView {
    @IBOutlet weak var sourceAddressLbl: UILabel!
    @IBOutlet weak var destinationAddressLbl: UILabel!
    @IBOutlet weak var rideFareAmountLbl: UILabel!

}
