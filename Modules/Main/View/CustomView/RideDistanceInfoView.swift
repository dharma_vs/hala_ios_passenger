//
//  RideDistanceInfoView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import UIKit

class RideDistanceInfoView: UIStackView {
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var distanceValue: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var durationValue: UILabel!
}
