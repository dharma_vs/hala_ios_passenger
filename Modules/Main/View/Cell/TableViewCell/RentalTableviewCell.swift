//
//  RentalTableviewCell.swift
//  HalaMobility
//
//  Created by ADMIN on 28/04/22.
//

import UIKit

class RentalTableviewCell: UITableViewCell {
    @IBOutlet weak var lblTitle: AppLabel!
    @IBOutlet weak var imgLogo: AppImageView!
    @IBOutlet weak var lblAddress: AppLabel!
    @IBOutlet weak var lblAvailablecount: AppLabel!
    @IBOutlet weak var lbltime: AppLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lbltime.addLeading(image:UIImage(named: "img_draw_time")!, text: " 31 Min")
    }
    func setValuesRentalcell(_ option: HomewayOption) {
        lblTitle?.text = option.rawValue.localized
        imgLogo?.image = option.iconImg
    }

}
