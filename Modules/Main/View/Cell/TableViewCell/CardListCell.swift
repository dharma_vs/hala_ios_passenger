//
//  CardListCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import UIKit
//import Stripe

class CardListCell: UITableViewCell {

    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblNo: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblBrand: UILabel!
    @IBOutlet var imgCard: UIImageView!
    @IBOutlet var btnDelete: UIButton!
    
    var didRequestToRemoveCard: (()->Void)?
    @IBAction func removeCard() {
        didRequestToRemoveCard?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setValues(_ card: Card_info?) {
        lblName?.text = card?.card_name
        lblNo?.text = card?.card_no?.cardNo
        lblDate?.text = card?.expiry_date?.expirs
        lblBrand?.text = card?.brand?.brand
//        imgCard?.image = STPImageLibrary.brandI mage(for: STPCardBrand(rawValue: card?.brand?.intValue() ?? 0) ?? .amex)
    }
}

extension String {
    var expirs: String? {
        return "Expires: \(self.prefix(2))/\(self.suffix(4))"
    }
    var brand: String? {
        return "Brand: \(self)"
    }
    var cardNo: String? {
        return "xxxx-xxxx-xxxx-\(self.suffix(4))"
    }
}
