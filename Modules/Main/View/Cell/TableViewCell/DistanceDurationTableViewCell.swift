//
//  DistanceDurationTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/04/22.
//

import Foundation
import UIKit

class DistanceDurationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var kmLbl: AppLabel!
    @IBOutlet weak var distanceLbl: AppLabel!
    @IBOutlet weak var timeLbl: AppLabel!
    @IBOutlet weak var durationLbl: AppLabel!

}
