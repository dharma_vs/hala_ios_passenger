//
//  HomeHeaderCollectionViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 21/02/22.
//

import UIKit

class HomeOptionsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var headerLbl: AppLabel!
    @IBOutlet weak var dashtripImg: AppImageView!
    @IBOutlet weak var footerLbl: AppLabel!
    
    weak var controller: UIViewController?
    
    var didRequestToSelect: ((HomeOptionsCollectionViewCell)->Void)?
    
    func setValues(_ option: HomeHeaderOption) {
        headerLbl?.text = option.info
        dashtripImg?.image = option.iconImg
        footerLbl?.text = option.rawValue.localized
        headerLbl?.isHidden = option.info == nil
    }
    
    @IBAction func selectCell(_ sender: UIButton) {
        didRequestToSelect?(self)
    }
}

