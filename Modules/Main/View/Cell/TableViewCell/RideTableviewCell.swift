//
//  RideTableviewCell.swift
//  HalaMobility
//
//  Created by ADMIN on 28/04/22.
//

import UIKit

class RideTableviewCell: UITableViewCell {
    @IBOutlet weak var lblTitle: AppLabel!
    @IBOutlet weak var imgLogo: AppImageView!
    @IBOutlet weak var lblAddress: AppLabel!
    @IBOutlet weak var lblCost: AppLabel!
    @IBOutlet weak var lblVehicleno: AppLabel!
    @IBOutlet weak var lblBattery: AppLabel!
    @IBOutlet weak var lblKM: AppLabel!
    var runningTimer: Timer?
    var rentalStatus: RentalStatus? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
       // lblVehicleno.addLeading(image:UIImage(named: "ic_vehicleno")!, text: "HM-0018")
        //lblBattery.addLeading(image:UIImage(named: "img_draw_battery")!, text: "0%")
        lblKM.addLeading(image:UIImage(named: "img_draw_distance")!, text: "You can travel upto 0 Km")
    }
    @objc func runningTimers() {
        GlobalValues.sharedInstance.GVtimer = GlobalValues.sharedInstance.GVtimer! + 1
        lblCost.text = GlobalValues.sharedInstance.GVtimer?.secondsToTime()
    }
    func setValuesRidecell(_ option: HomewayOption) {
        lblTitle?.text = option.rawValue.localized
        imgLogo?.image = option.iconImg
    }
    func setvaluesRideCellfromAPI(_ list: Vehiclelist?, detail:VehicleDetail?){
        GlobalValues.sharedInstance.GVtimer = 0
        lblAddress?.text = list?.displayName ?? ""
        imgLogo.loadTaxiImage(list?.image?.urlFromString)
        lblVehicleno.addLeading(image:UIImage(named: "ic_vehicleno")!, text: list?.vehicleNumber ?? "")
        let unlockcost =  Extension.updateCurrecyRate(fare: "\(String(detail?.fareBreakup?.unlockFee ?? 0))").currencyValue
        let perminute =  Extension.updateCurrecyRate(fare: "\(String(detail?.fareBreakup?.minute ?? 0))").currencyValue
        lblCost.text = "Unlock at".localized + unlockcost + "-" + perminute + "Per Min".localized
        let battery:Double = (list?.batteryMaxVoltage.calculateBatterpercentage(list: list!))!
        lblBattery.addLeading(image:UIImage(named: "img_draw_battery")!, text: "\(Int(battery))%")
    }
    
    func setvaluesRideCellfromAPI(_list:Rental1){
        lblAddress?.text = _list.vehicleTypeInfo?.displayName
        imgLogo.loadTaxiImage(_list.vehicleTypeInfo?.image.urlFromString)
        lblVehicleno.addLeading(image:UIImage(named: "ic_vehicleno")!, text: _list.vehicleInfo?.number ?? "")
        let startDate = _list.datetime!.toDateTime()
       // let  endDate = _list.startTime?.toDateTime()
        if GlobalValues.sharedInstance.currentRentalStatus == 6{
        let  endDate = _list.startTime?.toDateTime()
            GlobalValues.sharedInstance.GVtimer = Date.differencebetweendates(recent: startDate as Date, previous: endDate! as Date)
        runningTimer?.invalidate()
        runningTimer = nil
        runningTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runningTimers), userInfo: nil, repeats: true)
        }else{
            lblCost.text = rentalStatus?.status
        }
        let battery  = _list.vehicleInfo?.batteryMaxVoltage.calculateBatterpercentage(list:_list.vehicleInfo) ?? 0
        lblBattery.addLeading(image:UIImage(named: "img_draw_battery")!, text: "\(Int(battery))%")
        lblKM.isHidden = true
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        runningTimer?.invalidate()
        runningTimer = nil
    }
   }

