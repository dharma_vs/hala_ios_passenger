//
//  HomeFooterTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 21/02/22.
//

import UIKit

class HomeFooterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headerLbl: AppLabel!
    @IBOutlet weak var rentalLbl: AppLabel!
    @IBOutlet weak var subLbl: AppLabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tblTypes: UITableView!
    @IBOutlet var consttblHeight:NSLayoutConstraint!
    var didRequestToNavigateDashBoardWithOption: ((HomeFooterOption)->Void)?
    var stationlist: [Station]?
    var vehicleDet : [VehicleDetail]?
    var rentalmodel : Rental1?
    var vehiclelist: [Vehiclelist]? {
        didSet {
            
            tblTypes?.reloadData()
        }
    }
    /*var rideListItems = Rental1() {
        didSet {
           // tblTypes.reloadData()
        }
    }*/
   
    weak var controller: UIViewController?
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        consttblHeight.constant = CGFloat(HomewayOption.allCases.count * 100 + 20)
    }
}
extension HomeFooterTableViewCell: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            if rentalmodel != nil{ //ride going
                let header: RideTableviewCell = tableView.dequeueCell(indexPath)
                header.rentalStatus = RentalStatus.init(rawValue: rentalmodel?.bookingStatus?.intValue ?? 0)
                header.setvaluesRideCellfromAPI(_list: rentalmodel!)
                return header
            }else if rentalmodel == nil && vehiclelist?.first !=  nil{
                let header: RideTableviewCell = tableView.dequeueCell(indexPath)
                header.setvaluesRideCellfromAPI(vehiclelist?.first, detail: vehicleDet?.first)
                return header
            }
            else if rentalmodel?.id == nil && vehiclelist?.first ==  nil{ // Bring Hala button showing
                let header: BringhalaCell = tableView.dequeueCell(indexPath)
                header.didRequestToBringHalaOption = { [weak self] option in
                    print("Click bring Hala")
                    ServiceManager().requestBringHala { [weak self](inner) -> (Void) in
                        self?.controller?.showAlertWithMessage("updateshortly".localized, phrase1: "Ok".localized, action: UIAlertAction.Style.default, completion: { (style) in
                            if style == .default {
                                
                            }
                        })
                    }
                }
                return header
            }else{
                let header: RideTableviewCell = tableView.dequeueCell(indexPath)
                if vehiclelist == nil { // No ride available
                let values = HomewayOption.allCases[indexPath.row]
                header.setValuesRidecell(values)
                }else{
                    header.setvaluesRideCellfromAPI(vehiclelist?.first, detail: vehicleDet?.first)
                }
                return header
            }
        default:
            let cell: RentalTableviewCell = tableView.dequeueCell(indexPath)
            let values = HomewayOption.allCases[indexPath.row]
            cell.setValuesRentalcell(values)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return HomewayOption.allCases.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 120 : 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if rentalmodel?.id == nil && vehiclelist?.first ==  nil{ // Bring Hala button showing
            return
        }
        let clicktype = HomewayOption.allCases[indexPath.row]
        switch clicktype {
        case .Halaride:
            RideType = "2"
            if rentalmodel != nil{
                let unlockvc = UnlockVC.initFromStoryBoard(.Dashboard)
                unlockvc.vehicle = vehiclelist?.first
                unlockvc.vehicletypeinfo = rentalmodel?.vehicleTypeInfo
                unlockvc.modConfirmRental = rentalmodel
                self.controller?.pushViewController(unlockvc, type: UnlockVC.self)
            }else{
            let dashBoardVC = MapVC.initFromStoryBoard(.Dashboard)
            dashBoardVC.vehiclelist = vehiclelist
            dashBoardVC.stationlist = stationlist
            dashBoardVC.vehicleDet = vehicleDet
            dashBoardVC.isVehicle = true
            self.controller?.pushViewController(dashBoardVC, type: MapVC.self)
            }
        case .Halarental:
            RideType = "1"
            let dashBoardVC = MapVC.initFromStoryBoard(.Dashboard)
            self.controller?.pushViewController(dashBoardVC, type: MapVC.self)
            break
        case .Halaoperator:
            let dashBoardVC = MapVC.initFromStoryBoard(.Dashboard)
            self.controller?.pushViewController(dashBoardVC, type: MapVC.self)
            break
        case .HalaCharger:
            let dashBoardVC = MapVC.initFromStoryBoard(.Dashboard)
            self.controller?.pushViewController(dashBoardVC, type: MapVC.self)        }
    }
}
extension HomeFooterTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("numberOfItemsInSection : ", HomeFooterOption.allCases.count)
        return HomeFooterOption.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: FooterOptionsCollectionViewCell = collectionView.dequeueCell(indexPath)
        let option = HomeFooterOption.allCases[indexPath.item]
        cell.setValues(option)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = collectionView.bounds.size
        size.width /= CGFloat(HomeFooterOption.allCases.count)
        print("Size : ", size)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let option = HomeFooterOption.allCases[indexPath.item]
        self.didRequestToNavigateDashBoardWithOption?(option)
    }
    
}

enum HomeFooterOption: String, CaseIterable {
    case Taxi = "Taxi"
    case Delivery = "Delivery"
    
    var iconImg: UIImage? {
        switch self {
        case .Taxi:
            return #imageLiteral(resourceName: "img_menu_driver_vehicle").withRenderingMode(.alwaysTemplate)
        case .Delivery:
            return #imageLiteral(resourceName: "img_delivery_man").withRenderingMode(.alwaysTemplate)
        }
    }
    
    var info: String? {
        let userInfo = CacheManager.userInfo
        switch self {
        case .Taxi:
            return userInfo?.total_trips?.stringValue.doubleSpaced
        case .Delivery:
            return userInfo?.total_km_travelled?.stringValue.km.doubleSpaced
        }
    }
}

enum HomewayOption: String, CaseIterable {
    case Halaride = "Hala Ride"
    case Halarental = "Hala Rental"
    case Halaoperator = "Operator"
    case HalaCharger = "Hala Charger"
    
    var iconImg: UIImage? {
        switch self {
        case .Halaride:
            return #imageLiteral(resourceName: "img_menu_driver_vehicle").withRenderingMode(.alwaysTemplate)
        case .Halarental:
            return #imageLiteral(resourceName: "img_delivery_man").withRenderingMode(.alwaysTemplate)
        case .Halaoperator:
            return #imageLiteral(resourceName: "img_Operator").withRenderingMode(.alwaysTemplate)
        case .HalaCharger:
            return #imageLiteral(resourceName: "img_Charger").withRenderingMode(.alwaysTemplate)
        }
    }
    
    var info: String? {
        let userInfo = CacheManager.userInfo
        switch self {
        case .Halaride:
            return userInfo?.total_trips?.stringValue.doubleSpaced
        case .Halarental:
            return userInfo?.total_km_travelled?.stringValue.km.doubleSpaced
        case .Halaoperator:
            return userInfo?.total_trips?.stringValue.doubleSpaced
        case .HalaCharger:
            return userInfo?.total_km_travelled?.stringValue.km.doubleSpaced
        }
    }
}
