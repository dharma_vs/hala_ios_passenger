//
//  FooterOptionsCollectionView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 21/02/22.
//

import UIKit

class FooterOptionsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var deliverymanImg: AppImageView!
    @IBOutlet weak var deliveryLbl: AppLabel!
    
    func setValues(_ option: HomeFooterOption) {
        deliveryLbl?.text = option.rawValue.localized
        deliverymanImg?.image = option.iconImg
    }
}
