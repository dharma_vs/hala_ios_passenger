//
//  PickupDeliveryTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/04/22.
//

import Foundation
import UIKit

class PickupDeliveryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pickUpLbl: AppLabel!
    @IBOutlet weak var pickUpNameLbl: AppLabel!
    @IBOutlet weak var pickUpMobileNoLbl: AppLabel!
    @IBOutlet weak var pickUpAddressLbl: AppLabel!
    @IBOutlet weak var deliverLbl: AppLabel!
    @IBOutlet weak var deliverNameLbl: AppLabel!
    @IBOutlet weak var deliverMobileNoLbl: AppLabel!
    @IBOutlet weak var deliverAddressLbl: AppLabel!
    @IBOutlet weak var pickUpLocationImg: AppImageView!
    @IBOutlet weak var dropLocationImg: AppImageView!

    
}
