//
//  HomeHeaderTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 21/02/22.
//

import UIKit

class HomeHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLbl: AppLabel!
    @IBOutlet weak var homebackImg: AppImageView!
    @IBOutlet weak var collectionView: UICollectionView!

    weak var controller: UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let userInfo = CacheManager.userInfo
        nameLbl.text = "Hi" + " " + (userInfo?.name)!
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension HomeHeaderTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return HomeHeaderOption.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: HomeOptionsCollectionViewCell = collectionView.dequeueCell(indexPath)
        let option = HomeHeaderOption.allCases[indexPath.item]
        cell.setValues(option)
        cell.didRequestToSelect = { [weak self] cell in
            switch option {
            case .Trips:
                let vc = YourHistoryVC.initFromStoryBoard(.Main)
                self?.controller?.pushViewController(vc, type: YourHistoryVC.self)
            case .KMTraveled:
                break
            case .CO2Saved:
                break
            case .MyWallet:
                let vc = WalletHistoryViewController.initFromStoryBoard(.Payment)
                self?.controller?.pushViewController(vc, type: WalletHistoryViewController.self)
            case .Buy:
                break
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = collectionView.bounds.size
        size.width /= 5
//        size.width = 76.8
        return size
    }
    
}


enum HomeHeaderOption: String, CaseIterable {
    case Trips = "Trips"
    case KMTraveled = "KM Traveled"
    case CO2Saved = "CO2 Saved"
    case MyWallet = "My Wallet"
    case Buy = "Buy"
    
    var iconImg: UIImage? {
        switch self {
        case .Trips:
            return #imageLiteral(resourceName: "img_dash_trip")
        case .KMTraveled:
            return #imageLiteral(resourceName: "img_dash_km_travel")
        case .CO2Saved:
            return #imageLiteral(resourceName: "img_dash_co2")
        case .MyWallet:
            return #imageLiteral(resourceName: "img_dash_wallet")
        case .Buy:
            return #imageLiteral(resourceName: "img_dash_buy")
        }
    }
    
    var info: String? {
        let userInfo = CacheManager.userInfo
        switch self {
        case .Trips:
            return userInfo?.total_trips?.stringValue.doubleSpaced
        case .KMTraveled:
            return userInfo?.total_km_travelled?.stringValue.km.doubleSpaced
        case .CO2Saved:
            return userInfo?.total_trips?.stringValue.kgs.doubleSpaced
        case .MyWallet:
            return "\(Extension.updateCurrecyRate(fare: "\(userInfo?.walletAmount?.doubleValue ?? 0.0)"))".currencyValue.doubleSpaced
        case .Buy:
            return nil
        }
    }
}

extension String {
    var kgs: String {
        return self + " " + "kgs"
    }
}
