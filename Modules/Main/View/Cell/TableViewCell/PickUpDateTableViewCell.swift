//
//  PickUpDateTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/04/22.
//

import Foundation
import UIKit

class PickUpDateTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var calenderImg: AppImageView!
    @IBOutlet weak var dateLbl: AppLabel!
    @IBOutlet weak var timeLbl: AppLabel!

}
