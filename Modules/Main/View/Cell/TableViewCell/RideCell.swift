//
//  RideCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import UIKit

class RideCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var rideFareLbl: UILabel!
    @IBOutlet weak var carImage: UIImageView!
    
    var showCarInfo:(()->Void)?
    
    @IBAction func carInfoBtnAction(_ sender: Any) {
        showCarInfo?()			
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        titleLabel.text = ""
        descLabel.text = ""
        rideFareLbl.text = ""
//        carImage.image = nil
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
}
