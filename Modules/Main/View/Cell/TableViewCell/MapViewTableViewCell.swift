//
//  MapViewTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 21/02/22.
//

import UIKit
import GoogleMaps

class MapViewTableViewCell: UITableViewCell {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var mapImgView: AppImageView!
    weak var controller: UIViewController?
    let helper = GoogleMapsHelper()
    
    var stationlist: [Station]? {
        didSet {
            for index in 0..<stationlist!.count {
                self.addmarker(latitude: stationlist?[index].latitude ?? 0.0, longitude: stationlist?[index].longitude ?? 0.0,vehiclecount: stationlist?[index].vehicle_count ?? 0)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func addmarker(latitude: Double, longitude: Double, vehiclecount: Int = 0) {
        let marker = GMSMarker()
        let markerImage = UIImage(named: vehiclecount == 0 ? "img_marker_station" : "img_marker_parking_slot")!.withRenderingMode(.alwaysOriginal)
        //creating a marker view
        let markerView = UIImageView(image: markerImage)
        markerView.frame = CGRect(x: 0.0, y: markerView.center.y, width: 34, height: 43)
        //changing the tint color of the image
        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        marker.iconView = markerView
        marker.title = vehiclecount == 0 ? "Station".localized : "Parking-slot".localized
        marker.map = mapView
        
}
    func setCurrentLocationToCameraPosition() {
        self.helper.getCurrentLocation { [weak self] (location) in
            let camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 15.0)
            self?.mapView?.animate(to: camera)
        }
    }
}
