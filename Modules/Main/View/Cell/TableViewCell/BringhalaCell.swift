//
//  BringhalaCell.swift
//  HalaMobility
//
//  Created by ADMIN on 20/05/22.
//

import UIKit

class BringhalaCell: UITableViewCell {
    @IBOutlet weak var lblTitle: AppLabel!
    @IBOutlet weak var lblSubtitle: AppLabel!
    @IBOutlet weak var btnBringHala: AppButton!
    var didRequestToBringHalaOption: ((Bool)->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        lblTitle.text = "Hala Ride".localized
        lblSubtitle.text = "UNAVAILABLE RIDER".localized
        btnBringHala.setTitle("Bring Hala".localized.tripleSpaced, for: .normal)
    }
    @IBAction func bringhalaBtn(_ sender: AppButton) {
        print("bringhalaBtn")
        self.didRequestToBringHalaOption?(true)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
