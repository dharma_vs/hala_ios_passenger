//
//  VehicleInfoTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/04/22.
//

import Foundation
import UIKit

class VehicleInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var vehicleImg: AppImageView!
    @IBOutlet weak var streetLbl: AppLabel!
    @IBOutlet weak var driversLbl: AppLabel!
    @IBOutlet weak var amountLbl: AppLabel!

}
