//
//  RideCellTheme1.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
//

import UIKit

class RideCellTheme1: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var rideFareLbl: UILabel!
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var etaImage: UIImageView!
    
    @IBOutlet var bgView: UIView!
    var showCarInfo:(()->Void)?
    
    @IBAction func carInfoBtnAction(_ sender: Any) {
        showCarInfo?()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        titleLabel.text = ""
        descLabel.text = ""
        rideFareLbl.text = ""
        //        carImage.image = nil
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
}
