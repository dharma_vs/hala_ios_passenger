//
//  PaymentMethodTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/04/22.
//

import Foundation
import UIKit

class PaymentMethodTableViewCell: UITableViewCell {
    
    @IBOutlet weak var paymentMethodLbl: AppLabel!
    @IBOutlet weak var dropdownBtn: AppButton!
    @IBOutlet weak var downArrowImg: AppImageView!
    @IBOutlet weak var cashLbl: AppLabel!
    @IBOutlet weak var confirmBookingBtn: AppButton!
    
    @IBAction func dropdownBtn(_ sender: AppButton) {
        
    }
    @IBAction func confirmBookingBtn(_ sender: AppButton) {
        
    }
}
