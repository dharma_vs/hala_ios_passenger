//
//  ItemInfoTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/04/22.
//

import Foundation
import UIKit

class ItemInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var itemInfoLbl: AppLabel!
    @IBOutlet weak var additionalInfoLbl: AppLabel!
    @IBOutlet weak var additionalInfoDetailsLbl: AppLabel!
    
}
