//
//  VehicleInfoViewController.swift
//  TaxiPickup
//
//  Created by SELLADURAI on 3/21/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import Foundation
import UIKit

class VehicleInfoViewController: BaseViewController {
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var detailLbl: UILabel!
    
    @IBOutlet weak var detailValueLbl: UILabel!
    
    @IBOutlet weak var okBtn: UIButton!
    
    @IBOutlet weak var categoryImageView: UIImageView!
    
    var category: Category?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("category : ", category)
        //.loadTaxiImage(category.image?.urlFromString)
        
        self.name.text = category?.displayName
        self.categoryImageView.loadTaxiImage(category?.image?.urlFromString)
        self.categoryImageView.layer.borderColor = ColorTheme(rawValue: 1)?.color.cgColor
        self.categoryImageView.layer.borderWidth = 1.0
        self.categoryImageView.layer.cornerRadius = 16
        self.categoryImageView.clipsToBounds = true
        
        //\n\n\("Rate per km".localized)
        self.detailLbl?.text = "\("Base Fare".localized) (\(self.category?.fareBreakup?.minimumDistance?.intValue ?? 0) km)\n\n\("Rate per km")\n\n\("Ride Time Charge Per min".localized)\n\n\("Waiting fee per min".localized) (\("After XXX min".localized.replacingOccurrences(of: "XXX", with: self.category?.fareBreakup?.freeWaitingTime?.stringValue ?? "")))"
        //\n\n\("\(self.category?.fareBreakup?.costPerDistance ?? 0.0)".currencyValue)
        self.detailValueLbl.text = "\("\(Extension.updateCurrecyRate(fare: "\(self.category?.fareBreakup?.baseFare ?? 0.0)"))".currencyValue)\n\n\(self.category?.fareBreakup?.costPerDistance ?? 0.0)\n\n\(Extension.updateCurrecyRate(fare: "\(self.category?.fareBreakup?.rideCostPerMinute ?? 0.0)").currencyValue)\n\n\((Extension.updateCurrecyRate(fare: "\(self.category?.fareBreakup?.waitingPricePerMin ?? 0.0)")).currencyValue)"

        self.detailLbl.textColor = .colorBlack
        self.detailValueLbl.textColor = .colorBlack
        self.okBtn.setTitleColor(.colorWhite, for: .normal)
    }
}
