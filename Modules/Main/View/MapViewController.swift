//
//  MapViewController.swift
//  TaxiPickup
//
//  Created by TAMILARASAN on 26/02/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit
import GoogleMaps
import Haneke

class MapViewController: BaseViewController {
    
    deinit {
        print("Map VC deinits")
    }
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var locationMarker: UIImageView!
    @IBOutlet weak var selectLocationBtn: UIButton!
    
    @IBAction func selectLocationBtnAction(_ sender: Any) {
        self.didSelectLocation?(self.selectedLocationDetail)
    }
    
    var didSelectLocation:((LocationDetail?)->Void)?
    
    var selectedLocationDetail: LocationDetail?
    
    //Mapview variables
    var currentLocation: CLLocation?
    var zoomLevel: Float = 15.0
    var isMarkerTapped: Bool = false
    var isCurrentLocation : Bool = false
    
    var setOnce: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        mapView.settings.myLocationButton = false
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        
        if let currentLocation = LocationManager.shared.currentLocation {
            self.setLocationOnMap(currentLocation)
        }
        locationMarker?.image = #imageLiteral(resourceName: "img_contact_address").withRenderingMode(.automatic)//xxxx
    }
    
    @IBAction func moveCurrentLocation(_ sender: UIButton) {
        if let currentLocation = LocationManager.shared.currentLocation {
            self.setLocationOnMap(currentLocation)
        }
    }
    
    override func localize() {
        lbl_title.text = "chooseLocation".localized
        selectLocationBtn.setTitle("Select Location".localized.tripleSpaced, for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        LocationManager.shared.delegate = self
        if let settings = CacheManager.settings {
//            locationMarker.loadTaxiImage(settings.destination_marker?.urlFromString, placeHolder: #imageLiteral(resourceName: "TO"))
        }
    }
    
    
    @IBAction func btnclk_back() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
extension MapViewController: LocationServiceDelegate {
    
    func tracingLocationDidFailWithError(error: NSError) {
        //Geeting location error
        //UtilsManager.shared.showAlert(message: error.localizedDescription)
        
    }
    
    func tracingLocation(userLocation: CLLocation) {
        let location: CLLocation = userLocation
        isCurrentLocation = true
        self.setLocationOnMap(location)
        currentLocation = userLocation
    }
    
    func setLocationOnMap(_ location: CLLocation) {
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            if setOnce == false {
                setOnce = true
                mapView.animate(to: camera)
            }
        }
    }
}



extension MapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        return false
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        GoogleMapsHelper().getPlaceAddress(from: position.target) { [weak self](locationDetail) in
            DispatchQueue.main.async {
                self?.lbl_Address?.text = locationDetail.address
                self?.selectedLocationDetail = locationDetail
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        //
    }
}

extension UIImageView {
    
    func loadTaxiImage(_ url: URL?, placeHolder: UIImage? = nil) {
        if let _url = url {
            self.hnk_setImage(from: _url)
//            self.hnk_setImage(from: _url, placeholder: placeHolder, success: { (image) in
//                DispatchQueue.main.async { [weak self] in
//                 self?.image = image
//                }
//            }, failure: nil)
        }
    }
    
}

extension String {
    
    var urlFromString: URL? {
        if self.hasPrefix("http") == false {
            return URL(string: (baseImageURL + self).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
        }
        return URL.init(string: self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
    }
    
}
