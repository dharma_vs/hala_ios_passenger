//
//  ItemInformationViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 02/04/22.
//

import UIKit

class ItemInformationViewController: BaseViewController {

    @IBOutlet weak var itemInfoLbl: AppLabel!
    @IBOutlet weak var itemInfotxtField: AppTextField!
    @IBOutlet weak var addBtn: AppButton!
    @IBOutlet weak var additionalInfoLbl: AppLabel!
    @IBOutlet weak var additionalInfoTxtView: AppTextView!
    @IBOutlet weak var submitBtn: AppButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func addBtn(_ sender: AppButton) {
        
    }
    @IBAction func submitBtn(_ sender: AppButton) {
        
    }
    
}
