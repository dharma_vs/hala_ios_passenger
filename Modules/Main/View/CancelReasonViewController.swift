//
//  CancelReasonViewController.swift
//  TaxiPickup
//
//  Created by SELLADURAI on 3/27/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit
import CoreLocation

class CancelReasonViewController: BaseViewController {
    
    @IBOutlet weak var selectReasonLbl: UILabel!
    
    @IBOutlet weak var doNotCancelBtn: UIButton!
    
    @IBOutlet weak var cancelBtn: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    var locatioDetails :  LocationDetail?
    var templocationDetails :  LocationDetail?

    var selectedReason: CancelReason? {
        didSet {
            self.tableView?.reloadData()
        }
    }
    
    var rideDetail: RideDetail? {
        didSet{
            //            self.tableView?.reloadData()
        }
    }
    
    var cancelReasons = [CancelReason]() {
        didSet {
            self.tableView?.reloadData()
        }
    }
    
    var didCancelRide:(()->Void)?
    
    var didRequestToCancelRide: ((_ rideDetail: RideDetail?, _ reason: String, _ sourceLocation: CLLocationCoordinate2D) -> Void)?
    
    @IBAction func cancelRide(_ sender: Any) {
        guard let reason = self.selectedReason else {
            self.view.makeToast("Please choose reason to cancel".localized)
            return
        }
        if locatioDetails == nil {
            templocationDetails?.coordinate = CLLocationCoordinate2D(latitude: rideDetail?.estimated_address?.source?.latitude ?? 0.0, longitude: rideDetail?.estimated_address?.source?.longitude ?? 0.0)

        }

        didRequestToCancelRide?(self.rideDetail, reason.reason_name ?? "", (locatioDetails != nil ? CLLocationCoordinate2D(latitude: locatioDetails?.coordinate.latitude ?? 0.0, longitude: locatioDetails?.coordinate.longitude ?? 0.0) : CLLocationCoordinate2D(latitude: rideDetail?.estimated_address?.source?.latitude ?? 0.0, longitude: rideDetail?.estimated_address?.source?.longitude ?? 0.0)))
        
        /*
        self.startActivityIndicatorInWindow()
        ServiceManager().cancelRide(createRide: false, rideDetail, reason: reason.reason_name ?? "",sourceLocation: (locatioDetails != nil ? CLLocationCoordinate2D(latitude: locatioDetails?.coordinate.latitude ?? 0.0, longitude: locatioDetails?.coordinate.longitude ?? 0.0) : CLLocationCoordinate2D(latitude: rideDetail?.estimated_address?.source?.latitude ?? 0.0, longitude: rideDetail?.estimated_address?.source?.longitude ?? 0.0))) {[weak self] (inner) -> (Void) in
            do {
                let _ = try inner()
                //                self?.view.makeToast(response.message)
                self?.didCancelRide?()
                self?.backPressed()
            } catch {
                //                self?.view.makeToast((error as? CoreError)?.description)
            }
            self?.stopActivityIndicator()
        }
        */
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startActivityIndicatorInWindow()
        ServiceManager().cancelReason { [weak self](inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
                let reasons = try inner()
                self?.cancelReasons = reasons.filter{$0.reason_for?.stringValue == self?.rideDetail?.category_id ?? "2"}
            } catch _ { }
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()
        
    }
    
    override func localize() {
        self.selectReasonLbl.text = "Select Reason".localized
        self.doNotCancelBtn.setTitle("Do not Cancel".localized, for: UIControl.State())
        self.cancelBtn.setTitle("Cancel".localized.spaced, for: UIControl.State())
    }
    
}

extension BaseViewController {
    func cancelRide(rideDetail: RideDetail?, reason: String, sourceLocation: CLLocationCoordinate2D) {
        self.startActivityIndicatorInWindow()
        ServiceManager().cancelRide(createRide: false, rideDetail, reason: reason, sourceLocation: sourceLocation) {[weak self] (inner) -> (Void) in
            do {
                let _ = try inner()
                //                self?.view.makeToast(response.message))
                NotificationCenter.default.post(Notification.init(name: Notification.Name.init("CHECK_RIDE_UPDATES")))
                self?.backPressed()
            } catch {
                //                self?.view.makeToast((error as? CoreError)?.description)
            }
            self?.stopActivityIndicator()
        }
    }
}

extension CancelReasonViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cancelReasons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CancelReasonCell", for: indexPath) as! CancelReasonCell
        let reason = self.cancelReasons[indexPath.row]
        cell.reasonLbl.text = reason.reason_name?.localized
        cell.reasonLbl.numberOfLines = 0
        cell.selectionImage.image = self.selectedReason?.reason_name == reason.reason_name ? #imageLiteral(resourceName: "round_select") : #imageLiteral(resourceName: "round_unselect")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedReason = self.cancelReasons[indexPath.row]
    }
    
}

struct CancelReason: JSONSerializable {
    let id: Int?
    let reason_name: String?
    var reason_for: CustomType?
}

class CancelReasonCell: UITableViewCell {
    
    @IBOutlet weak var reasonLbl: UILabel!
    
    @IBOutlet weak var selectionImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
