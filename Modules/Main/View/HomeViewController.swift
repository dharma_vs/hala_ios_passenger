//
//  HomeViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 17/02/22.
//

import UIKit
import CoreLocation

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var menuBtn: AppButton!
    @IBOutlet weak var dashboardmembershipBtn: AppButton!
    @IBOutlet weak var customercareBtn: AppButton!
    @IBOutlet weak var tableView: UITableView!
    var fetchDetail : Rental1?
    var getStation: [Station]?
    var vehiclelist: [Vehiclelist]?
    var location :  CLLocation?
    var vehicleDet : [VehicleDetail]?
    let helper = GoogleMapsHelper()
    
    var rentalCheckHelper: RentalCheckHelper?
    var currentRideStatus: RentalStatus? = nil {
        willSet {
            print("test")
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        if let currentLocation = LocationManager.shared.currentLocation {
//            self.location = currentLocation
//            self.fetchAPI()
//        }
        helper.getCurrentLocation { loocation in
            self.location = loocation
            self.fetchAPI()
         }
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: String.LogoutNotification), object: nil, queue: nil) { [weak self] (note) in
            self?.showAlertWithMessage("Are you sure want to logout?".localized, phrase1: "Yes".localized, phrase2: "No".localized, action: UIAlertAction.Style.default, UIAlertAction.Style.cancel, completion: { (style) in
                if style == .default {
                    self?.logout()
                }
            })
        }
        registerXIBs()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationSetup()
        if GlobalValues.sharedInstance.currentRentalStatus != 0 || GlobalValues.sharedInstance.currentRentalStatus != 1 { 
            self.checkRentalList()
        }
    }
 //MARK: API'S
    func checkRentalList() {
        if rentalCheckHelper == nil {
            self.rentalCheckHelper = RentalCheckHelper()
        }
        self.rentalCheckHelper?.startListening(on: { [weak self] (error, rentallist) in
            guard error == nil else {
                return
            }
            if rentallist == nil{
                self?.rentalCheckHelper?.stopListening()
                GlobalValues.sharedInstance.currentRentalStatus = 0
                GlobalValues.sharedInstance.previousRentalStatus = 0
            }else{
                self?.fetchDetail = rentallist
                let rideStatus = self?.fetchDetail?.bookingStatus?.intValue
                self?.currentRideStatus = RentalStatus.init(rawValue: rideStatus ?? 0)!
                GlobalValues.sharedInstance.currentRentalStatus = rideStatus ?? 0
            }
            //            let model = DashboardViewModel(rentallist)
            //            model.controller = self
            //            self?.viewModel = model
            //self?.rideCountBtn.isHidden = ((rideList?.rides?.count) ?? 0) <= 1 ? true : false
            //self?.rideCountBtn.setTitle("\(((rideList?.rides?.count) ?? 0).stringValue)", for: .normal)
        })
    }
    func mqttAPI(){
        self.startActivityIndicator()
        ServiceManager().mqttapiRequest(fleetno: (self.fetchDetail?.fleetNo!.stringValue)!, completion: {[weak self] (inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
                let response = try inner()
                print("MQTT",response?.data.first?.fid ?? "")
            } catch {}
        })
    }
    func fetchAPI(){
        self.startActivityIndicator()
        ServiceManager().fetchapiRequest{ [weak self] (inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
                self?.fetchDetail = try inner()
                if self?.fetchDetail != nil {
                    if let station = self?.fetchDetail?.pickupStation {
                        self?.getStation = [station]
                        StationID = self?.getStation?.first?.id ?? 0
                    }
                    if let vehiclelist = self?.fetchDetail?.vehicleInfo {
                        self?.vehiclelist = [vehiclelist]
                    }
                    if let vehicledetails = self?.fetchDetail?.vehicleTypeInfo {
                        self?.vehicleDet = [vehicledetails]
                    }
                    CategoryID = self?.vehiclelist?.first?.categoryID ?? 0
                    GlobalValues.sharedInstance.currentRentalStatus = (self?.fetchDetail?.bookingStatus?.intValue)!
                    self?.tableView.reloadData()
                    if GlobalValues.sharedInstance.currentRentalStatus == 6{
                    self?.checkRentalList()
                    }
                    print("test")
                }else{
                    print("test1")
                    self?.startActivityIndicator()
                    //self?.location = LocationManager.shared.currentLocation
                    self?.getCategoryStationAPI()
                }
            } catch {
                print("test2")
            }
        }
    }
    func getCategoryStationAPI(){
        let params:[String : Any] = ["type" : RideType,"latitude" : location?.coordinate.latitude ?? 0.0,"longitude" : location?.coordinate.longitude ?? 0.0,"all":0]
        ServiceManager().categorystationapiRequest(params , completion: {[weak self] (inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
                let response = try inner()
                self?.getStation = response?.data
                if self?.getStation != nil{
                    StationID = self?.getStation?.first?.id ?? 0
                    self?.tableView?.reloadRows(at: [IndexPath(row: 1, section: 0)], with: UITableView.RowAnimation.none)
                    self?.vehiclelistAPI()
                }
            } catch {}
        })
    }
    func vehiclelistAPI(){
        ServiceManager().vehiclelistapiRequest( completion: { [weak self] (inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
              let response = try inner()
                self?.vehiclelist = response?.data
                if self?.vehiclelist !=  nil{
                    StationID = self?.getStation?.first?.id ?? 0
                    CategoryID = self?.vehiclelist?.first?.categoryID ?? 0
                    self?.categoryAPI()
                }
            }catch{}
        })
    }
    func categoryAPI(){
        ServiceManager().categoryapiRequest( completion: { [weak self] (inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
              let response = try inner()
                self?.vehicleDet = response?.data
                if self?.vehicleDet !=  nil{
                    self?.tableView?.reloadRows(at: [IndexPath(row: 2, section: 0)], with: UITableView.RowAnimation.none)
                }
            }catch{}
        })
    }
    private func registerXIBs() {
        tableView.register(UINib(nibName: "HomeFooterTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "homeFooter")
    }
    
    @IBAction func menuBtn(_ sender: AppButton) {
        
    }
    
    @IBAction func dashboardmembershipBtn(_ sender: AppButton) {
        
    }
    
    @IBAction func customercareBtn(_ sender: AppButton) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.rentalCheckHelper?.stopListening()
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let header: HomeHeaderTableViewCell = tableView.dequeueCell(indexPath)
            header.controller = self
            return header
        case 1:
            let cell: MapViewTableViewCell = tableView.dequeueCell(indexPath)
            cell.controller = self
            if self.getStation != nil{
                cell.stationlist = self.getStation
            }
            cell.setCurrentLocationToCameraPosition()
            return cell
        case 2:
            let cell: HomeFooterTableViewCell = tableView.dequeueCell(indexPath)
            cell.controller = self
            if fetchDetail != nil{
                cell.rentalmodel = fetchDetail
            }
            cell.vehiclelist = self.vehiclelist
            cell.vehicleDet = self.vehicleDet
            if self.getStation?.count != 0{
                cell.stationlist = self.getStation
            }
            cell.didRequestToNavigateDashBoardWithOption = { [weak self] option in
                switch option {
                case .Taxi:
                    let dashBoardVC = DashboardViewController.initFromStoryBoard(.Dashboard)
                    self?.pushViewController(dashBoardVC, type: DashboardViewController.self)
                case .Delivery:
                    let dashBoardVC = DashboardViewController.initFromStoryBoard(.Dashboard)
                    self?.pushViewController(dashBoardVC, type: DashboardViewController.self)
                }
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1{
            let dashBoardVC = MapVC.initFromStoryBoard(.Dashboard)
            self.pushViewController(dashBoardVC, type: MapVC.self)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
}
extension UILabel {
    func addTrailing(image: UIImage, text:String) {
        let attachment = NSTextAttachment()
        attachment.image = image
        let attachmentString = NSAttributedString(attachment: attachment)
        let string = NSMutableAttributedString(string: text, attributes: [:])
        string.append(attachmentString)
        self.attributedText = string
    }
    
    func addLeading(image: UIImage, text:String) {
        let attachment = NSTextAttachment()
        attachment.image = image
        attachment.bounds = CGRect(x: 0, y: -3, width: 15, height: 15)
        let attachmentString = NSAttributedString(attachment: attachment)
        let mutableAttributedString = NSMutableAttributedString()
        mutableAttributedString.append(attachmentString)
        let string = NSMutableAttributedString(string: " " + text, attributes: [:])
        mutableAttributedString.append(string)
        self.attributedText = mutableAttributedString
    }
}
