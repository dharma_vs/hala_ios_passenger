//
//  YourHistoryVC.swift
//  HalaMobility
//
//  Created by ADMIN on 29/04/22.
//

import UIKit

class YourHistoryVC: BaseViewController {
    @IBOutlet weak var btnTaxi: UIButton!
    @IBOutlet weak var btnRental: UIButton!
    @IBOutlet weak var btnBuy: UIButton!
    @IBOutlet weak var btnUpcoming: UIButton!
    @IBOutlet weak var btnPast: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var stackbottom: UIStackView!
    @IBOutlet weak var borderViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var constTopborderLeading: NSLayoutConstraint!
    @IBOutlet weak var contentCollectionView: UICollectionView!
    @IBOutlet weak var constBottomviewHeight: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        // Do any additional setup after loading the view.
    }
    
    override func localize() {
        self.titleLbl?.text = "Your Trips".localized
        self.btnUpcoming?.setTitle("Upcoming".localized, for: .normal)
        self.btnPast?.setTitle("Past".localized, for: .normal)
        self.btnTaxi?.setTitle("Taxi/Delivery".localized, for: .normal)
        self.btnRental?.setTitle("Rental/Ride".localized, for: .normal)
        self.btnBuy?.setTitle("Buy/Test".localized, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
        
    }
    @IBAction func btnclk_Taxi() {
        self.constTopborderLeading.constant = 0
       // stackbottom.isHidden = false
        constBottomviewHeight.constant = 44
        btnUpcoming.isHidden = false
        btnPast.isHidden = false
        self.btnUpcoming?.setTitle("Opened".localized, for: .normal)
        self.btnPast?.setTitle("Past".localized, for: .normal)
    }
    @IBAction func btnclk_Rental() {
        self.constTopborderLeading.constant = btnRental.frame.origin.x
        constBottomviewHeight.constant = 0
        btnUpcoming.isHidden = true
        btnPast.isHidden = true
    }
    @IBAction func btnclk_Buy() {
        self.constTopborderLeading.constant = btnBuy.frame.origin.x
        constBottomviewHeight.constant = 44
        btnUpcoming.isHidden = false
        btnPast.isHidden = false
        self.btnUpcoming?.setTitle("Buy".localized, for: .normal)
        self.btnPast?.setTitle("Test Drive".localized, for: .normal)
    }
    @IBAction func showUpcomingTrips(_ sender: Any) {
        if let currentIndexPath = self.contentCollectionView?.indexPathsForVisibleItems.first, currentIndexPath.item != 0 {
            self.contentCollectionView?.scrollToItem(at: IndexPath.init(item: 0, section: 0), at: UICollectionView.ScrollPosition.left, animated: true)
            if let cell = self.contentCollectionView?.cellForItem(at: IndexPath.init(item: 0, section: 0)) as? TripCollectionViewCell {
                cell.rideListItems = []
                cell.paginator = Paginator()
                cell.loadData(type: .open )
            }
        }
    }
    
    @IBAction func showPastTrips(_ sender: Any) {
        if let currentIndexPath = self.contentCollectionView?.indexPathsForVisibleItems.first, currentIndexPath.item == 0 {
            self.contentCollectionView?.scrollToItem(at: IndexPath.init(item: 1, section: 0), at: UICollectionView.ScrollPosition.left, animated: true)
            if let cell = self.contentCollectionView?.cellForItem(at: IndexPath.init(item: 0, section: 0)) as? TripCollectionViewCell {
                cell.rideListItems = []
                cell.paginator = Paginator()
                cell.loadData(type: .close )
            }
        }
    }
}

extension YourHistoryVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TripCollectionViewCell", for: indexPath) as! TripCollectionViewCell
        cell.tripsTableView.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let tmpCell =  cell as? TripCollectionViewCell {
            tmpCell.activityIndicator = { [weak self](show) -> Void in
                show ? self?.startActivityIndicatorInWindow() : self?.stopActivityIndicator()
            }
            tmpCell.rideListItems = []
            tmpCell.paginator = Paginator()
            tmpCell.loadData(type: indexPath.item == 0 ? .open : .close)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset
        self.borderViewLeadingConstraint.constant = contentOffset.x/2
    }
}
