//
//  RideFeedbackViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import UIKit


class RideFeedBackViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {


    @IBOutlet weak var feedBackTableView: UITableView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var billAmountHeadingLbl: UILabel!
    @IBOutlet weak var billAmountLbl: UILabel!

    var driverName = ""
    var vechileType = ""
    var currentRideID = ""

    var driverImageUrl : URL!
    var currentRide: RideDetail?
    var currentRideId: Int?

    var updateRide:(()->Void)?
    var feedBackDic = NSDictionary()
    var isFeedbackBool = Bool()
    var feedBackTimer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ServiceManager().fetchRideDetails("\(self.currentRide?.id ?? self.currentRideId ?? 0)", reason: "") { [weak self] (inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
                let rideList = try inner()
                self?.currentRide = rideList?.rides?.first
                self?.updateUI()
            } catch {}
        }

        updateUI()
        
    }
    
    func updateUI() {
        self.feedBackListApiCall()
        self.feedBackTableView.tableFooterView = UIView()
        driverName = currentRide?.driver_info?.name ?? ""
        vechileType = currentRide?.vehicle_type_info?.display_name ?? ""

        self.billAmountLbl.text =  Extension.updateCurrecyRate(fare: "\(String(currentRide?.trip_info?.amount ?? 0))").currencyValue
        
        driverImageUrl = currentRide?.driver_info?.image?.urlFromString

       // NotificationCenter.default.addObserver(self, selector: #selector(updateRideFeedBack(notification:)), name: NSNotification.Name(rawValue: "currentridefeedback"), object: nil)
        feedBackTimer?.invalidate()
        feedBackTimer = nil
        feedBackTimer = Timer.scheduledTimer(timeInterval: 7, target: self, selector: #selector(UpdateFeedBackList), userInfo: nil, repeats: true)
        self.currentRideID = self.currentRide?.rideId ?? ""
        UpdateFeedBackList()
    }
    
    @objc func UpdateFeedBackList(){
        ServiceManager().fetchRideDetails(self.currentRideID, reason: "") { [weak self] (inner) -> (Void) in
            do {
                let rideList = try inner()
                   self?.stopActivityIndicator()
                if rideList?.rides?.count ?? 0 > 0{
                    self?.billAmountLbl.text =  Extension.updateCurrecyRate(fare: "\(String(rideList?.rides?.first?.trip_info?.amount ?? 0))").currencyValue
                    self?.driverName = rideList?.rides?.first?.driver_info?.name ?? ""
                    self?.vechileType = rideList?.rides?.first?.vehicle_type_info?.display_name ?? ""
                    self?.driverImageUrl = rideList?.rides?.first?.driver_info?.image?.urlFromString
                    self?.feedBackTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: UITableView.RowAnimation.none)

                }

                if rideList?.rides?.first?.rider_rating ?? "" != ""{
                    if self?.feedBackTimer != nil {
                        self?.feedBackTimer?.invalidate()
                    }
                    self?.showAlertWithMessage("sorry! Someone already submitted the rating for this ride".localized, action: .default) { [weak self] (style) in
                        if UserDefaults.companyLogin{
//                            Router.setDashboardViewControllerAsRoot()
                            NotificationCenter.default.post(Notification.init(name: Notification.Name.init("CHECK_RIDE_UPDATES")))
                        }else{
                            self?.navigationController?.popViewController(animated: true)

                        }
                    }
                }
            }catch{
                   self?.stopActivityIndicator()
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if self.feedBackTimer != nil {
            self.feedBackTimer?.invalidate()
        }
    }
    @objc func updateRideFeedBack(notification: Notification) {
        
        self.isFeedbackBool = false
        for index in 0..<((((notification as NSNotification).value(forKey: "userInfo")as! NSDictionary).object(forKey: "ridelist")as! RideList).rides?.count ?? 0) {
            
            let currentRideItem = ((((notification as NSNotification).value(forKey: "userInfo")as! NSDictionary).object(forKey: "ridelist")as! RideList).rides![index])
            if currentRideItem.rideId == self.currentRide?.rideId {
                print(currentRideItem.rideId)
                self.isFeedbackBool = true
                
            }
        }
        if !self.isFeedbackBool || ((((notification as NSNotification).value(forKey: "userInfo")as! NSDictionary).object(forKey: "ridelist")as! RideList).rides?.count ?? 0) == 0{
            if self.updateRide != nil{
                self.updateRide!()
                self.showAlertWithMessage("Sorry! Someone already submitted the rating for this ride".localized, action: .default) { (style) in
                           }
            }
        
        }
    }
    func feedBackListApiCall() {
               self.startActivityIndicator()
               let urlString = componentScheme + "://" + componentHost + ":4040/master/feedback"
               
               let url = URL(string: urlString)!
               let session = URLSession.shared
               let request = NSMutableURLRequest(url: url as URL)
               
               request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
               let authKey = "Bearer " + (CacheManager.sharedInstance.getObjectForKey(.AuthToken) as! String)
               
               request.addValue(authKey , forHTTPHeaderField: "Authorization")
               request.httpMethod = "GET"
               
               
               let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                   DispatchQueue.main.async {
                       
                       guard error == nil else {
                           self.stopActivityIndicator()
                           return
                       }
                       guard let data = data else {
                           self.stopActivityIndicator()
                           return
                       }
                       do {
                           if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                               
                               self.feedBackDic = json as NSDictionary
                               self.feedBackTableView.reloadData()

                           }
                       } catch let error {
                           self.stopActivityIndicator()
                           print(error.localizedDescription)
                       }
                   }
                   
               })
               task.resume()
       }
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        print(textView.text)
        if textView.text == ""{
            textView.text = "Enter your feedback".localized
        }
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feedBackDic.count > 0 ? (self.feedBackDic.object(forKey: "data")as! NSArray).count + 2 : 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "driverInfoTableViewCell", for: indexPath) as! driverInfoTableViewCell
            cell.selectionStyle = .none
            cell.driverName.text = driverName
            cell.vechileType.text = vechileType
            cell.driverImage.hnk_setImage(from: driverImageUrl, placeholder: UIImage(named: "ic_dummy_user"), success: { (image) in
                cell.driverImage.image = image
            }, failure: nil)
            
            return cell
        }
        if self.feedBackDic.count > 0{
            if (self.feedBackDic.object(forKey: "data")as! NSArray).count > 0{
                if indexPath.row <= (self.feedBackDic.object(forKey: "data")as! NSArray).count{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "PassengerRatingCell", for: indexPath) as! PassengerRatingCell
                            cell.selectionStyle = .none
                            cell.passengerRating.tag = indexPath.row + 11
                            cell.questionLbl.text = "\(((self.feedBackDic.object(forKey: "data")as! NSArray).object(at: indexPath.row - 1)as! NSDictionary).object(forKey: "question")!)"
                            return cell
                      }
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PassengerFeedBackCell", for: indexPath) as! PassengerFeedBackCell
        cell.selectionStyle = .none
        cell.submitBtn.addTarget(self, action: #selector(popupToDashBoard), for: .touchUpInside)
        cell.generalRating.tag = 100
        return cell
    }
    @objc func popupToDashBoard(){
        let generalRatingView = self.view.viewWithTag(100)as! FloatRatingView

        if generalRatingView.rating == 0{
            self.view.makeToast("Please give your rating")
            return
        }
        let reviewTextView = self.view.viewWithTag(101)as! UITextView
        var answer = ""
        for item in 0..<(self.feedBackDic.object(forKey: "data")as! NSArray).count {
            print(item)
            let ratingView = self.view.viewWithTag(item + 12)as! FloatRatingView
            print(ratingView.rating)
            if answer == ""{
                answer =  "\(item + 1)" + ":\(ratingView.rating)"

            }else{
                answer = answer + ",\(item + 1)" + ":\(ratingView.rating)"

            }
        }
        var feedBackInfo = FeedbackInfo()
        feedBackInfo.rating = generalRatingView.rating
        feedBackInfo.feedback = reviewTextView.text
        feedBackInfo.answer = answer
        feedBackInfo.driver_id = "\(self.currentRide?.driver_info?.id ?? 0)"
        self.startActivityIndicatorInWindow()
        ServiceManager().updateRideFeedback(self.currentRide, feedBack: feedBackInfo, completion: { [weak self] (inner) -> (Void) in
            do {
                let _ = try inner()
                self?.speak(text: Speech.feedbackThanks.rawValue)
                if self?.updateRide != nil{
                    self?.updateRide!()
                       }
                self?.feedBackTimer?.invalidate()
                if UserDefaults.companyLogin{
//                    Router.setDashboardViewControllerAsRoot()
                    NotificationCenter.default.post(Notification.init(name: Notification.Name.init("CHECK_RIDE_UPDATES")))
                }else{
                self?.navigationController?.popViewController(animated: true)
                }
              
            } catch {
                //                    self?.view.makeToast((error as? CoreError)?.description)
            }
            self?.stopActivityIndicator()
        })

       
    }

}
class PassengerRatingCell: UITableViewCell {
    @IBOutlet weak var questionLbl: UILabel!
   @IBOutlet weak var passengerRating: FloatRatingView!
}
class PassengerFeedBackCell: UITableViewCell {
    @IBOutlet weak var generalQuestionLbl: UILabel!
   @IBOutlet weak var generalRating: FloatRatingView!
    @IBOutlet weak var reviewTextView: UITextView!
    @IBOutlet weak var submitBtn: AppButton!
    @IBOutlet weak var reviewTextViewPlaceHolderLbl: UILabel!
}

class driverInfoTableViewCell: UITableViewCell {
    @IBOutlet weak var driverName: UILabel!
   @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var vechileType: UILabel!

}
