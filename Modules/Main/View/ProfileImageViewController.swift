//
//  ProfileImageViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 05/03/22.
//

import UIKit


class ProfileImagesViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    var profileDocumentInfo: ProfileInfo!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionViewHeight.constant = CGFloat( 180 * profileDocumentInfo.titles.count)
        // Do any additional setup after loading the view.
    }
    
}

class ProfileImageCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: AppImageView!
    @IBOutlet weak var titleLbl: AppLabel!
    
    @IBOutlet weak var noImageAvailable: AppLabel!
    
}

extension ProfileImagesViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return profileDocumentInfo.titles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileImageCell", for: indexPath)
        if let tmpCell = cell as? ProfileImageCell {
            tmpCell.titleLbl.text = profileDocumentInfo.titles[indexPath.item]
            tmpCell.imageView.loadTaxiImage(profileDocumentInfo.urlStrings[indexPath.item].urlFromString)
            tmpCell.noImageAvailable.isHidden = profileDocumentInfo.urlStrings[indexPath.item].isEmpty == true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = collectionView.bounds.size
        size.height /= CGFloat(profileDocumentInfo.titles.count)
        return size
    }
}
