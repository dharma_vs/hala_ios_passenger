//
//  ConfirmDeliveryViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/04/22.
//

import UIKit

class ConfirmDeliveryViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
}

enum ConfirmDeliveryCellType: Int, CaseIterable {
    case PickUpDelivery = 0, DistanceDuration, VehicleInfo, ItemInfo, PickUpDate, ApplyCoupon, PaymentMethod
}

extension Int {
    var confirmDeliveryCellType: ConfirmDeliveryCellType {
        return ConfirmDeliveryCellType(rawValue: self) ?? .PickUpDelivery
    }
}

extension ConfirmDeliveryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ConfirmDeliveryCellType.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row.confirmDeliveryCellType {
        case .PickUpDelivery:
            let Cell: PickupDeliveryTableViewCell = tableView.dequeueCell()
            return Cell
        case .DistanceDuration:
            let Cell: DistanceDurationTableViewCell = tableView.dequeueCell()
            return Cell
        case .VehicleInfo:
            let Cell: VehicleInfoTableViewCell = tableView.dequeueCell()
            return Cell
        case .ItemInfo:
            let Cell: ItemInfoTableViewCell = tableView.dequeueCell()
            return Cell
        case .PickUpDate:
            let Cell: PickUpDateTableViewCell = tableView.dequeueCell()
            return Cell
        case .ApplyCoupon:
            let Cell: ApplyCouponTableViewCell = tableView.dequeueCell()
            return Cell
        case .PaymentMethod:
            let Cell: PaymentMethodTableViewCell = tableView.dequeueCell()
            return Cell
        }
    }
}
