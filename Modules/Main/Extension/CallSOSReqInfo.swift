//
//  CallSOSReqInfo.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 25/02/22.
//

struct CallSOSReqInfo: JSONSerializable {
    var ride_id: String!
    var lat: String!
    var lng: String!
}
