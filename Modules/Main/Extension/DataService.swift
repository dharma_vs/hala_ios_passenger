//
//  DataService.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 25/02/22.
//

import Foundation

extension ServiceManager {
    
    override func requestEndPoint(_ endPoint: AddressableEndPoint, withParameters parameters: AnyObject?, withBody body: AnyObject?, withHeaders headers: AnyObject?, andPathSuffix pathSuffix: AnyObject?, onCompletion completion: @escaping (() throws -> Data?, AnyObject?) -> Void) {
        var updatedHeaders = (headers as? [String: Any]) ?? [String: Any]()
        updatedHeaders["dbstring"] = UserDefaults.dbString
        
        var updatedBody = (body as? [String: Any]) ?? [String: Any]()
        updatedBody["partner_id"] = "0"
        
        super.requestEndPoint(endPoint, withParameters: parameters, withBody: updatedBody as AnyObject?, withHeaders: updatedHeaders as AnyObject?, andPathSuffix: pathSuffix, onCompletion: completion)
    }
    
    
    func keyVerification(_ key: String = "VIRTU7620", completion: @escaping (_ inner: () throws ->  KeyVerificationResponse?) -> (Void)) {
        var updatedHeaders = [String: Any]()
        updatedHeaders["dbstring"] = keyVerificationDbString
        super.requestEndPoint(DataServiceEndpoint.keyVerification, withParameters: ["key": key] as AnyObject?, withBody: nil, withHeaders: updatedHeaders as AnyObject?, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response  = data?.getDecodedObject(from: GeneralResponse<KeyVerificationResponse>.self)
                if response?.status == true {
                    UserDefaults.dbString = response?.data?.dbstring ?? ""
                    completion({ return response?.data })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func addMoneyToWallet(_ amount: Int, transactionId: String = "", completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.addMoneyToWallet, withParameters: nil, withBody: ["amount": amount, "transaction_id": transactionId] as AnyObject?, withHeaders: nil, andPathSuffix: nil) { [weak self](inner, header) in
            do {
                let data = try inner()
                self?.getProfile({ (inner) -> (Void) in  })
                let response = data?.getDecodedObject(from: CommonResponse.self)
                if response?.status == true {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func authenticateProfile(_ completion: @escaping (_ inner: () throws ->  AuthenticationInfo?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.authenticate, withParameters: nil, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponse<AuthenticationInfo>.self)
                if let data = response?.data, response?.status == true {
                    completion({ return data})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func getAllNotifications(_ currentPage: Int, completion: @escaping (_ inner: () throws ->  Paginator<TaxiNotification>) -> (Void)) {
        var parameters = [String: Any]()
        parameters["pageSize"] = pageSize
        parameters["currentPage"] = currentPage
        self.requestEndPoint(DataServiceEndpoint.getAllNotification, withParameters: parameters as AnyObject?, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponse<NotificationListResponse>.self)
                if let data = response?.data, response?.status == true {
                    completion({ return data})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func getNotificationById(_ id: Int, completion: @escaping (_ inner: () throws ->  TaxiNotification?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.getNotification, withParameters: nil, withBody: nil, withHeaders: nil, andPathSuffix: "/\(id)" as AnyObject?) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponse<[TaxiNotification]>.self)
                if let data = response?.data, response?.status == true {
                    completion({ return data.first})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func addFavouriteLocation(_ info: FavoriteLocationReqInfo, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.addFavouriteLocation, withParameters: nil, withBody: info.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CommonResponse.self)
                if response?.status == true {
                    DataService().getProfile({ (inner) -> (Void) in })
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func updateFavouriteLocation(_ info: FavoriteLocationReqInfo, id: Int, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.updateFavouriteLocation, withParameters: nil, withBody: info.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: id as AnyObject) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CommonResponse.self)
                if response?.status == true {
                    DataService().getProfile({ (inner) -> (Void) in })
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func deleteFavouriteLocationById(_ id: Int, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.deleteFavouriteLocation, withParameters: nil, withBody: nil, withHeaders: nil, andPathSuffix: "/\(id)" as AnyObject?) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CommonResponse.self)
                if response?.status == true {
                    DataService().getProfile({ (inner) -> (Void) in })
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func getFavouriteLocationById(_ locationId: Int, completion: @escaping (_ inner: () throws ->  FavoriteLocation?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.getFavouriteLocation, withParameters: nil, withBody: nil, withHeaders: nil, andPathSuffix: "/\(locationId)" as AnyObject?) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponse<[FavoriteLocation]>.self)
                if let data = response?.data, response?.status == true {
                    completion({ return data.first})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func getAllFavouriteLocations(_ completion: @escaping (_ inner: () throws ->  [FavoriteLocation]) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.getAllFavouriteLocations, withParameters: nil, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponse<[FavoriteLocation]>.self)
                if let data = response?.data, response?.status == true {
                    completion({ return data })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func couponList(_ completion: @escaping (_ inner: () throws ->  [Coupon]) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.couponList, withParameters: nil, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponse<[Coupon]>.self)
                if let data = response?.data, response?.status == true {
                    completion({ return data})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
//    func walletHistory(_ currentPage: Int, completion: @escaping (_ inner: () throws ->  Paginator<WalletHistoryInfo>?) -> (Void)) {
//        var parameters = [String: Any]()
//        parameters["type"] = "1"
//        parameters["pageSize"] = pageSize
//        parameters["currentPage"] = currentPage
//        self.requestEndPoint(DataServiceEndpoint.walletHistory, withParameters: parameters as AnyObject?, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
//            do {
//                let data = try inner()
//                let response = data?.getDecodedObject(from: GeneralResponse<WalletHistoryListResponse>.self)
//                if let data = response?.data, response?.status == true {
//                    completion({ return data})
//                } else {
//                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
//                }
//            } catch {
//                completion({ throw error })
//            }
//        }
//    }
    
    func rideList(_ currentPage: Int, rideType: RideListType, completion: @escaping (_ inner: () throws ->  Paginator<RideListItem>?) -> (Void)) {
        var parameters = [String: Any]()
        parameters["type"] = rideType == .open ? "1" : "2"
        parameters["pageSize"] = pageSize
        parameters["currentPage"] = currentPage
        parameters["company_id"] = UserDefaults.companyLogin ? CacheManager.riderInfo?.company_id ?? 0 : "0"
        self.requestEndPoint(DataServiceEndpoint.rideList, withParameters: parameters as AnyObject?, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponse<RideListResponse>.self)
                if let data = response?.data, response?.status == true {
                    completion({ return data})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func updateRideStatus(_ rideId: String?, status: RideState = .retry,sourceLocation:LocationDetail,rideStatus:Int,  completion: @escaping (_ inner: () throws ->  UpdatedStatus?) -> (Void)) {
        self.contentType = .json
        var body = [String: Any]()
        body["status"] = status.rawValue
        body["rejected_count"] = 0
        body["search_latitude"] = sourceLocation.coordinate.latitude
        body["search_longitude"] = sourceLocation.coordinate.longitude
        body["prev_status"] = rideStatus
        
        self.requestEndPoint(DataServiceEndpoint.updateRideStatus, withParameters: nil, withBody: body as AnyObject?, withHeaders: nil, andPathSuffix: (rideId ?? "")/* + "-" + riderId)*/ as AnyObject?) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: UpdatedStatus.self)
                if let status = response {
                    completion({ return status })
                } else {
                    completion({ return nil })
                }
            } catch {
                completion({throw error})
            }
        }
    }
    func updateRideStatusHubLocation(_ rideId: String?, status: RideState = .retry,lat:String,long:String,rideStatus:Int,category_id:String,  completion: @escaping (_ inner: () throws ->  UpdatedStatus?) -> (Void)) {
        self.contentType = .json
        var body = [String: Any]()
        body["status"] = status.rawValue
        body["rejected_count"] = 0
        body["search_latitude"] = lat
        body["search_longitude"] = long
        body["prev_status"] = 8
        body["category_id"] = category_id
        body["category"] = category_id
        body["vehicle_type"] = category_id
        
        self.requestEndPoint(DataServiceEndpoint.updateRideStatus, withParameters: nil, withBody: body as AnyObject?, withHeaders: nil, andPathSuffix: (rideId ?? "")/* + "-" + riderId)*/ as AnyObject?) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: UpdatedStatus.self)
                if let status = response {
                    completion({ return status })
                } else {
                    completion({ return nil })
                }
            } catch {
                completion({throw error})
            }
        }
    }
    func updateRideFeedback(_ ride: RideDetail?, feedBack: FeedbackInfo, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
        let rideId = "\(ride?.id ?? 0)"
        let driver_id = "\(ride?.driver_id ?? 0)"
        var feedback = feedBack
        feedback.driver_id = "\(ride?.driver_id ?? 0)"
        self.requestEndPoint(DataServiceEndpoint.updateFeedback, withParameters: nil, withBody: feedback.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: (rideId /* + "-" + driver_id*/) as AnyObject?) { (inner, header) in
            do {
                let data = try inner()
                if let status = data?.getDecodedObject(from: GeneralResponse<CommonResponse>.self)?.data {
                    completion({ return status })
                }
            } catch {
                completion({throw error})
            }
        }
    }
    
    func rideDetail(_ rideId: String, completion: @escaping (_ inner: () throws ->  RideDetail?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.rideDetail, withParameters: nil, withBody: nil, withHeaders: nil, andPathSuffix: rideId as AnyObject) { (inner, header) in
            do {
                let data = try inner()
                if let rideDetail = data?.getDecodedObject(from: JoyResponse.self)?.data?.rides?.first {
                    completion({ return rideDetail })
                }
            } catch {
                completion({throw error})
            }
        }
    }
    
    func updateCancelView(_ rideId: String, completion: @escaping (_ inner: () throws ->  Void) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.updateCancelView, withParameters: nil, withBody: nil, withHeaders: nil, andPathSuffix: rideId as AnyObject?) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CommonResponse.self)
                if let status = response, status.status == true {
                    completion({ return })
                } else {
                    completion({ return })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func cancelRide(createRide:Bool,_ ride: RideDetail?, reason: String,sourceLocation:LocationCoordinate, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
        let rideId = "\(ride?.id ?? 0)"
        let driver_id = "\(ride?.driver_id ?? 0)"
        var body = [String: Any]()
        
//        body["cancel_reason"] = reason
//        body["rejected_count"] = 0
//        body["search_latitude"] = sourceLocation.latitude
//        body["search_longitude"] = sourceLocation.longitude
//        body["prev_status"] = ride?.booking_status ?? 0
//        body["rider_id"] = driver_id
//        body["company_id"] = CacheManager.riderInfo?.company_id ?? 0
//
//        if createRide{
//            body["prev_status"] = 1
//            body["cancel_reason"] = 0
//
//        }else{
//            body["cancel_reason"] = reason
//
//            body["prev_status"] = 2
//
//        }
        if createRide{
            
            body["prev_status"] = ride?.booking_status
            body["cancel_reason"] = 0
            body["driver_id"] = "\(ride?.current_driver_id ?? 0)"
            
        }else{
            body["cancel_reason"] = reason
            body["driver_id"] = driver_id
            body["prev_status"] = ride?.booking_status
            body["rejected_count"] = 0
            body["search_latitude"] = sourceLocation.latitude
            body["search_longitude"] = sourceLocation.longitude
            
        }
        
        self.requestEndPoint(DataServiceEndpoint.cancelRide, withParameters: nil, withBody: body as AnyObject?, withHeaders: nil, andPathSuffix: (rideId /* + "-" + driver_id*/)  as AnyObject?) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CommonResponse.self)
                if let status = response, status.status == true {
                    completion({ return response})
                } else {
                    completion({ return response})
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    func info(_ index: Int, completion: @escaping (_ inner: () throws -> Info?) -> (Void)) {
           self.requestEndPoint(DataServiceEndpoint.info, withParameters: nil, withBody: nil, withHeaders: nil, andPathSuffix: "/\(index)" as AnyObject?) { (inner, header) in
               do {
                   let data = try inner()
                   let response  = data?.getDecodedObject(from: GeneralResponse<Info>.self)
                   if let info = response?.data {
                       completion({ return info })
                   } else {
                       completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                   }
               } catch {
                   completion({ throw error })
               }
           }
       }
       
    func confirmRide(_ info: RequestConfirmRide, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.confirmRide, withParameters: nil, withBody: info.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response  = data?.getDecodedObject(from: CommonResponse.self)
                if response?.status == true {
                    completion({ return response })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func checkCurrentRideStatus(_ completion: @escaping (_ inner: () throws ->  RideList?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.checkStatus, withParameters: nil, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                if let rideList = data?.getDecodedObject(from: JoyResponse.self)?.data {
                    completion({ return rideList })
                }
            } catch {
                completion({throw error})
            }
        }
    }
    func fetchRideDetails(_ rideId: String?, reason: String, completion: @escaping (_ inner: () throws ->  RideList?) -> (Void)) {
        let rideId = "\(rideId ?? "0")"
        guard rideId != "0" else {
            return
        }
        self.requestEndPoint(DataServiceEndpoint.fetchdetails, withParameters: nil, withBody: nil, withHeaders: nil, andPathSuffix: (rideId /* + "-" + driver_id*/)  as AnyObject?) { (inner, header) in
            do {
                let data = try inner()
                if let rideList = data?.getDecodedObject(from: JoyResponse.self)?.data {
                    completion({ return rideList })
                }
            } catch {
                completion({throw error})
            }
        }
    }
    func updateDestinationAddress(_ request: RequestConfirmRide?, reason: String, completion: @escaping (_ inner: () throws ->  RideList?) -> (Void)) {
        let rideId = "\(request?.rider_id ?? "0")"
        self.requestEndPoint(DataServiceEndpoint.updateDestination, withParameters: nil, withBody: request?.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: (rideId /* + "-" + driver_id*/)  as AnyObject?) { (inner, header) in
            do {
                let data = try inner()
                if let rideList = data?.getDecodedObject(from: JoyResponse.self)?.data {
                    completion({ return rideList })
                }
            } catch {
                completion({throw error})
            }
        }
    }
    func categoryList(_ info: CategoryReq, completion: @escaping (_ inner: () throws ->  [Category]) -> (Void)) {
        //        self.contentType = .json
        self.requestEndPoint(DataServiceEndpoint.categoryList, withParameters: info.JSONRepresentation as AnyObject?, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response  = data?.getDecodedObject(from: GeneralResponse<[Category]>.self)
                if let categoryList = response?.data, response?.status == true {
                    completion({ return categoryList })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func getProfile(_ completion: @escaping (_ inner: () throws ->  UserInfo) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.getProfile, withParameters: nil, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response  = data?.getDecodedObject(from: GeneralResponse<UserInfo>.self)
                print("Manually parsed  ", (try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)) ?? "nil")
                if response?.message?.contains("jwt expired") == true {
                    completion({ throw CoreError.init(code: 1000, description: "Session Expired, Please login again".localize(), innerError: nil, informations: nil) })
                    NotificationCenter.default.post(name: NSNotification.Name.init(SESSION_EXPIRED), object: nil)
                    return
                }
                if let userInfo = response?.data {
                    CacheManager.sharedInstance.setObject(userInfo, key: .UserInfo)
                    CacheManager.sharedInstance.saveCache()
                    NotificationCenter.default.post(name: NSNotification.Name.init("PROFILE_UPDATED"), object: nil)
                    completion({ return userInfo })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func updateProfile(_ info: ProfileReqInfo, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.updateProfile, withParameters: nil, withBody: info.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response  = data?.getDecodedObject(from: CommonResponse.self)
                if response?.status == true {
                    completion({ return response })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func appSettings(_ completion: @escaping (_ inner: () throws ->  AppSettings?) -> (Void)) {
        let currentLocale = NSLocale.current as NSLocale
        //        let countryCode = currentLocale.object(forKey: NSLocale.Key.countryCode) as! String
        //        let countryName = currentLocale.displayName(forKey: NSLocale.Key.countryCode, value: countryCode) ?? "India"
       // let params = ["country": UserDefaults.country]
        let params = ["country": "CG"]
        self.requestEndPoint(DataServiceEndpoint.appSettings, withParameters: params as AnyObject?, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response  = data?.getDecodedObject(from: GeneralResponse<AppSettings>.self)
                print("Manually parsed  ", (try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)) ?? "nil")
                if let settings = response?.data {
                    CacheManager.sharedInstance.setObjectInApplication(settings, key: .AppSettings)
                    CacheManager.sharedInstance.saveCache()
                    if "\(CacheManager.sharedInstance.getObjectForKey(.updatedCurrencySymbol) ?? "")" == "" {
                      //  CacheManager.sharedInstance.setObject("\((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currency_symbol ?? "")", key: .updatedCurrencySymbol)
                       // CacheManager.sharedInstance.setObject("\((((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currencies)?[0].rate ?? ""))", key: .currencyRate)
                        
                        CacheManager.sharedInstance.setObject("$", key: .updatedCurrencySymbol)
                        
                       (CacheManager.settings)?.currency_symbol = "$"
                        
                        if ((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currencies) != nil {
                            for item in (((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currencies)!){
                                
                                if "\(CacheManager.sharedInstance.getObjectForKey(.updatedCurrencySymbol) ?? "")" == "\(item.currency_symbol ?? "")"{
                                    CacheManager.sharedInstance.setObject("\(item.rate ?? "")", key: .currencyRate)
                                }
                            }
                        }

                    }else {
                        CacheManager.settings?.currency_symbol = "\(CacheManager.sharedInstance.getObjectForKey(.updatedCurrencySymbol) ?? "")"
                    }
                    completion({ return settings })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func checkAccount(_ body: AnyObject, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.checkAccount, withParameters: nil, withBody: body, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response  = data?.getDecodedObject(from: CommonResponse.self)
                completion({ return response })
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func login<T: Decodable>(_ type: T.Type, login: Login, completion: @escaping (_ inner: () throws ->  GeneralResponse<T>?) -> (Void)) {
        let body = login.JSONRepresentation as AnyObject
        self.requestEndPoint(DataServiceEndpoint.login, withParameters: nil, withBody: body, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response  = data?.getDecodedObject(from: GeneralResponse<T>.self)
                if response?.status == true, let token: Token = response?.data as? Token {
                    CacheManager.sharedInstance.setObject(token.accessToken ?? "", key: .AuthToken)
                    CacheManager.sharedInstance.setObject(token.refreshToken ?? "", key: .RefreshToken)
                    CacheManager.sharedInstance.saveCache()
                }
                completion({ return response })
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func sendOTP<T: Decodable>(_ type: T.Type, body: AnyObject, completion: @escaping (_ inner: () throws ->  GeneralResponse<T>?) -> (Void)) {
        self.contentType = .json

        self.requestEndPoint(DataServiceEndpoint.sendOTP, withParameters: nil, withBody: body, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response  = data?.getDecodedObject(from: GeneralResponse<T>.self)
                completion({ return response })
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func register<T: Decodable>(_ type: T.Type, register: Register, completion: @escaping (_ inner: () throws ->  GeneralResponse<T>?) -> (Void)) {
        let body = register.JSONRepresentation as AnyObject
        self.requestEndPoint(DataServiceEndpoint.register, withParameters: nil, withBody: body, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response  = data?.getDecodedObject(from: GeneralResponse<T>.self)
                if response?.status == true, let token: Token = response?.data as? Token {
                    CacheManager.sharedInstance.setObject(token.accessToken ?? "", key: .AuthToken)
                    CacheManager.sharedInstance.setObject(token.refreshToken ?? "", key: .RefreshToken)
                    CacheManager.sharedInstance.saveCache()
                    completion({ return response })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func resetPassword(_ password: String, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
        let body = ["password": password] as AnyObject
        self.requestEndPoint(DataServiceEndpoint.resetPassword, withParameters: nil, withBody: body, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response  = data?.getDecodedObject(from: CommonResponse.self)
                completion({ return response })
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func forgotPassword(_ body: AnyObject, completion: @escaping (_ inner: () throws ->  GeneralResponse<Token>?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.forgotPassword, withParameters: nil, withBody: body, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response  = data?.getDecodedObject(from: GeneralResponse<Token>.self)
                if response?.status == true, let token: Token = response?.data {
                    CacheManager.sharedInstance.setObject(token.accessToken ?? "", key: .AuthToken)
                    CacheManager.sharedInstance.setObject(token.refreshToken ?? "", key: .RefreshToken)
                    CacheManager.sharedInstance.saveCache()
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
                completion({ return response })
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func changePassword(_ info: ChangePasswordReqInfo, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.changePassword, withParameters: nil, withBody: info.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response  = data?.getDecodedObject(from: CommonResponse.self)
                if response?.status == true {
                    completion({ return response })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func updateFCMToken(_ completion: @escaping (_ inner: () throws ->  Void) -> (Void)) {
        let body = [
            "device_id": device_Id,
            "fcmToken": Jeffery_Passenger.fcmToken,
            "device_type" : "ios"
        ]
        
        self.requestEndPoint(DataServiceEndpoint.updateFCMToken, withParameters: nil, withBody: body as AnyObject?, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CommonResponse.self)
                if let status = response, status.status == true {
                    completion({ return })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func logout(_ completion: @escaping (_ inner: () throws ->  Void) -> (Void)) {
        let body = ["device_id": device_Id]  as AnyObject?
        self.requestEndPoint(DataServiceEndpoint.logout, withParameters: nil, withBody: body, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CommonResponse.self)
                if let status = response, status.status == true {
                    completion({ return })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func upload(_ image: UIImage, type: String, completion: @escaping (_ inner: () throws ->  GeneralResponse<FileUploadResponse>?) -> (Void)) {
        let body = ["type": type] as AnyObject
        super.uploadImage(image, filePathKey: "file", endPoint: DataServiceEndpoint.upload, withBodyParameters: body, withHeaders: nil, andPathSuffix: nil, mimeType: .jpeg) { (inner) in
            do {
                let data = try inner()
                let response  = data?.getDecodedObject(from: GeneralResponse<FileUploadResponse>.self)
                if response?.status == true {
                    completion({ return response })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func verifyCoupon(_ params: VerifyCouponParams, completion: @escaping (_ inner: () throws ->  GeneralResponse<[Coupon]>?) -> Void) {
        self.requestEndPoint(DataServiceEndpoint.verifyCoupon, withParameters: params.JSONRepresentation as AnyObject?, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponse<[Coupon]>.self)
                completion({ return response })
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func callSOS(_ info: CallSOSReqInfo, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> Void) {
        self.requestEndPoint(DataServiceEndpoint.callSOS, withParameters: info.JSONRepresentation as AnyObject?, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CommonResponse.self)
                if let status = response, status.status == true {
                    completion({return response})
                } else {
                    completion({ return  nil})
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func appFeedback(_ info: AppFeedBackReqInfo, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> Void) {
        self.requestEndPoint(DataServiceEndpoint.appFeedback, withParameters: nil, withBody: info.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CommonResponse.self)
                if let status = response, status.status == true {
                    completion({return response})
                } else {
                    completion({ return  nil})
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func aboutInfo(_ completion: @escaping (_ inner: () throws ->  AboutUsInfo) -> Void) {
        self.requestEndPoint(DataServiceEndpoint.aboutUsInfo, withParameters: nil, withBody: nil, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponse<AboutUsInfo>.self)
                if let data = response?.data, response?.status == true {
                    completion({return data})
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func addCard(dic: NSDictionary, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> Void) {
     self.requestEndPoint(DataServiceEndpoint.cards, withParameters: nil, withBody: dic as AnyObject?, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CommonResponse.self)
                if response != nil {
                    completion({return response})
                } else {
                    completion({ return  nil})
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    func deleteSavedCard(_ id: Int, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
           self.requestEndPoint(DataServiceEndpoint.deleteCards, withParameters: nil, withBody: nil, withHeaders: nil, andPathSuffix: "/\(id)" as AnyObject?) { (inner, header) in
               do {
                   let data = try inner()
                   let response = data?.getDecodedObject(from: CommonResponse.self)
                   if response?.status == true {
                       DataService().getProfile({ (inner) -> (Void) in })
                       completion({ return response})
                   } else {
                       completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                   }
               } catch {
                   completion({ throw error })
               }
           }
       }
    
    func cardPayment(info: NSDictionary, id : Int, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.cardPayment, withParameters: nil, withBody: info as AnyObject?, withHeaders: nil, andPathSuffix: id as AnyObject) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CommonResponse.self)
                if response?.status == true {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func jPayment(info: JPaymentReq, completion: @escaping (_ inner: () throws ->  GeneralResponse<JPaymentRes>?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.jpayment, withParameters: nil, withBody: info.JSONRepresentation as AnyObject?, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: GeneralResponse<JPaymentRes>.self)
                let checkMode = CacheManager.settings?.payment_mode?.stringValue == "Test"
                if response?.status == (checkMode == true ? false : true) {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    
    func updateCashPayment(info: NSDictionary, id : Int, completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.updatePayment, withParameters: nil, withBody: info as AnyObject?, withHeaders: nil, andPathSuffix: id as AnyObject) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CommonResponse.self)
                if response?.status == true {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            } catch {
                completion({ throw error })
            }
        }
    }
    func notifyPushNotificationMessage(rideId:String,riderId:Int,message:String,  completion: @escaping (_ inner: () throws ->  CommonResponse?) -> (Void)) {
        self.contentType = .json
        var body = [String: Any]()
        body["message"] = message
        body["ride_id"] = rideId
        body["driver_id"] = riderId

        self.requestEndPoint(DataServiceEndpoint.chatnotify, withParameters: nil, withBody: body as AnyObject?, withHeaders: nil, andPathSuffix: nil) { (inner, header) in
            do {
                let data = try inner()
                let response = data?.getDecodedObject(from: CommonResponse.self)
                if let status = response, status.status == true {
                    completion({return response})
                } else {
                    completion({ return  nil})
                }
            } catch {
                completion({throw error})
            }
        }
    }

    func updateRidePaymentMethod(_ info: UpdatePaymentMethodReq, completion: @escaping (_ inner: () throws ->  CustomType) -> (Void)) {
        self.requestEndPoint(DataServiceEndpoint.updateRidePaymentMethod, withParameters: info.JSONRepresentation as AnyObject?, withBody: nil, withHeaders: nil, andPathSuffix: "/\(info.ride_id ?? "")" as AnyObject?) { (inner, header) in
            do {
                let data = try inner()
                if let status = data?.getDecodedObject(from: CustomType.self) {
                    completion({ return status })
                }
            } catch {
                completion({throw error})
            }
        }
    }
}

