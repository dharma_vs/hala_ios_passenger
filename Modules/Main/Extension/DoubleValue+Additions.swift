//
//  DoubleValue+Additions.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import Foundation

extension String {
    var doubleValue: Double? {
        return Double(self)
    }
}
