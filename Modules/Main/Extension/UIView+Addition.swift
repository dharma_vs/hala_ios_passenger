//
//  UIView+Addition.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 25/02/22.
//

import Foundation
import UIKit

extension UIView {
    
    // Creating rounded corner
    func SetRoundedCorner(radius: CGFloat){
        self.layer.cornerRadius = radius
    }
    
    // Setting Border width and color for a UIView
    func SetBorder(width: CGFloat, color: UIColor){
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
        self.layer.masksToBounds = false
    }
    
    // Set Shadow for view
    func SetShadow(color: UIColor) {
        layer.shadowColor = color.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 3);
        layer.shadowOpacity = 0.3
        
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 5)
        layer.shadowPath = shadowPath.cgPath
    }
    
}
