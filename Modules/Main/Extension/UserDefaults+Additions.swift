//
//  UserDefaults+Extension.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/02/22.
//

import Foundation

extension UserDefaults {
    static func setFCMtoken(_ token: String) {
        UserDefaults.standard.set(token, forKey: "fcmToken")
        UserDefaults.standard.synchronize()
    }
    
    static func setFetchRideStaus(_ status: Bool) {
        UserDefaults.standard.set(status, forKey: "fetchRideStaus")
        UserDefaults.standard.synchronize()
    }
    static var getFetchRideStaus: Bool {
        return (UserDefaults.standard.object(forKey: "fetchRideStaus") as? Bool) ?? false
    }
    static var secondTheme: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: "theme2")
            UserDefaults.standard.synchronize()
        }
        get {
            return (UserDefaults.standard.value(forKey: "theme2") as? Bool)!
        }
    }
    static var appLanguage: String? {
        get {
            return UserDefaults.standard.value(forKey: .CurrentApplicationLanguage) as? String
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: .CurrentApplicationLanguage)
            UserDefaults.standard.synchronize()
        }
    }
    
    static var companyLogin: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: .company)
            UserDefaults.standard.synchronize()
        }
        get {
            return (UserDefaults.standard.value(forKey: .company) as? Bool) ?? false
        }
    }
    static var firstTimeLanuchStatus: Bool? {
        set {
            UserDefaults.standard.set(newValue, forKey: "firsttimerun")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.value(forKey: "firsttimerun") as? Bool
        }
    }
    static var pickupdropChangesStatus: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: "pickupdropChangesStatus")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.value(forKey: "pickupdropChangesStatus") as? Bool ?? false
        }
    }
}
