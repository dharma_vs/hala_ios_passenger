//
//  RideCheckHelper.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 03/03/22.
//

import Foundation

protocol CustomErrorProtocol : Error {
    var localizedDescription : String {get set}
}

struct CustomError : CustomErrorProtocol {
    
    var localizedDescription: String
    var statusCode : Int
    
    init(description : String, code : Int){
        self.localizedDescription = description
        self.statusCode = code
    }
}

let requestCheckInterval = 10.0

class RideCheckHelper {
    
    private weak var timer : Timer?
    
    private var completion: ((CustomError?,RideList?)->Void)?
    
    func startListening(on completion : @escaping ((CustomError?,RideList?)->Void)) {
        self.completion = completion
        if self.timer == nil {
            self.timerAction()
            self.timer = Timer.scheduledTimer(timeInterval: requestCheckInterval, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        }
    }
    
    @objc func timerAction() {
        ServiceManager().checkCurrentRideStatus { [weak self](inner) -> (Void) in
            do {
                let rideList = try inner()
                self?.completion?(nil,rideList)
            } catch {
                self?.completion?(CustomError.init(description: error.localizedDescription, code: 1001), nil)
            }
        }
    }
    
    func stopListening() {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    func resumeListening() {
        if self.timer?.isValid == false || self.timer == nil {
            self.timer?.invalidate()
            self.timer = nil
            self.timerAction()
            self.timer = Timer.scheduledTimer(timeInterval: requestCheckInterval, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        }
    }
    
    deinit {
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
}
