//
//  MainDataServiceEndpoint.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 07/03/22.
//

import Foundation

enum MainDataServiceEndpoint: AddressableEndPoint {
    
    case confirmRide, rentalList, rentalDetails, verifyCoupon, couponList, changePassword, callSOS, updateRideStatus, fetchRideDetails, updateCancelView, updateRideStatusHubLocation, categoryList, cancelRide, updateFCMToken, updateCashPayment, updateRideFeedback, updateDestinationAddress, addFavouriteLocation, cardPayment, cancelReason, checkCurrentRideStatus, operatorList, chargingStation, rentalFetch, bringHere, station, favLocation, vehiclelistbystation,categorydetails, checkzone, estimation, confirmRental, updateRentalStatus, mqtt, ridepausetime
    
    var endPointInfo: DataServiceEndpointInfo {
        switch self {
        case .confirmRide:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/ride/confirm", method: .POST, authType: .user)
            
        case .rentalList:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "rental/rental-list", method: .GET, authType: .user)
            
        case .rentalDetails:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/rental/details", method: .GET, authType: .user)

        case .verifyCoupon:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/ride/verify-coupon", method: .GET, authType: .user)
            
        case .couponList:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/ride/coupon-list", method: .GET, authType: .user)
            
        case .changePassword:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/rider/profile/change-password", method: .PUT, authType: .user)
            
        case .callSOS:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/rider/profile/callSOS", method: .POST, authType: .user)
            
        case .updateRideStatus:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/driver/ride/update-ride-request/", method: .PUT, authType: .user)
            
        case .fetchRideDetails:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/ride/fetch-details/", method: .GET, authType: .user)
            
        case .updateCancelView:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/ride/update-cancel/", method: .PUT, authType: .user)
            
        case .updateRideStatusHubLocation:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/updateRideStatusHubLocation", method: .POST, authType: .user)
            
        case .categoryList:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/category/list", method: .GET, authType: .user)
            
//        case .categoryList:
//            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/categoryList", method: .POST, authType: .user)
            
        case .cancelRide:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/ride/cancel/", method: .PUT, authType: .user)
            
        case .updateFCMToken:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  componentPathSuffix+"/profile/update-token", method: .PUT, authType: .user)
            
        case .updateCashPayment:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/updateCashPayment", method: .POST, authType: .user)
            
        case .updateRideFeedback:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/updateRideFeedback", method: .POST, authType: .user)
            
        case .updateDestinationAddress:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/updateDestinationAddress", method: .POST, authType: .user)
            
        case .addFavouriteLocation:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/rider/fav-location", method: .POST, authType: .user)
            
        case .cardPayment:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/ride/payment-completed/", method: .PUT, authType: .user)
            
        case .cancelReason:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  componentMasterPathSuffix+"/cancel-reason", method: .GET, authType: .none)
            
        case .checkCurrentRideStatus:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/ride/fetch", method: .GET, authType: .user)
            
        case .operatorList:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort , path: "operator/services/operator-list", method: .GET, authType: .user)
            
        case .chargingStation:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort , path: componentMasterPathSuffix+"/charging-station", method: .GET, authType: .none)
            
        case .rentalFetch:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort , path: "/rental/fetch/", method: .GET, authType: .user)
            
        case .bringHere:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort , path: "/ride/bring-here", method: .POST, authType: .user)
            
        case .station:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort , path: componentPathSuffix+"/category/station", method: .GET, authType: .user)
            
        case .favLocation:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort , path: componentMasterPathSuffix+"rider/fav-location", method: .POST, authType: .user)
            
        case .vehiclelistbystation:
             return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort , path: componentPathSuffix+"/category/vehicle-list-by-station/", method: .GET, authType: .user)
        
        case .categorydetails:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort , path: componentPathSuffixrental+"/rental-category/", method: .GET, authType: .user)
            
        case .checkzone:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort , path: componentPathSuffixrental+"/check-zone", method: .GET, authType: .user)
            
        case .estimation:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort , path: componentPathSuffixrental+"/rental-estimation", method: .GET, authType: .user)
            
        case .confirmRental:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/rental/confirm", method: .POST, authType: .user)
       
        case .updateRentalStatus:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "/rental/update-ride-request/", method: .POST, authType: .user)

        case .mqtt:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort , path: "/cron/mqtt", method: .GET, authType: .none)
            
        case .ridepausetime:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort , path: componentPathSuffixrental+"/update-ride-pausetime/", method: .PUT, authType: .user)
        }
    }
}

extension ServiceManager {
    
    func operatorList(_ info: OperatorListReq = OperatorListReq(), completion: @escaping (_ inner: () throws ->  OperatorList) -> Void) {
        self.request(endPoint: MainDataServiceEndpoint.operatorList, parameters: info.JSONRepresentation) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response = data.getDecodedObject(from: GeneralResponse<OperatorList>.self)
                if let data = response?.data, response?.status == true {
                    completion({return data})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func chargingStation(_ info: ChargingStationReq = ChargingStationReq(), completion: @escaping (_ inner: () throws ->  ChargingStation) -> Void) {
        self.request(endPoint: MainDataServiceEndpoint.chargingStation, parameters: info.JSONRepresentation) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response = data.getDecodedObject(from: GeneralResponse<ChargingStation>.self)
                if let data = response?.data, response?.status == true {
                    completion({return data})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func categoryList(_ info: CategoryListReq = CategoryListReq(), completion: @escaping (_ inner: () throws ->  CategoryList) -> Void) {
        self.request(endPoint: MainDataServiceEndpoint.categoryList, parameters: info.JSONRepresentation) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response = data.getDecodedObject(from: GeneralResponse<CategoryList>.self)
                if let data = response?.data, response?.status == true {
                    completion({return data})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func rentalFetch(_ info: RentalFetchReq = RentalFetchReq(), completion: @escaping (_ inner: () throws ->  RentalFetch) -> Void) {
        self.request(endPoint: MainDataServiceEndpoint.rentalFetch, parameters: info.JSONRepresentation) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response = data.getDecodedObject(from: GeneralResponse<RentalFetch>.self)
                if let data = response?.data, response?.status == true {
                    completion({return data})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func bringHere(_ info: BringHereReq = BringHereReq(), completion: @escaping (_ inner: () throws ->  BringHere) -> Void) {
        self.request(endPoint: MainDataServiceEndpoint.bringHere, parameters: info.JSONRepresentation) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response = data.getDecodedObject(from: GeneralResponse<BringHere>.self)
                if let data = response?.data, response?.status == true {
                    completion({return data})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func station(_ info: StationReq = StationReq(), completion: @escaping (_ inner: () throws ->  StationRes) -> Void) {
        self.request(endPoint: MainDataServiceEndpoint.station, parameters: info.JSONRepresentation) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response = data.getDecodedObject(from: GeneralResponse<StationRes>.self)
                if let data = response?.data, response?.status == true {
                    completion({return data})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func favLocation(_ info: FavLocationReq = FavLocationReq(), completion: @escaping (_ inner: () throws ->  FavLocationRes) -> Void) {
        self.request(endPoint: MainDataServiceEndpoint.favLocation, parameters: info.JSONRepresentation) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response = data.getDecodedObject(from: GeneralResponse<FavLocationRes>.self)
                if let data = response?.data, response?.status == true {
                    completion({return data})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    func checkZoneAPI(_ params: [String:Any], completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        self.request(endPoint: MainDataServiceEndpoint.checkzone,parameters: params
        ) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: CommonRes.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    func checkEstimation(_ params: [String:Any], completion: @escaping (_ inner: () throws ->  EstimateFareModel?) -> (Void)) {
        self.request(endPoint: MainDataServiceEndpoint.estimation,parameters: params, pathSuffix: "/\(RideType)/\(ZoneType)"
        ) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: EstimateFareModel.self)
                completion({ return response})
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func confirmRentalAPI(_ params: confirmRentalReq, completion: @escaping (_ inner: () throws ->  Rental1?) -> (Void)) {
        self.request(endPoint: MainDataServiceEndpoint.confirmRental,body: params.JSONRepresentation as Dictionary<String,Any>) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: GeneralResponse<RentalRes>.self)
                    completion({ return response?.data?.rental?.first})
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    func updateriderequestAPI(_ bookingID: String,params: [String:Any], completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        self.request(endPoint: MainDataServiceEndpoint.updateRentalStatus,body: params ,pathSuffix: bookingID) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: CommonRes.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    func updateridepauserequestAPI(_ bookingID: String,params: [String:Any], completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        self.request(endPoint: MainDataServiceEndpoint.ridepausetime,body: params ,pathSuffix: bookingID) { result in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: CommonRes.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}

struct OperatorListReq: JSONSerializable {
    var lat: Double!
    var lng: Double!
    var all: Int!

}

struct OperatorList: JSONSerializable {
    var data: [CustomType]?
}

struct ChargingStationReq: JSONSerializable {
    var latitude: Double!
    var longitude: Double!
    var all: Int!
    
}

struct ChargingStation: JSONSerializable {
    var data: [CustomType]?
}

struct CategoryListReq: JSONSerializable {
    var city: String!
    var city2: String!
    var s_latitude: Double!
    var s_longitude: Double!
    var d_latitude: Double!
    var d_longitude: Double!
    var distance: Int!
    var time: Int!
    var vehicle_type: String!
    var weight: Int!
    var category: Int!
    var rate_card_type: String!
    
}

struct CategoryList: JSONSerializable {
    var data: [CustomType]?
}

struct RentalFetchReq: JSONSerializable {

}

struct RentalFetch: JSONSerializable {
    var rental: [CustomType]?
}

struct BringHereReq: JSONSerializable {

}

struct BringHere: JSONSerializable {
    var data: CustomType?
}

struct StationReq: JSONSerializable {
    
}

struct StationRes: JSONSerializable {
    var status: Bool?
    var message: String?
    var data: [CustomType]?
}

struct FavLocationReq: JSONSerializable {
    var type: Int?
    var name: String?
    var lat: Double?
    var lng: Double?
    var address: String?
}

struct FavLocationRes: JSONSerializable {
    var status: Bool?
    var message: String?
    var data: FavLocation?
}

struct FavLocation: JSONSerializable {
}
