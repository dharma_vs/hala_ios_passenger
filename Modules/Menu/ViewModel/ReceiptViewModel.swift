//
//  ReceiptViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import Foundation

class ReceiptViewModel {
    
    let bookingId: String
    
    let invoiceNo: String
    
    let bookingDate: String
    
    let totalFare: String
    
    let sourceAddress: String
    
    let destinationAddress: String
    
    let distance: String
    
    let duration: String
    
    let passengerName: String
    
    let passengerPhone: String
    
    let driverPhone: String
    
    let driverName: String
    
    let vehicleName: String
    
    let licenseNo: String
    
    let rideFare: String
    
    let roundOff: String
    
    let weekTime: String
    
    let vehicleTypeName: String
    
    let cashPayment: String
    
    let cardPayment: String
    
    let walletPayment: String
    
    let hasCashPayment: Bool
    
    let hasCardPayment: Bool
    
    let hasWalletPayment: Bool
    
    let rideDetail: RideDetail
    let destinationChangedfare : String
    let waitingTimeFare : String
    var waitingTime = 0

    
    init(_ rideDetail: RideDetail) {
        
        self.rideDetail = rideDetail
        
        self.bookingId = "Booking Id".localized + ": " + "\(rideDetail.booking_id?.stringValue ?? "0")"
        
        self.invoiceNo = "Invoice No".localized + ": " + "\(rideDetail.invoice_no ?? "")"
        
        self.bookingDate = rideDetail.booking_time?.UTCToLocal1("yyyy-MM-dd HH:mm:ss", formatTo: "dd\nMMM\nyyyy") ?? ""
        
        self.distance = rideDetail.trip_info?.distance?.distanceValue ?? ""
        
        self.duration = "\(rideDetail.trip_info?.trip_time ?? 0 )".timeMinValue
        
        self.passengerName = rideDetail.user_info?.name ?? ""
        
        weekTime = rideDetail.booking_time?.UTCToLocal1("yyyy-MM-dd HH:mm:ss", formatTo:"EEEE, hh:mm a") ?? ""

        vehicleTypeName = rideDetail.vehicle_type_info?.display_name ?? ""
        
        vehicleName = rideDetail.vehicle_info?.model ?? ""
        
        destinationChangedfare = Extension.updateCurrecyRate(fare: "\(rideDetail.fare_breakup?.destination_change_fare?.doubleValue ?? 0)").currencyValue
        
        self.waitingTime = rideDetail.trip_info?.wait_time ?? 0

        waitingTimeFare = Extension.updateCurrecyRate(fare: "\(rideDetail.fare_breakup?.waiting_charges?.doubleValue ?? 0)").currencyValue

        self.totalFare = Extension.updateCurrecyRate(fare: "\(rideDetail.trip_info?.amount ?? 0)").currencyValue
        
        if rideDetail.actual_address?.isEmpty == true || rideDetail.actual_address == nil  {
            self.sourceAddress = rideDetail.estimated_address?.source?.address ?? ""
            self.destinationAddress = rideDetail.estimated_address?.destination?.address ?? ""
        } else {
            self.sourceAddress = rideDetail.actual_address?.source?.address ?? ""
            self.destinationAddress = rideDetail.actual_address?.destination?.address ?? ""
        }
        
        passengerPhone = (rideDetail.user_info?.ccp ?? "") + "-" + (rideDetail.user_info?.mobile ?? "")
        
        driverName = rideDetail.driver_info?.name ?? ""
        
        driverPhone = (rideDetail.driver_info?.ccp ?? "") + "-" + (rideDetail.driver_info?.mobile ?? "")
        
        licenseNo = rideDetail.vehicle_info?.number ?? ""
        rideFare = Extension.updateCurrecyRate(fare: "\(rideDetail.fare_breakup?.ride_fare?.doubleValue ?? 0)").currencyValue
        
        roundOff = Extension.updateCurrecyRate(fare: "\(rideDetail.fare_breakup?.round_off?.doubleValue ?? 0)").currencyValue
        
        cashPayment = Extension.updateCurrecyRate(fare: "\(rideDetail.payment_details?.cash_payment ?? 0)").currencyValue
        
        cardPayment = Extension.updateCurrecyRate(fare: "\(rideDetail.payment_details?.card_payment ?? 0)").currencyValue
        
        walletPayment = Extension.updateCurrecyRate(fare: "\(rideDetail.payment_details?.wallet_payment ?? 0)").currencyValue
        
        hasCardPayment = rideDetail.payment_details?.card_payment != nil && rideDetail.payment_details?.card_payment != 0
        
        hasCashPayment = rideDetail.payment_details?.cash_payment != nil && rideDetail.payment_details?.cash_payment != 0
        
        hasWalletPayment = rideDetail.payment_details?.wallet_payment != nil && rideDetail.payment_details?.wallet_payment != 0
    }
    
    func updateReceiptViewController(_ controller: ReceiptViewController) {
        
//        controller.bookingId.textColor = AppColors.colorAccent
//        controller.vehicleType.textColor = TpColor().colorTextDefault
//        controller.bookingDate.textColor = AppColors.colorAccent
//        controller.sourceAddress.textColor = TpColor().colorTextDefault
//        controller.destinationAddress.textColor = TpColor().colorTextDefault
//        controller.weekDayTimeLbl.textColor = TpColor().colorTextDefault
//        controller.distance.textColor = TpColor().colorTextDefault
//        controller.duration.textColor = TpColor().colorTextDefault
//        controller.passengerName.textColor = TpColor().colorTextDefault
//        controller.phoneNumber.textColor = TpColor().colorTextDefault
//        controller.driver.textColor = TpColor().colorTextDefault
//        controller.driverPhone.textColor = TpColor().colorTextDefault
//        controller.vehicle.textColor = AppColors.colorAccent
//        controller.licensePlage.textColor = TpColor().colorTextDefault
//        controller.rideFare.textColor = TpColor().colorTextDefault
//        controller.roundOff.textColor = TpColor().colorTextDefault
//        controller.card.textColor = TpColor().colorTextDefault
//        controller.cash.textColor = TpColor().colorTextDefault
//        controller.wallet.textColor = TpColor().colorTextDefault
//        controller.totalFare.textColor = AppColors.colorAccent
//        controller.rideType.textColor = TpColor().colorTextDefault
        
        controller.discountValueLbl?.text = Extension.updateCurrecyRate(fare: "\(rideDetail.trip_info?.discount?.doubleValue ?? 0)").currencyValue
        
        controller.discountValueLbl?.superview?.isHidden = (rideDetail.trip_info?.discount?.doubleValue ?? 0) <= 0
        
        controller.startReadingLbl?.text = self.rideDetail.rental?.startReading?.stringValue ?? ""
        controller.packageLbl?.text = self.rideDetail.rental?.packageName?.stringValue ?? ""
        controller.endReadingLbl?.text = self.rideDetail.rental?.endReading?.stringValue ?? ""
        
        controller.startReadingLbl?.superview?.isHidden = (self.rideDetail.rental?.startReading?.stringValue ?? "").isEmpty
        controller.endReadingLbl?.superview?.isHidden = (self.rideDetail.rental?.endReading?.stringValue ?? "").isEmpty
        controller.packageLbl?.superview?.isHidden = (self.rideDetail.rental?.packageName?.stringValue ?? "").isEmpty
        
        controller.bookingId.text = bookingId
        controller.invoiceNo.text = invoiceNo
        controller.vehicleType.text = vehicleTypeName
        controller.bookingDate.text = bookingDate
        controller.sourceAddress.text = sourceAddress.notMentioned
        controller.destinationAddress.text = destinationAddress.notMentioned
        controller.weekDayTimeLbl.text = weekTime
        controller.distance.text = distance
        controller.duration.text = duration
        controller.passengerName.text = passengerName
        controller.phoneNumber.text = passengerPhone
        controller.driver.text = driverName
        controller.driverPhone.text = driverPhone
        controller.vehicle.text = vehicleName
        controller.licensePlage.text = licenseNo
        controller.licensePlageLbl.text = "Plate Number".localized
        controller.rideFare.text = rideFare
        controller.extraKmValueLbl.text = Extension.updateCurrecyRate(fare: "\(rideDetail.fare_breakup?.extraKMFare?.doubleValue ?? 0)").currencyValue
        
        controller.extraKmValueLbl?.superview?.isHidden = (rideDetail.fare_breakup?.extraKMFare?.stringValue ?? "").isEmpty
        
        controller.roundOff.text = roundOff
        controller.card.text = cardPayment
        controller.cash.text = cashPayment
        controller.wallet.text = walletPayment
        controller.totalFare.text = totalFare
        controller.rideType.text = (self.rideDetail.category_id == "1" && self.rideDetail.ride_type == "Later" ? "5" : self.rideDetail.category_id)?.rideCategories.map{$0.stringValue}.first
        
        controller.cardContent.isHidden = !hasCardPayment
        controller.cashContent.isHidden = !hasCashPayment
        controller.walletContent.isHidden = !hasWalletPayment
        controller.destinationChargeValueLbl.text = destinationChangedfare
        controller.waitingChargeValueLbl.text = waitingTimeFare
        controller.waitingTimeValueLbl.text = "\(waitingTime)" + " Min"
        controller.peakTimeFareValueLbl.text = Extension.updateCurrecyRate(fare: "\(rideDetail.fare_breakup?.peak_time_charge?.doubleValue ?? 0)").currencyValue

        
        if rideDetail.fare_breakup?.destination_change_fare?.doubleValue ?? 0 <= 0{
            controller.destinationChargeValueLbl.isHidden = true
            controller.destinationChargeLbl.isHidden = true
        }
        if rideDetail.fare_breakup?.waiting_charges?.doubleValue ?? 0 <= 0{
            controller.waitingChargeValueLbl.isHidden = true
            controller.waitingChargeLbl.isHidden = true
        }
        if rideDetail.fare_breakup?.peak_time_charge?.doubleValue ?? 0 <= 0{
            controller.peakTimeFareValueLbl.isHidden = true
            controller.peakTimeFareLbl.isHidden = true
        }
        if rideDetail.fare_breakup?.extraKMFare?.doubleValue ?? 0 <= 0{
            controller.extraKmLbl.isHidden = true
            controller.extraKmValueLbl.isHidden = true
        }
        if rideDetail.trip_info?.wait_time ?? 0 <= 0{
            controller.waitingTimeValueLbl.isHidden = true
            controller.waitingTimeLbl.isHidden = true
        }
        
    }
    
}

extension String {
    var rideCategories: [RideCategory] {
        var categories = [RideCategory]()
        for id in self.components(separatedBy: ",") {
            if let intId = Int(id), let category = RideCategory.init(rawValue: intId) {
                categories.append(category)
            }
        }
        return categories
    }
}
