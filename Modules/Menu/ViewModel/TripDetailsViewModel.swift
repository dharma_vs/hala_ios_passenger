//
//  TripDetailsViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import Foundation
import UIKit

class TripDetailViewModel {
    
    let bookingId: String
    
    let  invoiceNo: String
    
    let bookingDate: String
    
    let totalFare: String
    
    let sourceAddress: String
    
    let destinationAddress: String
    
    let distance: String
    
    let duration: String
    
    let driverName: String
    
    let rating: Float
    
    let weekTime: String
    
    let vehicleName: String
    
    let vehicleModel: String
    
    let licensePlatNumber: String
    
    let status: String
    
    let userImageUrl: URL?
    
    let sourceCoordinate: LocationCoordinate
    
    let destinationCoordinate: LocationCoordinate
    
    let rideStatus: RideStatus?
    
    let rideDetail: RideDetail!
    
    let isAbleToCancel: Bool!
    
    var startReadingImage: String
    
    var endReadingImage: String
    
    let rideType: String
    
    let pickupContactno: String
    let pickupContactname: String
    let delivery_contact_name : String
    let delivery_contactno : String
    let item_information:String?
    let weight:Int?
    let Additioininfo:String?
    var waitingTime = 0

    init(_ rideDetail: RideDetail) {
        
        let rideStat = RideStatus.init(rawValue: rideDetail.booking_status ?? 0)
        
        self.rideType = rideDetail.ride_type?.lowercased() ?? ""
        
        self.rideStatus = rideStat
        
        self.rideDetail = rideDetail
        
        self.bookingId = "Booking Id".localized + ": " + "\(rideDetail.booking_id?.stringValue ?? "0")"
        
        self.invoiceNo = "Invoice No".localized + ": " + "\(rideDetail.invoice_no ?? "")"
        
        self.bookingDate = rideDetail.booking_time?.UTCToLocal1("yyyy-MM-dd HH:mm:ss", formatTo:"dd\nMMM\nyyyy") ?? ""
        
        self.distance = rideDetail.trip_info?.distance?.distanceValue ?? ""
        
        self.duration = "\(rideDetail.trip_info?.trip_time ?? 0 )".timeMinValue
        
        self.waitingTime = rideDetail.trip_info?.wait_time ?? 0

        self.driverName = rideDetail.driver_info?.name ?? ""
        
        self.userImageUrl = rideDetail.driver_info?.image?.urlFromString
        
        let rateString = rideDetail.driver_rating ?? "0"
        
        self.rating = Float(rateString == "" ? "0" : rateString) ?? 0.0
        
        
        weekTime = rideDetail.booking_time?.UTCToLocal1("yyyy-MM-dd HH:mm:ss", formatTo:"EEE, hh:mm a") ?? ""
        
        vehicleName = rideDetail.vehicle_type_info?.display_name ?? ""
        
        vehicleModel = rideDetail.vehicle_info?.model ?? ""
        
        licensePlatNumber = rideDetail.vehicle_info?.number ?? ""
        
        status = self.rideStatus?.tripStatus ?? ""
        
        self.pickupContactno = rideDetail.delivery?.pickupContactNumber ?? ""

        self.pickupContactname = rideDetail.delivery?.pickupContactName ?? ""
        self.delivery_contact_name = rideDetail.delivery?.deliveryContactName ?? ""
        self.delivery_contactno = rideDetail.delivery?.deliveryContactNumber ?? ""
        self.item_information = rideDetail.delivery?.itemInformation ?? ""
        self.weight =  rideDetail.delivery?.weight ?? 0
        self.Additioininfo = rideDetail.delivery?.descriptionNote ?? ""
        
//        let cancelStatuses = Set([RideStatus.UserCancelled, .DriverCancelled, .SystemCancelled, .NewRide, .Accepted, .Arrived])
        
        let cancelStatuses = Set([RideStatus.PaymentCompleted])
        
        if !cancelStatuses.contains(self.rideStatus!) {
        
//        if cancelStatuses.contains(self.rideStatus!) {
            
            self.totalFare = Extension.updateCurrecyRate(fare: "\(rideDetail.estimated_fare ?? 0)").currencyValue
            
        } else {
            
            self.totalFare = Extension.updateCurrecyRate(fare: "\(rideDetail.trip_info?.amount ?? 0)").currencyValue
            
        }
        
        if rideDetail.actual_address?.isEmpty == true || rideDetail.actual_address == nil {
            
            self.sourceAddress = rideDetail.estimated_address?.source?.address ?? ""
            
            self.destinationAddress = rideDetail.estimated_address?.destination?.address ?? ""
            
            self.sourceCoordinate = rideDetail.estimated_address?.source?.locationCoordinate ?? LocationCoordinate.zero
            
            self.destinationCoordinate = rideDetail.estimated_address?.destination?.locationCoordinate ?? LocationCoordinate.zero
            
        } else {
            self.sourceAddress = rideDetail.actual_address?.source?.address ?? ""
            
            self.destinationAddress = rideDetail.actual_address?.destination?.address ?? ""
            
            self.sourceCoordinate = rideDetail.actual_address?.source?.locationCoordinate ?? LocationCoordinate.zero
            
            self.destinationCoordinate = rideDetail.actual_address?.destination?.locationCoordinate ?? LocationCoordinate.zero
        }
        
        self.startReadingImage = rideDetail.rental?.startReadingImage?.stringValue ?? ""
        self.endReadingImage = rideDetail.rental?.endReadingImage?.stringValue ?? ""
        
        self.isAbleToCancel = (/*rideDetail.ride_type?.lowercased() != "now" && */(rideStat == .NewRide || rideStat == .Accepted))
    }
    
    func updateTripDetailsViewController(_ controller: TripDetailsViewController) {
        controller.rideTypeLbl.text = (self.rideDetail.category_id == "1" && self.rideDetail.ride_type == "Later" ? "5" : self.rideDetail.category_id)?.rideCategories.map{$0.stringValue}.first
        //controller.yourRatingLbl.text = "Your rating".localized
        controller.bookingId.text = self.bookingId
        //controller.invoiceNo.text = self.invoiceNo
        controller.tripTotal.text = self.totalFare
        controller.sourceAddress.text = self.sourceAddress
        controller.destinationAddress.text = self.destinationAddress.isEmpty ? "Not Mentioned".localized : self.destinationAddress
        controller.duration.text = self.duration
        controller.distance.text = self.distance
        
        let readingValue: String
        let readingEndValue: String
        let packageValue: String
        
        readingValue = self.rideDetail.rental?.startReading?.stringValue ?? ""
        readingEndValue = self.rideDetail.rental?.endReading?.stringValue ?? ""
        packageValue = self.rideDetail.rental?.packageName?.stringValue ?? ""
        
        
        
        //controller.packageLbl.text = packageValue
        //controller.readingStartValueLbl.text = readingValue
        //controller.readingEndValueLbl.text = readingEndValue
        self.startReadingImage = rideDetail.rental?.startReadingImage?.stringValue ?? ""
        self.endReadingImage = rideDetail.rental?.endReadingImage?.stringValue ?? ""
        controller.lblDriverNameans.text = self.driverName
        controller.date.text = self.bookingDate
        controller.completedDate.text = self.weekTime
        controller.rating.rating = self.rating
        controller.vehicleType.text = self.vehicleName
        controller.rideStatus.text = self.status
        //controller.passengerLbl.text = "Driver".localized
       // controller.licenseNumber.text = self.licensePlatNumber
        
       // controller.licenseInfoContent.isHidden = self.licensePlatNumber.isEmpty
        //controller.driverInfoContent.isHidden = self.passengerName.isEmpty
        //controller.vehicleInfoContent.isHidden = self.vehicleModel.isEmpty
        
//        controller.licensePlateLbl.text = "License Plate".localized
//        controller.vehicleLbl.text = "Vehicle".localized
//        controller.vehicleValue.text = self.vehicleModel
        controller.driverImage.loadTaxiImage(self.userImageUrl)
        controller.driverImage.isHidden = self.userImageUrl == nil
        controller.driverImageBtn.isHidden = self.userImageUrl == nil
        //controller.waitingTimeValueLbl.text = "\(waitingTime)" + " Min"
        controller.lblPickupnameans.text = self.pickupContactname
        controller.lblPickupContactans.text = self.pickupContactno
        controller.lblDeliverynameans.text = self.delivery_contact_name
        controller.lblDeliverycontactans.text = self.delivery_contactno
        controller.lblItemsans.text = self.item_information
        controller.lblTotalweightans.text = "\(self.weight ?? 0)"
        controller.lblAdditoninfoans.text = self.Additioininfo
        controller.lblVehicleans.text = self.vehicleModel
        controller.lblVehiclenoans.text = self.licensePlatNumber
        controller.lblBookedforans.text = self.rideDetail.bookingForName ?? ""
        var pickupSignature:String = baseImageURL + (rideDetail.pickupSignature ?? "")
        controller.imgPickupsignature.hnk_setImage(from: pickupSignature.urlFromString, placeholder: nil, success: { (image) in
                controller.imgPickupsignature?.image = image
            }, failure: nil)
        
        pickupSignature = baseImageURL + (rideDetail.deliverySignature ?? "")
        controller.imgDeliverysignature.hnk_setImage(from: pickupSignature.urlFromString, placeholder: nil, success: { (image) in
                controller.imgDeliverysignature?.image = image
            }, failure: nil)

        
        let statusCode = self.rideDetail.booking_status ?? 0
        let categoryID = self.rideDetail.category_id ?? "0"
        controller.stackDistancedet.isHidden = !(statusCode == 4 || statusCode == 5 || statusCode == 9)
        controller.stackPickupdet.isHidden = !(statusCode == 5 ||  statusCode == 9)
        controller.stackDeliverydet.isHidden = !(statusCode == 4 || statusCode == 5 || statusCode == 9)
        controller.stackItemsdet.isHidden = !(statusCode == 4 || statusCode == 5 || statusCode == 9)
        controller.stackDruverdet.isHidden = !(statusCode == 4 || statusCode == 5 || statusCode == 6 || statusCode == 7 || statusCode == 9)
        controller.stackSignaturedet.isHidden = !(statusCode == 4 || statusCode == 5 || statusCode == 9)
        controller.stackReciptdet.isHidden = !(statusCode == 4 || statusCode == 5 || statusCode == 9)
        controller.rating.isHidden = !(statusCode == 4 || statusCode == 5 || statusCode == 9)
        controller.lblDriverRating.isHidden = controller.rating.isHidden
        controller.stackBookedfordet.isHidden = self.rideDetail.bookingForName ?? "" == ""
        if statusCode == 9{ // completed
            controller.stackPickupdet.isHidden = !(categoryID == "6")
            controller.stackDeliverydet.isHidden = !(categoryID == "6")
            controller.stackItemsdet.isHidden = !(categoryID == "6")
            controller.stackSignaturedet.isHidden = !(categoryID == "6")
        }
        if rideDetail.trip_info?.wait_time ?? 0 <= 0{
                 /// controller.waitingTimeValueLbl.isHidden = true
                 // controller.waitingTimeLbl.isHidden = true
                  }
        
        let cancelStatuses = Set([RideStatus.UserCancelled, .DriverCancelled, .SystemCancelled])
        
//        controller.rideStatus.textColor = cancelStatuses.contains(self.rideStatus ?? .None) ? .red : self.rideStatus == .Started ? TpColor().green : .black
        
        controller.rideStatus.textColor = self.rideStatus?.color
        
        let upcomingStatues = Set([RideStatus.NewRide, .Accepted, .Arrived, .Started])
        
        if cancelStatuses.contains(self.rideStatus ?? .None) || upcomingStatues.contains(self.rideStatus ?? .None) {
            //controller.rateContentView.isHidden = true
            //controller.distanceContentView.isHidden = true
            //controller.durationContentView.isHidden = true
            //controller.viewReceiptBtn.isHidden = true

            controller.cancelView.isHidden = !self.isAbleToCancel
        }
        
        //controller.packageContentView.isHidden = !(self.rideType == "rental")
        //controller.startReadingContentView.isHidden = true
        //controller.endReadingContentView.isHidden = true
       // controller.packageLbl.text = self.rideDetail.rental?.packageName?.stringValue
        //let statusCode = self.rideDetail.booking_status ?? 0
        if self.rideType == "rental" {
           // controller.startReadingContentView.isHidden = !(statusCode == 4 || statusCode == 5 || statusCode == 9)
           // controller.endReadingContentView.isHidden = !(statusCode == 5 || statusCode == 9)
        }
        
        
        let statuses = Set([RideStatus.UserCancelled, .DriverCancelled, .SystemCancelled, .Accepted, .Arrived, .Started, .NewRide])
        if self.rideDetail.actual_address?.isEmpty == true || self.rideDetail.actual_address == nil {
            controller.mapImage.loadTaxiImage(self.rideDetail.estimated_mapInfo.mapURL.urlFromString)
        } else {
            controller.mapImage.loadTaxiImage(self.rideDetail.actual_mapInfo.mapURL.urlFromString)
        }
        
        func drawPolyline() {
            controller.mapViewHelper?.mapView?.clear()
            controller.sourceLocationMarker.position = self.sourceCoordinate
            controller.destinationLocationMarker.position = destinationCoordinate
            controller.sourceLocationMarker.map = controller.mapViewHelper?.mapView
            controller.destinationLocationMarker.map = controller.mapViewHelper?.mapView
            controller.mapViewHelper?.mapView?.drawPolygon(from: self.sourceCoordinate, to: self.destinationCoordinate, strokeWidth: 1.0, strokeColor: UIColor.colorAccent, onCompletion: { (polyline) -> (Void) in
                let line =  polyline()
                line.map = controller.mapViewHelper?.mapView
                controller.setMapImage?()
            })
            
            controller.mapViewHelper?.mapView?.isUserInteractionEnabled = false
        }
        
//        drawPolyline()
    }
    
}
