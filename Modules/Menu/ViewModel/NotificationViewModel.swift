//
//  NotificationViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 08/03/22.
//

import Foundation
import Alamofire

class NotificationViewModel: AppViewModel {
    weak var controller: NotificationViewController!
    required init(controller: NotificationViewController) {
        self.controller = controller
    }
}

extension ServiceManager {
    
    func getAllNotifications(_ currentPage: Int, completion: @escaping (_ inner: () throws ->  Paginator<NotificationInfo>?) -> (Void)) {
        
        var parameters = [String: Any]()
        parameters["pageSize"] = pageSize
        parameters["currentPage"] = currentPage
        
        self.request(endPoint: MenuDataServiceEndpoint.getAllNotifications, parameters: parameters as Dictionary<String, Any>?) { result in
            
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: Paginator<NotificationInfo>.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
