//
//  LicenceInfoViewModel.swift
//  Taxi
//
//  Created by VJ on 03/03/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

class LicenceInfoViewModel: NSObject {
    let regNo: String!
    let number: String!
    let issuedOn: String!
    let expiredOn: String!
    let licenceFront: URL!
    let licenceBack: URL!
    
    init(_ info: LicenceInfo) {
        number = info.licenceNumber?.stringValue
        issuedOn = info.issuedOn?.stringValue
        expiredOn = info.expiryOn?.stringValue
        regNo = info.registerNumber?.stringValue
        
        if let urlStr = info.licenceFront?.stringValue, let url = URL.init(string: baseImageURL + urlStr) {
            licenceFront = url
        } else {
            licenceFront = nil
        }
        
        if let urlStr = info.licenceRear?.stringValue, let url = URL.init(string: baseImageURL + urlStr) {
            licenceBack = url
        } else {
            licenceBack = nil
        }
    }
    
    func updateLicenceInfo(_ licenceVC: LicenceInfoViewController) {
        licenceVC.registerNumber.textFieldText = regNo
        licenceVC.number.textFieldText = number
//        licenceVC.issuedOn.textFieldText = issuedOn
//        licenceVC.expiredOn.textFieldText = expiredOn

        if issuedOn != ""{
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "yyyy-MM-dd"
            let showDate = inputFormatter.date(from: issuedOn)
            inputFormatter.dateFormat = "dd-MM-yyyy"
            let dateValue = inputFormatter.string(from: showDate!)
            licenceVC.issuedOn.textFieldText = dateValue
            if expiredOn != ""{
                
                inputFormatter.dateFormat = "yyyy-MM-dd"
                let showDate1 = inputFormatter.date(from: expiredOn)
                inputFormatter.dateFormat = "dd-MM-yyyy"
                let dateValue1 = inputFormatter.string(from: showDate1!)
                licenceVC.expiredOn.textFieldText = dateValue1

            }
            
        }
        licenceVC.issuedOn.date = issuedOn.dateWithFormat("dd-MM-yyyy")
        licenceVC.expiredOn.date = expiredOn.dateWithFormat("dd-MM-yyyy")
        
        if let url = licenceFront {
            licenceVC.frontImage.imageView.contentMode = UIView.ContentMode.scaleAspectFit
            licenceVC.frontImage.imageView.hnk_setImage(from: url, placeholder: nil)
        }
        
        if let url = licenceBack {
            licenceVC.backImage.imageView.contentMode = UIView.ContentMode.scaleAspectFit
            licenceVC.backImage.imageView.hnk_setImage(from: url, placeholder: nil)
        }
    }
}
