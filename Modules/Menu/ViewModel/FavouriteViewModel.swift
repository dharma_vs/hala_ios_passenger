//
//  SettingsViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit

class FavouriteViewViewModel: NSObject {
    
    weak var view: FavouriteView!
    var location: FavoriteLocation! {
        didSet {
            if let _loc = location {
                setAddress(_loc.locationName ?? "", address: _loc.locationAddress ?? "")
            }
        }
    }
    
    init(_ location: FavoriteLocation?) {
        self.location = location
    }
    
    func updateFavouriteView(_ fv: FavouriteView) {
        view = fv
    }
    
    func setAddress(_ title: String, address: String) {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(title)\n\(address)")
        attributeString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 13) as Any, range: NSMakeRange(0, title.count))

        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.colorBlack, range: NSMakeRange(0, title.count))
        attributeString.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 11) as Any, range: NSMakeRange(title.count, address.count+1))
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.darkGray, range: NSMakeRange(title.count, address.count+1))
        
        view.contentLbl.attributedText = attributeString
        view.rightBtn.setImage(UIImage(named: "img_delete")?.withRenderingMode(.alwaysTemplate), for: .normal)
    }
    
    func setLocationTitle(_ title: String) {
        view.contentLbl.text = title
        view.rightBtn.setImage(UIImage(named: "ic_setting_arrow")?.withRenderingMode(.alwaysTemplate), for: .normal)
        location = nil
    }
    
    func setLanguage() {
//        view.rightLbl.text = Language.language(Locale.getAppLanguage()).rawValue
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        view?.rightBtn?.tintColor = UIColor.colorBlack
    }
}

class SettingsViewModel: NSObject {
    
    let name: String?
    let imageURL: URL?
    let phone: String?
    let email: String?
    
    weak var svc: SettingsViewController!
    
    init(_ userInfo: UserInfo) {
        self.name = userInfo.name
        if let urlStr = userInfo.image, let url = URL.init(string: baseImageURL + urlStr) {
            self.imageURL = url
        } else {
            self.imageURL = nil
        }
        self.phone = userInfo.phone
        self.email = userInfo.email
    }
    
    func updateSettingViewController(_ settingVC: SettingsViewController) {
        self.svc = settingVC
        svc.profileImageView.loadTaxiImage(self.imageURL)
        svc.nameLbl?.text = name
        svc.phoneLbl?.text = phone
        svc.emailLbl?.text = email
        svc.languageView.viewModel.setLanguage()
        
        svc.homeFavouriteView.rightBtnAction = { [weak self] in
            DispatchQueue.main.async { [weak self] in
                self?.svc?.deleteFavLocation(self?.svc.homeFavouriteView.viewModel.location, completion: { [weak self] in
                    self?.svc.homeFavouriteView.viewModel.setLocationTitle("Add Home")
                })
            }
        }
        svc.homeFavouriteView.contentBtnAction = { [weak self] in
            DispatchQueue.main.async { [weak self] in
                let id = self?.svc.homeFavouriteView.viewModel?.location?.id ?? NSNotFound
                self?.svc?.showSearchViewController( String.Home, type: "1", id: id, completion: {[weak self] location in
                    self?.svc.homeFavouriteView.viewModel.location = location
                })
            }
        }
        
        svc.workFavouriteView.rightBtnAction = { [weak self] in
            DispatchQueue.main.async {[weak self] in
                self?.svc?.deleteFavLocation(self?.svc.workFavouriteView.viewModel.location, completion: {[weak self] in
                    self?.svc.workFavouriteView.viewModel.setLocationTitle("Add Work")
                })
            }
        }
        svc.workFavouriteView.contentBtnAction = { [weak self] in
            DispatchQueue.main.async { [weak self] in
                let id = self?.svc.workFavouriteView.viewModel?.location?.id ?? NSNotFound
                self?.svc?.showSearchViewController(String.Work, type: "2", id: id, completion: { [weak self] location in
                    self?.svc.workFavouriteView.viewModel.location = location
                })
            }
        }
        
//        svc.paymentView.contentBtnAction = { [weak self] in
//            let paymentVC = Router.main.instantiateViewController(withIdentifier: StoryboardIDs.PaymentViewController)
//            self?.svc.navigationController?.pushViewController(paymentVC, animated: true)
//        }
        
        svc.languageView.contentBtnAction = { [weak self] in
//            self?.svc.performSegue(withIdentifier: String.GotoChangeLanguageFromProfile, sender: self)
        }
        
        if UserDefaults.companyLogin {
            settingVC.locationContentView?.isHidden = true
            NSLayoutConstraint.deactivate([settingVC.topConstraint1])
        }
    }
    
}

extension UIViewController {
    
    func showSearchViewController(_  name: String, type: String, id: Int, isNeedToUpdate: Bool = true, searchCompletion: ((FavoriteLocationReqInfo?) -> Void)? = nil , completion: @escaping ((FavoriteLocation?) -> Void)) {
        let searchVC = SearchLocationViewController.initFromStoryBoard(.Main)
        searchVC.addressType = .favourite
        searchVC.didSelectLocation = {[unowned self] location in
            DispatchQueue.main.async {
                var favLocationReq = FavoriteLocationReqInfo()
                favLocationReq.address = location?.address
                favLocationReq.name = name
                favLocationReq.lat = location!.coordinate.latitude
                favLocationReq.lng = location!.coordinate.longitude
                favLocationReq.type = type
                
                let favLocation = FavoriteLocation()
                favLocation.locationAddress = location!.address
                favLocation.locationName = name
                favLocation.lat = location!.coordinate.latitude
                favLocation.lng = location!.coordinate.longitude
                favLocation.type = type.intValue()
                
                
                if !isNeedToUpdate {
                    searchCompletion?(favLocationReq)
                    return
                }
                
                if id == NSNotFound {
                    self.addFavouriteLocation(favLocationReq, completion: { [weak self] () in
                        completion(favLocation)
                        if let vc = self {
                            self?.navigationController?.popToViewController(vc, animated: true) }
                    })
                } else {
                    self.updateFavouriteLocation(favLocationReq, id: id, completion: { [weak self] in
                        completion(favLocation)
                        if let vc = self {
                            self?.navigationController?.popToViewController(vc, animated: true) }
                    })
                }
            }
        }
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
}


extension UIViewController {
    func deleteFavLocation(_ location: FavoriteLocation?, completion: @escaping (() -> Void)) {
        if let loc = location, let id = loc.id {
            self.startActivityIndicatorInWindow()
            ServiceManager().deleteFavouriteLocationById(id, completion: {[weak self]  (inner) -> (Void) in
                self?.stopActivityIndicator()
                do {
                    let response = try inner()
                    self?.view.makeToast(response?.message?.stringValue)
                    completion()
                } catch (_) {
                    
                }
            })
        }
    }
}

extension ServiceManager {
    
    func addFavouriteLocation(_ info: FavoriteLocationReqInfo, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
       
        self.request(endPoint: MainDataServiceEndpoint.addFavouriteLocation, body: info.JSONRepresentation) { result in
            
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: CommonRes.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
        
    func updateFavouriteLocation(_ info: FavoriteLocationReqInfo, id: Int,  completion: @escaping (_ inner: () throws -> CommonRes?) -> (Void)) {
            
            self.request(endPoint: MenuDataServiceEndpoint.updateFavouriteLocation, body: info.JSONRepresentation, pathSuffix: id as AnyObject as? String) { result in
                
                switch result {
                case .success(let data):
                    let response = data.getDecodedObject(from: CommonRes.self)
                    if ((response?.status) != nil) == true {
                        completion({ return response})
                    } else {
                        completion({ throw CoreError.init(code: 1000, description: response?.message?.stringValue ?? "", innerError: nil, informations: nil) })
                    }
                case .failure(let error):
                    completion({ throw error })
                }
            }
        }
    
    func deleteFavouriteLocationById(_ id: Int, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        self.request(endPoint: MenuDataServiceEndpoint.deleteFavouriteLocationById, pathSuffix: "/\(id)") { result in
            
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: CommonRes.self)
                if ((response?.status) != nil) == true {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message?.stringValue ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func getAllFavouriteLocations(_ completion: @escaping (_ inner: () throws -> [FavoriteLocation]?) -> (Void)) {
        
        self.request(endPoint: MenuDataServiceEndpoint.getAllFavouriteLocations) { result in
            
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: GeneralResponse<[FavoriteLocation]>.self)
                if let locations = response?.data, response?.status == true {
                    completion({ return locations})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}


