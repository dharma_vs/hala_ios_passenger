//
//  AboutViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 05/03/22.
//

import Foundation

class AboutViewModel: AppViewModel {
    weak var controller: AboutViewController!
    required init(controller: AboutViewController) {
        self.controller = controller
    }
    
    func getAboutInfo() {
        self.controller?.startActivityIndicatorInWindow()
        ServiceManager().aboutInfo { [weak self](inner) in
            do {
                let aboutInfo = try inner()
                self?.controller?.updateUI(aboutInfo)
            } catch _ {
                
            }
            self?.controller?.stopActivityIndicator()
        }
    }
    
}

extension ServiceManager {
    func aboutInfo(_ completion: @escaping (_ inner: () throws ->  AboutUsInfo) -> Void) {
        self.request(endPoint: MenuDataServiceEndpoint.aboutUsInfo, parameters: nil, body: nil, pathSuffix: nil) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response = data.getDecodedObject(from: GeneralResponse<AboutUsInfo>.self)
                if let data = response?.data, response?.status == true {
                    completion({return data})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
