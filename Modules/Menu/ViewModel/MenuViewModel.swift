//
//  MenuViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import Foundation

extension UserInfo {
    var phone: String {
        let ccp = (self.countryCode ?? defaultCountryCode)
        return "\(ccp.hasPrefix("+") ? ccp : "+"+(ccp.isEmpty ? defaultCountryCode.replacingOccurrences(of: "+", with: "") : ccp))" + " " + (self.phoneNo ?? "")
    }
}

class MenuViewModel: NSObject {
    
    let name: String?
    let imageURL: URL?
    let phone: String?
    let email: String?
    
    init(_ userInfo: UserInfo) {
        self.name = userInfo.name
        if let urlStr = userInfo.image, let url = URL.init(string: baseImageURL + urlStr) {
            self.imageURL = url
        } else {
            self.imageURL = nil
        }
        self.phone = userInfo.phone
        self.email = userInfo.email
    }
    
    func updateMenuViewController(_ menuVC: MenuViewController) {
        
        menuVC.imageView.loadTaxiImage(self.imageURL)
        menuVC.nameLbl?.text = name
        menuVC.phoneLbl?.text = phone
        menuVC.emailLbl?.text = email
    }
}

extension ServiceManager {
    
    func logout(_ completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        let body = ["device_id": device_Id]  as AnyObject?
        
        self.request(endPoint: MenuDataServiceEndpoint.logout, body: body as! Dictionary<String, Any>?) { result in
            
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: CommonRes.self)
                if let status = response, (status.status != nil) == true {
                    completion({ return response})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message?.stringValue ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
