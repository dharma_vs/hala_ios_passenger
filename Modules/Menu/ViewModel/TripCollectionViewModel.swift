//
//  TripCollectionViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 14/03/22.
//

import Foundation
import Alamofire

class TripCollectionViewModel: AppViewModel {
    weak var controller: TripCollectionViewCell!
    required init(controller: TripCollectionViewCell) {
        self.controller = controller
    }
}

extension ServiceManager {
    
    func rideList(_ currentPage: Int, rideType: RideListType, completion: @escaping (_ inner: () throws ->  Paginator<RideListItem>?) -> (Void)) {
        
        var parameters = [String: Any]()
        parameters["type"] = rideType == .open ? "1" : "2"
        parameters["pageSize"] = pageSize
        parameters["currentPage"] = currentPage
        parameters["company_id"] = UserDefaults.companyLogin ? CacheManager.riderInfo?.company_id ?? 0 : "0"
        
        self.request(endPoint: MenuDataServiceEndpoint.rideList, parameters: parameters as Dictionary<String, Any>?) { result in
            
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: GeneralResponse<RideListResponse>.self)
                if let data = response?.data, response?.status == true {
                    completion({ return data})
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}


