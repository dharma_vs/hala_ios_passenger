//
//  FeedbackViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 08/03/22.
//

import Foundation

class FeedbackViewModel: AppViewModel {
    weak var controller: FeedbackViewController!
    required init(controller: FeedbackViewController) {
        self.controller = controller
    }
}

extension ServiceManager {
    
    func appFeedback(_ info: AppFeedBackReqInfo, completion: @escaping (_ inner: () throws ->  CommonRes?) -> Void) {
        self.request(endPoint: MenuDataServiceEndpoint.appFeedback, body: info.JSONRepresentation) { result in
            switch result {
            case .success(let data):
                let response = data.getDecodedObject(from: CommonRes.self)
                if let status = response, (status.status != nil) {
                    completion({return response})
                } else {
                    completion({ return  nil})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
