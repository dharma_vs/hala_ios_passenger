//
//  CardListViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 07/03/22.
//

import Foundation

class CardListViewModel: AppViewModel {
    weak var controller: CardListViewController!
    required init(controller: CardListViewController) {
        self.controller = controller
    }
}



extension ServiceManager {
    
    func deleteSavedCard(_ id: Int, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        self.request(endPoint: MenuDataServiceEndpoint.deleteCards, pathSuffix: "/\(id)") { result in
            
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: CommonRes.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
