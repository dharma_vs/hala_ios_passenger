//
//  VerificationViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit
import TTTAttributedLabel
import Toast_Swift

class VerificationViewController: BaseViewController {
    
    @IBOutlet weak var titleLbl1: AppLabel!
    @IBOutlet weak var titleLbl2: AppLabel!
    @IBOutlet weak var updateBtn: AppButton!
    @IBOutlet weak var phoneNumberLbl: AppLabel!
    @IBOutlet var otpTextFields: [AppTextField]!
    @IBOutlet weak var resendLabel: TTTAttributedLabel!
    
    var phoneDetail: (String, String, String)! // (ccp, phone, otp)
    var resendOTPHandler: (() -> Void)?
    var otpVerificationSucceeds: (((String, String, String)) -> Void)?
    
    @IBAction func editPhoneNumber(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        if CacheManager.settings?.sms_enable != "true"{
            for (idx,char) in phoneDetail.2.enumerated() {
                self.otpTextFields?[idx].text = String(char)
            }
        }
    }
    
    override func setup() {
        //        setTpBackground()
        resendLabelLayout()
    }
    
    override func localize() {
        super.localize()
        
        self.updateBtn?.setTitle("Update".localized, for: .normal)
        titleLbl1.text = "Update Phone Number".localized
        titleLbl2.text = "Please input the 6 digit code sent to you".localized
        phoneNumberLbl.text = self.phoneDetail.0 + "-" + self.phoneDetail.1
        
    }
    
    func resendLabelLayout() {
        let resend = "Resend".localized
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\("You didn't receive any code?".localized) \(resend)")
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.colorAccent, range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Montserrat-Bold", size: 15) as Any, range: NSMakeRange(0, attributeString.length))
        
        resendLabel.linkAttributes = [NSAttributedString.Key.foregroundColor: UIColor.colorAccent.cgColor, NSAttributedString.Key.underlineStyle: 1]
        resendLabel.attributedText = attributeString
        resendLabel.addLink(to: URL(string: "local://resend")!, with: NSMakeRange(26, resend.count))
    }
    
    
    @IBAction func continueBtnPressed() {
        var otp = ""
        otpTextFields.forEach { (textField) in
            if let text = textField.text, !text.isEmpty {
                otp += text
            }
        }
        
        if otp.count == 6, otp == phoneDetail.2.decrypted {
            otpVerificationSucceeds?(self.phoneDetail)
        } else {
            self.view.makeToast("Enter valid OTP".localized)
        }
    }
    
    func sendOTP() {
        let phone = phoneDetail.1
        let code = phoneDetail.0
        let body = ["tel_no": phone.encrypted, "ccp": "+91"] as AnyObject
        self.startActivityIndicatorInWindow()
        
        ServiceManager().sendOTP(OTP.self, body: body, completion: {[weak self] (inner) in
            self?.stopActivityIndicator()
            do {
                let response = try inner()
                if response?.status == true, let otp = response?.data?.otp {
                    if CacheManager.settings?.sms_enable != "true"{
                        for (idx,char) in otp.enumerated() {
                            self?.otpTextFields?[idx].text = String(char)
                        }
                    }
                    self?.phoneDetail.2 = otp
                }
                self?.view.makeToast(response?.message)
            } catch (let e) {
                self?.view.makeToast("\("Failed".localized) \(e.localizedDescription)")
            }
        })
    }
}

extension VerificationViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    @IBAction func textFieldDidChange(textField: UITextField) {
        let text = textField.text
        
        if text?.count == 1 {
            switch textField {
            case otpTextFields[0]:
                otpTextFields[1].becomeFirstResponder()
            case otpTextFields[1]:
                otpTextFields[2].becomeFirstResponder()
            case otpTextFields[2]:
                otpTextFields[3].becomeFirstResponder()
            case otpTextFields[3]:
                otpTextFields[3].resignFirstResponder()
            default:
                break
            }
        } else {
            
        }
    }
}

extension VerificationViewController: TTTAttributedLabelDelegate {
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        if url.scheme == "local", url.host == "resend" {
            sendOTP()
        }
    }
}
