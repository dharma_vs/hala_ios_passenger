//
//  LicenceInfoViewController.swift
//  Taxi
//
//  Created by VJ on 28/02/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit
import Toast_Swift

class LicenceInfoViewController: BaseViewController {

    @IBOutlet weak var registerNumber: TextFieldWithTitleView!
    @IBOutlet weak var number: TextFieldWithTitleView!
    @IBOutlet weak var issuedOn: TextFieldWithTitleView!
    @IBOutlet weak var expiredOn: TextFieldWithTitleView!
    
    @IBOutlet weak var frontImage: ImageViewWithTitleView!
    @IBOutlet weak var backImage: ImageViewWithTitleView!
    @IBOutlet weak var updateBtn: UIButton!
    
    var onDidUpdate: ((LicenceInfo) -> Void)!
    var submitBtnTitle: (() -> String)!
    
    var viewModel: LicenceInfoViewModel! {
        didSet {
            viewModel.updateLicenceInfo(self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNumber.textField.text = ""
        self.issuedOn.maxDate = Date()
        self.expiredOn.minDate = Date()
    }
    
    override func setup() {
        super.setup()
        viewModel = LicenceInfoViewModel(CacheManager.driverCompleteInfo!.licence_info!)
    }
    
    override func localize() {
        super.localize()
        updateBtn.setTitle(submitBtnTitle?().localized, for: .normal)
    }
    
    @IBAction func updateBtnPressed() {
        view.endEditing(true)
        
        func updateLicence(_ licence: LicenceInfo) {
            ServiceManager().updateLicence(licence) {[weak self] (inner) -> (Void) in
                self?.stopActivityIndicator()
                do {
                    let response = try inner()
                    self?.view?.makeToast(response?.message?.stringValue, duration: 1.0, position: .bottom, title: nil, image: nil, style: ToastStyle(), completion: { [weak self] (success) in
                        if response?.isSuccess == true {
                            self?.onDidUpdate?(licence)
                        }
                    })
                } catch (_) {
                    
                }
            }
        }
        
        func uploadLicenceFront(_ completion: @escaping (String?) -> (Void)) {
            if let image = frontImage.imageViewImage {
                ServiceManager().upload(image, type: "licences") { (inner) -> (Void) in
                    do {
                        let response = try inner()
                        if response?.status == true {
                            completion(response?.data?.name!)
                        } else {
                            completion(nil)
                        }
                    } catch(_) {
                        completion(nil)
                    }
                }
            } else {
                completion(nil)
            }
        }
        
        func uploadLicenceRear(_ completion: @escaping (String?) -> (Void)) {
            if let image = backImage.imageViewImage {
                ServiceManager().upload(image, type: "licences") { (inner) -> (Void) in
                    do {
                        let response = try inner()
                        if response?.status == true {
                            completion(response?.data?.name!)
                        } else {
                            completion(nil)
                        }
                    } catch(_) {
                        completion(nil)
                    }
                }
            } else {
                completion(nil)
            }
        }
        
        if let licenceInfo = isValidForm() as? LicenceInfo {
            self.startActivityIndicatorInWindow()

            uploadLicenceFront { (name) -> (Void) in
                if let _name = name {
//                    licenceInfo.licenceFront = _name.currencyValue
                }
                
                uploadLicenceRear { (name) -> (Void) in
                    if let _name = name {
//                        licenceInfo.rear_image = _name
                    }
                    updateLicence(licenceInfo)
                }
            }
        }
    }
    
    func isValidForm() -> Any? {
        let info = LicenceInfo()
        
        func showMessage(_ message: String) -> Any? {
            self.view.makeToast(message)
            return nil
        }
        
//        if registerNumber.textFieldText?.isEmpty == true {
//            return showMessage("Please enter Register number".localized)
//        } else {
//        info.registerNumber = (registerNumber.textFieldText ?? "")
//        }
        
        if number.textFieldText?.isEmpty == true {
            return showMessage("Please enter Licence number".localized)
        } else {
//            info.number = number.textFieldText
        }
        
        if issuedOn.textFieldText?.isEmpty == true {
            return showMessage("Please select issued date".localized)
        } else {
            
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "dd-MM-yyyy"
            let showDate = inputFormatter.date(from: issuedOn.textFieldText ?? "")
            inputFormatter.dateFormat = "yyyy-MM-dd"
            let resultString = inputFormatter.string(from: showDate!)
//            info.issued = resultString
        }
        
        if expiredOn.textFieldText?.isEmpty == true {
            return showMessage("Please select expiry date".localized)
        } else {
            
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "dd-MM-yyyy"
            let showDate = inputFormatter.date(from: expiredOn.textFieldText ?? "")
            inputFormatter.dateFormat = "yyyy-MM-dd"
            let resultString = inputFormatter.string(from: showDate!)
//            info.expiry = resultString
        }
        
        if frontImage.imageViewImage == nil {
            return showMessage("Please upload Driving licence image".localized)
        }
        
        if backImage.imageViewImage == nil {
            return showMessage("Please upload Licence image".localized)
        }
        
//        info.driver_status = CacheManager.driverCompleteInfo?.driver_status
        
        return info
    }
}

extension LicenceInfoViewController: ImageViewWithTitleViewDatasource {
    func presender() -> UINavigationController {
        return navigationController!
    }
}

extension ServiceManager {
    func updateLicence(_ info: LicenceInfo, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        self.request(endPoint: ProfileDataServiceEndpoint.updateLicence, parameters: nil, body: info.JSONRepresentation, pathSuffix: nil) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                if let response = data.getDecodedObject(from: CommonRes.self), response.isSuccess == true {
                    completion({ return response })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: "Failed", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
