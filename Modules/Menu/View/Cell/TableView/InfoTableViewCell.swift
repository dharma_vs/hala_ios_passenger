//
//  FooterTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 07/04/22.
//

import Foundation
import UIKit

class InfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var homeImg: AppImageView!
    @IBOutlet weak var addHomeLbl: AppLabel!
    @IBOutlet weak var dropDownBtn: AppButton!
    @IBOutlet weak var switchCtrl: AppSwitch!
    @IBOutlet weak var seperator: AppImageView!
    
    @IBAction func switchCtrl(_ sender: AppSwitch) {
        
    }
    
    func setValues(_ type: SettingsType) {
        switch type {
        case .AddHome:
            dropDownBtn?.setImage(#imageLiteral(resourceName: "dwnArrowSmall"), for: .normal)
            dropDownBtn?.setTitle(nil, for: .normal)
            switchCtrl?.isHidden = true
            seperator?.isHidden = true
        case .AddWork:
            dropDownBtn?.setImage(#imageLiteral(resourceName: "dwnArrowSmall"), for: .normal)
            dropDownBtn?.setTitle(nil, for: .normal)
            switchCtrl?.isHidden = true
            seperator?.isHidden = true
        case .SavedPlaces:
            seperator?.isHidden = false
            break
        case .Language:
            dropDownBtn?.setImage(#imageLiteral(resourceName: "dwnArrowSmall"), for: .normal)
            dropDownBtn?.setTitle(nil, for: .normal)
            switchCtrl?.isHidden = true
            seperator?.isHidden = false
        case .Currency:
            dropDownBtn?.setImage(#imageLiteral(resourceName: "dwnArrowSmall"), for: .normal)
            dropDownBtn?.setTitle(nil, for: .normal)
            switchCtrl?.isHidden = true
            seperator?.isHidden = false
        case .Speech:
            dropDownBtn?.isHidden = true
            switchCtrl?.isHidden = false
            seperator?.isHidden = false
        case .SavedCards:
            dropDownBtn?.setImage(#imageLiteral(resourceName: "dwnArrowSmall"), for: .normal)
            dropDownBtn?.setTitle(nil, for: .normal)
            switchCtrl?.isHidden = true
            seperator?.isHidden = false
        case .Documents:
            dropDownBtn?.setImage(#imageLiteral(resourceName: "dwnArrowSmall"), for: .normal)
            dropDownBtn?.setTitle(nil, for: .normal)
            switchCtrl?.isHidden = true
            seperator?.isHidden = false
        }
    }
}
