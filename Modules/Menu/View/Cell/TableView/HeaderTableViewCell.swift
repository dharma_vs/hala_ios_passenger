//
//  HeaderTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 07/04/22.
//

import Foundation
import UIKit

class HeaderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var favouritesLbl: AppLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.favouritesLbl?.attributedText = "View other saved places".localized.underlined
    }
}

