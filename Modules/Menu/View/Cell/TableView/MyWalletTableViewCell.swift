//
//  MyWalletTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit

class MyWalletTableViewCell: UITableViewCell {

    @IBOutlet weak var walletLbl: AppLabel!
    @IBOutlet weak var dateTimeLbl: AppLabel!
    @IBOutlet weak var VehicleImg: AppImageView!
    @IBOutlet weak var messageLbl: AppLabel!
    @IBOutlet weak var receiptLbl: AppButton!
    @IBOutlet weak var timeLbl: AppLabel!
    
}
