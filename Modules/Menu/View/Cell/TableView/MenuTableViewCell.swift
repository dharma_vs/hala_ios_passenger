//
//  MenuTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit

class MenuCell: UITableViewCell {
    
    @IBOutlet weak var icon: AppImageView!
    @IBOutlet weak var title: AppLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
