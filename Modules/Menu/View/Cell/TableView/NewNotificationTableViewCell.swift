//
//  NewNotificationTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit

let newNotificationTableViewCellIdentifier = "NewNotificationTableViewCell"

class NewNotificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageheight: NSLayoutConstraint!
    
    @IBOutlet weak var headingLbl: AppLabel!
    
    @IBOutlet weak var notificationImage: AppImageView!
    
    @IBOutlet weak var messageLbl: AppLabel!
    
    @IBOutlet weak var urlLbl: AppLabel!
    
    @IBOutlet weak var dateTimeLbl: AppLabel!
    
    var didTapOnUrl: (()-> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapGesure = UITapGestureRecognizer.init(target: self, action: #selector(handleTap(_:)))
        tapGesure.numberOfTapsRequired = 1
        self.urlLbl.isUserInteractionEnabled = true
        self.urlLbl.addGestureRecognizer(tapGesure)

        
        // Initialization code
    }
    
    @objc func handleTap(_ sender: Any) {
        self.didTapOnUrl?()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
