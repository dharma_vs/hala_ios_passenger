//
//  TripTableViewCell.swift
//  Taxi
//
//  Created by SELLADURAI on 2/13/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

class TripTableViewCell: UITableViewCell {
    @IBOutlet weak var rideTypeLbl: AppLabel!
    @IBOutlet weak var dateLbl: AppLabel!
    @IBOutlet weak var weekDayTimeLbl: AppLabel!
    @IBOutlet weak var vehicleTypeLbl: AppLabel!
    @IBOutlet weak var tripStatusLbl: AppLabel!
    @IBOutlet weak var addressView: RideAddressView!
    
    @IBOutlet weak var sourceAddressLbl: AppLabel!
    @IBOutlet weak var destinationAddressLbl: AppLabel!
    @IBOutlet weak var rideFareAmountLbl: AppLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
