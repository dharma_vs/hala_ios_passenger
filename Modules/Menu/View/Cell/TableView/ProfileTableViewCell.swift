//
//  ProfileTableViewCell.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 07/04/22.
//

import Foundation
import UIKit

class ProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var accountLbl: AppLabel!
    @IBOutlet weak var profileImg: AppImageView!
    @IBOutlet weak var nameLbl: AppLabel!
    @IBOutlet weak var mobileNoLbl: AppLabel!
    @IBOutlet weak var emailLbl: AppLabel!

}
