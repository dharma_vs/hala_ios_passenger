//
//  MywalletCell.swift
//  TaxiPickup
//
//  Created by TAMILARASAN on 26/01/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

class MywalletCell: UITableViewCell {

    @IBOutlet weak var img_Cell: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var btn_Receipt: UIButton!
    @IBOutlet weak var lbl_Message: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
