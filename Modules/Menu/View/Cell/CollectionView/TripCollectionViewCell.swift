//
//  TripCollectionViewCell.swift
//  Taxi
//
//  Created by SELLADURAI on 2/13/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit
import CoreMedia

enum RideListType: String {
    case open = "OPEN"
    case close = "CLOSE"
}

class TripCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var tripsTableView: UITableView!
    @IBOutlet weak var noResultLbl: AppLabel!
    
    var paginator: Paginator<RideListItem>!
    var rideListItems = [RideListItem]() {
        didSet {
            tripsTableView.reloadData()
        }
    }
    fileprivate var rideType = RideListType.open
    var activityIndicator: ((Bool) -> Void)!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        noResultLbl.text = "No rides in your history".localized
    }
    
    func loadData(_ page: Int = 0, type: RideListType) {
        rideType = type
        
        //var pageSizeValue = page
        if UserDefaults.companyLogin && tripsTableView.tag == 0{
            self.rideListItems.removeAll()
        }
        
        if let _paginator = paginator, (page) > (_paginator.totalPages ?? 0) {
            return
        }
        
        self.activityIndicator?(true)
        ServiceManager().rideList(page, rideType: type) { [weak self] (inner) -> (Void) in
            do {
                self?.paginator = try inner()
                
                if let rideList = self?.paginator?.result {
                    self?.rideListItems = self?.rideListItems.appendWithoutDuplicates(rideList) ?? []
                }
                
                if self?.rideListItems.count == 0 {
                    self?.tripsTableView.isHidden = true
                    self?.noResultLbl.isHidden = false
                    

                }else{
                    if UserDefaults.companyLogin && self?.tripsTableView.tag == 0{
                        self?.tripsTableView.isHidden = false
                        self?.noResultLbl.isHidden = true
                         self?.tripsTableView.reloadData()
                        
                    }
                   
                }
            } catch(_) {
                
            }
            self?.activityIndicator?(false)
        }
    }
}

extension TripCollectionViewCell: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rideListItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = rideListItems[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripTableViewCell", for: indexPath) as! TripTableViewCell
        cell.dateLbl?.text = item.booking_time?.UTCToLocal1("yyyy-MM-dd HH:mm:ss", formatTo:"dd\nMMM\nyyyy")
        cell.tripStatusLbl.text = RideStatus(rawValue: item.booking_status ?? 0)?.tripStatus
        cell.tripStatusLbl.textColor = RideStatus(rawValue: item.booking_status ?? 0)?.color
        cell.vehicleTypeLbl.text = item.vehicle_type_name
        cell.weekDayTimeLbl.text = item.booking_time?.UTCToLocal1("yyyy-MM-dd HH:mm:ss", formatTo:"EEE, hh:mm a")
        
        
        //cell.rideTypeLbl.text = (item.category_id == "1" && item.ride_type == "Later" ? "5" : item.category_id)?.rideCategories.map{$0.stringValue}.first
       // cell.rideTypeLbl.text  =  "\(cell.rideTypeLbl.text ?? "" + "\n" + item.booking_id!)"
        let rideType = (item.category_id == "1" && item.ride_type == "Later" ? "5" : item.category_id)?.rideCategories.map{$0.stringValue}.first ?? ""
        cell.rideTypeLbl.text  =   rideType + "\n" + "\(item.booking_id!)"

        cell.rideTypeLbl.setNeedsLayout()
        cell.sourceAddressLbl.text = item.s_address ?? "Not mentioned".localized
        cell.destinationAddressLbl.text = item.d_address?.isEmpty == true || item.d_address == nil ? "Not Mentioned".localized: item.d_address
        cell.rideFareAmountLbl.text = Extension.updateCurrecyRate(fare: "\(item.fare ?? 0)").currencyValue
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (rideListItems.count - 1) {
            loadData(paginator.currentPage!.intValue() + 1, type: rideType)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.rideListItems[indexPath.row]
        
        if tableView.tag == 0 && UserDefaults.companyLogin{
           
            let detailVC = DashboardViewController.initFromStoryBoard(.Main)
                UIViewController.rootNavigation?.pushViewController(detailVC, animated: true)
                
            
        }else{
            let detailVC = TripDetailsViewController.initFromStoryBoard(.Menu)
            detailVC.rideId = "\(item.id ?? 0)"
            UIViewController.rootNavigation?.pushViewController(detailVC, animated: true)
            
        }
    }
}

extension TripCollectionViewCell: UIScrollViewDelegate {
    
    //  MARK: - UIScrollViewDelegate
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView == tripsTableView {
//            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
//                if rideListItems.count > 5 {
//                loadData(paginator.currentPage!.intValue() + 1, type: rideType)
//                }
//            }
//        }
//    }
}

extension Array where Element == RideListItem {
    func appendWithoutDuplicates(_ list: [RideListItem]) -> [RideListItem] {
        var existingItems = self
        let ids = existingItems.map{$0.id}
        list.forEach { item in
            if ids.contains(item.id) == false {
                existingItems.append(item)
            }
        }
        return existingItems
    }
}
