//
//  FeedbackViewController.swift
//  TaxiPickup
//
//  Created by Prabakaran on 21/01/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

class FeedbackViewController: BaseViewController {
    @IBOutlet weak var txtview_Feedback_Placeholder: AppLabel!
    @IBOutlet weak var txtview_Feedback: AppTextView!
    @IBOutlet weak var titlelbl: AppLabel!
    @IBOutlet weak var descLbl: AppLabel!
    @IBOutlet weak var submitBtn: AppButton!
    @IBOutlet weak var menuBtn: AppButton!
    @IBOutlet weak var feedbackImg: AppImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
//        setTpBackground()
        txtview_Feedback?.text = ""
        titlelbl?.text = "Feedback".localized
        descLbl?.text = "How was your experience with us?".localized
        submitBtn?.setTitle("Submit".localized.tripleSpaced, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnclk_Submit() {
        guard self.txtview_Feedback.text.isEmpty == false else {
            self.view.makeToast("Enter feed back info".localized)
            return
        }
        var info = AppFeedBackReqInfo()
        info.feedback = self.txtview_Feedback?.text ?? ""
        self.startActivityIndicatorInWindow()
        ServiceManager().appFeedback(info) { [weak self](inner) in
            do {
                let response = try inner()
                self?.view.makeToast("Thanks for your feedback")
            } catch {
            //    self?.view.makeToast(error.localizedDescription)
            }
            self?.txtview_Feedback?.text = ""
            self?.stopActivityIndicator()
        }
    }
    
    @IBAction func ShowSlidemenu()
    {
        self.slideMenuController()?.openLeft()
    }
}

extension FeedbackViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let string = textView.text?.replacingCharacters(in: Range(range, in: textView.text!)!, with: text) {
            txtview_Feedback_Placeholder?.isHidden = string.count > 0
        }
        return true
    }
}
