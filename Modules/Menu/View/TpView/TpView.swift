//
//  TpView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 05/03/22.
//

import Foundation
import UIKit

//@IBDesignable
class TpView: UIView {

}

//@IBDesignable
class TpImageView: AppImageView {
 
}

//@IBDesignable
class TpButton: AppButton {
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 0
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        layer.borderColor = UIColor.white.cgColor
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.shadowPath = shadowPath.cgPath
        
    }
}
