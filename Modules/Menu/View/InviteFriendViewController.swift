//
//  InviteFriendViewController.swift
//  TaxiPickup
//
//  Created by Prabakaran on 21/01/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String ?? ""

class InviteFriendViewController: BaseViewController {

    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var inviteInfo1: AppLabel!
    @IBOutlet weak var inviteInfo2: AppLabel!
    @IBOutlet weak var shareInfo: AppLabel!
    @IBOutlet weak var inviteCodeLbl: AppLabel!
    @IBOutlet weak var inviteFriendsBtn: AppButton!
    @IBOutlet weak var menuBtn: AppButton!
    @IBOutlet weak var inviteFriendsImg: AppImageView!
    @IBOutlet weak var shareBtn: AppButton!
    
    @IBAction func inviteFriends(_ sender: Any) {
        let descrption = "Hey, Please join Hala Mobility\n\nAndroid: https://play.google.com/store/apps/details?id=com.jeffery.passenger\n\niOS: https://apps.apple.com/us/app/id1517865497\n\nUse my referral code " + inviteCodeLbl.text!
        let inviteContent = [ (UIImage.init(named: "") ?? UIImage()), ("\n" + descrption)] as [Any]
        self.share(items: inviteContent)
    }
    
    @IBAction func copyInviteCode(_ sender: Any) {
        
        UIPasteboard.general.string = CacheManager.riderInfo?.myReferralCode
        self.view.makeToast("Referral Code Copied".localized)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inviteCodeLbl.text = CacheManager.riderInfo?.myReferralCode
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func localize() {
        super.localize()
//        setTpBackground()
        titleLbl.text = "Invite Friends".localized
//        inviteInfo1.text = "Invite Friends\nGet 3 coupons each".localized
//        inviteInfo1.text = CacheManager.settings?.passenger_referral_title = "Invite Friends Get 3 coupons each".localized
        inviteInfo1.text = "Invite Friends Get 3 coupons each".localized
        inviteInfo2.text =  CacheManager.settings?.passenger_referral_message//String.when_your_friend_signs_up_with_your_referral_code_you_both_get_3_coupons.localized
        shareInfo.text = "Share your invite code".localized
        inviteCodeLbl.text = "Share your invite code".localized
        inviteFriendsBtn.setTitle("Invite Friends".localized.tripleSpaced, for: .normal)
    }
}

extension InviteFriendViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
}
