//
//  TripsViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit

class TripsViewController: BaseViewController {
    
    @IBOutlet weak var openedBtn: UIButton!
    @IBOutlet weak var pastBtn: UIButton!
    
    @IBAction func showUpcomingTrips(_ sender: Any) {
        if let currentIndexPath = self.contentCollectionView?.indexPathsForVisibleItems.first, currentIndexPath.item != 0 {
            self.contentCollectionView?.scrollToItem(at: IndexPath.init(item: 0, section: 0), at: UICollectionView.ScrollPosition.left, animated: true)
            if let cell = self.contentCollectionView?.cellForItem(at: IndexPath.init(item: 0, section: 0)) as? TripCollectionViewCell {
                cell.rideListItems = []
                cell.paginator = Paginator()
                cell.loadData(type: .open )
            }
        }
    }
    
    @IBAction func showPastTrips(_ sender: Any) {
        if let currentIndexPath = self.contentCollectionView?.indexPathsForVisibleItems.first, currentIndexPath.item == 0 {
            self.contentCollectionView?.scrollToItem(at: IndexPath.init(item: 1, section: 0), at: UICollectionView.ScrollPosition.left, animated: true)
            if let cell = self.contentCollectionView?.cellForItem(at: IndexPath.init(item: 0, section: 0)) as? TripCollectionViewCell {
                cell.rideListItems = []
                cell.paginator = Paginator()
                cell.loadData(type: .close )
            }
        }
    }
    
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var borderViewLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var contentCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        // Do any additional setup after loading the view.
    }
    
    override func localize() {
        self.titleLbl?.text = "Your Trips".localized
        self.openedBtn?.setTitle("Opened".localized, for: .normal)
        self.pastBtn?.setTitle("Past".localized, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: String.LogoutNotification), object: nil, queue: nil) { [weak self] (note) in
                self?.showAlertWithMessage("Are you sure want to logout?".localized, phrase1: "Yes".localized, phrase2: "No".localized, action: UIAlertAction.Style.default, UIAlertAction.Style.cancel, completion: { [weak self] (style) in
                    if style == .default {
                        self?.logout()
                    }
                })
            }
    }
}

extension TripsViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TripCollectionViewCell", for: indexPath) as! TripCollectionViewCell
        cell.tripsTableView.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let tmpCell =  cell as? TripCollectionViewCell {
            tmpCell.activityIndicator = { [weak self](show) -> Void in
                show ? self?.startActivityIndicatorInWindow() : self?.stopActivityIndicator()
            }
            tmpCell.rideListItems = []
            tmpCell.paginator = Paginator()
            tmpCell.loadData(type: indexPath.item == 0 ? .open : .close)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset
        self.borderViewLeadingConstraint.constant = contentOffset.x/2
    }
}

