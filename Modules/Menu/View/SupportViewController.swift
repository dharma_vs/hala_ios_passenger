//
//  SupportViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit

class SupportViewController: BaseViewController {

    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var mainTitleLbl: AppLabel!
    @IBOutlet weak var subTitleLbl: AppLabel!
    @IBOutlet weak var callBtn: AppButton!
    @IBOutlet weak var mailBtn: AppButton!
    @IBOutlet weak var menuBtn: AppButton!
    @IBOutlet weak var supportImg: AppImageView!
    @IBOutlet weak var whatsappBtn: AppButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
    }
    
//    override func setup() {
//        super.setup()
//
//        setTpBackground()
//        callBtn.SetRoundedCorner(radius: callBtn.frame.width / 2)
//        callBtn.SetBorder(width: 1, color: .white)
//        mailBtn.SetRoundedCorner(radius: mailBtn.frame.width / 2)
//        mailBtn.SetBorder(width: 1, color: .white)
//    }
    
    override func localize() {
        super.localize()
        
        titleLbl.text = "Support".localized
        mainTitleLbl.text = "Customer Support".localized
        subTitleLbl.text = "Get in touch with us if you need any support regarding our service".localized
    }
    
    @IBAction func callBtnPressed() {
        call(to: CacheManager.settings?.support_number)
    }
    
    @IBAction func mailBtnPressed() {
        email(to: [(CacheManager.settings?.support_email ?? "")])
    }
    
    @IBAction func whatsappBtnPressed() {
        let originalString = ""
        let urlString = "whatsapp://send?phone=\((CacheManager.settings?.support_number ?? "").replacingOccurrences(of: " ", with: ""))&text=\(originalString)"
        if let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
}
