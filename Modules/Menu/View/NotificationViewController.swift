//
//  NotificationViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit

class NotificationViewController: BaseViewController {
    
    @IBOutlet weak var notifyTableview: UITableView!
    @IBOutlet weak var noNotificationsLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    
    var paginator: Paginator<NotificationInfo>!
    
    var notifications = [NotificationInfo]() {
        didSet {
            self.notifyTableview?.reloadData()
            self.notifyTableview?.isHidden = self.notifications.count <= 0
        }
    }
    
    var hasMore: Bool = true
    
    override func localize() {
        noNotificationsLbl.text = "No Notification".localized
        titleLbl.text = "Notifications".localized
    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        setTpBackground()
        self.paginator = Paginator<NotificationInfo>()
        
        // Do any additional setup after loading the view.
        
        notifyTableview.register(UINib.init(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        notifyTableview.register(UINib.init(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
        
        self.loadData()
    }
    
    func loadData(_ page: Int = 0) {
        
        if let _paginator = paginator, _paginator.currentPage?.intValue() != _paginator.totalPages {
            return
        }
        
        self.startActivityIndicatorInWindow()
        
        ServiceManager().getAllNotifications(page) { [weak self](inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
                let notifications = try inner()
                self?.paginator = notifications
                self?.hasMore = !((notifications?.result?.count ?? 0) < pageSize)
                if let notificationList = self?.paginator?.result {
                    self?.notifications.append(contentsOf: notificationList)
                }
                  self?.noNotificationsLbl.isHidden = true
                if self?.notifications.count == 0 {
                    self?.notifyTableview.isHidden = true
                    self?.noNotificationsLbl.isHidden = false
                }
                
            } catch {
                self?.showAlertWith((error as? CoreError)?.description ?? "")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ShowSlidemenu()
    {
        self.slideMenuController()?.openLeft()
    }
}

extension NotificationViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return 80
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.notifications.count
        return hasMore ? (count + 1) : count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let identifier = (indexPath.row == self.notifications.count)  ? "LoadMoreTableViewCell" : newNotificationTableViewCellIdentifier
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        if let tmpCell = cell as? NewNotificationTableViewCell {
            let notification = self.notifications[indexPath.row]
            tmpCell.headingLbl.text = notification.title
            tmpCell.messageLbl.text = notification.message
            tmpCell.urlLbl.text = notification.web_url
            tmpCell.notificationImage.loadTaxiImage(notification.image?.urlFromString)
            tmpCell.dateTimeLbl.text = notification.created_at?.UTCToLocal1("yyyy-MM-dd HH:mm:ss", formatTo:"dd-MM-yyyy hh:mm a")
            tmpCell.didTapOnUrl = { [weak self] in
                self?.openURL(notification.web_url ?? "")
            }
            
            tmpCell.imageheight.constant = notification.web_url == nil || notification.web_url?.isEmpty == true ? 1 : 100
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let _ =  cell as? LoadMoreTableViewCell {
            self.loadData((self.paginator.currentPage?.intValue() ?? 0) + 1 )
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
    }
}

