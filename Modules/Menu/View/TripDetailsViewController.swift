//
//  TripDetailsViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit
import GoogleMaps


enum DocumentType: String {
    case vehicle_document = "Vehicle Document"
    case insurance_document = "Insurance Document"
    case vehicle_image_document = "Vehicle Image Document"
    case license_document = "License Document"
    case other = "Other"
}

struct ProfileInfo {
    let info: String
    let value: String
    let isPhotoType: Bool
    let type: DocumentType
    var titles: [String]
    var urlStrings: [String]
    init(info: String, value: String, isPhotoType: Bool = false, type: DocumentType = .other, titles: [String] = [], urlStrings: [String] = []) {
        self.info = info
        self.value = value
        self.isPhotoType = isPhotoType
        self.type = type
        self.titles = titles
        self.urlStrings = urlStrings
    }
}

class TripDetailsViewController: BaseViewController {
    
    var viewModel: TripDetailViewModel! {
        didSet {
            self.viewModel.updateTripDetailsViewController(self)
        }
    }
    
 
    @IBOutlet weak var scrollView: UIView!
    @IBOutlet weak var viewMapOuter: UIView!
    
    
    @IBOutlet weak var bookingId: AppLabel!
    @IBOutlet weak var tripTotal: AppLabel!
    @IBOutlet weak var date: AppLabel!
    @IBOutlet weak var sourceAddress: AppLabel!
    @IBOutlet weak var destinationAddress: AppLabel!
    @IBOutlet weak var distance: AppLabel!
    @IBOutlet weak var duration: AppLabel!
    @IBOutlet weak var completedDate: AppLabel!
    @IBOutlet weak var vehicleType: AppLabel!
    @IBOutlet weak var rideStatus: AppLabel!
    @IBOutlet weak var distanceLbl: AppLabel!
    @IBOutlet weak var durationLbl: AppLabel!


    @IBOutlet weak var rideTypeLbl: AppLabel!
    
    @IBOutlet weak var lblPickupname: AppLabel!
    @IBOutlet weak var lblPickupnameans: AppLabel!
    @IBOutlet weak var lblPickupContact: AppLabel!
    @IBOutlet weak var lblPickupContactans: AppLabel!
    @IBOutlet weak var lblDeliveryname: AppLabel!
    @IBOutlet weak var lblDeliverynameans: AppLabel!
    @IBOutlet weak var lblDeliverycontact: AppLabel!
    @IBOutlet weak var lblDeliverycontactans: AppLabel!
    
    @IBOutlet weak var lblItems: AppLabel!
    @IBOutlet weak var lblItemsans: AppLabel!
    @IBOutlet weak var lblTotalweight: AppLabel!
    @IBOutlet weak var lblTotalweightans: AppLabel!
    @IBOutlet weak var lblAdditoninfo: AppLabel!
    @IBOutlet weak var lblAdditoninfoans: AppLabel!
    @IBOutlet weak var lblDriverName: AppLabel!
    @IBOutlet weak var lblDriverNameans: AppLabel!
    @IBOutlet weak var lblVehicle: AppLabel!
    @IBOutlet weak var lblVehicleans: AppLabel!
    @IBOutlet weak var lblVehicleno: AppLabel!
    @IBOutlet weak var lblVehiclenoans: AppLabel!
    @IBOutlet weak var lblDriverRating: AppLabel!
    @IBOutlet weak var lblPickupsignature: AppLabel!
    @IBOutlet weak var lblDeliverysignature: AppLabel!
    @IBOutlet weak var lblBookedfor: AppLabel!
    @IBOutlet weak var lblBookedforans: AppLabel!
    @IBOutlet weak var imgPickupsignature: AppImageView!
    @IBOutlet weak var imgDeliverysignature: AppImageView!
    @IBOutlet weak var btnReceipt: AppButton!

    @IBOutlet weak var driverImage: AppImageView!
    @IBOutlet weak var driverImageBtn: AppButton!
    @IBOutlet weak var cancelBtn: AppButton!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var rating: FloatRatingView!
    @IBOutlet weak var stackDistancedet: UIStackView!
    @IBOutlet weak var stackPickupdet: UIStackView!
    @IBOutlet weak var stackDeliverydet: UIStackView!
    @IBOutlet weak var stackItemsdet: UIStackView!
    @IBOutlet weak var stackDruverdet: UIStackView!
    @IBOutlet weak var stackSignaturedet: UIStackView!
    @IBOutlet weak var stackReciptdet: UIStackView!
    @IBOutlet weak var stackCanceledet: UIStackView!
    @IBOutlet weak var stackBookedfordet: UIStackView!


    @IBAction func zoomInUserImage(_ sender: Any) {
        let imageZoomVC = ImageZoomViewController.initFromStoryBoard(.Menu)
        imageZoomVC.url = self.viewModel.userImageUrl
        self.present(imageZoomVC, animated: false, completion: nil)
    }
    
    @IBAction func showStartReading(_ sender: Any) {
        let profileInfo = ProfileInfo(info: "Start Reading".localized, value: "1 Photo".localized, isPhotoType: true, type: .vehicle_document, titles: ["Start Reading".localized], urlStrings: [/*"documents/"+(*/self.viewModel.startReadingImage/*.components(separatedBy: "/").last*/ ?? ""])
        
        let imageVC = ProfileImagesViewController.initFromStoryBoard(.Main)
        imageVC.profileDocumentInfo = profileInfo
        imageVC.modalPresentationStyle = .overCurrentContext
        self.present(imageVC, animated: true, completion: nil)
    }
    
    @IBAction func showEndReading(_ sender: Any) {
        let profileInfo = ProfileInfo(info: "End Reading".localized, value: "1 Photo".localized, isPhotoType: true, type: .vehicle_document, titles: ["End Reading".localized], urlStrings: [/*"documents/"+(*/self.viewModel.endReadingImage/*.components(separatedBy: "/").last*/ ?? ""])
        
        let imageVC = ProfileImagesViewController.initFromStoryBoard(.Main)
        imageVC.profileDocumentInfo = profileInfo
        imageVC.modalPresentationStyle = .overCurrentContext
        self.present(imageVC, animated: true, completion: nil)
    }
    
    @IBAction func cancelTrip( sender: Any) {
        
        let cancelVC = CancelReasonViewController.initFromStoryBoard(.Main)
        cancelVC.rideDetail = self.viewModel.rideDetail
        cancelVC.didRequestToCancelRide = { [weak self] (rideDetail, reason, sourceLocation) in
           // self.reloadRideDetail()
//                    Router.setDashboardViewControllerAsRoot()
            
            self?.showAlertWithMessage("cancelrequest".localized, phrase1: "Yes".localized, phrase2: "No".localized, action: UIAlertAction.Style.default, .cancel) { [weak self](style) in
                self?.dismiss(animated: false, completion: nil)
                if style == .default {
                    self?.cancelRide(rideDetail: rideDetail, reason: reason, sourceLocation: sourceLocation)
                }
            }
        }
        cancelVC.modalPresentationStyle = .overCurrentContext
        self.present(cancelVC, animated: true, completion: nil)
        
        
    }
    
    @IBAction func viewReceipt( sender: Any) {
        let receiptVC = ReceiptViewController.initFromStoryBoard(.Menu)
        self.navigationController?.pushViewController(receiptVC, animated: true)
    }
    
    var mapViewHelper: GoogleMapsHelper?
    
    lazy var sourceLocationMarker : GMSMarker = {
        let marker = GMSMarker()
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: 30, height: 30)))
        imageView.contentMode =  .scaleAspectFit
        imageView.loadTaxiImage(CacheManager.settings?.source_marker?.urlFromString)
        marker.iconView = imageView
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.map = self.mapViewHelper?.mapView
        marker.tracksViewChanges = false
        return marker
    }()
    
    var setMapImage: (()->Void)?
    
    lazy var destinationMarkerImage: UIImageView = {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: 40, height: 40)))
        imageView.contentMode =  .scaleAspectFit
        imageView.loadTaxiImage(CacheManager.settings?.destination_marker?.urlFromString)
        return imageView
    }()
    
    lazy var destinationLocationMarker : GMSMarker = {
        let marker = GMSMarker()
        marker.iconView = self.destinationMarkerImage
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.map = self.mapViewHelper?.mapView
        marker.tracksViewChanges = false
        return marker
    }()
    
    @IBOutlet weak var mapImage: UIImageView!
    
    fileprivate func addMapView(){
        if self.mapViewHelper == nil {
            self.mapViewHelper = GoogleMapsHelper()
            self.mapViewHelper?.getMapView(withDelegate: self, in: self.viewMapOuter, isNeedToStretch: false)
            sourceLocationMarker.map = self.mapViewHelper?.mapView
            destinationLocationMarker.map = self.mapViewHelper?.mapView
            self.mapViewHelper?.mapView?.isMyLocationEnabled = false
            self.mapViewHelper?.mapView?.isUserInteractionEnabled = false
        }
    }
    
   /* func drawPolyline(_ rideDetail: RideDetail) {
        let sourceCoordinate = self.viewModel.sourceCoordinate
        let destCoordinate = self.viewModel.destinationCoordinate
        self.mapViewHelper?.mapView?.camera = GMSCameraPosition.camera(withTarget: sourceCoordinate, zoom: 15)
        self.mapViewHelper?.moveTo(location: sourceCoordinate, with: self.view.center)
        DispatchQueue.main.async { [weak self] in
            self?.mapViewHelper?.mapView?.clear()
            self?.sourceLocationMarker.map = self?.mapViewHelper?.mapView
            self?.destinationLocationMarker.map = self?.mapViewHelper?.mapView
            self?.sourceLocationMarker.position = sourceCoordinate
            self?.destinationLocationMarker.position = destCoordinate
            let position = GMSCameraPosition.init(target: sourceCoordinate, zoom: 15.0)
            self?.mapViewHelper?.mapView?.animate(to: position)
            self?.focusMapToCoordinates(sourceCoordinate, dest: destCoordinate)
        }
        self.mapViewHelper?.mapView?.drawPolygon(from: sourceCoordinate, to: destCoordinate) { [weak self] (inner) -> (Void) in
            let polyLine = inner()
            DispatchQueue.main.async { [weak self] in
                polyLine.map = self?.mapViewHelper?.mapView
                polyLine.strokeWidth = 1
                polyLine.strokeColor = UIColor.colorAccent
                self?.focusMapToCoordinates(sourceCoordinate, dest: destCoordinate)
            }
        }
    }*/
    
    var rideId: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        stackCanceledet.isHidden =  true
//        self.viewReceiptBtn.layer.borderColor = UIColor.init(hex: 0x880900).cgColor
//        self.viewReceiptBtn.layer.borderWidth = 2.0
//        self.addMapView()
        self.rating.isUserInteractionEnabled = false
        self.mapViewHelper?.mapView?.isHidden = true
//        self.rating.tintColor = TpColor().ratingColor
        self.reloadRideDetail()
       // self.scrollView.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    func reloadRideDetail() {
        self.startActivityIndicatorInWindow()
        
        ServiceManager().fetchRideDetails(self.rideId, reason: "") { [weak self] (inner) -> (Void) in
            do {
                if let rideDetail = try inner() {
                    DispatchQueue.main.async { [weak self] in
                        if rideDetail.rides?.first != nil {
                            self?.viewModel = TripDetailViewModel((rideDetail.rides?.first)!)
                            
                        }
                        self?.scrollView.isHidden = false
                    }
                }
            } catch {
                print(error)
            }
            self?.stopActivityIndicator()
        }
        
   }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
    }
    
    func focusMapToCoordinates(_ source: CLLocationCoordinate2D, dest: CLLocationCoordinate2D) {
        let bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: source, coordinate: dest)
        self.mapViewHelper?.mapView?.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 25))
        self.setMapImage?()
    }
    
    override func localize() {
        self.title = "Trip Details".localized
        self.durationLbl.text = "Duration".localized
        self.distanceLbl.text = "Distance".localized
        self.lblBookedfor.text = "Booked For".localized
        self.btnReceipt?.setTitle("View Receipt".localized, for: UIControl.State())
        //self.waitingTimeLbl.text = "Waiting Time".localized
        self.bookingId.font = UIFont.myBoldSystemFont(ofSize: 14)
        self.rideTypeLbl.font = UIFont.myBoldSystemFont(ofSize: 10)
        self.tripTotal.font = UIFont.myBoldSystemFont(ofSize: 20)
        self.date.font = UIFont.myBoldSystemFont(ofSize: 12)

    }
    @IBAction func btnClkReceipt() {
    }

}

extension TripDetailsViewController: GMSMapViewDelegate {
    
}
