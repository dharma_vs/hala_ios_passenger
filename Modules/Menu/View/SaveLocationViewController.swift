//
//  SaveLocationViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 05/03/22.
//

import UIKit

class SaveLocationViewController: BaseViewController {
    
    @IBOutlet weak var name: TextFieldWithTitleView!
    
    
    @IBOutlet weak var saveBtn: AppButton!
    @IBOutlet weak var cancelBtn: AppButton!
    
    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var lblLocation: AppLabel!
    @IBOutlet weak var lblLocationans: AppLabel!
    
    var locationInfo: FavoriteLocation?
    
    var didRequestToSaveLocation: ((FavoriteLocation?)->Void)?
    
    @IBAction func saveLocation(_ sender: UIButton) {
        guard name.textField.text?.isEmpty == false else {
            self.view.makeToast("Enter name".localized)
            return
        }
        
        guard lblLocationans.text?.isEmpty == false else {
            self.view.makeToast("Enter location address".localized)
            return
        }
        locationInfo?.locationName = name.textField.text
        locationInfo?.locationAddress = lblLocationans.text
        didRequestToSaveLocation?(self.locationInfo)
        self.dismiss(animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.name.textField?.text = locationInfo?.locationName
        self.lblLocationans.text = locationInfo?.locationAddress

        // Do any additional setup after loading the view.
    }
    
    override func localize() {
        self.name.titleLbl.textColor = UIColor.init(hex: 0x9A2315)
       // self.location.titleLbl.textColor = UIColor.init(hex: 0x9A2315)
        self.name.titleLbl.text = "Name".localized
        self.lblLocation.text = "Location".localized
        self.name.textField.textColor = .black
        self.name.textFieldPlaceholder = "Name".localized
        self.saveBtn?.setTitle("Save".localized, for: .normal)
        self.cancelBtn?.setTitle("Cancel".localized, for: .normal)
        self.titleLbl?.text = "Save Location".localized
    }

}
