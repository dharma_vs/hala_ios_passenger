//
//  NewSettingsViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 07/04/22.
//

import UIKit

class NewSettingsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

enum SettingsCellType: Int, CaseIterable {
    case Profile = 0, Header, Info
}

enum SettingsType: Int, CaseIterable {
    case AddHome = 0, AddWork, SavedPlaces, Language, Currency, Speech, SavedCards, Documents
}

extension Int {
    var settingsCellType: SettingsCellType {
        return SettingsCellType(rawValue: self) ?? .Profile
    }
    
    var settingsType: SettingsType {
        return SettingsType(rawValue: self) ?? .AddHome
    }
}

extension NewSettingsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section.settingsCellType {
        case .Profile:
            let cell: ProfileTableViewCell = tableView.dequeueCell()
            return cell
        default:
            let settingsType = indexPath.row.settingsType
            switch settingsType {
            case .AddHome:
                let cell: InfoTableViewCell = tableView.dequeueCell()
                return cell
            case .AddWork:
                let cell: InfoTableViewCell = tableView.dequeueCell()
                return cell
            case .SavedPlaces:
                let cell: HeaderTableViewCell = tableView.dequeueCell()
                return cell
            case .Language:
                let cell: InfoTableViewCell = tableView.dequeueCell()
                return cell
            case .Currency:
                let cell: InfoTableViewCell = tableView.dequeueCell()
                return cell
            case .Speech:
                let cell: InfoTableViewCell = tableView.dequeueCell()
                return cell
            case .SavedCards:
                let cell: InfoTableViewCell = tableView.dequeueCell()
                return cell
            case .Documents:
                let cell: InfoTableViewCell = tableView.dequeueCell()
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let cell: HeaderTableViewCell = tableView.dequeueCell()
            return cell
        }
        return nil
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : SettingsType.allCases.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}
