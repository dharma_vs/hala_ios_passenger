//
//  ImageViewWithTitleView.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import AVFoundation
import UIKit
import MobileCoreServices
import CropViewController

@objc protocol ImageViewWithTitleViewDatasource {
    func presender() -> UINavigationController
}

//@IBDesignable
class ImageViewWithTitleView: BaseView {
    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var imageView: AppImageView!
    @IBOutlet weak var chooseMediaBtn: AppButton!
    
    @IBOutlet weak var datasource: ImageViewWithTitleViewDatasource!
    
    @IBInspectable var title: String? {
        get {
            return titleLbl.text ?? ""
        }
        set {
            titleLbl.text = newValue?.localized
        }
    }
    
    @IBInspectable
    var titleColor: UIColor? {
        get {
            return titleLbl.textColor
        }
        set {
            if let color = newValue {
                titleLbl.textColor = color
            } else {
                titleLbl.textColor = .black
            }
        }
    }
    
    @IBInspectable
    var chooseMediaBtnImage: UIImage? {
        get {
            return chooseMediaBtn.image(for: .normal)
        }
        set {
            if let image = newValue {
                chooseMediaBtn.setImage(image, for: .normal)
            }
        }
    }
    
    @IBInspectable
    var imageViewImage: UIImage? {
        get {
            return imageView.image
        }
        set {
            if let image = newValue {
                imageView.image = image
            }
        }
    }
    
    @IBAction func chooseMediaBtnPressed() {
        let action = UIAlertController(title: "Pick Image From?".localized, message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Camera".localized, style: .default) { [weak self] (action) in
            self?.showCamera()
        }
        let gallery = UIAlertAction(title: "Gallery".localized, style: .default) { [weak self] (action) in
            self?.showPhotoLibrary()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        action.addAction(camera)
        action.addAction(gallery)
        action.addAction(cancel)
        datasource?.presender().present(action, animated: true, completion: nil)
    }
    
    func showCamera() {
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) == AVAuthorizationStatus.denied {
            UIViewController.showAlertForCameraAccessDenied(inViewController: datasource!.presender())
        } else {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { [weak self] (granted: Bool) -> Void in
                if granted == true {
                    let imagePicker = UIImagePickerController()
                    imagePicker.sourceType = UIImagePickerController.SourceType.camera
                    imagePicker.allowsEditing = false
                    imagePicker.delegate = self
                    self?.datasource?.presender().present(imagePicker, animated: true, completion: nil)
                } else {
                    // User Rejected
                }
            })
        }
    }
    
    
    @IBInspectable var titleThemeColor: Int = -1 {
        didSet {
            setup1()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup1()
    }
    
    func setup1() {
        if self.titleThemeColor != -1 {
            self.titleColor = ColorTheme.init(rawValue: self.titleThemeColor)?.color ?? ColorTheme.colorTextDefault.color
        } else {
            self.titleColor = ColorTheme.colorTextDefault.color
        }
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup1()
    }
    
    func showPhotoLibrary() {
        let imagePicker = UIImagePickerController()
        imagePicker.navigationController?.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        datasource?.presender().present(imagePicker, animated: true, completion: nil)
    }
}

extension ImageViewWithTitleView: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let mediaType = info[UIImagePickerController.InfoKey.mediaType] as! CFString
        switch mediaType {
        case kUTTypeImage:
            datasource?.presender().dismiss(animated: false) {
                if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                    let cropController = CropViewController(image: pickedImage)
                    cropController.aspectRatioPreset = .presetOriginal
                    cropController.delegate = self
                    self.datasource?.presender().pushViewController(cropController, animated: true)
                }
            }
        default:
            break
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        datasource?.presender().dismiss(animated: true, completion: nil)
    }
}

extension ImageViewWithTitleView: CropViewControllerDelegate {
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        datasource?.presender().popViewController(animated: true)
        self.imageView.image = image
    }
}
