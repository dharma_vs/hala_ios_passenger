//
//  SpendHistoryViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit
import Charts

class SpendHistoryViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,ChartViewDelegate {
   
    @IBOutlet weak var chartView: BarChartView!
    @IBOutlet weak var earningTableView: UITableView!
    @IBOutlet weak var detaildBtn: AppButton!
    @IBOutlet weak var overAllBtn: AppButton!
    @IBOutlet weak var detialsLbl: AppLabel!
    @IBOutlet weak var overAllLbl: AppLabel!
    @IBOutlet weak var titlelbl: AppLabel!
    @IBOutlet weak var descLbl: AppLabel!
    @IBOutlet weak var earningAMtLbl: AppLabel!
    let xaxisValue: [String] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sept", "Oct", "Nov", "Dec"]

    var earngListDict = NSDictionary()
    var detailsListDict = NSDictionary()
    var earningChartRes: EarningChartRes?

    var isearningBool = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isearningBool = true
        self.earningTableView.isHidden = true
        detaildBtn.setTitle("Details".localized, for: .normal)
        overAllBtn.setTitle("OverAll".localized, for: .normal)
        titlelbl.text = "Spend History".localized
        descLbl.text = "Total Spend".localized
        detialsLbl.isHidden = true
        self.earningTableView.tableFooterView = UIView()
        
        // X - Axis Setup
                let xaxis = chartView.xAxis
                let formatter = CustomLabelsXAxisValueFormatter()//custom value formatter
                formatter.labels = self.xaxisValue
                
                xaxis.valueFormatter = formatter
                xaxis.drawGridLinesEnabled = false
                xaxis.labelPosition = .bottom
        xaxis.labelTextColor = UIColor.colorPrimary
                xaxis.centerAxisLabelsEnabled = true
                xaxis.axisLineColor = UIColor.lightGray
                xaxis.granularityEnabled = true
                xaxis.enabled = true
                
                chartView.delegate = self
        chartView.noDataText = "No record found"
        chartView.noDataTextColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        chartView.chartDescription?.textColor = UIColor.lightGray
        
        spendEarningApiCall(url: ":4040/rider/profile/spend-report")

    }
    func spendEarningApiCall(url:String) {
               self.startActivityIndicator()
               let urlString = componentScheme + "://" + componentHost + url
               
               let url = URL(string: urlString)!
               let session = URLSession.shared
               let request = NSMutableURLRequest(url: url as URL)
               
               request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
               let authKey = "Bearer " + (CacheManager.sharedInstance.getObjectForKey(.AuthToken) as! String)
               
               request.addValue(authKey , forHTTPHeaderField: "Authorization")
        request.addValue(UserDefaults.dbString ?? "", forHTTPHeaderField: "dbstring")
        
               request.httpMethod = "GET"
               
               
               let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                   DispatchQueue.main.async {
                       
                       guard error == nil else {
                           self.stopActivityIndicator()
                           return
                       }
                       guard let data = data else {
                           self.stopActivityIndicator()
                           return
                       }
                       do {
                           if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                               
                            if self.isearningBool {
                                self.earngListDict = json as NSDictionary
                                self.earningChartRes = data.getDecodedObject(from: EarningChartRes.self)
                                self.reloadEarningList()
                                self.isearningBool = false
                                self.spendEarningApiCall(url: ":4040/rider/profile/spend-history?pageSize=10&currentPage=0")

                            }else{
                                self.detailsListDict = json as NSDictionary
                                self.earningTableView.reloadData()
                                self.stopActivityIndicator()

                            }
                              
                           }
                       } catch let error {
                           self.stopActivityIndicator()
                           print(error.localizedDescription)
                       }
                   }
                   
               })
               task.resume()
       }
    @IBAction func overallBtnAction(_ sender: Any) {
        self.earningTableView.isHidden = true
        self.chartView.isHidden = false
        detialsLbl.isHidden = true
        overAllLbl.isHidden = false
    }
    @IBAction func detailsBtnAction(_ sender: Any) {
        self.earningTableView.isHidden = false
        self.chartView.isHidden = true
        detialsLbl.isHidden = false
        overAllLbl.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.detailsListDict.count) > 0 ? (((self.detailsListDict.object(forKey: "data") as? NSDictionary)?.object(forKey: "result") as? NSArray) ?? []).count : 0
       }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return  60
       }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
               let cell = tableView.dequeueReusableCell(withIdentifier: "EarningHistoryTableViewCell", for: indexPath) as! EarningHistoryTableViewCell
        let earning = (((self.detailsListDict.object(forKey: "data")as! NSDictionary).object(forKey: "result")as! NSArray).object(at: indexPath.row)as! NSDictionary)
        
        cell.numberLbl.text = (earning.object(forKey: "booking_id") as! String)
               cell.dateLbl.text =  (earning.object(forKey: "payment_at") as! String).UTCToLocal("yyyy-MM-dd HH:mm:ss", formatTo:"dd-MM-yyyy-MM hh:mm a")
        cell.amountLbl.text = Extension.updateCurrecyRate(fare: "\((earning.object(forKey: "amount")as! NSNumber))").currencyValue
                       
               return cell
         
       }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let earning = (((self.detailsListDict.object(forKey: "data")as! NSDictionary).object(forKey: "result")as! NSArray).object(at: indexPath.row)as! NSDictionary)
        
        let riderID = earning["ride_id"] ?? ""
        
        if "\(riderID)" != "0" {
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            
            if let vc = storyBoard.instantiateViewController(withIdentifier: "TripDetailsViewController") as? TripDetailsViewController
            {
                vc.rideId = "\(riderID)"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    func reloadEarningList() {
        if ((self.earngListDict.object(forKey: "data") as? NSArray)?.count ?? 0) > 0{
        earningAMtLbl.text = Extension.updateCurrecyRate(fare: "\(((((self.earngListDict.object(forKey: "data")as! NSArray).object(at: 0))as! NSDictionary).object(forKey: "spend_money")!))").currencyValue
            self.setDataCount(xaxisValue.count, range:self.earningChartRes?.yearlyData.map{ Extension.updateCurrecyRate(fare: $0.spend_money?.stringValue ?? "0").doubleValue ?? 0} ?? [] ,values:self.earningChartRes?.yearlyData.map{$0.monthString } ?? [])
        }
        
    }
    func setDataCount(_ count: Int, range: [Double],values:[String]) {
        let start = 1
        
        let yVals = (start..<start+count).map { (i) -> BarChartDataEntry in
            return BarChartDataEntry(x: Double(i), y: range[i - 1] )
        }
        var set1: BarChartDataSet! = nil
        if let set = chartView.data?.dataSets.first as? BarChartDataSet {
            set1 = set
            set1.replaceEntries(yVals)
            chartView.data?.notifyDataChanged()
            chartView.notifyDataSetChanged()
        } else {
            set1 = BarChartDataSet(entries: yVals, label: "")
            set1.colors = [UIColor.gStartColor]
            set1.drawValuesEnabled = true
            set1.highlightEnabled = false
            set1.accessibilityElementsHidden = true
            
            chartView.rightAxis.drawGridLinesEnabled = false // disable horizontal grid lines
            
            chartView.leftAxis.axisMinimum = 0.0
            chartView.xAxis.granularityEnabled = true
            chartView.xAxis.axisLineColor = .black
            chartView.rightAxis.axisLineColor = .clear
            
            chartView.xAxis.granularity = 10.0
            chartView.xAxis.decimals = 0
            chartView.drawGridBackgroundEnabled = false
            chartView.gridBackgroundColor = UIColor.clear
            
            chartView.drawValueAboveBarEnabled = true
            
            chartView.xAxis.drawGridLinesEnabled = false
            chartView.rightAxis.drawGridLinesEnabled = false
            chartView.rightAxis.drawLabelsEnabled = false
            chartView.xAxis.centerAxisLabelsEnabled = true

            
            chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: values)
            chartView.xAxis.granularity = 1.0
            chartView.xAxis.granularityEnabled = true
            chartView.xAxis.labelPosition = .bottom
            
            let data = BarChartData(dataSet: set1)
            data.setValueFont(UIFont(name: "HelveticaNeue-Light", size: 10)!)
            data.barWidth = 0.5
//
//            let groupSpace = 0.4
//            let barSpace = 0.025
//            let barWidth = 0.2
//
//            data.barWidth = barWidth
////
//            chartView.xAxis.axisMinimum = 0.0
//            chartView.xAxis.axisMaximum = 0.0 + data.groupWidth(groupSpace: groupSpace, barSpace: barSpace) * Double(self.xaxisValue.count)
////
//            chartView.groupBars(fromX: 0.0, groupSpace: groupSpace, barSpace: barSpace)
//
//            chartView.xAxis.granularity = chartView.xAxis.axisMaximum / Double(self.xaxisValue.count)
//            chartView.data = data
//            chartView.notifyDataSetChanged()
//            chartView.setVisibleXRangeMaximum(4)
//            chartView.animate(yAxisDuration: 1.0, easingOption: .linear)
//            if Date().currentMonth() > 5  {
//                chartView.moveViewToX(chartView.xAxis.axisMaximum)
//            }
            
            chartView.data = data
            
        }
    }

}
class EarningHistoryTableViewCell: UITableViewCell {
    @IBOutlet weak var dateLbl: AppLabel!
    @IBOutlet weak var numberLbl: AppLabel!
    @IBOutlet weak var amountLbl: AppLabel!

}

import Foundation
import UIKit
//import Charts

class CustomLabelsXAxisValueFormatter : NSObject, IAxisValueFormatter {
    
    var labels: [String] = []
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
        let count = self.labels.count
        
        guard let axis = axis, count > 0 else {
            
            return ""
        }
        
        let factor = axis.axisMaximum / Double(count)
        
        let index = Int((value / factor).rounded())
        
        if index >= 0 && index < count {
            
            return self.labels[index]
        }
        
        return ""
    }
}

struct EarningChartData: JSONSerializable {
    var month, spend_money, year: CustomType?
    
    var monthString: String {
        let xaxisValue: [String] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sept", "Oct", "Nov", "Dec"]
        return xaxisValue[(self.month?.intValue ?? 1)-1]
    }
}

struct EarningChartRes: JSONSerializable {
    var data: [EarningChartData]?
    var message, status: CustomType?
}

extension EarningChartRes {
    var yearlyData: [EarningChartData] {
        
        let xaxisValue: [String] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sept", "Oct", "Nov", "Dec"]
        var data = [EarningChartData]()
        for (idx, _) in xaxisValue.enumerated() {
            let filteredData = self.data?.filter{$0.month?.intValue == idx+1}.first ?? EarningChartData.emptyData(idx+1, year: data.first?.year?.intValue ?? (Date.currentYear(Date()))())
            data.append(filteredData)
        }
        return data
    }
}

extension EarningChartData {
    static func emptyData(_ month: Int, year: Int = (Date.currentYear(Date()))()) -> EarningChartData {
        return EarningChartData(month: CustomType(value: month as AnyObject), spend_money: CustomType(value: 0 as AnyObject), year: CustomType(value: year as AnyObject))
    }
}

extension String {
    var doubleValue: Double? {
        return Double(self)
    }
}
