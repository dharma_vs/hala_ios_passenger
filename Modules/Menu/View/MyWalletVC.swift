//
//  MyWalletVC.swift
//  TaxiPickup
//
//  Created by TAMILARASAN on 25/01/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

class MyWalletVC: BaseViewController {

    @IBOutlet weak var lbl_Available: AppLabel!
    @IBOutlet weak var lbl_Amount: AppLabel!
    @IBOutlet weak var lbl_Currency: AppLabel!
    @IBOutlet weak var tbl_Mywallet: UITableView!
//
//    var paginator: Paginator<WalletHistoryInfo>!
//    var walletItems = [WalletHistoryInfo]() {
//        didSet {
//            tbl_Mywallet.reloadData()
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
//        lbl_Amount.text =   Extension.updateCurrecyRate(fare: "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)").currencyValue
        
            
         //   "\(CacheManager.userInfo?.walletAmount?.doubleValue ?? 0)".currencyValue
        
//        lbl_Currency.text = CacheManager.currencyShortName
        
//        self.loadData()

        // Do any additional setup after loading the view.
    }
    
  /*  func loadData(_ page: Int = 0) {
        if let _paginator = paginator, _paginator.currentPage?.intValue() != _paginator.totalPages {
            return
        }
        self.startActivityIndicatorInWindow()
        ServiceManager().walletHistory(page) { (inner) -> (Void) in
            self.stopActivityIndicator()
            do {
                self.paginator = try inner()
                if let walletItem = self.paginator?.result {
                    self.walletItems.append(contentsOf: walletItem)
                }
                
                if self.walletItems.count == 0 {
                    self.tbl_Mywallet.isHidden = true
//                    self.noResultLbl.isHidden = false
                }
            } catch(_) {
                
            }
        }
    }
    
    @IBAction func btnclk_Menu()
    {
        self.slideMenuController()?.openLeft()
    }
    @IBAction func btnclk_Addmoney() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMoneyVC") as! AddMoneyVC
        self.navigationController?.pushViewController(vc, animated: true)
        //self.slideMenuController()?.changeMainViewController(secondViewController, close: true)
    }
    @IBAction func btnclk_Coupon() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyCouponsVC") as! MyCouponsVC
        self.navigationController?.pushViewController(vc, animated: true)
        //self.slideMenuController()?.changeMainViewController(vc, close: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
*/
}
//extension MyWalletVC : UITableViewDelegate, UITableViewDataSource {
    
   /* func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return walletItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MywalletCell", for: indexPath) as! MywalletCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        let walletItem = walletItems[indexPath.row]
//        cell.lbl_Message = walletItem.payment_mode
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  95
    }
    */
    
//}

extension MyWalletVC: UIScrollViewDelegate {
    
    //  MARK: - UIScrollViewDelegate
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView == tbl_Mywallet {
//            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
//                loadData(paginator.currentPage!.intValue() + 1)
//            }
//        }
//    }
}

