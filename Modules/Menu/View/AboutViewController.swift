//
//  AboutViewController.swift
//  TaxiPickup
//
//  Created by Prabakaran on 21/01/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

class AboutViewController: BaseViewController {
    
    func updateUI(_ aboutInfo: AboutUsInfo?) {
        self.aboutLbl.attributedText = aboutInfo?.content?.htmlToAttributedString
        self.aboutLbl.textColor = .black
        self.view.bringSubviewToFront(self.aboutLbl)
    }
    
    @IBOutlet weak var aboutLbl: AppLabel!
    @IBOutlet weak var menuBtn: AppButton!
    @IBOutlet weak var titlelbl: AppLabel!
    @IBOutlet weak var logoImg: AppImageView!


    
    var viewModel: AboutViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        titlelbl?.text = "About".localized
        setTpBackground()
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func ShowSlidemenu()
    {
        self.slideMenuController()?.openLeft()
    }

}
