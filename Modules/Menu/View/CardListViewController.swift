//
//  CardListViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit
import Toast_Swift

class CardListViewController: BaseViewController {
    
    @IBOutlet weak var emptyLbl: AppLabel!
    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var titleLbl1: AppLabel!
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView?.dataSource = self
            self.tableView?.delegate = self
        }
    }
    
    @IBAction func addCard() {
        let paymentVC = AddDebitCardViewController.initFromStoryBoard(.Payment)
        paymentVC.delegate = self
        self.navigationController?.pushViewController(paymentVC, animated: true)
    }
    
    var userInfo: UserInfo? {
        didSet {
            tableView?.reloadData()
            self.emptyLbl?.isHidden = (userInfo?.card_info?.count ?? 0) > 0
        }
    }
    
    fileprivate func getCardList() {
        ServiceManager().getProfile { [weak self] (inner) -> (Void) in
            do {
                let userInfo = try inner()
                self?.userInfo = userInfo
            } catch {}
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView?.register(UINib.init(nibName: "CardListCell", bundle: nil), forCellReuseIdentifier: "CardListCell")
        
        getCardList()
        
        self.titleLbl?.text = "Saved Cards".localized
        self.titleLbl1?.text = "Saved Cards".localized
        self.emptyLbl?.text = "No Cards".localized
        
        // Do any additional setup after loading the view.
    }
  
}

extension CardListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.userInfo?.card_info ?? []).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CardListCell! = tableView.dequeueReusableCell(withIdentifier: "CardListCell", for: indexPath) as? CardListCell
        if (self.userInfo?.card_info ?? []).count > indexPath.row {
            let card = (self.userInfo?.card_info ?? [])[indexPath.row]
            cell?.setValues((self.userInfo?.card_info ?? [])[indexPath.row])
            cell?.didRequestToRemoveCard = { [weak self] in
                self?.showAlertWithMessage("Are you sure want to delete this card?", phrase1: "Yes", phrase2: "No", action: .default, .cancel, completion: { [weak self] (style) in
                    if style == .default {
                        ServiceManager().deleteSavedCard(card.id ?? 0) { [weak self] (inner) -> (Void) in
                            do {
                                let res = try inner()
                                self?.view.makeToast(res?.message?.stringValue)
                                self?.getCardList()
                            } catch { }
                        }
                    }
                })
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension CardListViewController: AddDebitCardDelegate {
    func newCardAdded(info: Card_info) {
        CacheManager.userInfo?.card_info?.append(info)
        self.getCardList()
    }
}
