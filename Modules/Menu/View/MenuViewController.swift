//
//  MenuViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit
import StoreKit

enum MenuItem: String {
    
    case Home = "Home", Notifications = "Notifications", YourTrips = "Your Trips", BookRide = "Book Ride", MyWallet = "My Wallet", RentARide = "Ride A Rent", SpendHistory = "Spend History", Feedback = "Feedback", About = "About", Support = "Support", InviteFriends = "Invite friends", Settings = "Settings", Logout = "Logout", RateUs = "Rate Us", ExitDemo = "Exit Demo", Terms = "Terms & Conditions", YourService = "Your Service"
    
    static var all: [MenuItem] {
        return UserDefaults.companyLogin ? [.YourTrips, .Feedback, .About, .Terms, .RateUs,  .Support, .Settings, .Logout] : [MenuItem.Home, .YourService, .SpendHistory, .Notifications, .Feedback, .About, .Terms, .InviteFriends, .RateUs, .Support, .Settings, .Logout]/*[MenuItem.BookRide, .RentARide, .YourTrips, .MyWallet, .SpendHistory, .Notifications, .Feedback, .About, .Terms, .InviteFriends, .RateUs,  .Support, .Settings, .Logout]*/
    }
    
    var imageIcon: UIImage? {
        switch self{
        case .Home:
            return UIImage(named: "img_menu_booking")?.withRenderingMode(.alwaysTemplate)
        case .YourService:
            return UIImage(named: "img_bike_service")?.withRenderingMode(.alwaysTemplate)
        case .SpendHistory:
            return UIImage(named: "img_menu_expense")?.withRenderingMode(.alwaysTemplate)
        case .Notifications:
            return UIImage(named: "img_dash_notification")?.withRenderingMode(.alwaysTemplate)
        case .Feedback:
            return UIImage(named: "img_menu_feedback")?.withRenderingMode(.alwaysTemplate)
        case .About:
            return UIImage(named: "img_menu_about")?.withRenderingMode(.alwaysTemplate)
        case .Terms:
            return UIImage(named: "img_menu_terms")?.withRenderingMode(.alwaysTemplate)
        case .InviteFriends:
            return UIImage(named: "img_menu_invite")?.withRenderingMode(.alwaysTemplate)
        case .RateUs:
            return UIImage(named: "img_menu_rate_us")?.withRenderingMode(.alwaysTemplate)
        case .Support:
            return UIImage(named: "img_menu_support")?.withRenderingMode(.alwaysTemplate)
        case .Settings:
            return UIImage(named: "img_menu_setting")?.withRenderingMode(.alwaysTemplate)
        case .Logout:
            return UIImage(named: "img_menu_logout")?.withRenderingMode(.alwaysTemplate)
        default:
            return UIImage(named: "")?.withRenderingMode(.alwaysTemplate)
        }
    }
}

class MenuViewController: BaseViewController {
    
    var iconList: [String] = UserDefaults.companyLogin ? [ "Menu2", "Menu5", "Menu6", "Menu12", "Bonuswallet", "Menu11", "Menu8", "Menu9"] : ["img_menu_booking", "img_bike_service", "img_menu_expense",  "img_dash_notification","img_menu_feedback", "img_menu_about", "img_menu_terms", "img_menu_invite", "img_menu_rate_us", "img_menu_support", "img_menu_setting", "img_menu_logout", "img_menu_invite", "img_menu_rate_us"]
    
    //var iconList: [String] = ["Menu3", "Menu1", "Menu2", "Menu10", "Menu4", "Menu5", "Menu6", "Menu12", "Menu7", "Bonuswallet", "Menu11", "Menu8", "Menu9", "icon_exitdemo"]

    
    @IBOutlet weak var accessKeyLbl: AppLabel!
    
    @IBOutlet weak var imageView: AppImageView!
    @IBOutlet weak var nameLbl: AppLabel!
    @IBOutlet weak var phoneLbl: AppLabel!
    @IBOutlet weak var emailLbl: AppLabel!
    @IBOutlet weak var versionLbl: AppLabel!
    @IBOutlet weak var accessKeyOrEnvLbl: AppLabel!
    @IBOutlet weak var madeWithLoveLbl: AppLabel!

    
    @IBOutlet weak var menuTableView: UITableView!
    
    var viewModel: MenuViewModel! {
        didSet {
            self.viewModel.updateMenuViewController(self)
        }
    }
    
    @IBAction func openProfile(_ sender: Any) {
        self.slideMenuController()?.closeLeft()
        let profileVC = ProfileViewController.initFromStoryBoard(.Profile)
        profileVC.didProfileUpdated = { [weak self] in
            if let riderInfo = CacheManager.riderInfo {
                self?.viewModel = MenuViewModel.init(riderInfo)
            }
        }
        self.pushViewController(profileVC, type: ProfileViewController.self, animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
//        self.accessKeyLbl.text = "Access Key: " + (UserDefaults.accessKey?.uppercased() ?? "")
        
        //TableView setup
        menuTableView.register(UINib.init(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        //menuTableView.separatorStyle = .none
        menuTableView.tableHeaderView = UIView()
        menuTableView.tableFooterView = UIView()
        
        refreshViewModel()
    }
    
    func refreshViewModel() {
        if let riderInfo = CacheManager.riderInfo {
            self.viewModel = MenuViewModel.init(riderInfo)
        }
    }
    
    override func localize() {
        self.versionLbl?.text = String.version
        self.accessKeyOrEnvLbl?.text = "Dev Team"
        madeWithLoveLbl?.attributedText = "Made with ♥ in India".heartAttributted
    }
}

extension String {
    static var version: String? {
        return "Version".localized + " " + (appVersion ?? "")
    }
}

//MARK: - UITableview Datasorce and delegate Methods
extension MenuViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuItem.all.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: MenuCell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        
        cell.icon.image = MenuItem.all[indexPath.row].imageIcon//UIImage(named: iconList[indexPath.row])?.withRenderingMode(.alwaysTemplate)
        cell.icon.tintColor = UIColor.colorAccent
        cell.title.text = MenuItem.all[indexPath.row].rawValue.localized
//        cell.title.textColor = UIColor.colorPrimary
        //        cell.title.font = UIFont.mySemiBoldSystemFont(ofSize: 17)
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        slideMenuController()?.closeLeft()
        
        let menuItem = MenuItem.all[indexPath.row]
        switch menuItem {
        case .Home, .BookRide:
            self.homeViewController?.rideCheckHelper?.stopListening()
            self.homeViewController?.mapView.delegate = nil
            self.homeViewController?.rideCheckHelper = nil
            self.homeViewController?.removeChatObserver()
            self.homeViewController?.stopAnimationTimer()
            self.homeViewController?.releaseCallBacks()
            Router.setDashboardViewControllerAsRoot()
        case .Notifications:
            let vc = NotificationViewController.initFromStoryBoard(.Menu)
            self.pushViewController(vc, type: NotificationViewController.self)
        case .YourTrips, .YourService:
            let vc = TripsViewController.initFromStoryBoard(.Menu)
            self.pushViewController(vc, type: TripsViewController.self)
            //        case .BookRide:
            //            let vc = Router.main.instantiateViewController(withIdentifier: StoryboardIDs.RideLaterViewController)
        //            self.pushViewController(vc, type: RideLaterViewController.self)
        case .RentARide:
            let vc = RideTourViewController.initFromStoryBoard(.Main)
            self.pushViewController(vc, type: RideTourViewController.self)
        case .SpendHistory:
            let vc = SpendHistoryViewController.initFromStoryBoard(.Payment)
            self.pushViewController(vc, type: SpendHistoryViewController.self)
        case .MyWallet:
            let vc = WalletHistoryViewController.initFromStoryBoard(.Payment)
            self.pushViewController(vc, type: WalletHistoryViewController.self)
        case .Feedback:
            let vc = FeedbackViewController.initFromStoryBoard(.Menu)
            self.pushViewController(vc, type: FeedbackViewController.self)
        case .About:
            let vc = AboutViewController.initFromStoryBoard(.Menu)
            self.pushViewController(vc, type: AboutViewController.self)
        case .Support:
            let vc = SupportViewController.initFromStoryBoard(.Menu)
            self.pushViewController(vc, type: SupportViewController.self)
        case .InviteFriends:
            let vc = InviteFriendViewController.initFromStoryBoard(.Menu)
            self.pushViewController(vc, type: InviteFriendViewController.self)
        case .Settings:
            let vc = SettingsViewController.initFromStoryBoard(.Menu)
            self.pushViewController(vc, type: SettingsViewController.self)
        case .RateUs:
            rateApp()
            break
        case .ExitDemo:
            self.showAlertWithMessage("Are you sure want to exit from demo?".localized, phrase1: "Yes".localized, phrase2: "No".localized, action: UIAlertAction.Style.default, UIAlertAction.Style.cancel, completion: {[weak self] (style) in
                if style == .default {
                    UserDefaults.accessKey = nil
                    self?.homeViewController?.rideCheckHelper?.stopListening()
                    self?.homeViewController?.mapView.delegate = nil
                    self?.homeViewController?.rideCheckHelper = nil
                    self?.homeViewController?.removeChatObserver()
                    self?.homeViewController?.stopAnimationTimer()
                    ServiceManager().logout { [weak self] (inner) -> (Void) in
                        CacheManager.sharedInstance.clearCache()
                        Router.setSplashViewControllerAsRoot()
                    }
                }
            })
            
        case .Logout:
            self.navigationController?.popToRootViewController(animated: true)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: String.LogoutNotification), object: nil)
        case .Terms:
            let infoVC = Router.authenticate.instantiateViewController(withIdentifier: "InfoViewController")
            (infoVC as? InfoViewController)?.title = "Terms & Conditions".localized
            (infoVC as? InfoViewController)?.index = 3
            self.pushViewController(infoVC, type: InfoViewController.self)
        default:
            break
        }
    }
    func rateApp() {
           /*if #available(iOS 10.3, *) {
               SKStoreReviewController.requestReview()

           } else if let url = URL(string: "itms-apps://itunes.apple.com/app/" + "1561373992") {
               if #available(iOS 10, *) {
                   UIApplication.shared.open(url, options: [:], completionHandler: nil)

               } else {
                   UIApplication.shared.openURL(url)
               }
           }*/
       }
}

extension Router {
    static func setSplashViewControllerAsRoot() {
        UserDefaults.firstTimeLanuchStatus = true
        let initialViewController = SplashViewController.initFromStoryBoard(.Authenticate)
        UIApplication.shared.keyWindow?.rootViewController = initialViewController
    }
}
