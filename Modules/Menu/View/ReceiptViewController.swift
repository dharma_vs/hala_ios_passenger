//
//  ReceiptViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit

class ReceiptViewController: BaseViewController {
    
    var viewModel: ReceiptViewModel! {
        didSet {
            self.viewModel.updateReceiptViewController(self)
        }
    }
    
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var discountValueLbl: UILabel!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var bookingDate: UILabel!
    @IBOutlet weak var bookingId: UILabel!
    @IBOutlet weak var invoiceNo: UILabel!
    @IBOutlet weak var totalFare: UILabel!
    @IBOutlet weak var vehicleType: UILabel!
    @IBOutlet weak var weekDayTimeLbl: UILabel!
    @IBOutlet weak var sourceAddress: UILabel!
    @IBOutlet weak var destinationAddress: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var passengerName: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var passengerLbl: UILabel!
    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var vehicleLbl: UILabel!
    @IBOutlet weak var vehicle: UILabel!
    @IBOutlet weak var driverLbl: UILabel!
    @IBOutlet weak var driver: UILabel!
    @IBOutlet weak var driverPhoneLbl: UILabel!
    @IBOutlet weak var driverPhone: UILabel!
    @IBOutlet weak var licensePlageLbl: UILabel!
    @IBOutlet weak var licensePlage: UILabel!
    @IBOutlet weak var billDetailsLbl: UILabel!
    @IBOutlet weak var rideFareLbl: UILabel!
    @IBOutlet weak var rideFare: UILabel!
    @IBOutlet weak var roundOffLbl: UILabel!
    @IBOutlet weak var extraKmLbl: UILabel!
    @IBOutlet weak var extraKmValueLbl: UILabel!
    @IBOutlet weak var roundOff: UILabel!
    @IBOutlet weak var totalFareLbl: UILabel!
    @IBOutlet weak var paymentLbl: UILabel!
    @IBOutlet weak var cashLbl: UILabel!
    @IBOutlet weak var cash: UILabel!
    @IBOutlet weak var cardLbl: UILabel!
    @IBOutlet weak var card: UILabel!
    @IBOutlet weak var walletLbl: UILabel!
    @IBOutlet weak var wallet: UILabel!
    @IBOutlet weak var rideType: UILabel!
    @IBOutlet weak var packageLbl: UILabel!
    @IBOutlet weak var startReadingLbl: UILabel!
    @IBOutlet weak var endReadingLbl: UILabel!
    
    @IBOutlet weak var destinationChargeValueLbl: UILabel!
    @IBOutlet weak var destinationChargeLbl: UILabel!
    @IBOutlet weak var waitingChargeLbl: UILabel!
    @IBOutlet weak var waitingChargeValueLbl: UILabel!
    @IBOutlet weak var waitingTimeLbl: UILabel!
    @IBOutlet weak var waitingTimeValueLbl: UILabel!
    @IBOutlet weak var peakTimeFareLbl: UILabel!
    @IBOutlet weak var peakTimeFareValueLbl: UILabel!

    @IBOutlet weak var cardContent: UIView!
    @IBOutlet weak var cashContent: UIView!
    @IBOutlet weak var walletContent: UIView!
    
    @IBOutlet weak var downloadBtn, share: UIButton!
    
    @IBAction func downloadReceipt(_ sender: Any) {
    var screenshotImage :UIImage?
         let layer = UIApplication.shared.keyWindow!.layer
         let scale = UIScreen.main.scale
         UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
         guard let context = UIGraphicsGetCurrentContext() else {return }
         layer.render(in:context)
         screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
         UIGraphicsEndImageContext()
         if let image = screenshotImage {
             UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            self.view.makeToast("Receipt saved in your Photos")
         }
    }
    
    @IBAction func shareReceipt(_ sender: Any) {
    
    }
    
    var rideDetail: RideDetail!

    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        self.viewModel = ReceiptViewModel(self.rideDetail)
        // Do any additional setup after loading the view.
    }
    
    override func localize() {
        discountLbl?.text = "Discount".localized
        titleLbl.text = "Receipt".localized
        distanceLbl.text = "Distance".localized
        durationLbl.text = "Duration".localized
        passengerLbl.text = "Passenger".localized
        phoneNumberLbl.text = "Phone Number".localized
        driverLbl.text = "Driver".localized
        driverPhoneLbl.text = "Phone Number".localized
        vehicleLbl.text = "Vehicle".localized
        licensePlageLbl.text = "License Plage".localized
        billDetailsLbl.text = "Bill Details".localized
        rideFareLbl.text = "Ride Fare".localized
        extraKmLbl.text = "Extra Km Fare".localized
        roundOffLbl.text = "Round Off".localized
        totalFareLbl.text = "Total Fare".localized
        paymentLbl.text = "Payment".localized
        cashLbl.text = "Cash".localized
        cardLbl.text = "Card".localized
        walletLbl.text = "Wallet".localized
        destinationChargeLbl.text = "Destination Change Fare".localized
        waitingChargeLbl.text = "Waiting charge".localized
        self.waitingTimeLbl.text = "Waiting Time".localized
        peakTimeFareLbl.text = "Peak Time Charge".localized

        
    }
    
}
