//
//  ChangeLanguageViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//
import UIKit
import DropDown
//import Localize_Swift

class ChangeLanguageViewController: BaseViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var languageTitle: UILabel!
    @IBOutlet weak var languageBtn: UIButton!
    @IBOutlet weak var updateBtn: UIButton!
    
    var didLanguageChange: (() -> Void)!
    
    var isLanguageBool = Bool()
    
    var currencySymbol = String()
    
    var currencyItem = ["INR"]
    
    lazy var dropDown: DropDown = {
        let dropDwn = DropDown()
        dropDwn.anchorView = self.languageBtn
        dropDwn.direction = .any
        dropDwn.backgroundColor = .white
        dropDwn.width = self.languageBtn.bounds.size.width
        if !isLanguageBool{
            currencyItem.removeAll()
            for item in (((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currencies)!){
                print(item)
                currencyItem.append("\(item.currency_code ?? "")-\(item.currency_symbol ?? "")")
                if "\(CacheManager.sharedInstance.getObjectForKey(.updatedCurrencySymbol) ?? "")" == "\(item.currency_symbol ?? "")"{
                    self.currencySymbol = "\(item.currency_code ?? "")-\(item.currency_symbol ?? "")"
                }
            }
            dropDwn.dataSource = currencyItem
            
        }else {
            dropDwn.dataSource = Language.allRawValues
        }
        dropDwn.textFont = UIFont(name: "Montserrat-SemiBold", size: 15) ?? UIFont.systemFont(ofSize: 15)
        
        dropDwn.selectionAction = { [weak self] (index: Int, item: String) in
            if !self!.isLanguageBool{
                
                CacheManager.settings?.currency_symbol = "\((((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currencies)?[index].currency_symbol ?? ""))"
                CacheManager.sharedInstance.setObject("\((((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currencies)?[index].currency_symbol ?? ""))", key: .updatedCurrencySymbol)
                CacheManager.sharedInstance.setObject("\((((CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings)?.currencies)?[index].rate ?? ""))", key: .currencyRate)
            }
            self?.languageBtn.setTitle(item, for: .normal)
            dropDwn.hide()
        }
        return dropDwn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        languageBtn.titleEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: (UIScreen.main.bounds.width * 0.55))
        
        // Do any additional setup after loading the view.
    }
    
    override func setup() {
        super.setup()
//        dropDown.selectRow(Language.language(Locale.getAppLanguage()).index)
    }
    
    override func localize() {
        super.localize()
        if !isLanguageBool{
            self.titleLbl.text = "Change Currency".localized
            self.languageTitle.text = "Select Currency".localized
            self.languageBtn.setTitle("\(self.currencySymbol)", for: .normal)
            
        }else {
            titleLbl.text = "Change Language".localized.capitalized
            languageTitle.text = "Select Language".localized
//            languageBtn.setTitle(Language.language(Locale.getAppLanguage()).rawValue, for: .normal)
        }
        updateBtn.setTitle("Update".localized, for: .normal)
        
    }
    
    @IBAction func languageBtnPressed() {
        self.dropDown.show()
    }
    
    @IBAction func continueClickAction() {
        if !isLanguageBool{
            didLanguageChange?()
            self.speak(text: "Your currency updated".localized)
        }else {
            if let language = dropDown.selectedItem, let lang = Language(rawValue: language) {
//                Locale.saveAppLanguage(lang.shortName)
//                Localize.setCurrentLanguage(lang.shortName)

                didLanguageChange?()
            }
        }
        self.homeViewController?.rideCheckHelper?.stopListening()
        self.homeViewController?.mapView.delegate = nil
        self.homeViewController?.rideCheckHelper = nil
        self.homeViewController?.removeChatObserver()
        self.homeViewController?.stopAnimationTimer()
        Router.setSplashViewControllerAsRoot()
    }
}
