//
//  ImageZoomViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit

class ImageZoomViewController: BaseViewController {
    
    @IBOutlet weak var userImage: AppImageView!
    
    @IBOutlet weak var userImageHeightConstraint:NSLayoutConstraint!
    
    @IBOutlet weak var dismissBtn: AppButton!
    
    var url: URL?

    override func viewDidLoad() {
        super.viewDidLoad()
        userImage?.loadTaxiImage(url)
        userImage.clipsToBounds = true
        userImage.layer.cornerRadius = 20
        self.userImageHeightConstraint.constant = 40
        self.dismissBtn.backgroundColor = UIColor.black
        self.dismissBtn.alpha = 0

       
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.5, animations: { [weak self] in
            self?.userImageHeightConstraint.constant = 200
            self?.userImage.layer.cornerRadius = 100
            self?.view.layoutIfNeeded()
            self?.dismissBtn.alpha = 0.75
        }) { [weak self](status) in
            self?.userImageHeightConstraint.constant = 200
            self?.userImage.layer.cornerRadius = 100
        }
    }
    
    @IBAction func dismisVC() {
        self.dismiss(animated: false, completion: nil)
    }

}
