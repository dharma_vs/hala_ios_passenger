//
//  YourTripsViewController.swift
//  TaxiPickup
//
//  Created by Prabakaran on 21/01/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

class YourTripsViewController: UIViewController {

    @IBOutlet weak var tripsTableview: UITableView!
    
    @IBOutlet weak var upcomingButton: UIButton!
    @IBOutlet weak var pastButton: UIButton!

    var not: Int = 0
    
    var paginator: Paginator<RideListItem>!
    
    var rideListItems = [RideListItem]() {
        didSet {
            tripsTableview.reloadData()
        }
    }
    
    fileprivate var rideType = RideListType.open

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.startActivityIndicatorInWindow()
        

        // Do any additional setup after loading the view.
        
        tripsTableview.register(UINib.init(nibName: "TripCell", bundle: nil), forCellReuseIdentifier: "TripCell")
        self.upcomingClickAction()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func ShowSlidemenu()
    {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func upcomingClickAction()
    {
        not = 3
        tripsTableview.reloadData()
        upcomingButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        pastButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
    }
    @IBAction func pastClickAction()
    {
        not = 10
        tripsTableview.reloadData()
        pastButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        upcomingButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
    }
    
    func loadData(_ page: Int = 0, type: RideListType) {
        rideType = type
        
        if let _paginator = paginator, _paginator.currentPage?.intValue() != _paginator.totalPages {
            return
        }
        
        self.startActivityIndicatorInWindow()
        
        ServiceManager().rideList(page, rideType: type) { [weak self] (inner) -> (Void) in
            do {
                self?.paginator = try inner()
                if let rideList = self?.paginator?.result {
                    self?.rideListItems.append(contentsOf: rideList)
                }
                
                if self?.rideListItems.count == 0 {
                    self?.tripsTableview.isHidden = true
//                    self.noResultLbl.isHidden = false
                }
            } catch(_) {
                
            }
            self?.stopActivityIndicator()
        }
    }
}

extension YourTripsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rideListItems.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 110
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripCell", for: indexPath) as! TripCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.separatorInset = UIEdgeInsets.zero;
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        if let vc = storyBoard.instantiateViewController(withIdentifier: "TripDetailsViewController") as? TripDetailsViewController
        {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
