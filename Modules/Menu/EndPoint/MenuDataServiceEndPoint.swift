//
//  MenuDataServiceEndPoint.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 05/03/22.
//

import Foundation

enum MenuDataServiceEndpoint: AddressableEndPoint {
    
    case aboutUsInfo, deleteCards, changePassword, appFeedback, logout, getAllNotifications, addFavouriteLocation, updateFavouriteLocation, deleteFavouriteLocationById, updateProfile, getAllFavouriteLocations, upload, rideList,
         walletHistory

    var endPointInfo: DataServiceEndpointInfo {
        switch self {
        case .aboutUsInfo:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentMasterPathSuffix+"/info/1", method: .GET, authType: .none)
            
        case .deleteCards:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: "/cards", method: .DELETE, authType: .user)
            
        case .changePassword:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: "/rider/profile/change-password", method: .PUT, authType: .user)
            
        case .appFeedback:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix+"/profile/app_feedback", method: .PUT, authType: .none)
            
        case .logout:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix+"/profile/logout", method: .POST, authType: .user)
            
        case .getAllNotifications:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: "/rider/profile/notification", method: .GET, authType: .user)
            
        case .addFavouriteLocation:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: "/rider/fav-location", method: .POST, authType: .user)
            
        case .updateFavouriteLocation:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: "/rider/fav-location/", method: .PUT, authType: .user)
            
        case .deleteFavouriteLocationById:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: "rider/fav-location/{id}", method: .DELETE, authType: .user)
            
        case .updateProfile:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix+"/profile/update-profile", method: .PUT, authType: .user)

        case .getAllFavouriteLocations:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: "/rider/fav-location", method: .GET, authType: .user)
            
        case .upload:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: "/upload"+componentPathSuffix, method: .POST, authType: .user)
            
        case .rideList:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: "/ride/ride-list", method: .GET, authType: .user)
            
        case .walletHistory:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: "/rider/profile/wallet-history", method: .GET, authType: .user)
        }
    }
}
