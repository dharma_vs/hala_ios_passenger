//
//  AboutInfo.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 05/03/22.
//

import Foundation

struct AboutUsInfo: JSONSerializable {
    let id: Int?
    let title: String?
    let content: String?
}
