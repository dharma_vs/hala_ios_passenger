//
//  FileUploadResponse.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 09/03/22.
//

import Foundation

class FileUploadResponse: NSObject, Decodable {
    var name: String!
}
