//
//  NotificationInfo.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 10/03/22.
//

import Foundation

struct NotificationInfo: JSONSerializable {
    let id: Int?
    let title: String?
    let message: String?
    let image: String?
    let web_url: String?
    let created_at: String?
}


class NotificationListResponse: Paginator<NotificationInfo> {
    
}
