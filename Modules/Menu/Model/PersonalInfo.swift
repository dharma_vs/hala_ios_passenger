//
//  PersonalInfo.swift
//  Taxi
//
//  Created by VJ on 03/03/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

class PersonalInfo: NSObject, JSONSerializable, CustomReflectable {
    var name: String!
    var email: String!
    var phone: String!
    var dob: String!
    var gender: String!
    var address: String!
    var city: String!
    var state: String!
    var identifyCard: String!
    var addressProof: String!
    var profileImage: String!
    var driver_status: String!
    var address_proof:String!
    var ccp:String!
    
    var customMirror: Mirror {
        return Mirror(PersonalInfo.self, children: ["name": name, "email": email, "ccp": ccp, "phone": phone, "dob": dob, "gender": gender, "address": address, "id_card": identifyCard, "profile_pic": profileImage, "driver_status": driver_status, "city_name": city, "state": state,"address_proof":address_proof], displayStyle: Mirror.DisplayStyle.class, ancestorRepresentation: .generated)
    }
}

class LicenceDetail: NSObject, JSONSerializable, CustomReflectable {
    var registerNumber: String!
    var number: String!
    var expiry: String!
    var issued: String!
    var front_image: String!
    var rear_image: String!
    var driver_status: String!
    
    var customMirror: Mirror {
        return Mirror(LicenceDetail.self, children: ["licence_number": number, "issued_on": issued, "expiry_on": expiry, "licence_front": front_image, "licence_rear": rear_image, "driver_status": driver_status, "reg_no": registerNumber], displayStyle: Mirror.DisplayStyle.class, ancestorRepresentation: .generated)
    }
}

class CabDetail: NSObject, JSONSerializable, CustomReflectable {
    var city: Int!
    var cityName: String!
    var type: Int!
    var typeName: String!
    var category: String!
    var model: Int!
    var modelName: String!
    var number: String!
    var color: String!
    var year: String!
    var document: String!
    var insurance: String!
    var photoFront: String!
    var photoRear: String!
    var driver_status: String!
    var vehicle_fc:String!
    var vehicle_permit:String!
    var vehicle_pollution_certificate:String!
    
    var customMirror: Mirror {
        return Mirror(CabDetail.self, children: ["category": category, "city_id": city, "vehicle_type_id": type, "model_id": model, "model_name": modelName, "vehicle_number": number, "color": color, "year": year, "vehicle_document": document, "vehicle_insurance": insurance, "vehicle_photo_front": photoFront, "vehicle_photo_rear": photoRear,"vehicle_fc":vehicle_fc,"vehicle_permit":vehicle_permit,"vehicle_pollution_certificate":vehicle_pollution_certificate, "driver_status": driver_status], displayStyle: Mirror.DisplayStyle.class, ancestorRepresentation: .generated)
    }
}

struct City: Decodable {
    let id: Int!
    let name: String!
    let country: String!
}

struct Type: Decodable {
    let vehicle_type: [VehicleType]!
    let category_type: [CategoryType]!
}

struct VehicleType: Decodable {
    let id: Int!
    let name: String!
    let image: String!
    let cab_marker: String!
    let available: String!
}

struct CategoryType: Decodable {
    let id: Int!
    let name: String!
    
    static func rideCategories(_ categories: [CategoryType]) -> [RideCategory] {
        var rideCategories = [RideCategory]()
        for cat in categories {
            if let rideCat = RideCategory.init(rawValue: cat.id) {
                rideCategories.append(rideCat)
            }
        }
        return rideCategories
    }
}

struct VehicleModel: Decodable {
    let id: Int!
    let name: String!
    let image: String!
}

struct Bank: Decodable {
    let id: Int!
    let name: String!
}
