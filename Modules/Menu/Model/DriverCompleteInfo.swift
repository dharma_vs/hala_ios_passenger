//
//  DriverCompleteInfo.swift
//  Taxi
//
//  Created by ShamlaTech on 2/15/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import Foundation



enum DriverProfileStatus: String {
    case new = "1"
    case approvalWaiting = "2"
    case enabled = "3"
    case rejected = "4"
    case disabled = "5"
    case update = "6"
}

class DriverCompleteInfo: NSObject, JSONSerializable, NSCoding {
    let id: Int?
    let id_no:String?
    var id_card: String?
    let device_id: String?
    let title: String?
    let message: String?
    var name: String?
    let ccp: String?
    var phone: String?
    var email: String?
    var image: String?
    let password: String?
    var address: String?
    var gender: String?
    var dob: String?
    var city: String?
    var state: String?
    var address_image: String?
    var online_status: String?
    let driver_status: String?
    let referral_code: String?
    let mobile_verified: String?
    let email_verified: String?
    let personal_info_updated: String?
    let cab_info_updated: String?
    let licence_info_updated: String?
    let cab_info: CabInfo?
    let licence_info: LicenceInfo?
    var account_info: BankInfo?
    let register_at: String?
    let completed_rides: Int?
    let link: String?
    let stats_info: StatsInfo?
    let withdraw_req: [WithdrawReq]?
    var wallet_amount: CustomType?
    var card_info: [Card_info]?
    let company_info: Company_info?
    let dial_code:String?
    
    var driverPhone: String {
        return (self.dial_code ?? "") + (self.phone ?? "")
    }
    
    var driverProfileStatus: DriverProfileStatus {
        get {
            return DriverProfileStatus(rawValue: driver_status ?? "1") ?? .new
        }
        set {

        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.title = aDecoder.decodeObject(forKey: "title") as? String
        self.message = aDecoder.decodeObject(forKey: "message") as? String
        self.register_at = aDecoder.decodeObject(forKey: "register_at") as? String
        self.completed_rides = aDecoder.decodeObject(forKey: "completed_rides") as? Int
        self.link = aDecoder.decodeObject(forKey: "link") as? String
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.id_card = aDecoder.decodeObject(forKey: "id_card") as? String
        self.id_no = aDecoder.decodeObject(forKey: "id_no") as? String
        self.device_id = aDecoder.decodeObject(forKey: "device_id") as? String
        self.name = aDecoder.decodeObject(forKey: "name") as? String
        self.ccp = aDecoder.decodeObject(forKey: "ccp") as? String
        self.phone = aDecoder.decodeObject(forKey: "phone") as? String
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.image = aDecoder.decodeObject(forKey: "image") as? String
        self.password = aDecoder.decodeObject(forKey: "password") as? String
        self.address = aDecoder.decodeObject(forKey: "address") as? String
        self.gender = aDecoder.decodeObject(forKey: "gender") as? String
        self.dob = aDecoder.decodeObject(forKey: "dob") as? String
        self.city = aDecoder.decodeObject(forKey: "city") as? String
        self.state = aDecoder.decodeObject(forKey: "state") as? String
        self.address_image = aDecoder.decodeObject(forKey: "address_image") as? String
        self.online_status = aDecoder.decodeObject(forKey: "online_status") as? String
        self.driver_status = aDecoder.decodeObject(forKey: "driver_status") as? String
        self.referral_code = aDecoder.decodeObject(forKey: "referral_code") as? String
        self.mobile_verified = aDecoder.decodeObject(forKey: "mobile_verified") as? String
        self.email_verified = aDecoder.decodeObject(forKey: "email_verified") as? String
        self.personal_info_updated = aDecoder.decodeObject(forKey: "personal_info_updated") as? String
        self.cab_info_updated = aDecoder.decodeObject(forKey: "cab_info_updated") as? String
        self.licence_info_updated = aDecoder.decodeObject(forKey: "licence_info_updated") as? String
        self.cab_info = aDecoder.decodeObject(forKey: "cab_info") as? CabInfo
        self.licence_info = aDecoder.decodeObject(forKey: "licence_info") as? LicenceInfo
        self.account_info = aDecoder.decodeObject(forKey: "account_info") as? BankInfo
        self.stats_info = aDecoder.decodeObject(forKey: "stats_info") as? StatsInfo
        self.withdraw_req = aDecoder.decodeObject(forKey: "withdraw_req") as? [WithdrawReq]
       // self.wallet_amount = aDecoder.decodeObject(forKey: "wallet_amount") as? Int
        self.wallet_amount = aDecoder.decodeObject(forKey: "wallet_amount") as? CustomType
        self.card_info = aDecoder.decodeObject(forKey: "card_info") as? [Card_info]
        self.company_info = aDecoder.decodeObject(forKey: "company_info") as? Company_info
        self.dial_code = aDecoder.decodeObject(forKey: "dial_code") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
    
    func mapPersonalInfo(_ info: PersonalInfo) {
        name = info.name
        email = info.email
        phone = info.phone
        dob = info.dob
        gender = info.gender
        address = info.address
        id_card = info.identifyCard
        address_image = info.addressProof
        image = info.profileImage
        city = info.city
        state = info.state
    }

    func mapCompanyInfo(_ info: Company_info) {
        company_info?.id = info.id
        company_info?.name = info.name
        company_info?.email = info.email
        company_info?.image = info.image
        company_info?.ccp = info.ccp
        company_info?.mobile = info.mobile
    }
    func mapLicenceInfo(_ info: LicenceInfo) {
        licence_info?.licenceNumber = info.licenceNumber
        licence_info?.issuedOn = info.issuedOn
        licence_info?.expiryOn = info.expiryOn
        licence_info?.licenceFront = info.licenceFront
        licence_info?.licenceRear = info.licenceRear
    }
    
    func mapCabInfo(_ info: CabDetail) {
        cab_info?.city_id = info.city
        cab_info?.city_name = info.cityName
        cab_info?.category_name = info.typeName
        cab_info?.category_id = info.type
        cab_info?.model_name = info.modelName
        cab_info?.model_id = info.model
        cab_info?.cab_number = info.number
        cab_info?.year = info.year
        cab_info?.cab_color = info.color
        cab_info?.vehicle_document = info.document
        cab_info?.vehicle_insurance = info.insurance
        cab_info?.vehicle_photo_front = info.photoFront
        cab_info?.vehicle_photo_rear = info.photoRear
    }
    
    func mapBankInfo(_ info: BankInfo) {
        account_info?.account_number = info.account_number
        account_info?.account_holder_name = info.account_holder_name
        account_info?.bank_id = info.bank_id
        account_info?.bank_name = info.bank_name
        //account_info?.ifsc_code = info.ifsc_code
        account_info?.bank_document = info.bank_document
    }
}

class WithdrawReq: NSObject, JSONSerializable, NSCoding {
    let id, amount: Int?
    let requestAt: String?
    
    var requestInfo: String {
        let date = self.requestAt?.UTCToLocal("yyyy-MM-dd HH:mm:ss", formatTo:"dd-MM-yyyy hh:mm a") ?? ""
        return "Request".localized + ": " + "\(self.amount ?? 0)".currencyValue + " (\(date))"
    }
    
    enum CodingKeys: String, CodingKey {
        case id, amount
        case requestAt = "request_at"
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.amount = aDecoder.decodeObject(forKey: "amount") as? Int
        self.requestAt = aDecoder.decodeObject(forKey: "requestAt") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
}

class CabInfo: NSObject, JSONSerializable, NSCoding {

    let cab_id: String?
    let cab_make: String?
    var cab_number: String?
    var model_id: Int?
    var model_name: String?
    var year: String?
    var cab_color: String?
    var city_id: Int?
    var city_name: String?
    var category_id: Int?
    var category: String?
    var category_name: String?
    let capacity: Int?
    let category_image: String?
    let cab_marker: String?
    var vehicle_document: String?
    var vehicle_insurance: String?
    
    let vehicle_type_id: Int?
    let vehicle_type_name: String?
    let vehicle_image: String?
    
    var vehicle_photo_front: String?
    var vehicle_photo_rear: String?
    var vehicle_fc: String?
    var vehicle_permit: String?
    var vehicle_pollution_certificate: String?

    required init?(coder aDecoder: NSCoder) {
        self.vehicle_type_id = aDecoder.decodeObject(forKey: "vehicle_type_id") as? Int
        self.vehicle_type_name = aDecoder.decodeObject(forKey: "vehicle_type_name") as? String
        self.vehicle_image = aDecoder.decodeObject(forKey: "vehicle_image") as? String
        self.vehicle_photo_front = aDecoder.decodeObject(forKey: "vehicle_photo_front") as? String
        self.vehicle_photo_rear = aDecoder.decodeObject(forKey: "vehicle_photo_rear") as? String
        self.cab_id = aDecoder.decodeObject(forKey: "model_id") as? String
        self.cab_make = aDecoder.decodeObject(forKey: "cab_make") as? String
        self.cab_number = aDecoder.decodeObject(forKey: "cab_number") as? String
        self.model_id = aDecoder.decodeObject(forKey: "model_id") as? Int
        self.model_name = aDecoder.decodeObject(forKey: "model_name") as? String
        self.year = aDecoder.decodeObject(forKey: "year") as? String
        self.cab_color = aDecoder.decodeObject(forKey: "cab_color") as? String
        self.city_id = aDecoder.decodeObject(forKey: "city_id") as? Int
        self.city_name = aDecoder.decodeObject(forKey: "city_name") as? String
        self.category_id = aDecoder.decodeObject(forKey: "category_id") as? Int
        self.category_name = aDecoder.decodeObject(forKey: "category_name") as? String
        self.category = aDecoder.decodeObject(forKey: "category") as? String
        self.capacity = aDecoder.decodeObject(forKey: "capacity") as? Int
        self.category_image = aDecoder.decodeObject(forKey: "category_image") as? String
        self.cab_marker = aDecoder.decodeObject(forKey: "cab_marker") as? String
        self.vehicle_document = aDecoder.decodeObject(forKey: "vehicle_document") as? String
        self.vehicle_insurance = aDecoder.decodeObject(forKey: "vehicle_insurance") as? String
        self.vehicle_fc = aDecoder.decodeObject(forKey: "vehicle_fc") as? String
        self.vehicle_permit = aDecoder.decodeObject(forKey: "vehicle_permit") as? String
        self.vehicle_pollution_certificate = aDecoder.decodeObject(forKey: "vehicle_pollution_certificate") as? String

    }
    
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
}


class BankInfo: NSObject, JSONSerializable, NSCoding, CustomReflectable {
    var bank_id: String?
    var bank_name: String?
    var account_holder_name: String?
    var account_number: String?
    //var ifsc_code: String?
    var bank_document: String? = "bankDocumentUploadedURL"
    var driver_status: String?
    
    var customMirror: Mirror {
        // "ifsc_code": ifsc_code ?? ""
        return Mirror(BankInfo.self, children: ["commission": 1, "bank_id": bank_id ?? "0", "bank_name": bank_name, "acc_holder_name": account_holder_name, "acc_no": account_number,  "passbook": bank_document, "driver_status": driver_status], displayStyle: Mirror.DisplayStyle.class, ancestorRepresentation: .generated)
    }
    
    override init() {
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        bank_id = aDecoder.decodeObject(forKey: "bank_id") as? String
        bank_name = aDecoder.decodeObject(forKey: "bank_name") as? String
        account_holder_name = aDecoder.decodeObject(forKey: "account_holder_name") as? String
        account_number = aDecoder.decodeObject(forKey: "account_number") as? String
        //ifsc_code = aDecoder.decodeObject(forKey: "ifsc_code") as? String
        bank_document = aDecoder.decodeObject(forKey: "bank_document") as? String
        driver_status = aDecoder.decodeObject(forKey: "driver_status") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
}

class StatsInfo: NSObject, JSONSerializable, NSCoding {
    var rating: CustomType?
    var acceptance_rate: CustomType?
    let completed_ride: CustomType?
    let rejected_rides: Int?
    let cancelled_rides: Int?
    let total_distance: Int?
    let total_duration: Int?
    
    required init?(coder aDecoder: NSCoder) {
        rating = aDecoder.decodeObject(forKey: "rating") as? CustomType
        acceptance_rate = aDecoder.decodeObject(forKey: "acceptance_rate") as? CustomType
        completed_ride = aDecoder.decodeObject(forKey: "completed_ride") as? CustomType
        rejected_rides = aDecoder.decodeObject(forKey: "rejected_rides") as? Int
        cancelled_rides = aDecoder.decodeObject(forKey: "cancelled_rides") as? Int
        total_distance = aDecoder.decodeObject(forKey: "total_distance") as? Int
        total_duration = aDecoder.decodeObject(forKey: "total_duration") as? Int
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        rating = try? values.decode(CustomType.self, forKey: .rating)
        acceptance_rate = try? values.decode(CustomType.self, forKey: .acceptance_rate)
        completed_ride = try? values.decode(CustomType.self, forKey: .completed_ride)
        rejected_rides = try? values.decode(Int.self, forKey: .rejected_rides)
        cancelled_rides = try? values.decode(Int.self, forKey: .cancelled_rides)
        total_distance = try? values.decode(Int.self, forKey: .total_distance)
        total_duration = try? values.decode(Int.self, forKey: .total_duration)
    }
    
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
}

class Company_info: NSObject, JSONSerializable, NSCoding{
    var id : Int?
    var name : String?
    var email : String?
    var mobile : String?
    var ccp : String?
    var image : String?
    
     required init?(coder aDecoder: NSCoder) {
           self.id = aDecoder.decodeObject(forKey: "id") as? Int
           self.name = aDecoder.decodeObject(forKey: "name") as? String
           self.email = aDecoder.decodeObject(forKey: "email") as? String
           self.mobile = aDecoder.decodeObject(forKey: "mobile") as? String
           self.ccp = aDecoder.decodeObject(forKey: "ccp") as? String
           self.image = aDecoder.decodeObject(forKey: "image") as? String
       }
       
       func encode(with aCoder: NSCoder) {
           for case let (label, value) in Mirror(reflecting: self).children {
               aCoder.encode(value, forKey: label!)
           }
       }

}
