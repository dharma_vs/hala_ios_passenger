//
//  File.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import Foundation

let pageSize = 10

class Paginator<T: JSONSerializable>: JSONSerializable {
    let totalHits: Int?
    let pageSize: String?
    var totalPages: Int?
    let currentPage: String?
    let result: [T]?
    
    init() {
        self.totalHits = nil
        self.pageSize = nil
        self.totalPages = nil
        self.currentPage = nil
        self.result = []
    }
}

struct RideListItem: JSONSerializable {
    let id: Int?
    let booking_id: String?
    let service_type: String?
    let category_id: String?
    let vehicle_type_id: Int?
    let vehicle_type_name: String?
    let s_name: String?
    let booking_status: Int?
    let s_address: String?
    let s_latitude: CustomType?
    let s_longitude: CustomType?
    let d_address: String?
    let d_latitude: CustomType?
    let d_longitude: CustomType?
    let otp: String?
    let ride_type: String?
    let create_time: String?
    let booking_time: String?
    let fare: Int?
    var ride_later_enabled: CustomType?
}

class RideListResponse: Paginator<RideListItem> {
    
}

extension String {
    var notMentioned: String {
        return self.isEmpty ? "Not Mentioned".localized : self
    }
}
