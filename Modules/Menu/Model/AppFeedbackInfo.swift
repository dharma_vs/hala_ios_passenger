//
//  AppFeedbackInfo.swift
//  HalaMobility
//
//  Created by Admin on 26/03/22.
//

import Foundation
import UIKit

struct AppFeedBackReqInfo: JSONSerializable {
    var feedback: String!
    var user: String!
    var mobile: String!
    var email: String!
    var app_version: String!
    var app_package: String!
    var mobile_os: String!
    var mobile_os_version: String!
    var device_id: String!
    
    init() {
        self.feedback = ""
        let userInfo = CacheManager.userInfo
        user = userInfo?.name ?? ""
        mobile = userInfo?.phoneNo ?? ""
        email = userInfo?.email ?? ""
        app_version = appVersion
        app_package = bundleIdentifier
        mobile_os = "ios"
        mobile_os_version = systemVersion
        device_id = HalaMobility.device_Id
    }
    
}

var systemVersion = UIDevice.current.systemVersion
let bundleIdentifier =  Bundle.main.bundleIdentifier
let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
