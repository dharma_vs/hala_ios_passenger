//
//  ProfileReqInfo.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 09/03/22.
//

import Foundation

struct ProfileReqInfo: JSONSerializable {
    var name: String?
    var email: String?
    var ccp: String?
    var tel_no: String?
    var image: String?
}
