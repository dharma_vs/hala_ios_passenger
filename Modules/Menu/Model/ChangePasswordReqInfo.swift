//
//  ChangePasswordReqInfo.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 09/03/22.
//

import Foundation

struct ChangePasswordReqInfo: JSONSerializable {
    var old_password: String?
    var new_password: String?
}
