//
//  ProfileDataServiceEndpoint.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 24/03/22.
//

import Foundation

enum ProfileDataServiceEndpoint: AddressableEndPoint {
    
    case profile, updateLicence
    
    var endPointInfo: DataServiceEndpointInfo {
        switch self {
        case .profile:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path:  "rider/profile", method: .GET, authType: .user)
        case .updateLicence:
            return DataServiceEndpointInfo(scheme: componentScheme, host: componentHost, port: componentPort, path: componentPathSuffix + "/profile/update-licence", method: .PUT, authType: .user)
        }
    }
}

extension ServiceManager {
    
    func profile(_ info: ProfileReq = ProfileReq(), completion: @escaping (_ inner: () throws ->  Profile) -> Void) {
        self.request(endPoint: ProfileDataServiceEndpoint.profile, parameters: info.JSONRepresentation) { result in
            switch result {
            case .success(let data):
                print("\(data) unread messages.")
                let response = data.getDecodedObject(from: GeneralResponse<Profile>.self)
                if let data = response?.data, response?.status == true {
                    completion({return data})
                }
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}

struct ProfileReq: JSONSerializable {

}

struct Profile: JSONSerializable {
    var idCard: String?
    var id, companyID: Int?
    var name, email, image, countryCode: String?
    var phoneNo, password, registerDate, countryShortName: String?
    var rating, categoryID: Int?
    var myReferralCode, referralCode, icNumber, icImage: String?
    var selfie, documentStatus, digioReference, digoTemplate: String?
    var isEmailVerify, isPhoneNoVerify: String?
    var walletAmount: Int?
    var updatedAt, status: String?
    var totalSpend, totalKMTravelled, totalTrips: Int?
    var companyInfo: String?
    var licenceInfo: LicenceInfo?
    var favoriteLocation: [CustomType]?
    var cardInfo: [CardInfo]?
    var membershipInfo: [CustomType]?

    enum CodingKeys: String, CodingKey {
        case idCard = "id_card"
        case id
        case companyID = "company_id"
        case name, email, image
        case countryCode = "country_code"
        case phoneNo = "phone_no"
        case password
        case registerDate = "register_date"
        case countryShortName = "country_short_name"
        case rating
        case categoryID = "category_id"
        case myReferralCode = "my_referral_code"
        case referralCode = "referral_code"
        case icNumber = "ic_number"
        case icImage = "ic_image"
        case selfie
        case documentStatus = "document_status"
        case digioReference = "digio_reference"
        case digoTemplate = "digo_template"
        case isEmailVerify = "IsEmailVerify"
        case isPhoneNoVerify = "IsPhoneNoVerify"
        case walletAmount = "wallet_amount"
        case updatedAt = "updated_at"
        case status
        case totalSpend = "total_spend"
        case totalKMTravelled = "total_km_travelled"
        case totalTrips = "total_trips"
        case companyInfo = "company_info"
        case licenceInfo = "licence_info"
        case favoriteLocation = "favorite_location"
        case cardInfo = "card_info"
        case membershipInfo = "membership_info"
    }
}

struct CardInfo: JSONSerializable {
    var id: Int?
    var registrationID, brand, cardName, cardNo: String?
    var expiryDate, cvv: String?

    enum CodingKeys: String, CodingKey {
        case id
        case registrationID = "registration_id"
        case brand
        case cardName = "card_name"
        case cardNo = "card_no"
        case expiryDate = "expiry_date"
        case cvv
    }
}



