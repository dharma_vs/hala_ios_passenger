//
//  CreatePasswordViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 07/03/22.
//

import Foundation

class CreatePasswordViewModel: AppViewModel {
    weak var controller: CreatePasswordViewController!
    required init(controller: CreatePasswordViewController) {
        self.controller = controller
    }
}

extension ServiceManager {
    
    func changePassword(_ info: ChangePasswordReqInfo, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        self.request(endPoint: MenuDataServiceEndpoint.changePassword, parameters: info.JSONRepresentation as Dictionary<String,Any>) { result in
            
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: CommonRes.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
}
