//
//  ProfileViewModel.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit

class ProfileViewModel: NSObject {
    let name: String!
    let email: String!
    let phone: String!
    let password: String!
    let imageURL: URL!
    let rating: Float!
    let isPhoneVerified: Bool!
    let isEmailVerified: Bool!
    let icNumber: String!
    let icImage: String!
    
    init(userInfo: UserInfo) {
        self.name = userInfo.name
        self.email = userInfo.email
        self.password = userInfo.password
        self.phone = userInfo.phone
        if let urlStr = userInfo.image, let url = URL.init(string: baseImageURL + urlStr) {
            self.imageURL = url
        } else {
            self.imageURL = nil
        }
        self.icNumber = userInfo.icNumber?.stringValue
        self.icImage = userInfo.icImage
        self.rating = userInfo.rating?.floatValue ?? 0.0
        self.isPhoneVerified = userInfo.isPhoneNoVerify == "1"
        self.isEmailVerified = (userInfo.isEmailVerify == "1") || userInfo.email?.isEmpty == true
    }
    
    func updateProfileViewController(_ pvc: ProfileViewController) {
        pvc.nameLbl.text = name
        pvc.profileImageView.loadTaxiImage(self.imageURL)
        pvc.nameField.textFieldText = name
        pvc.emailField.textFieldText = email
        pvc.phoneField.textFieldText = phone
        pvc.passwordField.textFieldText = password == "" ? "" : "123456"
        pvc.rating.rating = self.rating
        pvc.phoneLabelLayout(self.isPhoneVerified)
        pvc.emailLabelLayout(self.isEmailVerified)
        
        pvc.emailField?.leftActionBtn?.isHidden = self.isEmailVerified
        pvc.icNumberField?.textFieldText = icNumber
        pvc.icImageView.imageView.loadTaxiImage(self.icImage.urlFromString)
        
        if UserDefaults.companyLogin {
            pvc.nameField?.isUserInteractionEnabled = false
            pvc.phoneField?.isUserInteractionEnabled = false
            pvc.emailField?.isUserInteractionEnabled = false
            pvc.passwordField?.isUserInteractionEnabled = false
        }
        //        pvc?.textFieldText = icNumber
    }
}

extension String {
    var decodedStringFromBase64: String {
        let decodedData = Data(base64Encoded: self)!
        return String(data: decodedData, encoding: .utf8)!
    }
}

extension ServiceManager {
    
    func updateProfile(_ info: ProfileReqInfo, completion: @escaping (_ inner: () throws ->  CommonRes?) -> (Void)) {
        
        self.request(endPoint: MenuDataServiceEndpoint.updateProfile, body: info.JSONRepresentation as Dictionary<String, Any>?) { result in
            
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: CommonRes.self)
                completion({ return response })
            case .failure(let error):
                completion({ throw error })
            }
        }
    }
    
    func upload(_ image: UIImage, type: String, completion: @escaping (_ inner: () throws ->  GeneralResponse<FileUploadResponse>?) -> (Void)) {
        let body = ["type": type]
        let media = MediaData(image, data: nil, key: "file", imageName: "\(Date().timeIntervalSince1970)."+"jpeg", mimeType: .jpeg)!
        self.uploadRequest(endPoint: MenuDataServiceEndpoint.upload, body: body, media: [media]) { (result) in
            switch result {
            case .success(let data):
                let response  = data.getDecodedObject(from: GeneralResponse<FileUploadResponse>.self)
                if response?.status == true {
                    completion({ return response })
                } else {
                    completion({ throw CoreError.init(code: 1000, description: response?.message ?? "", innerError: nil, informations: nil) })
                }
            case .failure(let error):
                completion({ throw error })
            }
        } progressHandler: { (progress) in
            
        }
    }
}
