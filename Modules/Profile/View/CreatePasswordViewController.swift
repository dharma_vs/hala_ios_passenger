//
//  CreatePasswordViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit
import Toast_Swift

class CreatePasswordViewController: BaseViewController {

    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var oldPasswordTitleLbl: AppLabel!
    @IBOutlet weak var oldPasswordTextField: AppTextField!
    @IBOutlet weak var oldPasswordView: AppLabel!
    @IBOutlet weak var newPasswordTitleLbl: AppLabel!
    @IBOutlet weak var newPasswordTextField: AppTextField!
    @IBOutlet weak var confirmPasswordTitleLbl: AppLabel!
    @IBOutlet weak var confirmPasswordTextField: AppTextField!
    @IBOutlet weak var updateBtn: AppButton!
    
    var isCreatePassword: Bool = false
    
    var didPasswordUpdated: ((String) -> Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()

        // Do any additional setup after loading the view.
    }
    
    override func setup() {
        super.setup()
        
        self.oldPasswordView.isHidden = self.isCreatePassword
    }
    
    override func localize() {
        super.localize()
        titleLbl.text = isCreatePassword ? "Create Password".localized : "Update Password".localized
        oldPasswordTitleLbl.text = "Old Password".localized.capitalized
        newPasswordTitleLbl.text = "New Password".localized.capitalized
        confirmPasswordTitleLbl.text = "Confirm Password".localized.capitalized
        updateBtn.setTitle("Update".localized, for: .normal)
    }
    
    @IBAction func updateBtnPressed() {
        view.endEditing(true)
        var oldPassword = ""
        if isCreatePassword == false {
            if let password1 = oldPasswordTextField.text {
                oldPassword = password1
            } else {
                view.makeToast("Please enter old password".localized)
                return
            }
        }
        if let password1 = newPasswordTextField.text, !password1.isEmpty, let password2 = confirmPasswordTextField.text, !password2.isEmpty, password1 == password2 {
            self.startActivityIndicatorInWindow()
            var info = ChangePasswordReqInfo()
            info.old_password = isCreatePassword ? "" : oldPassword
            info.new_password = password2
            ServiceManager().changePassword(info) {[weak self] (inner) -> (Void) in
                self?.stopActivityIndicator()
                do {
                    let response = try inner()
                    self?.view.makeToast(response?.message?.stringValue, duration: 2, position: .bottom, title: nil, image: nil, style: ToastStyle(), completion: { [weak self] (success) in
                        if response?.status == nil {
                            self?.didPasswordUpdated?(password1)
                            self?.navigationController?.popViewController(animated: true)
                        }
                    })
                } catch (let e) {
                    
                    if self!.isCreatePassword{
                        self?.view.makeToast("Try again")

                    }else{
                        self?.view.makeToast("Old password does not match")

                    }
                }
            }
        } else {
            view.makeToast("Passwords are not matched".localized)
        }
        
    }
}
