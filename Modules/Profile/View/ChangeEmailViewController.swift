//
//  ChangeEmailViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit
import Toast_Swift

class ChangeEmailViewController: BaseViewController {
    
    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var languageTitle: AppLabel!
    @IBOutlet weak var textField: AppTextField!
    @IBOutlet weak var updateBtn: AppButton!
    
    var email: String!
    var didEmailChange: ((String) -> Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        // Do any additional setup after loading the view.
    }
    
    override func setup() {
        super.setup()
        textField.text = email
    }
    
    override func localize() {
        super.localize()
        titleLbl.text = "Update Email".localized
        languageTitle.text = "Email Address (Optional)".localized.capitalized
        updateBtn.setTitle("Update".localized, for: .normal)
    }
    
    @IBAction func continueClickAction() {
        if let email = textField.text, email.isValidEmail() {
            let body = ["ccp":"", "tel_no":"", "email": email] as AnyObject
            self.startActivityIndicatorInWindow()
            ServiceManager().checkAccount(body) { [weak self](inner) -> (Void) in
                do {
                    let response = try inner()
                    if response?.status == nil {
                        self?.didEmailChange?(email)
                    } else {
                        self?.view.makeToast(response?.message?.stringValue)
                        self?.stopActivityIndicator()
                    }
                } catch {
                    self?.stopActivityIndicator()
                    self?.view.makeToast("\("Failed".localized) \(error.localizedDescription)")
                }
            }
        } else {
            self.view.makeToast("")
        }
    }
}

