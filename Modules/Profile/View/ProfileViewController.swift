//
//  ProfileViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import AVFoundation
import UIKit
import MobileCoreServices
import CropViewController
import Toast_Swift


class ProfileViewController: BaseViewController {
    
    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var profileImageView: AppImageView!
    @IBOutlet weak var nameLbl: AppLabel!
    @IBOutlet weak var nameField: TextFieldWithTitleView!
    @IBOutlet weak var phoneField: TextFieldWithTitleView!
    @IBOutlet weak var emailField: TextFieldWithTitleView!
    @IBOutlet weak var passwordField: TextFieldWithTitleView!
    @IBOutlet weak var icNumberField: TextFieldWithTitleView!
    @IBOutlet weak var icImageView: ImageViewWithTitleView!
    @IBOutlet weak var rating: FloatRatingView!
    
    @IBOutlet weak var viewKYC: UIView!
    @IBOutlet weak var btnProceedkyc: AppButton!
    @IBOutlet weak var lblKYC: AppLabel!
    var didProfileUpdated: (() -> Void)!
    
    var viewModel: ProfileViewModel! {
        didSet {
            viewModel.updateProfileViewController(self)
        }
    }
    
    @IBAction func resendVerification(_ sender: Any) {
        ServiceManager().getProfile { [weak self](inner) -> (Void) in
            self?.view.makeToast("Email Sent".localized.tripleSpaced)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        if let userInfo = CacheManager.riderInfo {
            viewModel = ProfileViewModel(userInfo: userInfo)
        }
        self.emailField.leftActionBtn.setTitle("Resend Verification".localized, for: .normal)
        self.emailField.leftActionBtn.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == String.GoUpdateNameSegue {
            (segue.destination as! UpdateNameViewController).didNameChange = { [weak self] (name) in
                self?.nameField.textFieldText = name
                var info = ProfileReqInfo()
                info.name = name
                info.ccp = ""
                info.tel_no = ""
                info.email = ""
                self?.updateProfile(info)
                self?.navigationController?.popViewController(animated: true)
            }
        } else if segue.identifier == String.GoToChangeEmailFromProfile {
            (segue.destination as! ChangeEmailViewController).email = CacheManager.riderInfo?.email
            (segue.destination as! ChangeEmailViewController).didEmailChange = { [weak self] (email) in
                self?.emailField.textFieldText = email
                var info = ProfileReqInfo()
                info.name = ""
                info.ccp = ""
                info.tel_no = ""
                info.email = email
                self?.updateProfile(info)
                self?.navigationController?.popViewController(animated: true)
            }
        } else if segue.identifier == String.GotoPasswordFromProfile {
            if self.passwordField.textField.text == "" {
            (segue.destination as! CreatePasswordViewController).isCreatePassword = true
            }
            (segue.destination as! CreatePasswordViewController).didPasswordUpdated = { [weak self] (password) in
                self?.passwordField.textFieldText = password
                
                ServiceManager().getProfile({[weak self] (inner) -> (Void) in
                    if let userInfo = CacheManager.riderInfo {
                        self?.viewModel = ProfileViewModel(userInfo: userInfo)
                    }
                })
            }
        } else if segue.identifier == String.GoUpdateNumberSegue {
            (segue.destination as? UpdatePhoneViewController)?.otpVerificationSucceeds = {[weak self] detail in
                self?.navigationController?.popViewController(animated: false)
                var info = ProfileReqInfo()
                info.name = ""
                info.ccp = detail.0
                info.tel_no = detail.1
                info.email = ""
                self?.updateProfile(info)
                self?.navigationController?.popViewController(animated: true)

            }
        }else if segue.identifier == String.GotoKYCSegue {
            (segue.destination as? KYCVC)?.updateKYC = {[weak self] detail in
                self?.navigationController?.popViewController(animated: false)
//                var info = ProfileReqInfo()
//                info.name = nameField.textFieldText
//                info.email = emailField.textFieldText
//                info.tel_no = phoneField.textField
//                info.ccp = ""
//                self?.updateProfile(info)
                self?.navigationController?.popViewController(animated: true)

            }
        }
    }
    
    override func setup() {
        super.setup()
        emailField.didTapOnLeftAction = { [weak self] in
            ServiceManager().getProfile { [weak self](inner) -> (Void) in
                self?.view.makeToast("Email Sent".localized)
            }
        }
        phoneField.textField.isEnabled = false
        passwordField.textField.isSecureTextEntry = true
    }
    
    override func localize() {
        super.localize()
        
        titleLbl.text = "Edit Profile".localized
        nameField.titleLbl.text = "Name".localized
        emailField.titleLbl.text = "Email Address (Optional)".localized
        emailField.textFieldPlaceholder = "Email not available".localized
        passwordField.titleLbl.text = "Password".localized
        icNumberField.titleLbl.text = "I.C Number".localized
        icImageView.titleLbl.text = "I.C Image".localized
        icImageView.titleLbl.textColor = UIColor.colorAccent
        passwordField.textFieldPlaceholder = "Password not available".localized
        lblKYC.text = "KYCnotsubmittedyet".localized
        btnProceedkyc.setTitle("ProceedKYC".localized, for: .normal)
//        self.phoneField?.titleLbl.text = String.version
//        phoneField.titleLbl?.attributedText = "(Verified)".verifiedAttributted
    }
    
    func phoneLabelLayout(_ isPhoneVerified: Bool) {
        let phone = "Phone Number".localized
        let verified = isPhoneVerified ? "Verified".localized : "NotVerified".localized
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(phone) \(verified)")
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.colorTextDark, range: NSMakeRange(0, "\(phone) \(verified)".count))
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: (isPhoneVerified ? UIColor.primary : UIColor.red), range: NSMakeRange(phone.count, verified.count+1))

        phoneField.titleLbl.attributedText = attributeString
    }
    
    func emailLabelLayout(_ isEmailVerified: Bool) {
        let phone = "Email Address1".localized
        let verified = isEmailVerified ? "Verified".localized : "NotVerified".localized
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(phone) \(verified)")
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.colorTextDark, range: NSMakeRange(0, phone.count))
        attributeString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Montserrat-Medium", size: 15) as Any, range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 10), range: NSMakeRange(phone.count, verified.count+1))
        attributeString.addAttribute(NSAttributedString.Key.foregroundColor, value: isEmailVerified ? UIColor.primary : UIColor.red, range: NSMakeRange(phone.count, verified.count+1))
        emailField.titleLbl.attributedText = attributeString
    }
    
    func updateProfile(_ info: ProfileReqInfo) {
        self.startActivityIndicatorInWindow()
        ServiceManager().updateProfile(info) {[weak self] (inner) -> (Void) in
            self?.stopActivityIndicator()
            do {
                let response = try inner()
                self?.view.makeToast(response?.message?.stringValue, duration: 2, position: .bottom, title: nil, image: nil, style: ToastStyle(), completion: {[weak self] (success) in
                    if response?.status?.isSuccess == true {
                        self?.startActivityIndicatorInWindow()
                        ServiceManager().getProfile({[weak self] (inner) -> (Void) in
                            self?.stopActivityIndicator()
                            if let userInfo = CacheManager.riderInfo {
                                self?.viewModel = ProfileViewModel(userInfo: userInfo)
                            }
                            self?.didProfileUpdated?()
                        })
                    }
                })
            } catch(let e) {
                self?.view.makeToast("\("Failed".localized) \(e.localizedDescription)")
            }
        }
    }
    
    override func backPressed() {
        view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
        /*
        if isUserInfoChanged() {
            
            func uploadProfilePicture(_ completion: @escaping (String?) -> (Void)) {
                if profileImageView.image != UIImage(named: "ic_dummy_user") {
                    ServiceManager().upload(profileImageView.image!, type: "riders") { (inner) -> (Void) in
                        do {
                            let response = try inner()
                            if response?.status == true {
                                completion(response?.data?.name!)
                            } else {
                                completion(nil)
                            }
                        } catch(_) {
                            completion(nil)
                        }
                    }
                } else {
                    completion(nil)
                }
            }
            
            var profileInfo = ProfileReqInfo()
            profileInfo.name = nameField.textFieldText
            profileInfo.email = emailField.textFieldText
            profileInfo.tel_no = phoneField.textFieldText
            
            self.startActivityIndicatorInWindow()
            uploadProfilePicture { (name) -> (Void) in
                if let _name = name {
                    profileInfo.image = _name
                }
                ServiceManager().updateProfile(profileInfo) { (inner) -> (Void) in
                    self.stopActivityIndicator()
                    do {
                        let response = try inner()
                        self.view.makeToast(response?.message, duration: 2, position: .bottom, title: nil, image: nil, style: ToastStyle(), completion: { (success) in
                            if response?.status == true {
                                self.startActivityIndicatorInWindow()
                                ServiceManager().getProfile({ (inner) -> (Void) in
                                    self.stopActivityIndicator()
                                    self.didProfileUpdated?()
                                    self.navigationController?.popViewController(animated: true)
                                })
                            }
                        })
                    } catch(let e) {
                        self.view.makeToast("\(String.Failed.localized) \(e.localizedDescription)")
                    }
                }
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        */
    }
    @IBAction func btnclkKYCPorceed(){
        performSegue(withIdentifier: String.GotoKYCSegue, sender: self)
    }
    @IBAction func changeImageBtnPressed() {
        let action = UIAlertController(title: "Pick Image From?".localized, message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Camera".localized, style: .default) { [weak self] (action) in
            self?.showCamera()
        }
        let gallery = UIAlertAction(title: "Gallery".localized, style: .default) { [weak self] (action) in
            self?.showPhotoLibrary()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        action.addAction(camera)
        action.addAction(gallery)
        action.addAction(cancel)
        self.navigationController?.present(action, animated: true, completion: nil)
    }
    
    func showCamera() {
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) == AVAuthorizationStatus.denied {
            UIViewController.showAlertForCameraAccessDenied(inViewController: self)
        } else {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: {[weak self] (granted: Bool) -> Void in
                if granted == true {
                    
                    DispatchQueue.main.async { [weak self] in
                        let imagePicker = UIImagePickerController()
                        imagePicker.sourceType = UIImagePickerController.SourceType.camera
                        imagePicker.allowsEditing = false
                        imagePicker.delegate = self
                        self?.navigationController?.present(imagePicker, animated: true, completion: nil)
                    }
                   
                } else {
                    // User Rejected
                }
            })
        }
    }
    
    func showPhotoLibrary() {
        let imagePicker = UIImagePickerController()
        imagePicker.navigationController?.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        self.navigationController?.present(imagePicker, animated: true, completion: nil)
    }
    
    func isUserInfoChanged() -> Bool {
//        return CacheManager.riderInfo?.name != nameField.textFieldText ||
//        CacheManager.riderInfo?.email != emailField.textFieldText ||
        profileImageView.image != UIImage(named: "ic_dummy_user")
    }
}

extension ProfileViewController: ImageViewWithTitleViewDatasource {
    func presender() -> UINavigationController {
        return navigationController!
    }
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.navigationController?.dismiss(animated: false) {
            if let pickedImage = info[.originalImage] as? UIImage {
                let cropController = CropViewController(image: pickedImage)
                cropController.aspectRatioPreset = .presetOriginal
                cropController.delegate = self
                self.navigationController?.pushViewController(cropController, animated: true)
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension ProfileViewController: CropViewControllerDelegate {
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.profileImageView.image = image
        self.navigationController?.popViewController(animated: true)
        if profileImageView.image != UIImage(named: "ic_dummy_user") {
            self.startActivityIndicatorInWindow()
            ServiceManager().upload(profileImageView.image!, type: "riders") { [weak self](inner) -> (Void) in
                self?.stopActivityIndicator()
                do {
                    let response = try inner()
                    if response?.status == true {
                        var info = ProfileReqInfo()
                        info.image = response?.data?.name
                        info.name = ""
                        info.ccp = ""
                        info.tel_no = ""
                        info.email = ""
                        self?.updateProfile(info)
                    }
                    self?.view.makeToast(response?.message)
                } catch(_) {
                    
                }
            }
        } else {
            
        }
    }
}

extension ProfileViewController: TextFieldWithTitleViewDelegate {
    func textFieldWithTitleContentBtnPressed(_ view: TextFieldWithTitleView) {
        if view == nameField {
            performSegue(withIdentifier: String.GoUpdateNameSegue, sender: self)
        } else if view == emailField {
            performSegue(withIdentifier: String.GoToChangeEmailFromProfile, sender: self)
        } else if view == passwordField {
            performSegue(withIdentifier: String.GotoPasswordFromProfile, sender: self)
        } else if view == phoneField {
            performSegue(withIdentifier: String.GoUpdateNumberSegue, sender: self)
        }
    }
    
    func textFieldWithTitleRequestToShowTableView(_ view: TextFieldWithTitleView) {
        
    }
}

extension String {
    var verifiedAttributted: NSAttributedString {
        let mutableAttributted = NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13, weight: .regular)])
        mutableAttributted.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.green, range: (self as NSString).range(of: "(Verified)", options: .caseInsensitive))
        return mutableAttributted
    }
}
