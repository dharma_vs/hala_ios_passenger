//
//  UpdateNameViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit

class UpdateNameViewController: BaseViewController {
    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var nameLbl: AppLabel!
    @IBOutlet weak var nameTextField: AppTextField!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var updateBg: UIView!
    @IBOutlet weak var updateBtn: AppButton!
    @IBOutlet weak var updateBgGradient: GradientView!
    
    var didNameChange: ((String) -> Void)!
    
    @IBAction func updateName(_ sender: Any) {
        
        guard self.nameTextField.text?.isEmpty == false else {
            self.view.makeToast("Please enter the name".localized)
            return
        }
        self.didNameChange?(self.nameTextField.text!)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        
        nameTextField.text = CacheManager.userInfo?.name
        
        nameLbl.textColor = UIColor.colorAccent
        borderView.backgroundColor = UIColor.colorAccent
        updateBg.backgroundColor = UIColor.gStartColor
        updateBgGradient.gradientLayer.colors = [UIColor.gStartColor.cgColor, UIColor.gEndColor.cgColor]
        
        // Do any additional setup after loading the view.
    }
    
    override func localize() {
        self.titleLbl.text  = "Update Name".localized
        self.nameLbl.text  = "Name".localized.uppercased()
        self.updateBtn.setTitle("Update".localized, for: .normal)
    }
    
}

