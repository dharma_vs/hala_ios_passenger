//
//  KYCVC.swift
//  HalaMobility
//
//  Created by ADMIN on 20/04/22.
//

import UIKit

class KYCVC: BaseViewController {
    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var nameField: TextFieldWithTitleView!
    @IBOutlet weak var phoneField: TextFieldWithTitleView!
    @IBOutlet weak var emailField: TextFieldWithTitleView!
    @IBOutlet weak var btnProceedkyc: AppButton!
    @IBOutlet weak var btnCheckbox: AppButton!
    @IBOutlet weak var lblcheckboxvalu: AppLabel!
    
    var updateKYC:((String) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        // Do any additional setup after loading the view.
    }
    override func localize() {
        super.localize()
        titleLbl.text = "KYCVerification".localized
        nameField.titleLbl.text = "User Name".localized
        phoneField.titleLbl.text = "Phone Number".localized
        emailField.titleLbl.text = "Email Address".localized
        lblcheckboxvalu.text = "Didyoualreadylink".localized
        btnProceedkyc.setTitle("ProceedKYCVerification".localized, for: .normal)
        nameField.textField.text = CacheManager.userInfo?.name
        phoneField.textField.text = CacheManager.userInfo?.phoneNo
        emailField.textField.text = CacheManager.userInfo?.email
    }
    
    @IBAction func btnclkProceedKYC() {
        guard self.nameField.textField.text?.isEmpty == false else {
            self.view.makeToast("Please enter the name".localized)
            return
        }
        guard self.phoneField.textField.text?.isEmpty == false else {
            self.view.makeToast("Please enter the phone number".localized)
            return
        }
        guard self.emailField.textField.text?.isEmpty == false else {
            self.view.makeToast("Please enter the email".localized)
            return
        }
        
        //self.updateKYC?(self.nameField.textField.text!)
//        ServiceManager().getProfile { [weak self](inner) -> (Void) in
//            self?.view.makeToast("Email Sent".localized.tripleSpaced)
//        }
    }
    @IBAction func btnclkCheckbox() {
        if btnCheckbox.isSelected == true{
            btnCheckbox.isSelected = false
        }else{
            btnCheckbox.isSelected = true
        }
    }
}
