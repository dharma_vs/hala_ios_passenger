//
//  UpdatePhoneViewController.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit
import CountryPickerView

class UpdatePhoneViewController: BaseViewController {
    @IBOutlet weak var titleLbl: AppLabel!
    @IBOutlet weak var phoneNumberLbl: AppLabel!
    @IBOutlet weak var phoneTextField: AppTextField!
    @IBOutlet weak var countryCodeBtn: AppButton!
    @IBOutlet weak var countryCodeLbl: AppLabel!
    @IBOutlet weak var borderView: UIView!
   // @IBOutlet weak var updateBg: UIView!
    @IBOutlet weak var updateBtn: AppButton!
    //@IBOutlet weak var updateBgGradient: GradientView!
    
    var otpVerificationSucceeds: (((String, String, String)) -> Void)?
    
    let cpv = CountryPickerView()
    
    @IBAction func updatePhoneNumber(_ sender: Any) {
        
        guard self.phoneTextField.text?.isNumber == true && (self.phoneTextField.text?.count ?? 0) >= 10 else {
            self.view.makeToast("Please enter the phone number".localized)
            return
        }
        let body = ["ccp":"+91", "tel_no":phoneTextField.text!, "email": ""] as AnyObject
        self.startActivityIndicatorInWindow()
        ServiceManager().checkAccount(body) { [weak self](inner) -> (Void) in
            do {
                let response = try inner()
                if response?.status?.isSuccess == true {
                    let phone: String = (self?.phoneTextField.text) ?? ""
                    let code = (self?.countryCodeLbl?.text) ?? ""
                    let body = ["tel_no": phone, "ccp": "+91"] as AnyObject
                    
                    ServiceManager().sendOTP(OTP.self, body: body, completion: { [weak self] (inner) in
                        self?.stopActivityIndicator()
                        do {
                            let response = try inner()
                            if response?.status == true, let otp = response?.data?.otp {
                                let verificationVC = VerificationViewController.initFromStoryBoard(.Authenticate)
                                verificationVC.phoneDetail = ((self?.countryCodeLbl.text?.components(separatedBy: "+").last) ?? "", (self?.phoneTextField.text) ?? "", otp)
                                verificationVC.otpVerificationSucceeds = { [weak self]detail in
                                    self?.otpVerificationSucceeds?(detail)
                                }
                                self?.navigationController?.pushViewController(verificationVC, animated: true)
                                
                            }
                            self?.navigationController?.topViewController?.view!.makeToast(response?.message)
                        } catch (let e) {
                            self?.view.makeToast("\("Failed".localized) \(e.localizedDescription)")
                        }
                    })
                    
                } else {
                    DispatchQueue.main.async { [weak self] in
                        self?.stopActivityIndicator()
                        self?.view.makeToast(response?.message?.stringValue)
                    }
                }
            } catch {
                DispatchQueue.main.async { [weak self] in
                    self?.stopActivityIndicator()
//                    self?.view.makeToast("\(String.Failed.localized) \(error.localizedDescription)")
                }
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToVerificationSegue", let verificationVC = segue.destination as? VerificationViewController {
            verificationVC.phoneDetail = ((self.countryCodeLbl.text?.components(separatedBy: "+").last)!, phoneTextField.text!, "")
        }
    }
    
    @IBAction func changeCountryCode(_ sender: Any) {
        self.phoneTextField.resignFirstResponder()
        cpv.delegate = self
        cpv.dataSource = self
        cpv.showCountryCodeInView = true
        cpv.showPhoneCodeInView = true
        cpv.showCountriesList(from: self)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setTpBackground()
        countryCodeLbl.text = CacheManager.userInfo?.countryCode?.isEmpty == true || CacheManager.userInfo?.countryCode == nil ? defaultCountryCode : CacheManager.userInfo?.countryCode
        phoneTextField.text = CacheManager.userInfo?.phoneNo
        
        phoneNumberLbl.textColor = UIColor.colorAccent
        borderView.backgroundColor = UIColor.colorAccent
     //   updateBg.backgroundColor = AppColors.colorAccent
      //  updateBgGradient.gradientLayer.colors = [AppColors.gStartColor.cgColor, AppColors.gStartColor.cgColor]
        updateBtn.setTitleColor(.white, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    override func localize() {
        self.titleLbl.text  = "Update Phone Number".localized
        self.phoneNumberLbl.text  = "Phone Number".localized.uppercased()
        self.updateBtn.setTitle("Update".localized, for: .normal)
    }
    
}

extension UpdatePhoneViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTextField {
            let text = textField.text?.replacingCharacters(in: Range(range, in: textField.text!)!, with: string)
            let isNumber = text?.isNumber == true
            if isNumber {
                return (text?.count ?? 0) <= 10
            }
        }
        return true
    }
}

extension UpdatePhoneViewController: CountryPickerViewDelegate, CountryPickerViewDataSource {
    
    //  MARK: - CountryPickerViewDelegate
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        countryCodeLbl.text = country.phoneCode
    }
    
    //  MARK: - CountryPickerViewDataSource
    
    func showPhoneCodeInList(in countryPickerView: CountryPickerView) -> Bool {
        return true
    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select a Country".localized
    }
}
