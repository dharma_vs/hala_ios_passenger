//
//  AppButton.swift
//  Taxi
//
//  Created by Admin on 8/4/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

//@IBDesignable
class AppButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable var textThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var backgroundThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var borderThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var imageThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }

    override init(frame : CGRect) {
        super.init(frame : frame)
        setup()
    }
    
    convenience init() {
        self.init(frame:CGRect.zero)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    func setup() {
        self.setTitleColor(ColorTheme.init(rawValue: self.textThemeColor)?.color ?? UIColor.blue, for: UIControl.State())
        if imageThemeColor != -1 {
            let image = self.image(for: .normal)
            self.tintColor = ColorTheme.init(rawValue: self.imageThemeColor)?.color ?? UIColor.blue
            self.setImage(image?.withRenderingMode(.alwaysTemplate), for: .normal)
            self.setImage(image?.withRenderingMode(.alwaysTemplate), for: UIControl.State.highlighted)
            self.setImage(image?.withRenderingMode(.alwaysTemplate), for: UIControl.State())
        }
        if backgroundThemeColor != -1 {
            self.backgroundColor = ColorTheme.init(rawValue: self.backgroundThemeColor)?.color ?? UIColor.clear
        }
        if borderThemeColor != -1 {
            self.layer.borderColor = (ColorTheme.init(rawValue: self.borderThemeColor)?.color ?? UIColor.clear).cgColor
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.cornerRadius = isRounded ? bounds.height/2 : cornerRadius
    }
}
