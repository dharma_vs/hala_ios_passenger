//
//  AppImageView.swift
//  Taxi
//
//  Created by Admin on 8/4/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

class AppImageView: UIImageView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable var imageThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var backgroundThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var borderTheme: Int = -1 {
        didSet {
            setup()
        }
    }
    
    override init(frame : CGRect) {
        super.init(frame : frame)
        setup()
    }
    
    convenience init() {
        self.init(frame:CGRect.zero)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    func setup() {
        if self.imageThemeColor != -1 {
            self.tintColor = ColorTheme.init(rawValue: self.imageThemeColor)?.color ?? UIColor.blue
            self.image = self.image?.withRenderingMode(.alwaysTemplate)
        } else {
            self.image = self.image?.withRenderingMode(.alwaysOriginal)
        }
        
        if self.backgroundThemeColor != -1 {
            self.backgroundColor = (ColorTheme.init(rawValue: self.backgroundThemeColor)?.color ?? UIColor.clear)
        } else {

        }
        
        if self.borderTheme != -1 {
            self.layer.borderColor = (ColorTheme.init(rawValue: self.borderTheme)?.color ?? UIColor.clear).cgColor
        } else {
//            super.image = self.appImage
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.cornerRadius = isRounded ? bounds.height/2 : cornerRadius
    }

}
