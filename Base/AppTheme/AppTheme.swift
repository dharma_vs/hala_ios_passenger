//
//  AppTheme.swift
//  Taxi
//
//  Created by Admin on 7/31/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import Foundation
import UIKit

struct AppColors {
    static let primaryButton = UIColor.init(hex: 0xf9f9f9)
}

extension UIColor {
    static let colorPrimary = UIColor(hex: 0x2EB52E)//green
    static let colorPrimaryDark = UIColor(hex: 0x238E23)//green
    static let colorAccent = UIColor(hex: 0x000000) //black
    static let colorTaxiZone = UIColor(hex: 0x34C534) //green
    static let colorRideZone = UIColor(hex: 0xfab420) //lite yellow map battery bg
    static let colorTaxiZoneFill = UIColor(hex: 0x1A34C534)
    static let colorRideZoneFill = UIColor(hex: 0x1AFAB420)
    static let colorAppAccent = UIColor(hex: 0x2EB52E)
    static let colorAppAccentLight = UIColor(hex: 0x34C534)
    static let colorAppAccentDark = UIColor(hex: 0x2EB52E)
    static let colorAppAccentTrans = UIColor(hex: 0x972EB52E)
    static let colorAppSecondAccent = UIColor(hex: 0xfab420)
    static let colorAppSecondAccentTrans = UIColor(hex: 0x86FAB420)
    static let colorAppSecondAccentDark = UIColor(hex: 0xEAA515)
    static let colorAppGrayAccent = UIColor(hex: 0xCCCCCC)
    static let colorAppGrayTrans = UIColor(hex: 0x86CCCCCC)
    static let colorAppGrayDark = UIColor(hex: 0xDADADA)
    static let colorBackTop = UIColor(hex: 0x33cc33)
    static let colorAccentLight = UIColor(hex: 0x0C33CC33)
    static let colorOfflineBack = UIColor(hex: 0x94FF0000)
    static let colorChooseHint = UIColor(hex: 0x94FF0000)
    static let primaryButtonTextColor = UIColor(hex: 0xFFFFFF)
    static let primaryButton = UIColor(hex: 0x2EB52E)
    static let primaryButtonClick = UIColor(hex: 0x33cc33)
    static let primaryButton1 = UIColor(hex: 0x33CC33)
    static let primaryButtonClick1 = UIColor(hex: 0x2EB52E)
    static let primaryButtonTrans = UIColor(hex: 0x33CC33)
    static let primaryButtonTransClick = UIColor(hex: 0x33CC33)
    static let gStartColor = UIColor(hex: 0x33CC33)
    static let gEndColor = UIColor(hex: 0x2EB52E)
    static let gChartCollectionStartColor = UIColor(hex: 0x33CC33)
    static let gChartCollectionEndColor = UIColor(hex: 0x2EB52E)
    static let gChartCommissionStartColor = UIColor(hex: 0xfab420)
    static let gChartCommissionEndColor = UIColor(hex: 0xEAA515)
    static let colorBtnGreen = UIColor(hex: 0x33CC33)
    static let colorBtnGreenBorder = UIColor(hex: 0x2EB52E)
    static let colorBtnYellow = UIColor(hex: 0xfab420)
    static let colorBtnYellowBorder = UIColor(hex: 0xEAA515)
    static let colorBtnRed = UIColor(hex: 0xFF0000)
    static let colorBtnRedBorder = UIColor(hex: 0xE10000)
    static let warning = UIColor(hex: 0xFF9966)
    static let gCategoryStartColor = UIColor(hex: 0x33cc33)
    static let gCategoryEndColor = UIColor(hex: 0x33cc33)
    static let gStartTransColor = UIColor(hex: 0x33cc33)
    static let gEndTransColor = UIColor(hex: 0xFAB420)
    static let gAppBackStartColor = UIColor(hex: 0xf2f3f4)
    static let gAppBackEndColor = UIColor(hex: 0xf2f3f4)
    static let gStartColorClick = UIColor(hex: 0x33CC33)
    static let gEndColorClick = UIColor(hex: 0x2EB52E)
    static let colorAppBackground = UIColor(hex: 0xFAF8F8)
    static let colorBackBottomLight = UIColor(hex: 0xFAF8F8)
    static let colorBackBottom = UIColor(hex: 0xEFEAEA)
    static let colorRating = UIColor(hex: 0x33CC33)
    static let primarySelectedBack = UIColor(hex: 0x4F33CC33)
    static let full_trans = UIColor(hex: 0x00000000)
    static let colorPopButton = UIColor(hex: 0xdcdcdc)
    static let colorPopButtonBorder = UIColor(hex: 0xf4f4f4)
    static let colorWalletHistoryHeader = UIColor(hex: 0x2ba80d01)
    static let primaryButtonClickDisable = UIColor(hex: 0x8c8c8c)
    static let colorDarkGray_2 = UIColor(hex: 0x484848)
    static let hyperlink_blue = UIColor(hex: 0x0000EE)
    static let colorPopButtonClick = UIColor(hex: 0xbebebe)
    static let colorPopPositiveButton1 = UIColor(hex: 0xfab420)
    static let colorPopPositiveButtonClick1 = UIColor(hex: 0xEAA515)
    static let colorPopPositiveButtonBorder1 = UIColor(hex: 0xEAA515)
    static let colorPopPositiveButton = UIColor(hex: 0x33CC33)
    static let colorPopPositiveButtonClick = UIColor(hex: 0x2EB52E)
    static let colorPopPositiveButtonBorder = UIColor(hex: 0x2EB52E)
    static let colorPopNegativeButton = UIColor(hex: 0x484848)
    static let colorPopNegativeButtonClick = UIColor(hex: 0x383838)
    static let colorPopNegativeButtonBorder = UIColor(hex: 0x484848)
    static let colorDarkGray = UIColor(hex: 0xa1a1a1)
    static let colorDarkGray1 = UIColor(hex: 0x757575)
    static let colorRed = UIColor(hex: 0xff0101)
    static let colorWhite = UIColor(hex: 0xFFFFFF)
    static let colorWhiteHint = UIColor(hex: 0xC3C3C3)
    static let colorBlackHint = UIColor(hex: 0x4C4C4C)
    static let colorLightGray_1 = UIColor(hex: 0xefefef)
    static let colorLightGray_2 = UIColor(hex: 0xd1d1d1)
    static let colorLightGray_3 = UIColor(hex: 0xFCFCFC)
    static let colorLightGray_4 = UIColor(hex: 0xECECEC)
    static let colorLightGray_5 = UIColor(hex: 0xFAFAFA)
    static let colorLightGray = UIColor(hex: 0xF7F7F7)
    static let colorBlack = UIColor(hex: 0x000000)
    static let colorFontText = UIColor(hex: 0x373737)
    static let colorFontTextLight = UIColor(hex: 0x575757)
    static let colorBlack_1 = UIColor(hex: 0x292929)
    static let colorButtonFont = UIColor(hex: 0xFFFFFF)
    static let colorPrimaryButtonFont = UIColor(hex: 0x000000)
    static let colorGreen = UIColor(hex: 0x008000)
    static let colorTextLight = UIColor(hex: 0xA8A8A8)
    static let colorTextLight2 = UIColor(hex: 0x888888)
    static let colorTextDark = UIColor(hex: 0x0A0A0A)
    static let colorTextDefault = UIColor(hex: 0x0C0C0C)
    static let colorTextDate = UIColor(hex: 0xa1a1a1)
    static let colorStatusSearching = UIColor(hex: 0x3F51B5)
    static let colorStatusAccepted = UIColor(hex: 0x009688)
    static let colorStatusArrived = UIColor(hex: 0x2196F3)
    static let colorStatusRunning = UIColor(hex: 0xFF9800)
    static let colorStatusEnd = UIColor(hex: 0x673AB7)
    static let colorStatusCompleted = UIColor(hex: 0x4CAF50)
    static let colorStatusCancelled = UIColor(hex: 0xF44336)
    static let colorStatusNoDriver = UIColor(hex: 0xE91E63)
    static let colorStatusPaused = UIColor(hex: 0xFFC107)
    static let colorDeliveryStatusCreated = UIColor(hex: 0x3F51B5)
    static let colorDeliveryStatusAssigned = UIColor(hex: 0x009688)
    static let colorDeliveryStatusPickedUp = UIColor(hex: 0x2196F3)
    static let colorDeliveryStatusStarted = UIColor(hex: 0x00BCD4)
    static let colorDeliveryStatusReadyToDeliver = UIColor(hex: 0x114A7D)
    static let colorDeliveryStatusDelivered = UIColor(hex: 0x4CAF50)
    static let colorDeliveryStatusUnableToDelivery = UIColor(hex: 0xF44336)
    static let colorDeliveryStatusReturned = UIColor(hex: 0xE91E63)
    static let colorDeliveryStatusCancelled = UIColor(hex: 0xa80d01)
    static let colorDeliveryStatusOther = UIColor(hex: 0x3F51B5)
    static let colorSupportButtonBackClick = UIColor(hex: 0x8033CC33)
    static let colorSupportButtonBack = UIColor(hex: 0x8033CC33)
    static let colorSupportButtonBorder = UIColor(hex: 0x33CC33)
    static let colorWalletRechargeHeader = UIColor(hex: 0x8033CC33)
    static let colorWalletWithDrawHeader = UIColor(hex: 0x2ba80d01)
    static let colorDivider = UIColor(hex: 0xC4C4C4)
    static let colorWalletRecharge = UIColor(hex: 0x0b9e00)
    static let colorWalletWithDraw = UIColor(hex: 0xa80d01)
    static let transparent = UIColor(hex: 0x00000000)
    static let colorOnline = UIColor(hex: 0x4CAF50)
    static let colorOffline = UIColor(hex: 0xF44336)
    static let colorOnTrip = UIColor(hex: 0xFF9800)
    static let colorNew = UIColor(hex: 0x0000FF)
    static let colorPending = UIColor(hex: 0xFFFF00)
    static let colorRejected = UIColor(hex: 0xE91E63)
    static let colorDeactivate = UIColor(hex: 0x494949)
    static let colorChatBack = UIColor(hex: 0x03a80d01)
    static let colorMessageDate = UIColor(hex: 0xacacac)
    static let colorMyMessageBack = UIColor(hex: 0xFFFFFF)
    static let colorOtherMessageBack = UIColor(hex: 0xd033CC33)
    static let gDisableColor = UIColor(hex: 0x8f8f8f)
    static let colorDarkBlackTrans = UIColor(hex: 0x82000000)
    static let colorDarkGray_1 = UIColor(hex: 0x787878)
    static let colorWhiteDull_1 = UIColor(hex: 0xefefef)
    static let colorBlackFont = UIColor(hex: 0x444444)
    static let cal_text_color = UIColor(hex: 0x707070)
    static let cal_text_selected_color = UIColor(hex: 0xFFFFFF)
    static let colorTrans = UIColor(hex: 0x19000000)
    static let colorMenuSelection = UIColor(hex: 0x19000000)
    static let colorMenuUnselection = UIColor(hex: 0x00000000)
    static let colorAppBack = UIColor(hex: 0xf2f3f4)
    static let colorReserveBack = UIColor(hex: 0xF4F4F4)
    static let colorReserveBackBorder = UIColor(hex: 0x19000000)
    static let colorWhiteButtonBorder = UIColor(hex: 0xFFFFFF)
    static let colorWhiteButtonBack1 = UIColor(hex: 0xe2ffffff)
    static let colorOnlineButton = UIColor(hex: 0x006400)
    static let colorOfflineButton = UIColor(hex: 0x8B0000)
    static let colorTextOfflineButton = UIColor(hex: 0xFFFFFF)
    static let colorTextOnlineButton = UIColor(hex: 0xFFFFFF)
    static let default_calendar_background_color = UIColor(hex: 0x33cc33)
    static let default_day_back = UIColor(hex: 0xe6e6e6)
    static let default_month_text_color = UIColor(hex: 0x595f66)
    static let default_other_day_text_color = UIColor(hex: 0xc9cacd)
    static let default_day_text_color = UIColor(hex: 0x4a4a4a)
    static let default_weekend_day_text_color = UIColor(hex: 0x33cc33)
    static let default_week_day_title_text_color = UIColor(hex: 0x91959b)
    static let default_selected_day_text_color = UIColor(hex: 0x33cc33)
    static let default_selected_day_background_color = UIColor(hex: 0x33cc33)
    static let default_selected_day_background_start_color = UIColor(hex: 0x33cc33)
    static let default_selected_day_background_end_color = UIColor(hex: 0x33cc33)
    static let default_disabled_day_text_color = UIColor(hex: 0xc9cacd)
    static let default_border_color = UIColor(hex: 0xececec)
    static let default_selection_bar_month_title_text_color = UIColor(hex: 0xc9cacd)
    static let colorBtnBluetooth = UIColor(hex: 0x287AA9)
    static let colorBtnLock = UIColor(hex: 0x2EB52E)
    static let colorBtnUnlock = UIColor(hex: 0xFF0000)
    static let colorBtnFind = UIColor(hex: 0x000000)
    static let colorBtnStation = UIColor(hex: 0x238E23)
    static let colorBtnDirection = UIColor(hex: 0x0000FF)
    static let colorBtnSwap = UIColor(hex: 0xE91E63)
    static let colorBtnPause = UIColor(hex: 0xFF9800)
    static let colorGoodBattery = UIColor(hex: 0x2EB52E)
    static let colorGoodBatteryLight = UIColor(hex: 0xD5FFD5)
    static let colorBadBattery = UIColor(hex: 0xCD0000)
    static let colorBadBatteryLight = UIColor(hex: 0xFFE1E1)
    static let colorClusterDriver = UIColor(hex: 0x238E23)
    static let colorClusterVehicle = UIColor(hex: 0x238E23)
    static let colorClusterPartner = UIColor(hex: 0x000000)
    static let colorClusterOperator = UIColor(hex: 0xFF0000)
    static let colorClusterStation = UIColor(hex: 0x0000FF)
    static let colorClusterParking = UIColor(hex: 0x0000FF)
    static let colorClusterCharging = UIColor(hex: 0xFAB420)
    static let colorbatteryCharge = UIColor(hex: 0xd4fed5) // lite green vehicle list popup battery
    static let colorbatteryChargelow = UIColor(hex: 0xFAE0E1) // lite red vehicle list popup battery low

    
    
}

enum ColorTheme: Int, CaseIterable {
    case colorPrimary = 1
    case colorPrimaryDark = 2
    case colorAccent = 3 // black
    case colorTaxiZone = 4
    case colorRideZone = 5
    case colorTaxiZoneFill = 6
    case colorRideZoneFill = 7
    case colorAppAccent = 8
    case colorAppAccentLight = 9
    case colorAppAccentDark = 10
    case colorAppAccentTrans = 11
    case colorAppSecondAccent = 12
    case colorAppSecondAccentTrans = 13
    case colorAppSecondAccentDark = 14
    case colorAppGrayAccent = 15
    case colorAppGrayTrans = 16
    case colorAppGrayDark = 17
    case colorBackTop = 18
    case colorAccentLight = 19
    case colorOfflineBack = 20
    case colorChooseHint = 21
    case primaryButtonTextColor = 22 //white
    case primaryButton = 23
    case primaryButtonClick = 24
    case primaryButton1 = 25
    case primaryButtonClick1 = 26
    case primaryButtonTrans = 27
    case primaryButtonTransClick = 28
    case gStartColor = 29
    case gEndColor = 30
    case gChartCollectionStartColor = 31
    case gChartCollectionEndColor = 32
    case gChartCommissionStartColor = 33
    case gChartCommissionEndColor = 34
    case colorBtnGreen = 35
    case colorBtnGreenBorder = 36
    case colorBtnYellow = 37 //light yellow
    case colorBtnYellowBorder = 38
    case colorBtnRed = 39
    case colorBtnRedBorder = 40
    case warning = 41
    case gCategoryStartColor = 42
    case gCategoryEndColor = 43
    case gStartTransColor = 44
    case gEndTransColor = 45
    case gAppBackStartColor = 46
    case gAppBackEndColor = 47
    case gStartColorClick = 48
    case gEndColorClick = 49
    case colorAppBackground = 50
    case colorBackBottomLight = 51
    case colorBackBottom = 52
    case colorRating = 53
    case primarySelectedBack = 54
    case full_trans = 55
    case colorPopButton = 56
    case colorPopButtonBorder = 57
    case colorWalletHistoryHeader = 58
    case primaryButtonClickDisable = 59
    case colorDarkGray_2 = 60
    case hyperlink_blue = 61
    case colorPopButtonClick = 62
    case colorPopPositiveButton1 = 63
    case colorPopPositiveButtonClick1 = 64
    case colorPopPositiveButtonBorder1 = 65
    case colorPopPositiveButton = 66
    case colorPopPositiveButtonClick = 67
    case colorPopPositiveButtonBorder = 68
    case colorPopNegativeButton = 69
    case colorPopNegativeButtonClick = 70
    case colorPopNegativeButtonBorder = 71
    case colorDarkGray = 72
    case colorDarkGray1 = 73
    case colorRed = 74
    case colorWhite = 75
    case colorWhiteHint = 76
    case colorBlackHint = 77
    case colorLightGray_1 = 78
    case colorLightGray_2 = 79
    case colorLightGray_3 = 80
    case colorLightGray_4 = 81
    case colorLightGray_5 = 82
    case colorLightGray = 83
    case colorBlack = 84
    case colorFontText = 85
    case colorFontTextLight = 86
    case colorBlack_1 = 87
    case colorButtonFont = 88
    case colorPrimaryButtonFont = 89
    case colorGreen = 90
    case colorTextLight = 91
    case colorTextLight2 = 92
    case colorTextDark = 93 // black
    case colorTextDefault = 94
    case colorTextDate = 95
    case colorStatusSearching = 96
    case colorStatusAccepted = 97
    case colorStatusArrived = 98
    case colorStatusRunning = 99
    case colorStatusEnd = 100
    case colorStatusCompleted = 101
    case colorStatusCancelled = 102
    case colorStatusNoDriver = 103
    case colorStatusPaused = 104
    case colorDeliveryStatusCreated = 105
    case colorDeliveryStatusAssigned = 106
    case colorDeliveryStatusPickedUp = 107
    case colorDeliveryStatusStarted = 108
    case colorDeliveryStatusReadyToDeliver = 109
    case colorDeliveryStatusDelivered = 110
    case colorDeliveryStatusUnableToDelivery = 111
    case colorDeliveryStatusReturned = 112
    case colorDeliveryStatusCancelled = 113
    case colorDeliveryStatusOther = 114
    case colorSupportButtonBackClick = 115
    case colorSupportButtonBack = 116
    case colorSupportButtonBorder = 117
    case colorWalletRechargeHeader = 118
    case colorWalletWithDrawHeader = 119
    case colorDivider = 120
    case colorWalletRecharge = 121
    case colorWalletWithDraw = 122
    case transparent = 123
    case colorOnline = 124
    case colorOffline = 125
    case colorOnTrip = 126
    case colorNew = 127
    case colorPending = 128
    case colorRejected = 129
    case colorDeactivate = 130
    case colorChatBack = 131
    case colorMessageDate = 132
    case colorMyMessageBack = 133
    case colorOtherMessageBack = 134
    case gDisableColor = 135
    case colorDarkBlackTrans = 136
    case colorDarkGray_1 = 137
    case colorWhiteDull_1 = 138
    case colorBlackFont = 139
    case cal_text_color = 140
    case cal_text_selected_color = 141
    case colorTrans = 142
    case colorMenuSelection = 143
    case colorMenuUnselection = 144
    case colorAppBack = 145
    case colorReserveBack = 146
    case colorReserveBackBorder = 147
    case colorWhiteButtonBorder = 148
    case colorWhiteButtonBack1 = 149
    case colorOnlineButton = 150
    case colorOfflineButton = 151
    case colorTextOfflineButton = 152
    case colorTextOnlineButton = 153
    case default_calendar_background_color = 154
    case default_day_back = 155
    case default_month_text_color = 156
    case default_other_day_text_color = 157
    case default_day_text_color = 158
    case default_weekend_day_text_color = 159
    case default_week_day_title_text_color = 160
    case default_selected_day_text_color = 161
    case default_selected_day_background_color = 162
    case default_selected_day_background_start_color = 163
    case default_selected_day_background_end_color = 164
    case default_disabled_day_text_color = 165
    case default_border_color = 166
    case default_selection_bar_month_title_text_color = 167
    case colorBtnBluetooth = 168
    case colorBtnLock = 169
    case colorBtnUnlock = 170
    case colorBtnFind = 171 // logout dark color no
    case colorBtnStation = 172
    case colorBtnDirection = 173
    case colorBtnSwap = 174
    case colorBtnPause = 175
    case colorGoodBattery = 176
    case colorGoodBatteryLight = 177
    case colorBadBattery = 178
    case colorBadBatteryLight = 179
    case colorClusterDriver = 180
    case colorClusterVehicle = 181
    case colorClusterPartner = 182
    case colorClusterOperator = 183
    case colorClusterStation = 184 // dark blue
    case colorClusterParking = 185
    case colorClusterCharging = 186
    case colorbatteryCharge = 189
    case colorbatteryChargelow = 190 //light red

    var color: UIColor {
        switch self {
        case .colorPrimary: return .colorPrimary
        case .colorPrimaryDark: return .colorPrimaryDark
        case .colorAccent: return .colorAccent
        case .colorTaxiZone: return .colorTaxiZone
        case .colorRideZone: return .colorRideZone
        case .colorTaxiZoneFill: return .colorTaxiZoneFill
        case .colorRideZoneFill: return .colorRideZoneFill
        case .colorAppAccent: return .colorAppAccent
        case .colorAppAccentLight: return .colorAppAccentLight
        case .colorAppAccentDark: return .colorAppAccentDark
        case .colorAppAccentTrans: return .colorAppAccentTrans
        case .colorAppSecondAccent: return .colorAppSecondAccent
        case .colorAppSecondAccentTrans: return .colorAppSecondAccentTrans
        case .colorAppSecondAccentDark: return .colorAppSecondAccentDark
        case .colorAppGrayAccent: return .colorAppGrayAccent
        case .colorAppGrayTrans: return .colorAppGrayTrans
        case .colorAppGrayDark: return .colorAppGrayDark
        case .colorBackTop: return .colorBackTop
        case .colorAccentLight: return .colorAccentLight
        case .colorOfflineBack: return .colorOfflineBack
        case .colorChooseHint: return .colorChooseHint
        case .primaryButtonTextColor: return .primaryButtonTextColor
        case .primaryButton: return .primaryButton
        case .primaryButtonClick: return .primaryButtonClick
        case .primaryButton1: return .primaryButton1
        case .primaryButtonClick1: return .primaryButtonClick1
        case .primaryButtonTrans: return .primaryButtonTrans
        case .primaryButtonTransClick: return .primaryButtonTransClick
        case .gStartColor: return .gStartColor
        case .gEndColor: return .gEndColor
        case .gChartCollectionStartColor: return .gChartCollectionStartColor
        case .gChartCollectionEndColor: return .gChartCollectionEndColor
        case .gChartCommissionStartColor: return .gChartCommissionStartColor
        case .gChartCommissionEndColor: return .gChartCommissionEndColor
        case .colorBtnGreen: return .colorBtnGreen
        case .colorBtnGreenBorder: return .colorBtnGreenBorder
        case .colorBtnYellow: return .colorBtnYellow
        case .colorBtnYellowBorder: return .colorBtnYellowBorder
        case .colorBtnRed: return .colorBtnRed
        case .colorBtnRedBorder: return .colorBtnRedBorder
        case .warning: return .warning
        case .gCategoryStartColor: return .gCategoryStartColor
        case .gCategoryEndColor: return .gCategoryEndColor
        case .gStartTransColor: return .gStartTransColor
        case .gEndTransColor: return .gEndTransColor
        case .gAppBackStartColor: return .gAppBackStartColor
        case .gAppBackEndColor: return .gAppBackEndColor
        case .gStartColorClick: return .gStartColorClick
        case .gEndColorClick: return .gEndColorClick
        case .colorAppBackground: return .colorAppBackground
        case .colorBackBottomLight: return .colorBackBottomLight
        case .colorBackBottom: return .colorBackBottom
        case .colorRating: return .colorRating
        case .primarySelectedBack: return .primarySelectedBack
        case .full_trans: return .full_trans
        case .colorPopButton: return .colorPopButton
        case .colorPopButtonBorder: return .colorPopButtonBorder
        case .colorWalletHistoryHeader: return .colorWalletHistoryHeader
        case .primaryButtonClickDisable: return .primaryButtonClickDisable
        case .colorDarkGray_2: return .colorDarkGray_2
        case .hyperlink_blue: return .hyperlink_blue
        case .colorPopButtonClick: return .colorPopButtonClick
        case .colorPopPositiveButton1: return .colorPopPositiveButton1
        case .colorPopPositiveButtonClick1: return .colorPopPositiveButtonClick1
        case .colorPopPositiveButtonBorder1: return .colorPopPositiveButtonBorder1
        case .colorPopPositiveButton: return .colorPopPositiveButton
        case .colorPopPositiveButtonClick: return .colorPopPositiveButtonClick
        case .colorPopPositiveButtonBorder: return .colorPopPositiveButtonBorder
        case .colorPopNegativeButton: return .colorPopNegativeButton
        case .colorPopNegativeButtonClick: return .colorPopNegativeButtonClick
        case .colorPopNegativeButtonBorder: return .colorPopNegativeButtonBorder
        case .colorDarkGray: return .colorDarkGray
        case .colorDarkGray1: return .colorDarkGray1
        case .colorRed: return .colorRed
        case .colorWhite: return .colorWhite
        case .colorWhiteHint: return .colorWhiteHint
        case .colorBlackHint: return .colorBlackHint
        case .colorLightGray_1: return .colorLightGray_1
        case .colorLightGray_2: return .colorLightGray_2
        case .colorLightGray_3: return .colorLightGray_3
        case .colorLightGray_4: return .colorLightGray_4
        case .colorLightGray_5: return .colorLightGray_5
        case .colorLightGray: return .colorLightGray
        case .colorBlack: return .colorBlack
        case .colorFontText: return .colorFontText
        case .colorFontTextLight: return .colorFontTextLight
        case .colorBlack_1: return .colorBlack_1
        case .colorButtonFont: return .colorButtonFont
        case .colorPrimaryButtonFont: return .colorPrimaryButtonFont
        case .colorGreen: return .colorGreen
        case .colorTextLight: return .colorTextLight
        case .colorTextLight2: return .colorTextLight2
        case .colorTextDark: return .colorTextDark
        case .colorTextDefault: return .colorTextDefault
        case .colorTextDate: return .colorTextDate
        case .colorStatusSearching: return .colorStatusSearching
        case .colorStatusAccepted: return .colorStatusAccepted
        case .colorStatusArrived: return .colorStatusArrived
        case .colorStatusRunning: return .colorStatusRunning
        case .colorStatusEnd: return .colorStatusEnd
        case .colorStatusCompleted: return .colorStatusCompleted
        case .colorStatusCancelled: return .colorStatusCancelled
        case .colorStatusNoDriver: return .colorStatusNoDriver
        case .colorStatusPaused: return .colorStatusPaused
        case .colorDeliveryStatusCreated: return .colorDeliveryStatusCreated
        case .colorDeliveryStatusAssigned: return .colorDeliveryStatusAssigned
        case .colorDeliveryStatusPickedUp: return .colorDeliveryStatusPickedUp
        case .colorDeliveryStatusStarted: return .colorDeliveryStatusStarted
        case .colorDeliveryStatusReadyToDeliver: return .colorDeliveryStatusReadyToDeliver
        case .colorDeliveryStatusDelivered: return .colorDeliveryStatusDelivered
        case .colorDeliveryStatusUnableToDelivery: return .colorDeliveryStatusUnableToDelivery
        case .colorDeliveryStatusReturned: return .colorDeliveryStatusReturned
        case .colorDeliveryStatusCancelled: return .colorDeliveryStatusCancelled
        case .colorDeliveryStatusOther: return .colorDeliveryStatusOther
        case .colorSupportButtonBackClick: return .colorSupportButtonBackClick
        case .colorSupportButtonBack: return .colorSupportButtonBack
        case .colorSupportButtonBorder: return .colorSupportButtonBorder
        case .colorWalletRechargeHeader: return .colorWalletRechargeHeader
        case .colorWalletWithDrawHeader: return .colorWalletWithDrawHeader
        case .colorDivider: return .colorDivider
        case .colorWalletRecharge: return .colorWalletRecharge
        case .colorWalletWithDraw: return .colorWalletWithDraw
        case .transparent: return .transparent
        case .colorOnline: return .colorOnline
        case .colorOffline: return .colorOffline
        case .colorOnTrip: return .colorOnTrip
        case .colorNew: return .colorNew
        case .colorPending: return .colorPending
        case .colorRejected: return .colorRejected
        case .colorDeactivate: return .colorDeactivate
        case .colorChatBack: return .colorChatBack
        case .colorMessageDate: return .colorMessageDate
        case .colorMyMessageBack: return .colorMyMessageBack
        case .colorOtherMessageBack: return .colorOtherMessageBack
        case .gDisableColor: return .gDisableColor
        case .colorDarkBlackTrans: return .colorDarkBlackTrans
        case .colorDarkGray_1: return .colorDarkGray_1
        case .colorWhiteDull_1: return .colorWhiteDull_1
        case .colorBlackFont: return .colorBlackFont
        case .cal_text_color: return .cal_text_color
        case .cal_text_selected_color: return .cal_text_selected_color
        case .colorTrans: return .colorTrans
        case .colorMenuSelection: return .colorMenuSelection
        case .colorMenuUnselection: return .colorMenuUnselection
        case .colorAppBack: return .colorAppBack
        case .colorReserveBack: return .colorReserveBack
        case .colorReserveBackBorder: return .colorReserveBackBorder
        case .colorWhiteButtonBorder: return .colorWhiteButtonBorder
        case .colorWhiteButtonBack1: return .colorWhiteButtonBack1
        case .colorOnlineButton: return .colorOnlineButton
        case .colorOfflineButton: return .colorOfflineButton
        case .colorTextOfflineButton: return .colorTextOfflineButton
        case .colorTextOnlineButton: return .colorTextOnlineButton
        case .default_calendar_background_color: return .default_calendar_background_color
        case .default_day_back: return .default_day_back
        case .default_month_text_color: return .default_month_text_color
        case .default_other_day_text_color: return .default_other_day_text_color
        case .default_day_text_color: return .default_day_text_color
        case .default_weekend_day_text_color: return .default_weekend_day_text_color
        case .default_week_day_title_text_color: return .default_week_day_title_text_color
        case .default_selected_day_text_color: return .default_selected_day_text_color
        case .default_selected_day_background_color: return .default_selected_day_background_color
        case .default_selected_day_background_start_color: return .default_selected_day_background_start_color
        case .default_selected_day_background_end_color: return .default_selected_day_background_end_color
        case .default_disabled_day_text_color: return .default_disabled_day_text_color
        case .default_border_color: return .default_border_color
        case .default_selection_bar_month_title_text_color: return .default_selection_bar_month_title_text_color
        case .colorBtnBluetooth: return .colorBtnBluetooth
        case .colorBtnLock: return .colorBtnLock
        case .colorBtnUnlock: return .colorBtnUnlock
        case .colorBtnFind: return .colorBtnFind
        case .colorBtnStation: return .colorBtnStation
        case .colorBtnDirection: return .colorBtnDirection
        case .colorBtnSwap: return .colorBtnSwap
        case .colorBtnPause: return .colorBtnPause
        case .colorGoodBattery: return .colorGoodBattery
        case .colorGoodBatteryLight: return .colorGoodBatteryLight
        case .colorBadBattery: return .colorBadBattery
        case .colorBadBatteryLight: return .colorBadBatteryLight
        case .colorClusterDriver: return .colorClusterDriver
        case .colorClusterVehicle: return .colorClusterVehicle
        case .colorClusterPartner: return .colorClusterPartner
        case .colorClusterOperator: return .colorClusterOperator
        case .colorClusterStation: return .colorClusterStation
        case .colorClusterParking: return .colorClusterParking
        case .colorClusterCharging: return .colorClusterCharging
        case .colorbatteryCharge: return .colorbatteryCharge
        case .colorbatteryChargelow:return .colorbatteryChargelow
        }
    }
    
    static func printThemeCodes() {
        ColorTheme.allCases.forEach {
            print("\($0) = \($0.rawValue)")
        }
    }
}

struct Theme {
    var background: UIColor
    var text: UIColor
    var tint: UIColor
    
    init(background: UIColor = UIColor.clear, text: UIColor = AppColors.primaryButton, tint: UIColor = UIColor.blue) {
        self.background = background
        self.text = text
        self.tint = tint
    }
}

extension UIColor {
    convenience init(hex: Int64, alpha: CGFloat = 1.0) {
        let red   = CGFloat((hex & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((hex & 0xFF00) >> 8) / 255.0
        let blue  = CGFloat((hex & 0xFF)) / 255.0
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    struct Color {
        static let Tamil: UIColor = .init(hex: 0xFF0000)

        static let darkBlack: UIColor = .init(hex: 0x1F1F21)
        static let lightGray: UIColor = .init(hex: 0xc3c3ca)
        static let darkGray: UIColor  = .init(hex: 0x878787)
        static let red: UIColor       = .init(hex: 0xff3b30)
        static let orange: UIColor    = .init(hex: 0xff9500)
        static let green: UIColor     = .init(hex: 0x4cd964)
        static let blue: UIColor      = .init(hex: 0x007aff)
        static let purple: UIColor    = .init(hex: 0x5856d6)
        static let yellow: UIColor    = .init(hex: 0xffcc00)
        static let tealBlue: UIColor  = .init(hex: 0x5ac8fa)
        static let pink: UIColor      = .init(hex: 0xff2d55)
    }
    
}

extension UIColor {
    
    // Get color from HEXA code
    func hexColor(_ hexcode: String) -> UIColor {
        var cString:String = hexcode.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension UIColor {
    
    static let primary = #colorLiteral(red: 0.1803921569, green: 0.7098039216, blue: 0.1803921569, alpha: 1)
    static let primaryDark = #colorLiteral(red: 0.137254902, green: 0.5568627451, blue: 0.137254902, alpha: 1)
    static let btnYellow = #colorLiteral(red: 0.9803921569, green: 0.7058823529, blue: 0.1254901961, alpha: 1)
    
    //Login Screen
}
