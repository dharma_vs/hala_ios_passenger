//
//  AppTextField.swift
//  Taxi
//
//  Created by Admin on 8/4/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

//@IBDesignable
class AppTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable var textThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var backgroundThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var borderThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }
    
    override init(frame : CGRect) {
        super.init(frame : frame)
        setup()
    }
    
    @IBInspectable var cursorThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var placeHolderThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }
    
    convenience init() {
        self.init(frame:CGRect.zero)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    func setup() {
        let color = self.textColor
        self.textColor = ColorTheme.init(rawValue: self.textThemeColor)?.color ?? color
        if cursorThemeColor != -1 {
            let tint = self.tintColor
            self.tintColor = ColorTheme.init(rawValue: self.cursorThemeColor)?.color ?? tint
        }
        if placeHolderThemeColor != -1 {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: ColorTheme.init(rawValue: self.placeHolderThemeColor)?.color ?? .lightGray])
        }
        if backgroundThemeColor != -1 {
            self.backgroundColor = ColorTheme.init(rawValue: self.backgroundThemeColor)?.color ?? UIColor.clear
        }
        if borderThemeColor != -1 {
            self.layer.borderColor = (ColorTheme.init(rawValue: self.borderThemeColor)?.color ?? UIColor.clear).cgColor
        }
    }
    
    var tintedClearImage: UIImage?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.tintClearImage()
        self.cornerRadius = isRounded ? bounds.height/2 : cornerRadius
    }
    
    private func tintClearImage() {
        for view in subviews {
            if view is UIButton {
                let button = view as! UIButton
                if let image = button.image(for: .highlighted) {
                    if self.tintedClearImage == nil {
                        tintedClearImage = self.tintImage(image: image, color: self.tintColor)
                    }
                    button.setImage(self.tintedClearImage, for: .normal)
                    button.setImage(self.tintedClearImage, for: .highlighted)
                }
            }
        }
    }
    
    private func tintImage(image: UIImage, color: UIColor) -> UIImage {
        let size = image.size
        
        UIGraphicsBeginImageContextWithOptions(size, false, image.scale)
        let context = UIGraphicsGetCurrentContext()
        image.draw(at: .zero, blendMode: CGBlendMode.normal, alpha: 1.0)
        
        context?.setFillColor(color.cgColor)
        context?.setBlendMode(CGBlendMode.sourceIn)
        context?.setAlpha(1.0)
        
        let rect = CGRect(x: CGPoint.zero.x, y: CGPoint.zero.y, width: image.size.width, height: image.size.height)
        UIGraphicsGetCurrentContext()?.fill(rect)
        let tintedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return tintedImage ?? UIImage()
    }
    
    override var placeholder: String? {
        set {
            self.placeHolerString = newValue
        }
        get {
            return self.placeHolerString
        }
    }
    
    var placeHolerString: String? {
        didSet {
            if placeHolderThemeColor != -1 {
                self.attributedPlaceholder = NSAttributedString(string:self.placeHolerString != nil ? self.placeHolerString! : "", attributes:[NSAttributedString.Key.foregroundColor: ColorTheme.init(rawValue: self.placeHolderThemeColor)?.color ?? .lightGray])
            }
        }
    }
    
    open var errorMessage: String?
    
    var didTapOnLeftAction: (()->Void)?
    
    @IBAction func leftAction(_ sender: Any) {
        self.didTapOnLeftAction?()
    }
    
    @IBInspectable var textFieldText: String? 
}
