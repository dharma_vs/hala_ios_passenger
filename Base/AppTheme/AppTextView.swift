//
//  AppTextView.swift
//  Taxi
//
//  Created by Admin on 8/4/19.
//  Copyright © 2019 Virtue Sense. All rights reserved.
//

import UIKit

//@IBDesignable
class AppTextView: UITextView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

//    override init(frame : CGRect) {
//        super.init(frame : frame)
//        setup()
//    }
    
//    convenience init() {
//        self.init(frame:CGRect.zero)
//        setup()
//    }
    
    @IBInspectable var textThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var backgroundThemeColor: Int = -1 {
        didSet {
            setup()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    func setup() {
        self.textColor = ColorTheme.init(rawValue: self.textThemeColor)?.color ?? UIColor.black
//        self.backgroundColor = ColorTheme.init(rawValue: self.backgroundThemeColor)?.color ?? UIColor.white
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.cornerRadius = isRounded ? bounds.height/2 : cornerRadius
    }
}
