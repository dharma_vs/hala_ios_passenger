//
//  AppDelegate.swift
//  Base
//
//  Created by Admin on 02/01/22.
//

import UIKit
import CoreData
import GoogleMaps
import GoogleSignIn
import Firebase
import FirebaseMessaging
import UserNotifications
import IQKeyboardManagerSwift
import FBSDKCoreKit
import PubNub
import Toast_Swift
import Contacts
import TrueSDK

@main
class AppDelegate: MainAppDelegate {

    var window: UIWindow?
    var client: PubNub!

    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        self.enableGoogleSignIn = true
        self.enableFacebookSignIn = false
        Messaging.messaging().delegate = self
        
        
        if TCTrueSDK.sharedManager().isSupported() {
            TCTrueSDK.sharedManager().setup(withAppKey: "1PihD1f57261386af4d6dadf953311b481a78", appLink: "https://sic243f792559c48078351eadc924e0927.truecallerdevs.com")
        }
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        GMSServices.provideAPIKey("AIzaSyDfiJ1VuzBnT-2Mvr1cHNaSiJx9YHyx-fo")
        IQKeyboardManager.shared.enable = true
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
                
        for family in UIFont.familyNames {
            print( "Font Family: \(family)")

            for name in UIFont.fontNames(forFamilyName: family) {
                print("Font Name: \(name)")
            }
        }
        
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Base")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    override func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return super.application(app, open: url, options: options)
    }
    
    override func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return super.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    override func applicationWillResignActive(_ application: UIApplication) {
        super.applicationWillResignActive(application)
    }
    
    override func applicationDidEnterBackground(_ application: UIApplication) {
        super.applicationDidEnterBackground(application)
    }
    
    override func applicationWillEnterForeground(_ application: UIApplication) {
        super.applicationWillEnterForeground(application)
        NotificationCenter.default.post(name: NSNotification.Name.init("DID_RECEIVE_JOB_UPDATE"), object: nil)
    }
    
    override func applicationDidBecomeActive(_ application: UIApplication) {
        super.applicationDidBecomeActive(application)
    }
    
    override func applicationWillTerminate(_ application: UIApplication) {
        super.applicationWillTerminate(application)
    }
}

var channelName = "maps-channel"

extension AppDelegate: PNEventsListener {
    
    func initPubNub() {
        channelName = ""//"P_\(CacheManager.userInfo?.id ?? 0)"
        let configuration = PNConfiguration(publishKey: "pub-c-0b689a47-04ee-41e7-b5f6-38ac1f120d0a", subscribeKey: "sub-c-10136038-fea1-11e8-9488-9e80330262eb")
        self.client = PubNub.clientWithConfiguration(configuration)
        self.client.addListener(self)
        self.client.subscribeToChannels([channelName], withPresence: true)
    }
    
    func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
        if message.data.channel != message.data.subscription {
            // Message has been received on channel group stored in message.data.subscription.
        }
        else {
            // Message has been received on channel stored in message.data.channel.
        }
        let strMessage = message.data.message as? String ?? ""
        if (strMessage.lowercased() == "cancelled") {
            UIApplication.shared.keyWindow?.makeToast("cancelled by driver")
            //            Utilities.playAudio(activity, strMessage);
        } else if (strMessage.lowercased() == "arrived") {
            UIApplication.shared.keyWindow?.makeToast("driver arrived")
            //            Utilities.playAudio(activity, strMessage);
        }
        print("Received message: \(message.data.message ?? "") on channel \(message.data.channel) " +
            "at \(message.data.timetoken)")
    }
    
    func client(_ client: PubNub, didReceivePresenceEvent event: PNPresenceEventResult) {
        // Handle presence event event.data.presenceEvent (one of: join, leave, timeout, state-change).
        if event.data.channel != event.data.subscription {
            // Presence event has been received on channel group stored in event.data.subscription.
        } else {
            // Presence event has been received on channel stored in event.data.channel.
        }
        if event.data.presenceEvent != "state-change" {
            print("\(event.data.presence.uuid ?? "") \"\(event.data.presenceEvent)'ed\"\n" +
                "at: \(event.data.presence.timetoken) on \(event.data.channel) " +
                "(Occupancy: \(event.data.presence.occupancy))");
        } else {
            print("\(event.data.presence.uuid ?? "") changed state at: " +
                "\(event.data.presence.timetoken) on \(event.data.channel) to:\n" +
                    "\(event.data.presence.state ?? [:])");
        }
    }
    
    func client(_ client: PubNub, didReceive status: PNStatus) {
        if status.operation == .subscribeOperation {
            // Check whether received information about successful subscription or restore.
            if status.category == .PNConnectedCategory || status.category == .PNReconnectedCategory {
                let subscribeStatus: PNSubscribeStatus = status as! PNSubscribeStatus
                if subscribeStatus.category == .PNConnectedCategory {
                    // This is expected for a subscribe, this means there is no error or issue whatsoever.
                    // Select last object from list of channels and send message to it.
                    let targetChannel = client.channels().last!
                    client.publish("Hello from the PubNub Swift SDK", toChannel: targetChannel,
                                   compressed: false, withCompletion: { (publishStatus) -> Void in
                                    if !publishStatus.isError {
                                        // Message successfully published to specified channel.
                                    } else {
                                        /**
                                         Handle message publish error. Check 'category' property to find out
                                         possible reason because of which request did fail.
                                         Review 'errorData' property (which has PNErrorData data type) of status
                                         object to get additional information about issue.
                                         
                                         Request can be resent using: publishStatus.retry()
                                         */
                                    }
                    })
                } else {
                    /**
                     This usually occurs if subscribe temporarily fails but reconnects. This means there was
                     an error but there is no longer any issue.
                     */
                }
            }
            else if status.category == .PNUnexpectedDisconnectCategory {
                /**
                 This is usually an issue with the internet connection, this is an error, handle
                 appropriately retry will be called automatically.
                 */
            }
                // Looks like some kind of issues happened while client tried to subscribe or disconnected from
                // network.
            else {
                let errorStatus: PNErrorStatus = status as! PNErrorStatus
                if errorStatus.category == .PNAccessDeniedCategory {
                    /**
                     This means that PAM does allow this client to subscribe to this channel and channel group
                     configuration. This is another explicit error.
                     */
                } else {
                    /**
                     More errors can be directly specified by creating explicit cases for other error categories
                     of `PNStatusCategory` such as: `PNDecryptionErrorCategory`,
                     `PNMalformedFilterExpressionCategory`, `PNMalformedResponseCategory`, `PNTimeoutCategory`
                     or `PNNetworkIssuesCategory`
                     */
                }
            }
        }
    }
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        NotificationCenter.default.post(Notification.init(name: Notification.Name.init("CHECK_RIDE_UPDATES")))
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        NotificationCenter.default.post(Notification.init(name: Notification.Name.init("CHECK_RIDE_UPDATES")))
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        NotificationCenter.default.post(Notification.init(name: Notification.Name.init("CHECK_RIDE_UPDATES")))
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        if let token = Messaging.messaging().fcmToken {
            UserDefaults.fcmToken = token
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        Messaging.messaging().apnsToken = "1212121driver".data(using: String.Encoding.utf8)
        connectToFCM()
    }
    
}

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        UserDefaults.fcmToken = fcmToken ?? ""
    }
    
    func connectToFCM() {
        Messaging.messaging().isAutoInitEnabled = true
        if let token = Messaging.messaging().fcmToken {
            UserDefaults.fcmToken = token
        }
    }
    
}

//True caller 
extension AppDelegate {
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        return TCTrueSDK.sharedManager().application(application, continue: userActivity, restorationHandler: restorationHandler as? ([Any]?) -> Void)
    }
}

extension UserDefaults {
    static var fcmToken: String {
        get {
            return (UserDefaults.standard.object(forKey: "fcmToken") as? String) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "fcmToken")
            UserDefaults.standard.synchronize()
        }
    }
}
