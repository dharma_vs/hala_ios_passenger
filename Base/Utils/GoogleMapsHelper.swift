//
//  GoogleMapsHelper.swift
//  Taxi
//
//  Created by Appcoup on 2/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import GoogleMaps
import MapKit
import JSONJoy
import MapKit

var googleMapKey = "AIzaSyBMrzaqLihjWwOeZgtNzIJVlGk8GsWyqZY"
var defaultMapLocation = LocationCoordinate(latitude: 11.00988, longitude: 76.949966)

struct DistanceDuration: JSONJoy {
    var duration: Duration?
    var distance: Distance?
    var root: MapRoot?
    
    init(_ decoder: JSONLoader) throws {
        duration = try? Duration.init(decoder["duration"])
        distance = try? Distance.init(decoder["distance"])
        root = try? MapRoot.init(decoder["overview_polyline"])
    }
    
    var time: Int {
        return Int(self.duration?.text?.components(separatedBy: " ").first ?? "0") ?? 0
    }
    
    var km: Double {
        return Double(self.distance?.text?.components(separatedBy: " ").first ?? "0.0") ?? 0.0
    }
}

struct MapRoot: JSONJoy {
    let points: String?
    init(_ decoder: JSONLoader) throws {
        points = decoder["points"].getOptional()
    }
}

class Duration: JSONJoy {
    let text: String?
    let value: Int?
    required init(_ decoder: JSONLoader) throws {
        text = decoder["text"].getOptional()
        value = decoder["value"].getOptional()
    }
}


class Distance: JSONJoy {
    let text: String?
    var value: Double?
    required init(_ decoder: JSONLoader) throws {
        text = decoder["text"].getOptional()
        value = decoder["value"].getOptional()
        if value == nil {
            let val: Int? = decoder["value"].getOptional()
            value = Double(val ?? 0)
        }
    }
}

typealias LocationCoordinate = CLLocationCoordinate2D
typealias LocationDetail = (address : String, cities:(city:String?, city2: String?, state: String?), coordinate :LocationCoordinate, country: Countryy?)

extension CLLocationCoordinate2D {
    var location: CLLocation {
        return CLLocation.init(latitude: self.latitude, longitude: self.longitude)
    }
}

extension CLLocationCoordinate2D: Equatable { }
public func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool { return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude }

private struct Place : Decodable {
    
    var results : [Address]?
    
    var cities: (String?, String?, String?) {
        var city = ""
        var city2 = ""
        var state = ""
        for address in (self.results ?? []) {
            if city == "" {
                city =  address.address_components?.filter{
                    if $0.types?.contains("administrative_area_level_2") == true {
                        return true
                    } else {
                        return false
                    }
                    
                    }.first?.long_name ?? ""
            }
            if city2 == "" {
                city2 =  address.address_components?.filter{
                    if $0.types?.contains("locality") == true {
                        return true
                    } else {
                        return false
                    }
                    
                    }.first?.long_name ?? ""
            }
            if state == "" {
                state =  address.address_components?.filter{
                    if $0.types?.contains("administrative_area_level_1") == true {
                        return true
                    } else {
                        return false
                    }
                    
                    }.first?.long_name ?? ""
            }
            if !(city == "") && !(city2 == "") && !(state == "") {
                break
            }
        }
        return (city, city2, state)
    }
    
    var country: Countryy? {
        var country = Countryy()
        country.country_short_name = ""
        country.country_full_name = ""
        for address in (self.results ?? []) {
            if country.country_short_name == "" || country.country_short_name == nil {
                country.country_short_name =  address.address_components?.filter{
                    if $0.types?.contains("country") == true {
                        return true
                    } else {
                        return false
                    }
                    
                    }.first?.short_name ?? ""
            }
            if country.country_full_name == "" || country.country_full_name == nil {
                country.country_full_name =  address.address_components?.filter{
                    if $0.types?.contains("country") == true {
                        return true
                    } else {
                        return false
                    }
                    
                    }.first?.long_name ?? ""
            }
            
            if !(country.country_short_name == "") && !(country.country_full_name == "") {
                break
            }
        }
        return country
    }
}

struct Countryy: JSONSerializable {
    var country_short_name: String?
    var country_full_name: String?
}

private struct Address : Decodable {
    
    var formatted_address : String?
    var geometry : Geometry?
    var address_components:[AddressComponent]?
    
    var city: String? {
        return self.address_components?.filter{
            if $0.types?.contains("locality") == true {
                return true
            } else {
                return false
            }
            
            }.first?.long_name
    }
}

private struct Geometry : Decodable {
    
    var location : Location?
    
}

private struct AddressComponent: Decodable {
    var long_name: String?
    var short_name: String?
    var types: [String]?
}

private struct Location : Decodable {
    
    var lat : Double?
    var lng : Double?
}

class MapPin: NSObject, MKAnnotation {
    var title: String?
    let coordinate: CLLocationCoordinate2D
    init(title: String?, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
    }
}

class GoogleMapsHelper : NSObject {
    
    var mapView : GMSMapView?
    var locationManager : CLLocationManager?
    private var currentLocation : ((CLLocation)->Void)?
    
    var appleMap: MKMapView?
    
    func getAppleMapView(withDelegate delegate: MKMapViewDelegate? = nil, in view : UIView, withPosition position :LocationCoordinate = defaultMapLocation, zoom : Float = 15, isNeedToStretch: Bool = true) {
        appleMap = MKMapView(frame: isNeedToStretch ? UIScreen.main.bounds : view.frame)
        appleMap?.layoutMargins = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 25)
        appleMap?.showsUserLocation = true
        appleMap?.userTrackingMode = .followWithHeading
        if let userLocation = self.appleMap?.userLocation.coordinate {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 200, longitudinalMeters: 200)
            self.appleMap?.setRegion(viewRegion, animated: true)
        }
        appleMap?.delegate = delegate
        view.addSubview(appleMap!)
        appleMap?.translatesAutoresizingMaskIntoConstraints = false
        let attributes: [NSLayoutConstraint.Attribute] = [.top, .bottom, .right, .left]
        NSLayoutConstraint.activate(attributes.map {
            NSLayoutConstraint(item: appleMap ?? MKMapView(), attribute: $0, relatedBy: .equal, toItem: appleMap?.superview, attribute: $0, multiplier: 1, constant: 0)
        })
    }
    
    func showRouteOnMap(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D, zoomToUserLocation: Bool = false) {

        let sourcePlacemark = MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil)

        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)

        let sourceAnnotation = MapPin(title: "source", coordinate: pickupCoordinate)
        let destinationAnnotation = MapPin(title: "destination",coordinate: destinationCoordinate)
        appleMap?.addAnnotations([sourceAnnotation, destinationAnnotation])
//        let coordinateRegion = MKCoordinateRegion(center: sourceAnnotation.coordinate, latitudinalMeters: 800, longitudinalMeters: 800)
//        appleMap?.setRegion(coordinateRegion, animated: true)
//        let coordinateRegion1 = MKCoordinateRegion(center: destinationAnnotation.coordinate, latitudinalMeters: 800, longitudinalMeters: 800)
//        appleMap?.setRegion(coordinateRegion1, animated: true)

        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile

        // Calculate the direction
        let directions = MKDirections(request: directionRequest)

        directions.calculate {
            (response, error) -> Void in

            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }

                return
            }

            let route = response.routes[0]

            self.appleMap?.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)

            let rect = route.polyline.boundingMapRect
                        
            if zoomToUserLocation {
                if let userLocation = self.appleMap?.userLocation.coordinate {
                    let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 200, longitudinalMeters: 200)
                    self.appleMap?.setRegion(viewRegion, animated: true)
                }
            } else {
                self.appleMap?.setRegion(MKCoordinateRegion(rect), animated: true)
            }
        }
    }
    
    func getMapView(withDelegate delegate: GMSMapViewDelegate? = nil, in view : UIView, withPosition position :LocationCoordinate = defaultMapLocation, zoom : Float = 15, isNeedToStretch: Bool = true) {
        
        mapView = GMSMapView(frame: isNeedToStretch ? UIScreen.main.bounds : view.frame)
        self.setMapStyle(to : mapView)
        mapView?.isMyLocationEnabled = true
        mapView?.delegate = delegate
        mapView?.camera = GMSCameraPosition.camera(withTarget: position, zoom: 15)
        view.addSubview(mapView!)
        mapView?.translatesAutoresizingMaskIntoConstraints = false
        let attributes: [NSLayoutConstraint.Attribute] = [.top, .bottom, .right, .left]
        NSLayoutConstraint.activate(attributes.map {
            NSLayoutConstraint(item: mapView, attribute: $0, relatedBy: .equal, toItem: mapView?.superview ?? UIView(), attribute: $0, multiplier: 1, constant: 0)
        })
    }
    
    func getNormalMapView(withDelegate delegate: GMSMapViewDelegate? = nil, in view : UIView, withPosition position :LocationCoordinate = defaultMapLocation, zoom : Float = 15, isNeedToStretch: Bool = true) {
        mapView = GMSMapView(frame: isNeedToStretch ? UIScreen.main.bounds : view.frame)
        mapView?.delegate = delegate
        mapView?.camera = GMSCameraPosition.camera(withTarget: position, zoom: 15)
        view.addSubview(mapView!)
        mapView?.translatesAutoresizingMaskIntoConstraints = false
        let attributes: [NSLayoutConstraint.Attribute] = [.top, .bottom, .right, .left]
        NSLayoutConstraint.activate(attributes.map {
            NSLayoutConstraint(item: mapView, attribute: $0, relatedBy: .equal, toItem: mapView?.superview, attribute: $0, multiplier: 1, constant: 0)
        })
    }
//    func openTrackerInBrowser(toLat:String,toLong:String) {
//        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
//            
//            UIApplication.shared.openURL(URL(string:
//                                                "comgooglemaps://?saddr=&daddr=\(toLat),\(toLong)&directionsmode=driving")!)
//        } else {
//            print("Cannot use google maps at this movement")
//        }
//    }
    var monitorLocation: Bool = false
    func getCurrentLocation(_ monitorLocation: Bool = false, onReceivingLocation : @escaping ((CLLocation)->Void)){
        self.monitorLocation = monitorLocation
        self.currentLocation = onReceivingLocation
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager?.allowsBackgroundLocationUpdates = true
//        locationManager?.pausesLocationUpdatesAutomatically = false
        locationManager?.requestAlwaysAuthorization()
        locationManager?.distanceFilter = 50
    }
    
    func moveTo(location : LocationCoordinate = defaultMapLocation, with center : CGPoint) {
        
        CATransaction.begin()
        CATransaction.setValue(2, forKey: kCATransactionAnimationDuration)
        CATransaction.setCompletionBlock {
            self.mapView?.animate(to: GMSCameraPosition.camera(withTarget: location, zoom: 15))
        }
        CATransaction.commit()
        self.mapView?.center = center  //  Getting current location marker to center point
        
    }
    // Setting Map Style
    private func setMapStyle(to mapView: GMSMapView?){
        do {
            // Set the map style by passing a valid JSON string.
            if let url = Bundle.main.url(forResource: "Map_style", withExtension: "json") {
                mapView?.mapStyle = try GMSMapStyle(contentsOfFileURL: url)
            }else {
                print("error")
            }
            
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    func calculateDistanceDuration(_ fromLocation: LocationCoordinate, toLocation:
        LocationCoordinate,on completion : @escaping (((DistanceDuration?)) -> ())) {
        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(fromLocation.latitude),\(fromLocation.longitude)&destination=\(toLocation.latitude),\(toLocation.longitude)&key=\(googleMapKey)"//AIzaSyAzKFn6RDsH7cO3D9oamPsz0FRqUNaX-Lo
        guard let url = URL(string: urlString) else {
            print("Error in creating URL Geocoding")
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            print(url.absoluteString)
            let jsonObject = try? JSONSerialization.jsonObject(with: data ?? Data(), options: JSONSerialization.ReadingOptions.mutableContainers)
            var legsObject = (((jsonObject as? [String: Any])?["routes"] as? [[String: Any]])?.first?["legs"] as? [[String: Any] ])?.first
            legsObject?["overview_polyline"] = (((jsonObject as? [String: Any])?["routes"] as? [[String: Any]])?.first?["overview_polyline"] as? [String: Any] )
            completion(try? DistanceDuration.init(JSONLoader.init(legsObject as Any)))
            }.resume()
    }
    
    func getPlaceAddress(from location : LocationCoordinate, on completion : @escaping ((LocationDetail)->())){
        
        let urlString = "https://maps.googleapis.com/maps/api/geocode/json?latlng=\(location.latitude),\(location.longitude)&key=\(googleMapKey)"
        
        guard let url = URL(string: urlString) else {
            print("Error in creating URL Geocoding")
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let places = data?.getDecodedObject(from: Place.self), let address = places.results?.first?.formatted_address, let lattitude = places.results?.first?.geometry?.location?.lat, let longitude = places.results?.first?.geometry?.location?.lng {
                let cities = places.cities
                let country = places.country
                completion((address, cities, LocationCoordinate(latitude: lattitude, longitude: longitude), country: country))
            }
            }.resume()
        
    }
    
}


extension GoogleMapsHelper: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            print("Location: \(location)")
            self.currentLocation?(location)
            GlobalValues.sharedInstance.currentLocation = location
            defaultMapLocation = location.coordinate
            self.currentLocation = nil
        }
        
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
            if self.monitorLocation {
                manager.startMonitoringSignificantLocationChanges()
            }
            print("Location status is OK.")
        @unknown default:
            print("")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager?.stopUpdatingLocation()
        print("Error: \(error)")
    }
}

private struct MapPath : Decodable{
    
    var routes : [Route]?
    
}

private struct Route : Decodable{
    
    var overview_polyline : OverView?
}

private struct OverView : Decodable {
    
    var points : String?
}



extension GMSMapView {
    
    //MARK:- Call API for polygon points
    
    func drawPolygon(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D, color: UIColor = .black, strokeWidth: CGFloat = 3.0, strokeColor: UIColor = .black, mode:String = "driving", animated: Bool = true, onCompletion completion: ((_ inner: () -> GMSPolyline) -> (Void))? = nil){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        guard let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=\(mode)&key=\(googleMapKey)") else {
            return
        }
        
        DispatchQueue.main.async {
            
            session.dataTask(with: url) { (data, response, error) in
                print("Inside Polyline ", data != nil)
                guard data != nil else {
                    return
                }
                
                do {
                    
                    let route = try JSONDecoder().decode(MapPath.self, from: data!)
                    
                    if let points = route.routes?.first?.overview_polyline?.points {
                        //                        self.drawPath(with: points, color: color)
                        DispatchQueue.main.async {
                            
                            guard let path = GMSPath(fromEncodedPath: points) else { return }
                            let polyline = GMSPolyline(path: path)
                            polyline.strokeWidth = strokeWidth
                            polyline.strokeColor = strokeColor
                            if mode == "walking"{
                            let styles: [GMSStrokeStyle] = [.solidColor(.black), .solidColor(.clear)]
                            let scale = 1.0 / self.projection.points(forMeters: 1.0, at: self.camera.target)
                            let dashLength = NSNumber(value: 0.5 * Float(scale))
                            let gapLength = NSNumber(value: 0.5 * Float(scale))
                            polyline.spans = GMSStyleSpans(path, styles, [dashLength, gapLength], .rhumb)
                            }
//                            polyline.map = self
                            var bounds = GMSCoordinateBounds()
                            for index in 1...path.count() {
                                bounds = bounds.includingCoordinate(path.coordinate(at: index))
                            }
                            completion?({return polyline})
                            if animated {
                                self.animate(with: .fit(bounds))
                            }
                        }
                    }
                    
                } catch let error {
                    
                    print("Failed to draw ",error.localizedDescription)
                }
                
                
                }.resume()
            
            
        }
        
        
    }
    
    //MARK:- Draw polygon
    
    private func drawPath(with points : String, color: UIColor = .black){
        
        print("Drawing Polyline ", points)
        
        DispatchQueue.main.async {
            
            guard let path = GMSPath(fromEncodedPath: points) else { return }
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 3.0
            polyline.strokeColor = color
            polyline.map = self
            var bounds = GMSCoordinateBounds()
            for index in 1...path.count() {
                bounds = bounds.includingCoordinate(path.coordinate(at: index))
            }
            self.animate(with: .fit(bounds))
        }
        
    }
    
    
    
}

extension MKMapView {
    func removeAllAnnotations() {
        let userAnnotation = self.userLocation
        self.removeAnnotations(self.annotations)
        if userAnnotation != nil {
            self.addAnnotation(userAnnotation)
        }
    }
}

extension NSObject {
    func convertCllocation(_ latitude: String!, Longitude: String!) -> CLLocation {// latitude convert cllocation
        let lat:Double = Double(latitude)!
        let long:Double = Double(Longitude)!
        let firsLocation = CLLocation(latitude:lat, longitude:long)
        return firsLocation
    }
    
    func etaFrom(_ pickup: LocationCoordinate, drop: LocationCoordinate) -> String {
        let distanceKm = pickup.location.distance(from: drop.location)/1000.0
        var time = distanceKm/10
        let intTime = Int(time * 60)
        return "\(intTime.secondsToTime(isDay: true))"
//        if(time <= 1) {
//            time = time * 60
//            return String(format: "%.2f minutes", time)
//        } else {
//            return String(format: "%.2f hours", time)
//        }
    }
    
    func distanceKm(_ pickup: LocationCoordinate, drop: LocationCoordinate) -> String {
        let distanceKm = pickup.location.distance(from: drop.location)/1000.0
        return String(format: "%.2f", distanceKm).km
    }
}
/*
 func calculateDistanceDuration(_ fromLocation: LocationCoordinate, toLocation:
     LocationCoordinate,wayPoints:String = "",on completion : @escaping (((DistanceDuration?)) -> ())) {
     
     let sourcePlacemark = MKPlacemark(coordinate: fromLocation, addressDictionary: nil)
     let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
     
     let destinationPlacemark = MKPlacemark(coordinate: toLocation, addressDictionary: nil)
     let destinationMapItem = MKMapItem(placemark: destinationPlacemark)

     // Create request
     let request = MKDirections.Request()
     request.source = sourceMapItem
     request.destination = destinationMapItem
     request.transportType = MKDirectionsTransportType.automobile
     request.requestsAlternateRoutes = false
     let directions = MKDirections(request: request)
     directions.calculate { response, error in
         if let route = response?.routes.first {
             print("Distance: \(route.distance), ETA: \(route.expectedTravelTime)")
             var distanceDuration = DistanceDuration()
             distanceDuration.distance = Distance(text: route.distance.distance, value: Int(route.distance))
             distanceDuration.duration = Duration(text: route.expectedTravelTime.mins, value: Int(route.expectedTravelTime))
             completion(distanceDuration)
         }
 */
extension String {
    var km: String {
        return self + " km"
    }
}

import CoreLocation

extension CLLocation {
    var updateToFirebase: Void {
        var driverLocationInfo = DriverLocationInfo()
        driverLocationInfo.latitude = self.coordinate.latitude.stringValue
        driverLocationInfo.longitude = self.coordinate.longitude.stringValue
        FirebaseHelper.shared.updateDriverLocationInfo(driverLocationInfo)
        return
    }
}

extension Double {
    var stringValue: String {
        return "\(self)"
    }
}
