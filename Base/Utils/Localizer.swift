//
//  Localizer.swift
//  Base
//
//  Created by Admin on 05/01/22.
//

import Foundation


extension String {
    var localized: String {
        let path = Bundle.main.path(forResource: UserDefaults.selectedLanguage?.languageCode ?? "en", ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    var languageCode: String {
        switch self {
        case .English:
            return "en"
        case .Tamil:
            return "ta-IN"
        case .French:
            return "fr"
        default:
            return "en"
        }
    }
}


extension UserDefaults {
    static var selectedLanguage: String? {
        get {
            return UserDefaults.standard.string(forKey: .SELECTED_LANGUAGE)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: .SELECTED_LANGUAGE)
            UserDefaults.standard.synchronize()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: .AppLanguageChangedNotification), object: nil)
        }
    }
}

//Lanugauges
extension String {
    static let English = "English"
    static let French = "French"
    static let German = "German"
    static let Italian = "Italian"
    static let Tamil = "Tamil"
}


extension String {
    static let SELECTED_LANGUAGE = "SELECTED_LANGUAGE"
}
