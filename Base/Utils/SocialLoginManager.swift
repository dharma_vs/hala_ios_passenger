//
//  SocialLoginManager.swift
//  
//
//  Created by Appcoup on 8/20/19.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import FirebaseAuth
import Firebase
import AuthenticationServices

class SocialLogin: NSObject {
    static let shared = SocialLogin()
    private override init(){ }
    var successBlock: ((String?, String?, [String: Any]?) -> ())?
    var failureBlock:((Error?) -> ())?
    var facebookReadPermissions = ["public_profile", "email"/*, "user_friends"*/]
    
    func loginToFacebook(_ controller: UIViewController!, withSuccess successBlock: @escaping (_ token: String?, _ userId: String?, _ userInfo: [String: AnyObject]?) -> (), andFailure failureBlock: @escaping (Error?) -> ()) {
        LoginManager().logOut()
        if AccessToken.current != nil {
            return
        }
        
        LoginManager().logIn(permissions: self.facebookReadPermissions, from: controller) { (result, error) in
            if error != nil {
                LoginManager().logOut()
                failureBlock(error)
            } else if result?.isCancelled == true {
                LoginManager().logOut()
                failureBlock(nil)
            } else {
                var allPermsGranted = true
                let grantedPermissions = result?.grantedPermissions.map( {"\($0)"} )
                for permission in self.facebookReadPermissions {
                    if grantedPermissions?.contains(permission) == false {
                        allPermsGranted = false
                        break
                    }
                }
                if allPermsGranted {
                    // Do work
                    let fbToken = result?.token?.tokenString
                    let fbUserID = result?.token?.userID
                    if(AccessToken.current != nil){
                        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start { (connection, result, error) -> Void in
                            var dict = [String: AnyObject]()
                            if (error == nil){
                                dict = result as! [String : AnyObject]
                                successBlock(fbToken,fbUserID,dict)
                            } else {
                                successBlock(fbToken,fbUserID,nil)
                            }
                        }
                    } else {
                        successBlock(fbToken,fbUserID,nil)
                    }
                } else {
                    failureBlock(nil)
                }
            }
        }
    }
    
  /*  func loginToGoogle1(_ controller: UIViewController!, withSuccess successBlock: @escaping (_ token: String?, _ userId: String?, _ userInfo: [String: Any]?) -> (), andFailure failureBlock: @escaping (Error?) -> ()) {
        GIDSignIn.sharedInstance.signOut()
        self.successBlock = successBlock
        self.failureBlock = failureBlock
        let config = GIDConfiguration(clientID: "965253693292-0j2snvjg4h38lk1l65mqc23joc70msh1.apps.googleusercontent.com")
        GIDSignIn.sharedInstance.signIn(with: config, presenting: controller) { [weak self] (user, error) in
            if error == nil {
                let _ = ["ccp":"", "tel_no":"", "email": (user?.profile?.email ?? "")] as AnyObject
                guard let authentication = user?.authentication else { return }
                let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken ?? "", accessToken: authentication.accessToken)
                Auth.auth().signIn(with: credential) { (authResult, error) in
                    if let err = error {
                        self?.failureBlock?(err)
                        return
                    }
                    if let userInfo = authResult?.additionalUserInfo?.profile {
                        self?.successBlock?(authentication.accessToken,authResult?.user.email,userInfo)
                    }
                    GIDSignIn.sharedInstance.signOut()
                }
            }
        }
    }*/
    func loginToGoogle(_ controller: UIViewController!, withSuccess successBlock: @escaping (_ token: String?, _ userId: String?, _ userInfo: [String: Any]?) -> (), andFailure failureBlock: @escaping (Error?) -> ()) {
        GIDSignIn.sharedInstance.signOut()
        self.successBlock = successBlock
        self.failureBlock = failureBlock
        let config = GIDConfiguration(clientID: "965253693292-0j2snvjg4h38lk1l65mqc23joc70msh1.apps.googleusercontent.com")
        GIDSignIn.sharedInstance.signIn(withPresenting: controller) { signInResult, error in
            guard error == nil else { return }
            guard let signInResult = signInResult else { return }
            
            signInResult.user.refreshTokensIfNeeded { user, error in
                guard error == nil else { return }
                guard let user = user else { return }
                
                let idToken = user.idToken
                // Send ID token to backend (example below).
                let credential = GoogleAuthProvider.credential(withIDToken: user.idToken?.tokenString ?? "", accessToken: user.accessToken.tokenString)
                Auth.auth().signIn(with: credential) { (authResult, error) in
                    if let err = error {
                        self.failureBlock?(err)
                        return
                    }
                    if let userInfo = authResult?.additionalUserInfo?.profile {
                      //  self?.successBlock?(authentication.accessToken,authResult?.user.email,userInfo)
                        self.successBlock?(signInResult.user.accessToken.tokenString,authResult?.user.email,userInfo)
                    }
                    GIDSignIn.sharedInstance.signOut()
                }
            }
            
        }
    }
    func loginToApple(_ controller: UIViewController!, withSuccess successBlock: @escaping (_ token: String?, _ userId: String?, _ userInfo: [String: Any]?) -> (), andFailure failureBlock: @escaping (Error?) -> ()) {
        self.successBlock = successBlock
        self.failureBlock = failureBlock
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = controller
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
    }
    
    fileprivate func googleSignOut() {
        GIDSignIn.sharedInstance.signOut()
    }
    
    deinit{
        self.successBlock = nil
        self.failureBlock = nil
    }
}

@available(iOS 13.0, *)
extension SocialLogin: ASAuthorizationControllerDelegate {
    /// - Tag: did_complete_authorization
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            // Create an account in your system.
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
//            print(userIdentifier)
//            print(fullName)
//            print(email)
            if email?.isValidEmail() ?? false {
                var userInfo = [String: Any]()
                userInfo["email"] = email
                userInfo["firstName"] = fullName?.givenName
                userInfo["lastName"] = fullName?.familyName
                self.successBlock?(userIdentifier,email,userInfo)
//                processEmail(email: email, firstName: fullName?.givenName, lastName: fullName?.familyName)
            } else {
                UIApplication.keyWindow?.rootViewController?.showAlertWith("Invalid Email")
            }
            
            // For the purpose of this demo app, store the `userIdentifier` in the keychain.
//            self.saveUserInKeychain(userIdentifier)
            
            // For the purpose of this demo app, show the Apple ID credential information in the `ResultViewController`.
//            self.showResultViewController(userIdentifier: userIdentifier, fullName: fullName, email: email)
        case let passwordCredential as ASPasswordCredential:
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            // For the purpose of this demo app, show the password credential as an alert.
            DispatchQueue.main.async {
                self.showPasswordCredentialAlert(username: username, password: password)
            }
        default:
            break
        }
    }
    
    private func saveUserInKeychain(_ userIdentifier: String) {
        do {
            try KeychainItem(service: "com.a2solution.SpareParts", account: "userIdentifier").saveItem(userIdentifier)
        } catch {
            print("Unable to save userIdentifier to keychain.")
        }
    }
    
    private func showResultViewController(userIdentifier: String, fullName: PersonNameComponents?, email: String?) {
//        guard let viewController = self.presentingViewController as? ResultViewController
//            else { return }
//
//        DispatchQueue.main.async {
//            viewController.userIdentifierLabel.text = userIdentifier
//            if let givenName = fullName?.givenName {
//                viewController.givenNameLabel.text = givenName
//            }
//            if let familyName = fullName?.familyName {
//                viewController.familyNameLabel.text = familyName
//            }
//            if let email = email {
//                viewController.emailLabel.text = email
//            }
//            self.dismiss(animated: true, completion: nil)
//        }
    }
    
    private func showPasswordCredentialAlert(username: String, password: String) {
        let message = "The app has received your selected credential from the keychain. \n\n Username: \(username)\n Password: \(password)"
        let alertController = UIAlertController(title: "Keychain Credential Received",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        UIApplication.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    /// - Tag: did_complete_error
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
    }
}

extension UIViewController {
    
}

@available(iOS 13.0, *)
extension UIViewController: ASAuthorizationControllerPresentationContextProviding {
    /// - Tag: provide_presentation_anchor
    public func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return (self.view.window ?? UIApplication.keyWindow)!
    }
}


class MainAppDelegate: UIResponder, UIApplicationDelegate {
    
    var enableGoogleSignIn = false
    var enableFacebookSignIn = false
    var isNeedBackgroundLocationUpdates = false
        
    let player = AVPlayerHelper()
    
    override init() {
        super.init()
//        UIFont.overrideInitialize()
    }
    
    let helper = GoogleMapsHelper()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        if enableFacebookSignIn {
            ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
            LoginManager().reauthorizeDataAccess(from: UIApplication.shared.keyWindow?.rootViewController ?? UIViewController()) { (result, error) in
            }
        }
        
        if let value = launchOptions?[UIApplication.LaunchOptionsKey.location], (value as? Bool) == true {
            print("Launched by background location update")
            if isNeedBackgroundLocationUpdates {
                self.helper.getCurrentLocation(onReceivingLocation: { (location) in
                    location.updateToFirebase
                })
            }
        }
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if enableGoogleSignIn {
            return GIDSignIn.sharedInstance.handle(url)
        }
        return false
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if enableGoogleSignIn {
            return GIDSignIn.sharedInstance.handle(url)
        }
        if enableFacebookSignIn {
            let isFacebookURL = url.scheme != nil && url.scheme!.hasPrefix("fb\(Settings.shared.appID ?? "")") && url.host == "authorize"
            if isFacebookURL {
                return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
            }
        }
        return false
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        if isNeedBackgroundLocationUpdates {
            self.helper.getCurrentLocation(true, onReceivingLocation: { (location) in
                location.updateToFirebase
            })
            self.helper.locationManager?.startMonitoringSignificantLocationChanges()
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        if isNeedBackgroundLocationUpdates {
            self.helper.locationManager?.stopMonitoringSignificantLocationChanges()
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if enableFacebookSignIn {
//            AppEvents.activateApp()
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        if isNeedBackgroundLocationUpdates {
            self.helper.getCurrentLocation(true, onReceivingLocation: { [weak self] (location) in
                location.updateToFirebase
            })
        }
    }
}

//Usage

/*

@IBAction func googleBtnAction(_ sender: Any) {
    SocialLogin.shared.loginToGoogle(self, withSuccess: { (accessToken, userId, userInfo) in
        let credential = Credential.init(email: userId ?? "", source: "google", password: "")
        credential.token = accessToken
        credential.userInfo = userInfo
        self.delegate?.loginViewController(self, didRequestToAuthenticateCredential: credential)
    }) { (error) in
        if let err = error {
            self.showError(err)
        }
    }
}
@IBAction func faceBookBtnAction(_ sender: Any) {
    SocialLogin.shared.loginToFacebook(self, withSuccess: { (accessToken, userId, userInfo) in
        let credential = Credential.init(email: userId ?? "", source: "facebook", password: "")
        credential.token = accessToken
        credential.userInfo = userInfo
        self.delegate?.loginViewController(self, didRequestToAuthenticateCredential: credential)
        //            self.delegate?.loginViewController(self, didRequestToAuthenticateCredential: credential)
    }) { (error) in
        if let err = error {
            self.showError(err)
        }
    }
}

 */


extension String {
    func mapLabelWithKeys(_ keys: [String: String]) -> String {
        if let key = keys[self] {
            return key
        } else {
            return self
        }
    }
}

var deviceId: String  {
    if let deviceId = UserDefaults.standard.value(forKey: .DEVICE_ID) as? String {
        return deviceId
    } else {
        let deviceId = UIDevice.current.identifierForVendor!.uuidString
        UserDefaults.standard.setValue(deviceId, forKey: .DEVICE_ID)
        UserDefaults.standard.synchronize()
        return deviceId
    }
}
