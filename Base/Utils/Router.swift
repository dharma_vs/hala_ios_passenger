//
//  Router.swift
//
//
//  Created by Appcoup on 8/19/19.
//  Copyright © 2019 . All rights reserved.
//

import Foundation
import UIKit
import SlideMenuControllerSwift

class Router {
    
    static let authenticate = UIStoryboard.init(name: "Authenticate", bundle: nil)
    static let base = UIStoryboard(name: .Base, bundle: .main)
    static let main = UIStoryboard(name: .Main, bundle: .main)
    static let auth = UIStoryboard(name: .Authenticate, bundle: .main)
    static let chat = UIStoryboard(name: .Chat, bundle: .main)
    static let payment = UIStoryboard(name: .Payment, bundle: .main)
    
    static func setLoginViewControllerAsRoot() {
        let authVC = Router.auth.instantiateViewController(withIdentifier: "AuthRootVC")
        window?.rootViewController = authVC
    }
    
    static var window: UIWindow? {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if appDelegate?.window == nil {
            appDelegate?.window = UIApplication.shared.windows.first
        }
        return appDelegate?.window
    }
    
    static func setHomeViewControllerAsRoot() {
       
    }
    
    static func setLangageViewControllerAsRoot() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if appDelegate?.window == nil {
            appDelegate?.window = UIApplication.shared.windows.first
        }
        UserDefaults.appLanguage = "English"
        if UserDefaults.appLanguage != nil {
            Router.setLoginViewControllerAsRoot()
            return
        }
        let navigationViewController = BaseNavigationController.init(rootViewController: LanguageViewController.initFromStoryBoard(.Authenticate))
        navigationViewController.isNavigationBarHidden = true
        appDelegate?.window?.rootViewController = navigationViewController
    }
    
    static func setDashboardViewControllerAsRoot() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if appDelegate?.window == nil {
            appDelegate?.window = UIWindow.init(frame: UIScreen.main.bounds)
        }
        if UserDefaults.companyLogin{
            let tripsVC = TripsViewController.initFromStoryBoard(.Menu)
            let navigationViewController = BaseNavigationController.init(rootViewController: tripsVC)
                 navigationViewController.isNavigationBarHidden = true
            let menuViewController = MenuViewController.initFromStoryBoard(.Menu)
                 
                 SlideMenuOptions.hideStatusBar = false
                 SlideMenuOptions.contentViewScale = 1.0
                 SlideMenuOptions.panFromBezel = false
                 
                 let slideMenuViewController = SlideMenuController.init(mainViewController: navigationViewController, leftMenuViewController: menuViewController)
                 
                 appDelegate?.window?.rootViewController = slideMenuViewController
        }else{
            let dashboardVC = HomeViewController.initFromStoryBoard(.Main)
            let navigationViewController = BaseNavigationController.init(rootViewController: dashboardVC)
                 navigationViewController.isNavigationBarHidden = true
            let menuViewController = MenuViewController.initFromStoryBoard(.Menu)
                 
                 SlideMenuOptions.hideStatusBar = false
                 SlideMenuOptions.contentViewScale = 1.0
                 SlideMenuOptions.panFromBezel = false
                 
                 let slideMenuViewController = SlideMenuController.init(mainViewController: navigationViewController, leftMenuViewController: menuViewController)
                 
                 appDelegate?.window?.rootViewController = slideMenuViewController
            
//            DataService().checkCurrentRideStatus { (inner) -> (Void) in
//                let rideList = (try? inner()) ?? nil
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                    dashboardVC.viewModel = DashboardViewModel(rideList)
//                }
//            }
        }
     
    }
    
}

struct StoryBoradID {
    struct Cell {
        
    }
    struct Segue {
        
    }
}

class AppNavigationController: BaseNavigationController {
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

extension String {
    static let Base = "Base"
    static let Main = "Main"
    static let Authenticate = "Authenticate"
    static let Chat = "Chat"
    static let Payment = "Payment"
    static let Menu = "Menu"
    static let Settings = "Settings"
    static let Profile = "Profile"
    static let Dashboard = "Dashboard"
    static let Custom = "Custom"
    
    static let CurrentApplicationLanguage = "CurrentApplicationLanguage"
    static let AppLanguageChangedNotification = "AppLanguageChanged"
    
    static let Country = "Country"
    static let DB_STRING = "DB_STRING"
    static let DEVICE_ID = "DEVICE_ID"
    static let company = "company"
}

extension NSObject {
    static var appDelegate: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    static var keyWindow: UIWindow? {
        return NSObject.appDelegate?.window ?? UIApplication.shared.windows.first{$0.isKeyWindow}
    }
}

extension UserDefaults {
    
}

extension NSObject {
    class var className: String {
        return String(describing: self)
    }
    var classNameString: String {
        return String(describing: self)
    }
}
