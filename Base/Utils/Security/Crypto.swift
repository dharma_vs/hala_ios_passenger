//
//  Crypto.swift
//  Base
//
//  Created by Admin on 05/01/22.
//

import Foundation
import CryptoSwift

//let cryptoKey = "9z539xo3qr1mlqu6"
// cryptoIV = "4y55ujvx5zpcmwh0"
let cryptoKey = "9z539xo3qr1mlqu69z539xo3qr1mlqu6"
let cryptoIV = "4y55ujvx5zpcmwh0"
let iv = AES.randomIV(AES.blockSize)
extension String {
    var encrypted: String? {
        let aes = try? AES(key: cryptoKey.bytes, blockMode: CBC(iv: cryptoIV.bytes), padding: .pkcs5)
        let encrypted = try? aes?.encrypt(self.bytes)
        var s = encrypted?.toBase64()
        s = cryptoIV + s!
        return s?.base64Encoded()
    }
    
    var decrypted: String? {
        let str:String = String(self.base64Decoded() ?? "")
        var encryptedString = tail(s: str)
        let secretIV = prefix(s: str)
        encryptedString = String(encryptedString.filter { !"\n".contains($0) })
        let dec = try? AES(key: cryptoKey.bytes, blockMode: CBC(iv: secretIV.bytes), padding: .pkcs7)
        let decrypted = try? dec?.decrypt(Data(base64Encoded: encryptedString)?.bytes ?? [])
        return String(bytes: decrypted ?? [], encoding: .utf8)
    }

    var encryptedAppendWithIV: String? {
        let aes = try? AES(key: cryptoKey.bytes, blockMode: CBC(iv: cryptoIV.bytes), padding: .pkcs5)
        let encrypted = try? aes?.encrypt(self.bytes)
        return encrypted?.toBase64()
    }
    
    var decryptedAppendWithIV: String? {
        
        let aes = try? AES(key: cryptoKey.bytes, blockMode: CBC(iv: cryptoIV.bytes), padding: .pkcs5)
        let decrypted = try? aes?.decrypt(Data(base64Encoded: self)?.bytes ?? [])
        return String(bytes: decrypted ?? [], encoding: .utf8)
    }
}

extension String {
    
    func base64Encoded() -> String? {
        data(using: .utf8)?.base64EncodedString()
    }
    
    func base64Decoded() -> String? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return String(data: data, encoding: .utf8)
    }
    func tail(s: String) -> String {
        return String(s.suffix(from: s.index(s.startIndex, offsetBy: 16)))
    }
    func prefix(s: String) -> String {
        return String(s.prefix(upTo: s.index(s.startIndex, offsetBy: 16)))
    }
}
