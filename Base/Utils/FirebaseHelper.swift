//
//  FirebaseHelper.swift
//  ChatPOC
//
//  Created by CSS on 06/03/18.
//  Copyright © 2018 CSS. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth

let fireBaseURL = "https://halamobility.firebaseio.com"//"https://jeffery-2020.firebaseio.com/"
let fireBaseDB = "/Taxi/Dev"
let fireBaseFleetLocationDB = "/Taxi/Dev"
let fireBaseDriverLocationDB = "/Taxi/Dev/drivers"
let fireBaseStorageURL = "gs://halamobility.appspot.com"
let fireBaseUserName = "admin@halamobility.com"
let fireBasePassword = "HalaMobility2020!"
var firebaseUserID = "23hEwN0CM8RXxRNJ6uiHOCN9upl2"

var rideId: String = ""

typealias UploadTask = StorageUploadTask
typealias SnapShot = DataSnapshot
typealias EventType = DataEventType

enum ChatType : Int {
    
    case single = 1
    case group = 2
    case media = 3
    
}

class FirebaseHelper: NSObject {
    
    private var ref: DatabaseReference?
    
    private var storage : Storage?
    
    static var shared = FirebaseHelper()
    
    // Write Text Message
    
    func write(to userId : Int, with text : String, type chatType : ChatType = .single){
        
        self.storeData(to: userId, with: text, mime: .text, type: chatType)

    }
    
    // Upload from data
    
    func write(to userId : Int, with data : Data, mime type : Mime, type chatType : ChatType = .single, completion : @escaping (Bool)->())->UploadTask{
        
        let metadata = self.initializeStorage(with: type)
        
        return self.upload(data: data,forUser : userId, mime: type, type : chatType, metadata: metadata, completion: { (url) in
            
            completion(url != nil)
            
            guard url != nil else {
                return
            }
            
            self.storeData(to: userId, with: url, mime: type, type: chatType)
            
        })
        
    }
    
    // Upload from Filepath
    
    func write(to userId : Int, file url : URL, mime type : Mime, type chatType : ChatType = .single , completion : @escaping (Bool)->())->UploadTask{
        
        let metadata = self.initializeStorage(with: type)
        
        return self.upload(file: url,forUser : userId, mime: type, type : chatType,metadata: metadata, completion: { (url) in
            
            completion(url != nil)
            
            guard url != nil else {
                return
            }
            
            self.storeData(to: userId, with: url, mime: type, type: chatType)
            
        })
        
        
    }
    
    // Update Message in Specific Path
    
    func update(chat: ChatEntity, key : String, toUser user : Int, type chatType : ChatType = .single){
        
        let chatPath = fireBaseDB + "/Chats/" + rideId + "/Messages" //Common.getChatId(with: user) ?? .Empty
        
        self.update(chat: chat, key: key, inRoom: chatPath)
        
    }
    
    func observeDriverLocation(path : String, with : EventType){
        self.initializeDB()
        self.ref!.child(path).getData { (error, snapshot) in
            let values = (snapshot!.valueInExportFormat() as? [String:Any])
            if let lat = values?["latitude"] as? Double {
                NotificationCenter.default.post(name: Notification.Name("locationUpdate"), object: nil, userInfo: ["latitude":"\(lat)"])

            }
            if let lng = values?["longitude"] as? Double {
                NotificationCenter.default.post(name: Notification.Name("locationUpdate"), object: nil, userInfo: ["longitude":"\(lng)"])

            }
        }
        
        self.ref!.child(path).observe(with, with: { (snapShot) in
            print(snapShot)
            if snapShot.key == "latitude"{
                NotificationCenter.default.post(name: Notification.Name("locationUpdate"), object: nil, userInfo: ["latitude":snapShot.value ?? ""])

            }else if snapShot.key == "longitude"{
                NotificationCenter.default.post(name: Notification.Name("locationUpdate"), object: nil, userInfo: ["longitude":snapShot.value ?? ""])

            }
        })
    }
    
}


//MARK:- Helper Functions


extension FirebaseHelper {
    
    // Initializing DB
    
    private func initializeDB(){
        
        if ref == nil {
            let db = Database.database(url: fireBaseURL)
            db.isPersistenceEnabled = true
            self.ref = db.reference()
        }
        
        let auth = Auth.auth()
        auth.signIn(withEmail: fireBaseUserName, password: fireBasePassword) { (result, error) in
            print(result)
        }
    }
    
    // Initializing Storage
    
    private func initializeStorage(with type : Mime)->StorageMetadata{
        
        if self.storage == nil {
            self.storage = Storage.storage(url: fireBaseStorageURL)
        }
        
        
        let metadata = StorageMetadata()
        metadata.contentType = type.contentType
        
        return metadata
    }
    
    // Update Values in specific path
    
    private func update(chat : ChatEntity, key : String, inRoom room : String){
        if chat.receive_on?.contains("2018") == true {
            print("Stop")
        }
        self.ref?.child(room).child(key).updateChildValues(chat.JSONRepresentation)
        
    }
    
    // Common Function to Store Data
    
    private func storeData(to userId : Int, with string : String?, mime type : Mime, type chatType : ChatType){
        
        let chat = ChatEntity()
        chat.ride_id = rideId
        chat.read_on = ""
        chat.receive_on = ""
//        chat.status = MsgStatus.sent.rawValue
        chat.receiver_id = "P_\(userId)"
        chat.sender_id = ""//"D_\((CacheManager.driverInfo?.id?.intValue) ?? 0)"
//        chat.number = String.removeNil(User.main.mobile)
        chat.sent_on = "\(self.getUTCCurrentTimeMillis())"
        chat.message = string
        let messageId = "\(self.getUTCCurrentTimeMillis())_P_\(userId)"
        chat.message_id = messageId
        chat.type = "1"
        
//        if type == .text {
//
//            chat.text = string
//
//        } else {
//
//            chat.url = string
//
//        }
        
//        if chatType == .group {
//            chat.groupId = userId
//        }
        
        self.initializeDB()
        let chatPath = fireBaseDB + "/Chats/" + rideId + "/Messages/" + messageId //Common.getChatId(with: userId) ?? .Empty
        self.ref?.child(chatPath).setValue(chat.JSONRepresentation)
        
    }
    
    //MARK:- Upload Data to Storage Bucket
    
    private func upload(data : Data,forUser user : Int, mime : Mime, type chatType : ChatType, metadata : StorageMetadata, completion : @escaping (_  downloadUrl : String?) -> ())->UploadTask{
        
        let chatPath = self.getChatId(with: user) ?? ""//chatType == .group ? getGroupChat(with: user) : getRoom(forUser: user)
        let ref = self.storage?.reference(withPath: chatPath).child(ProcessInfo().globallyUniqueString+mime.ext)
        let uploadTask = ref?.putData(data, metadata: metadata, completion: { (metaData, error) in
            
            if error != nil ||  metaData == nil {
                
                print(" Error in uploading  ", error!.localizedDescription)
                
            } else {
//
//                if let image = UIImage(data: data) {  // Store the uploaded image in Cache
//                    ref?.downloadURL(completion: { (url, error) in
//                        completion(url?.absoluteString)
//                        if let urlObject = url?.absoluteString {
//                            Cache.shared.setObject(image, forKey: urlObject as AnyObject)
//                        }
//                    })
//
//                }
            }
        })
        
        return uploadTask!
        
    }
    
    func  getChatId(with userId : Int?) -> String? {
        let id: Int? = 0//(CacheManager.sharedInstance.getObjectForKey(.UserId) as? Int)
        guard let provider = id, let userId = userId else { return nil }
        
        return userId <= provider ? "u\(userId)_p\(provider)" : "p\(provider)_u\(userId)"
        
    }
    
    //MARK:- Upload File to Storage Bucket
    
    private func upload(file url : URL,forUser user : Int, mime : Mime, type chatType : ChatType, metadata : StorageMetadata, completion : @escaping (_  downloadUrl : String?) -> ())->UploadTask{
        
        let chatPath = self.getChatId(with: user) ?? ""//chatType == .group ? getGroupChat(with: user) : getRoom(forUser: user)
        let ref = self.storage?.reference(withPath: chatPath).child(ProcessInfo().globallyUniqueString+mime.ext)
        let uploadTask = ref?.putFile(from: url, metadata: metadata, completion: { (metaData, error) in
            
            if error != nil || metaData == nil {
                
                print(" Error in uploading  ", error!.localizedDescription)
                
                
            } else {
                
                ref?.downloadURL(completion: { (url, error) in
                    completion(url?.absoluteString)
                })
            }
            
            
        })
        
        return uploadTask!
    }

}


//MARK:- Observers

extension FirebaseHelper {
    
    // Observe if any value changes
    
    func observe(path : String, with : EventType, value : @escaping ([ChatResponse])->())->UInt {
        
        self.initializeDB()
        
        return self.ref!.child(path).queryOrdered(byChild: "receive_on").observe(with, with: { (snapShot) in
            
            value(self.getModal(from: snapShot))
            
        })
        
        
        
    }
    
    // Remove Firebase Observers
    func remove(observers : [UInt]){
        
        self.initializeDB()
        
        for observer in observers {
            
            self.ref?.removeObserver(withHandle: observer)
            
        }
        
    }
    
    func removeObserversWithPath(_ path: String, observers: [UInt]) {
        self.initializeDB()
        for observer in observers {
            self.ref?.child(path).queryOrdered(byChild: "receive_on").removeObserver(withHandle: observer)
        }
    }
    
    func removeAllObservers() {
        self.ref?.removeAllObservers()
    }
    
    // Observe Last message
    
    func observeLastMessage(path : String, with : EventType, value : @escaping ([ChatResponse])->())->UInt {
        
        self.initializeDB()
        
        return self.ref!.child(path).queryLimited(toLast: 1).observe(with, with: { (snapShot) in
            
            value(self.getModal(from: snapShot))
            
        })
        
    }
    
    // Get Values From SnapShot
    
    private func getModal(from snapShot : SnapShot)->[ChatResponse]{
        
        var chatArray = [ChatResponse]()
        var response : ChatResponse?
        var chat : ChatEntity?
        
        if let snaps = snapShot.valueInExportFormat() as? [String : NSDictionary] {
            
            for snap in snaps {
                
                self.getChatEntity(with: &response, chat: &chat, snap: snap)
                chatArray.append(response!)
                
            }
            
        } else if let snaps = snapShot.value as? NSDictionary {
            
            self.getChatEntity(with: &response, chat: &chat, snap: (key: snapShot.key , value: snaps))
            chatArray.append(response!)
        }
        
        
        return chatArray.sorted(by: { (obj1, obj2) -> Bool in
            return (Int64(obj1.response?.receive_on ?? "") ?? 0) < (Int64(obj2.response?.receive_on ?? "") ?? 0)
        })
    }
    
    private func getChatEntity( with response : inout ChatResponse?, chat : inout ChatEntity?,snap : (key : String, value : NSDictionary)){
        
        response = ChatResponse()
        chat = ChatEntity()
        
        response?.key = snap.key
        
        chat?.read_on = snap.value.value(forKey: FirebaseConstants.main.read_on) as? String
        chat?.receiver_id = snap.value.value(forKey: FirebaseConstants.main.receiver_id) as? String
        chat?.sender_id = snap.value.value(forKey: FirebaseConstants.main.sender_id) as? String
        chat?.receive_on = snap.value.value(forKey: FirebaseConstants.main.receive_on) as? String
        chat?.sent_on = snap.value.value(forKey: FirebaseConstants.main.sent_on) as? String
        chat?.type = snap.value.value(forKey: FirebaseConstants.main.type) as? String
        chat?.message_id = snap.value.value(forKey: FirebaseConstants.main.message_id) as? String
        chat?.message = snap.value.value(forKey: FirebaseConstants.main.message) as? String
        chat?.ride_id = snap.value.value(forKey: FirebaseConstants.main.ride_id) as? String
//        chat?.status = snap.value.value(forKey: FirebaseConstants.main.status) as? String

        response?.response = chat
        
    }
    
}

extension NSObject {
    func getUTCCurrentTimeMillis() -> Int64 {
        return Int64(Date().timeIntervalSince1970)
    }
}

extension FirebaseHelper {

    func updateDriverLocationInfo(_ info: DriverLocationInfo?) {
        self.initializeDB()
        let chatPath = fireBaseDriverLocationDB + "/\(info?.driver_id ?? "0")"
        self.ref?.child(chatPath).setValue(info?.JSONRepresentation ?? [String: String]())
    }
    
    func observeBaseURL(path : String = "grocecoup/baseURL", with : EventType = .value, value : @escaping (String?)->()) {
        
        self.initializeDB()
        
        self.ref!.child(path).observeSingleEvent(of: .value) { (snapShot) in
            value(snapShot.value as? String)
        }
        
    }
    
    func updateAppleInfo(info: AppleInfo, key: String) {
        self.initializeDB()
        let path = "users/" + firebaseUserID + fireBaseDB + "/AppleInfo/" + "/\(key)"
        self.ref?.child(path).setValue(info.JSONRepresentation)
    }
    
    func getAppleInfo(key: String, completion: ((AppleInfo?)->Void)?) {
        self.initializeDB()
        let path = "users/" + firebaseUserID + fireBaseDB + "/AppleInfo/" + "/\(key)"
        self.ref?.child(path).observeSingleEvent(of: .value, with: { (snapshot) in
            let jsonDict = (snapshot.valueInExportFormat() as? [String: Any])
            let info = jsonDict?.data?.getDecodedObject(from: AppleInfo.self)
            completion?(info)
        })
    }
    
}

struct AppleInfo: JSONSerializable {
    var email, firstName, lastName: String?
}

struct DriverLocationInfo: JSONSerializable {
    var device_id, driver_id, latitude, longitude: String?
    
    init() {
        self.device_id = deviceId
        self.driver_id = ""// CacheManager.driverInfo?.id?.stringValue
        self.latitude = nil
        self.longitude = nil
    }
}

class ChatEntity : JSONSerializable {
    
    var message_id: String?
    var sender_id: String?
    var receiver_id: String?
    var sent_on: String?
    var receive_on: String?
    var read_on: String?
    var type: String?
    var message: String?
    var ride_id: String?
//    var status: String?

}


enum MsgStatus: String {
    case read = "READ", received = "RECEIVED", sent = "SENT"
}

class ChatResponse : JSONSerializable {
    
    var key : String?
    var response : ChatEntity?
    
}

class Response : JSONSerializable {
    
    var response : [String : ChatEntity]?
    
    required init(from decoder: Decoder) throws{
        
        let container = try decoder.container(keyedBy: Key.self)
        
        print(container)
        
      //  self.key = try container.decode(String.self, forKey: Key.init(stringValue: "Key")!)
      //  self.response = try container.decode(ChatEntity.self, forKey: Key.init(stringValue: "response")!)

    }
    
}

class Key : CodingKey, JSONSerializable {
 
    var stringValue: String
    
    required init?(stringValue: String) {
        self.stringValue = stringValue
    }
    
    var intValue: Int?
    
    required init?(intValue: Int) {
        return nil
    }
    
}
