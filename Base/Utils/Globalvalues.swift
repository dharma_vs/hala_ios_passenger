//
//  Globalvalues.swift
//  HalaMobility
//
//  Created by ADMIN on 29/06/22.
//

import Foundation
import CoreLocation
class GlobalValues:NSObject{
    static let sharedInstance = GlobalValues()
    var currentLocation = CLLocation()
    var GVtimer:Int? = 0
    var minimumBattery:Double = 30.0
    var unlockDistance:Double = 500
    var countryName:String = "IN"
    var cityName:String = "Hyderabad"
    var requestCheckInterval = 10.0
    var currentRentalStatus = 0
    var previousRentalStatus = 0

}
