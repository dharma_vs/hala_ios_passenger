import Foundation
import CoreLocation

protocol LocationServiceDelegate: NSObjectProtocol {
    func tracingLocation(userLocation: CLLocation)
    func tracingLocationDidFailWithError(error: NSError)
}

class LocationManager: NSObject {
    
    static let shared = LocationManager()
    var locationManager :CLLocationManager?
    var currentLocation: CLLocation?
    var latestLocation: CLLocation?
    weak var delegate: LocationServiceDelegate?
    
    override init() {
        super.init()
        DispatchQueue.main.async{
        self.locationManager = CLLocationManager()
        self.locationManager!.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager!.distanceFilter = kCLDistanceFilterNone
        self.locationManager!.headingFilter = kCLHeadingFilterNone
        self.locationManager!.pausesLocationUpdatesAutomatically = false
        self.locationManager!.delegate = self
        self.locationManager!.requestWhenInUseAuthorization()
        self.locationManager!.startUpdatingLocation()
//            DispatchQueue.main.asyncAfter(deadline: .now() + 30.0, execute: {
//                self.locationManager?.stopUpdatingLocation()
//            })
        }
    }
    
    
    // Private function
    private func updateLocation(currentLocation: CLLocation){
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.tracingLocation(userLocation: currentLocation)
    }
    
    private func updateLocationDidFailWithError(error: NSError) {
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.tracingLocationDidFailWithError(error: error)
    }
}
extension LocationManager: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        DispatchQueue.main.async {

        //print("Location Service class: \(locations)")
        guard let location = locations.last else {
            return
        }
        
        // singleton for get last location
        self.currentLocation = location
        
        // use for real time update location
        self.updateLocation(currentLocation: location)
            
        //Stop updating locationmanager
        //self.locationManager?.stopUpdatingLocation()
        }
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
//            let permissionAlert = UIAlertController(title: "Location Service", message: "Do you want to call ", preferredStyle: .alert)
//            let persmissionSettings = UIAlertAction(title: "Settings", style: .default) { (action) in
//                if let url = URL(string: "App-Prefs:root=Privacy&path=LOCATION") {
//                    // If general location settings are disabled then open general location settings
//                    UIApplication.shared.openURL(url)
//                }
//            }
//            let permissionCanel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//            permissionAlert.addAction(persmissionSettings)
//            permissionAlert.addAction(permissionCanel)
//            UIApplication.shared.keyWindow?.rootViewController!.present(permissionAlert, animated: true, completion: nil)
            // Display the map using the default location.
        //mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager!.stopUpdatingLocation()
        print("Error: \(error)")
        updateLocationDidFailWithError(error: error as NSError)
    }
}
