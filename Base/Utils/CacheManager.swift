//
//  CacheManager.swift
//
//  Created by Admin on 03/09/15.
//

import UIKit
import Foundation

let cacheManagerName = "cachemanager"

public enum CacheKey: String {
    case AuthToken = "AuthToken", RefreshToken = "RefreshToken", UserEmail = "UserEmail", UserName = "UserName", UserFirstName = "UserFirstName", UserLastName = "UserLastName", UserId = "UserId", ProfilePicture = "ProfilePicture", CreatedAt = "CreatedAt", ExpireAt = "ExpireAt", LastApiCallAt = "LastApiCallAt", City = "City", Country = "Country", GuestToken = "GuestToken", RoleID = "RoleID", CompanyName = "CompanyName", CompanyId = "CompanyId", AddressID = "AddressId", UserAddress = "UserAddress", Mobile = "Mobile", AppSettings = "AppSettings", DriverInfo = "DriverInfo", FCMToken = "FCMToken", isNeedToEditProfile, DeviceId = "DeviceId", UserInfo = "UserInfo",  ETA = "ETA", SpeechEnabled = "SpeechEnabled", updatedCurrencySymbol = "updatedCurrencySymbol", currencyRate = "currencyRate", BillingInfo
}

open class Cache: NSObject, NSCoding {
    override init() {
        super.init()
    }
    fileprivate var cacheDic: [String: Any] = [:]
    fileprivate var applicationCacheDic: [String: Any] = [:]
    
    public required init(coder aDecoder: NSCoder) {
        if let dico: [String: Any] = aDecoder.decodeObject(forKey: "cacheDic") as? [String: Any] {
            self.cacheDic = dico
        }
        
        if let dico: [String: Any] = aDecoder.decodeObject(forKey: "applicationCacheDic") as? [String: Any] {
            self.applicationCacheDic = dico
        }
        
        super.init()
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.cacheDic, forKey: "cacheDic")
        aCoder.encode(self.applicationCacheDic, forKey: "applicationCacheDic")
    }
}

open class CacheManager {
    var currentCache: Cache?
    
    struct Static {
        static var instance: CacheManager?
        static var token: Int = 0
    }
    
    private static var excuteOnce: () = {
        Static.instance = CacheManager()
        if let filepath = CacheManager.pathInDocDirectory(cacheManagerName) {
            if let data = try? Data(contentsOf: URL(fileURLWithPath: filepath)) {
                if let mgr = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? Cache {
                    Static.instance!.currentCache = mgr
                }
            }
        }
        if Static.instance!.currentCache == nil {
            Static.instance!.currentCache = Cache()
        }
    }()
    open class var sharedInstance: CacheManager {
        _ = CacheManager.excuteOnce
        return Static.instance!
    }
    
    class func pathInDocDirectory(_ filename: String) -> String? {
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        if paths.count > 0 {
            let path: String = paths[0]
            return path + "/" + filename
        }
        return nil
    }

    open func saveCache() {
        if let path = CacheManager.pathInDocDirectory(cacheManagerName) {
            if let cache = self.currentCache {
                let data = NSKeyedArchiver.archivedData(withRootObject: cache)
                try? data.write(to: URL(fileURLWithPath: path))
            }
        }
    }
    
    open func clearCache() {
        if let path = CacheManager.pathInDocDirectory(cacheManagerName) {
            self.currentCache = Cache()
            if let cache = self.currentCache {
                let data = NSKeyedArchiver.archivedData(withRootObject: cache)
                try? data.write(to: URL(fileURLWithPath: path))
            }
        }
    }
    
    open func clearUserData() {
        self.currentCache?.cacheDic = [:]
        self.saveCache()
    }

    open func getObjectForKey(_ key: CacheKey) -> Any? {
        if let cacheValue = self.currentCache?.cacheDic[key.rawValue] {
            return cacheValue
        }
        return nil
    }
    
    open func setObject(_ value: Any, key: CacheKey) {
        self.currentCache?.cacheDic[key.rawValue] = value
        CacheManager.sharedInstance.saveCache()
    }
    
    open func removeObjectForKey(_ key: CacheKey) {
        self.currentCache?.cacheDic.removeValue(forKey: key.rawValue)
    }
    
    open func getObjectInApplicationForKey(_ key: CacheKey) -> Any? {
        if let cacheValue = self.currentCache?.applicationCacheDic[key.rawValue] {
            return cacheValue
        }
        return nil
    }
    
    open func setObjectInApplication(_ value: Any, key: CacheKey) {
        self.currentCache?.applicationCacheDic[key.rawValue] = value
        CacheManager.sharedInstance.saveCache()
    }
    
    open func removeObjectInApplicationForKey(_ key: CacheKey) {
        self.currentCache?.applicationCacheDic.removeValue(forKey: key.rawValue)
    }
}
