//
//  DataServiceEndpointInfo.swift
//  Base
//
//  Created by Admin on 02/01/22.
//

import Foundation

struct DataServiceEndpointInfo {
    
    var scheme: String!
    var host: String!
    var port: Int!
    var path: String!
    var method: Method!
    var authType: AuthroizationType
    
    func request(parameters: Dictionary<String,Any>? = nil, body: Dictionary<String,Any>? = nil, media: [MediaData]? = nil, headers: Dictionary<String,String>? = nil, pathSuffix: String? = nil, requestContentType: RequestContentType = .json) -> URLRequest {
        
        var request = self.buildURLRequestWith(parameters: parameters, pathSuffix: pathSuffix)
        
        request = request.updateURLRequestWith(body: body, media: media, requestContentType: requestContentType)
        
        request = request.updateURLRequestWith(headers: headers, authType: self.authType)
        
        return request
    }
    
    private func buildURLRequestWith(parameters: Dictionary<String,Any>? = nil, pathSuffix: String? = nil) -> URLRequest {
        var urlComponents = URLComponents()
        urlComponents.scheme = self.scheme
        if self.port != 0 {
            urlComponents.port = self.port
        }
        urlComponents.host = self.host
        if pathSuffix != nil {
            let updatedPath = self.path + pathSuffix!
            urlComponents.path = updatedPath
        } else {
            urlComponents.path = self.path
        }
        
        var queryString: String?
        
        if (self.method == .GET || self.method == .DELETE) {
            queryString = DataServiceEndpointInfo.formQueryBodyString(parameters)
        }
        if let _queryString = queryString {
            urlComponents.query = _queryString
        }
        
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = method.rawValue
        return request
    }
    
    static fileprivate func formQueryBodyString(_ parameters: [String: Any]?) -> String? {
        var queryBodyString = [String]()
        if let queryBody = parameters, queryBody.values.count > 0 {
            for (key, value) in queryBody {
                queryBodyString.append("\(key)=\(value)")
            }
        }
        let queryString = queryBodyString.joined(separator: "&")
        return queryString
    }
    
    static fileprivate func createDataBody(body: Dictionary<String,Any>? = nil, media: [MediaData]?, boundary: String = String.boundaryString) -> Data {
        let lineBreak = "\r\n"
        var bodyData = Data()
        
        if let parameters = body {
            for (key, value) in parameters {
                if (value is NSNull) == false {
                    bodyData.append("--\(boundary + lineBreak)")
                    bodyData.append("Content-Disposition: form-data; name=\"\(key)\"\(lineBreak + lineBreak)")
                    bodyData.append("\( String(describing: value) + lineBreak)")
                }
            }
        }
        
        if let media = media {
            for photo in media {
                bodyData.append("--\(boundary + lineBreak)")
                bodyData.append("Content-Disposition: form-data; name=\"\(photo.key ?? "file")\"; filename=\"\(photo.filename ?? "ImageName.jpg")\"\(lineBreak)")
                bodyData.append("Content-Type: \(photo.mimeType + lineBreak + lineBreak)")
                bodyData.append(photo.data)
                bodyData.append(lineBreak)
            }
        }
        
        bodyData.append("--\(boundary)--\(lineBreak)")
        
        return bodyData
    }
    
}

extension URLRequest {
    fileprivate mutating func updateURLRequestWith(body: Dictionary<String,Any>? = nil, media: [MediaData]? = nil, requestContentType: RequestContentType = .json) -> URLRequest {
        setValue(requestContentType.rawValue, forHTTPHeaderField: "Content-Type")
        if body != nil {
            switch requestContentType {
            case .json:
                let data = try? JSONSerialization.data(withJSONObject: body ?? Dictionary<String,Any>(), options: .fragmentsAllowed)
                httpBody = data
                setValue("\(data?.count ?? 0)", forHTTPHeaderField: "Content-Length")
            case .formurlencoded:
                let data = DataServiceEndpointInfo.formQueryBodyString(body)?.data(using: .utf8)
                httpBody = data
                setValue("\(data?.count ?? 0)", forHTTPHeaderField: "Content-Length")
            case .multipartformdata:
                let data = DataServiceEndpointInfo.createDataBody(body: body, media: media)
                httpBody = data
                setValue("\(data.count)", forHTTPHeaderField: "Content-Length")
            }
        }
        return self
    }
    
    fileprivate mutating func updateURLRequestWith(headers: Dictionary<String,String>? = nil, authType: AuthroizationType) -> URLRequest {
        var updatedHeaders = headers ?? [:]
        
        if updatedHeaders["Accept"] == nil {
            updatedHeaders["Accept"] = "application/json"
        }
        
        let cacheManager = CacheManager.sharedInstance
        let userAutheticateToken = cacheManager.getObjectForKey(.AuthToken) as? String
        let applicationAutheticateToken = cacheManager.getObjectInApplicationForKey(.AuthToken) as? String
        if authType != .none {
            if userAutheticateToken != nil {
                var userToken = ""
                if let tempString = userAutheticateToken {
                    userToken = "Bearer " + tempString
                }
                updatedHeaders["Authorization"] = userToken
            } else if (authType != .user && applicationAutheticateToken != nil) {
                var applicationToken = ""
                if let tempString = applicationAutheticateToken {
                    applicationToken = "Bearer " + tempString
                }
                updatedHeaders["Authorization"] = applicationToken
            }
        }
                
        for (key, value) in updatedHeaders {
            setValue(value, forHTTPHeaderField: key)
        }
        
        return self
    }
}

extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}

extension String {
    static var boundaryString: String {
        return "Boundary-\(UUID().uuidString)"
    }
}

enum MimeType:String { //--- add MimeType depends on your requirement
    case jpeg = "image/jpeg"
    case png = "image/png"
    case gif = "image/gif"
    case pdf = "application/pdf"
}

struct MediaData {
    let key: String!
    let filename: String!
    let data: Data!
    let mimeType: String!
    
    init?(_ image: UIImage! = nil, data: Data? = nil, key: String, imageName: String, mimeType:MimeType) {
        self.key = key
        self.mimeType = mimeType.rawValue
        self.filename = imageName
        if data != nil {
            self.data = data
        } else {
            if let data = image.jpegData(compressionQuality: 0.5) {
                self.data = data
            } else {
                self.data = Data()
            }
        }
    }
}

import UIKit
