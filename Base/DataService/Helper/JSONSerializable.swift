//
//  JSONSerializable.swift
//  AlmindsTask
//
//  Created by Admin on 12/26/21.
//

import Foundation

protocol JSONSerializable : Codable {
    var JSONRepresentation : [String : Any] { get }
    var defaultCount: Int { get }
}

extension JSONSerializable {
    
    public var defaultCount: Int {
        return 6
    }
    
    var JSONRepresentation : [String : Any] {
        
        var representation = [String:Any]()
        
        for case let (label?, value) in Mirror(reflecting: self).children {
            
            switch value {
            
            case let value as [String:CustomType]:
                representation[label] = value.JSONRepresentation
            
            case let value as Dictionary<String,Any>:
                
                if let val = value as? JSONSerializable {
                    representation[label] = val.JSONRepresentation as AnyObject
                } else if let val = value as? [String:CustomType] {
                    
                    representation[label] =  {
                        var dict = [String: Any]()
                        for (key, newValue) in val {
                            dict[key] = newValue.value
                        }
                        return dict
                        
                    }()
                } else {
                    representation[label] = value as AnyObject
                }
                
            case let value as Array<Any>:
                
                if let val = value as? [JSONSerializable]{
                    representation[label] = val.map({ $0.JSONRepresentation as AnyObject}) as AnyObject
                } else {
                    representation[label] = value as AnyObject
                }
            
                
            case let value as JSONSerializable:
                
                representation[label] = value.JSONRepresentation
                
            case let value as AnyObject :
                
                if value is NSNull {
                    break
                } else {
                    representation[label] = value
                }
                
            default: break
                
            }
        }
        return representation
    }
    
    // Convert to data
    var data: Data? {
        let value = self.JSONRepresentation["value"]
        return (value as? [String: Any])?.data
    }
    
    func transform<T>(with object : T.Type)->T? where T : Decodable {
        return self.data?.getDecodedObject(from: object)
    }
    
    func transformArray<T>(from object : T.Type)->T? where T : Decodable {
        
        return self.JSONRepresentation.data?.getDecodedObject(from: object)
        
    }
    
}

extension String {
    var data: Data? {
        return data(using: .nonLossyASCII)
    }
}

extension Dictionary where Key == String, Value == Any {
    var data: Data? {
        return try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
    }
}

extension Array where Element == Any {
    var data: Data? {
        return try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
    }
}

class CustomType: NSObject, JSONSerializable, NSCoding {
    
    override init() {
        self.value = nil
    }
    
    init(value: AnyObject?) {
        self.value = value
    }
    
    func encode(with aCoder: NSCoder) {
        for case let (label, value) in Mirror(reflecting: self).children {
            aCoder.encode(value, forKey: label!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        value = aDecoder.decodeObject(forKey: "value") as AnyObject
    }
    
    var value: AnyObject?
    
    var intValue: Int? {
        return Int(stringValue)
    }
    
    var boolValue: Bool {
        return stringValue == "1" || stringValue == "true"
    }
    
    var floatValue: Float? {
        return Float(stringValue)
    }
    
    var doubleValue: Double? {
        return Double(stringValue)
    }
    
    var stringValue: String {
        return (self.value as? String) ?? "\(self.value ?? "" as AnyObject)"
    }
    
    func transform<T>(to object : T.Type)->T? where T : Decodable {
        return self.stringValue.data?.getDecodedObject(from: object)
    }
    
    var object: Dictionary<String, CustomType>? {
        return value as? Dictionary<String, CustomType>
    }
    
    var objectArray: Dictionary<String, [CustomType]>? {
        return value as? Dictionary<String, [CustomType]>
    }
    
    var array: Array<CustomType>? {
        return value as? Array<CustomType>
    }
    
    required init(from decoder: Decoder) throws {
        let container = try? decoder.singleValueContainer()
        self.value = try? container?.decode(Int.self) as AnyObject
        if self.value == nil {
            self.value = try? container?.decode(Bool.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Float.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(String.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Double.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Float32.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Float64.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Int8.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Int16.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Int32.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Int64.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(UInt.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(UInt8.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(UInt16.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(UInt32.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(UInt64.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Dictionary<String, CustomType>.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Dictionary<String, [CustomType]>.self) as AnyObject
        }
        if self.value == nil {
            self.value = try? container?.decode(Array<CustomType>.self) as AnyObject
        }
    }
    
    func encode(to encoder: Encoder) throws {
        
    }
}

extension Dictionary where Key == String, Value == CustomType {
    var JSONRepresentation: [String: Any] {
        var representation = [String:Any]()
        for (key, newValue) in self {
            if newValue.value is Self {
                representation[key] = (newValue.value as? [String: CustomType])?.JSONRepresentation ?? [:]
            } else if newValue.value is [[String: CustomType]] {
                representation[key] = (newValue.value as? [[String: CustomType]])?.JSONRepresentation ?? []
            } else if newValue.value is [CustomType] {
                representation[key] = (newValue.value as? [CustomType])?.JSONRepresentation ?? []
            } else {
                representation[key] = newValue.value is JSONSerializable ? ((newValue.value as? JSONSerializable)?.JSONRepresentation ?? [:]) : newValue.value
            }
        }
        return representation
    }
}

extension Array where Element == [String: CustomType] {
    var JSONRepresentation: [Any] {
        var values = [Any]()
        for value in self {
            values.append(value.JSONRepresentation)
        }
        return values
    }
}

extension Array where Element == CustomType {
    var JSONRepresentation: [Any] {
        var values = [Any]()
        for value in self {
            if let val = value.JSONRepresentation["value"] {
                values.append(val)
            }
        }
        return values
    }
}

