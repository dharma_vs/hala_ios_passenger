//
//  DataServiceEndpoint.swift
//  AlmindsTask
//
//  Created by Admin on 12/26/21.
//

import UIKit

let componentPort: Int = 6364
let componentScheme = "http"
let componentPathSuffix = "/rider"
let componentPathSuffixrental = "/rental"
let componentMasterPathSuffix = "/master"
let componentUploadPathSuffix = "/upload"
let componentHost = "dev.virtuesense.com"
let baseImageURL = "http://dev.virtuesense.com/halamobility/web/uploads/"

enum DataServiceEndpoint: AddressableEndPoint {

    case getUsers, getUserDetails, createUser, updateUser, DeleteUser
    
    var endPointInfo: DataServiceEndpointInfo {
        switch self {
        case .getUsers:
            return DataServiceEndpointInfo(scheme: "https", host: "dummyapi.io", port: 0, path: "/data/v1/user", method: .GET, authType: .none)
        case .getUserDetails:
            return DataServiceEndpointInfo(scheme: "https", host: "dummyapi.io", port: 0, path: "/data/v1/user", method: .GET, authType: .none)
        case .createUser:
            return DataServiceEndpointInfo(scheme: "https", host: "dummyapi.io", port: 0, path: "/data/v1/user/create", method: .POST, authType: .none)
        case .updateUser:
            return DataServiceEndpointInfo(scheme: "https", host: "dummyapi.io", port: 0, path: "/data/v1/user", method: .PUT, authType: .none)
        case .DeleteUser:
            return DataServiceEndpointInfo(scheme: "https", host: "dummyapi.io", port: 0, path: "/data/v1/user", method: .DELETE, authType: .none)
        }
    }

}
