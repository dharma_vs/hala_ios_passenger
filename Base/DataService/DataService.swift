//
//  DataService.swift
//  AlmindsTask
//
//  Created by Admin on 12/26/21.
//

import Foundation
import Alamofire

var enableAPIDebugging = true

let timeOutInterval: TimeInterval = 60

public func == (lhs: CoreError, rhs: CoreError) -> Bool {
    return lhs.code == rhs.code
}

open class ErrorInformation {
    public let code: Int
    open var description: String
    
    public init(code: Int, description: String) {
        self.code = code
        self.description = description
    }
}

open class CoreError: ErrorInformation, Equatable, Error, @unchecked Sendable {
    public let innerError: CoreError?
    open var informations: [ErrorInformation]?
    public init(code: Int, description: String, innerError: CoreError?, informations: [ErrorInformation]?) {
        self.innerError = innerError
        self.informations = informations
        super.init(code: code, description: description)
    }
}

public enum HTTPStatusCode: Int {
    case ok = 200, created = 201, accepted = 202, forbidden = 403, unAuthorized = 401, notFound = 404, unProcessableEntity = 422, `internal` = 500, noContent = 204, badRequest = 400
}

open class TimeOutError: CoreError {
    init(innerError: CoreError?) {
        super.init(code: NSURLErrorTimedOut, description: HTTPURLResponse.localizedString(forStatusCode: 408), innerError: innerError, informations: nil)
    }
}

open class NetWorkNotReachError: CoreError {
    init(innerError: CoreError?) {
        super.init(code: 001, description: HTTPURLResponse.localizedString(forStatusCode: 408), innerError: innerError, informations: nil)
    }
}

open class UnAuthorisedError: CoreError {
    init(innerError: CoreError?) {
        super.init(code: HTTPStatusCode.unAuthorized.rawValue, description: HTTPURLResponse.localizedString(forStatusCode: HTTPStatusCode.unAuthorized.rawValue), innerError: innerError, informations: nil)
    }
}

open class UserTokenNotProvideError: CoreError {
    init(innerError: CoreError?) {
        super.init(code: HTTPStatusCode.unAuthorized.rawValue, description: HTTPURLResponse.localizedString(forStatusCode: HTTPStatusCode.unAuthorized.rawValue), innerError: innerError, informations: nil)
    }
}

open class ForbiddenError: CoreError {
    
    init(innerError: CoreError?) {
        super.init(code: HTTPStatusCode.forbidden.rawValue, description: HTTPURLResponse.localizedString(forStatusCode: HTTPStatusCode.forbidden.rawValue), innerError: innerError, informations: nil)
    }
}

open class InternalError: CoreError {
    init(innerError: CoreError?) {
        super.init(code: HTTPStatusCode.internal.rawValue, description: HTTPURLResponse.localizedString(forStatusCode: HTTPStatusCode.internal.rawValue), innerError: innerError, informations: nil)
    }
}

open class UnProcessableEntityError: CoreError {
    init(innerError: CoreError?, informations: [ErrorInformation]?) {
        super.init(code: HTTPStatusCode.unProcessableEntity.rawValue, description: HTTPURLResponse.localizedString(forStatusCode: HTTPStatusCode.unProcessableEntity.rawValue), innerError: innerError, informations: informations)
    }
}

open class NotFoundError: CoreError {
    init(innerError: CoreError?, informations: [ErrorInformation]?) {
        super.init(code: HTTPStatusCode.notFound.rawValue, description: HTTPURLResponse.localizedString(forStatusCode: HTTPStatusCode.notFound.rawValue), innerError: innerError, informations: informations)
    }
}

open class BadRequestError: CoreError {
    init(innerError: CoreError?, informations: [ErrorInformation]?) {
        super.init(code: HTTPStatusCode.badRequest.rawValue, description: HTTPURLResponse.localizedString(forStatusCode: HTTPStatusCode.badRequest.rawValue), innerError: innerError, informations: informations)
    }
}

enum AuthroizationType {
    case none, application, user
}

protocol AddressableEndPoint {
    var endPointInfo: DataServiceEndpointInfo { get }
}

enum RequestContentType: String {
    case json = "application/json", formurlencoded = "application/x-www-form-urlencoded", multipartformdata = "multipart/form-data"
}

enum Method:String {
    case GET
    case POST
    case PUT
    case DELETE
    case PATCH
}

class DataService {
    
    var userAutheticateToken: String?
    var applicationAutheticateToken: String?
    var requestContentType: RequestContentType!
    
    init() {
        userAutheticateToken = CacheManager.sharedInstance.getObjectForKey(.AuthToken) as? String
        applicationAutheticateToken = CacheManager.sharedInstance.getObjectInApplicationForKey(.AuthToken) as? String
        requestContentType = .json
    }

    func request(endPoint: AddressableEndPoint, parameters: Dictionary<String,Any>? = nil, body: Dictionary<String,Any>? = nil, pathSuffix: String? = nil, completion:@escaping(Result<Data,Error>) -> Void) {
        
        let isValid = self.validateToken(endPoint, completion: completion)
        
        if !isValid {
            return
        }
       
        var request = endPoint.endPointInfo.request(parameters: parameters, body: body, pathSuffix: pathSuffix, requestContentType: self.requestContentType)
                
        if !Reachability.shared.isConnected {
            request.cachePolicy = .returnCacheDataDontLoad
        }
        
        AF.request(request).response { (response) in
            self.handleResponse(response, parameters: parameters, body: body, completion: completion)
        }
    }
    
    func uploadRequest(endPoint: AddressableEndPoint, parameters: Dictionary<String,Any>? = nil, body: Dictionary<String,Any>? = nil, media: [MediaData]? = nil, pathSuffix: String? = nil, completion:@escaping(Result<Data,Error>) -> Void, progressHandler: @escaping(Double) -> Void) {
        
        self.requestContentType = .multipartformdata
        
        let isValid = self.validateToken(endPoint, completion: completion)
        
        if !isValid {
            return
        }
       
        var request = endPoint.endPointInfo.request(parameters: parameters, body: body, media: media, pathSuffix: pathSuffix, requestContentType: self.requestContentType)
                
        if !Reachability.shared.isConnected {
            request.cachePolicy = .returnCacheDataDontLoad
        }
        let bodyData = request.httpBody ?? Data()
        request.httpBody = nil
        AF.upload(bodyData, with: request).uploadProgress { (progress) in
            progressHandler(progress.fractionCompleted)
        }.response { (response) in
            self.handleResponse(response, parameters: parameters, body: body, completion: completion)
        }
    }
    
    private func validateToken(_ endPoint: AddressableEndPoint, completion:@escaping(Result<Data,Error>) -> Void) -> Bool {
        switch endPoint.endPointInfo.authType {
        case .none:
            break
        case .application:
            if self.userAutheticateToken != nil {
            } else if (self.applicationAutheticateToken != nil) {
            } else {
                completion(.failure(ErrorResult.custom(string: "Application_Token_Not_Provided")))
                return false
            }
            break
        case .user:
            if self.userAutheticateToken != nil {
            } else {
                completion(.failure(ErrorResult.custom(string: "User_Token_Not_Provided")))
                return false
            }
            break
        }
        return true
    }
    
    private func handleResponse(_ response: AFDataResponse<Data?>, parameters: Dictionary<String,Any>? = nil, body: Dictionary<String,Any>? = nil, completion:@escaping(Result<Data,Error>) -> Void) {
        switch response.response?.statusCode {
        case let successCode where Set([HTTPStatusCode.ok.rawValue, HTTPStatusCode.created.rawValue, HTTPStatusCode.accepted.rawValue, HTTPStatusCode.noContent.rawValue]).contains(successCode):
            if let responseData = response.data, responseData.count > 0 {
                if enableAPIDebugging {
                    let request = response.request
                    let headers = request?.allHTTPHeaderFields
                    print("URL: \(request?.url?.absoluteString ?? "")")
                    print("Method: \(request?.httpMethod ?? "")")
                    print("Parameters: \((parameters) ?? [:])")
                    print("Body: \((body) ?? [:])")
                    print("Headers: \((headers) ?? [:])")
                    if let codedString = NSString(data: responseData, encoding: String.Encoding.utf8.rawValue) {
                        print("Response: \(codedString)")
                        print("Status Code: \(response.response?.statusCode ?? 0)")
                    }
                }
                completion(.success(responseData))
            }
            break
        case let httpUrlResponseErrorCode:
            var innerError: CoreError?
            let informations: [ErrorInformation]? = []
            let request = response.request
            let headers = request?.allHTTPHeaderFields
            if enableAPIDebugging {
                print("URL: \(request?.url?.absoluteString ?? "")")
                print("Method: \(request?.httpMethod ?? "")")
                print("Parameters: \((parameters) ?? [:])")
                print("Body: \((body) ?? [:])")
                print("Headers: \((headers) ?? [:])")
                if let error = response.error {
                    print("Response Error: \(error.localizedDescription)")
                    completion(.failure(ErrorResult.network(string: "" + error.localizedDescription)))
                    return
                }
                print("Status Code: \(response.response?.statusCode ?? 0)")
            }
            let responseData = response.data
            if let errorInfo = responseData?.getDecodedObject(from: CommonRes.self) {
                innerError = CoreError(code: response.response?.statusCode ?? 0, description: errorInfo.detail?.stringValue ?? "", innerError: nil, informations: [])
            }
            switch (httpUrlResponseErrorCode) {
            case HTTPStatusCode.unAuthorized.rawValue:
                completion(.failure(UnAuthorisedError(innerError: innerError)))
            case HTTPStatusCode.forbidden.rawValue:
                completion(.failure(ForbiddenError(innerError: innerError)))
            case HTTPStatusCode.internal.rawValue:
                completion(.failure(InternalError(innerError: innerError)))
            case HTTPStatusCode.unProcessableEntity.rawValue:
                completion(.failure(UnProcessableEntityError(innerError: innerError, informations: informations)))
            case HTTPStatusCode.notFound.rawValue:
                completion(.failure(NotFoundError(innerError: innerError, informations: informations)))
            default:
                completion(.failure(innerError ?? CoreError(code: httpUrlResponseErrorCode ?? 0, description: "Un Handled Error", innerError: innerError, informations: informations)))
            }
        }
    }
}

struct CommonRes: JSONSerializable {
    var status: StatusInfo?
    var message, detail: CustomType?
    var isSuccess: Bool { return self.status?.isSuccess == true }
    
    init(from decoder: Decoder) throws {
        let values = try? decoder.container(keyedBy: CodingKeys.self)
        status = try? values?.decode(StatusInfo.self, forKey: .status)
        message = try? values?.decode(CustomType.self, forKey: .message)
        detail = try? values?.decode(CustomType.self, forKey: .detail)
        if status == nil {
            let status: CustomType? = try? values?.decode(CustomType.self, forKey: .status)
            let statusInfo = StatusInfo(success: status, message: nil)
            self.status = statusInfo
        }
    }
}

class GeneralResponse<T: Decodable>: Decodable {
    var status: Bool?
    var message: String?
    var data : T?
}

struct StatusInfo: JSONSerializable {
    var success, message: CustomType?
    var isSuccess: Bool { return self.success?.boolValue == true }
}

internal extension CacheManager {
    
    static var driverCompleteInfo: DriverCompleteInfo? {
        return CacheManager.sharedInstance.getObjectForKey(.DriverInfo) as? DriverCompleteInfo
    }
    
    static var settings: AppSettings? {
        get {
        return CacheManager.sharedInstance.getObjectInApplicationForKey(.AppSettings) as? AppSettings
        }
        set {
            CacheManager.sharedInstance.setObjectInApplication(newValue, key: .AppSettings)
            CacheManager.sharedInstance.saveCache()
        }
    }
}

extension UIViewController {
    static func showAlert(_ message: String?) {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.stopActivityIndicator()
            topController.showAlertWith(message ?? "")
        }
    }
}
