//
//  UIView+Additions.swift
//  AlmindsTask
//
//  Created by Admin on 12/26/21.
//

import UIKit

struct AssociatedKeys {
    static var isRounded: UInt8 = 0
}

extension UIView {

    @IBInspectable var isRounded: Bool {
        get {
            guard let value = objc_getAssociatedObject(self, &AssociatedKeys.isRounded) as? Bool else {
                return false
            }
            return value
        }
        set(newValue) {
            objc_setAssociatedObject(self, &AssociatedKeys.isRounded, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            if newValue {
                self.cornerRadius = bounds.width/2
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    func addCircleLayer(_ isFill: Bool = false) -> CAShapeLayer {
        let name = "CircleAnimationLayer"
        
        if let layer = self.layer.sublayers?.filter({ $0.name == name }).first {
            layer.removeFromSuperlayer()
        }
        
        let shapeLayer = CAShapeLayer()
        let center = CGPoint.init(x: self.bounds.width/2, y: self.bounds.height/2)
        let circularPath = UIBezierPath.init(arcCenter: center, radius: self.bounds.width/2, startAngle: -0.0001, endAngle: (2*CGFloat.pi) + 0.0001, clockwise: false)
        shapeLayer.path = circularPath.cgPath
        shapeLayer.strokeColor = (isFill ? UIColor.gStartColor : UIColor.white).cgColor
        shapeLayer.fillColor = (isFill ? UIColor.white : UIColor.gStartColor).cgColor
        shapeLayer.borderColor = UIColor.lightGray.cgColor
        shapeLayer.borderWidth = 1
        shapeLayer.lineWidth = 15
        shapeLayer.lineJoin = CAShapeLayerLineJoin.miter
        shapeLayer.miterLimit = 15
        shapeLayer.strokeEnd = 0
        shapeLayer.fillRule = CAShapeLayerFillRule.evenOdd
        shapeLayer.lineCap = CAShapeLayerLineCap.butt
        shapeLayer.name = name
        self.layer.addSublayer(shapeLayer)
        return shapeLayer
    }
    
    func animateCircleWithDuration(_ duration: Double, pollingTime: Double = (CacheManager.settings?.polling_time?.doubleValue ?? 0), isFill: Bool = false) {
        let basicAnimation = CABasicAnimation.init(keyPath: "strokeEnd")
        basicAnimation.fromValue = (pollingTime - duration) / pollingTime
        basicAnimation.toValue = 1
        basicAnimation.duration = duration
        basicAnimation.speed = 1
        basicAnimation.fillMode = CAMediaTimingFillMode.both
        basicAnimation.isRemovedOnCompletion = true
        self.addCircleLayer(isFill).add(basicAnimation, forKey: "urSoBasic")
    }
    func animateCircleWithDuration1(_ duration: Double, pollingTime: Double = (CacheManager.settings?.polling_time?.doubleValue ?? 0), isFill: Bool = false) {
        let basicAnimation = CABasicAnimation.init(keyPath: "strokeEnd")
        basicAnimation.fromValue = (pollingTime - duration) / pollingTime
        basicAnimation.fromValue = 63
        basicAnimation.toValue = 1
        basicAnimation.duration = duration
        basicAnimation.speed = 1
        basicAnimation.fillMode = CAMediaTimingFillMode.both
        basicAnimation.isRemovedOnCompletion = true
        self.addCircleLayer(isFill).add(basicAnimation, forKey: "urSoBasic")
    }

}
