//
//  UIViewController+Additions.swift
//  Taxi
//
//  Created by on 2/14/19.
//  Copyright © 2019. All rights reserved.
//

import UIKit
import Foundation
import Toast_Swift
import CoreLocation
import UIKit
import MapKit
fileprivate var bottomConstraint : NSLayoutConstraint?
fileprivate var imageCompletion : ((UIImage?)->())?
fileprivate var constraintValue : CGFloat = 0

extension UIViewController {
    
    //MARK:- Pop or dismiss View Controller
    
    func popOrDismiss(animation : Bool){
        
        DispatchQueue.main.async {
            
            if self.navigationController != nil {
                
                self.navigationController?.popViewController(animated: animation)
            } else {
                
                self.dismiss(animated: animation, completion: nil)
            }
            
        }
        
    }
    
    //MARK:- Present
    
    func present(id : String, animation : Bool, from storyboard : UIStoryboard = Router.main){
        
        let vc = storyboard.instantiateViewController(withIdentifier: id)
        self.present(vc, animated: animation, completion: nil)
        
        
    }
    
    //MARK:- Push
    
    func push(id : String, animation : Bool, from storyboard : UIStoryboard = Router.main){
        
        let vc = storyboard.instantiateViewController(withIdentifier: id)
        self.navigationController?.pushViewController(vc, animated: animation)
        
        
    }
    
    //MARK:- Push To Right
    
    func pushRight(toViewController viewController : UIViewController){
        
        self.makePush(transition: CATransitionSubtype.fromLeft.rawValue)
        navigationController?.pushViewController(viewController, animated: false)
        
    }
    
    private func makePush(transition type : String){
        
        let transition = CATransition()
        transition.duration = 0.45
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.default)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype(rawValue: type)
        navigationController?.view.layer.add(transition, forKey: nil)
        
    }
    
    func popLeft() {
        
        self.makePush(transition: CATransitionSubtype.fromRight.rawValue)
        navigationController?.popViewController(animated: true)
        
    }
    
    
    //MARK:- Add observers
    
    func addKeyBoardObserver(with constraint : NSLayoutConstraint){
        
        bottomConstraint = constraint
        
        constraintValue = constraint.constant
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(info:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(info:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(info:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        
    }
    //MARK:- Keyboard will show
    
    @IBAction private func keyboardWillShow(info : NSNotification){
        
        guard let keyboard = (info.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else{
            return
        }
        bottomConstraint?.constant = -(keyboard.height)
        self.view.layoutIfNeeded()
    }
    
    
    //MARK:- Keyboard will hide
    
    @IBAction private func keyboardWillHide(info : NSNotification){
        
        bottomConstraint?.constant = constraintValue
        self.view.layoutIfNeeded()
        
    }
    
}

extension NSObject {
    func call(to number : String?) {
        var newNumber = number?.replacingOccurrences(of: "+", with: "")
        newNumber = newNumber?.replacingOccurrences(of: " ", with: "")
        if let providerNumber = newNumber, let url = URL(string: "tel://\(providerNumber)") {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        } else {
            UIScreen.main.focusedView?.makeToast("Cannot call at this movement".localized)
        }
    }
}

extension UIViewController {
    
    var appDelegate: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    var keyWindow: UIWindow? {
        return UIApplication.shared.keyWindow
    }
    
    var rootViewController: UIViewController? {
        get {
            return self.keyWindow?.rootViewController
        }
        set {
            self.keyWindow?.rootViewController = newValue
        }
    }
    
    func openGoogleMap(_ saddr: String, daddr: String, name:String = "") {
        let hasGoogleMapApp = UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)
        if hasGoogleMapApp {
            let urlString = "comgooglemaps-x-callback://?saddr=&daddr=\(saddr),\(daddr)&directionsmode=driving"
            if let url = URL(string: urlString) {
                UIApplication.shared.open(url, options: [:])
            }
        } else {
             //If google map not installed, open with Apple Map App
            let latitude = CLLocationDegrees(saddr)!
            let longitude = CLLocationDegrees(daddr)
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude!)))
            mapItem.name = name
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
        }
    }
    
    class func showAlertForCameraAccessDenied(inViewController viewController: UIViewController) {
        let cameraPermissionAlert = UIAlertController(title: "Allow access to your camera to start taking photos.", message: "", preferredStyle: UIAlertController.Style.alert)
        cameraPermissionAlert.addAction(UIAlertAction(title: "Enable Camera Access", style: .default, handler: { (action: UIAlertAction!) in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(settingsUrl)
                }
            }
        }))
        cameraPermissionAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
        }))
        viewController.present(cameraPermissionAlert, animated: true, completion: nil)
    }
    

    func share(items : [Any]) {
        let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        self.present(activityController, animated: true, completion: nil)
    }

}

public class IndicatorStyle: NSObject {}

public class NormalIndicatorStyle: IndicatorStyle {}

public class CustomIndicatorStyle: IndicatorStyle {}

//import Common

private let tagValue = 9999

// MARK: - This extension is used for loading activity indicator view throughout the application
extension UIViewController {
    
    @objc open func indicatorStyle() -> IndicatorStyle {
        return  NormalIndicatorStyle()
    }
    
    @objc open func activityIndicatorView() -> UIView {
        return UIActivityIndicatorView(style: .whiteLarge)
    }
    
    /**
     Called to start the activity indicator
     */
    @objc open func startActivityIndicator() {
        
        if let alphaView = self.view.subviews.filter({ $0.tag == tagValue }).first {
            self.view.bringSubviewToFront(alphaView)
            return
        }
        
        let alphaView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        alphaView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleTopMargin, .flexibleLeftMargin, .flexibleRightMargin]
        alphaView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        alphaView.alpha = 1.0
        let activityIndicatorView = self.activityIndicatorView()
        activityIndicatorView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleTopMargin, .flexibleLeftMargin, .flexibleRightMargin]
        activityIndicatorView.frame = CGRect(origin: CGPoint(x: (alphaView.bounds.width/2) - 20, y: (alphaView.bounds.height/2) - 20), size: CGSize(width: 40, height: 40))
        alphaView.addSubview(activityIndicatorView)
        alphaView.tag = tagValue
        
        self.view.addSubview(alphaView)
        self.view.updateConstraintsIfNeeded()
        
        if self.indicatorStyle() is NormalIndicatorStyle {
            (activityIndicatorView as? UIActivityIndicatorView)?.startAnimating()
        }
    }
    
    @objc open func startActivityIndicatorInWindow() {
        
        if let window = UIApplication.shared.keyWindow, let alphaView = window.subviews.filter({ $0.tag == tagValue }).first {
            window.bringSubviewToFront(alphaView)
            return
        } else if let alphaView = self.view.subviews.filter({ $0.tag == tagValue }).first {
            self.view.bringSubviewToFront(alphaView)
            return
        }
        
        let alphaView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        alphaView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleTopMargin, .flexibleLeftMargin, .flexibleRightMargin]
        alphaView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        alphaView.alpha = 1.0
        let activityIndicatorView = self.activityIndicatorView()
        activityIndicatorView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleTopMargin, .flexibleLeftMargin, .flexibleRightMargin]
        activityIndicatorView.frame = CGRect(origin: CGPoint(x: (alphaView.bounds.width/2) - 20, y: (alphaView.bounds.height/2) - 20), size: CGSize(width: 40, height: 40))
        alphaView.addSubview(activityIndicatorView)
        alphaView.tag = tagValue
        
        let addDelagte = UIApplication.shared
        
        if let window = addDelagte.keyWindow {
            alphaView.frame = window.bounds
            window.addSubview(alphaView)
            window.updateConstraintsIfNeeded()
        } else {
            self.view.addSubview(alphaView)
            self.view.updateConstraintsIfNeeded()
        }
        
        if self.indicatorStyle() is NormalIndicatorStyle {
            (activityIndicatorView as? UIActivityIndicatorView)?.startAnimating()
        }
    }
    
    /**
     Called to stop the activity indicator
     */
    @objc open func stopActivityIndicator() {
        if let window = UIApplication.shared.keyWindow, let alphaView = window.subviews.filter({ $0.tag == tagValue }).first {
            alphaView.removeFromSuperview()
        } else if let alphaView = self.view.subviews.filter({ $0.tag == tagValue }).first {
            alphaView.removeFromSuperview()
        }
    }
}

extension UIViewController {
    
    func showLocalizedAlertMessage(_ message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.cancel, handler: { action in
            switch action.style {
            case .default:
                print("Default")
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default:
                print("")
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertMessage(_ message: String, title: String = "") {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: { action in
            switch action.style {
            case .default:
                print("Default")
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default:
                print("")
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWith(_ message: String, title: String = "", completion: ((UIAlertAction.Style) -> (Void))? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: { action in
            completion?(.cancel)
            switch action.style {
            case .default:
                print("Default")
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default:
                print("")
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertMessage(_ title: String = "" ,message: String, actionPhrase: String = "Ok", actionPhrase1: String = "Cancel" , completion: ((UIAlertAction.Style) -> (Void))? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: actionPhrase1, style: UIAlertAction.Style.cancel, handler: { action in
            completion?(.cancel)
            switch action.style {
            case .default:
                print("Default")
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default:
                print("")
            }
        }))
        alert.addAction(UIAlertAction(title: actionPhrase, style: UIAlertAction.Style.default, handler: { action in
            completion?(.default)
            switch action.style {
            case .default:
                print("Default")
            case .cancel:
                print("cancel")
            case .destructive:
                print("destructive")
            @unknown default:
                print("")
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension UIViewController {
    
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        present(viewControllerToPresent, animated: false)
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.25
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        self.view.window!.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false)
    }
}

extension UIViewController {
    func dch_checkDeallocation(afterDelay delay: TimeInterval = 2.0) {
        let rootParentViewController = dch_rootParentViewController
        
        // We don’t check `isBeingDismissed` simply on this view controller because it’s common
        // to wrap a view controller in another view controller (e.g. in UINavigationController)
        // and present the wrapping view controller instead.
        if isMovingFromParent || rootParentViewController.isBeingDismissed {
            let type1 = type(of: self)
            print("Opps...",type1)
            let disappearanceSource: String = isMovingFromParent ? "removed from its parent" : "dismissed"
            
            DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: { [weak self] in
                assert(self == nil, "\(type1) not deallocated after being \(disappearanceSource)")
            })
        }
    }
    
    private var dch_rootParentViewController: UIViewController {
        var root = self
        
        while let parent = root.parent {
            root = parent
        }
        
        return root
    }
}

