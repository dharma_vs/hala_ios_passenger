//
//  Date+Addtions.swift
//  HalaMobility
//
//  Created by Admin on 03/03/22.
//

import Foundation

extension Date {
    var components:DateComponents {
        let cal = Calendar.current
        return cal.dateComponents(Set([.year, .month, .day, .hour, .minute, .second, .weekday, .weekOfYear, .yearForWeekOfYear]), from: self)
    }
}

typealias MonthDetail = (currentMonthDaysCount : Int?, weekDay: Int?, previousMonthLastDay : Int?, currentDay: Int?)

var calendar: Calendar = {
    var calendar = Calendar.current
    calendar.locale = Locale.current
    return calendar
}()

extension Date {
    
    static func monthDetailFrom( _ month: Int, year: Int = calendar.component(.year, from: Date())) -> MonthDetail {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        
        var date = Date()
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = 1
        date = calendar.date(from: dateComponents) ?? Date()
        let range = calendar.range(of: .day, in: .month, for: date)
        let components = date.components
        
        dateComponents.year = month == 1 ? year - 1 : year
        
        return (range?.count, components.weekday, date.addingTimeInterval(-60*60*24).components.day,Date().components.day)
    }
    
    func currentMonth() -> Int {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        return calendar.component(.month, from: self)
    }
    
    func currentYear() -> Int {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        return calendar.component(.year, from: self)
    }
    
    func currentDay() -> Int {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        return calendar.component(.day, from: self)
    }
    
    func dateFromFormater(_ format: String) -> String? {
        let dateFormater = DateFormatter()
//        dateFormater.locale = Locale.current
        dateFormater.timeZone = TimeZone.current
        dateFormater.dateFormat = format
        return dateFormater.string(from: self)
    }
    
    func utcDateFromFormater(_ format: String) -> String? {
        let dateFormater = DateFormatter()
        dateFormater.timeZone = TimeZone.init(identifier: "UTC")
        dateFormater.dateFormat = format
        return dateFormater.string(from: self)
    }
    
    func currentMinutes() -> Int {
        let calendar = Calendar.current
        let time=calendar.dateComponents([.minute], from: Date())
        return time.value(for: .minute) ?? 0
    }
    
    func currentHour() -> Int {
        let calendar = Calendar.current
        let time=calendar.dateComponents([.hour], from: Date())
        return time.value(for: .hour) ?? 0
    }
    
    static func dateFrom(month: Int, day: Int, year: Int, minute: Int = 0) -> Date? {
        var calendar = Calendar.current
        calendar.locale = Locale.current
        var dateComponents = DateComponents()
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.minute = minute
        return calendar.date(from: dateComponents)
    }
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
}

