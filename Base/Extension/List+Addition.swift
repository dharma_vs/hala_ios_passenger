//
//  List+Addition.swift
//  HalaMobility
//
//  Created by Selladurai Murugesan on 04/03/22.
//

import UIKit
import Foundation

extension UITableView {
    func dequeueCell<T: NSObject>(_ indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withIdentifier: T.className, for: indexPath) as! T
    }
    
    func dequeueCell<T: NSObject>() -> T {
        return self.dequeueReusableCell(withIdentifier: T.className) as! T
    }
}

extension UICollectionView {
    func dequeueCell<T: NSObject>(_ indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withReuseIdentifier: T.className, for: indexPath) as! T
    }
}
