//
//  String+Additions.swift
//  HalaMobility
//
//  Created by Admin on 21/02/22.
//

import Foundation

extension String {
    
    static var Empty : String {
        return ""
    }
    
    static func removeNil(_ value : String?) -> String {
        return value ?? String.Empty
    }
    
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    var isSpecialChar: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.symbols.inverted) == nil
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    func boolValue() -> Bool {
        if self == "true" || self == "True" || self == "TRUE" || self == "1" || self == "yes" || self == "YES" || self == "Yes" {
            return true
        }
        return false
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func intValue() -> Int {
        return Int(self) ?? 0
    }
    
    func dateWithFormat(_ format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let date = dateFormatter.date(from:self)!
        return date
    }
    
    func UTCToLocal(_ formatFrom:String, formatTo: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatFrom
        dateFormatter.timeZone = TimeZone.current//TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = formatTo
        return dateFormatter.string(from: dt ?? Date())
    }
    func UTCToLocal1(_ formatFrom:String, formatTo: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatFrom
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = formatTo
        return dateFormatter.string(from: dt ?? Date())
    }
    //MARK:- Convert Local To UTC Date by passing date formats value
    func localToUTC(incomingFormat: String, outGoingFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = incomingFormat
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = outGoingFormat
        
        return dateFormatter.string(from: dt ?? Date())
    }
    func toDateTime() -> NSDate{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFromString = dateFormatter.date(from: self)
        
        //  let dateFromString : NSDate = dateFormatter.date(from: self)! as NSDate
        return dateFromString! as NSDate
    }
    
    var currencyValue: String { return (((CacheManager.settings?.currency_denote?.lowercased() ?? "symbol") == "symbol") ? CacheManager.currencySymbol : CacheManager.currencyShortName) + " " + self }
    
    var usdValue: String { return "USD" + " " + self }
    
    var currencySymbolValue: String { return CacheManager.currencySymbol + " " + self }
    
    var distanceValue: String { return self + " " + "km".localized }
    
    var timeMinValue: String { return self + " " + (self == "1" ? "Min" : "Min") }
    
    var timeHourValue: String { return self + " " + (self == "1" ? "Hr" : "Hrs") }
    
    var timeValue: String {
        let mins = Int(self) ?? 0
        let hrs = mins/60
        let day = hrs/24
        var timeStr = ""
        if day > 0 {
            timeStr += "\(day) " + (day > 1 ? "Days" : "Day")
        }
        if hrs%24 > 0 {
            timeStr += " \(hrs%24) " + ((hrs%24) > 1 ? "Hours" : "Hour")
        }
        if mins%60 > 0 {
            timeStr += " \(mins%60) " + ((mins%60) > 1 ? "Mins" : "Min")
        }
        if timeStr == ""{
            timeStr = "0 Min"
        }
        
        return timeStr
    }
}
extension Double {
    func rounded(digits: Int) -> Double {
        let multiplier = pow(10.0, Double(digits))
        return (self * multiplier).rounded() / multiplier
    }
}
